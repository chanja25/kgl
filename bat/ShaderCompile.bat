@echo off

rem	ソリューションからの相対位置
rem pushd "$(SolutionDir)"
rem call ".\bat\ShaderCompile.bat"
rem popd

set TOOL_PATH="./tools/ShaderCompiler"
set SHADER_PATH="./Library/kglShader/Shaders"

set OUTPUT_SM3_PATH="./Skeleton/Data/Shaders/SM3"
call %TOOL_PATH%\ShaderCompiler.exe %SHADER_PATH% -PLATFORM SM3 -O %OUTPUT_SM3_PATH%

set OUTPUT_SM5_PATH="./Skeleton/Data/Shaders/SM5"
call %TOOL_PATH%\ShaderCompiler.exe %SHADER_PATH% -PLATFORM SM5 -O %OUTPUT_SM5_PATH%

rem set OUTPUT_SM5_PATH="./Skeleton/Data/Shaders/GL"
rem call %TOOL_PATH%\ShaderCompiler.exe %SHADER_PATH% -PLATFORM GL -O %OUTPUT_GL_PATH%

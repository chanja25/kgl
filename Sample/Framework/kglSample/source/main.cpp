//----------------------------------------------------------------------
//!
//! @file	main.cpp
//! @brief	メイン
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "../../kgl/include/Utility/kgl.CommandLine.h"
#include "../../kgl/include/Utility/Pool/kgl.Pool.h"

//----------------------------------------------------------------------
// library
//----------------------------------------------------------------------
#if	defined(_WIN64)

#if	defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/x64/Debug/kgl.lib")
#else	// ~#if defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/x64/Release/kgl.lib")
#endif	// ~#if defined(_KGL_DEBUG)

#else	// ~#if defined(_WIN64)

#if	defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/win32/Debug/kgl.lib")
#else	// ~#if defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/win32/Release/kgl.lib")
#endif	// ~#if defined(_KGL_DEBUG)

#endif	// ~#if defined(_WIN64)


//	スマートポインタ定義
KGL_SMART_PTR_CLASS(CCommandLine, kgl::Utility::ICommandLine, kgl::Utility::Create);
KGL_SMART_PTR_CLASS(CStopwatch, kgl::Time::IStopwatch, kgl::Time::Create);
KGL_SMART_PTR_CLASS(CWindow, kgl::Window::IWindow, kgl::Window::Create);

//----------------------------------------------------------------------
//! アプリケーションの実行
//! @return 実行結果
//----------------------------------------------------------------------
s32 AppRun(CCommandLine CommandLine)
{
	//	ウィンドウの生成、初期化
	CWindow Window;
	Window->SetStyle(kgl::Window::EStyle::OverlappedWindow);
	Window->Create("KGL Library Template");
	//	システム生成
	App::System::CSystem System;

	//	KGLシステムの初期化
	System.InitializeKGL(Window, false);

	//	実行
	System.Run();

	//	KGLシステムの破棄
	System.FinalizeKGL();
 
	return System.GetExitCode();
}

//----------------------------------------------------------------------
//! エントリーポイント
//! @return 実行結果
//----------------------------------------------------------------------
s32 APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, s32)
{
	s32 iExitCode;
	{
		CCommandLine CommandLine;

		CommandLine->Initialize();

		iExitCode = AppRun(CommandLine);

		CommandLine->Finalize();
	}

	return iExitCode;
}

//======================================================================
//	END OF FILE
//======================================================================
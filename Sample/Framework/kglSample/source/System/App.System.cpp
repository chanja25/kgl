//----------------------------------------------------------------------//!
//! @file	main.cpp
//! @brief	メイン
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
#if	defined(_KGL_DEBUG)
	//	デバッグフォント
	kgl::Font::IFont* pDebugFont = null;
#endif	// ~#if defined(_KGL_DEBUG)

	//	シーン管理
	kgl::Scene::ISceneManager* pSceneManager = null;
	//	インプット
	App::Input::CInput* pInput = null;

	namespace System
	{
		kgl::Scene::ISceneBase* CreateSceneDebugTitle(void)
		{
			kgl::Scene::ISceneBase* pScene;
			pScene = new Scene::SceneDebugTitle;
			return pScene;
		}

		kgl::Scene::ISceneBase* CreateSceneTest(void)
		{
			kgl::Scene::ISceneBase* pScene;
			pScene = new Scene::SceneTest;
			return pScene;
		}

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CSystem::CSystem(void)
#if	defined(_KGL_DEBUG)
			: m_pFPSCounter(null)
#endif	// ~#if defined(_KGL_DEBUG)
		{
		}

		//----------------------------------------------------------------------
		// デストラクタ
		//----------------------------------------------------------------------
		CSystem::~CSystem(void)
		{
		}

		//----------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 CSystem::Initialize(void)
		{
#if	defined(_KGL_DEBUG)
			//	FPSカウンター生成
			kgl::Frame::Create(&m_pFPSCounter);
			//	デバッグフォント生成
			kgl::Font::Create(&pDebugFont);
			pDebugFont->Initialize(24, 400, false, "MS明朝");
#endif	// ~#if defined(_KGL_DEBUG)

			//m_pFrameControl->SetTargetFPS(30);

			//	シーン管理クラス生成
			kgl::Scene::Create(&pSceneManager);

			//	シーンのセット
			{
				//	デバッグタイトル
				pSceneManager->SetScene(ESceneMode::DEBUG_TITLE, CreateSceneDebugTitle);

				//	テスト
				pSceneManager->SetScene(ESceneMode::TEST, CreateSceneTest);
			}

			//	初期シーンの設定
			pSceneManager->JumpScene(ESceneMode::DEBUG_TITLE);

			//	インプット生成
			pInput = new Input::CInput;

			return true;
		}

		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CSystem::Finalize(void)
		{
#if	defined(_KGL_DEBUG)
			kgl::SafeRelease(m_pFPSCounter);

			if( pDebugFont )
			{
				pDebugFont->Finalize();
				kgl::SafeRelease(pDebugFont);
			}
#endif	// ~#if defined(_KGL_DEBUG)

			kgl::SafeDelete(pInput);
			kgl::SafeRelease(pSceneManager);
		}

		//----------------------------------------------------------------------
		//	更新
		//!	@param fElapsedTime [in] 経過時間
		//----------------------------------------------------------------------
		void CSystem::Update(f32 fElapsedTime)
		{
			pSceneManager->Update(fElapsedTime);
#if	defined(_KGL_DEBUG)
			//	FPSカウンター更新
			m_pFPSCounter->Update(fElapsedTime);
#endif	// ~#if defined(_KGL_DEBUG)
		}

		//----------------------------------------------------------------------
		//	描画
		//!	@param fElapsedTime [in] 経過時間
		//----------------------------------------------------------------------
		void CSystem::Render(f32 fElapsedTime)
		{
			pSceneManager->Render(fElapsedTime);

#if	defined(_KGL_DEBUG)
			//	キーの状態表示
			pDebugFont->Draw(0, 720, kgl::CharSet::Format("×:%d ○:%d □:%d △%d\nL1:%d R1:%d L2:%d R2:%d\nSELECT:%d START:%d\n↑:%d ↓:%d ←:%d →:%d",
									pInput->IsKeyPress(EKeyCode::CROSS),
									pInput->IsKeyPress(EKeyCode::CIRCLE),
									pInput->IsKeyPress(EKeyCode::SQUARE),
									pInput->IsKeyPress(EKeyCode::TRIANGLE),
									pInput->IsKeyPress(EKeyCode::L1),
									pInput->IsKeyPress(EKeyCode::R1),
									pInput->IsKeyPress(EKeyCode::L2),
									pInput->IsKeyPress(EKeyCode::R2),
									pInput->IsKeyPress(EKeyCode::SELECT),
									pInput->IsKeyPress(EKeyCode::START),
									pInput->IsKeyPress(EKeyCode::UP),
									pInput->IsKeyPress(EKeyCode::DOWN),
									pInput->IsKeyPress(EKeyCode::LEFT),
									pInput->IsKeyPress(EKeyCode::RIGHT)),
									0xff00ff00, kgl::Font::EFormat::BOTTOM);

			//	FPS表示
			pDebugFont->Draw(0, 0, kgl::CharSet::Format("FPS:%.3f", m_pFPSCounter->GetFPS()), 0xff00ff00);
			//	メモリ表示
			pDebugFont->Draw(0, 24, kgl::CharSet::Format("Memory:%dK", kgl::Memory::GetAllocateSize() >> 10), 0xff00ff00);
#endif	// ~#if defined(_KGL_DEBUG)
		}
	}
}


//======================================================================
//	END OF FILE
//======================================================================
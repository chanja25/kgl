//----------------------------------------------------------------------
//!
//!	@file	App.SceneDebugTitle.cpp
//!	@brief	デバッグタイトル
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#pragma once

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Scene
	{
		//----------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 SceneDebugTitle::Initialize(void)
		{
			m_ucSelect = 0;

			return true;
		}

		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void SceneDebugTitle::Finalize(void)
		{
		}

		//----------------------------------------------------------------------
		//	更新
		//!	@param fElapsedTime [in] 経過時間
		//----------------------------------------------------------------------
		void SceneDebugTitle::Update(f32 fElapsedTime)
		{
			(void)fElapsedTime;

			if( pInput->IsKeyPush(EKeyCode::UP) )
				m_ucSelect --;
			if( pInput->IsKeyPush(EKeyCode::DOWN) )
				m_ucSelect ++;

			m_ucSelect = kgl::Clamp(m_ucSelect, (u8)0, (u8)1);

			if( pInput->IsKeyPush(EKeyCode::START) )
			{
				switch( m_ucSelect )
				{
				case 0:
					pSceneManager->JumpScene(ESceneMode::TEST);
					break;

				default:
					pSceneManager->JumpScene(ESceneMode::TEST);
					break;
				}
			}
		}

		//----------------------------------------------------------------------
		//	描画
		//!	@param fElapsedTime [in] 経過時間
		//----------------------------------------------------------------------
		void SceneDebugTitle::Render(f32 fElapsedTime)
		{
			(void)fElapsedTime;
#if	defined(_KGL_DEBUG)
			s32 iY = 48;
			//	debug
			pDebugFont->Draw(0, iY, "DebugTitle", 0xff00ff00);

			//	シーン名
			c8* pScene[] =
			{
				"Test1",
				"Test2",
				null,
			};

			for( u32 i=0; pScene[i] != null; i++ )
			{
				iY += 24;
				u32 uiColor = (i == m_ucSelect)? 0xff00ff00: 0x7f00ff00;
				pDebugFont->Draw(0, iY, pScene[i], uiColor);
			}

			//	FPS表示
			pDebugFont->Draw(1280, 0, "Scene:DebugTitle", 0xff00ff00, kgl::Font::EFormat::RIGHT);
#endif	// ~#if defined(_KGL_DEBUG)
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
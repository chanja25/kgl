//----------------------------------------------------------------------
//!
//!	@file	App.SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#pragma once

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Scene
	{
		//----------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 SceneTest::Initialize(void)
		{
			kgl::Time::Create(&m_pStopwatch);

			kgl::File::IFile* pFile;
			kgl::File::Create(&pFile);

			if( pFile->Open("Data/Sound/BGM.ogg") )
			{
				s32 iSize = pFile->GetSize();
				void* pData = kgl::Memory::Allocate(iSize);
				pFile->Reading(pData, iSize);
				pFile->Close();

				kgl::Sound::Create(&m_pSound);
				m_pSound->Initialize(1);
				m_pSound->Create(0, pData);
				m_pSound->Play(0);

				kgl::Memory::Free(pData);
			}
			
			kgl::Sound::Create(&m_pStream);
			if( pFile->Open("Data/Sound/BGM.ogg") )
			{
				m_pStream->Initialize(pFile);
			}
			kgl::SafeRelease(pFile);
			return true;
		}

		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void SceneTest::Finalize(void)
		{
			if( m_pSound )
			{
				m_pSound->Cleanup(0);
				m_pSound->Finalize();
			}
			if( m_pStream )
			{
				m_pStream->Finalize();
			}
			kgl::SafeRelease(m_pStream);
			kgl::SafeRelease(m_pSound);
			kgl::SafeRelease(m_pStopwatch);
		}

		//----------------------------------------------------------------------
		//	更新
		//!	@param fElapsedTime [in] 経過時間
		//----------------------------------------------------------------------
		void SceneTest::Update(f32 fElapsedTime)
		{
			(void)fElapsedTime;

			BEGIN_PROFILE("Allocate", 0);

			if( pInput->IsKeyPush(EKeyCode::START) )
			{
				pSceneManager->JumpScene(ESceneMode::DEBUG_TITLE);
			}
			if( pInput->IsKeyPush(EKeyCode::CROSS) && m_pStream->IsReady() )
			{
				if( m_pSound->IsPlay(0) )
				{
					m_pSound->Stop(0);
					m_pStream->SetVolume(-10000);
					m_pStream->SetMode(kgl::Sound::ESoundStreamMode::FADEIN, 60);
					m_pStream->Play();
				}
				else
				{
					m_pStream->Stop();
					m_pSound->Play(0, true);
				}
			}

			END_PROFILE("Allocate");
		}

		//----------------------------------------------------------------------
		//	描画
		//!	@param fElapsedTime [in] 経過時間
		//----------------------------------------------------------------------
		void SceneTest::Render(f32 fElapsedTime)
		{
			(void)fElapsedTime;

#if	defined(_KGL_DEBUG)

			kgl::Profiler::Log* pLog = PROFILER.GetLog("Allocato");
			if( pLog )
			{
				pDebugFont->Draw(1280, 24, kgl::CharSet::Format("Allocato AVG Time:%f", pLog->fAverageTime), 0xff00ff00, kgl::Font::EFormat::RIGHT);
				pDebugFont->Draw(1280, 48, kgl::CharSet::Format("Allocato Time:%f", pLog->fEndTime - pLog->fBeginTime), 0xff00ff00, kgl::Font::EFormat::RIGHT);
			}
			pDebugFont->Draw(1280, 64, kgl::CharSet::Format("Time:%f", m_pStopwatch->Get()*0.001), 0xff00ff00, kgl::Font::EFormat::RIGHT);

			//	FPS表示
			pDebugFont->Draw(1280, 0, "Scene:SceneTest", 0xff00ff00, kgl::Font::EFormat::RIGHT);
#endif	// ~#if defined(_KGL_DEBUG)
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
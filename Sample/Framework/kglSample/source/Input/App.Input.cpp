//----------------------------------------------------------------------
//!
//!	@file	App.CInput.cpp
//!	@brief	インプット
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace Input
	{
		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CInput::CInput(void)
			: m_pKeyboard(null)
		{
			Initialize();
		}

		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CInput::~CInput(void)
		{
			kgl::SafeDelete(m_pKeyboard);
		}

		//----------------------------------------------------------------------
		//	初期化
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 CInput::Initialize(void)
		{
			//	キーボードクラスの生成
			kgl::Input::Create(&m_pKeyboard);

			//	キーの初期設定
			SetKeyMapping(EKeyCode::CROSS,		kgl::Input::EKey::KEY_Z);
			SetKeyMapping(EKeyCode::CIRCLE,		kgl::Input::EKey::KEY_X);
			SetKeyMapping(EKeyCode::SQUARE,		kgl::Input::EKey::KEY_C);
			SetKeyMapping(EKeyCode::TRIANGLE,	kgl::Input::EKey::KEY_V);
			SetKeyMapping(EKeyCode::L1,			kgl::Input::EKey::KEY_A);
			SetKeyMapping(EKeyCode::R1,			kgl::Input::EKey::KEY_S);
			SetKeyMapping(EKeyCode::L2,			kgl::Input::EKey::KEY_Q);
			SetKeyMapping(EKeyCode::R2,			kgl::Input::EKey::KEY_W);
			SetKeyMapping(EKeyCode::START,		kgl::Input::EKey::KEY_RETURN);
			SetKeyMapping(EKeyCode::SELECT,		kgl::Input::EKey::KEY_SPACE);
			SetKeyMapping(EKeyCode::UP,			kgl::Input::EKey::KEY_UP);
			SetKeyMapping(EKeyCode::DOWN,		kgl::Input::EKey::KEY_DOWN);
			SetKeyMapping(EKeyCode::LEFT,		kgl::Input::EKey::KEY_LEFT);
			SetKeyMapping(EKeyCode::RIGHT,		kgl::Input::EKey::KEY_RIGHT);

			return true;
		}									  

		//----------------------------------------------------------------------
		//	キーのセット
		//!	@param iKey [in] キー番号
		//!	@param iKeyCode [in] キーコード
		//----------------------------------------------------------------------
		void CInput::SetKeyMapping(u32 uiKey, u32 uiKeyCode)
		{
			m_KeyMap[uiKey] = uiKeyCode;
		}

		//----------------------------------------------------------------------
		//	キーが押されているかチェック
		//!	@param iKey [in] キー
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 CInput::IsKeyPress(u32 uiKey)
		{
			if( m_KeyMap.find(uiKey) != m_KeyMap.end() )
				return m_pKeyboard->IsKeyPress(m_KeyMap[uiKey]);

			return m_pKeyboard->IsKeyPress(uiKey);
		}

		//----------------------------------------------------------------------
		//	キーを押したかチェック
		//!	@param iKey [in] キー
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 CInput::IsKeyPush(u32 uiKey)
		{
			if( m_KeyMap.find(uiKey) != m_KeyMap.end() )
				return m_pKeyboard->IsKeyPush(m_KeyMap[uiKey]);

			return m_pKeyboard->IsKeyPush(uiKey);
		}

		//----------------------------------------------------------------------
		//	キーが離されたかチェック
		//!	@param iKey [in] キー
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 CInput::IsKeyRelease(u32 uiKey)
		{
			if( m_KeyMap.find(uiKey) != m_KeyMap.end() )
				return m_pKeyboard->IsKeyRelease(m_KeyMap[uiKey]);

			return m_pKeyboard->IsKeyRelease(uiKey);
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
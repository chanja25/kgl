//----------------------------------------------------------------------
//!
//!	@file	App.SceneTest.h
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__APP_SCENETEST_H__
#define	__APP_SCENETEST_H__
#endif	// __APP_SCENETEST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../App.h"
#include "../../../kgl/include/Time/kgl.Time.h"
#include "../../../kgl/include/Sound/kgl.Sound.h"

namespace App
{
	namespace Scene
	{
		//======================================================================
		//!	テスト用シーン
		//======================================================================
		class SceneTest
			: public kgl::Scene::ISceneBase
		{
		public:
			//!	初期化
			b8 Initialize(void);
			//!	破棄
			void Finalize(void);

			//!	更新
			void Update(f32 fElapsedTime);
			//!	描画
			void Render(f32 fElapsedTime);

		private:
			kgl::Time::IStopwatch*		m_pStopwatch;	//!< タイマー
			kgl::Sound::ISoundManager*	m_pSound;		//!< サウンド
			kgl::Sound::IStreamSound*	m_pStream;		//!< ストリーム
		};
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	App.SceneDebugTitle.h
//!	@brief	デバッグタイトル
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__APP_SCENEDEBUGTITLE_H__
#define	__APP_SCENEDEBUGTITLE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../App.h"
#include "../../../kgl/include/Scene/kgl.Scene.h"

namespace App
{
	namespace Scene
	{
		//======================================================================
		//!	デバッグタイトル
		//======================================================================
		class SceneDebugTitle
			: public kgl::Scene::ISceneBase
		{
		public:
			//!	初期化
			b8 Initialize(void);
			//!	破棄
			void Finalize(void);

			//!	更新
			void Update(f32 fElapsedTime);
			//!	描画
			void Render(f32 fElapsedTime);

		public:
			u8	m_ucSelect;	//!< セレクト
		};
	}
}

#endif	// __APP_SCENEDEBUGTITLE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
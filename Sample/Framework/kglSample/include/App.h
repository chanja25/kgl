//----------------------------------------------------------------------
//!
//! @file	App.h
//! @brief	アプリケーション
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__APP_H__
#define	__APP_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl/include/kgl.h"
#include "../../kgl/include/Render/Font/kgl.Font.h"

#include "Input/App.Input.h"
#include "System/App.System.h"

#include "Scene/App.Scene.h"


namespace App
{
#if	defined(_KGL_DEBUG)
	extern kgl::Font::IFont* pDebugFont;
#endif	// ~#if defined(_KGL_DEBUG)

	//	シーン管理
	extern kgl::Scene::ISceneManager* pSceneManager;
	//	インプット
	extern App::Input::CInput* pInput;

	//!	シーン
	namespace ESceneMode
	{
		//const u8 TITLE = 0;
		//const u8 GAME = 1;
		//const u8 CREAR = 2;

		const u8 TEST = 254;
		const u8 DEBUG_TITLE = 255;
	}

	//!	キーコード
	namespace EKeyCode
	{
		const u32 CROSS = 0;
		const u32 CIRCLE = 1;
		const u32 SQUARE = 2;
		const u32 TRIANGLE = 3;
		const u32 L1 = 4;
		const u32 R1 = 5;
		const u32 L2 = 6;
		const u32 R2 = 7;
		const u32 SELECT = 8;
		const u32 START = 9;
		const u32 UP = 10;
		const u32 DOWN = 11;
		const u32 LEFT = 12;
		const u32 RIGHT = 13;
	}
}

#endif	// __APP_H__
//======================================================================
//	END OF FILE
//======================================================================
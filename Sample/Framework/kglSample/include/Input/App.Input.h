//----------------------------------------------------------------------
//!
//!	@file	App.Input.h
//!	@brief	インプット
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__APP_INPUT_H__
#define	__APP_INPUT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../App.h"
#include "../../../kgl/include/Input/kgl.Input.h"
#include <map>

namespace App
{
	namespace Input
	{
		//======================================================================
		//!	インプットクラス
		//======================================================================
		class CInput
		{
		public:
			//!	コンストラクタ
			CInput(void);
			//!	デストラクタ
			~CInput(void);

			//!	初期化
			b8 Initialize(void);

			//!	キーのセット
			void SetKeyMapping(u32 uiKey, u32 uiKeyCode);

			//!	キーが押されているかチェック
			b8 IsKeyPress(u32 uiKey);
			//!	キーを押したかチェック
			b8 IsKeyPush(u32 uiKey);
			//!	キーが離されたかチェック
			b8 IsKeyRelease(u32 uiKey);

		private:
			std::map<u32, u32>		m_KeyMap;		//!< キーマップ
			kgl::Input::IKeyboard*	m_pKeyboard;	//!< キーボード
		};
	}
}

#endif	// __APP_INPUT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
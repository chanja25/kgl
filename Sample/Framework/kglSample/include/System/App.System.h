//----------------------------------------------------------------------
//!
//! @file	App.System.h
//! @brief	システム
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__APP_SYSTEM_H__
#define	__APP_SYSTEM_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../App.h"
#include "../../../kgl/include/System/kgl.System.h"

namespace App
{
	namespace System
	{
		//======================================================================
		//	システムクラス
		//======================================================================
		class CSystem
			: public kgl::System::ISystem
		{
		public:
			//!	コンストラクタ
			CSystem(void);
			//!	デストラクタ
			~CSystem(void);

		private:
			//!	初期化
			b8 Initialize(void);
			//!	破棄
			void Finalize(void);

			//!	更新
			void Update(f32 fElapsedTime);
			//!	描画
			void Render(f32 fElapsedTime);

		private:
#if	defined(_KGL_DEBUG)
			kgl::Frame::IFPSCounter* m_pFPSCounter;
#endif	// ~#if defined(_KGL_DEBUG)
		};
	}
}

#endif	// __APP_SYSTEM_H__
//======================================================================
//	END OF FILE
//======================================================================
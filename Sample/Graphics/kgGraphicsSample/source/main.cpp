//----------------------------------------------------------------------
//!
//! @file	main.cpp
//! @brief	メイン
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphicsSample.h"

//----------------------------------------------------------------------
// library
//----------------------------------------------------------------------
#if	defined(_WIN64)

#if	defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/x64/Debug/kgl.lib")
#else	// ~#if defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/x64/Release/kgl.lib")
#endif	// ~#if defined(_KGL_DEBUG)

#else	// ~#if defined(_WIN64)

#if	defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/win32/Debug/kgl.lib")
#else	// ~#if defined(_KGL_DEBUG)
#pragma comment(lib, "../KGL/win32/Release/kgl.lib")
#endif	// ~#if defined(_KGL_DEBUG)

#endif	// ~#if defined(_WIN64)


//----------------------------------------------------------------------
//! エントリーポイント
//! @return 実行結果
//----------------------------------------------------------------------
s32 APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, s32)
{
	return 0;
}

//======================================================================
//	END OF FILE
//======================================================================
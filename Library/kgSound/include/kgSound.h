//----------------------------------------------------------------------
//!
//!	@file	kgSound.h
//!	@brief	Kisaragi Game Sound ヘッダー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_SOUND_H__
#define	__KG_SOUND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../../kgl/include/kgl.h"

#if	KGL_DEBUG
#define KG_SOUND_DEBUG 1
#else	// ~#if KGL_DEBUG
#define KG_SOUND_DEBUG 0
#endif	// ~#if KGL_DEBUG

#include "Utility/kgSoundUtility.h"

//----------------------------------------------------------------------
// Kisaragi Game Graphics
//----------------------------------------------------------------------
namespace kgSound
{
	//======================================================================
	//	サウンドデバイスインターフェース
	//======================================================================
	interface ISoundDevice
		: public kgl::IKGLBase
	{
	public:
		//!	マスターボリューム設定
		//!	@param fVolume [in] マスターボリューム
		virtual void SetVolume(f32 fVolume) = 0;
		//!	マスターボリューム取得
		//!	@return マスターボリューム
		virtual f32 GetVolume(void) = 0;
	};

	//======================================================================
	//	サウンドドライバーインターフェース
	//======================================================================
	interface ISoundDriver
		: public kgl::IKGLBase
	{
	public:
		//!	デバイスの取得
		//!	@return デバイス
		FORCEINLINE ISoundDevice* SoundDevice(void) { return m_pDevice; }
		//!	サウンドバッファ管理の取得
		//!	@return サウンドバッファ管理
		FORCEINLINE ISoundBufferManager* SoundBufferManager(void) { return m_pSoundBufferManager; }
		//!	デコード管理の取得
		//!	@return デコード管理
		FORCEINLINE IDecoderManager* DecoderManager(void) { return m_pDecoderManager; }

	protected:
		kgl::TKGLPtr<ISoundDevice>			m_pDevice;				//!< サウンドデバイス
		kgl::TKGLPtr<ISoundBufferManager>	m_pSoundBufferManager;	//!< サウンドバッファ管理
		kgl::TKGLPtr<IDecoderManager>		m_pDecoderManager;		//!< デコード管理
	};
}

#if	USE_XAUDIO2
#include "XAudio2Driver/XAudio2Driver.h"
#endif	// ~#if USE_XAUDIO2

#endif	// __KG_SOUND_H__
//======================================================================
//	END OF FILE
//======================================================================
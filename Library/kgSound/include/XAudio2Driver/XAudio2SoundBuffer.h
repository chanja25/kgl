//----------------------------------------------------------------------
//!
//!	@file	XAudio2SoundBuffer.h
//!	@brief	サウンドバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__XAUDIO2_SOUND_BUFFER_H__
#define	__XAUDIO2_SOUND_BUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgSound.h"
#include "XAudio2Device.h"

namespace kgSound
{
	//!	バッファ情報
	struct SoundBuffer
	{
		IXAudio2SourceVoice*	pVoice;		//!< サウンド再生用
		kgl::TSmartPtr<s8>		pBuffer;	//!< バッファ
		bool					bLock;		//!< ロックの有無
		bool					bLoop;		//!< ループフラグ
		bool					bPause;		//!< 一時停止中か？
	};

	//======================================================================
	//!	サウンドバッファ
	//======================================================================
	class CXAudio2SoundBuffer
		: public ISoundBuffer
	{
	public:
		//!	コンストラクタ
		CXAudio2SoundBuffer(void);
		//!	デストラクタ
		~CXAudio2SoundBuffer(void);

	public:
		//!	サウンドバッファ生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] サウンドバッファ情報
		//!	@param pData	[in] データ
		//!	@return 結果
		b8 Create(kgl::TKGLPtr<CXAudio2Device> pDevice, const WaveInfo& Info, const void* pData);
		//!	サウンドバッファ生成
		//!	@param pDevice	[in] デバイス
		//!	@param pDecoder	[in] デコーダー
		//!	@return 結果
		b8 Create(kgl::TKGLPtr<CXAudio2Device> pDevice, IDecoder* pDecoder);

	public:
		//!	再生
		//!	@return 結果
		b8 Play(b8 bLoop, f32 fOffset);
		//!	停止
		void Stop(void);
		//!	一時停止
		void Pause(void);
		//!	一時停止の解除
		void Resume(void);

		//!	状態の取得
		//!	@return 状態(EState参照)
		u8 GetState(void);

		//!	ボリューム設定
		//!	@param fVolume [in] ボリューム
		void SetVolume(f32 fVolume);
		//!	ボリューム取得
		//!	@return ボリューム
		f32 GetVolume(void);
		//!	ボリューム設定
		//!	@param uiChannels	[in] チャンネル
		//!	@param pVolume		[in] ボリューム
		void SetChannelVolumes(u32 uiChannels, const f32* pVolume);
		//!	ボリューム取得
		//!	@param uiChannels	[in]  チャンネル
		//!	@param pVolume		[out] ボリューム
		void GetChannelVolumes(u32 uiChannels, f32* pVolume);

		//!	周波数調整比の設定
		//!	@param fRate [in] レート(0.5fで1オクターブ下がる。2.0fで1オクターブ上がる)
		void SetFrequencyRatio(f32 fRate);
		//!	周波数調整比の取得
		//!	@return ボリューム
		f32 GetFrequencyRatio(void);

		//!	現在の再生時間を取得
		//!	@return 現在の再生時間
		f32 CurrentTime(void);
		//!	現在の再生サンプル数を取得
		//!	@return 現在の再生サンプル数
		u32 CurrentSamples(void);

		//!	ループフラグ取得
		//!	@return ループフラグ
		b8 IsLoop(void);

	public:
		//!	バッファのロック
		//!	@param uiOffset	[in]  オフセット位置
		//!	@param uiSize	[in]  サイズ
		//!	@param ppBuf1	[out] バッファ1
		//!	@param pSize1	[out] サイズ1
		//!	@param ppBuf2	[out] バッファ2(オフセット位置+サイズがバッファの末尾を超えている場合は回り込んだバッファが設定される)
		//!	@param pSize2	[out] サイズ2(オフセット位置+サイズがバッファの末尾を超えている場合は回り込んだバッファサイズが設定される)
		void Lock(u32 uiOffset, u32 uiSize, void** ppBuf1, u32* pSize1, void** ppBuf2, u32* pSize2);
		//!	バッファのアンロック
		void Unlock(void);

	public:
		//!	サウンドバッファの取得
		//!	@return サウンドバッファ
		INLINE void* GetSoundBuffer(void) { return m_Buffer.pVoice; }

	private:
		SoundBuffer	m_Buffer;
		u64			m_u64StartSample;
		u64			m_u64OffsetSample;
	};

	//======================================================================
	//	サウンドバッファ管理
	//======================================================================
	class CXAudio2SoundBufferManager
		: public ISoundBufferManager
	{
	public:
		//!	コンストラクタ
		CXAudio2SoundBufferManager(kgl::TKGLPtr<CXAudio2Device> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	サウンドバッファ生成
		//!	@param ppBuffer	[out] サウンドバッファ
		//!	@param Info		[in]  サウンドバッファ情報
		//!	@return 結果
		b8 CreateSoundBuffer(ISoundBuffer** ppBuffer, const WaveInfo& Info, const void* pData);
		//!	サウンドバッファ生成
		//!	@param ppBuffer	[out] テクスチャー
		//!	@param pDecoder	[in]  デコーダー
		//!	@return 結果
		b8 CreateSoundBuffer(ISoundBuffer** ppBuffer, IDecoder* pDecoder);

	private:
		kgl::TKGLPtr<CXAudio2Device> m_pDevice;	//!< オーディオデバイス
	};
}

#endif	// __XAUDIO2_SOUND_BUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
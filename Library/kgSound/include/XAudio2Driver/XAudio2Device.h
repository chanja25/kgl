//----------------------------------------------------------------------
//!
//!	@file	XAudio2Device.h
//!	@brief	XAudio2
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__XAUDIO2_DEVICE_H__
#define	__XAUDIO2_DEVICE_H__

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
//	XAudio2のヘルパー関数使用用
#define	XAUDIO2_HELPER_FUNCTIONS

#if	KG_SOUND_DEBUG
#define	VerifyResult(x)	kgAssert(SUCCEEDED(x), #x)
#else	// ~#if KG_SOUND_DEBUG
#define	VerifyResult(x)	(x)
#endif	// ~#if KG_SOUND_DEBUG

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgSound.h"
#include <xaudio2.h>

namespace kgSound
{
	//======================================================================
	//	XAudio2用デバイス
	//======================================================================
	class CXAudio2Device
		: public ISoundDevice
	{
	public:
		//!	コンストラクタ
		CXAudio2Device(void);
		//!	デストラクタ
		~CXAudio2Device(void);

		//!	初期化
		//!	@return 結果
		b8 Initialize(void);
		//!	終了処理
		void Finalize(void);

	public:
		//!	マスターボリューム設定
		//!	@param fVolume [in] マスターボリューム
		void SetVolume(f32 fVolume);
		//!	マスターボリューム取得
		//!	@return マスターボリューム
		f32 GetVolume(void);

	public:
		//!	オーディオデバイスの取得
		//!	@return オーディオデバイス
		INLINE IXAudio2* GetAudioDevice(void) { return m_pXAudio2; }
		//!	マスターボイスの取得
		//!	@return マスターボイス
		INLINE IXAudio2MasteringVoice* GetMasteringVoice(void) { return m_pMasteringVoice; }

	private:
		kgl::TKGLPtr<IXAudio2>	m_pXAudio2;			//!< XAudio2
		IXAudio2MasteringVoice*		m_pMasteringVoice;	//!< マスターボイス
	};
}

#endif	// __XAUDIO2_DEVICE_H__
//======================================================================
//	END OF FILE
//======================================================================
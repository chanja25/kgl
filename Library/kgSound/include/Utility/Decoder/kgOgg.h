//----------------------------------------------------------------------
//!
//!	@file	kgOgg.h
//!	@brief	Ogg関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_OGG_H__
#define	__KG_OGG_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgSound.h"
#include "../kgSoundBuffer.h"
#include "../kgDecoder.h"
#include "ogg/vorbisfile.h"

namespace kgSound
{
	using namespace kgl;
	//======================================================================
	//	Waveデコードクラス
	//======================================================================
	class COggDecoder
		: public IDecoder
	{
	public:
		//!	コンストラクタ
		COggDecoder(void);
		//!	デストラクタ
		~COggDecoder(void);

	public:
		//!	生成
		//!	@param pFile		[in] ファイルハンドル
		//!	@param usCategory	[in] カテゴリー
		//!	@param usMultiPlay	[in] 最大同時再生数
		//!	@return 結果
		b8 Create(File::IFile* pFile, u16 usCategory, u16 usMultiPlay);
		//!	バッファの読み込み
		//!	@param pBuffer	[out] バッファの格納先
		//!	@param uiSize	[in] 読み込むサイズ
		//!	@return 読み込んだサイズ
		u32	Read(void* pBuffer, u32 uiSize);
		//!	バッファ位置のセット
		//!	@param iMove		[in] 移動量
		//!	@param iPosition	[in] 移動開始位置(ESeekPoint参照)
		//!	@return 移動後の位置
		u32 Seek(s32 iMove, s32 iPosition);
		//!	現在のバッファ位置の取得
		//!	@return 現在の位置
		u32	Tell(void);

	public:
		//!	現在位置までの再生時間を取得
		//!	@return 現在位置までの再生時間
		f32 CurrentTime(void);

	public:
		//!	データタイプの取得
		//!	@return データタイプ
		u32 Type(void);

	private:
		OggVorbis_File	m_OggFile;	//!< Oggファイル管理
	};
}


#endif	// __KG_OGG_H__
//======================================================================
//	END OF FILE
//======================================================================
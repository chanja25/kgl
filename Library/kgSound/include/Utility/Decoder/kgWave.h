//----------------------------------------------------------------------
//!
//!	@file	kgWave.h
//!	@brief	Wave関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_WAVE_H__
#define	__KG_WAVE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgSound.h"
#include "../kgSoundBuffer.h"
#include "../kgDecoder.h"

namespace kgSound
{
	using namespace kgl;
	//======================================================================
	//	Waveデコードクラス
	//======================================================================
	class CWaveDecoder
		: public IDecoder
	{
	public:
		//!	コンストラクタ
		CWaveDecoder(void)
			: m_uiOffset(0)
		{}
		//!	デストラクタ
		~CWaveDecoder(void)
		{
			if( m_pFile )
			{
				m_pFile->Close();
				m_pFile = null;
			}
		}

	public:
		//!	生成
		//!	@param pFile		[in] ファイルハンドル
		//!	@param usCategory	[in] カテゴリー
		//!	@param usMultiPlay	[in] 最大同時再生数
		//!	@return 結果
		b8 Create(File::IFile* pFile, u16 usCategory, u16 usMultiPlay);
		//!	バッファの読み込み
		//!	@param pBuffer	[out] バッファの格納先
		//!	@param uiSize	[in] 読み込むサイズ
		//!	@return 読み込んだサイズ
		u32	Read(void* pBuffer, u32 uiSize);
		//!	バッファ位置のセット
		//!	@param iMove		[in] 移動量
		//!	@param iPosition	[in] 移動開始位置(ESeekPoint参照)
		//!	@return 移動後の位置
		u32 Seek(s32 iMove, s32 iPosition);
		//!	現在のバッファ位置の取得
		//!	@return 現在の位置
		u32	Tell(void);

	public:
		//!	現在位置までの再生時間を取得
		//!	@return 現在位置までの再生時間
		f32 CurrentTime(void);

	public:
		//!	データタイプの取得
		//!	@return データタイプ
		u32 Type(void);

	private:
		TKGLPtr<File::IFile>	m_pFile;	//!< ファイル管理
		u32						m_uiOffset;	//!< バッファへのオフセット位置
	};

	//======================================================================
	//	Waveデコードクラス(オンメモリ)
	//======================================================================
	class CWaveOnMemoryDecoder
		: public IDecoder
	{
	public:
		//!	コンストラクタ
		CWaveOnMemoryDecoder(void)
			: m_pBuffer(null)
			, m_uiOffset(0)
		{}

	public:
		//!	生成
		//!	@param pData		[in] データ
		//!	@param usCategory	[in] カテゴリー
		//!	@param usMultiPlay	[in] 最大同時再生数
		//!	@return 結果
		b8 Create(const void* pData, u16 usCategory, u16 usMultiPlay);

		//!	バッファの読み込み
		//!	@param pBuffer	[out] バッファの格納先
		//!	@param uiSize	[in] 読み込むサイズ
		//!	@return 読み込んだサイズ
		u32	Read(void* pBuffer, u32 uiSize);
		//!	バッファ位置のセット
		//!	@param iMove		[in] 移動量
		//!	@param iPosition	[in] 移動開始位置(ESeekPoint参照)
		//!	@return 移動後の位置
		u32 Seek(s32 iMove, s32 iPosition);
		//!	現在のバッファ位置の取得
		//!	@return 現在の位置
		u32	Tell(void);

	public:
		//!	現在位置までの再生時間を取得
		//!	@return 現在位置までの再生時間
		f32 CurrentTime(void);

	public:
		//!	データタイプの取得
		//!	@return データタイプ
		u32 Type(void);

	private:
		const u8*	m_pBuffer;	//!< バッファの先頭アドレス
		u32			m_uiOffset;	//!< 現在のバッファ位置
	};
}


#endif	// __KG_WAVE_H__
//======================================================================
//	END OF FILE
//======================================================================
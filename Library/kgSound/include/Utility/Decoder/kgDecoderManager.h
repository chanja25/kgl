//----------------------------------------------------------------------
//!
//!	@file	kgDecoderManager.h
//!	@brief	デコーダー管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_DECODER_MANAGER_H__
#define	__KG_DECODER_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgSound.h"
#include "../kgSoundBuffer.h"
#include "../kgDecoder.h"

namespace kgSound
{
	//======================================================================
	//	デコーダー管理クラス
	//======================================================================
	class CDecoderManager
		: public IDecoderManager
	{
	public:
		//!	デコーダー生成
		//!	@param ppDecoder	[out] デコーダー
		//!	@param pData		[in]  データ
		//!	@param usCategory	[in]  カテゴリー
		//!	@param usMultiPlay	[in]  最大同時再生数
		//!	@return 結果
		b8 CreateDecoderFromWave(IDecoder** ppDecoder, const void* pData, u16 usCategory, u16 usMultiPlay);
		//!	デコーダー生成
		//!	@param ppDecoder	[out] デコーダー
		//!	@param pFile		[in]  ファイルハンドル
		//!	@param usCategory	[in]  カテゴリー
		//!	@param usMultiPlay	[in]  最大同時再生数
		//!	@return 結果
		b8 CreateDecoderFromWave(IDecoder** ppDecoder, File::IFile* pFile, u16 usCategory, u16 usMultiPlay);
		//!	デコーダー生成
		//!	@param ppDecoder	[out] デコーダー
		//!	@param pFile		[in]  ファイルハンドル
		//!	@param usCategory	[in]  カテゴリー
		//!	@param usMultiPlay	[in]  最大同時再生数
		//!	@return 結果
		b8 CreateDecoderFromOgg(IDecoder** ppDecoder, File::IFile* pFile, u16 usCategory, u16 usMultiPlay);
	};
}


#endif	// __KG_DECODER_MANAGER_H__
//======================================================================
//	END OF FILE
//======================================================================
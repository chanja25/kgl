//----------------------------------------------------------------------
//!
//!	@file	kgDecoder.h
//!	@brief	デコード関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_DECODER_H__
#define	__KG_DECODER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgSound.h"
#include "kgSoundBuffer.h"

namespace kgSound
{
	using namespace kgl;

	//!	データタイプ
	namespace EType
	{
		const u32 Wave		= 0;	//!< Wave
		const u32 Ogg		= 1;	//!< Ogg
	}
	//!	シークの基準位置
	namespace ESeekPoint
	{
		const s32 Begin   = 0;	//!< 先頭
		const s32 Current = 1;	//!< 現在位置
		const s32 End	  = 2;	//!< 終端
	}

	//======================================================================
	//	デコーダーインターフェース
	//======================================================================
	interface IDecoder
		: public kgl::IKGLBase
	{
	public:
		//!	バッファの読み込み
		//!	@param pBuffer	[out] バッファの格納先
		//!	@param uiSize	[in] 読み込むサイズ
		//!	@return 読み込んだサイズ
		virtual u32	Read(void* pBuffer, u32 uiSize) = 0;
		//!	バッファ位置のセット
		//!	@param iMove		[in] 移動量
		//!	@param iPosition	[in] 移動開始位置(ESeekPoint参照)
		//!	@return 移動後の位置
		virtual u32 Seek(s32 iMove, s32 iPosition = ESeekPoint::Begin) = 0;
		//!	現在のバッファ位置の取得
		//!	@return 現在の位置
		virtual u32	Tell(void) = 0;

	public:
		//!	現在位置までの再生時間を取得
		//!	@return 現在位置までの再生時間
		virtual f32 CurrentTime(void) = 0;

	public:
		//!	データタイプの取得
		//!	@return データタイプ
		virtual u32 Type(void) = 0;
		//!	Wave情報の取得
		//!	@return Wave情報
		FORCEINLINE const WaveInfo& GetWaveInfo(void) { return m_WaveInfo; }

	protected:
		WaveInfo	m_WaveInfo;	//!< Wave情報
	};
	//======================================================================
	//	デコーダー管理インターフェース
	//======================================================================
	interface IDecoderManager
		: public kgl::IKGLBase
	{
	public:
		//!	デコーダー生成
		//!	@param ppDecoder	[out] デコーダー
		//!	@param pData		[in]  データ
		//!	@param usCategory	[in]  カテゴリー
		//!	@param usMultiPlay	[in]  最大同時再生数
		//!	@return 結果
		virtual b8 CreateDecoderFromWave(IDecoder** ppDecoder, const void* pData, u16 usCategory = ECategory::SE, u16 usMultiPlay = 1) = 0;
		//!	デコーダー生成
		//!	@param ppDecoder	[out] デコーダー
		//!	@param pFile		[in]  ファイルハンドル
		//!	@param usCategory	[in]  カテゴリー
		//!	@param usMultiPlay	[in]  最大同時再生数
		//!	@return 結果
		virtual b8 CreateDecoderFromWave(IDecoder** ppDecoder, File::IFile* pFile, u16 usCategory = ECategory::SE, u16 usMultiPlay = 1) = 0;
		//!	デコーダー生成
		//!	@param ppDecoder	[out] デコーダー
		//!	@param pFile		[in]  ファイルハンドル
		//!	@param usCategory	[in]  カテゴリー
		//!	@param usMultiPlay	[in]  最大同時再生数
		//!	@return 結果
		virtual b8 CreateDecoderFromOgg(IDecoder** ppDecoder, File::IFile* pFile, u16 usCategory = ECategory::SE, u16 usMultiPlay = 1) = 0;
	};
}


#endif	// __KG_DECODER_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgSoundUtility.h
//!	@brief	サウンドユーティリティ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_SOUND_UTILITY_H__
#define	__KG_SOUND_UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgSound.h"


namespace kgSound
{
	const f32 FreqencyRaitoMin	= (1.0f/1024.0f);
	const f32 FreqencyRaitoMax	= (4.0f);

	//!	サウンド
	namespace ECategory
	{
		const u16 SE		= 0;	//!< SE
		const u16 VOICE		= 1;	//!< VOICE
		const u16 BGM		= 2;	//!< BGM
	}
	//!	フォーマット
	namespace EFormat
	{
		const u16 PCM		= 0;	//!< PCM
	}
	//!	状態
	namespace EState
	{
		const u8 Playing	= 0;	//!< 再生中
		const u8 Stoped		= 1;	//!< 停止中
		const u8 Paused		= 2;	//!< 一時停止中
	}

	//!	WAVEフォーマット
	struct WaveFormat
	{
		u16	FormatType;			//!< フォーマットタイプ
		u16	Channels;			//!< チャンネル数
		u32	SamplesPerSecond;	//!< サンプルレート
		u32	AvgBytesPerSecond;	//!< 平均転送レート
		u16	BlockAlign;			//!< データのブロックサイズ
		u16	BitsPerSample;		//!< ビットレート
	};

	//!	WAVE情報
	struct WaveInfo
	{
		u16			Category;		//!< カテゴリー
		u16			MultiPlay;		//!< 最大同時再生数(ストリーム再生時は必ず1が入る)
		u32			BufferSize;		//!< バッファサイズ
		u32			TotalSample;	//!< 総サンプル数
		f32			PlaySecond;		//!< 再生時間(ストリーム再生時はバッファの長さと異なった値になる)
		WaveFormat	Format;			//!< フォーマット
	};
}


#include "kgSoundBuffer.h"
#include "kgDecoder.h"

#endif	// __KG_SOUND_UTILITY_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgSoundBuffer.h
//!	@brief	サウンドバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_SOUND_BUFFER_H__
#define	__KG_SOUND_BUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgSound.h"
#include "../../kgl/include/File/kgl.File.h"

namespace kgSound
{
	interface IDecoder;

	//======================================================================
	//	サウンドバッファインターフェース
	//======================================================================
	interface ISoundBuffer
		: public kgl::IKGLBase
	{
	public:
		//!	再生
		//!	@return 結果
		virtual b8 Play(b8 bLoop = false, f32 fOffset = 0) = 0;
		//!	停止
		virtual void Stop(void) = 0;
		//!	一時停止
		virtual void Pause(void) = 0;
		//!	一時停止の解除
		virtual void Resume(void) = 0;

		//!	状態の取得
		//!	@return 状態(EState参照)
		virtual u8 GetState(void) = 0;

		//!	ボリューム設定
		//!	@param fVolume [in] ボリューム
		virtual void SetVolume(f32 fVolume) = 0;
		//!	ボリューム取得
		//!	@return ボリューム
		virtual f32 GetVolume(void) = 0;
		//!	ボリューム設定
		//!	@param uiChannels	[in] チャンネル
		//!	@param pVolume		[in] ボリューム
		virtual void SetChannelVolumes(u32 uiChannels, const f32* pVolume) = 0;
		//!	ボリューム取得
		//!	@param uiChannels	[in]  チャンネル
		//!	@param pVolume		[out] ボリューム
		virtual void GetChannelVolumes(u32 uiChannels, f32* pVolume) = 0;

		//!	周波数調整比の設定(FreqencyRaitoMin, FreqencyRaitoMax)
		//!	@param fRate [in] レート(0.5fで1オクターブ下がる。2.0fで1オクターブ上がる)
		virtual void SetFrequencyRatio(f32 fRate) = 0;
		//!	周波数調整比の取得
		//!	@return 周波数調整比
		virtual f32 GetFrequencyRatio(void) = 0;

		//!	現在の再生時間を取得
		//!	@return 現在の再生時間
		virtual f32 CurrentTime(void) = 0;
		//!	現在の再生サンプル数を取得
		//!	@return 現在の再生サンプル数
		virtual u32 CurrentSamples(void) = 0;

		//!	ループフラグ取得
		//!	@return ループフラグ
		virtual b8 IsLoop(void) = 0;

	public:
		//!	バッファのロック
		//!	@param uiOffset	[in]  オフセット位置
		//!	@param uiSize	[in]  サイズ
		//!	@param ppBuf1	[out] バッファ1
		//!	@param pSize1	[out] サイズ1
		//!	@param ppBuf2	[out] バッファ2(オフセット位置+サイズがバッファの末尾を超えている場合は回り込んだバッファが設定される)
		//!	@param pSize2	[out] サイズ2(オフセット位置+サイズがバッファの末尾を超えている場合は回り込んだバッファサイズが設定される)
		virtual void Lock(u32 uiOffset, u32 uiSize, void** ppBuf1, u32* pSize1, void** ppBuf2, u32* pSize2) = 0;
		//!	バッファのアンロック
		virtual void Unlock(void) = 0;

	public:
		//!	サウンドバッファの取得
		//!	@return サウンドバッファ
		virtual void* GetSoundBuffer(void) = 0;
		//!	Wave情報の取得
		//!	@return Wave情報
		FORCEINLINE const WaveInfo& GetWaveInfo(void) { return m_WaveInfo; }

	protected:
		WaveInfo	m_WaveInfo;	//!< Waveフォーマット
	};

	//======================================================================
	//	サウンドバッファ管理インターフェース
	//======================================================================
	interface ISoundBufferManager
		: public kgl::IKGLBase
	{
	public:
		//!	サウンドバッファ生成
		//!	@param ppBuffer	[out] サウンドバッファ
		//!	@param Info		[in] サウンドバッファ情報
		//!	@param pData	[in] データ
		//!	@return 結果
		virtual b8 CreateSoundBuffer(ISoundBuffer** ppBuffer, const WaveInfo& Info, const void* pData = null) = 0;
		//!	サウンドバッファ生成
		//!	@param ppBuffer	[out] テクスチャー
		//!	@param pDecoder	[in]  デコーダー
		//!	@return 結果
		virtual b8 CreateSoundBuffer(ISoundBuffer** ppBuffer, IDecoder* pDecoder) = 0;
	};
}

#endif	// __KG_SOUND_BUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
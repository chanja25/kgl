//----------------------------------------------------------------------
//!
//!	@file	kgOgg.h
//!	@brief	Ogg関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgSound.h"
#include "Utility/kgSoundBuffer.h"
#include "Utility/Decoder/kgOgg.h"

namespace kgSound
{
	namespace Ogg
	{
		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイル読み込み)
		//!	@param pPtr [out] 読み込んだファイルを入れるポインタ
		//!	@param dwSize [in] データのサイズ
		//!	@param dwNmemb [in] データの数
		//!	@param pDataSource [in] ハンドル
		//!	@retva ファイルサイズ
		//---------------------------------------------------------------------------
		size_t CallbackRead(void* pPtr, size_t size, size_t nmemb, void* pDataSource)
		{
			File::IFile* pFile = (File::IFile*)pDataSource;
			return (size_t)pFile->Read(pPtr, (u32)(size * nmemb));
		}
		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイルシーク)
		//!	@param pDataSource [in] ハンドル
		//!	@param iOffset [in] データの位置
		//!	@param iWhence) [in] シークするモード
		//!	@retva 結果
		//---------------------------------------------------------------------------
		s32 CallbackSeek(void* pDataSource, s64 i64Offset, s32 iWhence)
		{
			File::IFile* pFile = (File::IFile*)pDataSource;

			u32 uiResult = 0xffffffff;
			switch( iWhence )
			{
			case SEEK_SET: uiResult = pFile->Seek((s32)i64Offset, FILE_BEGIN);		break;
			case SEEK_CUR: uiResult = pFile->Seek((s32)i64Offset, FILE_CURRENT);	break;
			case SEEK_END: uiResult = pFile->Seek((s32)i64Offset, FILE_END);		break;
			}

			return (uiResult == 0xffffffff)? -1 : 0;
		}
		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイルクローズ)
		//!	@param pDataSource [in] ファイルのハンドル
		//!	@return 結果
		//---------------------------------------------------------------------------
		s32 CallbackClose(void* pDataSource)
		{
			File::IFile* pFile = (File::IFile*)pDataSource;
			pFile->Close();
			SafeRelease(pFile);

			return EOF;
		}
		//---------------------------------------------------------------------------
		//!	Vorbisfileコールバック(ファイルの現在位置取得)
		//!	@param pDataSource [in] ファイルのハンドル
		//!	@return 現在位置
		//---------------------------------------------------------------------------
		long CallbackTell(void* pDataSource)
		{
			File::IFile* pFile = (File::IFile*)pDataSource;
			return (long)pFile->Tell();
		}
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	COggDecoder::COggDecoder(void)
	{
		ZeroMemory(&m_OggFile, sizeof(m_OggFile));
	}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	COggDecoder::~COggDecoder(void)
	{
		if( m_OggFile.datasource )
		{
			ov_clear(&m_OggFile);
		}
	}

	//----------------------------------------------------------------------
	//	生成
	//----------------------------------------------------------------------
	b8 COggDecoder::Create(File::IFile* pFile, u16 usCategory, u16 usMultiPlay)
	{
		OggVorbis_File vf;

		ov_callbacks oggCallbacks =	{ Ogg::CallbackRead, Ogg::CallbackSeek, Ogg::CallbackClose, Ogg::CallbackTell };
		if( ov_open_callbacks(pFile, &vf, null, 0, oggCallbacks) )
		{
			pFile->Close();
			return false;
		}
		pFile->AddRef();

		const vorbis_info* info = ov_info(&vf, -1);
		if( info == null )
		{
			ov_clear(&vf);
			return false;
		}

		const u16 BitsPerSample = 16;

		u32 uiSize = (u32)(ov_pcm_total(&vf, -1) * info->channels * (BitsPerSample / 8));

		WaveInfo Info = {0};
		Info.Format.FormatType			= EFormat::PCM;
		Info.Format.Channels			= (u16)info->channels;
		Info.Format.SamplesPerSecond	= info->rate;
		Info.Format.BitsPerSample		= BitsPerSample;
		Info.Format.BlockAlign			= Info.Format.Channels * BitsPerSample / 8;
		Info.Format.AvgBytesPerSecond	= info->rate * Info.Format.BlockAlign;
		Info.Category					= usCategory;
		Info.MultiPlay					= usMultiPlay;
		Info.BufferSize					= uiSize;
		Info.PlaySecond					= (f32)ov_time_total(&vf, -1);

		m_WaveInfo = Info;
		m_OggFile = vf;

		return true;
	}

	//----------------------------------------------------------------------
	//	バッファの読み込み
	//----------------------------------------------------------------------
	u32	COggDecoder::Read(void* pBuffer, u32 uiSize)
	{
		u32 uiRead, uiActualRead = 0;
		c8* pPtr = (c8*)pBuffer;

		const u32 ReadSize = 4096;

		do
		{
			uiRead = ov_read(&m_OggFile, pPtr + uiActualRead, Min(uiSize - uiActualRead, ReadSize), 0, 2, 1, null);
			uiActualRead += uiRead;
		}
		while( uiSize > uiActualRead && uiRead != 0 );

		return uiActualRead;
	}

	//----------------------------------------------------------------------
	//	バッファ位置のセット
	//----------------------------------------------------------------------
	u32 COggDecoder::Seek(s32 iMove, s32 iPosition)
	{
		switch( iPosition )
		{
		case ESeekPoint::Current:	iMove += Tell();				break;
		case ESeekPoint::End:		iMove += m_WaveInfo.BufferSize;	break;
		}
		return (u32)ov_pcm_seek(&m_OggFile, iMove);
	}

	//----------------------------------------------------------------------
	//	現在のバッファ位置の取得
	//----------------------------------------------------------------------
	u32	COggDecoder::Tell(void)
	{
		return (u32)ov_pcm_tell(&m_OggFile);
	}

	//----------------------------------------------------------------------
	//	現在位置までの再生時間を取得
	//----------------------------------------------------------------------
	f32 COggDecoder::CurrentTime(void)
	{
		return (f32)ov_time_tell(&m_OggFile);
	}

	//----------------------------------------------------------------------
	//	データタイプの取得
	//----------------------------------------------------------------------
	u32 COggDecoder::Type(void)
	{
		return EType::Ogg;
	}
}


//======================================================================
//	END OF FILE
//======================================================================
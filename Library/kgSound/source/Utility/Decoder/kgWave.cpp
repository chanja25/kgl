//----------------------------------------------------------------------
//!
//!	@file	kgWave.h
//!	@brief	Wave関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgSound.h"
#include "Utility/kgSoundBuffer.h"
#include "Utility/Decoder/kgWave.h"

namespace kgSound
{
	//----------------------------------------------------------------------
	//	生成
	//----------------------------------------------------------------------
	b8 CWaveDecoder::Create(File::IFile* pFile, u16 usCategory, u16 usMultiPlay)
	{
		u32 uiForcc;
		//	"RIFF"
		pFile->Read(&uiForcc, sizeof(uiForcc));
		if( uiForcc != FOURCC('R', 'I', 'F', 'F') )
		{
			return false;
		}
		u32 uiSize;
		//	ファイルサイズ
		pFile->Read(&uiSize, sizeof(uiSize));

		//	"WAVE"
		pFile->Read(&uiForcc, sizeof(uiForcc));
		if( uiForcc != FOURCC('W', 'A', 'V', 'E') )
		{
			return false;
		}
		//	"fmt "
		pFile->Read(&uiForcc, sizeof(uiForcc));
		if( uiForcc != FOURCC('f', 'm', 't', ' ') )
		{
			return false;
		}
		//	フォーマットの構造体サイズ
		pFile->Read(&uiSize, sizeof(uiSize));
		WAVEFORMATEX wfx;
		ZeroMemory(&wfx, sizeof(wfx));
		pFile->Read(&wfx, uiSize);

		//	"fact"
		pFile->Read(&uiForcc, sizeof(uiForcc));
		if( uiForcc == FOURCC('f', 'a', 'c', 't') )
		{
			pFile->Read(&uiSize, sizeof(uiSize));
			pFile->Seek(uiSize, File::EFilePointer::Current);

			pFile->Read(&uiForcc, sizeof(uiForcc));
		}
		//	"data"
		if( uiForcc != FOURCC('d', 'a', 't', 'a') )
		{
			return false;
		}
		pFile->Read(&uiSize, sizeof(uiSize));

		WaveInfo Info = {0};
		Info.Format.FormatType			= EFormat::PCM;
		Info.Format.Channels			= wfx.nChannels;
		Info.Format.SamplesPerSecond	= wfx.nSamplesPerSec;
		Info.Format.AvgBytesPerSecond	= wfx.nAvgBytesPerSec;
		Info.Format.BlockAlign			= wfx.nBlockAlign;
		Info.Format.BitsPerSample		= wfx.wBitsPerSample;
		Info.Category					= usCategory;
		Info.MultiPlay					= usMultiPlay;
		Info.BufferSize					= uiSize;
		Info.PlaySecond					= (f32)Info.BufferSize / (f32)Info.Format.AvgBytesPerSecond;

		m_WaveInfo	= Info;
		m_pFile		= pFile;
		m_uiOffset	= pFile->Tell();

		return true;
	}

	//----------------------------------------------------------------------
	//	バッファの読み込み
	//----------------------------------------------------------------------
	u32	CWaveDecoder::Read(void* pBuffer, u32 uiSize)
	{
		return m_pFile->Read(pBuffer, uiSize);
	}

	//----------------------------------------------------------------------
	//	バッファ位置のセット
	//----------------------------------------------------------------------
	u32 CWaveDecoder::Seek(s32 iMove, s32 iPosition)
	{
		const s32 FilePointer[] =
		{
			File::EFilePointer::Begin,
			File::EFilePointer::Current,
			File::EFilePointer::End,
		};
		if( iPosition == ESeekPoint::Begin )
		{
			iMove += m_uiOffset;
		}

		return m_pFile->Seek(iMove, FilePointer[iPosition]);
	}

	//----------------------------------------------------------------------
	//	現在のバッファ位置の取得
	//----------------------------------------------------------------------
	u32	CWaveDecoder::Tell(void)
	{
		return m_pFile->Tell() - m_uiOffset;
	}

	//----------------------------------------------------------------------
	//	現在位置までの再生時間を取得
	//----------------------------------------------------------------------
	f32 CWaveDecoder::CurrentTime(void)
	{
		return (f32)Tell() / (f32)m_WaveInfo.Format.AvgBytesPerSecond;
	}

	//----------------------------------------------------------------------
	//	データタイプの取得
	//----------------------------------------------------------------------
	u32 CWaveDecoder::Type(void)
	{
		return EType::Wave;
	}


	//----------------------------------------------------------------------
	//	データの読み込み
	//----------------------------------------------------------------------
	const u8* ReadBuffer(void* pDst, const u8* pSrc, u32 uiSize)
	{
		kgMemcpy(pDst, pSrc, uiSize);
		return pSrc + uiSize;
	}

	//----------------------------------------------------------------------
	//	生成
	//----------------------------------------------------------------------
	b8 CWaveOnMemoryDecoder::Create(const void* pData, u16 usCategory, u16 usMultiPlay)
	{
		const u8* pBuffer = (const u8*)pData;

		u32 uiForcc;
		//	"RIFF"
		pBuffer = ReadBuffer(&uiForcc, pBuffer, sizeof(uiForcc));
		if( uiForcc != FOURCC('R', 'I', 'F', 'F') )
		{
			return false;
		}

		u32 uiSize;
		//	ファイルサイズ
		pBuffer = ReadBuffer(&uiSize, pBuffer, sizeof(uiSize));

		//	"WAVE"
		pBuffer = ReadBuffer(&uiForcc, pBuffer, sizeof(uiForcc));
		if( uiForcc != FOURCC('W', 'A', 'V', 'E') )
		{
			return false;
		}
		//	"fmt "
		pBuffer = ReadBuffer(&uiForcc, pBuffer, sizeof(uiForcc));
		if( uiForcc != FOURCC('f', 'm', 't', ' ') )
		{
			return false;
		}
		//	フォーマットの構造体サイズ
		pBuffer = ReadBuffer(&uiSize, pBuffer, sizeof(uiSize));
		WAVEFORMATEX wfx;
		ZeroMemory(&wfx, sizeof(wfx));
		pBuffer = ReadBuffer(&wfx, pBuffer, uiSize);

		//	"fact"
		pBuffer = ReadBuffer(&uiForcc, pBuffer, sizeof(uiForcc));
		if( uiForcc == FOURCC('f', 'a', 'c', 't') )
		{
			pBuffer = ReadBuffer(&uiSize, pBuffer, sizeof(uiSize));
			pBuffer += uiSize;

			pBuffer = ReadBuffer(&uiForcc, pBuffer, sizeof(uiForcc));
		}
		//	"data"
		if( uiForcc != FOURCC('d', 'a', 't', 'a') )
		{
			return false;
		}
		pBuffer = ReadBuffer(&uiSize, pBuffer, sizeof(uiSize));

		WaveInfo Info = {0};
		Info.Format.FormatType			= EFormat::PCM;
		Info.Format.Channels			= wfx.nChannels;
		Info.Format.SamplesPerSecond	= wfx.nSamplesPerSec;
		Info.Format.AvgBytesPerSecond	= wfx.nAvgBytesPerSec;
		Info.Format.BlockAlign			= wfx.nBlockAlign;
		Info.Format.BitsPerSample		= wfx.wBitsPerSample;
		Info.Category					= usCategory;
		Info.MultiPlay					= usMultiPlay;
		Info.BufferSize					= uiSize;
		Info.PlaySecond					= (f32)uiSize / (f32)wfx.nAvgBytesPerSec;
		Info.TotalSample				= uiSize / Info.Format.BlockAlign;

		m_WaveInfo	= Info;
		m_pBuffer	= pBuffer;
		m_uiOffset	= 0;

		return true;
	}

	//----------------------------------------------------------------------
	//	バッファの読み込み
	//----------------------------------------------------------------------
	u32	CWaveOnMemoryDecoder::Read(void* pBuffer, u32 uiSize)
	{
		if( m_uiOffset + uiSize > m_WaveInfo.BufferSize )
		{
			uiSize = (m_uiOffset + uiSize) - m_WaveInfo.BufferSize;
		}

		ReadBuffer(pBuffer, m_pBuffer + m_uiOffset, uiSize);
		m_uiOffset += uiSize;

		return uiSize;
	}

	//----------------------------------------------------------------------
	//	バッファ位置のセット
	//----------------------------------------------------------------------
	u32 CWaveOnMemoryDecoder::Seek(s32 iMove, s32 iPosition)
	{
		switch( iPosition )
		{
		case ESeekPoint::Begin:	m_uiOffset = 0;						break;
		case ESeekPoint::End:	m_uiOffset = m_WaveInfo.BufferSize;	break;
		}

		return m_uiOffset += iMove;
	}

	//----------------------------------------------------------------------
	//	現在のバッファ位置の取得
	//----------------------------------------------------------------------
	u32	CWaveOnMemoryDecoder::Tell(void)
	{
		return m_uiOffset;
	}

	//----------------------------------------------------------------------
	//	現在位置までの再生時間を取得
	//----------------------------------------------------------------------
	f32 CWaveOnMemoryDecoder::CurrentTime(void)
	{
		return (f32)Tell() / (f32)m_WaveInfo.Format.AvgBytesPerSecond;
	}

	//----------------------------------------------------------------------
	//	データタイプの取得
	//----------------------------------------------------------------------
	u32 CWaveOnMemoryDecoder::Type(void)
	{
		return EType::Wave;
	}
}


//======================================================================
//	END OF FILE
//======================================================================
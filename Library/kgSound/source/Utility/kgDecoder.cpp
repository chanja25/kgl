//----------------------------------------------------------------------
//!
//!	@file	kgOgg.h
//!	@brief	Ogg関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgSound.h"
#include "Utility/kgSoundBuffer.h"
#include "Utility/kgDecoder.h"
#include "Utility/Decoder/kgDecoderManager.h"
#include "Utility/Decoder/kgWave.h"
#include "Utility/Decoder/kgOgg.h"

namespace kgSound
{
	//----------------------------------------------------------------------
	//	デコーダー生成
	//----------------------------------------------------------------------
	b8 CDecoderManager::CreateDecoderFromWave(IDecoder** ppDecoder, const void* pData, u16 usCategory, u16 usMultiPlay)
	{
		TKGLPtr<CWaveOnMemoryDecoder> pDecoder = new CWaveOnMemoryDecoder;

		if( pDecoder->Create(pData, usCategory, usMultiPlay) )
		{
			pDecoder->AddRef();
			*ppDecoder = pDecoder;
			return true;
		}
		return false;
	}
	//----------------------------------------------------------------------
	//	デコーダー生成
	//----------------------------------------------------------------------
	b8 CDecoderManager::CreateDecoderFromWave(IDecoder** ppDecoder, File::IFile* pFile, u16 usCategory, u16 usMultiPlay)
	{
		TKGLPtr<CWaveDecoder> pDecoder = new CWaveDecoder;

		if( pDecoder->Create(pFile, usCategory, usMultiPlay) )
		{
			pDecoder->AddRef();
			*ppDecoder = pDecoder;
			return true;
		}
		return false;
	}
	//----------------------------------------------------------------------
	//	デコーダー生成
	//----------------------------------------------------------------------
	b8 CDecoderManager::CreateDecoderFromOgg(IDecoder** ppDecoder, File::IFile* pFile, u16 usCategory, u16 usMultiPlay)
	{
		TKGLPtr<COggDecoder> pDecoder = new COggDecoder;

		if( pDecoder->Create(pFile, usCategory, usMultiPlay) )
		{
			pDecoder->AddRef();
			*ppDecoder = pDecoder;
			return true;
		}
		return false;
	}
}


//======================================================================
//	END OF FILE
//======================================================================
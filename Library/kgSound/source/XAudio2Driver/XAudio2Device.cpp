//----------------------------------------------------------------------
//!
//!	@file	XAudio2Device.cpp
//!	@brief	XAudio2
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgSound.h"

#if	USE_XAUDIO2
/*
#pragma comment(lib, "xaudio2.lib")
#pragma comment(lib, "X3DAudio.lib")
#pragma comment(lib, "xapobase.lib")
#pragma comment(lib, "XAPOFX.lib")
#pragma comment(lib, "dxguid.lib")
*/

#include "XAudio2Driver/XAudio2Device.h"

namespace kgSound
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CXAudio2Device::CXAudio2Device(void)
		: m_pMasteringVoice(null)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CXAudio2Device::~CXAudio2Device(void)
	{
		Finalize();
	}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CXAudio2Device::Initialize(void)
	{
		kgAssert(!m_pXAudio2.IsValid(), "既にCXAudio2Deviceは初期化されています(CXAudio2Device::Initialize)");

		u32 uiFlag = 0;
#if	KG_SOUND_DEBUG
		if( IsDebuggerPresent() )
		{
			uiFlag |= XAUDIO2_DEBUG_ENGINE;
		}
#endif	// ~#if KG_SOUND_DEBUG

		kgl::TKGLPtr<IXAudio2> pXAudio2;
		HRESULT hr;
		//	XAudio2の初期化
		hr = XAudio2Create(pXAudio2.GetReference(), uiFlag, XAUDIO2_DEFAULT_PROCESSOR);
		if( FAILED(hr) )
		{
			ErrorTrace("XAudio2の初期化に失敗しました(CXAudio2Device::Initialize)");
			return false;
		}
		//	マスターボイスの生成
		hr = pXAudio2->CreateMasteringVoice(
			&m_pMasteringVoice,
			XAUDIO2_DEFAULT_CHANNELS,
			XAUDIO2_DEFAULT_SAMPLERATE,
			0,
			0,
			0);
		if( FAILED(hr) )
		{
			ErrorTrace("XAudio2のMasteringVoiceの作成に失敗しました(CXAudio2Device::Initialize)");
			return false;
		}

		m_pXAudio2 = pXAudio2;

		return true;
	}

	//----------------------------------------------------------------------
	//	終了処理
	//----------------------------------------------------------------------
	void CXAudio2Device::Finalize(void)
	{
		if( m_pMasteringVoice )
		{
			m_pMasteringVoice->DestroyVoice();
			m_pMasteringVoice = null;
		}
		m_pXAudio2 = null;
	}
	//----------------------------------------------------------------------
	//	マスターボリューム設定
	//----------------------------------------------------------------------
	void CXAudio2Device::SetVolume(f32 fVolume)
	{
		m_pMasteringVoice->SetVolume(fVolume);
	}
	//----------------------------------------------------------------------
	//	マスターボリューム取得
	//----------------------------------------------------------------------
	f32 CXAudio2Device::GetVolume(void)
	{
		f32 fVolume;
		m_pMasteringVoice->GetVolume(&fVolume);
		return fVolume;
	}
}

#endif	// ~#if USE_XAUDIO2

//======================================================================
//	END OF FILE
//======================================================================
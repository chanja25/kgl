//----------------------------------------------------------------------
//!
//!	@file	XAudio2SoundBuffer.cpp
//!	@brief	サウンドバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgSound.h"
#include "XAudio2Driver/XAudio2SoundBuffer.h"

namespace kgSound
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CXAudio2SoundBuffer::CXAudio2SoundBuffer(void)
		: m_u64StartSample(0)
		, m_u64OffsetSample(0)
	{
		m_Buffer.pVoice		= null;
		m_Buffer.pBuffer	= null;
		m_Buffer.bLock		= false;
		m_Buffer.bLoop		= false;
		m_Buffer.bPause		= false;
	}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CXAudio2SoundBuffer::~CXAudio2SoundBuffer(void)
	{
		if( m_Buffer.pVoice )
		{
			m_Buffer.pVoice->DestroyVoice();
			m_Buffer.pVoice		= null;
			m_Buffer.pBuffer	= null;
			kgAssert(!m_Buffer.bLock, "バッファのロックが解除されていません(CXAudio2SoundBuffer::~CXAudio2SoundBuffer)");
		}
	}
	//----------------------------------------------------------------------
	//	バッファ生成
	//----------------------------------------------------------------------
	b8 CXAudio2SoundBuffer::Create(kgl::TKGLPtr<CXAudio2Device> pDevice, const WaveInfo& Info, const void* pData)
	{
		kgAssert(m_Buffer.pVoice == null, "既に初期化されています(CXAudio2SoundBuffer::Create)");
		if( m_Buffer.pVoice )
		{
			m_Buffer.pVoice->DestroyVoice();
			m_Buffer.pVoice		= null;
			m_Buffer.pBuffer	= null;
		}

		kgl::TSmartPtr<s8> pBuffer = new s8[Info.BufferSize];
		if( pBuffer == null )
		{
			ErrorTrace("サウンドバッファ用メモリ確保に失敗しました(CXAudio2SoundBuffer::Create)");
			ErrorTrace("メモリの残量を確認してください(%d)", Info.BufferSize);
			return false;
		}

		const u16 Format[] =
		{
			WAVE_FORMAT_PCM,
		};

		WAVEFORMATEX wfx;
		wfx.wFormatTag		= Format[Info.Format.FormatType];
		wfx.nChannels		= Info.Format.Channels;
		wfx.nAvgBytesPerSec	= Info.Format.AvgBytesPerSecond;
		wfx.nBlockAlign		= Info.Format.BlockAlign;
		wfx.wBitsPerSample	= Info.Format.BitsPerSample;
		wfx.nSamplesPerSec	= Info.Format.SamplesPerSecond;
		wfx.cbSize			= 0;

		IXAudio2SourceVoice* pVoice;
		HRESULT hr;
		hr = pDevice->GetAudioDevice()->CreateSourceVoice(
				&pVoice,
				&wfx,
				0,
				FreqencyRaitoMax/*,
				XAUDIO2_DEFAULT_CHANNELS,
				XAUDIO2_DEFAULT_SAMPLERATE,
				0,
				0,
				0*/);
		if( FAILED(hr) )
		{
			ErrorTrace("サウンドバッファの生成に失敗しました(CXAudio2SoundBuffer::Create)");
			return false;
		}
		m_Buffer.pVoice		= pVoice;
		m_Buffer.pBuffer	= pBuffer;
		m_WaveInfo			= Info;

		if( pData )
		{
			void* pBuf1;
			void* pBuf2;
			u32	uiBufSize1, uiBufSize2;
			Lock(0, Info.BufferSize, &pBuf1, &uiBufSize1, &pBuf2, &uiBufSize2);

			kgMemcpy(pBuf1, pData, uiBufSize1);

			Unlock();
		}

		return true;
	}
	//----------------------------------------------------------------------
	//	バッファ生成
	//----------------------------------------------------------------------
	b8 CXAudio2SoundBuffer::Create(kgl::TKGLPtr<CXAudio2Device> pDevice, IDecoder* pDecoder)
	{
		if( pDecoder == null )
		{
			ErrorTrace("デコーダーが指定されていません(SoundManager::Create)");
			return false;
		}

		const WaveInfo& Info = pDecoder->GetWaveInfo();

		if( !Create(pDevice, Info, null) )
		{
			return false;
		}

		void* pBuf1;
		void* pBuf2;
		u32	uiBufSize1, uiBufSize2;
		Lock(0, Info.BufferSize, &pBuf1, &uiBufSize1, &pBuf2, &uiBufSize2);

		pDecoder->Read(pBuf1, uiBufSize1);
		if( uiBufSize2 > 0 )
		{
			pDecoder->Read(pBuf2, uiBufSize2);
		}

		Unlock();

		return true;
	}
	//----------------------------------------------------------------------
	//	再生
	//----------------------------------------------------------------------
	b8 CXAudio2SoundBuffer::Play(b8 bLoop, f32 fOffset)
	{
		//if( m_Buffer.uiSize <= uiOffset )
		//{
		//	ErrorTrace("指定されたオフセット位置(%d)がバッファサイズを超えています(CXAudio2SoundBuffer::Play)", uiOffset);
		//	return false;
		//}
		fOffset = Clamp(fOffset, 0.0f, m_WaveInfo.PlaySecond);

        XAUDIO2_BUFFER buffer = {0};
		buffer.PlayBegin	= (u32)((f32)m_WaveInfo.Format.SamplesPerSecond * fOffset);
		//buffer.PlayLength	= 0;
		buffer.AudioBytes	= m_WaveInfo.BufferSize;
		buffer.pAudioData	= m_Buffer.pBuffer;
        buffer.Flags		= XAUDIO2_END_OF_STREAM;

		if( bLoop )
		{
			//buffer.LoopBegin	= 0;
			//buffer.LoopLength	= 0;
			buffer.LoopCount	= XAUDIO2_LOOP_INFINITE;
		}

		if( GetState() == EState::Playing )
		{
			Stop();
		}
		XAUDIO2_VOICE_STATE state;
		m_Buffer.pVoice->GetState(&state);
		m_u64StartSample = state.SamplesPlayed;
		m_u64OffsetSample = buffer.PlayBegin;

		m_Buffer.pVoice->FlushSourceBuffers();
        m_Buffer.pVoice->SubmitSourceBuffer(&buffer);
		m_Buffer.pVoice->Start();

		m_Buffer.bLoop	= bLoop;
		m_Buffer.bPause	= false;
		return true;
	}
	//----------------------------------------------------------------------
	//	停止
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::Stop(void)
	{
		m_Buffer.pVoice->Stop();
		m_Buffer.pVoice->FlushSourceBuffers();
		m_Buffer.bPause	= false;
	}
	//----------------------------------------------------------------------
	//	一時停止
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::Pause(void)
	{
		if( !m_Buffer.bPause )
		{
			m_Buffer.pVoice->Stop();
			m_Buffer.bPause	= true;
		}
	}
	//----------------------------------------------------------------------
	//	一時停止の解除
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::Resume(void)
	{
		if( m_Buffer.bPause )
		{
			m_Buffer.pVoice->Start();
			m_Buffer.bPause	= false;
		}
	}
	//----------------------------------------------------------------------
	//	状態の取得
	//----------------------------------------------------------------------
	u8 CXAudio2SoundBuffer::GetState(void)
	{
		XAUDIO2_VOICE_STATE state;
		m_Buffer.pVoice->GetState(&state);

		u8 ucState = m_Buffer.bPause? EState::Paused: EState::Playing;

		return (state.BuffersQueued > 0)? ucState: EState::Stoped;
	}

	//----------------------------------------------------------------------
	//	ボリューム設定
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::SetVolume(f32 fVolume)
	{
		m_Buffer.pVoice->SetVolume(fVolume);
	}
	//----------------------------------------------------------------------
	//	ボリューム設定
	//----------------------------------------------------------------------
	f32 CXAudio2SoundBuffer::GetVolume(void)
	{
		f32 fVolume;
		m_Buffer.pVoice->GetVolume(&fVolume);
		return fVolume;
	}
	//----------------------------------------------------------------------
	//	ボリューム設定
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::SetChannelVolumes(u32 uiChannels, const f32* pVolume)
	{
		m_Buffer.pVoice->SetChannelVolumes(uiChannels, pVolume);
	}
	//----------------------------------------------------------------------
	//	ボリューム取得
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::GetChannelVolumes(u32 uiChannels, f32* pVolume)
	{
		m_Buffer.pVoice->GetChannelVolumes(uiChannels, pVolume);
	}
	//----------------------------------------------------------------------
	//	周波数調整比の設定
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::SetFrequencyRatio(f32 fRate)
	{
		fRate = kgl::Clamp(fRate, FreqencyRaitoMin, FreqencyRaitoMax);
		m_Buffer.pVoice->SetFrequencyRatio(fRate);
	}
	//----------------------------------------------------------------------
	//	周波数調整比の取得
	//----------------------------------------------------------------------
	f32 CXAudio2SoundBuffer::GetFrequencyRatio(void)
	{
		f32 fRate;
		m_Buffer.pVoice->GetFrequencyRatio(&fRate);
		return fRate;
	}

	//----------------------------------------------------------------------
	//	現在の再生時間を取得
	//----------------------------------------------------------------------
	f32 CXAudio2SoundBuffer::CurrentTime(void)
	{
		XAUDIO2_VOICE_STATE state;
		m_Buffer.pVoice->GetState(&state);
		return ((f32)(state.SamplesPlayed - m_u64StartSample + m_u64OffsetSample) / (f32)m_WaveInfo.Format.SamplesPerSecond);
	}

	//----------------------------------------------------------------------
	//	現在の再生時間を取得
	//----------------------------------------------------------------------
	u32 CXAudio2SoundBuffer::CurrentSamples(void)
	{
		XAUDIO2_VOICE_STATE state;
		m_Buffer.pVoice->GetState(&state);
		return (u32)state.SamplesPlayed;
	}

	//----------------------------------------------------------------------
	//	ループフラグ取得
	//----------------------------------------------------------------------
	b8 CXAudio2SoundBuffer::IsLoop(void)
	{
		return !!m_Buffer.bLoop;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::Lock(u32 uiOffset, u32 uiSize, void** ppBuf1, u32* pSize1, void** ppBuf2, u32* pSize2)
	{
#if	KG_SOUND_DEBUG
		if( m_Buffer.bLock )
		{
			WarningTrace("バッファのロックが解除されていません(CXAudio2SoundBuffer::Lock)");
		}
#endif	// ~#if KG_SOUND_DEBUG
		kgAssert(ppBuf1, "ppBuf1がnullです(CXAudio2SoundBuffer::Lock)");
		kgAssert(pSize1, "pSize1がnullです(CXAudio2SoundBuffer::Lock)");
		kgAssert(ppBuf2, "ppBuf2がnullです(CXAudio2SoundBuffer::Lock)");
		kgAssert(pSize2, "pSize2がnullです(CXAudio2SoundBuffer::Lock)");
		if( m_WaveInfo.BufferSize < uiSize )
		{
			ErrorTrace("Lockするにはバッファが足りません(CXAudio2SoundBuffer::Lock)");
			return;
		}

		u32 uiTemp = uiOffset + uiSize;
		u32 uiBufSize = uiSize;
		if( m_WaveInfo.BufferSize < uiTemp )
		{
			uiBufSize = uiSize - (uiTemp - m_WaveInfo.BufferSize);
		}
		m_Buffer.bLock = true;
		*ppBuf1	= &m_Buffer.pBuffer[uiOffset];
		*pSize1	= uiBufSize;
		*ppBuf2	= m_Buffer.pBuffer;
		*pSize2	= uiSize - uiBufSize;
	}
	//----------------------------------------------------------------------
	//	バッファのアンロック
	//----------------------------------------------------------------------
	void CXAudio2SoundBuffer::Unlock(void)
	{
#if	KG_SOUND_DEBUG
		if( !m_Buffer.bLock )
		{
			WarningTrace("バッファがロックされていません(CXAudio2SoundBuffer::Unlock)");
		}
#endif	// ~#if KG_SOUND_DEBUG
		m_Buffer.bLock = false;
	}

	//----------------------------------------------------------------------
	//	サウンドバッファ生成
	//----------------------------------------------------------------------
	b8 CXAudio2SoundBufferManager::CreateSoundBuffer(ISoundBuffer** ppBuffer, const WaveInfo& Info, const void* pData)
	{
		kgl::TKGLPtr<CXAudio2SoundBuffer> pSoundBuffer = new CXAudio2SoundBuffer;
		if( !pSoundBuffer->Create(m_pDevice, Info, pData) )
		{
			return false;
		}
		pSoundBuffer->AddRef();
		*ppBuffer = pSoundBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	サウンドバッファ生成
	//----------------------------------------------------------------------
	b8 CXAudio2SoundBufferManager::CreateSoundBuffer(ISoundBuffer** ppBuffer, IDecoder* pDecoder)
	{
		kgl::TKGLPtr<CXAudio2SoundBuffer> pSoundBuffer = new CXAudio2SoundBuffer;
		if( !pSoundBuffer->Create(m_pDevice, pDecoder) )
		{
			return false;
		}
		pSoundBuffer->AddRef();
		*ppBuffer = pSoundBuffer;
		return true;
	}
}

//======================================================================
//	END OF FILE
//======================================================================
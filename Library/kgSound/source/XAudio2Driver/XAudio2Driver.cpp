//----------------------------------------------------------------------
//!
//!	@file	XAudio2Driver.cpp
//!	@brief	XAudio2
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgSound.h"

#if	USE_XAUDIO2
#include "XAudio2Driver/XAudio2Device.h"
#include "XAudio2Driver/XAudio2SoundBuffer.h"
#include "Utility/Decoder/kgDecoderManager.h"

namespace kgSound
{
	//======================================================================
	//	XAudio2用ドライバー
	//======================================================================
	class CXAudio2Driver
		: public ISoundDriver
	{
	public:
		//----------------------------------------------------------------------
		//!	初期化
		//!	@param pWindow	[in] ウィンドウ
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 Initialize(void)
		{
			kgl::TKGLPtr<CXAudio2Device> pDevice;
			pDevice = new CXAudio2Device;
			if( !pDevice->Initialize() )
			{
				return false;
			}
			m_pDevice = pDevice;
			m_pSoundBufferManager = new CXAudio2SoundBufferManager(pDevice);
			m_pDecoderManager = new CDecoderManager;

			return true;
		}
	};

	//----------------------------------------------------------------------
	//	XAudio2用ドライバー作成
	//----------------------------------------------------------------------
	b8 CreateXAudio2Driver(ISoundDriver** ppDriver)
	{
		kgl::TKGLPtr<CXAudio2Driver> pDriver = new CXAudio2Driver;
		if( !pDriver->Initialize() )
		{
			return false;
		}
		pDriver->AddRef();
		*ppDriver = pDriver;

		return true;
	}
}

#endif	// ~#if USE_XAUDIO2

//======================================================================
//	END OF FILE
//======================================================================
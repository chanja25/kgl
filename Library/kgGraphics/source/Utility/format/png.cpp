//----------------------------------------------------------------------
//!
//!	@file	dds.cpp
//!	@brief	DDS関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//	include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	KG_USE_LIB_PNG
#include <png.h>

#pragma comment(lib, "libpng15.lib")

namespace kgGraphics
{
	struct ConvertInfo
	{
		u32	uiOffset;
		const u8*	pBuffer;
	};

	//----------------------------------------------------------------------
	//	PNG読み込み
	//----------------------------------------------------------------------
	void ReadPNG(png_structp png_ptr, png_bytep data, png_size_t length)
	{
		ConvertInfo* pInfo = (ConvertInfo*)png_get_io_ptr(png_ptr);
		auto pBuffer = pInfo->pBuffer + pInfo->uiOffset;

		kgMemcpy(data, pBuffer, length);
		pInfo->uiOffset += (u32)length;
	}

	//----------------------------------------------------------------------
	//	PNG分析
	//----------------------------------------------------------------------
	b8 AnalyzePNG(PNG_INFO* pInfo, const void* pAddr)
	{
		png_structp	png_ptr;
		png_infop	info_ptr;

		png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, null, null, null);
		info_ptr = png_create_info_struct(png_ptr);
		if( !png_ptr || !info_ptr )
		{
			ErrorTrace("pngファイルの読み込みに失敗しました(kgGraphics::AnalizePNG)");
			return false;
		}
		u32 uiWidth, uiHeight;
		s32 iBitDepth, iColorType, iInterlaceType;

		ConvertInfo CnvInfo;
		CnvInfo.pBuffer = (u8*)pAddr;
		CnvInfo.uiOffset = 0;

		png_set_read_fn(png_ptr, &CnvInfo, ReadPNG);
		png_read_info(png_ptr, info_ptr);

		png_get_IHDR(png_ptr, info_ptr,
			&uiWidth, &uiHeight,
			&iBitDepth, &iColorType, &iInterlaceType, null, null);

		u32 uiPixelCount = uiWidth * uiHeight;
		png_byte* png_image = new png_byte[uiPixelCount * iBitDepth];
		kgl::TSmartPtr<png_bytep> png_ptrs	= new png_bytep[uiHeight];
		for( u32 i=0; i < uiHeight; i++ )
		{
			png_ptrs[i] = &png_image[i * uiHeight * iBitDepth];
		}

		png_read_image(png_ptr, png_ptrs);
		png_read_end(png_ptr, info_ptr);

		png_destroy_write_struct(&png_ptr, &info_ptr);

		// データの設定
		pInfo->pPixels = png_image;
		pInfo->uiFormat = ETextureFormat::A8R8G8B8;
		pInfo->uiWidth = uiWidth;
		pInfo->uiHeight = uiHeight;

		return true;
	}
}
#endif	// ~#if KG_USE_LIB_PNG

//======================================================================
//	END OF FILE
//======================================================================

//----------------------------------------------------------------------
//!
//!	@file	dds.cpp
//!	@brief	DDS関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//	include
//----------------------------------------------------------------------
#include "kgGraphics.h"


namespace kgGraphics
{
	FORCEINLINE void* SWAP_DDSURFACEDESC2(void* ptr, s32 loop = 1) { return kgl::bswap(ptr, "4444444444444444444444444444444", loop);	}

	//----------------------------------------------------------------------
	//	DDS分析
	//----------------------------------------------------------------------
	b8 AnalyzeDDS(DDS_INFO* pInfo, const void* pData)
	{
		if( pData == null )	return false;

		const c8* pAddr = (const c8*)pData;
		kgZeroMemory(pInfo, sizeof(DDS_INFO));

		s32 iID = *(s32*)pAddr;
		if( iID == FOURCC('D', 'D', 'S', ' ') )
		{
			const DDSURFACEDESC2* pDdsd;
			DDSURFACEDESC2 Ddsd;
			pDdsd = (const DDSURFACEDESC2*)(pAddr + 4);
			Ddsd = *pDdsd;
			//	エンディアンチェック
			if( !kgl::IsLittleEndian() )
			{
				SWAP_DDSURFACEDESC2(&Ddsd);
			}

			switch( Ddsd.ddspf.dwFourCC )
			{
			case FOURCC_DXT1: pInfo->uiFormat = DDSFMT_DXT1; break;
			case FOURCC_DXT3: pInfo->uiFormat = DDSFMT_DXT3; break;
			case FOURCC_DXT5: pInfo->uiFormat = DDSFMT_DXT5; break;

			default:
				{
					if( Ddsd.ddspf.dwRGBBitCount == 32 )
					{
						if( Ddsd.ddspf.dwRBitMask == 0x00ff0000 &&
							Ddsd.ddspf.dwGBitMask == 0x0000ff00 &&
							Ddsd.ddspf.dwBBitMask == 0x000000ff )
						{
							pInfo->uiFormat = DDSFMT_A8R8G8B8;
						}
					}
					else if( Ddsd.ddspf.dwRGBBitCount == 16 )
					{
						if( Ddsd.ddspf.dwRBitMask == 0x001f0000 &&
							Ddsd.ddspf.dwGBitMask == 0x00001f00 &&
							Ddsd.ddspf.dwBBitMask == 0x0000001f )
						{
							pInfo->uiFormat = DDSFMT_A1R5G5B5;
						}
						else if( Ddsd.ddspf.dwRBitMask == 0x000f0000 &&
								 Ddsd.ddspf.dwGBitMask == 0x00000f00 &&
								 Ddsd.ddspf.dwBBitMask == 0x0000000f &&
								 Ddsd.ddspf.dwABitMask == 0x01000000 )
						{
							pInfo->uiFormat = DDSFMT_A4R4G4B4;
						}
					}
					else if( Ddsd.ddspf.dwRGBBitCount == 8 )
					{
						if( Ddsd.ddspf.dwABitMask == 0xff000000 )
						{
							pInfo->uiFormat = DDSFMT_A8;
						}
						if( Ddsd.ddspf.dwRBitMask == 0x00ff0000 )
						{
							pInfo->uiFormat = DDSFMT_L8;
						}
					}

					if( !pInfo->uiFormat )	return false;
				}
				break;
			}

			pInfo->pPixels		= pDdsd + 1;
			pInfo->uiWidth		= Ddsd.dwWidth;
			pInfo->uiHeight		= Ddsd.dwHeight;
			pInfo->iMipLevels	= kgl::Max<s32>(Ddsd.dwMipMapCount, 1);
			pInfo->iCubemap		= (s32)Ddsd.dwCubemapFlags;

			return true;
		}

		return false;
	}
}


//======================================================================
//	END OF FILE
//======================================================================

//----------------------------------------------------------------------
//!
//!	@file	dds.cpp
//!	@brief	DDS関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//	include
//----------------------------------------------------------------------
#include "kgGraphics.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	ピクセル毎のサイズを取得
	//----------------------------------------------------------------------
	u32 GetStride(s32 iFormat)
	{
		switch( iFormat )
		{
		case ETextureFormat::R8G8B8:		return 3;
		case ETextureFormat::A8R8G8B8:		return 4;
		case ETextureFormat::A16R16G16B16:	return 8;
		case ETextureFormat::A16R16G16B16F:	return 8;
		case ETextureFormat::R5G6B5:		return 2;
		case ETextureFormat::A1R5G5B5:		return 2;
		case ETextureFormat::A4R4G4B4:		return 2;
		case ETextureFormat::A8:			return 1;
		case ETextureFormat::L8:			return 1;
		case ETextureFormat::A8L8:			return 2;
		case ETextureFormat::R16F:			return 2;
		case ETextureFormat::R32F:			return 4;
		case ETextureFormat::DXT1:			return 0;
		case ETextureFormat::DXT3:			return 0;
		case ETextureFormat::DXT5:			return 0;
		case ETextureFormat::D16:			return 2;
		case ETextureFormat::D32:			return 4;
		case ETextureFormat::D24S8:			return 4;
		}
		return 0;
	}

	//----------------------------------------------------------------------
	//	テクスチャーデータの構築
	//----------------------------------------------------------------------
	TextureData* BuildTextureData(const TextureInfo& Info, const void* pData)
	{
		const c8* pOffset = (const c8*)pData;
		TextureData* pTexData = new TextureData[Info.iMipLevels];

		switch( Info.iFormat )
		{
		case ETextureFormat::R8G8B8:
		case ETextureFormat::A8R8G8B8:
		case ETextureFormat::A16R16G16B16:
		case ETextureFormat::A16R16G16B16F:
		case ETextureFormat::R5G6B5:
		case ETextureFormat::A1R5G5B5:
		case ETextureFormat::A4R4G4B4:
		case ETextureFormat::A8:
		case ETextureFormat::L8:
		case ETextureFormat::A8L8:
		case ETextureFormat::R16F:
		case ETextureFormat::R32F:
		case ETextureFormat::D16:
		case ETextureFormat::D32:
		case ETextureFormat::D24S8:
			{
				u32 uiStride = GetStride(Info.iFormat);
				for( s32 i=0; i < Info.iMipLevels; i++ )
				{
					u32 uiWidth	 = kgl::Max<u32>(Info.uiWidth  >> i, 1);
					u32 uiHeight = kgl::Max<u32>(Info.uiHeight >> i, 1);

					pTexData[i].pData	= pOffset;
					pTexData[i].uiPitch	= uiWidth * uiStride;
					pTexData[i].uiSize	= uiHeight * pTexData[i].uiPitch;

					pOffset += pTexData[i].uiSize;
				}
			}
			break;

		case ETextureFormat::DXT1:
		case ETextureFormat::DXT3:
		case ETextureFormat::DXT5:
			{
				u32 BlockSize = 16;
				if( Info.iFormat == ETextureFormat::DXT1 )
				{
					BlockSize = 8;
				}
				for( s32 i=0; i < Info.iMipLevels; i++ )
				{
					u32 uiWidth	 = kgl::Max<u32>(Info.uiWidth  >> i, 1);
					u32 uiHeight = kgl::Max<u32>(Info.uiHeight >> i, 1);

					pTexData[i].pData	= pOffset;
					pTexData[i].uiPitch	= kgl::Align<u32>(uiWidth,  4) / 4 * BlockSize;
					pTexData[i].uiSize	= kgl::Align<u32>(uiHeight, 4) / 4 * pTexData[i].uiPitch;

					pOffset += pTexData[i].uiSize;
				}
			}
			break;
		}
		return pTexData;
	}
}


//======================================================================
//	END OF FILE
//======================================================================

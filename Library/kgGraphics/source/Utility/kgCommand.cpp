//----------------------------------------------------------------------
//!
//!	@file	kgCommand.cpp
//!	@brief	グラフィックコマンド関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#define	ResetInfo(param)		kgMemset(&m_##param, -1, sizeof(m_##param));
#define	ResetInfoArray(param)	kgMemset(m_##param, -1, sizeof(m_##param));

namespace kgGraphics
{
	//	コマンド情報のリセット
	void ICommandCreator::ResetState(void)
	{
		ResetInfo(AlphaTestInfo);
		ResetInfo(DepthTestInfo);
		ResetInfo(StencilTestInfo);
		ResetInfo(BlendInfo);
		ResetInfo(CullModeInfo);
		ResetInfoArray(TextureInfo);
		ResetInfoArray(LODBiasInfo);
		ResetInfoArray(SamplerStateInfo);
		ResetInfo(ViewPortInfo);
		ResetInfoArray(RenderTargetInfo);
		ResetInfo(DepthTargetInfo);
		ResetInfo(DeclarationInfo);
		ResetInfoArray(VertexBindInfo);
		ResetInfo(IndexBindInfo);
		ResetInfo(VertexShaderInfo);
		ResetInfo(PixelShaderInfo);
		ResetInfo(ScissoringInfo);
	}
}

//======================================================================
//	END OF FILE
//======================================================================
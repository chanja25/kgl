//----------------------------------------------------------------------
//!
//!	@file	Dx11IndexBuffer.cpp
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11IndexBuffer.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11IndexBuffer::CDx11IndexBuffer(void)
		: m_pBuffer(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx11IndexBuffer::Initialize(CDx11Device* pDevice, u32 uiSize, u8 ucType, b8 bDynamic)
	{
		m_pDevice	= pDevice;
		m_ucType	= ucType;

		D3D11_BUFFER_DESC desc;
		desc.ByteWidth				= uiSize;
		desc.Usage					= D3D11_USAGE_DYNAMIC;
		desc.BindFlags				= D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags			= bDynamic? D3D11_CPU_ACCESS_WRITE: 0;
		desc.MiscFlags				= 0;
		desc.StructureByteStride	= 0;

		VerifyResult(pDevice->GetDevice()->CreateBuffer(&desc, null, m_pBuffer.GetReference()));
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CDx11IndexBuffer::Lock(u32 uiOffset, u32 uiSize, s32 iType)
	{
		(void)uiSize;

		D3D11_MAP MapType[] =
		{
			D3D11_MAP_READ,
			D3D11_MAP_WRITE_DISCARD,
			D3D11_MAP_WRITE_NO_OVERWRITE,
			D3D11_MAP_READ_WRITE,
		};
		D3D11_MAPPED_SUBRESOURCE resource;

		VerifyResult(m_pDevice->DeviceContext()->Map(m_pBuffer, 0, MapType[iType], 0, &resource));
		return (u8*)resource.pData + uiOffset;
	}
	//----------------------------------------------------------------------
	//	バッファのアンロック
	//----------------------------------------------------------------------
	void CDx11IndexBuffer::Unlock(void)
	{
		m_pDevice->DeviceContext()->Unmap(m_pBuffer, 0);
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
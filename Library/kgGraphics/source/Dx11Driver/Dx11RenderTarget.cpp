//----------------------------------------------------------------------
//!
//!	@file	Dx11RenderTarget.cpp
//!	@brief	レンダーターゲット
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11RenderTarget.h"
#include "Dx11Driver/Dx11Texture.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	レンダーターゲット生成
	//----------------------------------------------------------------------
	b8 CDx11RenderTargetManager::CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info)
	{
		kgl::TKGLPtr<CDx11RenderTarget> pRenderTarget = new CDx11RenderTarget;
		if( !pRenderTarget->Create(m_pDevice, Info) )
		{
			return false;
		}

		pRenderTarget->AddRef();
		*ppRenderTarget = pRenderTarget;
		return true;
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット生成
	//----------------------------------------------------------------------
	b8 CDx11RenderTarget::Create(CDx11Device* pDevice, const RenderTargetInfo& Info)
	{
		TextureInfo TexInfo;
		TexInfo.iType		= ETextureType::Normal;
		TexInfo.iUsage		= Info.iUsage;
		TexInfo.iFormat		= Info.iFormat;
		TexInfo.uiWidth		= Info.uiWidth;
		TexInfo.uiHeight	= Info.uiHeight;
		TexInfo.SampleDesc	= Info.SampleDesc;
		TexInfo.iMipLevels	= 1;

		m_Info = Info;

		kgl::TKGLPtr<CDx11Texture2D> pRenderTarget = new CDx11Texture2D;
		if( !pRenderTarget->Create(pDevice, TexInfo, null) )
		{
			return false;
		}
		m_pRenderTarget = pRenderTarget;
		return true;
	}

	//----------------------------------------------------------------------
	//	レンダーターゲットのロック
	//----------------------------------------------------------------------
	void* CDx11RenderTarget::Lock(s32 iType)
	{
		return m_pRenderTarget->Lock(0, iType);
	}
	//----------------------------------------------------------------------
	//	レンダーターゲットのアンロック
	//----------------------------------------------------------------------
	void CDx11RenderTarget::Unlock(void)
	{
		m_pRenderTarget->Unlock(0);
	}
	//----------------------------------------------------------------------
	//	レンダーターゲット(テクスチャー)の取得
	//----------------------------------------------------------------------
	void CDx11RenderTarget::GetRenderTarget(ITextureBase** ppTexture)
	{
		m_pRenderTarget->AddRef();
		*ppTexture = m_pRenderTarget;
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx11RenderTarget::GetSurface(ISurfaceBase** ppSurface)
	{
		return m_pRenderTarget->GetSurface(ppSurface, 0);
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
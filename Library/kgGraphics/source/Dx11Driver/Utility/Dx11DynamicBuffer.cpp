//----------------------------------------------------------------------
//!
//!	@file	Dx11DynamicBuffer.cpp
//!	@brief	動的バッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Utility/Dx11DynamicBuffer.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11DynamicVertexBuffer::CDx11DynamicVertexBuffer(void)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx11DynamicVertexBuffer::Initialize(CDx11Device* pDevice)
	{
		m_pDevice = pDevice;

		u32 uiSize[] =
		{
			1024,
			1024 * 32,
			1024 * 64,
			1024 * 128,
		};
		for (u32 i=0; i < BufferMax; i++)
		{
			Allocate(i, uiSize[i]);
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CDx11DynamicVertexBuffer::Lock(u32 uiSize)
	{
		kgl::TKGLPtr<CDx11VertexBuffer> pBuffer;
		for (u32 i = 0; i < BufferMax; i++)
		{
			if (uiSize <= m_uiSize[i])
			{
				pBuffer = m_pBuffer[i];
				break;
			}
		}
		if (!pBuffer.IsValid())
		{
			pBuffer = Allocate(BufferMax - 1, uiSize);
		}

		m_pLock = pBuffer;
		return pBuffer->Lock(0, uiSize, ELockType::Write);
	}
	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	CDx11VertexBuffer* CDx11DynamicVertexBuffer::Unlock(void)
	{
		kgl::TKGLPtr<CDx11VertexBuffer> pBuffer = m_pLock;
		m_pLock = null;
		if (pBuffer.IsValid())
		{
			pBuffer->Unlock();
		}
		return pBuffer;
	}
	//----------------------------------------------------------------------
	//	バッファの確保
	//----------------------------------------------------------------------
	CDx11VertexBuffer* CDx11DynamicVertexBuffer::Allocate(u32 uiNo, u32 uiSize)
	{
		kgl::TKGLPtr<CDx11VertexBuffer> pBuffer;
		pBuffer = new CDx11VertexBuffer();
		pBuffer->Initialize(m_pDevice, uiSize, true);

		m_pBuffer[uiNo] = pBuffer;
		m_uiSize[uiNo] = uiSize;
		return pBuffer;
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11DynamicIndexBuffer::CDx11DynamicIndexBuffer(void)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx11DynamicIndexBuffer::Initialize(CDx11Device* pDevice)
	{
		m_pDevice = pDevice;

		u32 uiSize[] =
		{
			1024,
			1024 * 32,
			1024 * 64,
			1024 * 128,
		};
		for (u32 i=0; i < BufferMax; i++)
		{
			Allocate(i, uiSize[i]);
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CDx11DynamicIndexBuffer::Lock(u32 uiSize)
	{
		kgl::TKGLPtr<CDx11IndexBuffer> pBuffer;
		for (u32 i = 0; i < BufferMax; i++)
		{
			if (uiSize < m_uiSize[i])
			{
				pBuffer = m_pBuffer[i];
				break;
			}
		}
		if (!pBuffer.IsValid())
		{
			pBuffer = Allocate(BufferMax - 1, uiSize);
		}

		m_pLock = pBuffer;
		return pBuffer->Lock(0, uiSize, ELockType::Write);
	}
	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	CDx11IndexBuffer* CDx11DynamicIndexBuffer::Unlock(void)
	{
		kgl::TKGLPtr<CDx11IndexBuffer> pBuffer = m_pLock;
		m_pLock = null;
		if (pBuffer.IsValid())
		{
			pBuffer->Unlock();
		}

		return pBuffer;
	}
	//----------------------------------------------------------------------
	//	バッファの確保
	//----------------------------------------------------------------------
	CDx11IndexBuffer* CDx11DynamicIndexBuffer::Allocate(u32 uiNo, u32 uiSize)
	{
		kgl::TKGLPtr<CDx11IndexBuffer> pBuffer;
		pBuffer = new CDx11IndexBuffer();
		pBuffer->Initialize(m_pDevice, uiSize, EIndexType::Index16, true);

		m_pBuffer[uiNo] = pBuffer;
		m_uiSize[uiNo] = uiSize;
		return pBuffer;
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
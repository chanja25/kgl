//----------------------------------------------------------------------
//!
//!	@file	Dx11Texture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Texture.h"
#include "Dx11Driver/Dx11Surface.h"

namespace kgGraphics
{
	namespace ETextureUsage
	{
		const s32 StagingWrite	= (1 << 30);
		const s32 StagingRead	= (1 << 31);
		const s32 Staging		= StagingWrite | StagingRead;
	}

	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx11TextureManager::CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData)
	{
		kgl::TKGLPtr<CDx11Texture2D> pTexture = new CDx11Texture2D;
		if( !pTexture->Create(m_pDevice, Info, pData) )
		{
			return false;
		}
		pTexture->AddRef();
		*ppTexture = pTexture;
		return true;
	}
	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx11TextureManager::CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData)
	{
		kgl::TKGLPtr<CDx11Texture2D> pTexture = new CDx11Texture2D;
		if( !pTexture->CreateFromDDS(m_pDevice, pData) )
		{
			return false;
		}
		pTexture->AddRef();
		*ppTexture = pTexture;
		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx11Texture2D::Create(CDx11Device* pDevice, const TextureInfo& Info, const void* pData)
	{
		m_pDevice = pDevice;

		DXGI_FORMAT eFormat;
		switch( Info.iFormat )
		{
		case ETextureFormat::R8G8B8:		eFormat = DXGI_FORMAT_R8G8B8A8_UNORM;		break;
		case ETextureFormat::A8R8G8B8:		eFormat = DXGI_FORMAT_R8G8B8A8_UNORM;		break;
		case ETextureFormat::A16R16G16B16:	eFormat = DXGI_FORMAT_R16G16B16A16_UINT;	break;
		case ETextureFormat::A16R16G16B16F:	eFormat = DXGI_FORMAT_R16G16B16A16_FLOAT;	break;
		case ETextureFormat::R5G6B5:		eFormat = DXGI_FORMAT_B5G6R5_UNORM;			break;
		case ETextureFormat::A1R5G5B5:		eFormat = DXGI_FORMAT_B5G5R5A1_UNORM;		break;
		case ETextureFormat::A4R4G4B4:		eFormat = DXGI_FORMAT_UNKNOWN;				break;
		case ETextureFormat::A8:			eFormat = DXGI_FORMAT_A8_UNORM;				break;
		case ETextureFormat::L8:			eFormat = DXGI_FORMAT_R8_UINT;				break;
		case ETextureFormat::A8L8:			eFormat = DXGI_FORMAT_R8G8_UINT;			break;
		case ETextureFormat::R16F:			eFormat = DXGI_FORMAT_R16_FLOAT;			break;
		case ETextureFormat::R32F:			eFormat = DXGI_FORMAT_R32_FLOAT;			break;
		case ETextureFormat::DXT1:			eFormat = DXGI_FORMAT_BC1_UNORM;			break;
		case ETextureFormat::DXT3:			eFormat = DXGI_FORMAT_BC2_UNORM;			break;
		case ETextureFormat::DXT5:			eFormat = DXGI_FORMAT_BC3_UNORM;			break;
		case ETextureFormat::D16:			eFormat = DXGI_FORMAT_D16_UNORM;			break;
		case ETextureFormat::D32:			eFormat = DXGI_FORMAT_D32_FLOAT;			break;
		case ETextureFormat::D24S8:			eFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;	break;
		default:
			ErrorTrace("指定されたFormatが対応していないか不正な値です(CDx11Texture2D::Create)");
			return false;
		}
		s32 iMipLevels = kgl::Max(1, Info.iMipLevels);

		D3D11_USAGE eUsage = D3D11_USAGE_DEFAULT; // GPUからのReadWriteを許可
		if( Info.iUsage & ETextureUsage::Staging )
		{
			eUsage = D3D11_USAGE_STAGING;	// CPUからのReadWriteを許可
		}
		u32 uiBindFlag = 0;
		if( Info.iUsage & ETextureUsage::Depth )
		{
			uiBindFlag |= D3D11_BIND_DEPTH_STENCIL;
		}
		else
		{
			uiBindFlag |= !(Info.iUsage & ETextureUsage::Staging)?		D3D11_BIND_SHADER_RESOURCE: 0;
			uiBindFlag |= (Info.iUsage & ETextureUsage::RenderTarget)?	D3D11_BIND_RENDER_TARGET:	0;
		}

		u32 uiAccessFlag = 0;
		uiAccessFlag |= (Info.iUsage & ETextureUsage::StagingWrite)? D3D11_CPU_ACCESS_WRITE: 0;
		uiAccessFlag |= (Info.iUsage & ETextureUsage::StagingRead)? D3D11_CPU_ACCESS_READ: 0;

		D3D11_TEXTURE2D_DESC desc;
		desc.Width				= Info.uiWidth;
		desc.Height				= Info.uiHeight;
		desc.MipLevels			= iMipLevels;
		desc.ArraySize			= 1;
		desc.Format				= eFormat;
		desc.SampleDesc.Count	= (u32)Info.SampleDesc.iCount;
		desc.SampleDesc.Quality	= (u32)Info.SampleDesc.iQuality;
		desc.Usage				= eUsage;
		desc.BindFlags			= uiBindFlag;
		desc.CPUAccessFlags		= uiAccessFlag;
		desc.MiscFlags			= 0;

		kgl::TSmartPtr<D3D11_SUBRESOURCE_DATA> initData;
		D3D11_SUBRESOURCE_DATA* pInitData = null;
		if( pData )
		{
			kgl::TSmartPtr<TextureData> TexData;
			TexData = BuildTextureData(Info, pData);

			initData = new D3D11_SUBRESOURCE_DATA[iMipLevels];
			for( s32 i=0; i < iMipLevels; i++ )
			{
				initData[i].pSysMem				= TexData[i].pData;
				initData[i].SysMemPitch			= TexData[i].uiPitch;
				initData[i].SysMemSlicePitch	= TexData[i].uiSize;
			}
			pInitData = initData;
		}
		HRESULT hr;
		hr = pDevice->GetDevice()->CreateTexture2D(&desc, pInitData, m_pTexture.GetReference());
		if( FAILED(hr) )
		{
			ErrorTrace("テクスチャーの生成に失敗しました(CDx11Texture2D::Create)");
			ErrorTrace("Type:%d Usage:%d Format:%d", Info.iType, Info.iUsage, Info.iFormat);
			ErrorTrace("Width:%d Height:%d MipLevels:%d", Info.uiWidth, Info.uiHeight, Info.iMipLevels);
			return false;
		}

		if( uiBindFlag & D3D11_BIND_SHADER_RESOURCE )
		{
			hr = pDevice->GetDevice()->CreateShaderResourceView(m_pTexture, null, m_pResource.GetReference());
			if( FAILED(hr) )
			{
				ErrorTrace("シェーダーリソースの生成に失敗しました(CDx11Texture2D::Create)");
				ErrorTrace("Type:%d Usage:%d Format:%d", Info.iType, Info.iUsage, Info.iFormat);
				ErrorTrace("Width:%d Height:%d MipLevels:%d", Info.uiWidth, Info.uiHeight, Info.iMipLevels);
				return false;
			}
		}
		if( Info.iUsage & ETextureUsage::RenderTarget )
		{
			kgl::TKGLPtr<ID3D11RenderTargetView> pView;
			hr = pDevice->GetDevice()->CreateRenderTargetView(m_pTexture, null, pView.GetReference());
			if( FAILED(hr) )
			{
				ErrorTrace("レンダーターゲットビューの生成に失敗しました(CDx11Texture2D::Create)");
				ErrorTrace("Type:%d Usage:%d Format:%d", Info.iType, Info.iUsage, Info.iFormat);
				ErrorTrace("Width:%d Height:%d MipLevels:%d", Info.uiWidth, Info.uiHeight, Info.iMipLevels);
				return false;
			}
			m_pView = pView;
		}
		if( Info.iUsage & ETextureUsage::Depth )
		{
			kgl::TKGLPtr<ID3D11DepthStencilView> pView;
			hr = pDevice->GetDevice()->CreateDepthStencilView(m_pTexture, null, pView.GetReference());
			if( FAILED(hr) )
			{
				ErrorTrace("レンダーターゲットビューの生成に失敗しました(CDx11Texture2D::Create)");
				ErrorTrace("Type:%d Usage:%d Format:%d", Info.iType, Info.iUsage, Info.iFormat);
				ErrorTrace("Width:%d Height:%d MipLevels:%d", Info.uiWidth, Info.uiHeight, Info.iMipLevels);
				return false;
			}
			m_pView = pView;
		}
		D3D11_TEXTURE2D_DESC temp;
		m_pTexture->GetDesc(&temp);

		m_Info.iType		= Info.iType;
		m_Info.iUsage		= Info.iUsage;
		m_Info.iFormat		= Info.iFormat;
		m_Info.uiWidth		= Info.uiWidth;
		m_Info.uiHeight		= Info.uiHeight;
		m_Info.iMipLevels	= iMipLevels;
		return true;
	}

	//----------------------------------------------------------------------
	//	DDSからテクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx11Texture2D::CreateFromDDS(CDx11Device* pDevice, const void* pData)
	{
		if( pData == null )
		{
			ErrorTrace("指定されたデータがnullです(CDx11Texture2D::CreateFromDDS)");
			return false;
		}
		DDS_INFO ddsInfo;
		if( !AnalyzeDDS(&ddsInfo, pData) )
		{
			ErrorTrace("指定されたデータが壊れているか対応していないフォーマットです(CDx11Texture2D::CreateFromDDS)");
			return false;
		}
		if( ddsInfo.iCubemap )
		{
			ErrorTrace("現状のKGLはキューブマップに未対応です(CDx11Texture2D::CreateFromDDS)");
			return false;
		}

		TextureInfo Info;
		Info.iType		= ETextureType::Normal;
		Info.iUsage		= 0;
		Info.uiWidth	= ddsInfo.uiWidth;
		Info.uiHeight	= ddsInfo.uiHeight;
		Info.iMipLevels	= ddsInfo.iMipLevels;

		switch( ddsInfo.uiFormat )
		{
		case DDSFMT_DXT1:		Info.iFormat = ETextureFormat::DXT1;		break;
		case DDSFMT_DXT3:		Info.iFormat = ETextureFormat::DXT3;		break;
		case DDSFMT_DXT5:		Info.iFormat = ETextureFormat::DXT5;		break;
		case DDSFMT_A8R8G8B8:	Info.iFormat = ETextureFormat::A8R8G8B8;	break;
		case DDSFMT_A1R5G5B5:	Info.iFormat = ETextureFormat::A1R5G5B5;	break;
		case DDSFMT_A4R4G4B4:	Info.iFormat = ETextureFormat::A4R4G4B4;	break;
		case DDSFMT_A8:			Info.iFormat = ETextureFormat::A8;			break;
		case DDSFMT_L8:			Info.iFormat = ETextureFormat::L8;			break;
		default:
			ErrorTrace("指定されたデータが対応していないフォーマットです(CDx11Texture2D::CreateFromDDS)");
			return false;
		}

		if( !Create(pDevice, Info, ddsInfo.pPixels) )
		{
			ErrorTrace("テクスチャーの生成に失敗しました(CDx11Texture2D::CreateFromDDS)");
			return false;
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャーのロック
	//----------------------------------------------------------------------
	void* CDx11Texture2D::Lock(s32 iLevel, s32 iType)
	{
		if( m_Info.iUsage & ETextureUsage::Staging )
		{
			D3D11_MAP MapType[] =
			{
				D3D11_MAP_READ,
				D3D11_MAP_WRITE,
				D3D11_MAP_WRITE_NO_OVERWRITE,
				D3D11_MAP_READ_WRITE,
			};
			D3D11_MAPPED_SUBRESOURCE resource;
			u32 uiSubResource = D3D11CalcSubresource(iLevel, 0, m_Info.iMipLevels);

			VerifyResult(m_pDevice->DeviceContext()->Map(m_pTexture, uiSubResource , MapType[iType], 0, &resource));
			return resource.pData;
		}
		else
		{
			const s32 Usage[] =
			{
				ETextureUsage::StagingRead,
				ETextureUsage::StagingWrite,
				ETextureUsage::StagingWrite,
				ETextureUsage::Staging,
			};
			if( !m_pLockResource.IsValid() )
			{
				TextureInfo Info = m_Info;
				Info.iUsage = (m_Info.iUsage & ETextureUsage::Dynamic)? ETextureUsage::Staging: Usage[iType];

				m_pLockResource = new CDx11Texture2D;
				m_pLockResource->Create(m_pDevice, Info, null);
				m_pDevice->DeviceContext()->CopyResource(m_pLockResource->m_pTexture, m_pTexture);
			}
			return m_pLockResource->Lock(iLevel, iType);
		}
	}

	//----------------------------------------------------------------------
	//	テクスチャーのアンロック
	//----------------------------------------------------------------------
	void CDx11Texture2D::Unlock(s32 iLevel)
	{
		if( m_Info.iUsage & ETextureUsage::Staging )
		{
			u32 uiSubResource = D3D11CalcSubresource(iLevel, 0, m_Info.iMipLevels);
			m_pDevice->DeviceContext()->Unmap(m_pTexture, uiSubResource);
		}
		else if( m_pLockResource.IsValid() )
		{
			m_pLockResource->Unlock(iLevel);
			m_pDevice->DeviceContext()->CopyResource(m_pTexture, m_pLockResource->m_pTexture);
			if( !(m_Info.iUsage & ETextureUsage::Dynamic) )
			{
				m_pLockResource = null;
			}
		}
	}

	//----------------------------------------------------------------------
	//	リソースのコピー
	//----------------------------------------------------------------------
	b8 CDx11Texture2D::CopyResource(ITextureBase* pResource)
	{
		auto& SrcInfo = pResource->GetInfo();
		if( m_Info.iMipLevels != SrcInfo.iMipLevels ||
			m_Info.uiWidth != SrcInfo.uiWidth ||
			m_Info.uiHeight != SrcInfo.uiHeight ||
			m_Info.iFormat != SrcInfo.iFormat )
		{
			return false;
		}
		auto pTexture = kgl::Cast<CDx11Texture2D>(pResource);
		if( pTexture == null )
		{
			return false;
		}
		m_pDevice->DeviceContext()->CopyResource(m_pTexture, pTexture->m_pTexture);
		return true;
	}

	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx11Texture2D::GetSurface(ISurfaceBase** ppSurface, s32 iLevel)
	{
		if( iLevel >= m_Info.iMipLevels )
		{
			return false;
		}

		kgl::TKGLPtr<CDx11Surface> pSurface = new CDx11Surface;
		SurfaceInfo Info;
		Info.iFormat	= m_Info.iFormat;
		Info.iUsage		= m_Info.iUsage;
		Info.uiWidth	= m_Info.uiWidth  / (iLevel+1);
		Info.uiHeight	= m_Info.uiHeight / (iLevel+1);
		pSurface->Setup(m_pDevice, Info, m_pTexture, m_pView, m_Info.iMipLevels, iLevel);

		pSurface->AddRef();
		*ppSurface = pSurface;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
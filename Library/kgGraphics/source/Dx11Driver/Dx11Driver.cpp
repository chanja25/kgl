//----------------------------------------------------------------------
//!
//!	@file	Dx11Drivcer.h
//!	@brief	DirectX9
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")

#include "Dx11Driver/Dx11Device.h"
#include "Dx11Driver/Dx11Primitive.h"
#include "Dx11Driver/Dx11Command.h"
#include "Dx11Driver/Dx11Texture.h"
#include "Dx11Driver/Dx11Shader.h"
#include "Dx11Driver/Dx11RenderTarget.h"

namespace kgGraphics
{
	//======================================================================
	//	DirectX11用ドライバー
	//======================================================================
	class CDx11Driver
		: public IGraphicDriver
	{
	public:
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		~CDx11Driver(void)
		{
			Finalize();
		}

	public:
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 Initialize(const GRAPHICS_INFO& info)
		{
			kgAssert(!m_pDevice.IsValid(), "既に初期化されています(CDx11Driver::Initialize)");
			kgl::TKGLPtr<CDx11Device> pDevice = new CDx11Device;
			if( pDevice->Initialize(info) == false )
			{
				return false;
			}

			m_pDevice				= pDevice;
			m_pPrimitiveManager		= new CDx11PrimitiveManager(pDevice);
			m_pTextureManager		= new CDx11TextureManager(pDevice);
			m_pRenderTargetManager	= new CDx11RenderTargetManager(pDevice);
			m_pShaderManager		= new CDx11ShaderManager(pDevice);
			m_pCommandCreator		= new CDx11Command(pDevice);
			return true;
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void Finalize(void)
		{
			if( m_pDevice.IsValid() )
			{
				IGraphicDevice* p = m_pDevice;
				auto pDevice = (CDx11Device*)p;
				pDevice->Finalize();
			}
			m_pCommandCreator		= null;
			m_pShaderManager		= null;
			m_pRenderTargetManager	= null;
			m_pTextureManager		= null;
			m_pPrimitiveManager		= null;
			m_pDevice				= null;
		}

		//----------------------------------------------------------------------
		//	ドライバータイプの取得
		//----------------------------------------------------------------------
		s32 GetType(void)
		{
			return EDriverType::DirectX11;
		}
		//----------------------------------------------------------------------
		//	ドライバー名の取得
		//----------------------------------------------------------------------
		const c8* Name(void)
		{
			return "DirectX11";
		}
	};

	//----------------------------------------------------------------------
	//	DirectX11用ドライバー作成
	//----------------------------------------------------------------------
	b8 CreateDX11Driver(IGraphicDriver** ppDriver)
	{
		*ppDriver = new CDx11Driver;
		(*ppDriver)->AddRef();
		return true;
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
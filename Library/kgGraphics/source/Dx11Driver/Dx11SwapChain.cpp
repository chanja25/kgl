//----------------------------------------------------------------------
//!
//!	@file	Dx11SwapChain.cpp
//!	@brief	スワップチェイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11SwapChain.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	スワップチェイン生成
	//----------------------------------------------------------------------
	b8 CDx11SwapChain::Create(CDx11Device* pDevice, kgl::Window::IWindow* pWindow)
	{
		s32 iWidth, iHeight;
		pWindow->GetSize(iWidth, iHeight);
		//	スワップチェインの設定
		DXGI_SWAP_CHAIN_DESC desc;
		kgZeroMemory(&desc, sizeof(desc));
		desc.BufferCount						= 1;
		desc.BufferDesc.Width					= iWidth;
		desc.BufferDesc.Height					= iHeight;
		desc.BufferDesc.Format					= DXGI_FORMAT_R10G10B10A2_UNORM;//DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.BufferDesc.RefreshRate.Numerator	= 0;
		desc.BufferDesc.RefreshRate.Denominator	= 0;
		desc.BufferDesc.Scaling					= DXGI_MODE_SCALING_UNSPECIFIED;
		desc.BufferDesc.ScanlineOrdering		= DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		desc.BufferUsage						= DXGI_USAGE_RENDER_TARGET_OUTPUT;
		desc.OutputWindow						= (HWND)pWindow->GetHandle();
		desc.SampleDesc.Count					= 1;
		desc.SampleDesc.Quality					= 0;//(UINT)D3D11_STANDARD_MULTISAMPLE_PATTERN;
		desc.Windowed							= !pWindow->IsFullScreen();
		desc.Flags								= DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

		if( FAILED(pDevice->Factory()->CreateSwapChain(pDevice->GetDevice(), &desc, m_pSwapChain.GetReference())) )
		{
			ErrorTrace("SwapChainの生成に失敗しました(CDx11SwapChain::Initialize)");
			return false;
		}

		kgl::TKGLPtr<ID3D11Texture2D> pBuffer;
		// レンダーバッファの作成
		m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), pBuffer.GetComReference());
		if( FAILED(pDevice->GetDevice()->CreateRenderTargetView(pBuffer, null, m_pView.GetReference())) )
		{
			ErrorTrace("レンダーバッファの生成に失敗しました(CDx11SwapChain::Initialize)");
			return false;
		}

		m_pSurface = new CDx11Surface;
		SurfaceInfo Info;
		Info.iFormat	= ETextureFormat::A8R8G8B8;
		Info.iUsage		= ETextureUsage::RenderTarget;
		Info.uiWidth	= iWidth;
		Info.uiHeight	= iHeight;
		m_pSurface->Setup(pDevice, Info, pBuffer, m_pView);
		m_pWindow = pWindow;

		return true;
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx11SwapChain::Present(bool bVsync)
	{
		if( m_pWindow->IsForcus() )
		{
			s32 iFullScreen;
			m_pSwapChain->GetFullscreenState(&iFullScreen, null);
			//	スクリーンモードが予期せぬ変更を行われていた場合は再度更新する
			if( !!iFullScreen != m_pWindow->IsFullScreen() )
			{
				m_pSwapChain->SetFullscreenState(!iFullScreen, null);
			}
		}
		m_pSwapChain->Present(bVsync ? 1 : 0, 0);
		return true;
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx11SwapChain::GetSurface(ISurfaceBase** ppSurface)
	{
		m_pSurface->AddRef();
		*ppSurface = m_pSurface;
		return true;
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
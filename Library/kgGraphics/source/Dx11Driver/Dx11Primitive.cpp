//----------------------------------------------------------------------
//!
//!	@file	Dx11Primitive.cpp
//!	@brief	Primitive関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Primitive.h"
#include "Dx11Driver/Dx11IndexBuffer.h"
#include "Dx11Driver/Dx11VertexBuffer.h"
#include "Dx11Driver/Dx11Declaration.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	インデックスバッファ生成
	//----------------------------------------------------------------------
	b8 CDx11PrimitiveManager::CreateIndexBuffer(IIndexBuffer** ppIndex, u32 uiSize, u8 ucType, b8 bDynamic)
	{
		kgl::TKGLPtr<CDx11IndexBuffer> pBuffer = new CDx11IndexBuffer;
		if( !pBuffer->Initialize(m_pDevice, uiSize, ucType, bDynamic) )
		{
			return false;
		}
		pBuffer->AddRef();
		*ppIndex = pBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	頂点バッファ生成
	//----------------------------------------------------------------------
	b8 CDx11PrimitiveManager::CreateVertexBuffer(IVertexBuffer** ppVertex, u32 uiSize, b8 bDynamic)
	{
		kgl::TKGLPtr<CDx11VertexBuffer> pBuffer = new CDx11VertexBuffer;
		if( !pBuffer->Initialize(m_pDevice, uiSize, bDynamic) )
		{
			return false;
		}
		pBuffer->AddRef();
		*ppVertex = pBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	頂点フォーマット設定生成
	//----------------------------------------------------------------------
	b8 CDx11PrimitiveManager::CreateDeclaration(IDeclaration** ppDeclaration, const DeclarationInfo* pDeclInfo)
	{
		kgl::TKGLPtr<CDx11Declaration> pDecl = new CDx11Declaration;
		if( !pDecl->Initialize(m_pDevice, pDeclInfo) )
		{
			return false;
		}
		pDecl->AddRef();
		*ppDeclaration = pDecl;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
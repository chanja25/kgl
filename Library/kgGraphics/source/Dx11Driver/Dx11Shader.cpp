//----------------------------------------------------------------------
//!
//!	@file	Dx11Shader.cpp
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Shader.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CDx11VertexShader::Create(CDx11Device* pDevice, const void* pData)
	{
		m_pInfo = (const ShaderInfo*)pData;
		VerifyResult(pDevice->GetDevice()->CreateVertexShader((m_pInfo + 1), m_pInfo->iSize, null, m_pShader.GetReference()));

		return true;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CPixelShader::Create(CDx11Device* pDevice, const void* pData)
	{
		const ShaderInfo* pInfo = (const ShaderInfo*)pData;
		VerifyResult(pDevice->GetDevice()->CreatePixelShader((pInfo + 1), pInfo->iSize, null, m_pShader.GetReference()));

		return true;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CDx11ShaderManager::CreateVertexShader(IVertexShader** ppShader, const void* pData)
	{
		kgl::TKGLPtr<CDx11VertexShader> pShader = new CDx11VertexShader;
		if( !pShader->Create(m_pDevice, pData) )
		{
			return false;
		}
		pShader->AddRef();
		*ppShader = pShader;
		return true;
	}
	//----------------------------------------------------------------------
	//	ピクセルシェーダー生成
	//----------------------------------------------------------------------
	b8 CDx11ShaderManager::CreatePixelShader(IPixelShader** ppShader, const void* pData)
	{
		kgl::TKGLPtr<CPixelShader> pShader = new CPixelShader;
		if( !pShader->Create(m_pDevice, pData) )
		{
			return false;
		}
		pShader->AddRef();
		*ppShader = pShader;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx11VertexBuffer.cpp
//!	@brief	頂点バッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11VertexBuffer.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11VertexBuffer::CDx11VertexBuffer(void)
		: m_pBuffer(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx11VertexBuffer::Initialize(CDx11Device* pDevice, u32 uiSize, b8 bDynamic)
	{
		m_pDevice = pDevice;

		D3D11_BUFFER_DESC desc;
		desc.ByteWidth				= uiSize;
		desc.Usage					= D3D11_USAGE_DYNAMIC;
		desc.BindFlags				= D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags			= bDynamic? D3D11_CPU_ACCESS_WRITE: 0;
		desc.MiscFlags				= 0;
		desc.StructureByteStride	= 0;

		VerifyResult(pDevice->GetDevice()->CreateBuffer(&desc, null, m_pBuffer.GetReference()));
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CDx11VertexBuffer::Lock(u32 uiOffset, u32 uiSize, s32 iType)
	{
		(void)uiSize;

		const D3D11_MAP MapType[] =
		{
			D3D11_MAP_READ,
			D3D11_MAP_WRITE_DISCARD,
			D3D11_MAP_WRITE_NO_OVERWRITE,
			D3D11_MAP_READ_WRITE,
		};
		D3D11_MAPPED_SUBRESOURCE resource;

		VerifyResult(m_pDevice->DeviceContext()->Map(m_pBuffer, 0, MapType[iType], 0, &resource));
		return (u8*)resource.pData + uiOffset;
	}
	//----------------------------------------------------------------------
	//	バッファのアンロック
	//----------------------------------------------------------------------
	void CDx11VertexBuffer::Unlock(void)
	{
		m_pDevice->DeviceContext()->Unmap(m_pBuffer, 0);
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx11Command.cpp
//!	@brief	コマンド生成
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Command.h"
#include "Dx11Driver/Dx11Declaration.h"
#include "Dx11Driver/Dx11VertexBuffer.h"
#include "Dx11Driver/Dx11IndexBuffer.h"
#include "Dx11Driver/Dx11Texture.h"
#include "Dx11Driver/Dx11Shader.h"

#define	Dx11Device()			m_pDevice->GetDevice()
#define	CurrentContext()		m_pDeviceContext
#define	SetRenderState			Dx11Device()->SetRenderState

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	描画形状の取得
	//----------------------------------------------------------------------
	static INLINE D3D11_PRIMITIVE_TOPOLOGY PrimitiveTeplogy(s32 iPimitive)
	{
		const D3D11_PRIMITIVE_TOPOLOGY Primitive[] =
		{
			D3D_PRIMITIVE_TOPOLOGY_POINTLIST,
			D3D_PRIMITIVE_TOPOLOGY_LINELIST,
			D3D_PRIMITIVE_TOPOLOGY_LINESTRIP,
			D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
			D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
		};
		return Primitive[iPimitive];
	}
	//----------------------------------------------------------------------
	//	描画形状の取得
	//----------------------------------------------------------------------
	static INLINE s32 CalcIndexCount(s32 iPrimitive, s32 iCount)
	{
		s32 iIndexCount = 0;
		switch( iPrimitive )
		{
		case Command::EPrimitive::PointList:		iIndexCount = iCount;		break;
		case Command::EPrimitive::LineList:			iIndexCount = iCount * 2;	break;
		case Command::EPrimitive::LineStrip:		iIndexCount = iCount + 1;	break;
		case Command::EPrimitive::TriangleList:		iIndexCount = iCount * 3;	break;
		case Command::EPrimitive::TriangleStrip:	iIndexCount = iCount + 2;	break;
		}
		return iIndexCount;
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11Command::CDx11Command(kgl::TKGLPtr<CDx11Device> pDevice)
		: m_pDevice(pDevice)
		, m_pDeviceContext(m_pDevice->DeviceContext())
	{
		InitResource();
	}
	//----------------------------------------------------------------------
	//	リソースの初期化
	//----------------------------------------------------------------------
	void CDx11Command::InitResource(void)
	{
		m_pIndexBuffer = new CDx11DynamicIndexBuffer();
		m_pIndexBuffer->Initialize(m_pDevice);

		m_pVertexBuffer = new CDx11DynamicVertexBuffer();
		m_pVertexBuffer->Initialize(m_pDevice);
	}

	//----------------------------------------------------------------------
	//	コマンド情報の記録開始
	//----------------------------------------------------------------------
	void CDx11Command::SaveCommand(void)
	{
#if 0
		kgl::TKGLPtr<ID3D11DeviceContext> pDeviceContext;
		m_pDevice->GetDevice()->CreateDeferredContext(0, pDeviceContext.GetReference());
		m_pDeviceContext = pDeviceContext;
#endif // 0
	}
	//----------------------------------------------------------------------
	//	記録したコマンド情報の使用
	//----------------------------------------------------------------------
	void CDx11Command::UseCommand(void)
	{
#if 0
		kgl::TKGLPtr<ID3D11DeviceContext> pDeviceContext;
		pDeviceContext = m_pDevice->DeviceContext();

		kgl::TKGLPtr<ID3D11CommandList> pCommandList;
		m_pDeviceContext->FinishCommandList(true, pCommandList.GetReference());
		pDeviceContext->ExecuteCommandList(pCommandList, true);

		m_pDeviceContext = pDeviceContext;
#endif // 0
	}

	//----------------------------------------------------------------------
	//	描画開始
	//----------------------------------------------------------------------
	void CDx11Command::Begin(void)
	{
		ResetState();

		m_pBlendState = null;
		m_pDepthStencilState = null;
		m_pRasterState = null;
		for( auto& pSampler: m_pSamplerState )
		{
			pSampler = null;
		}
		m_ChangeState.Clear();

		if (m_pRenderTarget != m_pDevice->RenderTargetView() ||
			m_pDepthStencil != m_pDevice->DepthStencilView())
		{
			m_pRenderTarget = m_pDevice->RenderTargetView();
			m_pDepthStencil = m_pDevice->DepthStencilView();

			ID3D11RenderTargetView* pTarget = m_pRenderTarget;
			//	描画ターゲットの設定
			CurrentContext()->OMSetRenderTargets(1, &pTarget, m_pDepthStencil);
		}
		//	描画範囲の初期化
		Command::ScissoringInfo ScissorInfo;
		ScissorInfo.iX		= 0;
		ScissorInfo.iY		= 0;
		ScissorInfo.iWidth	= kgl::Config::ResolutionWidth;
		ScissorInfo.iHeight	= kgl::Config::ResolutionHeight;
		SetScissoring(ScissorInfo);
	}
	//----------------------------------------------------------------------
	//	描画終了
	//----------------------------------------------------------------------
	void CDx11Command::End(void)
	{
	}
	//----------------------------------------------------------------------
	//	コマンド情報のリセット
	//----------------------------------------------------------------------
	void CDx11Command::ResetState(void)
	{
		ICommandCreator::ResetState();
	}

	//----------------------------------------------------------------------
	//	描画リソースの適応
	//----------------------------------------------------------------------
	void CDx11Command::CommitGraphicState(void)
	{
		if( m_ChangeState.Check(EChangeStateBlend) )
		{
			kgl::TKGLPtr<ID3D11BlendState> pBlend;
			s32 iIndex = m_BlendStateList.GetIndex(m_BlendDesc);
			if( iIndex == kgl::algorithm::NotFind )
			{
				Dx11Device()->CreateBlendState(&m_BlendDesc, pBlend.GetReference());
				m_BlendStateList.AddValue(pBlend, m_BlendDesc);
			}
			else
			{
				pBlend = m_BlendStateList.GetValue(iIndex);
			}
			if( pBlend != m_pBlendState )
			{
				m_pBlendState = pBlend;
				CurrentContext()->OMSetBlendState(pBlend, 0, 0xffffffff);
			}
		}

		if( m_ChangeState.Check(EChangeStateDepthStencil) )
		{
			kgl::TKGLPtr<ID3D11DepthStencilState> pDepthStencil;
			s32 iIndex = m_DepthStencilList.GetIndex(m_DepthStencilDesc);
			if( iIndex == kgl::algorithm::NotFind )
			{
				Dx11Device()->CreateDepthStencilState(&m_DepthStencilDesc, pDepthStencil.GetReference());
				m_DepthStencilList.AddValue(pDepthStencil, m_DepthStencilDesc);
			}
			else
			{
				pDepthStencil = m_DepthStencilList.GetValue(iIndex);
			}
			if( pDepthStencil != m_pDepthStencilState )
			{
				m_pDepthStencilState = pDepthStencil;
				CurrentContext()->OMSetDepthStencilState(pDepthStencil, m_StencilTestInfo.ucRef);
			}
		}

		if( m_ChangeState.Check(EChangeStateRasterizer) )
		{
			kgl::TKGLPtr<ID3D11RasterizerState> pRasterState;
			s32 iIndex = m_RasterStateList.GetIndex(m_RasterizerDesc);
			if( iIndex == kgl::algorithm::NotFind )
			{
				Dx11Device()->CreateRasterizerState(&m_RasterizerDesc, pRasterState.GetReference());
				m_RasterStateList.AddValue(pRasterState, m_RasterizerDesc);
			}
			else
			{
				pRasterState = m_RasterStateList.GetValue(iIndex);
			}
			if( pRasterState != m_pRasterState )
			{
				m_pRasterState = pRasterState;
				CurrentContext()->RSSetState(pRasterState);
			}
		}

		if( m_ChangeState.Check(EChangeStateSampler) )
		{
			u32 uiSlot = 0xffffffff;
			u32 uiNums = 0;
			ID3D11SamplerState* pSamplers[TEXSTAGE_MAX] = { 0 };
			for( u32 i=0; i < TEXSTAGE_MAX; i++ )
			{
				D3D11_SAMPLER_DESC& desc = m_SamplerDesc[i];
				kgl::TKGLPtr<ID3D11SamplerState> pSampler;
				s32 iIndex = m_SamplerStateList.GetIndex(desc);
				if( iIndex == kgl::algorithm::NotFind )
				{
					Dx11Device()->CreateSamplerState(&desc, pSampler.GetReference());
					m_SamplerStateList.AddValue(pSampler, desc);
				}
				else
				{
					pSampler = m_SamplerStateList.GetValue(iIndex);
				}
				if( m_pSamplerState[i] != pSampler )
				{
					m_pSamplerState[i] = pSampler;
					if( uiSlot == 0xffffffff )
					{
						uiSlot = i;
					}
					uiNums = i - uiSlot + 1;
				}
				pSamplers[i] = pSampler;
			}
			if( uiSlot != 0xffffffff )
			{
				CurrentContext()->PSSetSamplers(uiSlot, uiNums, pSamplers);
			}
		}
		m_ChangeState.Clear();
	}
	//----------------------------------------------------------------------
	//	描画リソースの適応
	//----------------------------------------------------------------------
	b8 CDx11Command::CommitGraphicResource(s32 iPrimitive, s32 iCount, const void* pIndexes, s32 iVertexCount, const void* pVertexes, s32 iStride)
	{
		kgl::TKGLPtr<CDx11Declaration> pDecl = kgl::Cast<CDx11Declaration>(m_DeclarationInfo.pDeclaration);
		CDx11VertexShader* pShader = kgl::Cast<CDx11VertexShader>(m_VertexShaderInfo.pShader);
		if (!pDecl.IsValid() || pShader == null)
		{
			return false;
		}

		static ID3D11InputLayout* pPrevLayout = null;
		ID3D11InputLayout* pLayout = pDecl->GetLayout(pShader);
		if( pLayout != pPrevLayout )
		{
			CurrentContext()->IASetInputLayout(pLayout);
			pPrevLayout = pLayout;
		}
		if( pIndexes )
		{
			u32 uiSize = CalcIndexCount(iPrimitive, iCount) * sizeof(u16);
			void* pBuffer = m_pIndexBuffer->Lock(uiSize);
			kgMemcpy(pBuffer, pIndexes, uiSize);
			CDx11IndexBuffer* pIndex = m_pIndexBuffer->Unlock();

			Command::IndexBindInfo Info;
			Info.pIndex		= pIndex;
			Info.iOffset	= 0;
			SetIndex(Info);
		}
		if( pVertexes )
		{
			u32 uiSize = ((iVertexCount != 0)? iVertexCount: CalcIndexCount(iPrimitive, iCount)) * iStride;
			void* pBuffer = m_pVertexBuffer->Lock(uiSize);
			kgMemcpy(pBuffer, pVertexes, uiSize);
			CDx11VertexBuffer* pVertex = m_pVertexBuffer->Unlock();

			Command::VertexBindInfo Info;
			Info.iStream = 0;
			Info.pVertex = pVertex;
			Info.iOffset = 0;
			Info.iStride = iStride;
			SetVertex(Info);
		}
		return true;
	}
	//----------------------------------------------------------------------
	//	アルファテスト設定
	//----------------------------------------------------------------------
	void CDx11Command::SetAlphaTest(Command::AlphaTestInfo& Info)
	{
		UpdateInfo(AlphaTestInfo);
	}

	//----------------------------------------------------------------------
	//	デプステスト設定
	//----------------------------------------------------------------------
	void CDx11Command::SetDepthTest(Command::DepthTestInfo& Info)
	{
		const D3D11_COMPARISON_FUNC Func[] =
		{
			D3D11_COMPARISON_NEVER,
			D3D11_COMPARISON_NEVER,
			D3D11_COMPARISON_ALWAYS,
			D3D11_COMPARISON_LESS,
			D3D11_COMPARISON_LESS_EQUAL,
			D3D11_COMPARISON_GREATER,
			D3D11_COMPARISON_GREATER_EQUAL,
			D3D11_COMPARISON_EQUAL,
			D3D11_COMPARISON_NOT_EQUAL,
		};

		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	デプステストの設定
		if( CheckInfo(DepthTestInfo, iTest) ||
			CheckInfo(DepthTestInfo, bWrite) )
		{
			D3D11_DEPTH_STENCIL_DESC& desc = m_DepthStencilDesc;
			desc.DepthEnable					= Info.bWrite && bEnable;
			desc.DepthWriteMask					= D3D11_DEPTH_WRITE_MASK_ALL;
			desc.DepthFunc						= Func[Info.iTest];

			UpdateInfo(DepthTestInfo);
			m_ChangeState.Set(EChangeStateDepthStencil);
		}
	}

	//----------------------------------------------------------------------
	//	ステンシルテスト設定
	//----------------------------------------------------------------------
	void CDx11Command::SetStencilTest(Command::StencilTestInfo& Info)
	{
		const D3D11_COMPARISON_FUNC Func[] =
		{
			D3D11_COMPARISON_NEVER,
			D3D11_COMPARISON_NEVER,
			D3D11_COMPARISON_ALWAYS,
			D3D11_COMPARISON_LESS,
			D3D11_COMPARISON_LESS_EQUAL,
			D3D11_COMPARISON_GREATER,
			D3D11_COMPARISON_GREATER_EQUAL,
			D3D11_COMPARISON_EQUAL,
			D3D11_COMPARISON_NOT_EQUAL,
		};
		const D3D11_STENCIL_OP Op[] =
		{
			D3D11_STENCIL_OP_KEEP,
			D3D11_STENCIL_OP_ZERO,
			D3D11_STENCIL_OP_REPLACE,
			D3D11_STENCIL_OP_INCR_SAT,
			D3D11_STENCIL_OP_DECR_SAT,
			D3D11_STENCIL_OP_INVERT,
			D3D11_STENCIL_OP_INCR,
			D3D11_STENCIL_OP_DECR,
		};
		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	ステンシルテストの設定
		if( CheckInfo(StencilTestInfo, iTest) ||
			CheckInfo(StencilTestInfo, iTest) ||
			CheckInfo(StencilTestInfo, iPassOp) ||
			CheckInfo(StencilTestInfo, iFailOp) ||
			CheckInfo(StencilTestInfo, iDepthFailOp) ||
			CheckInfo(StencilTestInfo, ucRef) ||
			CheckInfo(StencilTestInfo, bWriteMask) )
		{
			D3D11_DEPTH_STENCIL_DESC& desc = m_DepthStencilDesc;
			if( bEnable )
			{
				desc.StencilEnable					= Info.bWriteMask;
				desc.StencilReadMask				= Info.ucMask;
				desc.StencilWriteMask				= Info.ucMask;
				desc.FrontFace.StencilFunc			= Func[Info.iTest];
				desc.FrontFace.StencilFailOp		= Op[Info.iFailOp];
				desc.FrontFace.StencilDepthFailOp	= Op[Info.iDepthFailOp];
				desc.FrontFace.StencilPassOp		= Op[Info.iPassOp];
				desc.BackFace						= desc.FrontFace;
			}
			else
			{
				desc.StencilEnable					= false;
				desc.StencilReadMask				= 0;
				desc.StencilWriteMask				= 0;
				desc.FrontFace.StencilFunc			= Func[Info.iTest];
				desc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_ZERO;
				desc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_ZERO;
				desc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_ZERO;
				desc.BackFace						= desc.FrontFace;
			}
			UpdateInfo(StencilTestInfo);
			m_ChangeState.Set(EChangeStateDepthStencil);
		}
	}

	//----------------------------------------------------------------------
	//	ブレンド設定
	//----------------------------------------------------------------------
	void CDx11Command::SetBlend(Command::BlendInfo& Info)
	{
		const D3D11_BLEND_OP Op[] =
		{
			D3D11_BLEND_OP_ADD,
			D3D11_BLEND_OP_ADD,
			D3D11_BLEND_OP_SUBTRACT,
			D3D11_BLEND_OP_REV_SUBTRACT,
			D3D11_BLEND_OP_MIN,
			D3D11_BLEND_OP_MAX,
		};
		const D3D11_BLEND Param[] =
		{
			D3D11_BLEND_ZERO,
			D3D11_BLEND_ONE,
			D3D11_BLEND_SRC_COLOR,
			D3D11_BLEND_INV_SRC_COLOR,
			D3D11_BLEND_DEST_COLOR,
			D3D11_BLEND_INV_DEST_COLOR,
			D3D11_BLEND_SRC_ALPHA,
			D3D11_BLEND_INV_SRC_ALPHA,
			D3D11_BLEND_DEST_ALPHA,
			D3D11_BLEND_INV_DEST_ALPHA,
		};

		u8 ucWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
		//	カラーブレンド設定
		b8 bEnableColorBlend = Info.iColorOp != Command::EBlendOp::Disable;
		if( !bEnableColorBlend )
		{
			Info.iColorOp	= Command::EBlendOp::Add;
			Info.iSrcColor	= Command::EBlendParam::One;
			Info.iDstColor	= Command::EBlendParam::One;
			ucWriteMask &= ~(D3D11_COLOR_WRITE_ENABLE_RED | D3D11_COLOR_WRITE_ENABLE_GREEN | D3D11_COLOR_WRITE_ENABLE_BLUE);
		}
		//	アルファブレンド設定
		b8 bEnableAlphaBlend = Info.iAlphaOp != Command::EBlendOp::Disable;
		if( !bEnableAlphaBlend )
		{
			Info.iAlphaOp	= Command::EBlendOp::Add;
			Info.iSrcAlpha	= Command::EBlendParam::One;
			Info.iDstAlpha	= Command::EBlendParam::One;
			ucWriteMask &= ~D3D11_COLOR_WRITE_ENABLE_ALPHA;
		}
		if( CheckInfo(BlendInfo, iColorOp) ||
			CheckInfo(BlendInfo, iSrcColor) ||
			CheckInfo(BlendInfo, iDstColor) ||
			CheckInfo(BlendInfo, iAlphaOp) ||
			CheckInfo(BlendInfo, iSrcAlpha) ||
			CheckInfo(BlendInfo, iDstAlpha) )
		{
			D3D11_BLEND_DESC& desc = m_BlendDesc;
			desc.AlphaToCoverageEnable					= false;
			desc.IndependentBlendEnable					= false;
			desc.RenderTarget[0].SrcBlend				= Param[Info.iSrcColor];
			desc.RenderTarget[0].SrcBlendAlpha			= Param[Info.iSrcAlpha];
			desc.RenderTarget[0].BlendOp				= Op[Info.iColorOp];
			desc.RenderTarget[0].BlendOpAlpha			= Op[Info.iAlphaOp];
			desc.RenderTarget[0].DestBlend				= Param[Info.iDstColor];
			desc.RenderTarget[0].DestBlendAlpha			= Param[Info.iDstAlpha];
			desc.RenderTarget[0].RenderTargetWriteMask	= ucWriteMask;
			desc.RenderTarget[0].BlendEnable			= ucWriteMask != 0;

			UpdateInfo(BlendInfo);
			m_ChangeState.Set(EChangeStateBlend);
		}
	}

	//----------------------------------------------------------------------
	//	カリングモード設定
	//----------------------------------------------------------------------
	void CDx11Command::SetCullMode(Command::CullModeInfo& Info)
	{
		const D3D11_CULL_MODE Mode[] =
		{
			D3D11_CULL_BACK,
			D3D11_CULL_FRONT,
			D3D11_CULL_NONE,
		};
		if( CheckInfo(CullModeInfo, iMode) )
		{
			D3D11_RASTERIZER_DESC& desc = m_RasterizerDesc;
			desc.FillMode				= D3D11_FILL_SOLID;
			desc.CullMode				= Mode[Info.iMode];
			desc.FrontCounterClockwise	= true;
			desc.DepthBias				= 0;
			desc.DepthBiasClamp			= 0;
			desc.SlopeScaledDepthBias	= 0;
			desc.DepthClipEnable		= false;
			desc.ScissorEnable			= true;
			desc.MultisampleEnable		= false;
			desc.AntialiasedLineEnable	= false;

			UpdateInfo(CullModeInfo);
			m_ChangeState.Set(EChangeStateRasterizer);
		}
	}

	//----------------------------------------------------------------------
	//	テクスチャ設定
	//----------------------------------------------------------------------
	void CDx11Command::SetTexture(Command::TextureInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");
		if( CheckInfo(TextureInfo[Info.iStage], pTexture) )
		{
			ITextureBase* pTexture = Info.pTexture;
			ID3D11ShaderResourceView* pResource = (ID3D11ShaderResourceView*)pTexture->GetTexture();
			CurrentContext()->PSSetShaderResources(Info.iStage, 1, &pResource);

			UpdateInfo(TextureInfo[Info.iStage]);
		}
	}

	//----------------------------------------------------------------------
	//	LODバイアス設定
	//----------------------------------------------------------------------
	void CDx11Command::SetLODBias(Command::LODBiasInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");

		UpdateInfo(LODBiasInfo[Info.iStage]);
	}

	//----------------------------------------------------------------------
	//	サンプラーステート設定
	//----------------------------------------------------------------------
	void CDx11Command::SetSamplerState(Command::SamplerStateInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");

		const  D3D11_TEXTURE_ADDRESS_MODE Wrap[] =
		{
			D3D11_TEXTURE_ADDRESS_WRAP,
			D3D11_TEXTURE_ADDRESS_MIRROR,
			D3D11_TEXTURE_ADDRESS_CLAMP,
			D3D11_TEXTURE_ADDRESS_BORDER,
		};

		if( CheckInfo(SamplerStateInfo[Info.iStage], iFilterMin) ||
			CheckInfo(SamplerStateInfo[Info.iStage], iFilterMag) ||
			CheckInfo(SamplerStateInfo[Info.iStage], iFilterMip) ||
			CheckInfo(SamplerStateInfo[Info.iStage], iWrapU) ||
			CheckInfo(SamplerStateInfo[Info.iStage], iWrapV) )
		{
			D3D11_SAMPLER_DESC& desc = m_SamplerDesc[Info.iStage];
			desc.Filter			= D3D11_FILTER_MIN_MAG_MIP_POINT;
			desc.AddressU		= Wrap[Info.iWrapU];
			desc.AddressV		= Wrap[Info.iWrapV];
			desc.AddressW		= D3D11_TEXTURE_ADDRESS_WRAP;
			desc.MipLODBias		= 0.0f;
			desc.MinLOD			= 0.0f;
			desc.MaxLOD			= D3D11_FLOAT32_MAX;
			desc.MaxAnisotropy	= 0;
			desc.ComparisonFunc	= D3D11_COMPARISON_NEVER;
			desc.BorderColor[0]	= 0.0f;
			desc.BorderColor[1]	= 0.0f;
			desc.BorderColor[2]	= 0.0f;
			desc.BorderColor[3]	= 0.0f;

			UpdateInfo(SamplerStateInfo[Info.iStage]);
			m_ChangeState.Set(EChangeStateSampler);
		}
	}

	//----------------------------------------------------------------------
	//	ビューポート設定
	//----------------------------------------------------------------------
	void CDx11Command::SetViewPort(Command::ViewPortInfo& Info)
	{
		if( CheckInfo(ViewPortInfo, iX) ||
			CheckInfo(ViewPortInfo, iY) ||
			CheckInfo(ViewPortInfo, iWidth) ||
			CheckInfo(ViewPortInfo, iHeight) )
		{
			D3D11_VIEWPORT ViewPort;
			ViewPort.TopLeftX	= (f32)Info.iX;
			ViewPort.TopLeftY	= (f32)Info.iY;
			ViewPort.Width		= (f32)Info.iWidth;
			ViewPort.Height		= (f32)Info.iHeight;
			ViewPort.MinDepth	= Info.fMinZ;
			ViewPort.MaxDepth	= Info.fMaxZ;
			CurrentContext()->RSSetViewports(1, &ViewPort);

			UpdateInfo(ViewPortInfo);
		}
	}

	static ID3D11RenderTargetView* temp[4] = { null };
	//----------------------------------------------------------------------
	//	レンダーターゲット設定
	//----------------------------------------------------------------------
	void CDx11Command::SetRenderTarget(Command::RenderTargetInfo& Info)
	{
		kgAssert(Info.iIndex < RENDER_STAGE_MAX, "Indexに指定できる数はRENDER_STAGE_MAXまでです");

		if( CheckInfo(RenderTargetInfo[Info.iIndex], pTarget) )
		{
			ID3D11RenderTargetView* pTarget = null;
			if( Info.pTarget )
			{
				pTarget = (ID3D11RenderTargetView*)Info.pTarget->GetSurface();
				m_pRenderSurface = Info.pTarget;
			}
			else if( Info.iIndex == 0 )
			{
				pTarget = m_pDevice->RenderTargetView();
				m_pRenderSurface = null;
			}
			temp[Info.iIndex] = pTarget;

			CurrentContext()->OMSetRenderTargets(4, temp, m_pDepthStencil);
			m_pRenderTarget = pTarget;

			UpdateInfo(RenderTargetInfo[Info.iIndex]);
		}
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット設定
	//----------------------------------------------------------------------
	void CDx11Command::SetDepthTarget(Command::DepthTargetInfo& Info)
	{
		if( CheckInfo(DepthTargetInfo, pDepthTarget) )
		{
			ID3D11DepthStencilView* pTarget = null;
			if( Info.pDepthTarget )
			{
				pTarget = (ID3D11DepthStencilView*)Info.pDepthTarget->GetSurface();
				m_pDepthSurface = Info.pDepthTarget;
			}
			else
			{
				pTarget = m_pDevice->DepthStencilView();
				m_pDepthSurface = null;
			}

			CurrentContext()->OMSetRenderTargets(4, temp, pTarget);
			m_pDepthStencil = pTarget;

			UpdateInfo(DepthTargetInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点フォーマット設定
	//----------------------------------------------------------------------
	void CDx11Command::SetDeclaration(Command::DeclarationInfo& Info)
	{
		if( CheckInfo(DeclarationInfo, pDeclaration) )
		{
			UpdateInfo(DeclarationInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点設定
	//----------------------------------------------------------------------
	void CDx11Command::SetVertex(Command::VertexBindInfo& Info)
	{
		kgAssert(Info.iStream < VERTEX_STREAM_MAX, "Streamに指定できる数はVERTEX_STREAM_MAXまでです");

		if( CheckInfo(VertexBindInfo[Info.iStream], iOffset) ||
			CheckInfo(VertexBindInfo[Info.iStream], iStride) ||
			CheckInfo(VertexBindInfo[Info.iStream], pVertex) )
		{
			IVertexBuffer* pVertex = Info.pVertex;

			ID3D11Buffer* pBuffer = (ID3D11Buffer*)pVertex->GetBuffer();
			u32 uiStride = (u32)Info.iStride;
			u32 uiOffset = (u32)Info.iOffset;
			CurrentContext()->IASetVertexBuffers(Info.iStream, 1, &pBuffer, &uiStride, &uiOffset);

			UpdateInfo(VertexBindInfo[Info.iStream]);
		}
	}

	//----------------------------------------------------------------------
	//	インデックス設定
	//----------------------------------------------------------------------
	void CDx11Command::SetIndex(Command::IndexBindInfo& Info)
	{
		if( CheckInfo(IndexBindInfo, pIndex) )
		{
			IIndexBuffer* pIndex = Info.pIndex;

			DXGI_FORMAT Format[] =
			{
				DXGI_FORMAT_R16_UINT,
				DXGI_FORMAT_R32_UINT
			};
			CurrentContext()->IASetIndexBuffer((ID3D11Buffer*)pIndex->GetBuffer(), Format[pIndex->GetType()], Info.iOffset);

			UpdateInfo(IndexBindInfo);
		}
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CDx11Command::DrawPrimitiveUP(Command::DrawPrimitiveInfo& Info)
	{
		if( !CommitGraphicResource(Info.iPrimitive, Info.iCount, null, 0, Info.pVertex, Info.iStride) )
		{
			return;
		}
		CommitGraphicState();
		u32 uiCount = CalcIndexCount(Info.iPrimitive, Info.iCount);

		CurrentContext()->IASetPrimitiveTopology(PrimitiveTeplogy(Info.iPrimitive));
		CurrentContext()->Draw(uiCount, 0);
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CDx11Command::DrawPrimitive(Command::DrawInfo& Info)
	{
		if( !CommitGraphicResource() )
		{
			return;
		}
		CommitGraphicState();
		u32 uiCount = CalcIndexCount(Info.iPrimitive, Info.iCount);

		CurrentContext()->IASetPrimitiveTopology(PrimitiveTeplogy(Info.iPrimitive));
		CurrentContext()->Draw(uiCount, Info.iVertexOffset);
	}
	//----------------------------------------------------------------------
	//	描画(インデックス有り)
	//----------------------------------------------------------------------
	void CDx11Command::DrawIndexedPrimitiveUP(Command::DrawIndexPrimitiveInfo& Info)
	{
		if( !CommitGraphicResource(Info.iPrimitive, Info.iCount, Info.pIndex, Info.iVertexCount, Info.pVertex, Info.iStride) )
		{
			return;
		}
		CommitGraphicState();
		u32 uiCount = CalcIndexCount(Info.iPrimitive, Info.iCount);

		CurrentContext()->IASetPrimitiveTopology(PrimitiveTeplogy(Info.iPrimitive));
		CurrentContext()->DrawIndexed(uiCount, 0, 0);
	}
	//----------------------------------------------------------------------
	//	描画(インデックス有り)
	//----------------------------------------------------------------------
	void CDx11Command::DrawIndexedPrimitive(Command::DrawIndexInfo& Info)
	{
		if( !CommitGraphicResource() )
		{
			return;
		}
		CommitGraphicState();
		u32 uiCount = CalcIndexCount(Info.iPrimitive, Info.iCount);

		CurrentContext()->IASetPrimitiveTopology(PrimitiveTeplogy(Info.iPrimitive));
		CurrentContext()->DrawIndexed(uiCount, Info.iIndexOffset, Info.iVertexOffset);
	}

	//----------------------------------------------------------------------
	//	頂点シェーダ設定
	//----------------------------------------------------------------------
	void CDx11Command::SetVertexShader(Command::VertexShaderInfo& Info)
	{
		if( CheckInfo(VertexShaderInfo, pShader) )
		{
			IVertexShader* pVertexShader = Info.pShader;
			CurrentContext()->VSSetShader((ID3D11VertexShader*)pVertexShader->GetShader(), null, 0);

			UpdateInfo(VertexShaderInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点シェーダパラメータ設定
	//----------------------------------------------------------------------
	void CDx11Command::SetVertexShaderParam(Command::VertexShaderParamInfo& Info)
	{
		switch( Info.iType )
		{
		case Command::EShaderParam::Constant:
			CurrentContext()->VSSetConstantBuffers(Info.iRegister, 1, null);
			break;

		default:
			kgAssert(false, "不明なパラメータが設定されています(CDx11Command::SetVertexShaderParam)");
			break;
		}
	}

	//----------------------------------------------------------------------
	//	頂点シェーダ設定
	//----------------------------------------------------------------------
	void CDx11Command::SetPixelShader(Command::PixelShaderInfo& Info)
	{
		if( CheckInfo(PixelShaderInfo, pShader) )
		{
			IPixelShader* pPixcelShader = Info.pShader;
			CurrentContext()->PSSetShader((ID3D11PixelShader*)pPixcelShader->GetShader(), null, 0);

			UpdateInfo(PixelShaderInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点シェーダパラメータ設定
	//----------------------------------------------------------------------
	void CDx11Command::SetPixelShaderParam(Command::PixelShaderParamInfo& Info)
	{
		switch( Info.iType )
		{
		case Command::EShaderParam::Constant:
			CurrentContext()->PSSetConstantBuffers(Info.iRegister, 1, null);
			break;

		default:
			kgAssert(false, "不明なパラメータが設定されています(CDx11Command::SetPixelShaderParam)");
			break;
		}
	}

	//----------------------------------------------------------------------
	//	ターゲットのクリア
	//----------------------------------------------------------------------
	void CDx11Command::ClearTarget(Command::ClearTargetInfo& Info)
	{
		u32 uiFlag = 0;
		uiFlag |= (Info.iTarget & Command::EClearTarget::Depth)?   D3D11_CLEAR_DEPTH: 0;
		uiFlag |= (Info.iTarget & Command::EClearTarget::Stencil)? D3D11_CLEAR_STENCIL: 0;

		if( Info.iTarget & Command::EClearTarget::Color )
		{
			kgl::FColor Color(Info.uiColor);
			for( auto pTarget:temp )
			{
				if( pTarget )
				{
					CurrentContext()->ClearRenderTargetView(pTarget, Color.c);
				}
			}
		}
		if( uiFlag != 0 )
		{
			CurrentContext()->ClearDepthStencilView(m_pDepthStencil, uiFlag, Info.fDepth, (u8)Info.iStencil);
		}
	}
	//----------------------------------------------------------------------
	//	シザリング設定
	//----------------------------------------------------------------------
	void CDx11Command::SetScissoring(Command::ScissoringInfo& Info)
	{
		if( CheckInfo(ScissoringInfo, iX) ||
			CheckInfo(ScissoringInfo, iY) ||
			CheckInfo(ScissoringInfo, iWidth) ||
			CheckInfo(ScissoringInfo, iHeight) )
		{
			RECT rect;
			rect.left	= Info.iX;
			rect.top	= Info.iY;
			rect.right	= Info.iX + Info.iWidth;
			rect.bottom	= Info.iY + Info.iHeight;
			
			CurrentContext()->RSSetScissorRects(1, &rect);
			UpdateInfo(ScissoringInfo);
		}
	}
	//----------------------------------------------------------------------
	//	ビュー行列設定
	//----------------------------------------------------------------------
	void CDx11Command::SetView(Command::ViewInfo& Info)
	{
		const u32 Register = 0;
		kgl::Matrix mView;
		kgl::Math::MatrixInverse(mView, *Info.pView);

		Command::PixelShaderParamInfo ParamInfo;
		ParamInfo.iCount = 1;
		ParamInfo.iRegister = Register;
		ParamInfo.iType = Command::EShaderParam::Matrix;
		ParamInfo.pParam = &mView;
		SetPixelShaderParam(ParamInfo);
	}
	//----------------------------------------------------------------------
	//	プロジェクション行列設定
	//----------------------------------------------------------------------
	void CDx11Command::SetProjection(Command::ProjectionInfo& Info)
	{
		const u32 Register = 4;
		kgl::Matrix mProjection;
		kgl::Math::MatrixInverse(mProjection, *Info.pProjection);

		Command::PixelShaderParamInfo ParamInfo;
		ParamInfo.iCount = 1;
		ParamInfo.iRegister = Register;
		ParamInfo.iType = Command::EShaderParam::Matrix;
		ParamInfo.pParam = &mProjection;
		SetPixelShaderParam(ParamInfo);
	}
	//----------------------------------------------------------------------
	//	ワールド行列設定
	//----------------------------------------------------------------------
	void CDx11Command::SetWorld(Command::WorldInfo& Info)
	{
		u32 Register = 8;

		kgl::Matrix mWorld;
		for( s32 i=0; i < Info.iCount; i++ )
		{
			kgl::Math::MatrixInverse(mWorld, *(Info.pWorld + i));

			Command::PixelShaderParamInfo ParamInfo;
			ParamInfo.iCount = 1;
			ParamInfo.iRegister = Register;
			ParamInfo.iType = Command::EShaderParam::Matrix;
			ParamInfo.pParam = &mWorld;
			SetPixelShaderParam(ParamInfo);
			Register += 4;
		}
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
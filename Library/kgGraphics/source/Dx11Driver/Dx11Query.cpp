//----------------------------------------------------------------------
//!
//!	@file	Dx11Query.cpp
//!	@brief	クエリ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Query.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	クエリ生成
	//----------------------------------------------------------------------
	b8 CDx11Query::Create(CDx11Device* pDevice, const QueryInfo& Info)
	{
		m_pDevice = pDevice;
		m_Info = Info;

		const D3D11_QUERY Query[] =
		{
			D3D11_QUERY_EVENT,
			D3D11_QUERY_OCCLUSION,
			D3D11_QUERY_TIMESTAMP,
			D3D11_QUERY_TIMESTAMP_DISJOINT,
		};

		D3D11_QUERY_DESC desc;
		desc.MiscFlags	= 0;
		desc.Query		= Query[Info.iQuery];

		HRESULT hr;
		hr = pDevice->GetDevice()->CreateQuery(&desc, m_pQuery.GetReference());
		if( FAILED(hr) )
		{
			ErrorTrace("クエリの生成に失敗しました(CDx11Query::Create)");
			ErrorTrace("Usage:%d", Info.iQuery);
			return false;
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	クエリ発行
	//----------------------------------------------------------------------
	void CDx11Query::Begin(void)
	{
		switch( m_Info.iQuery )
		{
		case EQuery::Event:
		case EQuery::TimeStamp:
			return;
		}
		m_pDevice->DeviceContext()->Begin(m_pQuery);
	}
	//----------------------------------------------------------------------
	//	発行したクエリの終了
	//----------------------------------------------------------------------
	void CDx11Query::End(void)
	{
		m_pDevice->DeviceContext()->End(m_pQuery);
	}
	//----------------------------------------------------------------------
	//	データの取得
	//----------------------------------------------------------------------
	b8 CDx11Query::GetData(void* pData, u32 uiSize)
	{
		HRESULT hr;
		hr = m_pDevice->DeviceContext()->GetData(m_pQuery, pData, uiSize, 0);
		return SUCCEEDED(hr);
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx11Surface.cpp
//!	@brief	サーフェイス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Surface.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	サーフェイスのロック
	//----------------------------------------------------------------------
	void* CDx11Surface::Lock(s32 iType)
	{
		D3D11_MAP MapType[] =
		{
			D3D11_MAP_READ,
			D3D11_MAP_WRITE,
			D3D11_MAP_READ_WRITE,
		};
		D3D11_MAPPED_SUBRESOURCE resource;

		u32 uiSubResource = D3D11CalcSubresource(m_iLevel, 0, m_iMipLevels);

		VerifyResult(m_pContext->Map(m_pSurface, uiSubResource , MapType[iType], 0, &resource));
		return resource.pData;
	}
	//----------------------------------------------------------------------
	//	サーフェイスのアンロック
	//----------------------------------------------------------------------
	void CDx11Surface::Unlock(void)
	{
		u32 uiSubResource = D3D11CalcSubresource(m_iLevel, 0, m_iMipLevels);

		m_pContext->Unmap(m_pSurface, uiSubResource);
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx11Device.h
//!	@brief	DirectX11
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Device.h"
#include "Dx11Driver/Dx11Surface.h"
#include "Dx11Driver/Dx11Query.h"
#include "Dx11Driver/Dx11SwapChain.h"

namespace kgGraphics
{

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11Device::CDx11Device(void)
	{}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CDx11Device::~CDx11Device(void)
	{
		Finalize();
	}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx11Device::Initialize(const GRAPHICS_INFO& info)
	{
		kgAssert(!m_pDevice.IsValid(), "既にCDx11Deviceは初期化されています(CDx11Device::Initialize)");

		u32 uiCreateDeviceFlags = 0;

#if	KGL_DEBUG
		//	処理負荷が掛かりすぎるので必要な時だけ使用すること(PIXの使用など)
		//uiCreateDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif	// ~#if	KGL_DEBUG
		
		//	ドライバータイプ
		D3D_DRIVER_TYPE eDriverTypes[] =
		{
			D3D_DRIVER_TYPE_HARDWARE,
			//D3D_DRIVER_TYPE_WARP,
			//D3D_DRIVER_TYPE_REFERENCE,
		};
		u32 uiNumDriverTypes = kgl::ArrayCount(eDriverTypes);

		//	機能レベル
		D3D_FEATURE_LEVEL eFeatureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			//D3D_FEATURE_LEVEL_10_1,
			//D3D_FEATURE_LEVEL_10_0,
		};
		u32 uiNumFeatureLevels = kgl::ArrayCount(eFeatureLevels);

		m_bVsync = info.bUseVSync? true: false;

		HRESULT hr = ERROR_SEVERITY_ERROR;
		//	デバイスとスワップチェインを作成
		for( u32 i=0; i < uiNumDriverTypes; i++ )
		{
			m_eType = eDriverTypes[i];
			hr = D3D11CreateDevice(
				null,
				m_eType,
				null,
				uiCreateDeviceFlags,
				eFeatureLevels,
				uiNumFeatureLevels,
				D3D11_SDK_VERSION,
				m_pDevice.GetReference(),
				&m_eFeatureLevel,
				m_pDeviceContext.GetReference());
		
			if( SUCCEEDED(hr) )
			{
				break;
			}
		}
		if( FAILED(hr) )
		{
			ErrorTrace("DirectX11のデバイス生成に失敗しました(CDx11Device::Initialize)");
			return false;
		}

		kgl::TKGLPtr<IDXGIDevice1> pDXGIDevice;
		kgl::TKGLPtr<IDXGIAdapter> pDXGIAdapter;
		if( SUCCEEDED(m_pDevice->QueryInterface(__uuidof(IDXGIDevice1), pDXGIDevice.GetComReference())) )
		{
			if( SUCCEEDED(pDXGIDevice->GetAdapter(pDXGIAdapter.GetReference())) )
			{
				pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), m_pFactory.GetComReference());
			}
		}
		if( !m_pFactory.IsValid() )
		{
			ErrorTrace("IDXGIFactoryの取得に失敗しました(CDx11Device::Initialize)");
			return false;
		}
		kgl::TKGLPtr<ISwapChainBase> pSwapChain;
		if( !CreateSwapChain(pSwapChain.GetReference(), info.pWindow) )
		{
			ErrorTrace("IDXGIFactoryの取得に失敗しました(CDx11Device::Initialize)");
			return false;
		}
		m_pSwapChain = kgl::Cast<CDx11SwapChain, ISwapChainBase>(pSwapChain);

		{
			kgl::TKGLPtr<IDXGIOutput> pOutput;
			HRESULT hr;
			kgl::TKGLPtr<kgGraphics::ISurfaceBase> pSurface;
			m_pSwapChain->GetSurface(pSurface.GetReference());
			ID3D11View* pView = (ID3D11View*)pSurface->GetSurface();
			kgl::TKGLPtr<ID3D11Resource> pResource;
			pView->GetResource(pResource.GetReference());
			D3D11_TEXTURE2D_DESC desc;
			((ID3D11Texture2D*)pResource.Get())->GetDesc(&desc);

			hr = pDXGIAdapter->EnumOutputs(0, pOutput.GetReference());
			UINT numModes = 0;
			DXGI_FORMAT format = desc.Format;
			kgl::TSmartPtr<DXGI_MODE_DESC> displayModes;
			// Get the number of elements
			hr = pOutput->GetDisplayModeList( format, 0, &numModes, NULL);

			displayModes = new DXGI_MODE_DESC[numModes];
			// Get the list
			hr = pOutput->GetDisplayModeList(format, 0, &numModes, displayModes);
			if( FAILED(hr) )
			{
				ErrorTrace("Display Mode Count Error");
			}
			else
			{
				Trace("================================================");
				for( int i=0; ; i++ )
				{
					DISPLAY_DEVICE display = {0};
					display.cb = sizeof(display);
					if( !EnumDisplayDevices(NULL, i, &display, 0) )
					{
						break;
					}

					if( !(display.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE) )
					{
						DEVMODE mode = {0};
						mode.dmSize = sizeof(DEVMODE);
						if( EnumDisplaySettings(display.DeviceName, ENUM_CURRENT_SETTINGS, &mode) )
						{
							Trace("Display Resolution:%d x %d", mode.dmPelsWidth, mode.dmPelsHeight);
						}
						break;
					}
				}
				struct DeviceResolution
				{
					UINT	width;
					UINT	height;
					bool	fullscreen;

					bool operator == (const DeviceResolution& r) const
					{
						return width == r.width && height == r.height;
					}
					bool operator < (const DeviceResolution& r) const
					{
						if( width == r.width )
						{
							return height < r.height;
						}
						return width < r.width;
					}
				};
				kgl::algorithm::vector<DeviceResolution> resolutions;

				const float base_aspect = 16.0f / 9.0f;
				Trace("Display Mode Count:%d", numModes);
				for( u32 i=0; i < numModes; i++ )
				{
					auto& mode = displayModes[i];

					const float aspect = (f32)mode.Width / (f32)mode.Height;
					if( base_aspect || aspect )
					{
						DeviceResolution res;
						res.width = mode.Width;
						res.height = mode.Height;
						res.fullscreen = true;
						if( resolutions.find(res) == kgl::algorithm::NotFind )
						{
							resolutions.push_back(res);
						}
					}
					{
						Trace("");
						Trace("=== Display Mode:%d x %d(%f) ===", mode.Width, mode.Height, (f32)mode.Width / (f32)mode.Height);
						Trace("Display Format:%d", mode.Format);
						Trace("RefreshRate %d, Numerator:%d, Denominator:%d", mode.RefreshRate.Numerator / mode.RefreshRate.Denominator, mode.RefreshRate.Numerator, mode.RefreshRate.Denominator);
						Trace("ScanlineOrdering:%d Scaling::%d", mode.ScanlineOrdering, mode.Scaling);
					}
				}

				Trace("================================================");
				Trace("=========resolution===============");
				for( auto res:resolutions )
				{
					const float aspect = (f32)res.width / (f32)res.height;
					Trace("=== resolution:%d x %d(%f) ===", res.width, res.height, aspect);
				}
				Trace("================================================");
				Trace("================================================");
			}

			DXGI_MODE_DESC descMode;
			DXGI_MODE_DESC descDesiredMode =
			{
				1600, 900,
				{ 60, 1 },
				desc.Format,
				DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED,
				DXGI_MODE_SCALING_UNSPECIFIED,
			};
			hr = pOutput->FindClosestMatchingMode(&descDesiredMode, &descMode, m_pDevice);
			{
				Trace("=== FindClosestMatchingMode ===");
				Trace("=== Display Mode:%d x %d(%f) ===", descMode.Width, descMode.Height, (f32)descMode.Width / (f32)descMode.Height);
				Trace("Display Format:%d", descMode.Format);
				Trace("RefreshRate Numerator:%d, Denominator:%d", descMode.RefreshRate.Numerator, descMode.RefreshRate.Denominator);
				Trace("ScanlineOrdering:%d Scaling::%d", descMode.ScanlineOrdering, descMode.Scaling);
			}
		}

		// Alt+Enterによるフルスクリーン機能の抑制
		m_pFactory->MakeWindowAssociation((HWND)info.pWindow->GetHandle(), DXGI_MWA_NO_ALT_ENTER);

		kgl::TKGLPtr<ISurfaceBase> pBuffer;
		m_pSwapChain->GetSurface(pBuffer.GetReference());
		m_pRenderTargetView = (ID3D11RenderTargetView*)pBuffer->GetSurface();

		DXGI_SWAP_CHAIN_DESC sd;
		m_pSwapChain->GetSwapChain()->GetDesc(&sd);
		// 深度バッファの作成
		D3D11_TEXTURE2D_DESC depthDesc;
		depthDesc.Width					= info.iWidth;
		depthDesc.Height				= info.iHeight;
		depthDesc.MipLevels				= 1;
		depthDesc.ArraySize				= 1;
		depthDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthDesc.SampleDesc			= sd.SampleDesc;
		depthDesc.Usage					= D3D11_USAGE_DEFAULT;
		depthDesc.BindFlags				= D3D11_BIND_DEPTH_STENCIL;
		depthDesc.CPUAccessFlags		= 0;
		depthDesc.MiscFlags				= 0;

		m_pDevice->CreateTexture2D(&depthDesc, null, m_pDepthStencil.GetReference());
		if( FAILED(m_pDevice->CreateDepthStencilView(m_pDepthStencil, null, m_pDepthStencilView.GetReference())) )
		{
			ErrorTrace("深度バッファの生成に失敗しました(CDx11Device::Initialize)");
			return false;
		}
		m_pWindow = info.pWindow;

		return true;
	}
	//----------------------------------------------------------------------
	//	終了処理
	//----------------------------------------------------------------------
	void CDx11Device::Finalize(void)
	{
		if( m_pSwapChain )
		{
			// フルスクリーンのまま終了すると画面設定が壊れるので元に戻しておく(DirectXの仕様)
			m_pSwapChain->GetSwapChain()->SetFullscreenState(false, null);
		}
		if( m_pDeviceContext.IsValid() )
		{
			m_pDeviceContext->ClearState();
		}

		m_pDepthStencil		= null;
		m_pDepthStencilView	= null;
		m_pRenderTargetView	= null;
		m_pSwapChain		= null;
		m_pDeviceContext	= null;
		m_pDevice			= null;
	}
	//----------------------------------------------------------------------
	//	描画シーン開始
	//----------------------------------------------------------------------
	void CDx11Device::BeginScene(void)
	{}
	//----------------------------------------------------------------------
	//	描画シーン終了
	//----------------------------------------------------------------------
	void CDx11Device::EndScene(void)
	{
		m_pSwapChain->Present(!!m_bVsync);
	}
	//----------------------------------------------------------------------
	//	GPUのコマンドを実行する
	//----------------------------------------------------------------------
	void CDx11Device::FlushCommand(void)
	{
		if( m_pDeviceContext )
		{
			m_pDeviceContext->Flush();
		}
	}
	//----------------------------------------------------------------------
	//	GPUの処理終了待ち
	//----------------------------------------------------------------------
	void CDx11Device::WaitForCommand(void)
	{
		if( m_pDeviceContext )
		{
			m_pDeviceContext->Flush();

			QueryInfo Info;
			Info.iQuery = EQuery::Event;
			kgl::TKGLPtr<IQueryBase> pQuery;
			if( CreateQuery(pQuery.GetReference(), Info) )
			{
				pQuery->End();
				BOOL data = 0;
				while( pQuery->GetData(&data, sizeof(data)) && !data )
				{
					kgSleep(0);
				}
			}
		}
	}
	//----------------------------------------------------------------------
	//	バックバッファの取得
	//----------------------------------------------------------------------
	void CDx11Device::GetBackBuffer(ISurfaceBase** ppBackBuffer)
	{
		kgl::TKGLPtr<ID3D11Texture2D> pBuffer;
		m_pRenderTargetView->GetResource((ID3D11Resource**)pBuffer.GetReference());

		D3D11_TEXTURE2D_DESC desc;
		pBuffer->GetDesc(&desc);

		kgl::TKGLPtr<CDx11Surface> pSurface = new CDx11Surface;
		SurfaceInfo Info;
		Info.iFormat	= ETextureFormat::A8R8G8B8;
		Info.iUsage		= ETextureUsage::RenderTarget;
		Info.uiWidth	= desc.Width;
		Info.uiHeight	= desc.Height;
		pSurface->Setup(this, Info, pBuffer, m_pRenderTargetView);

		pSurface->AddRef();
		*ppBackBuffer = pSurface;
	}
	//----------------------------------------------------------------------
	//	デプスバッファの取得
	//----------------------------------------------------------------------
	void CDx11Device::GetDepthBuffer(ISurfaceBase** ppDepthBuffer)
	{
		D3D11_TEXTURE2D_DESC desc;
		m_pDepthStencil->GetDesc(&desc);

		kgl::TKGLPtr<CDx11Surface> pSurface = new CDx11Surface;
		SurfaceInfo Info;
		Info.iFormat	= ETextureFormat::D24S8;
		Info.iUsage		= ETextureUsage::Depth;
		Info.uiWidth	= desc.Width;
		Info.uiHeight	= desc.Height;
		pSurface->Setup(this, Info, m_pDepthStencil, m_pDepthStencilView);

		pSurface->AddRef();
		*ppDepthBuffer = pSurface;
	}
	//----------------------------------------------------------------------
	//	VSyncの有無を設定
	//----------------------------------------------------------------------
	void CDx11Device::SetVSync(b8 bEnable)
	{
		m_bVsync = bEnable ? true : false;
	}

	//----------------------------------------------------------------------
	//	クエリー生成
	//----------------------------------------------------------------------
	b8 CDx11Device::CreateQuery(IQueryBase** ppQuery, const QueryInfo& Info)
	{
		kgl::TKGLPtr<CDx11Query> pQuery = new CDx11Query;
		if( !pQuery->Create(this, Info) )
		{
			return false;
		}
		pQuery->AddRef();
		*ppQuery = pQuery;
		return true;
	}
	//----------------------------------------------------------------------
	//	スワップチェインの生成
	//----------------------------------------------------------------------
	b8 CDx11Device::CreateSwapChain(ISwapChainBase** ppSwapChain, kgl::Window::IWindow* pWindow)
	{
		kgl::TKGLPtr<CDx11SwapChain> pSwapChain;
		pSwapChain = new CDx11SwapChain;
		if( !pSwapChain->Create(this, pWindow) )
		{
			return false;
		}
		pSwapChain->AddRef();
		*ppSwapChain = pSwapChain;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
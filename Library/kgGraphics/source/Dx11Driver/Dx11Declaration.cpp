//----------------------------------------------------------------------
//!
//!	@file	Dx11IndexBuffer.cpp
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Declaration.h"
#include "../../../kgl/include/Utility/algorithm/kgl.find.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx11Declaration::CDx11Declaration(void)
		: m_pLayout(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx11Declaration::Initialize(CDx11Device* pDevice, const DeclarationInfo* pDecl)
	{
		m_pDevice = pDevice;

		s32 iCount = 0;
		//	KGG_DECL_END()までの数を算出
		while( (pDecl + iCount)->usStream != 0xffff )
		{
			iCount ++;
			kgAssert(iCount < 100, "Declationの設定でKGG_DECL_END()が指定されておりません(CDx11Declaration::Initialize)");
		}
		m_iCount = iCount;

		//	頂点タイプ
		const DXGI_FORMAT DeclType[] =
		{
			DXGI_FORMAT_R32_FLOAT,
			DXGI_FORMAT_R32G32_FLOAT,
			DXGI_FORMAT_R32G32B32_FLOAT,
			DXGI_FORMAT_R32G32B32A32_FLOAT,
			DXGI_FORMAT_B8G8R8A8_UNORM,
			DXGI_FORMAT_R8G8B8A8_UINT,
			DXGI_FORMAT_R16G16_SINT,
			DXGI_FORMAT_R16G16B16A16_SINT,
			DXGI_FORMAT_R8G8B8A8_UNORM,
			DXGI_FORMAT_R16G16_SNORM,
			DXGI_FORMAT_R16G16B16A16_SNORM,
			DXGI_FORMAT_R16G16_UINT,
			DXGI_FORMAT_R16G16B16A16_UINT,
			DXGI_FORMAT_R16G16_FLOAT,
			DXGI_FORMAT_R16G16B16A16_FLOAT,
		};
		const c8* DeclUsage[] =
		{
			"POSITION",
			"BLENDWEIGHT",
			"BLENDINDICES",
			"NORMAL",
			"PSIZE",
			"TEXCOORD",
			"TANGENT",
			"BINORMAL",
			"TESSFACTOR",
			"POSITIONT",
			"COLOR",
			"FOG",
			"DEPTH",
		};

		m_pLayout = new D3D11_INPUT_ELEMENT_DESC[iCount];
		//	頂点フォーマットの設定
		for( s32 i= 0; i < iCount; i++ )
		{
			const DeclarationInfo& Info = pDecl[i];
			D3D11_INPUT_ELEMENT_DESC& desc = m_pLayout[i];

			desc.InputSlot				= Info.usStream;
			desc.AlignedByteOffset		= Info.usOffset;
			desc.Format					= DeclType[Info.ucType];
			desc.SemanticName			= DeclUsage[Info.ucUsage];
			desc.SemanticIndex			= Info.ucUsageIndex;
			desc.InputSlotClass			= D3D11_INPUT_PER_VERTEX_DATA;
			desc.InstanceDataStepRate	= 0;
		}

		return true;
	}

	//----------------------------------------------------------------------
	//	レイアウト情報
	//----------------------------------------------------------------------
	ID3D11InputLayout* CDx11Declaration::GetLayout(kgl::TKGLPtr<CDx11VertexShader> pShader)
	{
		kgl::TKGLPtr<ID3D11InputLayout> pLayout;
		u32 uiIndex = kgl::algorithm::NotFind;
		if( !m_pShaderList.empty() )
		{
			uiIndex = kgl::algorithm::linear_search(pShader, &m_pShaderList[0], m_pShaderList.size());
		}

		if( uiIndex == kgl::algorithm::NotFind )
		{
			const ShaderInfo* pInfo = pShader->GetInfo();
			VerifyResult(m_pDevice->GetDevice()->CreateInputLayout(m_pLayout, m_iCount, pInfo + 1, pInfo->iSize, pLayout.GetReference()));

			m_pShaderList.push_back(pShader);
			m_pLayoutList.push_back(pLayout);
			uiIndex = m_pLayoutList.size() - 1;
		}
		return m_pLayoutList[uiIndex];
	}
}
#endif	// ~#if USE_DIRECTX11

//======================================================================
//	END OF FILE
//======================================================================
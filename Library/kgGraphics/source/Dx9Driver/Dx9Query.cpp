//----------------------------------------------------------------------
//!
//!	@file	Dx11Query.cpp
//!	@brief	クエリ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Query.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	クエリ生成
	//----------------------------------------------------------------------
	b8 CDx9Query::Create(CDx9Device* pDevice, const QueryInfo& Info)
	{
		m_pDevice = pDevice;
		m_Info = Info;

		const D3DQUERYTYPE Query[] =
		{
			D3DQUERYTYPE_EVENT,
			D3DQUERYTYPE_OCCLUSION,
			D3DQUERYTYPE_TIMESTAMP,
		};

		HRESULT hr;
		hr = pDevice->GetDevice()->CreateQuery(Query[Info.iQuery], m_pQuery.GetReference());
		if( FAILED(hr) )
		{
			ErrorTrace("クエリの生成に失敗しました(CDx11Query::Create)");
			ErrorTrace("Usage:%d", Info.iQuery);
			return false;
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	クエリ発行
	//----------------------------------------------------------------------
	void CDx9Query::Begin(void)
	{
		switch( m_Info.iQuery )
		{
		case EQuery::Event:
		case EQuery::TimeStamp:
			return;
		}
		m_pQuery->Issue(D3DISSUE_BEGIN);
	}
	//----------------------------------------------------------------------
	//	発行したクエリの終了
	//----------------------------------------------------------------------
	void CDx9Query::End(void)
	{
		m_pQuery->Issue(D3DISSUE_END);
	}
	//----------------------------------------------------------------------
	//	データの取得
	//----------------------------------------------------------------------
	b8 CDx9Query::GetData(void* pData, u32 uiSize)
	{
		HRESULT hr;
		hr = m_pQuery->GetData(pData, uiSize, D3DGETDATA_FLUSH);
		return SUCCEEDED(hr);
	}
}
#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
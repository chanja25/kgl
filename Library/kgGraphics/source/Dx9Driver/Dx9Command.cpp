//----------------------------------------------------------------------
//!
//!	@file	Dx9Command.cpp
//!	@brief	コマンド生成
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Command.h"
#include "Dx9Driver/Dx9Declaration.h"
#include "Dx9Driver/Dx9VertexBuffer.h"
#include "Dx9Driver/Dx9IndexBuffer.h"
#include "Dx9Driver/Dx9Texture.h"

#define	Dx9Device()				m_pDevice->GetDevice()
#define	SetRenderState			Dx9Device()->SetRenderState

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	描画形状の取得
	//----------------------------------------------------------------------
	static INLINE D3DPRIMITIVETYPE PrimitiveType(s32 iPimitive)
	{
		const D3DPRIMITIVETYPE Primitive[] =
		{
			D3DPT_POINTLIST,
			D3DPT_LINELIST,
			D3DPT_LINESTRIP,
			D3DPT_TRIANGLELIST,
			D3DPT_TRIANGLESTRIP,
			D3DPT_TRIANGLEFAN,
		};
		return Primitive[iPimitive];
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx9Command::CDx9Command(kgl::TKGLPtr<CDx9Device> pDevice)
		: m_pDevice(pDevice)
	{}

	//----------------------------------------------------------------------
	//	アルファテスト設定
	//----------------------------------------------------------------------
	void CDx9Command::SetAlphaTest(Command::AlphaTestInfo& Info)
	{
		const dword Func[] =
		{
			D3DCMP_NEVER,
			D3DCMP_NEVER,
			D3DCMP_ALWAYS,
			D3DCMP_LESS,
			D3DCMP_LESSEQUAL,
			D3DCMP_GREATER,
			D3DCMP_GREATEREQUAL,
			D3DCMP_EQUAL,
			D3DCMP_NOTEQUAL,
		};
		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	αテストの有無を設定
		if( CheckInfo(AlphaTestInfo, iTest) )
		{
			m_AlphaTestInfo.iTest = Info.iTest;
			SetRenderState(D3DRS_ALPHATESTENABLE, bEnable);
		}
		if( bEnable )
		{
			//	αテストの内容を設定
			if( CheckInfo(AlphaTestInfo, iTest) )
				SetRenderState(D3DRS_ALPHAFUNC,	Func[Info.iTest]);
			if( CheckInfo(AlphaTestInfo, ucRef) )
				SetRenderState(D3DRS_ALPHAREF,	Info.ucRef);

			UpdateInfo(AlphaTestInfo);
		}
	}

	//----------------------------------------------------------------------
	//	デプステスト設定
	//----------------------------------------------------------------------
	void CDx9Command::SetDepthTest(Command::DepthTestInfo& Info)
	{
		const dword Func[] =
		{
			D3DCMP_NEVER,
			D3DCMP_NEVER,
			D3DCMP_ALWAYS,
			D3DCMP_LESS,
			D3DCMP_LESSEQUAL,
			D3DCMP_GREATER,
			D3DCMP_GREATEREQUAL,
			D3DCMP_EQUAL,
			D3DCMP_NOTEQUAL,
		};

		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	デプステストの有無を設定
		if( CheckInfo(DepthTestInfo, iTest) )
		{
			m_DepthTestInfo.iTest = Info.iTest;
			SetRenderState(D3DRS_ZENABLE, bEnable);
		}
		if( bEnable )
		{
			//	デプステストの内容を設定
			if( CheckInfo(DepthTestInfo, iTest) )
				SetRenderState(D3DRS_ZFUNC,	Func[Info.iTest]);
			if( CheckInfo(DepthTestInfo, bWrite) )
				SetRenderState(D3DRS_ZWRITEENABLE,	Info.bWrite);

			UpdateInfo(DepthTestInfo);
		}
	}

	//----------------------------------------------------------------------
	//	ステンシルテスト設定
	//----------------------------------------------------------------------
	void CDx9Command::SetStencilTest(Command::StencilTestInfo& Info)
	{
		const dword Func[] =
		{
			D3DCMP_NEVER,
			D3DCMP_NEVER,
			D3DCMP_ALWAYS,
			D3DCMP_LESS,
			D3DCMP_LESSEQUAL,
			D3DCMP_GREATER,
			D3DCMP_GREATEREQUAL,
			D3DCMP_EQUAL,
			D3DCMP_NOTEQUAL,
		};
		const dword Op[] =
		{
			D3DSTENCILCAPS_KEEP,
			D3DSTENCILCAPS_ZERO,
			D3DSTENCILCAPS_REPLACE,
			D3DSTENCILCAPS_INCRSAT,
			D3DSTENCILCAPS_DECRSAT,
			D3DSTENCILCAPS_INVERT,
			D3DSTENCILCAPS_INCR,
			D3DSTENCILCAPS_DECR,
		};

		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	ステンシルテストの有無を設定
		if( CheckInfo(StencilTestInfo, iTest) )
		{
			m_StencilTestInfo.iTest = Info.iTest;
			SetRenderState(D3DRS_STENCILENABLE, bEnable);
		}
		if( bEnable )
		{
			//	ステンシルテストの内容を設定
			if( CheckInfo(StencilTestInfo, iTest) )
				SetRenderState(D3DRS_STENCILFUNC,		Func[Info.iTest]);
			if( CheckInfo(StencilTestInfo, iPassOp) )
				SetRenderState(D3DRS_STENCILPASS,		Op[Info.iPassOp]);
			if( CheckInfo(StencilTestInfo, iFailOp) )
				SetRenderState(D3DRS_STENCILFAIL,		Op[Info.iFailOp]);
			if( CheckInfo(StencilTestInfo, iDepthFailOp) )
				SetRenderState(D3DRS_STENCILZFAIL,		Op[Info.iDepthFailOp]);
			if( CheckInfo(StencilTestInfo, ucRef) )
				SetRenderState(D3DRS_STENCILREF,		Info.ucRef);
			if( CheckInfo(StencilTestInfo, bWriteMask) )
				SetRenderState(D3DRS_STENCILWRITEMASK,	Info.bWriteMask);
			if( Info.bWriteMask )
			{
				if( CheckInfo(StencilTestInfo, ucMask) )
					SetRenderState(D3DRS_STENCILMASK,	Info.ucMask);
			}
			else
			{
				RevertInfo(StencilTestInfo, ucMask);
			}

			UpdateInfo(StencilTestInfo);
		}
	}

	//----------------------------------------------------------------------
	//	ブレンド設定
	//----------------------------------------------------------------------
	void CDx9Command::SetBlend(Command::BlendInfo& Info)
	{
		const dword Op[] =
		{
			D3DBLENDOP_ADD,
			D3DBLENDOP_ADD,
			D3DBLENDOP_SUBTRACT,
			D3DBLENDOP_REVSUBTRACT,
			D3DBLENDOP_MIN,
			D3DBLENDOP_MAX,
		};
		const dword Param[] =
		{
			D3DBLEND_ZERO,
			D3DBLEND_ONE,
			D3DBLEND_SRCCOLOR,
			D3DBLEND_INVSRCCOLOR,
			D3DBLEND_DESTCOLOR,
			D3DBLEND_INVDESTCOLOR,
			D3DBLEND_SRCALPHA,
			D3DBLEND_INVSRCALPHA,
			D3DBLEND_DESTALPHA,
			D3DBLEND_INVDESTALPHA,
		};

		//	カラーブレンド設定
		b8 bEnableColorBlend = Info.iColorOp != Command::EBlendOp::Disable;

		if( bEnableColorBlend )
		{
			if( CheckInfo(BlendInfo, iColorOp) )
				SetRenderState(D3DRS_BLENDOP,	Op[Info.iColorOp]);
			if( CheckInfo(BlendInfo, iSrcColor) )
				SetRenderState(D3DRS_SRCBLEND,	Param[Info.iSrcColor]);
			if( CheckInfo(BlendInfo, iDstColor) )
				SetRenderState(D3DRS_DESTBLEND,	Param[Info.iDstColor]);
		}
		else
		{
			RevertInfo(BlendInfo, iColorOp);
			RevertInfo(BlendInfo, iSrcColor);
			RevertInfo(BlendInfo, iDstColor);
		}
		//	アルファブレンド設定
		b8 bEnableAlphaBlend = Info.iAlphaOp != Command::EBlendOp::Disable;
		if( CheckInfo(BlendInfo, iAlphaOp) )
		{
			m_BlendInfo.iAlphaOp = Info.iAlphaOp;
			SetRenderState(D3DRS_ALPHABLENDENABLE, bEnableAlphaBlend);
		}
		if( bEnableAlphaBlend )
		{
			if( CheckInfo(BlendInfo, iAlphaOp) )
				SetRenderState(D3DRS_BLENDOPALPHA,		Op[Info.iAlphaOp]);
			if( CheckInfo(BlendInfo, iSrcAlpha) )
				SetRenderState(D3DRS_SRCBLENDALPHA,		Param[Info.iSrcAlpha]);
			if( CheckInfo(BlendInfo, iDstAlpha) )
				SetRenderState(D3DRS_DESTBLENDALPHA,	Param[Info.iDstAlpha]);
		}
		else
		{
			RevertInfo(BlendInfo, iAlphaOp);
			RevertInfo(BlendInfo, iSrcAlpha);
			RevertInfo(BlendInfo, iDstAlpha);
		}

		UpdateInfo(BlendInfo);
	}

	//----------------------------------------------------------------------
	//	カリングモード設定
	//----------------------------------------------------------------------
	void CDx9Command::SetCullMode(Command::CullModeInfo& Info)
	{
		const dword Mode[] =
		{
			D3DCULL_CW,
			D3DCULL_CCW,
			D3DCULL_NONE,
		};

		if( CheckInfo(CullModeInfo, iMode) )
		{
			SetRenderState(D3DRS_CULLMODE, Mode[Info.iMode]);

			UpdateInfo(CullModeInfo);
		}
	}

	//----------------------------------------------------------------------
	//	テクスチャ設定
	//----------------------------------------------------------------------
	void CDx9Command::SetTexture(Command::TextureInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");
		if( CheckInfo(TextureInfo[Info.iStage], pTexture) )
		{
			ITextureBase* pTexture = Info.pTexture;
			Dx9Device()->SetTexture(Info.iStage, (IDirect3DTexture9*)pTexture->GetTexture());

			UpdateInfo(TextureInfo[Info.iStage]);
		}
	}

	//----------------------------------------------------------------------
	//	LODバイアス設定
	//----------------------------------------------------------------------
	void CDx9Command::SetLODBias(Command::LODBiasInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");
		if( CheckInfo(LODBiasInfo[Info.iStage], fBias) )
		{
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_MIPMAPLODBIAS, *((dword*)&Info.fBias));
		}
		if( CheckInfo(LODBiasInfo[Info.iStage], iMaxMipLevel) )
		{
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_MAXMIPLEVEL, Info.iMaxMipLevel);
		}
		UpdateInfo(LODBiasInfo[Info.iStage]);
	}

	//----------------------------------------------------------------------
	//	サンプラーステート設定
	//----------------------------------------------------------------------
	void CDx9Command::SetSamplerState(Command::SamplerStateInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");

		const dword Filter[] =
		{
			D3DTEXF_POINT,
			D3DTEXF_LINEAR,
		};
		const dword Wrap[] =
		{
			D3DTADDRESS_WRAP,
			D3DTADDRESS_MIRROR,
			D3DTADDRESS_CLAMP,
			D3DTADDRESS_BORDER,
		};
		if( CheckInfo(SamplerStateInfo[Info.iStage], iFilterMin) )
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_MINFILTER, Filter[Info.iFilterMin]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iFilterMag) )
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_MAGFILTER, Filter[Info.iFilterMag]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iFilterMip) )
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_MIPFILTER, Filter[Info.iFilterMip]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iWrapU) )
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_ADDRESSU, Wrap[Info.iWrapU]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iWrapV) )
			Dx9Device()->SetSamplerState(Info.iStage, D3DSAMP_ADDRESSV, Wrap[Info.iWrapV]);

		UpdateInfo(SamplerStateInfo[Info.iStage]);
	}

	//----------------------------------------------------------------------
	//	ビューポート設定
	//----------------------------------------------------------------------
	void CDx9Command::SetViewPort(Command::ViewPortInfo& Info)
	{
		if( CheckInfo(ViewPortInfo, iX) ||
			CheckInfo(ViewPortInfo, iY) ||
			CheckInfo(ViewPortInfo, iWidth) ||
			CheckInfo(ViewPortInfo, iHeight) )
		{
			D3DVIEWPORT9 ViewPort;
			ViewPort.X		= Info.iX;
			ViewPort.Y		= Info.iY;
			ViewPort.Width	= Info.iWidth;
			ViewPort.Height	= Info.iHeight;
			ViewPort.MinZ	= Info.fMinZ;
			ViewPort.MaxZ	= Info.fMaxZ;
			Dx9Device()->SetViewport(&ViewPort);

			UpdateInfo(ViewPortInfo);
		}
	}

	static IDirect3DSurface9* temp[4] = { null };
	//----------------------------------------------------------------------
	//	レンダーターゲット設定
	//----------------------------------------------------------------------
	void CDx9Command::SetRenderTarget(Command::RenderTargetInfo& Info)
	{
		kgAssert(Info.iIndex < RENDER_STAGE_MAX, "Indexに指定できる数はRENDER_STAGE_MAXまでです");

		if( CheckInfo(RenderTargetInfo[Info.iIndex], pTarget) )
		{
			ISurfaceBase* pTargetSurface = Info.pTarget;
			IDirect3DSurface9* pSurface = null;
			if( pTargetSurface )
			{
				pSurface = (IDirect3DSurface9*)pTargetSurface->GetSurface();
				pSurface->AddRef();
			}
			else if( Info.iIndex == 0 )
			{
				//	バックバッファを元に戻す
				pSurface = (IDirect3DSurface9*)m_pDevice->GetDefaultBackBuffer()->GetSurface();
				pSurface->AddRef();
			}
			temp[Info.iIndex] = pSurface;
			Dx9Device()->SetRenderTarget(Info.iIndex, pSurface);
			kgl::SafeRelease(pSurface);

			UpdateInfo(RenderTargetInfo[Info.iIndex]);
		}
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット設定
	//----------------------------------------------------------------------
	void CDx9Command::SetDepthTarget(Command::DepthTargetInfo& Info)
	{
		if( CheckInfo(DepthTargetInfo, pDepthTarget) )
		{
			ISurfaceBase* pTargetSurface = Info.pDepthTarget;
			IDirect3DSurface9* pSurface = null;
			if( pTargetSurface )
			{
				pSurface = (IDirect3DSurface9*)pTargetSurface->GetSurface();
				pSurface->AddRef();
			}
			else
			{
				//	デプスバッファを元に戻す
				pSurface = (IDirect3DSurface9*)m_pDevice->GetDefaultDepthBuffer()->GetSurface();
				pSurface->AddRef();
			}
			Dx9Device()->SetDepthStencilSurface(pSurface);
			kgl::SafeRelease(pSurface);

			UpdateInfo(DepthTargetInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点フォーマット設定
	//----------------------------------------------------------------------
	void CDx9Command::SetDeclaration(Command::DeclarationInfo& Info)
	{
		if( CheckInfo(DeclarationInfo, pDeclaration) )
		{
			IDeclaration* pDecl = Info.pDeclaration;
			Dx9Device()->SetVertexDeclaration((IDirect3DVertexDeclaration9*)pDecl->GetDeclaration());

			UpdateInfo(DeclarationInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点設定
	//----------------------------------------------------------------------
	void CDx9Command::SetVertex(Command::VertexBindInfo& Info)
	{
		kgAssert(Info.iStream < VERTEX_STREAM_MAX, "Streamに指定できる数はVERTEX_STREAM_MAXまでです");

		if( CheckInfo(VertexBindInfo[Info.iStream], iOffset) ||
			CheckInfo(VertexBindInfo[Info.iStream], iStride) ||
			CheckInfo(VertexBindInfo[Info.iStream], pVertex) )
		{
			IVertexBuffer* pVertex = Info.pVertex;
			Dx9Device()->SetStreamSource(Info.iStream, (IDirect3DVertexBuffer9*)pVertex->GetBuffer(), Info.iOffset, Info.iStride);

			UpdateInfo(VertexBindInfo[Info.iStream]);
		}
	}

	//----------------------------------------------------------------------
	//	インデックス設定
	//----------------------------------------------------------------------
	void CDx9Command::SetIndex(Command::IndexBindInfo& Info)
	{
		if( CheckInfo(IndexBindInfo, pIndex) )
		{
			IIndexBuffer* pIndex = Info.pIndex;
			Dx9Device()->SetIndices((IDirect3DIndexBuffer9*)pIndex->GetBuffer());

			UpdateInfo(IndexBindInfo);
		}
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CDx9Command::DrawPrimitiveUP(Command::DrawPrimitiveInfo& Info)
	{
		Dx9Device()->DrawPrimitiveUP(PrimitiveType(Info.iPrimitive), Info.iCount, Info.pVertex, Info.iStride);
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CDx9Command::DrawPrimitive(Command::DrawInfo& Info)
	{
		Dx9Device()->DrawPrimitive(PrimitiveType(Info.iPrimitive), Info.iVertexOffset, Info.iCount);
	}
	//----------------------------------------------------------------------
	//	描画(インデックス有り)
	//----------------------------------------------------------------------
	void CDx9Command::DrawIndexedPrimitiveUP(Command::DrawIndexPrimitiveInfo& Info)
	{
		const D3DFORMAT Format[] =
		{
			D3DFMT_INDEX16,
			D3DFMT_INDEX32,
		};

		Dx9Device()->DrawIndexedPrimitiveUP(PrimitiveType(Info.iPrimitive), 0, Info.iVertexCount, Info.iCount, Info.pIndex, Format[Info.iIndexType], Info.pVertex, Info.iStride);
	}
	//----------------------------------------------------------------------
	//	描画(インデックス有り)
	//----------------------------------------------------------------------
	void CDx9Command::DrawIndexedPrimitive(Command::DrawIndexInfo& Info)
	{
		Dx9Device()->DrawIndexedPrimitive(PrimitiveType(Info.iPrimitive), Info.iVertexOffset, 0, Info.iVertexCount, Info.iIndexOffset, Info.iCount);
	}

	//----------------------------------------------------------------------
	//	頂点シェーダ設定
	//----------------------------------------------------------------------
	void CDx9Command::SetVertexShader(Command::VertexShaderInfo& Info)
	{
		if( CheckInfo(VertexShaderInfo, pShader) )
		{
			IVertexShader* pVertexShader = Info.pShader;
			Dx9Device()->SetVertexShader((IDirect3DVertexShader9*)pVertexShader->GetShader());

			UpdateInfo(VertexShaderInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点シェーダパラメータ設定
	//----------------------------------------------------------------------
	void CDx9Command::SetVertexShaderParam(Command::VertexShaderParamInfo& Info)
	{
		switch( Info.iType )
		{
		case Command::EShaderParam::Boolean:
			Dx9Device()->SetVertexShaderConstantB(Info.iRegister, (BOOL*)Info.pParam, Info.iCount); 
			break;
		case Command::EShaderParam::S32:
			Dx9Device()->SetVertexShaderConstantI(Info.iRegister, (s32*)Info.pParam, (Info.iCount+3)>>2);
			break;
		case Command::EShaderParam::F32:
			Dx9Device()->SetVertexShaderConstantF(Info.iRegister, (f32*)Info.pParam, (Info.iCount+3)>>2);
			break;
		case Command::EShaderParam::Vector4:
			Dx9Device()->SetVertexShaderConstantF(Info.iRegister, (f32*)Info.pParam, Info.iCount);
			break;
		case Command::EShaderParam::Matrix:
			Dx9Device()->SetVertexShaderConstantF(Info.iRegister, (f32*)Info.pParam, Info.iCount * 4);
			break;

		default:
			kgAssert(false, "不明なパラメータが設定されています(CDx9Command::SetVertexShaderParam)");
			break;
		}
	}

	//----------------------------------------------------------------------
	//	頂点シェーダ設定
	//----------------------------------------------------------------------
	void CDx9Command::SetPixelShader(Command::PixelShaderInfo& Info)
	{
		if( CheckInfo(PixelShaderInfo, pShader) )
		{
			IPixelShader* pPixelShader = Info.pShader;
			Dx9Device()->SetPixelShader((IDirect3DPixelShader9*)pPixelShader->GetShader());

			UpdateInfo(PixelShaderInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点シェーダパラメータ設定
	//----------------------------------------------------------------------
	void CDx9Command::SetPixelShaderParam(Command::PixelShaderParamInfo& Info)
	{
		switch( Info.iType )
		{
		case Command::EShaderParam::Boolean:
			Dx9Device()->SetPixelShaderConstantB(Info.iRegister, (BOOL*)Info.pParam, Info.iCount);
			break;
		case Command::EShaderParam::S32:
			Dx9Device()->SetPixelShaderConstantI(Info.iRegister, (s32*)Info.pParam, (Info.iCount+3)>>2);
			break;
		case Command::EShaderParam::F32:
			Dx9Device()->SetPixelShaderConstantF(Info.iRegister, (f32*)Info.pParam, (Info.iCount+3)>>2);
			break;
		case Command::EShaderParam::Vector4:
			Dx9Device()->SetPixelShaderConstantF(Info.iRegister, (f32*)Info.pParam, Info.iCount);
			break;
		case Command::EShaderParam::Matrix:
			Dx9Device()->SetPixelShaderConstantF(Info.iRegister, (f32*)Info.pParam, Info.iCount * 4);
			break;

		default:
			kgAssert(false, "不明なパラメータが設定されています(CDx9Command::SetVertexShaderParam)");
			break;
		}
	}

	//----------------------------------------------------------------------
	//	ターゲットのクリア
	//----------------------------------------------------------------------
	void CDx9Command::ClearTarget(Command::ClearTargetInfo& Info)
	{
		dword dwFlag = 0;
		dwFlag |= (Info.iTarget & Command::EClearTarget::Color)?   D3DCLEAR_TARGET : 0;
		dwFlag |= (Info.iTarget & Command::EClearTarget::Depth)?   D3DCLEAR_ZBUFFER: 0;
		dwFlag |= (Info.iTarget & Command::EClearTarget::Stencil)? D3DCLEAR_STENCIL: 0;

		u32 num = 0;
		for (u32 i = 0; i < kgl::ArrayCount(temp); i++)
		{
			if (!temp[i]) break;
			num = i + 1;
		}
		Dx9Device()->Clear(num, null, dwFlag, CalcDx9Color(Info.uiColor), Info.fDepth, Info.iStencil);
	}
	//----------------------------------------------------------------------
	//	シザリング設定
	//----------------------------------------------------------------------
	void CDx9Command::SetScissoring(Command::ScissoringInfo& Info)
	{
		if( CheckInfo(ScissoringInfo, iX) ||
			CheckInfo(ScissoringInfo, iY) ||
			CheckInfo(ScissoringInfo, iWidth) ||
			CheckInfo(ScissoringInfo, iHeight) )
		{
			RECT rect;
			rect.left	= Info.iX;
			rect.top	= Info.iY;
			rect.right	= Info.iX + Info.iWidth;
			rect.bottom	= Info.iY + Info.iHeight;
			Dx9Device()->SetScissorRect(&rect);

			b8 bEnable = (Info.iX != 0) ||
						 (Info.iY != 0) ||
						 (Info.iWidth != kgl::Config::ResolutionWidth) ||
						 (Info.iHeight != kgl::Config::ResolutionHeight);

			SetRenderState(D3DRS_SCISSORTESTENABLE, bEnable);
			UpdateInfo(ScissoringInfo);
		}
	}
	//----------------------------------------------------------------------
	//	ビュー行列設定
	//----------------------------------------------------------------------
	void CDx9Command::SetView(Command::ViewInfo& Info)
	{
		const u32 ViewRegister = 0;
		kgl::Matrix mView;
		kgl::Math::MatrixInverse(mView, *Info.pView);
		Dx9Device()->SetPixelShaderConstantF(ViewRegister, (f32*)Info.pView, 3);
	}
	//----------------------------------------------------------------------
	//	プロジェクション行列設定
	//----------------------------------------------------------------------
	void CDx9Command::SetProjection(Command::ProjectionInfo& Info)
	{
		const u32 ProjectionRegister = 4;
		kgl::Matrix mProjection;
		kgl::Math::MatrixInverse(mProjection, *Info.pProjection);
		Dx9Device()->SetPixelShaderConstantF(ProjectionRegister, (f32*)Info.pProjection, 3);
	}
	//----------------------------------------------------------------------
	//	ワールド行列設定
	//----------------------------------------------------------------------
	void CDx9Command::SetWorld(Command::WorldInfo& Info)
	{
		u32 WorldRegister = 8;

		kgl::Matrix mWorld;
		for( s32 i=0; i < Info.iCount; i++ )
		{
			kgl::Math::MatrixInverse(mWorld, *(Info.pWorld + i));
			Dx9Device()->SetPixelShaderConstantF(WorldRegister, (f32*)&mWorld, 3);
			WorldRegister += 4;
		}
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
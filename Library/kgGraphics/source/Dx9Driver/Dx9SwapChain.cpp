//----------------------------------------------------------------------
//!
//!	@file	Dx9SwapChain.cpp
//!	@brief	スワップチェイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9SwapChain.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	スワップチェイン生成
	//----------------------------------------------------------------------
	b8 CDx9SwapChain::Create(CDx9Device* pDevice, kgl::Window::IWindow* pWindow)
	{
		//	画面のモード取得
		D3DDISPLAYMODE d3ddm;
		if (FAILED(pDevice->GetD3D()->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
		{
			ErrorTrace("画面のモード取得を失敗しました(CDx9Device::Initialize)");
			return false;
		}

		s32 iWidth, iHeight;
		pWindow->GetSize(iWidth, iHeight);
		kgZeroMemory(&m_d3dpp, sizeof(D3DPRESENT_PARAMETERS));
		//	Direct3DDeviceの設定
		m_d3dpp.Windowed			= !pWindow->IsFullScreen();
		m_d3dpp.BackBufferWidth		= iWidth;
		m_d3dpp.BackBufferHeight	= iHeight;
		m_d3dpp.Flags				= 0;
		m_d3dpp.BackBufferCount		= 1;
		m_d3dpp.BackBufferFormat	= d3ddm.Format;
		m_d3dpp.SwapEffect			= D3DSWAPEFFECT_DISCARD;
		m_d3dpp.hDeviceWindow		= (HWND)pWindow->GetHandle();
		m_d3dpp.MultiSampleType		= D3DMULTISAMPLE_NONE;
		m_d3dpp.MultiSampleQuality	= 0;

		m_d3dpp.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;
		m_d3dpp.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;

		if( FAILED(pDevice->GetDevice()->CreateAdditionalSwapChain(&m_d3dpp, m_pSwapChain.GetReference())) )
		{
			return false;
		}

		kgl::TKGLPtr<IDirect3DSurface9> pDxSurface;

		if( FAILED(m_pSwapChain->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, pDxSurface.GetReference())) )
		{
			return false;
		}
		kgl::TKGLPtr<CDx9Surface> pSurface = new CDx9Surface;
		SurfaceInfo Info;
		Info.iFormat	= ETextureFormat::A8R8G8B8;
		Info.iUsage		= ETextureUsage::RenderTarget;
		Info.uiWidth	= m_d3dpp.BackBufferWidth;
		Info.uiHeight	= m_d3dpp.BackBufferHeight;
		pSurface->Setup(Info, pDxSurface);

		pSurface->AddRef();
		m_pSurface = pSurface;
		m_pWindow = pWindow;

		return true;
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx9SwapChain::Present(bool bVsync)
	{
		(void)bVsync;
		return SUCCEEDED(m_pSwapChain->Present(null, null, null, null, 0));
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx9SwapChain::GetSurface(ISurfaceBase** ppSurface)
	{
		m_pSurface->AddRef();
		*ppSurface = m_pSurface;
		return true;
	}
}
#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
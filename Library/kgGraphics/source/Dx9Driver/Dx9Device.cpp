//----------------------------------------------------------------------
//!
//!	@file	Dx9Device.h
//!	@brief	DirectX9
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Device.h"
#include "Dx9Driver/Dx9Surface.h"
#include "Dx9Driver/Dx9Query.h"
#include "Dx9Driver/Dx9SwapChain.h"
#include "Dx9Driver/Dx9RenderTarget.h"
#include <functional>
#include "../../../kgl/include/Thread/kgl.Event.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx9Device::CDx9Device(void)
	{
	}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CDx9Device::~CDx9Device(void)
	{
		Finalize();
	}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx9Device::Initialize(const GRAPHICS_INFO& info)
	{
		kgAssert(!m_pD3D.IsValid(), "既にCDx9Deviceは初期化されています(CDx9Device::Initialize)");
		//	Direct3Dオブジェクトの生成
		*m_pD3D.GetReference() = Direct3DCreate9(D3D_SDK_VERSION);
		if (!m_pD3D.IsValid())
		{
			ErrorTrace("Direct3Dオブジェクトの生成に失敗しました(CDx9Device::Initialize)");
			return false;
		}
		//	画面のモード取得
		D3DDISPLAYMODE d3ddm;
		if (FAILED(m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm)))
		{
			ErrorTrace("画面のモード取得を失敗しました(CDx9Device::Initialize)");
			return false;
		}
		D3DCAPS9 Caps;
		kgZeroMemory(&Caps, sizeof(Caps));
		//	シェーダーのバージョンを確認
		m_pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &Caps);
		if( Caps.VertexShaderVersion < D3DVS_VERSION(3, 0) )
		{
			ErrorTrace("[VertexShader]デバイスの性能が足りません(CDx9Device::Initialize)");
			return false;
		}
		if( Caps.PixelShaderVersion < D3DPS_VERSION(3, 0) )
		{
			ErrorTrace("[PixelShader]デバイスの性能が足りません(CDx9Device::Initialize)");
			return false;
		}

		m_bVSync = info.bUseVSync;
		kgZeroMemory(&m_d3dpp, sizeof(D3DPRESENT_PARAMETERS));
		//	Direct3DDeviceの設定
		m_d3dpp.Windowed				= true;
		m_d3dpp.BackBufferWidth			= info.iWidth;
		m_d3dpp.BackBufferHeight		= info.iHeight;

		m_d3dpp.Flags					= 0;
		m_d3dpp.BackBufferCount			= 1;
		m_d3dpp.BackBufferFormat		= d3ddm.Format;
		m_d3dpp.SwapEffect				= D3DSWAPEFFECT_DISCARD;
		m_d3dpp.hDeviceWindow			= (HWND)info.pWindow->GetHandle();
		m_d3dpp.MultiSampleType			= D3DMULTISAMPLE_NONE;
		m_d3dpp.MultiSampleQuality		= 0;

		m_d3dpp.EnableAutoDepthStencil	= false;
		m_d3dpp.AutoDepthStencilFormat	= D3DFMT_D24S8;

		if (m_bVSync)	m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;			// VSYNC待ち有り
		else			m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	// VSYNC待ち無し

		m_d3dpp.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;

		m_d3dppFS = m_d3dpp;
		m_d3dppFS.Windowed = false;

		m_pd3dpp = info.bFullScreen? &m_d3dppFS: &m_d3dpp;
		//	Direct3Dデバイスの生成
		if( FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT,
										D3DDEVTYPE_HAL,
										m_pd3dpp->hDeviceWindow,
										D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
										m_pd3dpp,
										m_pDevice.GetReference())) )
		{
			ErrorTrace("Direct3Dデバイスの生成に失敗しました(CDx9Device::Initialize)");
			return false;
		}
		m_pWindow = info.pWindow;

		//m_pDevice->CreateTexture(info.iWidth, info.iHeight, 1, D3DUSAGE_RENDERTARGET, D3DFMT_R32F, D3DPOOL_DEFAULT, m_pDepthStencilTex.GetReference(), null);
		if( m_pDepthStencilTex.IsValid() )
		{
			m_pDepthStencilTex->GetSurfaceLevel(0, m_pDepthStencil.GetReference());
		}
		else
		{
			m_pDevice->CreateDepthStencilSurface(info.iWidth, info.iHeight, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, false, m_pDepthStencil.GetReference(), null);
		}
		GetBackBuffer(m_pBackBuffer.GetReference());
		GetDepthBuffer(m_pDepthBuffer.GetReference());

		return true;
	}
	//----------------------------------------------------------------------
	//	終了処理
	//----------------------------------------------------------------------
	void CDx9Device::Finalize(void)
	{
		m_pDevice	= null;
		m_pD3D		= null;
	}
	//----------------------------------------------------------------------
	//	描画シーン開始
	//----------------------------------------------------------------------
	void CDx9Device::BeginScene(void)
	{
		m_pDevice->BeginScene();
	}
	//----------------------------------------------------------------------
	//	描画シーン終了
	//----------------------------------------------------------------------
	void CDx9Device::EndScene(void)
	{
		m_pDevice->EndScene();
		//	画面に反映させる
		if( FAILED(m_pDevice->Present(null, null, null, null)) )
		{
			if( m_pDevice->TestCooperativeLevel() != D3DERR_DEVICELOST )
			{
				if( m_pWindow->IsActive() )
				{
					kgl::TKGLPtr<kgl::Thread::IEvent> pEvent;
					kgl::Thread::Create(pEvent.GetReference(), true);
					HRESULT hr;
					std::function<void(void)> func = [&] {
						hr = m_pDevice->Reset(m_pd3dpp); pEvent->SetSignal();
					};
					SendMessage(m_pd3dpp->hDeviceWindow, 0x00001001, (s32)&func, 0);
					pEvent->Wait();
					if( SUCCEEDED(hr) )
					{
						m_pBackBuffer = null;
						m_pDepthBuffer = null;
						GetBackBuffer(m_pBackBuffer.GetReference());
						GetDepthBuffer(m_pDepthBuffer.GetReference());
					}
				}
			}
		}
		else
		{
			b8 bFullScreen = !m_pd3dpp->Windowed;
			if (bFullScreen != m_pWindow->IsFullScreen())
			{
				m_pd3dpp = m_pWindow->IsFullScreen()? &m_d3dppFS: &m_d3dpp;
				HRESULT hr = m_pDevice->Reset(m_pd3dpp);
				if( FAILED(hr) )
				{
					Trace("error");
				}
			}
		}
	}
	//----------------------------------------------------------------------
	//	GPUのコマンドを実行する
	//----------------------------------------------------------------------
	void CDx9Device::FlushCommand(void)
	{
		if( m_pDevice )
		{
			QueryInfo Info;
			Info.iQuery = EQuery::Event;
			kgl::TKGLPtr<IQueryBase> pQuery;
			if( CreateQuery(pQuery.GetReference(), Info) )
			{
				pQuery->End();
				pQuery->GetData(null, 0);
			}
		}
	}
	//----------------------------------------------------------------------
	//	GPUの処理終了待ち
	//----------------------------------------------------------------------
	void CDx9Device::WaitForCommand(void)
	{
		if( m_pDevice )
		{
			QueryInfo Info;
			Info.iQuery = EQuery::Event;
			kgl::TKGLPtr<IQueryBase> pQuery;
			if( CreateQuery(pQuery.GetReference(), Info) )
			{
				pQuery->End();
				BOOL data = 0;
				while( pQuery->GetData(&data, sizeof(data)) && !data )
				{
					kgSleep(0);
				}
			}
		}
	}
	//----------------------------------------------------------------------
	//	バックバッファの取得
	//----------------------------------------------------------------------
	void CDx9Device::GetBackBuffer(ISurfaceBase** ppBackBuffer)
	{
		kgl::TKGLPtr<IDirect3DSurface9> pDxSurface;

		m_pDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, pDxSurface.GetReference());
		if( !pDxSurface.IsValid() ) return;

		kgl::TKGLPtr<CDx9Surface> pSurface = new CDx9Surface;
		SurfaceInfo Info;
		Info.iFormat	= ETextureFormat::A8R8G8B8;
		Info.iUsage		= ETextureUsage::RenderTarget;
		Info.uiWidth	= m_pd3dpp->BackBufferWidth;
		Info.uiHeight	= m_pd3dpp->BackBufferHeight;
		pSurface->Setup(Info, pDxSurface);

		pSurface->AddRef();
		*ppBackBuffer = pSurface;
	}
	//----------------------------------------------------------------------
	//	デプスバッファの取得
	//----------------------------------------------------------------------
	void CDx9Device::GetDepthBuffer(ISurfaceBase** ppDepthBuffer)
	{
		kgl::TKGLPtr<IDirect3DSurface9> pDxSurface;

		pDxSurface = m_pDepthStencil;
		//m_pDevice->GetDepthStencilSurface(pDxSurface.GetReference());
		if (!pDxSurface.IsValid())	return;

		kgl::TKGLPtr<CDx9Surface> pSurface = new CDx9Surface;
		SurfaceInfo Info;
		Info.iFormat	= ETextureFormat::D24S8;
		Info.iUsage		= ETextureUsage::Depth;
		Info.uiWidth	= m_pd3dpp->BackBufferWidth;
		Info.uiHeight	= m_pd3dpp->BackBufferHeight;
		pSurface->Setup(Info, pDxSurface);

		pSurface->AddRef();
		*ppDepthBuffer = pSurface;
	}
	//----------------------------------------------------------------------
	//	VSyncの有無を設定
	//----------------------------------------------------------------------
	void CDx9Device::SetVSync(b8 bEnable)
	{
		m_bVSync = bEnable;

		u32 oldInterval = m_d3dpp.PresentationInterval;
		if (m_bVSync)	m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;			// VSYNC待ち有り
		else			m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	// VSYNC待ち無し

		if (m_d3dpp.PresentationInterval != oldInterval)
		{
			m_d3dppFS.PresentationInterval = m_d3dpp.PresentationInterval;
			m_pDevice->Reset(m_pd3dpp);
		}
	}
	//----------------------------------------------------------------------
	//	クエリー生成
	//----------------------------------------------------------------------
	b8 CDx9Device::CreateQuery(IQueryBase** ppQuery, const QueryInfo& Info)
	{
		kgl::TKGLPtr<CDx9Query> pQuery = new CDx9Query;
		if( !pQuery->Create(this, Info) )
		{
			return false;
		}
		pQuery->AddRef();
		*ppQuery = pQuery;
		return true;
	}
	//----------------------------------------------------------------------
	//	スワップチェインの生成
	//----------------------------------------------------------------------
	b8 CDx9Device::CreateSwapChain(ISwapChainBase** ppSwapChain, kgl::Window::IWindow* pWindow)
	{
		kgl::TKGLPtr<CDx9SwapChain> pSwapChain;
		pSwapChain = new CDx9SwapChain;
		if( !pSwapChain->Create(this, pWindow) )
		{
			return false;
		}
		pSwapChain->AddRef();
		*ppSwapChain = pSwapChain;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
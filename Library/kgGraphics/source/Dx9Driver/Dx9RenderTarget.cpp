//----------------------------------------------------------------------
//!
//!	@file	Dx9Texture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9RenderTarget.h"
#include "Dx9Driver/Dx9Texture.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	レンダーターゲット生成
	//----------------------------------------------------------------------
	b8 CDx9RenderTargetManager::CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info)
	{
		kgl::TKGLPtr<CDx9RenderTarget> pRenderTarget = new CDx9RenderTarget;
		if( !pRenderTarget->Create(m_pDevice, Info) )
		{
			return false;
		}

		pRenderTarget->AddRef();
		*ppRenderTarget = pRenderTarget;
		return true;
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット生成
	//----------------------------------------------------------------------
	b8 CDx9RenderTarget::Create(CDx9Device* pDevice, const RenderTargetInfo& Info)
	{
		TextureInfo TexInfo;
		TexInfo.iType		= ETextureType::Normal;
		TexInfo.iUsage		= Info.iUsage;
		TexInfo.iFormat		= Info.iFormat;
		TexInfo.uiWidth		= Info.uiWidth;
		TexInfo.uiHeight	= Info.uiHeight;
		TexInfo.iMipLevels	= 1;

		m_Info = Info;

		kgl::TKGLPtr<CDx9Texture2D> pRenderTarget = new CDx9Texture2D;
		if( !pRenderTarget->Create(pDevice, TexInfo, null) )
		{
			return false;
		}
		m_pRenderTarget = pRenderTarget;
		return true;
	}

	//----------------------------------------------------------------------
	//	レンダーターゲットのロック
	//----------------------------------------------------------------------
	void* CDx9RenderTarget::Lock(s32 iType)
	{
		return m_pRenderTarget->Lock(0, iType);
	}
	//----------------------------------------------------------------------
	//	レンダーターゲットのアンロック
	//----------------------------------------------------------------------
	void CDx9RenderTarget::Unlock(void)
	{
		m_pRenderTarget->Unlock(0);
	}
	//----------------------------------------------------------------------
	//	レンダーターゲット(テクスチャー)の取得
	//----------------------------------------------------------------------
	void CDx9RenderTarget::GetRenderTarget(ITextureBase** ppTexture)
	{
		m_pRenderTarget->AddRef();
		*ppTexture = m_pRenderTarget;
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx9RenderTarget::GetSurface(ISurfaceBase** ppSurface)
	{
		return m_pRenderTarget->GetSurface(ppSurface, 0);
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx9IndexBuffer.cpp
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Declaration.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx9Declaration::CDx9Declaration(void)
		: m_pDecl(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx9Declaration::Initialize(CDx9Device* pDevice, const DeclarationInfo* pDecl)
	{
		s32 iCount = 0;
		//	KGG_DECL_END()までの数を算出
		while( (pDecl + iCount)->usStream != 0xffff )
		{
			iCount ++;
			kgAssert(iCount < 100, "Declationの設定でKGG_DECL_END()が指定されておりません(CDx9Declaration::Initialize)");
		}

		//	頂点タイプ
		const u8 DECL_TYPE[] =
		{
			D3DDECLTYPE_FLOAT1,
			D3DDECLTYPE_FLOAT2,
			D3DDECLTYPE_FLOAT3,
			D3DDECLTYPE_FLOAT4,
			D3DDECLTYPE_D3DCOLOR,
			D3DDECLTYPE_UBYTE4,
			D3DDECLTYPE_SHORT2,
			D3DDECLTYPE_SHORT4,
			D3DDECLTYPE_UBYTE4N,
			D3DDECLTYPE_SHORT2N,
			D3DDECLTYPE_SHORT4N,
			D3DDECLTYPE_USHORT2N,
			D3DDECLTYPE_USHORT4N,
			D3DDECLTYPE_FLOAT16_2,
			D3DDECLTYPE_FLOAT16_4,
		};

		const u8 DECL_USAGE[] =
		{
			D3DDECLUSAGE_POSITION,
			D3DDECLUSAGE_BLENDWEIGHT,
			D3DDECLUSAGE_BLENDINDICES,
			D3DDECLUSAGE_NORMAL,
			D3DDECLUSAGE_PSIZE,
			D3DDECLUSAGE_TEXCOORD,
			D3DDECLUSAGE_TANGENT,
			D3DDECLUSAGE_BINORMAL,
			D3DDECLUSAGE_TESSFACTOR,
			D3DDECLUSAGE_POSITIONT,
			D3DDECLUSAGE_COLOR,
			D3DDECLUSAGE_FOG,
			D3DDECLUSAGE_DEPTH,
		};

		kgl::TSmartPtr<D3DVERTEXELEMENT9> pTempDecl = new D3DVERTEXELEMENT9[iCount + 1];
		//	頂点フォーマットの設定
		for( s32 i=0; i < iCount; i++ )
		{
			pTempDecl[i].Stream		= pDecl[i].usStream;
			pTempDecl[i].Offset		= pDecl[i].usOffset;
			pTempDecl[i].Type		= DECL_TYPE[pDecl[i].ucType];
			pTempDecl[i].Method		= D3DDECLMETHOD_DEFAULT;
			pTempDecl[i].Usage		= DECL_USAGE[pDecl[i].ucUsage];
			pTempDecl[i].UsageIndex	= pDecl[i].ucUsageIndex;
		}
		const D3DVERTEXELEMENT9 DECL_END = D3DDECL_END();
		//	末尾の設定
		kgMemcpy(&pTempDecl[iCount], &DECL_END, sizeof(D3DVERTEXELEMENT9));

		VerifyResult(pDevice->GetDevice()->CreateVertexDeclaration(pTempDecl, m_pDecl.GetReference()));

		return true;
	}
}
#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
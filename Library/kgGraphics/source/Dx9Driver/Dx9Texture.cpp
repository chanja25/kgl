//----------------------------------------------------------------------
//!
//!	@file	Dx9Texture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Texture.h"
#include "Dx9Driver/Dx9Surface.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx9TextureManager::CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData)
	{
		kgl::TKGLPtr<CDx9Texture2D> pTexture = new CDx9Texture2D;
		if( !pTexture->Create(m_pDevice, Info, pData) )
		{
			return false;
		}
		pTexture->AddRef();
		*ppTexture = pTexture;
		return true;
	}
	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx9TextureManager::CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData)
	{
		kgl::TKGLPtr<CDx9Texture2D> pTexture = new CDx9Texture2D;
		if( !pTexture->CreateFromDDS(m_pDevice, pData) )
		{
			return false;
		}
		pTexture->AddRef();
		*ppTexture = pTexture;
		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx9Texture2D::Create(CDx9Device* pDevice, const TextureInfo& Info, const void* pData)
	{
		dword dwUsage = 0;
		dwUsage |= (Info.iUsage & ETextureUsage::RenderTarget)?	D3DUSAGE_RENDERTARGET:	0;
		dwUsage |= (Info.iUsage & ETextureUsage::Depth)?		D3DUSAGE_DEPTHSTENCIL:	0;
		//dwUsage |= (Info.iUsage & ETextureUsage::Dynamic)?		D3DUSAGE_DYNAMIC:		0;

		D3DFORMAT eFormat;
		switch( Info.iFormat )
		{
		case ETextureFormat::R8G8B8:		eFormat = D3DFMT_R8G8B8;		break;
		case ETextureFormat::A8R8G8B8:		eFormat = D3DFMT_A8R8G8B8;		break;
		case ETextureFormat::A16R16G16B16:	eFormat = D3DFMT_A16B16G16R16;	break;
		case ETextureFormat::A16R16G16B16F:	eFormat = D3DFMT_A16B16G16R16F;	break;
		case ETextureFormat::R5G6B5:		eFormat = D3DFMT_R5G6B5;		break;
		case ETextureFormat::A1R5G5B5:		eFormat = D3DFMT_A1R5G5B5;		break;
		case ETextureFormat::A4R4G4B4:		eFormat = D3DFMT_A4R4G4B4;		break;
		case ETextureFormat::A8:			eFormat = D3DFMT_A8;			break;
		case ETextureFormat::L8:			eFormat = D3DFMT_L8;			break;
		case ETextureFormat::A8L8:			eFormat = D3DFMT_A8L8;			break;
		case ETextureFormat::R16F:			eFormat = D3DFMT_R16F;			break;
		case ETextureFormat::R32F:			eFormat = D3DFMT_R32F;			break;
		case ETextureFormat::DXT1:			eFormat = D3DFMT_DXT1;			break;
		case ETextureFormat::DXT3:			eFormat = D3DFMT_DXT3;			break;
		case ETextureFormat::DXT5:			eFormat = D3DFMT_DXT5;			break;
		case ETextureFormat::D16:			eFormat = D3DFMT_D16;			break;
		case ETextureFormat::D32:			eFormat = D3DFMT_D32;			break;
		case ETextureFormat::D24S8:			eFormat = D3DFMT_D24S8;			break;
		default:
			ErrorTrace("指定されたFormatが対応していないか不正な値です(CDx9Texture2D::Create)");
			return false;
		}
		s32 iMipLevels = kgl::Max(1, Info.iMipLevels);

		D3DPOOL ePool = D3DPOOL_MANAGED;
		if( dwUsage & (D3DUSAGE_RENDERTARGET | D3DUSAGE_DEPTHSTENCIL) )// | D3DUSAGE_DYNAMIC) )
		{
			ePool = D3DPOOL_DEFAULT;
		}

		HRESULT hr;
		hr = D3DXCreateTexture(
			pDevice->GetDevice(),
			Info.uiWidth,
			Info.uiHeight,
			iMipLevels,
			dwUsage,
			eFormat,
			ePool,
			m_pTexture.GetReference());
		if( FAILED(hr) )
		{
			ErrorTrace("テクスチャーの生成に失敗しました(CDx9Texture2D::Create)");
			ErrorTrace("Type:%d Usage:%d Format:%d", Info.iType, Info.iUsage, Info.iFormat);
			ErrorTrace("Width:%d Height:%d MipLevels:%d", Info.uiWidth, Info.uiHeight, Info.iMipLevels);
			return false;
		}
		m_Info.iType		= Info.iType;
		m_Info.iUsage		= Info.iUsage;
		m_Info.iFormat		= Info.iFormat;
		m_Info.uiWidth		= Info.uiWidth;
		m_Info.uiHeight		= Info.uiHeight;
		m_Info.iMipLevels	= iMipLevels;

		if( pData )
		{
			kgl::TSmartPtr<TextureData> TexData;
			TexData = BuildTextureData(Info, pData);
			for( s32 i=0; i < iMipLevels; i++ )
			{
				void* pPixel = Lock(i, ELockType::Write);
				kgMemcpy(pPixel, TexData[i].pData, TexData[i].uiSize);
				Unlock(i);
			}
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	DDSからテクスチャー生成
	//----------------------------------------------------------------------
	b8 CDx9Texture2D::CreateFromDDS(CDx9Device* pDevice, const void* pData)
	{
		if( pData == null )
		{
			ErrorTrace("指定されたデータがnullです(CDx9Texture2D::CreateFromDDS)");
			return false;
		}
		DDS_INFO ddsInfo;
		if( !AnalyzeDDS(&ddsInfo, pData) )
		{
			ErrorTrace("指定されたデータが壊れているか対応していないフォーマットです(CDx9Texture2D::CreateFromDDS)");
			return false;
		}
		if( ddsInfo.iCubemap )
		{
			ErrorTrace("現状のKGLはキューブマップに未対応です(CDx9Texture2D::CreateFromDDS)");
			return false;
		}

		TextureInfo Info;
		Info.iType		= ETextureType::Normal;
		Info.iUsage		= 0;
		Info.uiWidth	= ddsInfo.uiWidth;
		Info.uiHeight	= ddsInfo.uiHeight;
		Info.iMipLevels	= ddsInfo.iMipLevels;

		switch( ddsInfo.uiFormat )
		{
		case DDSFMT_DXT1:		Info.iFormat = ETextureFormat::DXT1;		break;
		case DDSFMT_DXT3:		Info.iFormat = ETextureFormat::DXT3;		break;
		case DDSFMT_DXT5:		Info.iFormat = ETextureFormat::DXT5;		break;
		case DDSFMT_A8R8G8B8:	Info.iFormat = ETextureFormat::A8R8G8B8;	break;
		case DDSFMT_A1R5G5B5:	Info.iFormat = ETextureFormat::A1R5G5B5;	break;
		case DDSFMT_A4R4G4B4:	Info.iFormat = ETextureFormat::A4R4G4B4;	break;
		case DDSFMT_A8:			Info.iFormat = ETextureFormat::A8;			break;
		case DDSFMT_L8:			Info.iFormat = ETextureFormat::L8;			break;
		default:
			ErrorTrace("指定されたデータが対応していないフォーマットです(CDx9Texture2D::CreateFromDDS)");
			return false;
		}

		if( !Create(pDevice, Info, ddsInfo.pPixels) )
		{
			ErrorTrace("テクスチャーの生成に失敗しました(CDx9Texture2D::CreateFromDDS)");
			return false;
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャーのロック
	//----------------------------------------------------------------------
	void* CDx9Texture2D::Lock(s32 iLevel, s32 iType)
	{
		dword dwFlags = 0;
		switch( iType )
		{
		case ELockType::Read:		dwFlags = D3DLOCK_READONLY;	break;
		case ELockType::Write:		dwFlags = D3DLOCK_DISCARD;	break;
		case ELockType::ReadWrite:	dwFlags = 0;				break;
		}
		D3DLOCKED_RECT Lock;
		//	テクスチャー情報の取得
		VerifyResult(m_pTexture->LockRect(iLevel, &Lock, null, dwFlags));

		return Lock.pBits;
	}

	//----------------------------------------------------------------------
	//	テクスチャーのアンロック
	//----------------------------------------------------------------------
	void CDx9Texture2D::Unlock(s32 iLevel)
	{
		VerifyResult(m_pTexture->UnlockRect(iLevel));
	}
	//----------------------------------------------------------------------
	//	リソースのコピー
	//----------------------------------------------------------------------
	b8 CDx9Texture2D::CopyResource(ITextureBase* pResource)
	{
		auto& SrcInfo = pResource->GetInfo();
		if( m_Info.iMipLevels != SrcInfo.iMipLevels ||
			m_Info.uiWidth != SrcInfo.uiWidth ||
			m_Info.uiHeight != SrcInfo.uiHeight ||
			m_Info.iFormat != SrcInfo.iFormat )
		{
			return false;
		}
		u32 uiSize = GetStride(m_Info.iFormat) * m_Info.uiWidth * m_Info.uiHeight;
		for( s32 i=0; i < m_Info.iMipLevels; i++ )
		{
			auto dst = Lock(i, ELockType::Write);
			auto src = pResource->Lock(i);
			memcpy(dst, src, uiSize);
			pResource->Unlock(i);
			Unlock(i);
			uiSize /= 2;
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CDx9Texture2D::GetSurface(ISurfaceBase** ppSurface, s32 iLevel)
	{
		kgl::TKGLPtr<IDirect3DSurface9> pDxSurface;
		m_pTexture->GetSurfaceLevel(iLevel, pDxSurface.GetReference());
		if( !pDxSurface.IsValid() )
		{
			return false;
		}

		kgl::TKGLPtr<CDx9Surface> pSurface = new CDx9Surface;
		SurfaceInfo Info;
		Info.iFormat	= m_Info.iFormat;
		Info.iUsage		= m_Info.iUsage;
		Info.uiWidth	= m_Info.uiWidth  / (iLevel+1);
		Info.uiHeight	= m_Info.uiHeight / (iLevel+1);
		pSurface->Setup(Info, pDxSurface);

		pSurface->AddRef();
		*ppSurface = pSurface;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx9VertexBuffer.cpp
//!	@brief	頂点バッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9VertexBuffer.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx9VertexBuffer::CDx9VertexBuffer(void)
		: m_pBuffer(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx9VertexBuffer::Initialize(CDx9Device* pDevice, u32 uiSize, b8 bDynamic)
	{
		(void)bDynamic;
		m_pBuffer = null;
		VerifyResult(pDevice->GetDevice()->CreateVertexBuffer(uiSize, 0, 0, D3DPOOL_MANAGED, m_pBuffer.GetReference(), null));
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CDx9VertexBuffer::Lock(u32 uiOffset, u32 uiSize, s32 iType)
	{
		dword dwFlags = 0;
		switch( iType )
		{
		case ELockType::Read:				dwFlags = D3DLOCK_READONLY;		break;
		case ELockType::Write:				dwFlags = D3DLOCK_DISCARD;		break;
		case ELockType::WriteNoOverWrite:	dwFlags = D3DLOCK_NOOVERWRITE;	break;
		case ELockType::ReadWrite:			dwFlags = 0;					break;
		}
		void* pBuffer;
		VerifyResult(m_pBuffer->Lock(uiOffset, uiSize, &pBuffer, dwFlags));
		return pBuffer;
	}
	//----------------------------------------------------------------------
	//	バッファのアンロック
	//----------------------------------------------------------------------
	void CDx9VertexBuffer::Unlock(void)
	{
		VerifyResult(m_pBuffer->Unlock());
	}
}
#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
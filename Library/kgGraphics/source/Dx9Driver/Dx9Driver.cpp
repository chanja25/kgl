//----------------------------------------------------------------------
//!
//!	@file	Dx9Drivcer.h
//!	@brief	DirectX9
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include "Dx9Driver/Dx9Device.h"
#include "Dx9Driver/Dx9Primitive.h"
#include "Dx9Driver/Dx9Command.h"
#include "Dx9Driver/Dx9Texture.h"
#include "Dx9Driver/Dx9Shader.h"
#include "Dx9Driver/Dx9RenderTarget.h"

namespace kgGraphics
{
	//======================================================================
	//	DirectX9用ドライバー
	//======================================================================
	class CDx9Driver
		: public IGraphicDriver
	{
	public:
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 Initialize(const GRAPHICS_INFO& info)
		{
			kgAssert(!m_pDevice.IsValid(), "既に初期化されています(CDx9Driver::Initialize)");
			kgl::TKGLPtr<CDx9Device> pDevice = new CDx9Device;
			if( pDevice->Initialize(info) == false )
			{
				return false;
			}

			m_pDevice				= pDevice;
			m_pPrimitiveManager		= new CDx9PrimitiveManager(pDevice);
			m_pTextureManager		= new CDx9TextureManager(pDevice);
			m_pRenderTargetManager	= new CDx9RenderTargetManager(pDevice);
			m_pShaderManager		= new CDx9ShaderManager(pDevice);
			m_pCommandCreator		= new CDx9Command(pDevice);
			return true;
		}

		//----------------------------------------------------------------------
		//	ドライバータイプの取得
		//----------------------------------------------------------------------
		s32 GetType(void)
		{
			return EDriverType::DirectX9;
		}
		//----------------------------------------------------------------------
		//	ドライバー名の取得
		//----------------------------------------------------------------------
		const c8* Name(void)
		{
			return "DirectX9";
		}
	};

	//----------------------------------------------------------------------
	//	DirectX9用ドライバー作成
	//----------------------------------------------------------------------
	b8 CreateDX9Driver(IGraphicDriver** ppDriver)
	{
		*ppDriver = new CDx9Driver;
		(*ppDriver)->AddRef();
		return true;
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
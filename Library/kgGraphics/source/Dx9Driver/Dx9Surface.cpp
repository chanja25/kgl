//----------------------------------------------------------------------
//!
//!	@file	Dx9Surface.cpp
//!	@brief	サーフェイス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Surface.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	サーフェイスのロック
	//----------------------------------------------------------------------
	void* CDx9Surface::Lock(s32 iType)
	{
		dword dwFlags = 0;
		switch( iType )
		{
		case ELockType::Read:		dwFlags = D3DLOCK_READONLY;	break;
		case ELockType::Write:		dwFlags = D3DLOCK_DISCARD;	break;
		case ELockType::ReadWrite:	dwFlags = 0;				break;
		}
		D3DLOCKED_RECT Lock;
		//	サーフェイス情報の取得
		VerifyResult(m_pSurface->LockRect(&Lock, null, dwFlags));

		return Lock.pBits;
	}
	//----------------------------------------------------------------------
	//	サーフェイスのアンロック
	//----------------------------------------------------------------------
	void CDx9Surface::Unlock(void)
	{
		VerifyResult(m_pSurface->UnlockRect());
	}
}
#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
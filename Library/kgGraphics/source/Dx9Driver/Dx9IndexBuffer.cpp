//----------------------------------------------------------------------
//!
//!	@file	Dx9IndexBuffer.cpp
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9IndexBuffer.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDx9IndexBuffer::CDx9IndexBuffer(void)
		: m_pBuffer(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CDx9IndexBuffer::Initialize(CDx9Device* pDevice, u32 uiSize, u8 ucType, b8 bDynamic)
	{
		(void)bDynamic;
		m_pBuffer = null;
		const D3DFORMAT Format[] =
		{
			D3DFMT_INDEX16,
			D3DFMT_INDEX32,
		};
		
		VerifyResult(pDevice->GetDevice()->CreateIndexBuffer(uiSize, 0, Format[ucType], D3DPOOL_MANAGED, m_pBuffer.GetReference(), null));

		m_ucType = ucType;
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CDx9IndexBuffer::Lock(u32 uiOffset, u32 uiSize, s32 iType)
	{
		dword dwFlags = 0;
		switch( iType )
		{
		case ELockType::Read:				dwFlags = D3DLOCK_READONLY;		break;
		case ELockType::Write:				dwFlags = D3DLOCK_DISCARD;		break;
		case ELockType::WriteNoOverWrite:	dwFlags = D3DLOCK_NOOVERWRITE;	break;
		case ELockType::ReadWrite:			dwFlags = 0;					break;
		}
		void* pBuffer;
		VerifyResult(m_pBuffer->Lock(uiOffset, uiSize, &pBuffer, dwFlags));
		return pBuffer;
	}
	//----------------------------------------------------------------------
	//	バッファのアンロック
	//----------------------------------------------------------------------
	void CDx9IndexBuffer::Unlock(void)
	{
		VerifyResult(m_pBuffer->Unlock());
	}
}
#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
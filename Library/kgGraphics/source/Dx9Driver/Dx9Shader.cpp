//----------------------------------------------------------------------
//!
//!	@file	Dx9Shader.cpp
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Shader.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CDx9VertexShader::Create(CDx9Device* pDevice, const void* pData)
	{
		const ShaderInfo* pInfo = (const ShaderInfo*)pData;
		VerifyResult(pDevice->GetDevice()->CreateVertexShader((const dword*)(pInfo + 1), m_pShader.GetReference()));

		return true;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CPixelShader::Create(CDx9Device* pDevice, const void* pData)
	{
		const ShaderInfo* pInfo = (const ShaderInfo*)pData;
		VerifyResult(pDevice->GetDevice()->CreatePixelShader((const dword*)(pInfo + 1), m_pShader.GetReference()));

		return true;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CDx9ShaderManager::CreateVertexShader(IVertexShader** ppShader, const void* pData)
	{
		kgl::TKGLPtr<CDx9VertexShader> pShader = new CDx9VertexShader;
		if( !pShader->Create(m_pDevice, pData) )
		{
			return false;
		}
		pShader->AddRef();
		*ppShader = pShader;
		return true;
	}
	//----------------------------------------------------------------------
	//	ピクセルシェーダー生成
	//----------------------------------------------------------------------
	b8 CDx9ShaderManager::CreatePixelShader(IPixelShader** ppShader, const void* pData)
	{
		kgl::TKGLPtr<CPixelShader> pShader = new CPixelShader;
		if( !pShader->Create(m_pDevice, pData) )
		{
			return false;
		}
		pShader->AddRef();
		*ppShader = pShader;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
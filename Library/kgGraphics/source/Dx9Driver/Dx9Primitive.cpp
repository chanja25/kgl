//----------------------------------------------------------------------
//!
//!	@file	Dx9Primitive.cpp
//!	@brief	Primitive関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Primitive.h"
#include "Dx9Driver/Dx9IndexBuffer.h"
#include "Dx9Driver/Dx9VertexBuffer.h"
#include "Dx9Driver/Dx9Declaration.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	インデックスバッファ生成
	//----------------------------------------------------------------------
	b8 CDx9PrimitiveManager::CreateIndexBuffer(IIndexBuffer** ppIndex, u32 uiSize, u8 ucType, b8 bDynamic)
	{
		kgl::TKGLPtr<CDx9IndexBuffer> pBuffer = new CDx9IndexBuffer;
		if( !pBuffer->Initialize(m_pDevice, uiSize, ucType, bDynamic) )
		{
			return false;
		}
		pBuffer->AddRef();
		*ppIndex = pBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	頂点バッファ生成
	//----------------------------------------------------------------------
	b8 CDx9PrimitiveManager::CreateVertexBuffer(IVertexBuffer** ppVertex, u32 uiSize, b8 bDynamic)
	{
		kgl::TKGLPtr<CDx9VertexBuffer> pBuffer = new CDx9VertexBuffer;
		if( !pBuffer->Initialize(m_pDevice, uiSize, bDynamic) )
		{
			return false;
		}
		pBuffer->AddRef();
		*ppVertex = pBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	頂点フォーマット設定生成
	//----------------------------------------------------------------------
	b8 CDx9PrimitiveManager::CreateDeclaration(IDeclaration** ppDeclaration, const DeclarationInfo* pDeclInfo)
	{
		kgl::TKGLPtr<CDx9Declaration> pDecl = new CDx9Declaration;
		if( !pDecl->Initialize(m_pDevice, pDeclInfo) )
		{
			return false;
		}
		pDecl->AddRef();
		*ppDeclaration = pDecl;
		return true;
	}
}

#endif	// ~#if USE_DIRECTX9

//======================================================================
//	END OF FILE
//======================================================================
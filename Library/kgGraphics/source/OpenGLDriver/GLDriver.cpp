//----------------------------------------------------------------------
//!
//!	@file	GLDrivcer.h
//!	@brief	OpenGL
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL

#include "OpenGLDriver/GLDevice.h"
#include "OpenGLDriver/GLPrimitive.h"
#include "OpenGLDriver/GLCommand.h"
#include "OpenGLDriver/GLTexture.h"
#include "OpenGLDriver/GLShader.h"
#include "OpenGLDriver/GLRenderTarget.h"

namespace kgGraphics
{
	//======================================================================
	//	OpenGL用ドライバー
	//======================================================================
	class CGLDriver
		: public IGraphicDriver
	{
	public:
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 Initialize(const GRAPHICS_INFO& info)
		{
			kgAssert(!m_pDevice.IsValid(), "既に初期化されています(CGLDriver::Initialize)");
			kgl::TKGLPtr<CGLDevice> pDevice = new CGLDevice;
			if( pDevice->Initialize(info) == false )
			{
				return false;
			}

			m_pDevice				= pDevice;
			m_pPrimitiveManager		= new CGLPrimitiveManager(pDevice);
			m_pTextureManager		= new CGLTextureManager(pDevice);
			m_pRenderTargetManager	= new CGLRenderTargetManager(pDevice);
			m_pShaderManager		= new CGLShaderManager(pDevice);
			m_pCommandCreator		= new CGLCommand(pDevice);
			return true;
		}

		//----------------------------------------------------------------------
		//	ドライバータイプの取得
		//----------------------------------------------------------------------
		s32 GetType(void)
		{
			return EDriverType::OpenGL;
		}
		//----------------------------------------------------------------------
		//	ドライバー名の取得
		//----------------------------------------------------------------------
		const c8* Name(void)
		{
			return "OpenGL";
		}
	};

	//----------------------------------------------------------------------
	//	DirectX9用ドライバー作成
	//----------------------------------------------------------------------
	b8 CreateOpenGLDriver(IGraphicDriver** ppDriver)
	{
		*ppDriver = new CGLDriver;
		(*ppDriver)->AddRef();
		return true;
	}
}

#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
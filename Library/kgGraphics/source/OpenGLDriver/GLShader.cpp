//----------------------------------------------------------------------
//!
//!	@file	GLShader.cpp
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLShader.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CGLShaderManager::CreateVertexShader(IVertexShader** ppShader, const void* pData)
	{
		kgl::TKGLPtr<CGLVertexShader> pShader = new CGLVertexShader;
		if( !pShader->Create(m_pDevice, pData) )
		{
			return false;
		}
		pShader->AddRef();
		*ppShader = pShader;
		return true;
	}
	//----------------------------------------------------------------------
	//	ピクセルシェーダー生成
	//----------------------------------------------------------------------
	b8 CGLShaderManager::CreatePixelShader(IPixelShader** ppShader, const void* pData)
	{
		kgl::TKGLPtr<CGLPixelShader> pShader = new CGLPixelShader;
		if( !pShader->Create(m_pDevice, pData) )
		{
			return false;
		}
		pShader->AddRef();
		*ppShader = pShader;
		return true;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CGLVertexShader::Create(CGLDevice* pDevice, const void* pData)
	{
		(void)pDevice;
		(void)pData;
		return true;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダー生成
	//----------------------------------------------------------------------
	b8 CGLPixelShader::Create(CGLDevice* pDevice, const void* pData)
	{
		(void)pDevice;
		(void)pData;
		return true;
	}
}

#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
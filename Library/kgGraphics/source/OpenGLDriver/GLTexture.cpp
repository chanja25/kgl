//----------------------------------------------------------------------
//!
//!	@file	GLTexture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLTexture.h"
#include "OpenGLDriver/GLSurface.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CGLTextureManager::CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData)
	{
		kgl::TKGLPtr<CGLTexture2D> pTexture = new CGLTexture2D;
		if( !pTexture->Create(m_pDevice, Info, pData) )
		{
			return false;
		}
		pTexture->AddRef();
		*ppTexture = pTexture;
		return true;
	}
	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CGLTextureManager::CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData)
	{
		kgl::TKGLPtr<CGLTexture2D> pTexture = new CGLTexture2D;
		if( !pTexture->CreateFromDDS(m_pDevice, pData) )
		{
			return false;
		}
		pTexture->AddRef();
		*ppTexture = pTexture;
		return true;
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CGLTexture2D::CGLTexture2D(void)
		: m_uiTexture(IndexNone)
	{}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CGLTexture2D::~CGLTexture2D(void)
	{
		if( m_uiTexture != IndexNone )
		{
			glDeleteTextures(1, &m_uiTexture);
		}
	}

	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CGLTexture2D::Create(CGLDevice* pDevice, const TextureInfo& Info, const void* pData)
	{
		(void)pDevice;

		GLenum eTarget = GL_TEXTURE_2D;
		//dwUsage |= (Info.iUsage & ETextureUsage::RenderTarget)?	GL_FRAMEBUFFER:	0;
		//dwUsage |= (Info.iUsage & ETextureUsage::Depth)?		GL_DEPTH:		0;
		//dwUsage |= (Info.iUsage & ETextureUsage::Dynamic)?		GL_TEXTURE_2D:	0;

		GLenum eFormat;
		GLenum eType;
		GLint iInternalFormat;
		switch( Info.iFormat )
		{
		case ETextureFormat::R8G8B8:		eFormat = GL_RGB;							eType = GL_UNSIGNED_INT_8_8_8_8_REV;	iInternalFormat = 3;	break;
		case ETextureFormat::A8R8G8B8:		eFormat = GL_RGBA8;							eType = GL_UNSIGNED_INT_8_8_8_8_REV;	iInternalFormat = 4;	break;
		case ETextureFormat::A16R16G16B16:	eFormat = GL_RGBA16;						eType = GL_UNSIGNED_SHORT;				iInternalFormat = 4;	break;
		case ETextureFormat::A16R16G16B16F:	eFormat = GL_RGBA16F;						eType = GL_FLOAT_RGBA16_NV;				iInternalFormat = 4;	break;
		case ETextureFormat::R5G6B5:		eFormat = GL_RGB565;						eType = GL_UNSIGNED_SHORT_5_6_5_REV;	iInternalFormat = 3;	break;
		case ETextureFormat::A1R5G5B5:		eFormat = GL_RGB5_A1;						eType = GL_UNSIGNED_SHORT_5_5_5_1;		iInternalFormat = 4;	break;
		case ETextureFormat::A4R4G4B4:		eFormat = GL_RGBA4;							eType = GL_UNSIGNED_SHORT_4_4_4_4_REV;	iInternalFormat = 4;	break;
		case ETextureFormat::A8:			eFormat = GL_ALPHA8;						eType = GL_UNSIGNED_BYTE;				iInternalFormat = 1;	break;
		case ETextureFormat::L8:			eFormat = GL_LUMINANCE;						eType = GL_UNSIGNED_BYTE;				iInternalFormat = 1;	break;
		case ETextureFormat::A8L8:			eFormat = GL_LUMINANCE_ALPHA;				eType = GL_UNSIGNED_SHORT_8_8_REV_APPLE;iInternalFormat = 2;	break;
		case ETextureFormat::R16F:			eFormat = GL_R16F;							eType = GL_FLOAT16_NV;					iInternalFormat = 1;	break;
		case ETextureFormat::R32F:			eFormat = GL_R32F;							eType = GL_FLOAT;						iInternalFormat = 1;	break;
		case ETextureFormat::DXT1:			eFormat = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;	eType = GL_UNSIGNED_BYTE;				iInternalFormat = 4;	break;
		case ETextureFormat::DXT3:			eFormat = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;	eType = GL_UNSIGNED_BYTE;			iInternalFormat = 4;	break;
		case ETextureFormat::DXT5:			eFormat = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;	eType = GL_UNSIGNED_BYTE;			iInternalFormat = 4;	break;
		case ETextureFormat::D16:			eFormat = GL_DEPTH_COMPONENT16;				eType = GL_UNSIGNED_SHORT;			iInternalFormat = 1;	break;
		case ETextureFormat::D32:			eFormat = GL_DEPTH_STENCIL;					eType = GL_UNSIGNED_INT;			iInternalFormat = 1;	break;
		case ETextureFormat::D24S8:			eFormat = GL_DEPTH24_STENCIL8_EXT;			eType = GL_UNSIGNED_INT_24_8_EXT;	iInternalFormat = 2;	break;
		default:
			ErrorTrace("指定されたFormatが対応していないか不正な値です(CDx9Texture2D::Create)");
			return false;
		}
		s32 iMipLevels = kgl::Max(1, Info.iMipLevels);

		glGenTextures(1, &m_uiTexture);
		glBindTexture(eTarget, m_uiTexture);

		glTexImage2D(
				eTarget,
				iMipLevels,
				iInternalFormat,
				Info.uiWidth,
				Info.uiHeight,
				0,
				eFormat,
				eType,
				pData
				);

		return true;
	}

	//----------------------------------------------------------------------
	//	DDSからテクスチャー生成
	//----------------------------------------------------------------------
	b8 CGLTexture2D::CreateFromDDS(CGLDevice* pDevice, const void* pData)
	{
		(void)pDevice;
		if( pData == null )
		{
			ErrorTrace("指定されたデータがnullです(CGLTexture2D::CreateFromDDS)");
			return false;
		}
		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャーのロック
	//----------------------------------------------------------------------
	void* CGLTexture2D::Lock(s32 iLevel, s32 iType)
	{
		(void)iLevel;
		(void)iType;

		GLenum eAccess = GL_READ_WRITE;
		switch( iType )
		{
		case ELockType::Read:		eAccess = GL_READ_ONLY;		break;
		case ELockType::Write:		eAccess = GL_WRITE_ONLY;	break;
		case ELockType::ReadWrite:	eAccess = GL_READ_WRITE;	break;
		}

		glBindTexture(GL_PIXEL_UNPACK_BUFFER_ARB, m_uiTexture);
		u8* pBuffer = (u8*)glMapBuffer(GL_TEXTURE_2D, eAccess);
		kgAssert(pBuffer != null, "バッファのLockに失敗しました(CGLIndexBuffer::Lock)");

		return pBuffer;
	}

	//----------------------------------------------------------------------
	//	テクスチャーのアンロック
	//----------------------------------------------------------------------
	void CGLTexture2D::Unlock(s32 iLevel)
	{
		(void)iLevel;
	}

	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CGLTexture2D::GetSurface(ISurfaceBase** ppSurface, s32 iLevel)
	{
		(void)ppSurface;
		(void)iLevel;
		//kgl::TKGLPtr<IDirect3DSurface9> pDxSurface;

		//m_pTexture->GetSurfaceLevel(iLevel, pDxSurface.GetReference());
		//if( pDxSurface == null )
		//{
		//	return false;
		//}

		//kgl::TKGLPtr<CSurface> pSurface = new CSurface;
		//SurfaceInfo Info;
		//Info.iFormat	= m_Info.iFormat;
		//Info.iUsage		= m_Info.iUsage;
		//Info.uiWidth	= m_Info.uiWidth  / (iLevel+1);
		//Info.uiHeight	= m_Info.uiHeight / (iLevel+1);
		//pSurface->Setup(Info, pDxSurface);

		//pSurface->AddRef();
		//*ppSurface = pSurface;
		return true;
	}
}

#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
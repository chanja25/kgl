//----------------------------------------------------------------------
//!
//!	@file	GLCommand.cpp
//!	@brief	コマンド生成
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLCommand.h"
#include "OpenGLDriver/GLDeclaration.h"
#include "OpenGLDriver/GLVertexBuffer.h"
#include "OpenGLDriver/GLIndexBuffer.h"
#include "OpenGLDriver/GLTexture.h"


namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CGLCommand::CGLCommand(kgl::TKGLPtr<CGLDevice> pDevice)
		: m_pDevice(pDevice)
		, m_uiDeclCount(0)
	{}
	//----------------------------------------------------------------------
	//	アルファテスト設定
	//----------------------------------------------------------------------
	void CGLCommand::SetAlphaTest(Command::AlphaTestInfo& Info)
	{
		const GLenum Func[] =
		{
			GL_NEVER,
			GL_NEVER,
			GL_ALWAYS,
			GL_LESS,
			GL_LEQUAL,
			GL_GREATER,
			GL_GEQUAL,
			GL_EQUAL,
			GL_NOTEQUAL
		};

		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	デプステストの有無を設定
		if( CheckInfo(AlphaTestInfo, iTest) )
		{
			Info.iTest = Info.iTest;
			if( bEnable )
			{
				glEnable(GL_ALPHA_TEST);
			}
			else
			{
				glDisable(GL_ALPHA_TEST);
			}
		}
		if( bEnable )
		{
			//	デプステストの内容を設定
			if( CheckInfo(AlphaTestInfo, iTest) ||
				CheckInfo(AlphaTestInfo, ucRef) )
				glAlphaFunc(Func[Info.iTest], (f32)Info.ucRef/255.0f);

			UpdateInfo(AlphaTestInfo);
		}
	}

	//----------------------------------------------------------------------
	//	デプステスト設定
	//----------------------------------------------------------------------
	void CGLCommand::SetDepthTest(Command::DepthTestInfo& Info)
	{
		const GLenum Func[] =
		{
			GL_NEVER,
			GL_NEVER,
			GL_ALWAYS,
			GL_LESS,
			GL_LEQUAL,
			GL_GREATER,
			GL_GEQUAL,
			GL_EQUAL,
			GL_NOTEQUAL
		};

		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	デプステストの有無を設定
		if( CheckInfo(DepthTestInfo, iTest) )
		{
			m_DepthTestInfo.iTest = Info.iTest;
			if( bEnable )
			{
				glEnable(GL_DEPTH_TEST);
			}
			else
			{
				glDisable(GL_DEPTH_TEST);
			}
		}
		if( bEnable )
		{
			//	デプステストの内容を設定
			if( CheckInfo(DepthTestInfo, iTest) )
				glDepthFunc(Func[Info.iTest]);
			if( CheckInfo(DepthTestInfo, bWrite) )
				glDepthMask(Info.bWrite);

			UpdateInfo(DepthTestInfo);
		}
	}

	//----------------------------------------------------------------------
	//	ステンシルテスト設定
	//----------------------------------------------------------------------
	void CGLCommand::SetStencilTest(Command::StencilTestInfo& Info)
	{
		const GLenum Func[] =
		{
			GL_NEVER,
			GL_NEVER,
			GL_ALWAYS,
			GL_LESS,
			GL_LEQUAL,
			GL_GREATER,
			GL_GEQUAL,
			GL_EQUAL,
			GL_NOTEQUAL,
		};
		const GLenum Op[] =
		{
			GL_KEEP,
			GL_ZERO,
			GL_REPLACE,
			GL_INCR,
			GL_DECR,
			GL_INVERT,
			GL_INCR_WRAP,
			GL_DECR_WRAP,
		};

		b8 bEnable = Info.iTest != Command::ETest::Disable;
		//	ステンシルテストの有無を設定
		if( CheckInfo(StencilTestInfo, iTest) )
		{
			m_StencilTestInfo.iTest = Info.iTest;
			if( bEnable )
			{
				glEnable(GL_STENCIL_TEST);
			}
			else
			{
				glDisable(GL_STENCIL_TEST);
			}
		}
		if( bEnable )
		{
			//	ステンシルテストの内容を設定
			if( CheckInfo(StencilTestInfo, iTest) ||
				CheckInfo(StencilTestInfo, ucRef) ||
				CheckInfo(StencilTestInfo, bWriteMask) )
			{
				glStencilFunc(Func[Info.iTest], Info.ucRef, Info.bWriteMask? 0: 0xff);
			}

			if( CheckInfo(StencilTestInfo, iPassOp) ||
				CheckInfo(StencilTestInfo, iFailOp) ||
				CheckInfo(StencilTestInfo, iDepthFailOp) )
			{
				glStencilOp(Op[Info.iFailOp], Op[Info.iDepthFailOp], Op[Info.iPassOp]);
			}

			UpdateInfo(StencilTestInfo);
		}
	}

	//----------------------------------------------------------------------
	//	ブレンド設定
	//----------------------------------------------------------------------
	void CGLCommand::SetBlend(Command::BlendInfo& Info)
	{
		const GLenum Op[] =
		{
			GL_FUNC_ADD,
			GL_FUNC_ADD,
			GL_FUNC_SUBTRACT,
			GL_FUNC_REVERSE_SUBTRACT,
			GL_MIN,
			GL_MAX,
		};
		const GLenum Param[] =
		{
			GL_ZERO,
			GL_ONE,
			GL_SRC_COLOR,
			GL_ONE_MINUS_SRC_COLOR,
			GL_DST_COLOR,
			GL_ONE_MINUS_DST_COLOR,
			GL_SRC_ALPHA,
			GL_ONE_MINUS_SRC_ALPHA,
			GL_DST_ALPHA,
			GL_ONE_MINUS_DST_ALPHA,
		};

		b8 bEnableAlphaBlend = Info.iAlphaOp != Command::EBlendOp::Disable;
		if( CheckInfo(BlendInfo, iColorOp) ||
			CheckInfo(BlendInfo, iAlphaOp) )
		{
			if( bEnableAlphaBlend )
			{
				glEnable(GL_BLEND);
			}
			else
			{
				glDisable(GL_BLEND);
			}
			m_BlendInfo.iColorOp = Info.iColorOp;
			m_BlendInfo.iAlphaOp = Info.iAlphaOp;
			glBlendEquationSeparate(Op[Info.iColorOp], Op[Info.iAlphaOp]);
		}
		if( !bEnableAlphaBlend )
		{
			RevertInfo(BlendInfo, iAlphaOp);
			RevertInfo(BlendInfo, iSrcAlpha);
			RevertInfo(BlendInfo, iDstAlpha);
		}
		if( CheckInfo(BlendInfo, iSrcColor) ||
			CheckInfo(BlendInfo, iDstColor) ||
			CheckInfo(BlendInfo, iSrcAlpha) ||
			CheckInfo(BlendInfo, iDstAlpha) )
		{
			glBlendFuncSeparate(Param[Info.iSrcColor], Param[Info.iDstColor], Param[Info.iSrcAlpha], Param[Info.iDstAlpha]);
		}

		UpdateInfo(BlendInfo);
	}

	//----------------------------------------------------------------------
	//	カリングモード設定
	//----------------------------------------------------------------------
	void CGLCommand::SetCullMode(Command::CullModeInfo& Info)
	{
		const GLenum Mode[] =
		{
			GL_FRONT,
			GL_BACK,
		};

		if( CheckInfo(CullModeInfo, iMode) )
		{
			b8 bEnable = (Info.iMode != Command::ECullMode::None);
			if( bEnable )
			{
				glEnable(GL_CULL_FACE);
				glCullFace(Mode[Info.iMode]);
			}
			else
			{
				glDisable(GL_CULL_FACE);
			}

			UpdateInfo(CullModeInfo);
		}
	}

	//----------------------------------------------------------------------
	//	テクスチャ設定
	//----------------------------------------------------------------------
	void CGLCommand::SetTexture(Command::TextureInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");
		if( CheckInfo(TextureInfo[Info.iStage], pTexture) )
		{
			GLuint uiTexture =  *(GLuint*)Info.pTexture->GetTexture();
			ActiveTexture(GL_TEXTURE0+Info.iStage);
			glBindTexture(GL_TEXTURE_2D, uiTexture);

			UpdateInfo(TextureInfo[Info.iStage]);
		}
	}

	//----------------------------------------------------------------------
	//	LODバイアス設定
	//----------------------------------------------------------------------
	void CGLCommand::SetLODBias(Command::LODBiasInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");
		ActiveTexture(GL_TEXTURE0+Info.iStage);
		//	todo:用確認
		if( CheckInfo(LODBiasInfo[Info.iStage], fBias) )
		{
			glTexEnvf(GL_TEXTURE_FILTER_CONTROL, GL_TEXTURE_LOD_BIAS, Info.fBias);
		}
		if( CheckInfo(LODBiasInfo[Info.iStage], iMaxMipLevel) )
		{
			glTexEnvi(GL_TEXTURE_FILTER_CONTROL, GL_TEXTURE_MAX_LOD, Info.iMaxMipLevel);
		}
		UpdateInfo(LODBiasInfo[Info.iStage]);
	}

	//----------------------------------------------------------------------
	//	サンプラーステート設定
	//----------------------------------------------------------------------
	void CGLCommand::SetSamplerState(Command::SamplerStateInfo& Info)
	{
		kgAssert(Info.iStage < TEXSTAGE_MAX, "Stageに指定できる数はTEXSTAGE_MAXまでです");
		ActiveTexture(GL_TEXTURE0+Info.iStage);
	
		const GLenum Filter[] =
		{
			GL_NEAREST,
			GL_LINEAR,
			GL_NEAREST_MIPMAP_LINEAR,
			GL_LINEAR_MIPMAP_LINEAR,
		};
		const GLenum Wrap[] =
		{
			GL_REPEAT,
			GL_MIRRORED_REPEAT,
			GL_CLAMP_TO_EDGE,
			GL_CLAMP_TO_BORDER,
		};
		//	todo:用確認
		if( Info.iFilterMip == 1 )
		{
			Info.iFilterMin += 2;
		}
		if( CheckInfo(SamplerStateInfo[Info.iStage], iFilterMin) )
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, Filter[Info.iFilterMin]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iFilterMag) )
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, Filter[Info.iFilterMag]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iWrapU) )
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, Wrap[Info.iWrapU]);
		if( CheckInfo(SamplerStateInfo[Info.iStage], iWrapV) )
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, Wrap[Info.iWrapV]);

		UpdateInfo(SamplerStateInfo[Info.iStage]);
	}

	//----------------------------------------------------------------------
	//	ビューポート設定
	//----------------------------------------------------------------------
	void CGLCommand::SetViewPort(Command::ViewPortInfo& Info)
	{
		if( CheckInfo(ViewPortInfo, iX) ||
			CheckInfo(ViewPortInfo, iY) ||
			CheckInfo(ViewPortInfo, iWidth) ||
			CheckInfo(ViewPortInfo, iHeight)||
			CheckInfo(ViewPortInfo, fMinZ)||
			CheckInfo(ViewPortInfo, fMaxZ) )
		{
			glViewport(Info.iX, Info.iY, Info.iWidth, Info.iHeight);
			glDepthRange(Info.fMinZ, Info.fMaxZ);

			UpdateInfo(ViewPortInfo);
		}
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット設定
	//----------------------------------------------------------------------
	void CGLCommand::SetRenderTarget(Command::RenderTargetInfo& Info)
	{
		(void)Info;
		//todo:未実装
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット設定
	//----------------------------------------------------------------------
	void CGLCommand::SetDepthTarget(Command::DepthTargetInfo& Info)
	{
		(void)Info;
		//todo:未実装
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	//----------------------------------------------------------------------
	//	頂点フォーマット設定
	//----------------------------------------------------------------------
	void CGLCommand::SetDeclaration(Command::DeclarationInfo& Info)
	{
		if( CheckInfo(DeclarationInfo, pDeclaration) )
		{
			m_bUpdateDecl = true;
			UpdateInfo(DeclarationInfo);
		}
	}

	//----------------------------------------------------------------------
	//	頂点設定
	//----------------------------------------------------------------------
	void CGLCommand::SetVertex(Command::VertexBindInfo& Info)
	{
		GLuint uiBuffer = *(GLuint*)Info.pVertex->GetBuffer();
		BindBuffer<GL_ARRAY_BUFFER>(uiBuffer);
	}

	//----------------------------------------------------------------------
	//	インデックス設定
	//----------------------------------------------------------------------
	void CGLCommand::SetIndex(Command::IndexBindInfo& Info)
	{
		GLuint uiBuffer = *(GLuint*)Info.pIndex->GetBuffer();
		BindBuffer<GL_ELEMENT_ARRAY_BUFFER>(uiBuffer);
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CGLCommand::DrawPrimitiveUP(Command::DrawPrimitiveInfo& Info)
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		SetDeclaration(m_DeclarationInfo, Info.iStride, Info.pVertex);

		const GLenum Primitive[] =
		{
			GL_POINTS,
			GL_LINES,
			GL_LINE_STRIP,
			GL_TRIANGLES,
			GL_TRIANGLE_STRIP,
			GL_TRIANGLE_FAN,
		};
		glDrawArrays(Primitive[Info.iPrimitive], 0, Info.iCount);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CGLCommand::DrawPrimitive(Command::DrawInfo& Info)
	{
		(void)Info;
	}
	//----------------------------------------------------------------------
	//	描画(インデックス有り)
	//----------------------------------------------------------------------
	void CGLCommand::DrawIndexedPrimitiveUP(Command::DrawIndexPrimitiveInfo& Info)
	{
		SetDeclaration(m_DeclarationInfo, Info.iStride, Info.pVertex);

		const GLenum Primitive[] =
		{
			GL_POINTS,
			GL_LINES,
			GL_LINE_STRIP,
			GL_TRIANGLES,
			GL_TRIANGLE_STRIP,
			GL_TRIANGLE_FAN,
		};
		glDrawElements(Primitive[Info.iPrimitive], Info.iCount, GL_UNSIGNED_SHORT, Info.pIndex);
	}
	//----------------------------------------------------------------------
	//	描画(インデックス有り)
	//----------------------------------------------------------------------
	void CGLCommand::DrawIndexedPrimitive(Command::DrawIndexInfo& Info)
	{
		(void)Info;
	}

	//----------------------------------------------------------------------
	//	頂点シェーダ設定
	//----------------------------------------------------------------------
	void CGLCommand::SetVertexShader(Command::VertexShaderInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}

	//----------------------------------------------------------------------
	//	頂点シェーダパラメータ設定
	//----------------------------------------------------------------------
	void CGLCommand::SetVertexShaderParam(Command::VertexShaderParamInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}

	//----------------------------------------------------------------------
	//	頂点シェーダ設定
	//----------------------------------------------------------------------
	void CGLCommand::SetPixelShader(Command::PixelShaderInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}

	//----------------------------------------------------------------------
	//	頂点シェーダパラメータ設定
	//----------------------------------------------------------------------
	void CGLCommand::SetPixelShaderParam(Command::PixelShaderParamInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}

	//----------------------------------------------------------------------
	//	ターゲットのクリア
	//----------------------------------------------------------------------
	void CGLCommand::ClearTarget(Command::ClearTargetInfo& Info)
	{
		GLbitfield mask = 0;
		mask |= (Info.iTarget & Command::EClearTarget::Color)?   GL_COLOR_BUFFER_BIT:	0;
		mask |= (Info.iTarget & Command::EClearTarget::Depth)?   GL_DEPTH_BUFFER_BIT:	0;
		mask |= (Info.iTarget & Command::EClearTarget::Stencil)? GL_STENCIL_BUFFER_BIT:	0;

		f32 r = (f32)((Info.uiColor >> 24) & 0xff) / 255.0f;
		f32 g = (f32)((Info.uiColor >> 16) & 0xff) / 255.0f;
		f32 b = (f32)((Info.uiColor >>  8) & 0xff) / 255.0f;
		f32 a = (f32)((Info.uiColor >>  0) & 0xff) / 255.0f;
		glClearColor(r, g, b, a);
		glClearDepth(Info.fDepth);
		glClearStencil(Info.iStencil);
		glClear(mask);
	}
	//----------------------------------------------------------------------
	//	シザリング設定
	//----------------------------------------------------------------------
	void CGLCommand::SetScissoring(Command::ScissoringInfo& Info)
	{
		if( CheckInfo(ScissoringInfo, iX) ||
			CheckInfo(ScissoringInfo, iY) ||
			CheckInfo(ScissoringInfo, iWidth) ||
			CheckInfo(ScissoringInfo, iHeight) )
		{
			glScissor(Info.iX, Info.iY, Info.iWidth, Info.iHeight);

			if( (Info.iX != 0) ||
				(Info.iY != 0) ||
				(Info.iWidth != kgl::Config::ResolutionWidth) ||
				(Info.iHeight != kgl::Config::ResolutionHeight) )
			{
				glEnable(GL_SCISSOR_TEST);
			}
			else
			{
				glDisable(GL_SCISSOR_TEST);
			}
			UpdateInfo(ScissoringInfo);
		}
	}
	//----------------------------------------------------------------------
	//	ビュー行列設定
	//----------------------------------------------------------------------
	void CGLCommand::SetView(Command::ViewInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}
	//----------------------------------------------------------------------
	//	プロジェクション行列設定
	//----------------------------------------------------------------------
	void CGLCommand::SetProjection(Command::ProjectionInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}
	//----------------------------------------------------------------------
	//	ワールド行列設定
	//----------------------------------------------------------------------
	void CGLCommand::SetWorld(Command::WorldInfo& Info)
	{
		(void)Info;
		//todo:未実装
	}
	//----------------------------------------------------------------------
	//	頂点フォーマットの設定
	//----------------------------------------------------------------------
	void CGLCommand::SetDeclaration(Command::DeclarationInfo& Info, s32 iStride, const void* pVertex)
	{
		if( !m_bUpdateDecl )return;
		m_bUpdateDecl = false;

		GLDeclarationInfo* pInfo = (GLDeclarationInfo*)Info.pDeclaration->GetDeclaration();

		const GLint ValueCount[] =
		{
			1,	//Float1
			2,	//Float2
			3,	//Float3
			4,	//Float4
			4,	//Color
			4,	//UByte4
			2,	//Short2
			4,	//Short4
			4,	//UByte4N
			2,	//Short2N
			4,	//Short4N
			2,	//UShort2N
			4,	//UShort4N
			2,	//Float16_2
			4,	//Float16_4
		};
		const GLenum Type[] =
		{
			GL_FLOAT,			//Float1
			GL_FLOAT,			//Float2
			GL_FLOAT,			//Float3
			GL_FLOAT,			//Float4
			GL_UNSIGNED_BYTE,	//Color
			GL_UNSIGNED_BYTE,	//UByte4
			GL_SHORT,			//Short2
			GL_SHORT,			//Short4
			GL_UNSIGNED_BYTE,	//UByte4N
			GL_SHORT,			//Short2N
			GL_SHORT,			//Short4N
			GL_UNSIGNED_SHORT,	//UShort2N
			GL_UNSIGNED_SHORT,	//UShort4N
			GL_FLOAT16_NV,		//Float16_2
			GL_FLOAT16_NV,		//Float16_4
		};
		const GLboolean Normalize[] =
		{
			false,	//Float1
			false,	//Float2
			false,	//Float3
			false,	//Float4
			true,	//Color
			false,	//UByte4
			false,	//Short2
			false,	//Short4
			true,	//UByte4N
			true,	//Short2N
			true,	//Short4N
			true,	//UShort2N
			true,	//UShort4N
			false,	//Float16_2
			false,	//Float16_4
		};
		u32 i;
		u8* pOffset = (u8*)pVertex;
		for( i = 0; i < pInfo->uiCount; i++ )
		{
			DeclarationInfo& DeclInfo = pInfo->pDeclInfo[i];
			glEnableVertexAttribArray(i);
			glVertexAttribPointer(i, ValueCount[DeclInfo.ucType], Type[DeclInfo.ucType], Normalize[DeclInfo.ucType], iStride, pOffset+DeclInfo.usOffset);
		}
		for( ; i < m_uiDeclCount; i++ )
		{
			glDisableVertexAttribArray(i);
		}
		m_uiDeclCount = pInfo->uiCount;
	}
}

#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	GLTexture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLRenderTarget.h"
#include "OpenGLDriver/GLTexture.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	レンダーターゲット生成
	//----------------------------------------------------------------------
	b8 CGLRenderTargetManager::CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info)
	{
		kgl::TKGLPtr<CGLRenderTarget> pRenderTarget = new CGLRenderTarget;
		if( !pRenderTarget->Create(m_pDevice, Info) )
		{
			return false;
		}

		pRenderTarget->AddRef();
		*ppRenderTarget = pRenderTarget;
		return true;
	}

	//----------------------------------------------------------------------
	//	レンダーターゲット生成
	//----------------------------------------------------------------------
	b8 CGLRenderTarget::Create(CGLDevice* pDevice, const RenderTargetInfo& Info)
	{
		(void)pDevice;
		(void)Info;
		//TextureInfo TexInfo;
		//TexInfo.iType		= ETextureType::Normal;
		//TexInfo.iUsage		= Info.iUsage;
		//TexInfo.iFormat		= Info.iFormat;
		//TexInfo.uiWidth		= Info.uiWidth;
		//TexInfo.uiHeight	= Info.uiHeight;
		//TexInfo.iMipLevels	= 1;

		//m_Info = Info;

		//kgl::TKGLPtr<CTexture2D> pRenderTarget = new CTexture2D;
		//if( !pRenderTarget->Create(pDevice, TexInfo) )
		//{
		//	return false;
		//}
		//m_pRenderTarget = pRenderTarget;
		return true;
	}

	//----------------------------------------------------------------------
	//	レンダーターゲットのロック
	//----------------------------------------------------------------------
	void* CGLRenderTarget::Lock(s32 iType)
	{
		(void)iType;
		return null;
	}
	//----------------------------------------------------------------------
	//	レンダーターゲットのアンロック
	//----------------------------------------------------------------------
	void CGLRenderTarget::Unlock(void)
	{
	}
	//----------------------------------------------------------------------
	//	レンダーターゲット(テクスチャー)の取得
	//----------------------------------------------------------------------
	void CGLRenderTarget::GetRenderTarget(ITextureBase** ppTexture)
	{
		(void)ppTexture;
	}
	//----------------------------------------------------------------------
	//	サーフェイスの取得
	//----------------------------------------------------------------------
	b8 CGLRenderTarget::GetSurface(ISurfaceBase** ppSurface)
	{
		(void)ppSurface;
		return true;
	}
}

#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
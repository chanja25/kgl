//----------------------------------------------------------------------
//!
//!	@file	GLIndexBuffer.cpp
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLDeclaration.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CGLDeclaration::Initialize(CGLDevice* pDevice, const DeclarationInfo* pDecl)
	{
		(void)pDevice;
		u32 uiCount = 0;
		//	KGG_DECL_END()までの数を算出
		while( (pDecl + uiCount)->usStream != 0xffff )
		{
			uiCount ++;
			kgAssert(uiCount < 100, "Declationの設定でKGG_DECL_END()が指定されておりません(CGLDeclaration::Initialize)");
		}
		if( uiCount == 0 )return false;

		DeclarationInfo* pDeclInfo = new DeclarationInfo[uiCount];
		kgMemcpy(pDeclInfo, pDecl, sizeof(DeclarationInfo) * uiCount);

		m_DeclInfo.uiCount		= uiCount;
		m_DeclInfo.pDeclInfo	= pDeclInfo;
		return true;
	}
}
#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
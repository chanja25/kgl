//----------------------------------------------------------------------
//!
//!	@file	GLPrimitive.cpp
//!	@brief	Primitive関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLPrimitive.h"
#include "OpenGLDriver/GLIndexBuffer.h"
#include "OpenGLDriver/GLVertexBuffer.h"
#include "OpenGLDriver/GLDeclaration.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	インデックスバッファ生成
	//----------------------------------------------------------------------
	b8 CGLPrimitiveManager::CreateIndexBuffer(IIndexBuffer** ppIndex, u32 uiSize, u8 ucType, b8 bDynamic)
	{
		kgl::TKGLPtr<CGLIndexBuffer> pBuffer = new CGLIndexBuffer;
		if( !pBuffer->Initialize(m_pDevice, uiSize, ucType, bDynamic) )
		{
			return false;
		}
		pBuffer->AddRef();
		*ppIndex = pBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	頂点バッファ生成
	//----------------------------------------------------------------------
	b8 CGLPrimitiveManager::CreateVertexBuffer(IVertexBuffer** ppVertex, u32 uiSize, b8 bDynamic)
	{
		kgl::TKGLPtr<CGLVertexBuffer> pBuffer = new CGLVertexBuffer;
		if( !pBuffer->Initialize(m_pDevice, uiSize, bDynamic) )
		{
			return false;
		}
		pBuffer->AddRef();
		*ppVertex = pBuffer;
		return true;
	}
	//----------------------------------------------------------------------
	//	頂点フォーマット設定生成
	//----------------------------------------------------------------------
	b8 CGLPrimitiveManager::CreateDeclaration(IDeclaration** ppDeclaration, const DeclarationInfo* pDeclInfo)
	{
		kgl::TKGLPtr<CGLDeclaration> pDecl = new CGLDeclaration;
		if( !pDecl->Initialize(m_pDevice, pDeclInfo) )
		{
			return false;
		}
		pDecl->AddRef();
		*ppDeclaration = pDecl;
		return true;
	}
}

#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	GLDevice.h
//!	@brief	OpenGL
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#include "OpenGLDriver/GLDevice.h"
//#include "OpenGLDriver/GLSurface.h"

//	OpenGLの拡張関数を初期化
extern b8 InitGLFunc(void);

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CGLDevice::CGLDevice(void)
	{}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CGLDevice::~CGLDevice(void)
	{
		Finalize();
	}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CGLDevice::Initialize(const GRAPHICS_INFO& info)
	{
		HWND hWnd;
		HDC hDC;
		HGLRC hGLRC;

		hWnd	= (HWND)info.pHandle;
		hDC		= GetDC(hWnd);

		PIXELFORMATDESCRIPTOR PixelFormat =
		{
			sizeof(PIXELFORMATDESCRIPTOR),
			1,
			(PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER),
			PFD_TYPE_RGBA,
			32,				// color
			0, 0,			// R
			0, 0,			// G
			0, 0,			// B
			0, 0,			// A
			0, 0, 0, 0, 0,	// AC R G B A
			24,				// depth
			8,				// stencil
			0,				// aux
			0,				// layertype
			0,				// reserved
			0,				// layermask
			0,				// visiblemask
			0				// damagemask
		};
		s32 iPixelFormat = ChoosePixelFormat(hDC, &PixelFormat);
		SetPixelFormat(hDC, iPixelFormat, &PixelFormat);

		hGLRC = wglCreateContext(hDC);
		if( !wglMakeCurrent(hDC, hGLRC) )
		{
			return false;
		}
		m_DrawContext.hWnd	= hWnd;
		m_DrawContext.hDC	= hDC;
		m_DrawContext.hGLRC	= hGLRC;

		InitGLFunc();

		return true;
	}
	//----------------------------------------------------------------------
	//	終了処理
	//----------------------------------------------------------------------
	void CGLDevice::Finalize(void)
	{
		wglMakeCurrent(null, null);
		wglDeleteContext(m_DrawContext.hGLRC);
		ReleaseDC(m_DrawContext.hWnd, m_DrawContext.hDC);
	}
	//----------------------------------------------------------------------
	//	描画シーン開始
	//----------------------------------------------------------------------
	void CGLDevice::BeginScene(void)
	{
	}
	//----------------------------------------------------------------------
	//	描画シーン終了
	//----------------------------------------------------------------------
	void CGLDevice::EndScene(void)
	{
		SwapBuffers(m_DrawContext.hDC);
	}
	//----------------------------------------------------------------------
	//	GPUの処理終了待ち
	//----------------------------------------------------------------------
	void CGLDevice::FlushCommand(void)
	{
		glFlush();
	}
	//----------------------------------------------------------------------
	//	バックバッファの取得
	//----------------------------------------------------------------------
	void CGLDevice::GetBackBuffer(ISurfaceBase** ppBackBuffer)
	{
		(void)ppBackBuffer;
	}
	//----------------------------------------------------------------------
	//	デプスバッファの取得
	//----------------------------------------------------------------------
	void CGLDevice::GetDepthBuffer(ISurfaceBase** ppDepthBuffer)
	{
		(void)ppDepthBuffer;
	}
	//----------------------------------------------------------------------
	//	VSyncの有無を設定
	//----------------------------------------------------------------------
	void CGLDevice::SetVSync(b8 bEnable)
	{
		(void)bEnable;
	}
}

#endif	// ~#if USE_OPNENGL

//======================================================================
//	END OF FILE
//======================================================================
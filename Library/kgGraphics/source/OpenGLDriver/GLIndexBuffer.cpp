//----------------------------------------------------------------------
//!
//!	@file	GLIndexBuffer.cpp
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgGraphics.h"

#if	USE_OPENGL
#include "OpenGLDriver/GLIndexBuffer.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CGLIndexBuffer::CGLIndexBuffer(void)
		: m_uiBuffer(IndexNone)
		, m_uiSize(0)
	{}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CGLIndexBuffer::~CGLIndexBuffer(void)
	{
		if( m_uiBuffer != IndexNone )
		{
			glDeleteBuffers(1, &m_uiBuffer);
		}
	}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CGLIndexBuffer::Initialize(CGLDevice* pDevice, u32 uiSize, u8 ucType, b8 bDynamic)
	{
		(void)pDevice;
		m_uiSize = uiSize;
		m_ucType = ucType;

		glGenBuffers(1, &m_uiBuffer);
		BindBuffer<GL_ELEMENT_ARRAY_BUFFER>(m_uiBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, uiSize, null, bDynamic? GL_STREAM_DRAW: GL_STATIC_DRAW);
		return true;
	}

	//----------------------------------------------------------------------
	//	バッファのロック
	//----------------------------------------------------------------------
	void* CGLIndexBuffer::Lock(u32 uiOffset, u32 uiSize, s32 iType)
	{
		kgAssert(m_bLock, "ロック済みのバッファです(CGLIndexBuffer::Lock)");
		m_bLock = true;

		if( m_uiSize-uiOffset < uiSize )
		{
			kgAssert(false, kgl::CharSet::Format("引数がバッファに対して有効な値ではありません[offset:%u, size:%u, type:%d](CGLIndexBuffer::Lock)", uiOffset, uiSize, iType));
			return null;
		}
		GLenum eAccess = GL_READ_WRITE;
		switch( iType )
		{
		case ELockType::Read:		eAccess = GL_READ_ONLY;		break;
		case ELockType::Write:		eAccess = GL_WRITE_ONLY;	break;
		case ELockType::ReadWrite:	eAccess = GL_READ_WRITE;	break;
		}

		BindBuffer<GL_ELEMENT_ARRAY_BUFFER>(m_uiBuffer);
		u8* pBuffer = (u8*)glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, eAccess);
		kgAssert(pBuffer != null, "バッファのLockに失敗しました(CGLIndexBuffer::Lock)");

		return pBuffer + uiOffset;
	}
	//----------------------------------------------------------------------
	//	バッファのアンロック
	//----------------------------------------------------------------------
	void CGLIndexBuffer::Unlock(void)
	{
		if( m_bLock )
		{
			VerifyResult(glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER));
			m_bLock = false;
		}
	}
}
#endif	// ~#if USE_OPENGL

//======================================================================
//	END OF FILE
//======================================================================
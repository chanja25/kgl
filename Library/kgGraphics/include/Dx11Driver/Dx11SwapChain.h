//----------------------------------------------------------------------
//!
//!	@file	Dx11SwapChain.h
//!	@brief	スワップチェイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_SWAPCHAIN_H__
#define	__DX11_SWAPCHAIN_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"
#include "Dx11Surface.h"

namespace kgGraphics
{
	//======================================================================
	//!	スワップチェインクラス
	//======================================================================
	class CDx11SwapChain
		: public ISwapChainBase
	{
	public:
		//!	スワップチェイン生成
		//!	@param pDevice	[in] デバイス
		//!	@param pWindow	[in]  ウィンドウ
		//!	@return 結果
		b8 Create(CDx11Device* pDevice, kgl::Window::IWindow* pWindow);

	public:
		//!	描画内容の反映
		//!	@return 結果
		b8 Present(bool bVsync);

		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface);

	public:
		//!	Dx11スワップチェインの取得
		//!	@return Dx11スワップチェイン
		kgl::TKGLPtr<IDXGISwapChain> GetSwapChain(void) { return m_pSwapChain; }

	private:
		kgl::TKGLPtr<IDXGISwapChain>			m_pSwapChain;	//!< スワップチェイン
		kgl::TKGLPtr<ID3D11RenderTargetView>	m_pView;		//!< 描画ビュー
		kgl::TKGLPtr<CDx11Surface>				m_pSurface;		//!< サーフェイス
		kgl::TKGLPtr<kgl::Window::IWindow>		m_pWindow;		//!< ウィンドウ
	};
}

#endif	// __DX11_SWAPCHAIN_H__
//======================================================================
//	END OF FILE
//======================================================================
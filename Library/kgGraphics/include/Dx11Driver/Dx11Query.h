//----------------------------------------------------------------------
//!
//!	@file	Dx11Query.h
//!	@brief	クエリ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_QUERY_H__
#define	__DX11_QUERY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	クエリクラス
	//======================================================================
	class CDx11Query
		: public IQueryBase
	{
	public:
		//!	クエリ生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] クエリ情報
		//!	@return 結果
		b8 Create(CDx11Device* pDevice, const QueryInfo& Info);

		//!	クエリ発行
		void Begin(void);
		//!	発行したクエリの終了
		void End(void);

		//!	データの取得
		//!	@param pData	[out] データを受け取るバッファ
		//!	@param uiSize	[in]  バッファのサイズ
		//!	@return 結果
		b8 GetData(void* pData, u32 uiSize);

	public:
		//!	クエリ取得
		//!	@return クエリ
		INLINE void* GetQuery(void) { return m_pQuery; }

	private:
		kgl::TKGLPtr<CDx11Device>		m_pDevice;		//!< デバイス
		kgl::TKGLPtr<ID3D11Query>		m_pQuery;		//!< クエリ
	};
}

#endif	// __DX11_QUERY_H__
//======================================================================
//	END OF FILE
//======================================================================
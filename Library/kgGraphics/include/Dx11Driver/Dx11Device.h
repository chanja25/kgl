//----------------------------------------------------------------------
//!
//!	@file	Dx11Device.h
//!	@brief	DirectX11
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_DEVICE_H__
#define	__DX11_DEVICE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include <d3d11.h>

#if	KG_GRAPHICS_DEBUG
#define	VerifyResult(x)	kgAssert(SUCCEEDED(x), #x)
#else	// ~#if KG_GRAPHICS_DEBUG
#define	VerifyResult(x)	(x)
#endif	// ~#if KG_GRAPHICS_DEBUG

namespace kgGraphics
{
	class CDx11SwapChain;
	//======================================================================
	//	DirectX11用Device
	//======================================================================
	class CDx11Device
		: public IGraphicDevice
	{
	public:
		//!	コンストラクタ
		CDx11Device(void);
		//!	デストラクタ
		~CDx11Device(void);

	public:
		//!	初期化
		//!	@param info	[in] グラフィック設定
		//!	@return 結果
		b8 Initialize(const GRAPHICS_INFO& info);
		//!	終了処理
		void Finalize(void);

	public:
		//!	描画シーン開始
		void BeginScene(void);
		//!	描画シーン終了
		void EndScene(void);

	public:
		//!	GPUのコマンドを実行する
		void FlushCommand(void);
		//!	GPUの処理終了待ち
		void WaitForCommand(void);

	public:
		//!	バックバッファの取得
		//!	@param ppBackBuffer [out] バックバッファ
		void GetBackBuffer(ISurfaceBase** ppBackBuffer);
		//!	デプスバッファの取得
		//!	@param ppDepthBuffer [out] デプスバッファ
		void GetDepthBuffer(ISurfaceBase** ppDepthBuffer);

		//!	VSyncの有無を設定
		//!	@param bEnable	[in] VSyncの有無
		void SetVSync(b8 bEnable);

	public:
		//!	クエリー生成
		//!	@param ppQuery	[out] クエリ
		//!	@param Info		[in]  クエリ情報
		//!	@return 結果
		b8 CreateQuery(IQueryBase** ppQuery, const QueryInfo& Info);
		//!	スワップチェインの生成
		//!	@param ppSwapChain	[out] スワップチェイン
		//!	@param pWindow		[in]  ウィンドウ
		//!	@return 結果
		b8 CreateSwapChain(ISwapChainBase** ppSwapChain, kgl::Window::IWindow* pWindow);

	public:
		//!	デバイスの取得
		//!	@return Direct3DDevice
		INLINE ID3D11Device* GetDevice(void) { return m_pDevice; }
		//!	デバイスコンテキストの取得
		//!	@return ID3D11DeviceContext
		INLINE ID3D11DeviceContext* DeviceContext(void) { return m_pDeviceContext; }
		//!	ドライバータイプの取得
		//!	@return D3D_DRIVER_TYPE
		INLINE D3D_DRIVER_TYPE DriverType(void) { return m_eType; }
		//!	機能レベルの取得
		//!	@return D3D_FEATURE_LEVEL
		INLINE D3D_FEATURE_LEVEL FeatureLevel(void) { return m_eFeatureLevel; }
		//!	レンダーターゲットの取得
		//!	@return レンダーターゲット
		INLINE ID3D11RenderTargetView* RenderTargetView(void) { return m_pRenderTargetView; }
		//!	深度バッファの取得
		//!	@return 深度バッファ
		INLINE ID3D11DepthStencilView* DepthStencilView(void) { return m_pDepthStencilView; }
		//!	生成クラスの取得
		//!	@return 生成クラス
		INLINE IDXGIFactory* Factory(void) { return m_pFactory; }

	private:
		kgl::TKGLPtr<kgl::Window::IWindow>		m_pWindow;				//!< ウィンドウ
		kgl::TKGLPtr<ID3D11Device>				m_pDevice;				//!< Direct3Dデバイス
		kgl::TKGLPtr<IDXGIFactory>				m_pFactory;				//!< 生成クラス
		kgl::TKGLPtr<CDx11SwapChain>			m_pSwapChain;			//!< スワップチェイン
		kgl::TKGLPtr<ID3D11DeviceContext>		m_pDeviceContext;		//!< デバイスコンテキスト
		kgl::TKGLPtr<ID3D11RenderTargetView>	m_pRenderTargetView;	//!< 描画ビュー
		kgl::TKGLPtr<ID3D11DepthStencilView>	m_pDepthStencilView;	//!< 深度ビュー
		kgl::TKGLPtr<ID3D11Texture2D>			m_pDepthStencil;		//!< 深度バッファ
		D3D_DRIVER_TYPE							m_eType;				//!< デバイスタイプ
		D3D_FEATURE_LEVEL						m_eFeatureLevel;		//!< 機能レベル
		BITFIELD								m_bVsync: 1;			//!< VSyncを行うか？
	};
}

#endif	// __DX11_DEVICE_H__
//======================================================================
//	END OF FILE
//======================================================================
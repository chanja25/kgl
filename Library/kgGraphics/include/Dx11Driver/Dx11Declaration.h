//----------------------------------------------------------------------
//!
//!	@file	Dx11Declaration.h
//!	@brief	頂点宣言
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_DECLARATION_H__
#define	__DX11_DECLARATION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"
#include "Dx11Shader.h"


namespace kgGraphics
{
	//======================================================================
	//!	頂点フォーマット設定用クラス
	//======================================================================
	class CDx11Declaration
		: public IDeclaration
	{
	public:
		//!	コンストラクタ
		CDx11Declaration(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param pDecl	[in] 頂点宣言
		//!	@return 結果
		b8 Initialize(CDx11Device* pDevice, const DeclarationInfo* pDecl);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetDeclaration(void) { return m_pLayout; }
		//!	レイアウト情報
		//!	@param pShader	[in] 頂点シェーダー
		//!	@return レイアウト情報
		ID3D11InputLayout* GetLayout(kgl::TKGLPtr<CDx11VertexShader> pShader);

	private:
		typedef kgl::algorithm::vector<kgl::TKGLPtr<CDx11VertexShader>>	ShaderList;
		typedef kgl::algorithm::vector<kgl::TKGLPtr<ID3D11InputLayout>>	LayoutList;

		kgl::TKGLPtr<CDx11Device>					m_pDevice;		//!< デバイス
		s32											m_iCount;		//!< 情報数
		kgl::TSmartPtr<D3D11_INPUT_ELEMENT_DESC>	m_pLayout;		//!< レイアウト情報
		LayoutList									m_pLayoutList;	//!< 入力レイアウト
		ShaderList									m_pShaderList;	//!< シェーダーリスト
	};
}

#endif	// __DX11_DECLARATION_H__
//======================================================================
//	END OF FILE
//======================================================================
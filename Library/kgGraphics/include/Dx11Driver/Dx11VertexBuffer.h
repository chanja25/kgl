//----------------------------------------------------------------------
//!
//!	@file	Dx11VertexBuffer.h
//!	@brief	頂点バッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_VERTEXBUFFER_H__
#define	__DX11_VERTEXBUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	頂点バッファ
	//======================================================================
	class CDx11VertexBuffer
		: public IVertexBuffer
	{
	public:
		//!	コンストラクタ
		CDx11VertexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param uiSize	[in] バッファサイズ
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 Initialize(CDx11Device* pDevice, u32 uiSize, b8 bDynamic);

	public:
		//!	バッファのロック
		//!	@param uiOffset		[in] ロックするバッファのオフセット
		//!	@param uiSize		[in] ロックするバッファサイズ
		//!	@param iType		[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		void* Lock(u32 uiOffset, u32 uiSize, s32 iType);
		//!	バッファのアンロック
		void Unlock(void);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetBuffer(void) { return m_pBuffer; }

	private:
		kgl::TKGLPtr<CDx11Device>	m_pDevice;	//!< デバイス
		kgl::TKGLPtr<ID3D11Buffer>	m_pBuffer;	//!< 頂点バッファ
	};
}

#endif	// __DX11_VERTEXBUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
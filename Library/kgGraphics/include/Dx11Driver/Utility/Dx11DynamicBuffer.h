//----------------------------------------------------------------------
//!
//!	@file	Dx11DynamicBuffer.h
//!	@brief	動的バッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_DYNAMIC_BUFFER_H__
#define	__DX11_DYNAMIC_BUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgGraphics.h"
#include "../Dx11Device.h"
#include "../Dx11VertexBuffer.h"
#include "../Dx11IndexBuffer.h"

namespace kgGraphics
{
	//======================================================================
	//!	動的頂点バッファ
	//======================================================================
	class CDx11DynamicVertexBuffer
		: public kgl::IKGLBase
	{
	public:
		//!	コンストラクタ
		CDx11DynamicVertexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@return 結果
		b8 Initialize(CDx11Device* pDevice);

	public:
		//!	バッファのロック
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@return	バッファ
		void* Lock(u32 uiSize);
		//!	バッファのアンロック
		//!	@return	バッファ
		CDx11VertexBuffer* Unlock(void);

	private:
		//!	バッファの確保
		//!	@param uiNo		[in] 確保先番号
		//!	@param uiSize	[in] バッファサイズ
		//!	@return	バッファ
		CDx11VertexBuffer* Allocate(u32 uiNo, u32 uiSize);

	public:
		const static u32 BufferMax = 4;

	private:
		kgl::TKGLPtr<CDx11Device>		m_pDevice;				//!< デバイス
		kgl::TKGLPtr<CDx11VertexBuffer>	m_pLock;				//!< ロック中のバッファ
		kgl::TKGLPtr<CDx11VertexBuffer>	m_pBuffer[BufferMax];	//!< バッファ
		u32								m_uiSize[BufferMax];	//!< バッファサイズ
	};

	//======================================================================
	//!	動的インデックスバッファ
	//======================================================================
	class CDx11DynamicIndexBuffer
		: public kgl::IKGLBase
	{
	public:
		//!	コンストラクタ
		CDx11DynamicIndexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@return 結果
		b8 Initialize(CDx11Device* pDevice);

	public:
		//!	バッファのロック
		//!	@param uiSize	[in] バッファサイズ
		//!	@return	バッファ
		void* Lock(u32 uiSize);
		//!	バッファのアンロック
		//!	@return	バッファ
		CDx11IndexBuffer* Unlock(void);

	private:
		//!	バッファの確保
		//!	@param uiNo		[in] 確保先番号
		//!	@param uiSize	[in] バッファサイズ
		//!	@return	バッファ
		CDx11IndexBuffer* Allocate(u32 uiNo, u32 uiSize);

	public:
		const static u32 BufferMax = 4;

	private:
		kgl::TKGLPtr<CDx11Device>		m_pDevice;				//!< デバイス
		kgl::TKGLPtr<CDx11IndexBuffer>	m_pLock;				//!< ロック中のバッファ
		kgl::TKGLPtr<CDx11IndexBuffer>	m_pBuffer[BufferMax];	//!< バッファ
		u32								m_uiSize[BufferMax];	//!< バッファサイズ
	};
}

#endif	// __DX11_DYNAMIC_BUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
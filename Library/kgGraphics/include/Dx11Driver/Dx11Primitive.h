//----------------------------------------------------------------------
//!
//!	@file	Dx11Primitive.h
//!	@brief	Primitive関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_PRIMITIVE_H__
#define	__DX11_PRIMITIVE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	プリミティブ管理インターフェース
	//======================================================================
	class CDx11PrimitiveManager
		: public IPrimitiveManager
	{
	public:
		//!	コンストラクタ
		CDx11PrimitiveManager(kgl::TKGLPtr<CDx11Device> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	インデックスバッファ生成
		//!	@param ppIndex	[out] インデックスバッファ
		//!	@param uiSize	[in]  バッファサイズ
		//!	@param ucType	[in]  タイプ(EIndexType参照)
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 CreateIndexBuffer(IIndexBuffer** ppIndex, u32 uiSize, u8 ucType, b8 bDynamic);
		//!	頂点バッファ生成
		//!	@param ppVertex	[out] 頂点バッファ
		//!	@param uiSize	[in]  バッファサイズ
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 CreateVertexBuffer(IVertexBuffer** ppVertex, u32 uiSize, b8 bDynamic);
		//!	頂点フォーマット設定生成
		//!	@param ppDeclaration	[out] 頂点フォーマット
		//!	@param pDeclInfo		[in]  頂点宣言
		//!	@return 結果
		b8 CreateDeclaration(IDeclaration** ppDeclaration, const DeclarationInfo* pDeclInfo);

	private:
		kgl::TKGLPtr<CDx11Device>	m_pDevice;	//!< デバイス
	};
}

#endif	// __DX11_PRIMITIVE_H__
//======================================================================
//	END OF FILE
//======================================================================
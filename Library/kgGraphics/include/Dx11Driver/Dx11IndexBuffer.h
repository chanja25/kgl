//----------------------------------------------------------------------
//!
//!	@file	Dx11IndexBuffer.h
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_INDEXBUFFER_H__
#define	__DX11_INDEXBUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	インデックスバッファ
	//======================================================================
	class CDx11IndexBuffer
		: public IIndexBuffer
	{
	public:
		//!	コンストラクタ
		CDx11IndexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param uiSize	[in] バッファサイズ
		//!	@param ucType	[in] タイプ(EIndexType参照)
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 Initialize(CDx11Device* pDevice, u32 uiSize, u8 ucType, b8 bDynamic);

	public:
		//!	バッファのロック
		//!	@param uiOffset	[in] ロックするバッファのオフセット
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		void* Lock(u32 uiOffset, u32 uiSize, s32 iType);
		//!	バッファのアンロック
		void Unlock(void);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetBuffer(void) { return m_pBuffer; }

	private:
		kgl::TKGLPtr<CDx11Device>	m_pDevice;	//!< デバイス
		kgl::TKGLPtr<ID3D11Buffer>	m_pBuffer;	//!< 頂点バッファ
	};
}

#endif	// __DX11_INDEXBUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
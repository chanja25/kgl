//----------------------------------------------------------------------
//!
//!	@file	Dx11Texture.h
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_TEXTURE_H__
#define	__DX11_TEXTURE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	テクスチャー管理クラス
	//======================================================================
	class CDx11TextureManager
		: public ITextureManager
	{
	public:
		//!	コンストラクタ
		CDx11TextureManager(kgl::TKGLPtr<CDx11Device> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param Info			[in]  テクスチャー情報
		//!	@param pData		[in]  ピクセルデータ
		//!	@return 結果
		b8 CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData);
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param pData		[in]  DDSデータ
		//!	@return 結果
		b8 CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData);

	private:
		kgl::TKGLPtr<CDx11Device>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	テクスチャークラス
	//======================================================================
	class CDx11Texture2D
		: public ITextureBase
	{
	public:
		//!	テクスチャー生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] テクスチャー情報
		//!	@param pData	[in] ピクセルデータ
		//!	@return 結果
		b8 Create(CDx11Device* pDevice, const TextureInfo& Info, const void* pData);
		//!	DDSからテクスチャー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] DDSデータ
		//!	@return 結果
		b8 CreateFromDDS(CDx11Device* pDevice, const void* pData);

	public:
		//!	テクスチャーのロック
		//!	@param iLevel	[in] ミップレベル
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return テクスチャーデータの先頭アドレス
		void* Lock(s32 iLevel, s32 iType);
		//!	テクスチャーのアンロック
		//!	@param iLevel [in] ミップレベル
		void Unlock(s32 iLevel);

		//!	リソースのコピー
		//!	@param pResource [in] コピー元のリソース
		//!	@return 結果
		b8 CopyResource(ITextureBase* pResource);

	public:
		//!	テクスチャーの取得
		//!	@return テクスチャー
		INLINE void* GetTexture(void) { return m_pResource; }
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@param iLevel		[in]  ミップレベル
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface, s32 iLevel);

	private:
		kgl::TKGLPtr<CDx11Device>				m_pDevice;			//!< デバイス
		kgl::TKGLPtr<ID3D11Texture2D>			m_pTexture;			//!< テクスチャー
		kgl::TKGLPtr<ID3D11ShaderResourceView>	m_pResource;		//!< 描画リソース
		kgl::TKGLPtr<ID3D11View>				m_pView;			//!< レンダーターゲット用

		kgl::TKGLPtr<CDx11Texture2D>			m_pLockResource;	//!< ロック用
	};
}

#endif	// __DX11_TEXTURE_H__
//======================================================================
//	END OF FILE
//======================================================================
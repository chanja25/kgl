//----------------------------------------------------------------------
//!
//!	@file	Dx11RenderTarget.h
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_RENDERTARGET_H__
#define	__DX11_RENDERTARGET_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"
#include "Dx11Texture.h"

namespace kgGraphics
{
	//======================================================================
	//!	レンダーターゲット管理インターフェース
	//======================================================================
	class CDx11RenderTargetManager
		: public IRenderTargetManager
	{
	public:
		//!	コンストラクタ
		CDx11RenderTargetManager(kgl::TKGLPtr<CDx11Device> pDevice)
			: m_pDevice(pDevice)
		{}
		//!	レンダーターゲット生成
		//!	@param ppRenderTarget	[out] レンダーターゲット
		//!	@param Info				[in]  レンダーターゲット情報
		//!	@return 結果
		b8 CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info);

	private:
		kgl::TKGLPtr<CDx11Device>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	レンダーターゲットクラス
	//======================================================================
	class CDx11RenderTarget
		: public IRenderTargetBase
	{
	public:
		//!	レンダーターゲット生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] レンダーターゲット情報
		//!	@return 結果
		b8 Create(CDx11Device* pDevice, const RenderTargetInfo& Info);

	public:
		//!	レンダーターゲットのロック
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return レンダーターゲットデータの先頭アドレス
		void* Lock(s32 iType);
		//!	レンダーターゲットのアンロック
		void Unlock(void);

	public:
		//!	レンダーターゲット(テクスチャー)の取得
		//!	@return レンダーターゲット
		void GetRenderTarget(ITextureBase** ppTexture);
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface);

	private:
		kgl::TKGLPtr<CDx11Texture2D>	m_pRenderTarget;	//!< テクスチャー
	};
}

#endif	// __DX11_RENDERTARGET_H__
//======================================================================
//	END OF FILE
//======================================================================
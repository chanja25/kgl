//----------------------------------------------------------------------
//!
//!	@file	Dx11Shader.h
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_SHADER_H__
#define	__DX11_SHADER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	頂点シェーダーインターフェース
	//======================================================================
	class CDx11VertexShader
		: public IVertexShader
	{
	public:
		//!	頂点シェーダー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] シェーダーデータ
		//!	@return 結果
		b8 Create(CDx11Device* pDevice, const void* pData);

	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		INLINE void* GetShader(void) { return m_pShader; }
		//!	シェーダー情報の取得
		//!	@return シェーダー情報
		INLINE const ShaderInfo* GetInfo(void) { return m_pInfo; }

	private:
		kgl::TKGLPtr<ID3D11VertexShader>	m_pShader;	//!< シェーダー
		const ShaderInfo*					m_pInfo;	//!< シェーダー情報
	};
	//======================================================================
	//!	ピクセルシェーダーインターフェース
	//======================================================================
	class CPixelShader
		: public IPixelShader
	{
	public:
		//!	頂点シェーダー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] シェーダーデータ
		//!	@return 結果
		b8 Create(CDx11Device* pDevice, const void* pData);

	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		INLINE void* GetShader(void) { return m_pShader; }

	private:
		kgl::TKGLPtr<ID3D11PixelShader>	m_pShader;	//!< シェーダー
	};

	//======================================================================
	//!	シェーダー管理インターフェース
	//======================================================================
	class CDx11ShaderManager
		: public IShaderManager
	{
	public:
		//!	コンストラクタ
		CDx11ShaderManager(kgl::TKGLPtr<CDx11Device> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	頂点シェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		b8 CreateVertexShader(IVertexShader** ppShader, const void* pData);
		//!	ピクセルシェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		b8 CreatePixelShader(IPixelShader** ppShader, const void* pData);

	private:
		kgl::TKGLPtr<CDx11Device>	m_pDevice;	//!< デバイス
	};
}

#endif	// __DX11_SHADER_H__
//======================================================================
//	END OF FILE
//======================================================================
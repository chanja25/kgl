//----------------------------------------------------------------------
//!
//!	@file	Dx11Command.h
//!	@brief	コマンド生成
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_COMMAND_H__
#define	__DX11_COMMAND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"
#include "Dx11VertexBuffer.h"
#include "Dx11IndexBuffer.h"
#include "Utility/Dx11DynamicBuffer.h"
#include "../../../kgl/include/Utility/Hash/kgl.HashList.h"

namespace kgGraphics
{
	//======================================================================
	//!	グラフィックコマンドクリエーター
	//======================================================================
	class CDx11Command
		: public ICommandCreator
	{
	public:
		CDx11Command(kgl::TKGLPtr<CDx11Device> pDevice);

	private:
		//! リソースの初期化
		void InitResource(void);

	public:
		//!	コマンド情報のキャッシュが使用できるか？
		b8 IsSaveCommand(void) { return true; }
		//!	コマンド情報の記録開始
		void SaveCommand(void);
		//!	記録したコマンド情報の使用
		void UseCommand(void);

	public:
		//!	描画開始
		void Begin(void);
		//!	描画終了
		void End(void);
		//!	コマンド情報のリセット
		void ResetState(void);

		//!	描画ステートの適応
		void CommitGraphicState(void);
		//!	描画リソースの適応
		b8 CommitGraphicResource(s32 iPrimitive = -1, s32 iCount = 0, const void* pIndexes = null, s32 iVertexCount = 0, const void* pVertexes = null, s32 iStride = 0);

		DECLARE_COMMAND_FUNCTION(SetAlphaTest, AlphaTestInfo);
		DECLARE_COMMAND_FUNCTION(SetDepthTest, DepthTestInfo);
		DECLARE_COMMAND_FUNCTION(SetStencilTest, StencilTestInfo);
		DECLARE_COMMAND_FUNCTION(SetBlend, BlendInfo);
		DECLARE_COMMAND_FUNCTION(SetCullMode, CullModeInfo);
		DECLARE_COMMAND_FUNCTION(SetTexture, TextureInfo);
		DECLARE_COMMAND_FUNCTION(SetLODBias, LODBiasInfo);
		DECLARE_COMMAND_FUNCTION(SetSamplerState, SamplerStateInfo);
		DECLARE_COMMAND_FUNCTION(SetViewPort, ViewPortInfo);
		DECLARE_COMMAND_FUNCTION(SetRenderTarget, RenderTargetInfo);
		DECLARE_COMMAND_FUNCTION(SetDepthTarget, DepthTargetInfo);
		DECLARE_COMMAND_FUNCTION(SetDeclaration, DeclarationInfo);
		DECLARE_COMMAND_FUNCTION(SetVertex, VertexBindInfo);
		DECLARE_COMMAND_FUNCTION(SetIndex, IndexBindInfo);
		DECLARE_COMMAND_FUNCTION(DrawPrimitiveUP, DrawPrimitiveInfo);
		DECLARE_COMMAND_FUNCTION(DrawPrimitive, DrawInfo);
		DECLARE_COMMAND_FUNCTION(DrawIndexedPrimitiveUP, DrawIndexPrimitiveInfo);
		DECLARE_COMMAND_FUNCTION(DrawIndexedPrimitive, DrawIndexInfo);
		DECLARE_COMMAND_FUNCTION(SetVertexShader, VertexShaderInfo);
		DECLARE_COMMAND_FUNCTION(SetVertexShaderParam, VertexShaderParamInfo);
		DECLARE_COMMAND_FUNCTION(SetPixelShader, PixelShaderInfo);
		DECLARE_COMMAND_FUNCTION(SetPixelShaderParam, PixelShaderParamInfo);
		DECLARE_COMMAND_FUNCTION(ClearTarget, ClearTargetInfo);
		DECLARE_COMMAND_FUNCTION(SetScissoring, ScissoringInfo);
		DECLARE_COMMAND_FUNCTION(SetView, ViewInfo);
		DECLARE_COMMAND_FUNCTION(SetProjection, ProjectionInfo);
		DECLARE_COMMAND_FUNCTION(SetWorld, WorldInfo);

	private:
		typedef kgl::Hash::THashList<kgl::TKGLPtr<ID3D11BlendState>, D3D11_BLEND_DESC>					BlendStateList;
		typedef kgl::Hash::THashList<kgl::TKGLPtr<ID3D11RasterizerState>, D3D11_RASTERIZER_DESC>		RasterStateList;
		typedef kgl::Hash::THashList<kgl::TKGLPtr<ID3D11SamplerState>, D3D11_SAMPLER_DESC>				SamplerStateList;
		typedef kgl::Hash::THashList<kgl::TKGLPtr<ID3D11DepthStencilState>, D3D11_DEPTH_STENCIL_DESC>	DepthStencilList;

		kgl::TKGLPtr<CDx11Device>				m_pDevice;						//!< デバイス
		kgl::TKGLPtr<ID3D11DeviceContext>		m_pDeviceContext;				//!< デバイスコンテキスト
		kgl::TKGLPtr<ID3D11RenderTargetView>	m_pRenderTarget;				//!< 描画ターゲット
		kgl::TKGLPtr<ID3D11DepthStencilView>	m_pDepthStencil;				//!< 深度ターゲット
		kgl::TKGLPtr<ISurfaceBase>				m_pRenderSurface;				//!< サーフェイス(描画ターゲット)
		kgl::TKGLPtr<ISurfaceBase>				m_pDepthSurface;				//!< サーフェイス(デプスバッファ)
		kgl::TKGLPtr<CDx11DynamicIndexBuffer>	m_pIndexBuffer;					//!< インデックスバッファ
		kgl::TKGLPtr<CDx11DynamicVertexBuffer>	m_pVertexBuffer;				//!< 頂点バッファ
		kgl::TKGLPtr<ID3D11BlendState>			m_pBlendState;					//!< ブレンド
		kgl::TKGLPtr<ID3D11DepthStencilState>	m_pDepthStencilState;			//!< デプスステンシル
		kgl::TKGLPtr<ID3D11RasterizerState>		m_pRasterState;					//!< ラスタライズ
		kgl::TKGLPtr<ID3D11SamplerState>		m_pSamplerState[TEXSTAGE_MAX];	//!< 頂点バッファ

		enum EChangeState
		{
			EChangeStateBlend			= (1 << 0),
			EChangeStateDepthStencil	= (1 << 1),
			EChangeStateRasterizer		= (1 << 2),
			EChangeStateSampler			= (1 << 3),

			EChangeStateAll				= 0xffffffff,
		};
		kgl::Utility::CFlag						m_ChangeState;					//!< 変更状態判定用
		D3D11_BLEND_DESC						m_BlendDesc;					//!< ブレンド情報
		D3D11_DEPTH_STENCIL_DESC				m_DepthStencilDesc;				//!< デプスステンシル情報
		D3D11_RASTERIZER_DESC					m_RasterizerDesc;				//!< ラスタライズ情報
		D3D11_SAMPLER_DESC						m_SamplerDesc[TEXSTAGE_MAX];	//!< サンプラー情報

		BlendStateList							m_BlendStateList;				//!< ブレンドステート
		RasterStateList							m_RasterStateList;				//!< ラスタライズステート
		SamplerStateList						m_SamplerStateList;				//!< サンプラーステート
		DepthStencilList						m_DepthStencilList;				//!< デプスステンシルステート
	};
}

#endif	// __DX11_COMMAND_H__
//======================================================================
//	END OF FILE
//======================================================================
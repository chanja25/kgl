//----------------------------------------------------------------------
//!
//!	@file	Dx11Surface.h
//!	@brief	サーフェイス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX11_SURFACE_H__
#define	__DX11_SURFACE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx11Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	サーフェイスクラス
	//======================================================================
	class CDx11Surface
		: public ISurfaceBase
	{
	public:
		//!	サーフェイスのロック
		//!	@return サーフェイスデータの先頭アドレス
		void* Lock(s32 iType);
		//!	サーフェイスのアンロック
		void Unlock(void);

	public:
		//!	サーフェイスの取得
		//!	@return サーフェイス
		INLINE void Setup(CDx11Device* pDevice, const SurfaceInfo& Info, ID3D11Resource* pSurface, ID3D11View* pView, s32 iMipLevels = 1, s32 iLevel = 0)
		{
			m_pContext		= pDevice->DeviceContext();
			m_Info			= Info;
			m_pSurface		= pSurface;
			m_pView			= pView;
			m_iMipLevels	= iMipLevels;
			m_iLevel		= iLevel;
		}
		//!	サーフェイスの取得
		//!	@return サーフェイス
		INLINE void* GetSurface(void) { return m_pView; }

	private:
		kgl::TKGLPtr<ID3D11DeviceContext>	m_pContext;		//!< デバイス
		kgl::TKGLPtr<ID3D11Resource>		m_pSurface;		//!< サーフェイス
		kgl::TKGLPtr<ID3D11View>			m_pView;		//!< ビュー
		s32									m_iMipLevels;	//!< ミップレベル数
		s32									m_iLevel;		//!< ミップレベル
	};
}

#endif	// __DX11_SURFACE_H__
//======================================================================
//	END OF FILE
//======================================================================
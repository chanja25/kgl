//----------------------------------------------------------------------
//!
//!	@file	Dx9Device.h
//!	@brief	DirectX9
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_DEVICE_H__
#define	__DX9_DEVICE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include <d3dx9.h>

#if	KG_GRAPHICS_DEBUG
#define	VerifyResult(x)	kgAssert(SUCCEEDED(x), #x)
#else	// ~#if KG_GRAPHICS_DEBUG
#define	VerifyResult(x)	(x)
#endif	// ~#if KG_GRAPHICS_DEBUG

namespace kgGraphics
{
	INLINE u32 CalcDx9Color(u32 color)
	{
		u8 r = (color >>  0) & 0xff;
		u8 g = (color >>  8) & 0xff;
		u8 b = (color >> 16) & 0xff;
		u8 a = (color >> 24) & 0xff;
		return FOURCC(b, g, r, a);
	}

	//======================================================================
	//	DirectX9用Device
	//======================================================================
	class CDx9Device
		: public IGraphicDevice
	{
	public:
		//!	コンストラクタ
		CDx9Device(void);
		//!	デストラクタ
		~CDx9Device(void);

	public:
		//!	初期化
		//!	@param info	[in] グラフィック設定
		//!	@return 結果
		b8 Initialize(const GRAPHICS_INFO& info);
		//!	終了処理
		void Finalize(void);

	public:
		//!	描画シーン開始
		void BeginScene(void);
		//!	描画シーン終了
		void EndScene(void);

	public:
		//!	GPUのコマンドを実行する
		void FlushCommand(void);
		//!	GPUの処理終了待ち
		void WaitForCommand(void);

	public:
		//!	バックバッファの取得
		//!	@param ppBackBuffer [out] バックバッファ
		void GetBackBuffer(ISurfaceBase** ppBackBuffer);
		//!	デプスバッファの取得
		//!	@param ppDepthBuffer [out] デプスバッファ
		void GetDepthBuffer(ISurfaceBase** ppDepthBuffer);

		//!	VSyncの有無を設定
		//!	@param bEnable	[in] VSyncの有無
		void SetVSync(b8 bEnable);

	public:
		//!	クエリー生成
		//!	@param ppQuery	[out] クエリ
		//!	@param Info		[in]  クエリ情報
		//!	@return 結果
		b8 CreateQuery(IQueryBase** ppQuery, const QueryInfo& Info);
		//!	スワップチェインの生成
		//!	@param ppSwapChain	[out] スワップチェイン
		//!	@param pWindow		[in]  ウィンドウ
		//!	@return 結果
		b8 CreateSwapChain(ISwapChainBase** ppSwapChain, kgl::Window::IWindow* pWindow);

	public:
		//!	DirectXオブジェクトの取得
		//!	@return Direct3D9
		INLINE IDirect3D9* GetD3D(void) { return m_pD3D; }
		//!	デバイスの取得
		//!	@return Direct3DDevice
		INLINE IDirect3DDevice9* GetDevice(void) { return m_pDevice; }
		//!	パラメーターの取得
		//!	@return パラメーター
		INLINE D3DPRESENT_PARAMETERS* GetPresentParam(void) { return &m_d3dpp; }
		//!	バックバッファの取得
		//!	@return バックバッファ
		INLINE ISurfaceBase* GetDefaultBackBuffer(void) { return m_pBackBuffer; }
		//!	デプスバッファの取得
		//!	@return デプスバッファ
		INLINE ISurfaceBase* GetDefaultDepthBuffer(void) { return m_pDepthBuffer; }

	private:
		kgl::TKGLPtr<kgl::Window::IWindow>	m_pWindow;			//!< ウィンドウ
		kgl::TKGLPtr<IDirect3D9>			m_pD3D;				//!< DirectXオブジェクト
		kgl::TKGLPtr<IDirect3DDevice9>		m_pDevice;			//!< Direct3Dデバイス
		D3DPRESENT_PARAMETERS				m_d3dpp;			//!< Direct3Dのパラメータ
		D3DPRESENT_PARAMETERS				m_d3dppFS;			//!< Direct3Dのパラメータ(FullScreen)
		D3DPRESENT_PARAMETERS*				m_pd3dpp;			//!< 使用中のパラメータ
		BITFIELD							m_bVSync : 1;		//!< VSyncの有無
		kgl::TKGLPtr<IDirect3DSurface9>		m_pDepthStencil;	//!< バックバッファ
		kgl::TKGLPtr<IDirect3DTexture9>		m_pDepthStencilTex;	//!< バックバッファ
		kgl::TKGLPtr<ISurfaceBase>			m_pBackBuffer;		//!< バックバッファ
		kgl::TKGLPtr<ISurfaceBase>			m_pDepthBuffer;		//!< デプスバッファ
	};
}

#endif	// __DX9_DEVICE_H__
//======================================================================
//	END OF FILE
//======================================================================
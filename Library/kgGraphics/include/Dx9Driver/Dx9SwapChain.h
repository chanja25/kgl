//----------------------------------------------------------------------
//!
//!	@file	Dx9SwapChain.h
//!	@brief	スワップチェイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_SWAPCHAIN_H__
#define	__DX9_SWAPCHAIN_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"
#include "Dx9Surface.h"

namespace kgGraphics
{
	//======================================================================
	//!	スワップチェインクラス
	//======================================================================
	class CDx9SwapChain
		: public ISwapChainBase
	{
	public:
		//!	スワップチェイン生成
		//!	@param pDevice	[in] デバイス
		//!	@param pWindow	[in]  ウィンドウ
		//!	@return 結果
		b8 Create(CDx9Device* pDevice, kgl::Window::IWindow* pWindow);

	public:
		//!	描画内容の反映
		//!	@return 結果
		b8 Present(bool bVsync);

		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface);

	private:
		//kgl::TKGLPtr<ID3D11RenderTargetView>	m_pView;		//!< 描画ビュー
		D3DPRESENT_PARAMETERS					m_d3dpp;
		kgl::TKGLPtr<IDirect3DSwapChain9>		m_pSwapChain;	//!< スワップチェイン
		kgl::TKGLPtr<CDx9Surface>				m_pSurface;		//!< サーフェイス
		kgl::TKGLPtr<kgl::Window::IWindow>		m_pWindow;		//!< ウィンドウ
	};
}

#endif	// __DX9_SWAPCHAIN_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx9RenderTarget.h
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_RENDERTARGET_H__
#define	__DX9_RENDERTARGET_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"
#include "Dx9Texture.h"

namespace kgGraphics
{
	//======================================================================
	//!	レンダーターゲット管理インターフェース
	//======================================================================
	class CDx9RenderTargetManager
		: public IRenderTargetManager
	{
	public:
		//!	コンストラクタ
		CDx9RenderTargetManager(kgl::TKGLPtr<CDx9Device> pDevice)
			: m_pDevice(pDevice)
		{}
		//!	レンダーターゲット生成
		//!	@param ppRenderTarget	[out] レンダーターゲット
		//!	@param Info				[in]  レンダーターゲット情報
		//!	@return 結果
		b8 CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info);

	private:
		kgl::TKGLPtr<CDx9Device>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	レンダーターゲットクラス
	//======================================================================
	class CDx9RenderTarget
		: public IRenderTargetBase
	{
	public:
		//!	レンダーターゲット生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] レンダーターゲット情報
		//!	@return 結果
		b8 Create(CDx9Device* pDevice, const RenderTargetInfo& Info);

	public:
		//!	レンダーターゲットのロック
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return レンダーターゲットデータの先頭アドレス
		void* Lock(s32 iType);
		//!	レンダーターゲットのアンロック
		void Unlock(void);

	public:
		//!	レンダーターゲット(テクスチャー)の取得
		//!	@return レンダーターゲット
		void GetRenderTarget(ITextureBase** ppTexture);
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface);

	private:
		kgl::TKGLPtr<CDx9Texture2D>	m_pRenderTarget;	//!< テクスチャー
	};
}

#endif	// __DX9_RENDERTARGET_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Dx9Texture.h
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_TEXTURE_H__
#define	__DX9_TEXTURE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	テクスチャー管理クラス
	//======================================================================
	class CDx9TextureManager
		: public ITextureManager
	{
	public:
		//!	コンストラクタ
		CDx9TextureManager(kgl::TKGLPtr<CDx9Device> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param Info			[in]  テクスチャー情報
		//!	@param pData		[in]  ピクセルデータ
		//!	@return 結果
		b8 CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData);
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param pData		[in]  DDSデータ
		//!	@return 結果
		b8 CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData);

	private:
		kgl::TKGLPtr<CDx9Device>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	テクスチャークラス
	//======================================================================
	class CDx9Texture2D
		: public ITextureBase
	{
	public:
		//!	テクスチャー生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] テクスチャー情報
		//!	@param pData	[in] ピクセルデータ
		//!	@return 結果
		b8 Create(CDx9Device* pDevice, const TextureInfo& Info, const void* pData);
		//!	DDSからテクスチャー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] DDSデータ
		//!	@return 結果
		b8 CreateFromDDS(CDx9Device* pDevice, const void* pData);

	public:
		//!	テクスチャーのロック
		//!	@param iLevel	[in] ミップレベル
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return テクスチャーデータの先頭アドレス
		void* Lock(s32 iLevel, s32 iType);
		//!	テクスチャーのアンロック
		//!	@param iLevel [in] ミップレベル
		void Unlock(s32 iLevel);

		//!	リソースのコピー
		//!	@param pResource [in] コピー元のリソース
		//!	@return 結果
		b8 CopyResource(ITextureBase* pResource);

	public:
		//!	テクスチャーの取得
		//!	@return テクスチャー
		INLINE void* GetTexture(void) { return m_pTexture; }
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@param iLevel		[in]  ミップレベル
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface, s32 iLevel);

	private:
		kgl::TKGLPtr<IDirect3DTexture9>	m_pTexture;	//!< テクスチャー
	};
}

#endif	// __DX9_TEXTURE_H__
//======================================================================
//	END OF FILE
//======================================================================
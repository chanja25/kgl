//----------------------------------------------------------------------
//!
//!	@file	Dx9IndexBuffer.h
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_INDEXBUFFER_H__
#define	__DX9_INDEXBUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	インデックスバッファ
	//======================================================================
	class CDx9IndexBuffer
		: public IIndexBuffer
	{
	public:
		//!	コンストラクタ
		CDx9IndexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param iSize	[in] バッファサイズ
		//!	@param ucType	[in] タイプ(EIndexType参照)
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 Initialize(CDx9Device* pDevice, u32 uiSize, u8 ucType, b8 bDynamic);

	public:
		//!	バッファのロック
		//!	@param uiOffset	[in] ロックするバッファのオフセット
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		void* Lock(u32 uiOffset, u32 uiSize, s32 iType);
		//!	バッファのアンロック
		void Unlock(void);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetBuffer(void) { return m_pBuffer; }

	private:
		kgl::TKGLPtr<IDirect3DIndexBuffer9>	m_pBuffer;	//!< 頂点バッファ
	};
}

#endif	// __DX9_INDEXBUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
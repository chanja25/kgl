//----------------------------------------------------------------------
//!
//!	@file	Dx9Shader.h
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_SHADER_H__
#define	__DX9_SHADER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	頂点シェーダーインターフェース
	//======================================================================
	class CDx9VertexShader
		: public IVertexShader
	{
	public:
		//!	頂点シェーダー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] シェーダーデータ
		//!	@return 結果
		b8 Create(CDx9Device* pDevice, const void* pData);

	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		INLINE void* GetShader(void) { return m_pShader; }

	private:
		kgl::TKGLPtr<IDirect3DVertexShader9>	m_pShader;	//!< シェーダー
	};
	//======================================================================
	//!	ピクセルシェーダーインターフェース
	//======================================================================
	class CPixelShader
		: public IPixelShader
	{
	public:
		//!	頂点シェーダー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] シェーダーデータ
		//!	@return 結果
		b8 Create(CDx9Device* pDevice, const void* pData);

	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		INLINE void* GetShader(void) { return m_pShader; }

	private:
		kgl::TKGLPtr<IDirect3DPixelShader9>	m_pShader;	//!< シェーダー
	};

	//======================================================================
	//!	シェーダー管理インターフェース
	//======================================================================
	class CDx9ShaderManager
		: public IShaderManager
	{
	public:
		//!	コンストラクタ
		CDx9ShaderManager(kgl::TKGLPtr<CDx9Device> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	頂点シェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		b8 CreateVertexShader(IVertexShader** ppShader, const void* pData);
		//!	ピクセルシェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		b8 CreatePixelShader(IPixelShader** ppShader, const void* pData);

	private:
		kgl::TKGLPtr<CDx9Device>	m_pDevice;	//!< デバイス
	};
}

#endif	// __DX9_SHADER_H__
//======================================================================
//	END OF FILE
//======================================================================
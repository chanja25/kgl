//----------------------------------------------------------------------
//!
//!	@file	Dx9Declaration.h
//!	@brief	頂点宣言
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_DECLARATION_H__
#define	__DX9_DECLARATION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"


namespace kgGraphics
{
	//======================================================================
	//!	頂点フォーマット設定用クラス
	//======================================================================
	class CDx9Declaration
		: public IDeclaration
	{
	public:
		//!	コンストラクタ
		CDx9Declaration(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param pDecl	[in] 頂点宣言
		//!	@return 結果
		b8 Initialize(CDx9Device* pDevice, const DeclarationInfo* pDecl);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetDeclaration(void) { return m_pDecl; }

	private:
		kgl::TKGLPtr<IDirect3DVertexDeclaration9>	m_pDecl;
	};
}

#endif	// __DX9_DECLARATION_H__
//======================================================================
//	END OF FILE
//======================================================================
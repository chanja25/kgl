//----------------------------------------------------------------------
//!
//!	@file	Dx9Surface.h
//!	@brief	サーフェイス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DX9_SURFACE_H__
#define	__DX9_SURFACE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "Dx9Device.h"

namespace kgGraphics
{
	//======================================================================
	//!	サーフェイスクラス
	//======================================================================
	class CDx9Surface
		: public ISurfaceBase
	{
	public:
		//!	サーフェイスのロック
		//!	@return サーフェイスデータの先頭アドレス
		void* Lock(s32 iType);
		//!	サーフェイスのアンロック
		void Unlock(void);

	public:
		//!	サーフェイスの取得
		//!	@return サーフェイス
		INLINE void Setup(const SurfaceInfo& Info, IDirect3DSurface9* pSurface)
		{
			m_Info		= Info;
			m_pSurface	= pSurface;
		}
		//!	サーフェイスの取得
		//!	@return サーフェイス
		INLINE void* GetSurface(void) { return m_pSurface; }

	private:
		kgl::TKGLPtr<IDirect3DSurface9>	m_pSurface;	//!< サーフェイス
	};
}

#endif	// __DX9_SURFACE_H__
//======================================================================
//	END OF FILE
//======================================================================
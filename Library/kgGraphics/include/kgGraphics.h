//----------------------------------------------------------------------
//!
//!	@file	kgGraphics.h
//!	@brief	Kisaragi Game Graphics ヘッダー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_GRAPHICS_H__
#define	__KG_GRAPHICS_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../../kgl/include/kgl.h"
#include "../../../kgl/include/System/kgl.Window.h"

#if	KGL_DEBUG
#define KG_GRAPHICS_DEBUG	1
#else	// ~#if KGL_DEBUG
#define KG_GRAPHICS_DEBUG	0
#endif	// ~#if KGL_DEBUG

#include "Utility/kgUtility.h"

//----------------------------------------------------------------------
// Kisaragi Game Graphics
//----------------------------------------------------------------------
namespace kgGraphics
{
	//!	グラフィック情報
	struct GRAPHICS_INFO
	{
		s32						iWidth;			//!< 解像度(横)
		s32						iHeight;		//!< 解像度(縦)
		kgl::Window::IWindow*	pWindow;		//!< 描画するWindows
		BITFIELD				bFullScreen:1;	//!< フルスクリーンか？
		BITFIELD				bUseVSync:1;	//!< 同期待ちの有無
	};

	//======================================================================
	//	グラフィックデバイスインターフェース
	//======================================================================
	interface IGraphicDevice
		: public kgl::IKGLBase
	{
	public:
		//!	描画シーン開始
		virtual void BeginScene(void) = 0;
		//!	描画シーン終了
		virtual void EndScene(void) = 0;

	public:
		//!	GPUのコマンドを実行する
		virtual void FlushCommand(void) = 0;
		//!	GPUの処理終了待ち
		virtual void WaitForCommand(void) = 0;

	public:
		//!	バックバッファの取得
		//!	@param ppBackBuffer [out] バックバッファ
		virtual void GetBackBuffer(ISurfaceBase** ppBackBuffer) = 0;
		//!	デプスバッファの取得
		//!	@param ppDepthBuffer [out] デプスバッファ
		virtual void GetDepthBuffer(ISurfaceBase** ppDepthBuffer) = 0;

		//!	VSyncの有無を設定
		//!	@param bEnable	[in] VSyncの有無
		virtual void SetVSync(b8 bEnable) = 0;

	public:
		//!	クエリー生成
		//!	@param ppQuery	[out] クエリ
		//!	@param Info		[in]  クエリ情報
		//!	@return 結果
		virtual b8 CreateQuery(IQueryBase** ppQuery, const QueryInfo& Info) = 0;
		//!	スワップチェインの生成
		//!	@param ppSwapChain	[out] スワップチェイン
		//!	@param pWindow		[in]  ウィンドウ
		//!	@return 結果
		virtual b8 CreateSwapChain(ISwapChainBase** ppSwapChain, kgl::Window::IWindow* pWindow) = 0;
	};

	//======================================================================
	//	グラフィックドライバーインターフェース
	//======================================================================
	interface IGraphicDriver
		: public kgl::IKGLBase
	{
	public:
		//!	初期化
		//!	@param info	[in] グラフィック設定
		//!	@return 結果
		virtual b8 Initialize(const GRAPHICS_INFO& info) = 0;
		//!	破棄
		virtual void Finalize(void){}

	public:
		//!	ドライバータイプの取得
		//!	@return ドライバータイプ
		virtual s32 GetType(void) = 0;
		//!	ドライバー名の取得
		//!	@return ドライバー名
		virtual const c8* Name(void) = 0;

	public:
		//!	デバイスの取得
		//!	@return デバイス
		FORCEINLINE IGraphicDevice* GraphicDevice(void) { return m_pDevice; }
		//!	プリミティブ管理の取得
		//!	@return プリミティブ管理
		FORCEINLINE IPrimitiveManager* PrimitiveManager(void) { return m_pPrimitiveManager; }
		//!	テクスチャー管理の取得
		//!	@return テクスチャー管理
		FORCEINLINE ITextureManager* TextureManager(void) { return m_pTextureManager; }
		//!	レンダーターゲット管理の取得
		//!	@return レンダーターゲット管理
		FORCEINLINE IRenderTargetManager* RenderTargetManager(void) { return m_pRenderTargetManager; }
		//!	シェーダー管理の取得
		//!	@return シェーダー管理
		FORCEINLINE IShaderManager* ShaderManager(void) { return m_pShaderManager; }
		//!	コマンド生成の取得
		//!	@return コマンド生成
		FORCEINLINE ICommandCreator* CommandCreator(void) { return m_pCommandCreator; }

	protected:
		kgl::TKGLPtr<IGraphicDevice>		m_pDevice;				//!< グラフィックデバイス
		kgl::TKGLPtr<IPrimitiveManager>		m_pPrimitiveManager;	//!< プリミティブ管理
		kgl::TKGLPtr<ITextureManager>		m_pTextureManager;		//!< テクスチャー管理
		kgl::TKGLPtr<IRenderTargetManager>	m_pRenderTargetManager;	//!< レンダーターゲット管理
		kgl::TKGLPtr<IShaderManager>		m_pShaderManager;		//!< シェーダー管理
		kgl::TKGLPtr<ICommandCreator>		m_pCommandCreator;		//!< グラフィックコマンド制御
	};
}


#if	USE_DIRECTX9
#include "Dx9Driver/Dx9Driver.h"
#endif	// ~#if USE_DIRECTX9

#if	USE_DIRECTX11
#include "Dx11Driver/Dx11Driver.h"
#endif	// ~#if USE_DIRECTX11

#if	USE_OPENGL
#include "OpenGLDriver/GLDriver.h"
#endif	// ~#if USE_OPENGL


#endif	// __KG_GRAPHICS_H__
//======================================================================
//	END OF FILE
//======================================================================
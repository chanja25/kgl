//----------------------------------------------------------------------
//!
//!	@file	kgTextureUtil.h
//!	@brief	テクスチャー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_TEXTURE_UTIL_H__
#define	__KG_TEXTURE_UTIL_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgGraphics.h"


namespace kgGraphics
{
	struct TextureInfo;
	//!	テクスチャーデータ
	struct TextureData
	{
		const void*	pData;
		u32			uiPitch;
		u32			uiSize;
	};

	//!	1ピクセル毎のピクセルのバイト数を取得
	//!	@param iFormat [in] フォーマット
	//!	@return 結果
	u32 GetStride(s32 iFormat);
	//!	テクスチャーデータの構築
	//!	@param Info		[in] テクスチャー情報
	//!	@param pData	[in] データ
	//!	@return テクスチャーデータ
	TextureData* BuildTextureData(const TextureInfo& Info, const void* pData);
}

#endif	// __KG_TEXTURE_UTIL_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgTexture.h
//!	@brief	テクスチャー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_RENDERTARGET_H__
#define	__KG_RENDERTARGET_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"


namespace kgGraphics
{
	//!	レンダーターゲットの情報
	struct RenderTargetInfo
	{
		s32			iUsage;		//!< タイプ(ETextureUsage参照)
		s32			iFormat;	//!< フォーマット(ETextureFormat参照)
		u32			uiWidth;	//!< 横幅
		u32			uiHeight;	//!< 縦幅
		SampleDesc	SampleDesc;	//!< サンプリング情報
	};

	//======================================================================
	//!	レンダーターゲットインターフェース
	//======================================================================
	interface IRenderTargetBase
		: public kgl::IKGLBase
	{
	public:
		//!	レンダーターゲットのロック
		//!	@param iType [in] ロックタイプ(ELockType参照)
		//!	@return レンダーターゲットデータの先頭アドレス
		virtual void* Lock(s32 iType = ELockType::Read) = 0;
		//!	レンダーターゲットのアンロック
		virtual void Unlock(void) = 0;

		//!	レンダーターゲット(テクスチャー)の取得
		//!	@return レンダーターゲット
		virtual void GetRenderTarget(ITextureBase** ppTexture) = 0;
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@return 結果
		virtual b8 GetSurface(ISurfaceBase** ppSurface) = 0;

	public:
		//!	レンダーターゲット情報の取得
		//!	@return レンダーターゲット情報
		INLINE const RenderTargetInfo& GetInfo(void) { return m_Info; }

	protected:
		RenderTargetInfo	m_Info;		//!< レンダーターゲット情報
	};

	//======================================================================
	//!	レンダーターゲット管理インターフェース
	//======================================================================
	interface IRenderTargetManager
		: public kgl::IKGLBase
	{
	public:
		//!	レンダーターゲット生成
		//!	@param ppRenderTarget	[out] レンダーターゲット
		//!	@param Info				[in]  レンダーターゲット情報
		//!	@return 結果
		virtual b8 CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info) = 0;
	};
}


#endif	// __KG_RENDERTARGET_H__
//======================================================================
//	END OF FILE
//======================================================================
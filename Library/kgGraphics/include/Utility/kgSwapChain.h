//----------------------------------------------------------------------
//!
//!	@file	kgSwapChaing.h
//!	@brief	テクスチャー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_SWAPCHAIN_H__
#define	__KG_SWAPCHAIN_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "kgSurface.h"

namespace kgGraphics
{
	//======================================================================
	//!	スワップチェインインターフェース
	//======================================================================
	interface ISwapChainBase
		: public kgl::IKGLBase
	{
	public:
		//!	描画内容の反映
		//!	@return 結果
		virtual b8 Present(bool bVsync = true) = 0;

		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	"@return 結果
		virtual b8 GetSurface(ISurfaceBase** ppSurface) = 0;
	};

	//======================================================================
	//!	スワップチェイン管理インターフェース
	//======================================================================
	class ISwapChainManager
		: public kgl::IKGLBase
	{
	public:
		//!	スワップチェイン生成
		//!	@param ppSwapChain	[out] スワップチェイン
		//!	@param pWindow		[in]  ウィンドウ
		//!	@return 結果
		virtual b8 CreateSwapChain(ISwapChainBase** ppSwapChain, kgl::Window::IWindow* pWindow) = 0;
	};
}


#endif	// __KG_SWAPCHAIN_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgQuery.h
//!	@brief	クエリ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_QUERY_H__
#define	__KG_QUERY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"


namespace kgGraphics
{
	struct QueryInfo
	{
		s32	iQuery;		//!< タイプ(EQuery参照)
	};

	//======================================================================
	//!	クエリインターフェース
	//======================================================================
	interface IQueryBase
		: public kgl::IKGLBase
	{
	public:
		//!	クエリの発行
		virtual void Begin(void) = 0;
		//!	クエリの終了
		virtual void End(void) = 0;

		//!	データの取得
		//!	@param pData	[out] データを受け取るバッファ
		//!	@param uiSize	[in]  バッファのサイズ
		//!	@return 結果
		virtual b8 GetData(void* pData, u32 uiSize) = 0;

		//!	クエリ取得
		//!	@return クエリ
		virtual void* GetQuery(void) = 0;

	public:
		//!	クエリ情報の取得
		//!	@return	クエリ情報
		INLINE const QueryInfo& GetInfo(void) { return m_Info; }

	protected:
		QueryInfo	m_Info;	//!< クエリ情報
	};
}


#endif	// __KG_QUERY_H__
//======================================================================
//	END OF FILE
//======================================================================
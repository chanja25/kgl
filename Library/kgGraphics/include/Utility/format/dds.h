//----------------------------------------------------------------------
//!
//!	@file	dds.h
//!	@brief	DDS関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_UTILITY_DDS_H__
#define	__KG_UTILITY_DDS_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgGraphics.h"


#ifndef FOURCC_DXT1
#define FOURCC_DXT1	FOURCC('D','X','T','1')
#endif // !FOURCC_DXT1
#ifndef FOURCC_DXT3
#define FOURCC_DXT3	FOURCC('D','X','T','3')
#endif // !FOURCC_DXT3
#ifndef FOURCC_DXT5
#define FOURCC_DXT5	FOURCC('D','X','T','5')
#endif // !FOURCC_DXT5


#define DDSCAPS2_CUBEMAP			0x00000200L
#define DDSCAPS2_CUBEMAP_POSITIVEX	0x00000400L
#define DDSCAPS2_CUBEMAP_NEGATIVEX	0x00000800L
#define DDSCAPS2_CUBEMAP_POSITIVEY	0x00001000L
#define DDSCAPS2_CUBEMAP_NEGATIVEY	0x00002000L
#define DDSCAPS2_CUBEMAP_POSITIVEZ	0x00004000L
#define DDSCAPS2_CUBEMAP_NEGATIVEZ	0x00008000L
#define	DDSCAPS2_CUBEMAP_ALL		0x0000FE00L

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//!	DDSフォーマット
	//----------------------------------------------------------------------
	enum
	{
		DDSFMT_DXT1,
		DDSFMT_DXT3,
		DDSFMT_DXT5,
		DDSFMT_A8R8G8B8,
		DDSFMT_A1R5G5B5,
		DDSFMT_A4R4G4B4,
		DDSFMT_A8,
		DDSFMT_L8,
	};

	//----------------------------------------------------------------------
	//!	DDSフォーマット
	//----------------------------------------------------------------------
	struct DDS_PIXELFORMAT
	{
		dword	dwSize;
		dword	dwFlags;
		dword	dwFourCC;
		dword	dwRGBBitCount;
		dword	dwRBitMask;
		dword	dwGBitMask;
		dword	dwBBitMask;
		dword	dwABitMask;
	};

	//----------------------------------------------------------------------
	//!	DDSサーフェイス情報
	//----------------------------------------------------------------------
	struct DDSURFACEDESC2
	{
		dword			dwSize;
		dword			dwHeaderFlags;
		dword			dwHeight;
		dword			dwWidth;
		dword			dwPitchOrLinearSize;
		dword			dwDepth;
		dword			dwMipMapCount;
		dword			dwReserved1[11];
		DDS_PIXELFORMAT ddspf;
		dword			dwSurfaceFlags;
		dword			dwCubemapFlags;
		dword			dwReserved2[3];
	};

	//----------------------------------------------------------------------
	//!	DDSデータヘッダ
	//----------------------------------------------------------------------
	struct DDS_HEADER
	{
		dword			dwMagic;
		DDSURFACEDESC2	ddsd;
	};

	//----------------------------------------------------------------------
	//!	DDS情報
	//----------------------------------------------------------------------
	struct DDS_INFO
	{
		u32			uiWidth;
		u32			uiHeight;
		u32			uiFormat;
		s32			iMipLevels;
		s32			iCubemap;
		const void*	pPixels;
	};

	//----------------------------------------------------------------------
	//!	DDS分析
	//!	@param pInfo [out] DDS情報
	//!	@param pAddr [in] DDSデータ
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 AnalyzeDDS(DDS_INFO* pInfo, const void* pAddr);
}


#endif	// __KG_UTILITY_DDS_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgPrimitive.h
//!	@brief	Primitive関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_PRIMITIVE_H__
#define	__KG_PRIMITIVE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define	KGG_DECL_END()	{ 0xffff, 0, 0, 0, 0, 0 }

namespace kgGraphics
{
	//	インデックスタイプ
	namespace EIndexType
	{
		const u8 Index16	= 0;	//!< 16bitインデックス
		const u8 Index32	= 1;	//!< 32bitインデックス
	}
	//	頂点タイプ
	namespace EDeclType
	{
		const u8 Float1		= 0;	//!< (v[0],    0,    0,     1)
		const u8 Float2		= 1;	//!< (v[0], v[1],    0,     1)
		const u8 Float3		= 2;	//!< (v[0], v[1], v[2],     1)
		const u8 Float4		= 3;	//!< (v[0], v[1], v[2],  v[3])
		const u8 Color		= 4;	//!< (v[0], v[1], v[2],  v[3]) ※u8型4個が0〜1の範囲にマップされる
		const u8 UByte4		= 5;	//!< (v[0], v[1], v[2],  v[3])
		const u8 Short2		= 6;	//!< (v[0], v[1],    0,     1)
		const u8 Short4		= 7;	//!< (v[0], v[1], v[2],  v[3])

		const u8 UByte4N	= 8;	//!< (v[0]/255.0f,   v[1]/255.0f,     v[2]/255.0f,    v[3]/255.0f)
		const u8 Short2N	= 9;	//!< (v[0]/32767.0f, v[1]/32767.0f,             0,              1)
		const u8 Short4N	= 10;	//!< (v[0]/32767.0f, v[1]/32767.0f, v[2]/32767.0f,  v[3]/32767.0f)
		const u8 UShort2N	= 11;	//!< (v[0]/65535.0f, v[1]/65535.0f,             0,              1)
		const u8 UShort4N	= 12;	//!< (v[0]/65535.0f, v[1]/65535.0f, v[2]/65535.0f,  v[3]/65535.0f)
		const u8 Float16_2	= 13;	//!< (v[0], v[1],    0,    1)
		const u8 Float16_4	= 14;	//!< (v[0], v[1], v[2], v[3])
	}
	//	頂点の使用用途
	namespace EDeclUsage
	{
		const u8 Position		= 0;	//!< 座標
		const u8 BlendWeight	= 1;	//!< ブレンドWeight
		const u8 BlendIndex		= 2;	//!< ブレンドIndex
		const u8 Normal			= 3;	//!< 法線
		const u8 PSize			= 4;	//!< ポイントスプライトのサイズ
		const u8 UV				= 5;	//!< UV座標
		const u8 Tangent		= 6;	//!< 接線
		const u8 Binormal		= 7;	//!< 従法線
		const u8 Tessfactor		= 8;	//!< テセレーション係数
		const u8 PositionT		= 9;	//!< トランスフォーム後の位置。これを含む宣言が設定されると、頂点処理をバイパスする。
		const u8 Color			= 10;	//!< カラー
		const u8 Fog			= 11;	//!< フォグ
		const u8 Depth			= 12;	//!< 深度
		const u8 Max			= 13;
	}
	//!	頂点宣言用構造体
	struct DeclarationInfo
	{
		u16		usStream;		//!< 使用するストリーム
		u16		usOffset;		//!< ストリームのオフセット
		u8		ucType;			//!< データタイプ
		u8		ucUsage;		//!< 使用用途
		u8		ucUsageIndex;	//!< 使用用途が複数ある場合のインデックス
		u8		ucReserved;		//!< 拡張用(未使用)
	};

	//======================================================================
	//!	インデックスバッファインターフェース
	//======================================================================
	class IIndexBuffer
		: public kgl::IKGLBase
	{
	public:
		//!	デストラクタ
		~IIndexBuffer(void){};
		//!	バッファのロック
		//!	@param uiOffset	[in] ロックするバッファのオフセット
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		virtual void* Lock(u32 uiOffset, u32 uiSize, s32 iType = ELockType::ReadWrite) = 0;
		//!	バッファのアンロック
		virtual void Unlock(void) = 0;

	public:
		//!	バッファ取得
		//!	@return	バッファ
		virtual void* GetBuffer(void) = 0;
		//!	インデックスタイプ取得
		//!	@return	インデックスタイプ
		INLINE u8 GetType(void) { return m_ucType; }

	protected:
		u8	m_ucType;	//!< インデックスタイプ
		u8	m_pad[3];
	};
	//======================================================================
	//!	頂点バッファインターフェース
	//======================================================================
	class IVertexBuffer
		: public kgl::IKGLBase
	{
	public:
		//!	デストラクタ
		~IVertexBuffer(void){};
		//!	バッファのロック
		//!	@param uiOffset	[in] ロックするバッファのオフセット
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		virtual void* Lock(u32 uiOffset, u32 uiSize, s32 iType = ELockType::ReadWrite) = 0;
		//!	バッファのアンロック
		virtual void Unlock(void) = 0;

	public:
		//!	バッファ取得
		//!	@return	バッファ
		virtual void* GetBuffer(void) = 0;
	};
	//======================================================================
	//!	頂点フォーマット設定用インターフェース
	//======================================================================
	class IDeclaration
		: public kgl::IKGLBase
	{
	public:
		//!	デストラクタ
		~IDeclaration(void){};
		//!	バッファ取得
		//!	@return	バッファ
		virtual void* GetDeclaration(void) = 0;
	};

	//======================================================================
	//!	プリミティブ管理インターフェース
	//======================================================================
	class IPrimitiveManager
		: public kgl::IKGLBase
	{
	public:
		//!	インデックスバッファ生成
		//!	@param ppIndex	[out] インデックスバッファ
		//!	@param uiSize	[in]  バッファサイズ
		//!	@param ucType	[in]  タイプ(EIndexType参照)
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		virtual b8 CreateIndexBuffer(IIndexBuffer** ppIndex, u32 uiSize, u8 ucType = EIndexType::Index16, b8 bDynamic = false) = 0;
		//!	頂点バッファ生成
		//!	@param ppVertex	[out] 頂点バッファ
		//!	@param uiSize	[in]  バッファサイズ
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		virtual b8 CreateVertexBuffer(IVertexBuffer** ppVertex, u32 uiSize, b8 bDynamic = false) = 0;
		//!	頂点フォーマット設定生成
		//!	@param ppDeclaration	[out] 頂点フォーマット
		//!	@param pDeclInfo		[in]  頂点宣言
		//!	@return 結果
		virtual b8 CreateDeclaration(IDeclaration** ppDeclaration, const DeclarationInfo* pDeclInfo) = 0;
	};
}

#endif	// __KG_PRIMITIVE_H__
//======================================================================
//	END OF FILE
//======================================================================
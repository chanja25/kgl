//----------------------------------------------------------------------
//!
//!	@file	kgSurface.h
//!	@brief	テクスチャー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_SURFACE_H__
#define	__KG_SURFACE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"


namespace kgGraphics
{
	struct SurfaceInfo
	{
		s32	iUsage;		//!< タイプ(ETextureUsage参照)
		s32	iFormat;	//!< フォーマット(ETextureFormat参照)
		u32	uiWidth;	//!< 横幅
		u32	uiHeight;	//!< 縦幅
	};

	//======================================================================
	//!	サーフェイスインターフェース
	//======================================================================
	interface ISurfaceBase
		: public kgl::IKGLBase
	{
	public:
		//!	サーフェイスのロック
		//!	@param iType [in] ロックタイプ(ELockType参照)
		//!	@return サーフェイスデータの先頭アドレス
		virtual void* Lock(s32 iType = ELockType::ReadWrite) = 0;
		//!	サーフェイスのアンロック
		virtual void Unlock(void) = 0;

		//!	サーフェイスの取得
		//!	@return サーフェイス
		virtual void* GetSurface(void) = 0;

	public:
		//!	サーフェイス情報の取得
		//!	@return サーフェイス情報
		INLINE const SurfaceInfo& GetInfo(void) { return m_Info; }

	protected:
		SurfaceInfo	m_Info;	//!< サーフェイス情報
	};
}


#endif	// __KG_SURFACE_H__
//======================================================================
//	END OF FILE
//======================================================================
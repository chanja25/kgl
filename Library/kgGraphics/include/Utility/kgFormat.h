//----------------------------------------------------------------------
//!
//!	@file	kgFormat.h
//!	@brief	フォーマット
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_FORMAT_H__
#define	__KG_FORMAT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"


namespace kgGraphics
{
	//!	テクスチャータイプ
	namespace ETextureType
	{
		const s32 Normal	= 0;	//!< 2Dテクスチャー
		const s32 Cube		= 1;	//!< キューブテクスチャー
		const s32 Volume	= 2;	//!< ボリュームテクスチャー
	}
	//!	テクスチャーの使用方法
	namespace ETextureUsage
	{
		const s32 RenderTarget	= (1 << 1);	//!< レンダーターゲット
		const s32 Depth			= (1 << 2);	//!< デプステクスチャー
		const s32 Dynamic		= (1 << 3);	//!< CPUから動的にアクセス可能なテクスチャー
	}
	//!	テクスチャーフォーマット
	namespace ETextureFormat
	{
		const s32 Unkown		= -1;	//!< Unknown
		const s32 R8G8B8		= 0;	//!< R8G8B8(24bit)
		const s32 A8R8G8B8		= 1;	//!< A8R8G8B8(32bit)
		const s32 A16R16G16B16	= 2;	//!< A16R16G16B16(64bit)
		const s32 A16R16G16B16F	= 3;	//!< A16R16G16B16F(64bit)
		const s32 R5G6B5		= 4;	//!< R5G6B5(16bit)
		const s32 A1R5G5B5		= 5;	//!< A1R5G5B5(16bit)
		const s32 A4R4G4B4		= 6;	//!< A4R4G4B4(16bit)
		const s32 A8			= 8;	//!< A8(8bit)
		const s32 L8			= 9;	//!< A8(8bit)
		const s32 A8L8			= 10;	//!< A8L8(16bit)
		const s32 R16F			= 12;	//!< R16F(16bit)
		const s32 R32F			= 13;	//!< R32F(32bit)
		const s32 D16			= 14;	//!< D16(16bit)
		const s32 D32			= 15;	//!< D32(32bit)
		const s32 D24S8			= 17;	//!< D24S8(32bit)

		const s32 DXT1			= 100;	//!< DXT1
		const s32 DXT3			= 102;	//!< DXT3
		const s32 DXT5			= 104;	//!< DXT5
	}
}


#endif	// __KG_FORMAT_H__
//======================================================================
//	END OF FILE
//======================================================================
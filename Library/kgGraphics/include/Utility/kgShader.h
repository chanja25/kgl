//----------------------------------------------------------------------
//!
//!	@file	kgShader.h
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_SHADER_H__
#define	__KG_SHADER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"


namespace kgGraphics
{
	namespace EShaderType
	{
		const s32 Unknown	= -1;	//!< 不明なタイプ
		const s32 Vertex	= 0;	//!< 頂点シェーダー
		const s32 Pixel		= 1;	//!< ピクセルシェーダー
	}

	//!	シェーダー情報
	struct ShaderInfo
	{
		s32	iType;	//!< シェーダータイプ
		s32	iSize;	//!< データサイズ
	};

	//======================================================================
	//!	シェーダーインターフェース
	//======================================================================
	interface IShaderBase
		: public kgl::IKGLBase
	{
	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		virtual void* GetShader(void) = 0;
	};
	//======================================================================
	//!	頂点シェーダーインターフェース
	//======================================================================
	interface IVertexShader
		: public IShaderBase
	{
	public:
	};
	//======================================================================
	//!	ピクセルシェーダーインターフェース
	//======================================================================
	interface IPixelShader
		: public IShaderBase
	{
	public:
	};

	//======================================================================
	//!	シェーダー管理インターフェース
	//======================================================================
	interface IShaderManager
		: public kgl::IKGLBase
	{
	public:
		//!	頂点シェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		virtual b8 CreateVertexShader(IVertexShader** ppShader, const void* pData) = 0;
		//!	ピクセルシェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		virtual b8 CreatePixelShader(IPixelShader** ppShader, const void* pData) = 0;
	};
}


#endif	// __KG_SHADER_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgTexture.h
//!	@brief	テクスチャー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_TEXRURE_H__
#define	__KG_TEXRURE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"


namespace kgGraphics
{
	//!	サンプリング情報
	struct SampleDesc
	{
		s32	iCount;		//!< サンプル数
		s32	iQuality;	//!< 品質(-1で自動的に設定)

		//!	コンストラクタ
		SampleDesc(void) : iCount(1), iQuality(0) {}
	};
	//!	テクスチャーの情報
	struct TextureInfo
	{
		s32			iType;		//!< タイプ(ETextureType参照)
		s32			iUsage;		//!< 使用方法(ETextureUsage参照)
		s32			iFormat;	//!< フォーマット(ETextureFormat参照)
		u32			uiWidth;	//!< 横幅
		u32			uiHeight;	//!< 縦幅
		s32			iMipLevels;	//!< ミップマップ数
		SampleDesc	SampleDesc;	//!< サンプリング情報
	};

	//======================================================================
	//!	テクスチャーインターフェース
	//======================================================================
	interface ITextureBase
		: public kgl::IKGLBase
	{
	public:
		//!	テクスチャーのロック
		//!	@param iLevel	[in] ミップレベル
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return テクスチャーデータの先頭アドレス
		virtual void* Lock(s32 iLevel = 0, s32 iType = ELockType::Read) = 0;
		//!	テクスチャーのアンロック
		//!	@param iLevel [in] ミップレベル
		virtual void Unlock(s32 iLevel = 0) = 0;

		//!	リソースのコピー
		//!	@param pResource [in] コピー元のリソース
		//!	@return 結果
		virtual b8 CopyResource(ITextureBase* pResource) = 0;

		//!	テクスチャーの取得
		//!	@return テクスチャー
		virtual void* GetTexture(void) = 0;
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@param iLevel		[in]  ミップレベル
		//!	@return 結果
		virtual b8 GetSurface(ISurfaceBase** ppSurface, s32 iLevel = 0) = 0;

	public:
		//!	テクスチャー情報の取得
		//!	@return テクスチャー情報
		INLINE const TextureInfo& GetInfo(void) { return m_Info; }
		//!	テクスチャー情報の取得
		//!	@return テクスチャー情報
		INLINE kgl::Vector2 GetSize(void) { return kgl::Vector2((f32)m_Info.uiWidth, (f32)m_Info.uiHeight); }

	protected:
		TextureInfo	m_Info;		//!< テクスチャー情報
	};

	//======================================================================
	//!	テクスチャー管理インターフェース
	//======================================================================
	interface ITextureManager
		: public kgl::IKGLBase
	{
	public:
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param Info			[in]  テクスチャー情報
		//!	@param pData		[in]  ピクセルデータ
		//!	@return 結果
		virtual b8 CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData = null) = 0;
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param pData		[in]  DDSデータ
		//!	@return 結果
		virtual b8 CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData) = 0;
	};
}


#endif	// __KG_TEXRURE_H__
//======================================================================
//	END OF FILE
//======================================================================
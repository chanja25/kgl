//----------------------------------------------------------------------
//!
//!	@file	kgUtility.h
//!	@brief	Utility関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_UTILITY_H__
#define	__KG_UTILITY_H__

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define	KG_USE_LIB_PNG	1


namespace kgGraphics
{
	//!	ドライバータイプ
	namespace EDriverType
	{
		const s32 Unknown	= -1;
		const s32 DirectX9	= 0;
		const s32 DirectX11	= 1;
		const s32 OpenGL	= 2;
	}

	//!	ロックタイプ
	namespace ELockType
	{
		const s32 Read				= 0;
		const s32 Write				= 1;
		const s32 WriteNoOverWrite	= 2;
		const s32 ReadWrite			= 3;
	}
	//!	クエリタイプ
	namespace EQuery
	{
		const s32 Event				= 0;
		const s32 Occlusion			= 1;
		const s32 TimeStamp			= 2;
		const s32 TimeStampDisjoint	= 3;
	}
}

// include
#include "format/format.h"
#include "texture/kgTextureUtil.h"
#include "kgFormat.h"
#include "kgSurface.h"
#include "kgTexture.h"
#include "kgSwapChain.h"
#include "kgRenderTarget.h"
#include "kgPrimitive.h"
#include "kgShader.h"
#include "kgQuery.h"
#include "kgCommand.h"


#endif	// __KG_UTILITY_H__
//======================================================================
//	END OF FILE
//======================================================================
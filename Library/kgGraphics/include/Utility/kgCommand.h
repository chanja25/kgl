//----------------------------------------------------------------------
//!
//!	@file	kgCommand.h
//!	@brief	グラフィックコマンド関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_COMMAND_H__
#define	__KG_COMMAND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"

#define	DECLARE_VIRTUAL_COMMAND_FUNCTION(func, param)	virtual void func(Command::param& Info) = 0
#define	DECLARE_COMMAND_FUNCTION(func, param)			void func(Command::param& Info)

#define	DECLARE_COMMAND_INFO(name)				Command::name	m_##name
#define	DECLARE_COMMAND_INFO_ARRAY(name, count)	Command::name	m_##name[count]

#define	CheckInfo(name, param)	(m_##name.param != Info.param)
#define	RevertInfo(name, param)	(Info.param = m_##name.param)
#define	UpdateInfo(name)		(m_##name = Info)

#define	TEXSTAGE_MAX		(5)
#define	RENDER_STAGE_MAX	(5)
#define	VERTEX_STREAM_MAX	(5)

namespace kgGraphics
{
	namespace Command
	{
		//!	テスト方法
		namespace ETest
		{
			const s32 Disable		= 0;	//!< 無効
			const s32 Never			= 1;	//!< 常に失敗
			const s32 Always		= 2;	//!< 常に成功
			const s32 Less			= 3;	//!< 参照値より小さい場合に成功
			const s32 LessEqual		= 4;	//!< 参照値と同じか小さい場合に成功
			const s32 Greater		= 5;	//!< 参照値より大きい場合に成功
			const s32 GreaterEqual	= 6;	//!< 参照値と同じか大きい場合に成功
			const s32 Equal			= 7;	//!< 参照値と同じ場合に成功
			const s32 NotEqual		= 8;	//!< 参照値と違う場合に成功
		}
		//!	ステンシルテストをパスした際の演算方法
		namespace EStencilOp
		{
			const s32 Keep			= 0;	//!< 変更しない
			const s32 Zero			= 1;	//!< 0にする
			const s32 Replace		= 2;	//!< 参照値を設定
			const s32 Inclement		= 3;	//!< 現在の値をインクリメント
			const s32 Decrement		= 4;	//!< 現在の値をデクリメント
			const s32 Inverse		= 5;	//!< 現在の値をビット単位で反転する
			const s32 InclementWrap	= 6;	//!< 現在の値をインクリメント(255の次は0になる)
			const s32 DecrementWrap	= 7;	//!< 現在の値をデクリメント(0の次は255になる)
		}
		//!	ブレンド方法
		namespace EBlendOp
		{
			const s32 Disable	= 0;	//!< 無効
			const s32 Add		= 1;	//!< 加算
			const s32 Subtrace	= 2;	//!< 減算
			const s32 Reverse	= 3;	//!< デスティネーション値からソース値を減算
			const s32 Min		= 4;	//!< 小さい方を設定
			const s32 Max		= 5;	//!< 大きい方を設定
		}
		//!	ブレンド係数
		namespace EBlendParam
		{
			const s32 Zero			= 0;	//!< 0
			const s32 One			= 1;	//!< 1
			const s32 SrcRGB		= 2;	//!< ソースのRGB値
			const s32 SrcRGB1M		= 3;	//!< ソースのRGB値の補数
			const s32 DstRGB		= 4;	//!< デストネーションのRGB値
			const s32 DstRGB1M		= 5;	//!< デストネーションのRGB値の補数
			const s32 SrcAlpha		= 6;	//!< ソースのα値
			const s32 SrcAlpha1M	= 7;	//!< ソースのα値の補数
			const s32 DstAlpha		= 8;	//!< デストネーションのα値
			const s32 DstAlpha1M	= 9;	//!< デストネーションのα値の補数
//			const s32 ConstRGB		= 10;	//!< 設定されている定数のRGB値
//			const s32 ConstRGB1M	= 11;	//!< 設定されている定数のRGB値の補数
//			const s32 ConstAlpha	= 12;	//!< 設定されている定数のα値
//			const s32 ConstAlpha1M	= 13;	//!< 設定されている定数のα値の補数
		}
		//!	カリングモード
		namespace ECullMode
		{
			const s32 CW	= 0;	//!< CW
			const s32 CCW	= 1;	//!< CCW
			const s32 None	= 2;	//!< None
		}
		//!	テクスチャフィルタ
		namespace ETextureFilter
		{
			const s32 Nearest	= 0;	//!< Nearest
			const s32 Linear	= 1;	//!< Linear(default値)
		}
		//!	テクスチャアドレッシングモード
		namespace ETextureWrap
		{
			const s32 Repeat	= 0;	//!< 繰り返し(default値)
			const s32 Mirror	= 1;	//!< 反転
			const s32 Clamp		= 2;	//!< 0〜1まで
			const s32 Border	= 3;	//!< 0〜1まで(範囲外は境界色)
		}
		//!	頂点の形状
		namespace EPrimitive
		{
			const s32 PointList		= 0;	//!< ポイント
			const s32 LineList		= 1;	//!< ラインリスト
			const s32 LineStrip		= 2;	//!< ラインストリップ
			const s32 TriangleList	= 3;	//!< トライアングル
			const s32 TriangleStrip	= 4;	//!< トライアングルストリップ
		}
		//!	頂点の形状
		namespace EShaderParam
		{
			const s32 Boolean	= 0;	//!< Boolean
			const s32 S32		= 1;	//!< 32bit整数
			const s32 F32		= 2;	//!< 32bit浮動小数
			const s32 Vector4	= 3;	//!< ベクトル
			const s32 Matrix	= 4;	//!< 行列

			const s32 Constant	= 255;	//!< コンスタントバッファ
		}
		//!	クリアターゲット
		namespace EClearTarget
		{
			const s32 Color			= (1 << 0);			//!< カラー
			const s32 Depth			= (1 << 1);			//!< デプス
			const s32 Stencil		= (1 << 2);			//!< ステンシル
			const s32 DepthStencil	= Depth | Stencil;	//!< デプス&ステンシル
			const s32 All			= 0xffffffff;		//!< 全て
		}
		//!	コマンド
		namespace ECommand
		{
			const s32 AlphaTest			= 0;	//!< アルファテスト
			const s32 DepthTest			= 1;	//!< デプステスト
			const s32 StencilTest		= 2;	//!< ステンシルテスト
			const s32 Blend				= 3;	//!< ブレンド
			const s32 CullMode			= 4;	//!< カリングモード

			const s32 Texture			= 21;	//!< テクスチャ
			const s32 LODBias			= 22;	//!< LODバイアス
			const s32 SamplerState		= 23;	//!< サンプラーステート

			const s32 ViewPort			= 30;	//!< ビューポート
			const s32 RenderTarget		= 31;	//!< レンダーターゲット
			const s32 Declaration		= 32;	//!< 頂点フォーマット
			const s32 VertexBind		= 33;	//!< 頂点バインド
			const s32 Draw				= 34;	//!< 描画
			const s32 DrawIndex			= 35;	//!< 描画(インデックス有り)

			const s32 VertexShader		= 40;	//!< 頂点シェーダ
			const s32 VertexShaderParam	= 41;	//!< 頂点シェーダパラメータ
			const s32 PixelShader		= 42;	//!< ピクセルシェーダ
			const s32 PixelShaderParam	= 43;	//!< ピクセルシェーダパラメータ

			const s32 ClearTarget		= 50;	//!< ターゲットのクリア
			const s32 Scissoring		= 51;	//!< シザリング設定
			const s32 View				= 52;	//!< ビュー行列設定
			const s32 Projection		= 53;	//!< プロジェクション行列設定
			const s32 World				= 54;	//!< ワールド行列設定
		}

		//!	アルファテスト情報
		struct AlphaTestInfo
		{
			s32	iTest;	//!< テスト
			u8	ucRef;	//!< 参照パラメータ
			u8	pad[3];
		};
		//!	デプステスト情報
		struct DepthTestInfo
		{
			s32			iTest;		//!< テスト
			BITFIELD	bWrite:1;	//!< 書き込みの有無
		};
		//!	ステンシルテスト情報
		struct StencilTestInfo
		{
			s32			iTest;			//!< テスト
			s32			iPassOp;		//!< 成功時の処理
			s32			iFailOp;		//!< 失敗の処理
			s32			iDepthFailOp;	//!< デプステスト失敗時の処理
			BITFIELD	bWriteMask:1;	//!< 書き込みの有無
			u8			ucMask;			//!< マスク
			u8			ucRef;			//!< 参照パラメータ
			u8			pad[2];
		};
		//!	ブレンド情報
		struct BlendInfo
		{
			s32	iColorOp;	//!< カラーブレンド
			s32	iAlphaOp;	//!< アルファブレンド
			s32	iSrcColor;	//!< ソースカラーの係数
			s32	iSrcAlpha;	//!< ソースアルファの係数
			s32	iDstColor;	//!< ディストネーションカラーの係数
			s32	iDstAlpha;	//!< ディストネーションアルファの係数
		};
		//!	カリング情報
		struct CullModeInfo
		{
			s32	iMode;		//!< カリングモード
		};
		//!	テクスチャ情報
		struct TextureInfo
		{
			s32				iStage;		//!< テクスチャステージ
			ITextureBase*	pTexture;	//!< テクスチャ
		};
		//!	サンプラーステート情報
		struct SamplerStateInfo
		{
			s32	iStage;		//!< テクスチャステージ
			s32	iFilterMin;	//!< Minフィルター
			s32	iFilterMag;	//!< Magフィルター
			s32	iFilterMip;	//!< Mipフィルター
			s32	iWrapU;		//!< U方向のラップモード
			s32	iWrapV;		//!< V方向のラップモード
		};
		//!	サンプラーステート情報
		struct LODBiasInfo
		{
			s32	iStage;			//!< テクスチャステージ
			s32	iMaxMipLevel;	//!< 最大Mipmapレベル
			f32	fBias;			//!< LODバイアス
		};
		//!	ビューポート情報
		struct ViewPortInfo
		{
			s32	iX;			//!< X座標
			s32	iY;			//!< Y座標
			s32	iWidth;		//!< 横幅
			s32	iHeight;	//!< 縦幅
			f32	fMinZ;		//!< Zの最低値
			f32	fMaxZ;		//!< Zの最大値
		};
		//!	レンダーターゲット情報
		struct RenderTargetInfo
		{
			s32				iIndex;		//!< インデックス
			ISurfaceBase*	pTarget;	//!< サーフェイス
		};
		//!	デプスターゲット情報
		struct DepthTargetInfo
		{
			ISurfaceBase*	pDepthTarget;	//!< サーフェイス
		};
		//!	頂点フォーマット情報
		struct DeclarationInfo
		{
			IDeclaration*	pDeclaration;	//!< 頂点フォーマット
		};
		//!	頂点バインド情報
		struct VertexBindInfo
		{
			IVertexBuffer*	pVertex;	//!< 頂点バッファ
			s32				iStream;	//!< ストリーム番号
			s32				iOffset;	//!< オフセット
			s32				iStride;	//!< ストライド
		};
		//!	頂点バインド情報
		struct IndexBindInfo
		{
			IIndexBuffer*	pIndex;		//!< インデックスバッファ
			s32				iOffset;	//!< オフセット
		};
		//!	描画情報
		struct DrawPrimitiveInfo
		{
			s32			iPrimitive;	//!< 形状
			s32			iCount;		//!< iPrimitiveの描画数
			const void*	pVertex;	//!< 頂点データ
			s32			iStride;	//!< ストライド
		};
		//!	描画情報
		struct DrawInfo
		{
			s32	iPrimitive;		//!< 形状
			s32	iCount;			//!< iPrimitiveの描画数
			s32	iVertexOffset;	//!< 頂点オフセット
		};
		//!	描画情報
		struct DrawIndexPrimitiveInfo
		{
			s32			iPrimitive;		//!< 形状
			s32			iCount;			//!< iPrimitiveの描画数
			const void*	pIndex;			//!< Indexデータ
			s32			iIndexType;		//!< Indexタイプ(EIndexType参照)
			const void*	pVertex;		//!< 頂点データ
			s32			iVertexCount;	//!< 頂点数
			s32			iStride;		//!< ストライド
		};
		//!	描画情報(インデックス有り)
		struct DrawIndexInfo
		{
			s32	iPrimitive;		//!< 形状
			s32	iCount;			//!< iPrimitiveの描画数
			s32	iVertexCount;	//!< 頂点数
			s32	iVertexOffset;	//!< 頂点オフセット
			s32	iIndexOffset;	//!< インデックスオフセット
		};
		//!	頂点シェーダ情報
		struct VertexShaderInfo
		{
			IVertexShader*	pShader;	//!< シェーダ
		};
		//!	頂点シェーダパラメータ情報
		struct VertexShaderParamInfo
		{
			s32		iRegister;	//!< レジスタ番号
			s32		iType;		//!< パラメータの種類
			s32		iCount;		//!< パラメータ数
			void*	pParam;		//!< パラメータ
		};
		//!	ピクセルシェーダ情報
		struct PixelShaderInfo
		{
			IPixelShader*	pShader;	//!< シェーダ
		};
		//!	ピクセルシェーダパラメータ情報
		struct PixelShaderParamInfo
		{
			s32		iRegister;	//!< レジスタ番号
			s32		iType;		//!< パラメータの種類
			s32		iCount;		//!< パラメータ数
			void*	pParam;		//!< パラメータ
		};
		//!	ターゲットクリア情報
		struct ClearTargetInfo
		{
			s32	iTarget;		//!< ターゲット
			u32	uiColor;		//!< クリアカラー
			f32	fDepth;			//!< デプス
			s32	iStencil;		//!< ステンシル
		};
		//!	シザリング情報
		struct ScissoringInfo
		{
			s32	iX;			//!< 左端
			s32	iY;			//!< 上端
			s32	iWidth;		//!< 横幅
			s32	iHeight;	//!< 縦幅
		};
		//!	ビュー行列情報
		struct ViewInfo
		{
			kgl::Matrix*	pView;	//!< ビュー行列
		};
		//!	プロジェクション行列情報
		struct ProjectionInfo
		{
			kgl::Matrix*	pProjection;	//!< プロジェクション行列
		};
		//!	ワールド行列情報
		struct WorldInfo
		{
			s32				iCount;	//!< 数
			kgl::Matrix*	pWorld;	//!< ワールド行列
		};
	}

	//======================================================================
	//!	グラフィックコマンドインターフェース
	//======================================================================
	interface ICommandCreator
		: public kgl::IKGLBase
	{
	public:
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetAlphaTest, AlphaTestInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetDepthTest, DepthTestInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetStencilTest, StencilTestInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetBlend, BlendInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetCullMode, CullModeInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetTexture, TextureInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetLODBias, LODBiasInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetSamplerState, SamplerStateInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetViewPort, ViewPortInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetRenderTarget, RenderTargetInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetDepthTarget, DepthTargetInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetDeclaration, DeclarationInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetVertex, VertexBindInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetIndex, IndexBindInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(DrawPrimitiveUP, DrawPrimitiveInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(DrawPrimitive, DrawInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(DrawIndexedPrimitiveUP, DrawIndexPrimitiveInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(DrawIndexedPrimitive, DrawIndexInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetVertexShader, VertexShaderInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetVertexShaderParam, VertexShaderParamInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetPixelShader, PixelShaderInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetPixelShaderParam, PixelShaderParamInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(ClearTarget, ClearTargetInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetScissoring, ScissoringInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetView, ViewInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetProjection, ProjectionInfo);
		DECLARE_VIRTUAL_COMMAND_FUNCTION(SetWorld, WorldInfo);

	public:
		//!	コマンド情報のリセット
		virtual void ResetState(void);
		//!	コマンド情報のキャッシュが使用できるか？
		virtual b8 IsSaveCommand(void) { return false; }
		//!	コマンド情報の記録開始
		virtual void SaveCommand(void) {}
		//!	記録したコマンド情報の使用
		virtual void UseCommand(void) {}

	public:
		//!	描画開始
		virtual void Begin(void) { ResetState(); }
		//!	描画終了
		virtual void End(void) {}

	protected:
		DECLARE_COMMAND_INFO(AlphaTestInfo);
		DECLARE_COMMAND_INFO(DepthTestInfo);
		DECLARE_COMMAND_INFO(StencilTestInfo);
		DECLARE_COMMAND_INFO(BlendInfo);
		DECLARE_COMMAND_INFO(CullModeInfo);
		DECLARE_COMMAND_INFO_ARRAY(TextureInfo, TEXSTAGE_MAX);
		DECLARE_COMMAND_INFO_ARRAY(LODBiasInfo, TEXSTAGE_MAX);
		DECLARE_COMMAND_INFO_ARRAY(SamplerStateInfo, TEXSTAGE_MAX);
		DECLARE_COMMAND_INFO(ViewPortInfo);
		DECLARE_COMMAND_INFO_ARRAY(RenderTargetInfo, RENDER_STAGE_MAX);
		DECLARE_COMMAND_INFO(DepthTargetInfo);
		DECLARE_COMMAND_INFO(DeclarationInfo);
		DECLARE_COMMAND_INFO_ARRAY(VertexBindInfo, VERTEX_STREAM_MAX);
		DECLARE_COMMAND_INFO(IndexBindInfo);
		DECLARE_COMMAND_INFO(VertexShaderInfo);
		DECLARE_COMMAND_INFO(PixelShaderInfo);
		DECLARE_COMMAND_INFO(ScissoringInfo);
	};
}


#endif	// __KG_COMMAND_H__
//======================================================================
//	END OF FILE
//======================================================================
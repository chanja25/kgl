//----------------------------------------------------------------------
//!
//!	@file	GLSurface.h
//!	@brief	サーフェイス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_SURFACE_H__
#define	__GL_SURFACE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//======================================================================
	//!	サーフェイスクラス
	//======================================================================
	class CGLSurface
		: public ISurfaceBase
	{
	public:
		//!	サーフェイスのロック
		//!	@return サーフェイスデータの先頭アドレス
		void* Lock(s32 iType);
		//!	サーフェイスのアンロック
		void Unlock(void);

	public:
		//!	サーフェイスの取得
		//!	@return サーフェイス
		INLINE void Setup(const SurfaceInfo& Info, void* pSurface)
		{
			(void)pSurface;
			m_Info		= Info;
		}
		//!	サーフェイスの取得
		//!	@return サーフェイス
		INLINE void* GetSurface(void) { return null; }

	private:
	};
}

#endif	// __GL_SURFACE_H__
//======================================================================
//	END OF FILE
//======================================================================
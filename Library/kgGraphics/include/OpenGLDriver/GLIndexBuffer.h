//----------------------------------------------------------------------
//!
//!	@file	GLIndexBuffer.h
//!	@brief	インデックスバッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_INDEXBUFFER_H__
#define	__GL_INDEXBUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//======================================================================
	//!	インデックスバッファ
	//======================================================================
	class CGLIndexBuffer
		: public IIndexBuffer
	{
	public:
		//!	コンストラクタ
		CGLIndexBuffer(void);
		//!	デストラクタ
		~CGLIndexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param iSize	[in] バッファサイズ
		//!	@param ucType	[in] タイプ(EIndexType参照)
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 Initialize(CGLDevice* pDevice, u32 uiSize, u8 ucType, b8 bDynamic);

	public:
		//!	バッファのロック
		//!	@param uiOffset	[in] ロックするバッファのオフセット
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		void* Lock(u32 uiOffset, u32 uiSize, s32 iType);
		//!	バッファのアンロック
		void Unlock(void);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetBuffer(void) { return &m_uiBuffer; }

	private:
		GLuint		m_uiBuffer;
		u32			m_uiSize;
		BITFIELD	m_bLock: 1;
	};
}

#endif	// __GL_INDEXBUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
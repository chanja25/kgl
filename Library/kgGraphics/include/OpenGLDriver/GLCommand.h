//----------------------------------------------------------------------
//!
//!	@file	GLCommand.h
//!	@brief	コマンド生成
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_COMMAND_H__
#define	__GL_COMMAND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//======================================================================
	//!	グラフィックコマンドクリエーター
	//======================================================================
	class CGLCommand
		: public ICommandCreator
	{
	public:
		CGLCommand(kgl::TKGLPtr<CGLDevice> pDevice);

	public:
		DECLARE_COMMAND_FUNCTION(SetAlphaTest, AlphaTestInfo);
		DECLARE_COMMAND_FUNCTION(SetDepthTest, DepthTestInfo);
		DECLARE_COMMAND_FUNCTION(SetStencilTest, StencilTestInfo);
		DECLARE_COMMAND_FUNCTION(SetBlend, BlendInfo);
		DECLARE_COMMAND_FUNCTION(SetCullMode, CullModeInfo);
		DECLARE_COMMAND_FUNCTION(SetTexture, TextureInfo);
		DECLARE_COMMAND_FUNCTION(SetLODBias, LODBiasInfo);
		DECLARE_COMMAND_FUNCTION(SetSamplerState, SamplerStateInfo);
		DECLARE_COMMAND_FUNCTION(SetViewPort, ViewPortInfo);
		DECLARE_COMMAND_FUNCTION(SetRenderTarget, RenderTargetInfo);
		DECLARE_COMMAND_FUNCTION(SetDepthTarget, DepthTargetInfo);
		DECLARE_COMMAND_FUNCTION(SetDeclaration, DeclarationInfo);
		DECLARE_COMMAND_FUNCTION(SetVertex, VertexBindInfo);
		DECLARE_COMMAND_FUNCTION(SetIndex, IndexBindInfo);
		DECLARE_COMMAND_FUNCTION(DrawPrimitiveUP, DrawPrimitiveInfo);
		DECLARE_COMMAND_FUNCTION(DrawPrimitive, DrawInfo);
		DECLARE_COMMAND_FUNCTION(DrawIndexedPrimitiveUP, DrawIndexPrimitiveInfo);
		DECLARE_COMMAND_FUNCTION(DrawIndexedPrimitive, DrawIndexInfo);
		DECLARE_COMMAND_FUNCTION(SetVertexShader, VertexShaderInfo);
		DECLARE_COMMAND_FUNCTION(SetVertexShaderParam, VertexShaderParamInfo);
		DECLARE_COMMAND_FUNCTION(SetPixelShader, PixelShaderInfo);
		DECLARE_COMMAND_FUNCTION(SetPixelShaderParam, PixelShaderParamInfo);
		DECLARE_COMMAND_FUNCTION(ClearTarget, ClearTargetInfo);
		DECLARE_COMMAND_FUNCTION(SetScissoring, ScissoringInfo);
		DECLARE_COMMAND_FUNCTION(SetView, ViewInfo);
		DECLARE_COMMAND_FUNCTION(SetProjection, ProjectionInfo);
		DECLARE_COMMAND_FUNCTION(SetWorld, WorldInfo);

	private:
		//!	頂点フォーマットの設定
		void SetDeclaration(Command::DeclarationInfo& Info, s32 iStride, const void* pVertex = null);

	private:
		kgl::TKGLPtr<CGLDevice>		m_pDevice;			//!< デバイス
		kgl::TKGLPtr<ISurfaceBase>	m_pBackBuffer;		//!< バックバッファ
		kgl::TKGLPtr<ISurfaceBase>	m_pDepthBuffer;		//!< デプスバッファ
		BITFIELD					m_bUpdateDecl: 1;	//!< 頂点フォーマットが変わったか？
		u32							m_uiDeclCount;		//!< 頂点フォーマットの数
	};
}

#endif	// __GL_COMMAND_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	GLTexture.h
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_TEXTURE_H__
#define	__GL_TEXTURE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//======================================================================
	//!	テクスチャー管理クラス
	//======================================================================
	class CGLTextureManager
		: public ITextureManager
	{
	public:
		//!	コンストラクタ
		CGLTextureManager(kgl::TKGLPtr<CGLDevice> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param Info			[in] テクスチャー情報
		//!	@param pData		[in] ピクセルデータ
		//!	@return 結果
		b8 CreateTexture(ITextureBase** ppTexture, const TextureInfo& Info, const void* pData);
		//!	テクスチャー生成
		//!	@param ppTexture	[out] テクスチャー
		//!	@param pData		[in] DDSデータ
		//!	@return 結果
		b8 CreateTextureFromDDS(ITextureBase** ppTexture, const void* pData);

	private:
		kgl::TKGLPtr<CGLDevice>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	テクスチャークラス
	//======================================================================
	class CGLTexture2D
		: public ITextureBase
	{
	public:
		//!	コンストラクタ
		CGLTexture2D(void);
		//!	デストラクタ
		~CGLTexture2D(void);

		//!	テクスチャー生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] テクスチャー情報
		//!	@param pData	[in] ピクセルデータ
		//!	@return 結果
		b8 Create(CGLDevice* pDevice, const TextureInfo& Info, const void* pData);
		//!	DDSからテクスチャー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] DDSデータ
		//!	@return 結果
		b8 CreateFromDDS(CGLDevice* pDevice, const void* pData);

	public:
		//!	テクスチャーのロック
		//!	@param iLevel	[in] ミップレベル
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return テクスチャーデータの先頭アドレス
		void* Lock(s32 iLevel, s32 iType);
		//!	テクスチャーのアンロック
		//!	@param iLevel [in] ミップレベル
		void Unlock(s32 iLevel);

	public:
		//!	テクスチャーの取得
		//!	@return テクスチャー
		INLINE void* GetTexture(void) { return &m_uiTexture; }
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@param iLevel		[in]  ミップレベル
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface, s32 iLevel);

	private:
		GLuint	m_uiTexture;
	};
}

#endif	// __GL_TEXTURE_H__
//======================================================================
//	END OF FILE
//======================================================================
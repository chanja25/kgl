//----------------------------------------------------------------------
//!
//!	@file	GLDevice.h
//!	@brief	OpenGL
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_DEVICE_H__
#define	__GL_DEVICE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"

#include <gl/gl.h>
#include <gl/glu.h>
#include "GL/glext.h"
#include "GLFunction.h"
#include "GLHelper.h"

#if	KG_GRAPHICS_DEBUG
#define	VerifyResult(x)	kgAssert((x), #x)
#else	// ~#if KG_GRAPHICS_DEBUG
#define	VerifyResult(x)	(x)
#endif	// ~#if KG_GRAPHICS_DEBUG


namespace kgGraphics
{
	//!	無効なインデックス
	const GLuint IndexNone		= 0xffffffff;

	//!	描画コンテキスト
	struct DrawContext
	{
		HWND	hWnd;	//!< ウィンドウハンドル
		HDC		hDC;	//!< Windowのデバイスコンテキスト
		HGLRC	hGLRC;	//!< OpenGL用の描画コンテキスト
	};

	//======================================================================
	//	DirectX9用Device
	//======================================================================
	class CGLDevice
		: public IGraphicDevice
	{
	public:
		//!	コンストラクタ
		CGLDevice(void);
		//!	デストラクタ
		~CGLDevice(void);

	public:
		//!	初期化
		//!	@param info	[in] グラフィック設定
		//!	@return 結果
		b8 Initialize(const GRAPHICS_INFO& info);
		//!	終了処理
		void Finalize(void);

	public:
		//!	描画シーン開始
		void BeginScene(void);
		//!	描画シーン終了
		void EndScene(void);

	public:
		//!	GPUの処理終了待ち
		void FlushCommand(void);

	public:
		//!	バックバッファの取得
		//!	@param ppBackBuffer [out] バックバッファ
		void GetBackBuffer(ISurfaceBase** ppBackBuffer);
		//!	デプスバッファの取得
		//!	@param ppDepthBuffer [out] デプスバッファ
		void GetDepthBuffer(ISurfaceBase** ppDepthBuffer);

		//!	VSyncの有無を設定
		//!	@param bEnable	[in] VSyncの有無
		void SetVSync(b8 bEnable);

	public:
		//!	描画コンテキストの取得
		//!	@return DrawContext
		INLINE DrawContext& GetDrawContext(void) { return m_DrawContext; }
		//!	パラメーターの取得
		//!	@return パラメーター
		//INLINE D3DPRESENT_PARAMETERS* GetPresentParam(void) { return &m_d3dpp; }

	private:
		DrawContext	m_DrawContext;
	};
}

#endif	// __GL_DEVICE_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	GLDeclaration.h
//!	@brief	頂点宣言
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_DECLARATION_H__
#define	__GL_DECLARATION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"


namespace kgGraphics
{
	//!	頂点フォーマット情報(OpenGL)
	struct GLDeclarationInfo
	{
		u32									uiCount;	//!< 頂点フォーマットの種類
		kgl::TSmartPtr<DeclarationInfo>	pDeclInfo;	//!< 頂点フォーマット
	};

	//======================================================================
	//!	頂点フォーマット設定用クラス
	//======================================================================
	class CGLDeclaration
		: public IDeclaration
	{
	public:
		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param pDecl	[in] 頂点宣言
		//!	@return 結果
		b8 Initialize(CGLDevice* pDevice, const DeclarationInfo* pDecl);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetDeclaration(void) { return &m_DeclInfo; }

	private:
		GLDeclarationInfo	m_DeclInfo;
	};
}

#endif	// __GL_DECLARATION_H__
//======================================================================
//	END OF FILE
//======================================================================
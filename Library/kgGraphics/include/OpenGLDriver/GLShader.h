//----------------------------------------------------------------------
//!
//!	@file	GLShader.h
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_SHADER_H__
#define	__GL_SHADER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//======================================================================
	//!	シェーダー管理インターフェース
	//======================================================================
	class CGLShaderManager
		: public IShaderManager
	{
	public:
		//!	コンストラクタ
		CGLShaderManager(kgl::TKGLPtr<CGLDevice> pDevice)
			: m_pDevice(pDevice)
		{}

	public:
		//!	頂点シェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		b8 CreateVertexShader(IVertexShader** ppShader, const void* pData);
		//!	ピクセルシェーダー生成
		//!	@param ppShader	[out] 頂点シェーダー
		//!	@param pData	[in]  シェーダーデータ
		//!	@return 結果
		b8 CreatePixelShader(IPixelShader** ppShader, const void* pData);

	private:
		kgl::TKGLPtr<CGLDevice>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	頂点シェーダーインターフェース
	//======================================================================
	class CGLVertexShader
		: public IVertexShader
	{
	public:
		//!	頂点シェーダー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] シェーダーデータ
		//!	@return 結果
		b8 Create(CGLDevice* pDevice, const void* pData);

	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		INLINE void* GetShader(void) { return null; }
	};
	//======================================================================
	//!	ピクセルシェーダーインターフェース
	//======================================================================
	class CGLPixelShader
		: public IPixelShader
	{
	public:
		//!	頂点シェーダー生成
		//!	@param pDevice	[in] デバイス
		//!	@param pData	[in] シェーダーデータ
		//!	@return 結果
		b8 Create(CGLDevice* pDevice, const void* pData);

	public:
		//!	シェーダーの取得
		//!	@return シェーダー
		INLINE void* GetShader(void) { return null; }
	};
}

#endif	// __GL_SHADER_H__
//======================================================================
//	END OF FILE
//======================================================================
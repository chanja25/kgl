//----------------------------------------------------------------------
//!
//!	@file	GLVertexBuffer.h
//!	@brief	頂点バッファ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_VERTEXBUFFER_H__
#define	__GL_VERTEXBUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//======================================================================
	//!	頂点バッファ
	//======================================================================
	class CGLVertexBuffer
		: public IVertexBuffer
	{
	public:
		//!	コンストラクタ
		CGLVertexBuffer(void);
		//!	デストラクタ
		~CGLVertexBuffer(void);

		//!	初期化
		//!	@param pDevice	[in] デバイス
		//!	@param iSize	[in] バッファサイズ
		//!	@param bDynamic	[in]  動的にバッファを変更するか？
		//!	@return 結果
		b8 Initialize(CGLDevice* pDevice, u32 uiSize, b8 bDynamic);

	public:
		//!	バッファのロック
		//!	@param uiOffset	[in] ロックするバッファのオフセット
		//!	@param uiSize	[in] ロックするバッファサイズ
		//!	@param iType	[in] ロックタイプ(ELockType参照)
		//!	@return	バッファ
		void* Lock(u32 uiOffset, u32 uiSize, s32 iType);
		//!	バッファのアンロック
		void Unlock(void);

	public:
		//!	バッファ取得
		//!	@return	バッファ
		INLINE void* GetBuffer(void) { return &m_uiBuffer; }

	private:
		GLuint	m_uiBuffer;
		u32		m_uiSize;
	};
}

#endif	// __GL_VERTEXBUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
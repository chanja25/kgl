//----------------------------------------------------------------------
//!
//!	@file	GLHelper.h
//!	@brief	コマンド生成
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_HELPER_H__
#define	__GL_HELPER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"

namespace kgGraphics
{
	//----------------------------------------------------------------------
	//!	バッファのバインド
	//----------------------------------------------------------------------
	template<GLenum type>
	FORCEINLINE void BindBuffer(GLuint uiIndex)
	{
		static GLuint BindIndex = IndexNone;
		if( BindIndex != uiIndex )
		{
			glBindBuffer(type, uiIndex);
			BindIndex = uiIndex;
		}
	}
	//----------------------------------------------------------------------
	//!	バッファのバインド
	//----------------------------------------------------------------------
	FORCEINLINE void ActiveTexture(GLenum stage)
	{
		static GLenum ActiveStage = 0;
		if( ActiveStage != stage )
		{
			glActiveTexture(stage);
			ActiveStage = stage;
		}
	}
}

#endif	// __GL_HELPER_H__
//======================================================================
//	END OF FILE
//======================================================================
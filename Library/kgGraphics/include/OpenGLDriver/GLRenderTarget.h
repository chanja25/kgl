//----------------------------------------------------------------------
//!
//!	@file	GLRenderTarget.h
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GL_RENDERTARGET_H__
#define	__GL_RENDERTARGET_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgGraphics.h"
#include "GLDevice.h"
#include "GLTexture.h"

namespace kgGraphics
{
	//======================================================================
	//!	レンダーターゲット管理インターフェース
	//======================================================================
	class CGLRenderTargetManager
		: public IRenderTargetManager
	{
	public:
		//!	コンストラクタ
		CGLRenderTargetManager(kgl::TKGLPtr<CGLDevice> pDevice)
			: m_pDevice(pDevice)
		{}
		//!	レンダーターゲット生成
		//!	@param ppRenderTarget	[out] レンダーターゲット
		//!	@param Info				[in]  レンダーターゲット情報
		//!	@return 結果
		b8 CreateRenderTarget(IRenderTargetBase** ppRenderTarget, const RenderTargetInfo& Info);

	private:
		kgl::TKGLPtr<CGLDevice>	m_pDevice;	//!< デバイス
	};

	//======================================================================
	//!	レンダーターゲットクラス
	//======================================================================
	class CGLRenderTarget
		: public IRenderTargetBase
	{
	public:
		//!	レンダーターゲット生成
		//!	@param pDevice	[in] デバイス
		//!	@param Info		[in] レンダーターゲット情報
		//!	@return 結果
		b8 Create(CGLDevice* pDevice, const RenderTargetInfo& Info);

	public:
		//!	レンダーターゲットのロック
		//!	@return レンダーターゲットデータの先頭アドレス
		void* Lock(s32 iType);
		//!	レンダーターゲットのアンロック
		void Unlock(void);

	public:
		//!	レンダーターゲット(テクスチャー)の取得
		//!	@return レンダーターゲット
		void GetRenderTarget(ITextureBase** ppTexture);
		//!	サーフェイスの取得
		//!	@param ppSurface	[out] サーフェイス
		//!	@return 結果
		b8 GetSurface(ISurfaceBase** ppSurface);

	private:
		kgl::TKGLPtr<CGLTexture2D>	m_pRenderTarget;	//!< テクスチャー
	};
}

#endif	// __GL_RENDERTARGET_H__
//======================================================================
//	END OF FILE
//======================================================================
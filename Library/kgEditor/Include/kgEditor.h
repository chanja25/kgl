//----------------------------------------------------------------------
//!
//!	@file	kgEditor.h
//!	@brief	Kisaragi Game Editor ヘッダー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_EDITOR_H__
#define	__KG_EDITOR_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../../kgl/include/kgl.h"

#if	KGL_USE_WXWIDGETS
#include <wx/wx.h>
#endif	// ~#if	KGL_USE_WXWIDGETS

#if	KGL_DEBUG
#define KG_EDITOR_DEBUG	1
#else	// ~#if KGL_DEBUG
#define KG_EDITOR_DEBUG	0
#endif	// ~#if KGL_DEBUG


//!	アプリケーションの起動
extern s32 wxLaunchGame(void);

//!	アプリケーションの終了
extern void wxExitGame(void);


#endif	// __KG_EDITOR_H__
//======================================================================
//	END OF FILE
//======================================================================
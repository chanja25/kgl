//----------------------------------------------------------------------
//!
//!	@file	kgFrame.h
//!	@brief	Kisaragi Game Editor �w�b�_�[
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_FRAME_H__
#define	__KG_FRAME_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgEditor.h"


enum
{
    ID_Quit = 1,
    ID_About,
};

class MyFrame
	: public wxFrame
{
public:
	MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size);

    virtual bool Destroy();
	virtual void OnQuit(wxCommandEvent& event);
	virtual void OnAbout(wxCommandEvent& event);

	DECLARE_EVENT_TABLE()
};


#endif	// __KG_EDITOR_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgEditor.cpp
//!	@brief	Kisaragi Game Editor
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgEditor.h"


#if	KGL_USE_WXWIDGETS
#include <wx/wx.h>
#include "Frame/kgFrame.h"

//----------------------------------------------------------------------
// library
//----------------------------------------------------------------------
#if	defined(_DEBUG)
	#pragma comment(lib, "wxmsw28ud_core.lib")
	#pragma comment(lib, "wxbase28ud.lib")
	//#pragma comment(lib, "wxregexud.lib")
	//#pragma comment(lib, "wxtiffd.lib")
	//#pragma comment(lib, "wxmsw28ud_adv.lib")
	//#pragma comment(lib, "wxbase28ud_net.lib")
	//#pragma comment(lib, "wxmsw28ud_media.lib")
	//#pragma comment(lib, "wxbase28ud_odbc.lib")
	//#pragma comment(lib, "wxmsw28ud_dbgrid.lib")
	//#pragma comment(lib, "wxmsw28ud_xrc.lib")
	//#pragma comment(lib, "wxbase28ud_xml.lib")
	//#pragma comment(lib, "wxmsw28ud_richtext.lib")
	//#pragma comment(lib, "wxmsw28ud_qa.lib")
	//#pragma comment(lib, "wxmsw28ud_gl.lib")
	//#pragma comment(lib, "wxmsw28ud_aui.lib")
#else	// ~#if	defined(_DEBUG)
	#pragma comment(lib, "wxmsw28u_core.lib")
	#pragma comment(lib, "wxbase28u.lib")
	//#pragma comment(lib, "wxregexu.lib")
	//#pragma comment(lib, "wxtiff.lib")
	//#pragma comment(lib, "wxmsw28u_adv.lib")
	//#pragma comment(lib, "wxbase28u_net.lib")
	//#pragma comment(lib, "wxmsw28u_media.lib")
	//#pragma comment(lib, "wxbase28u_odbc.lib")
	//#pragma comment(lib, "wxmsw28u_dbgrid.lib")
	//#pragma comment(lib, "wxmsw28u_xrc.lib")
	//#pragma comment(lib, "wxbase28u_xml.lib")
	//#pragma comment(lib, "wxmsw28u_richtext.lib")
	//#pragma comment(lib, "wxmsw28u_qa.lib")
	//#pragma comment(lib, "wxmsw28u_gl.lib")
	//#pragma comment(lib, "wxmsw28u_aui.lib")
#endif	// ~#if	defined(_DEBUG)

#pragma comment(lib, "comctl32.lib")
#pragma comment(lib, "rpcrt4.lib")
//#pragma comment(lib, "wsock32.lib")
//#pragma comment(lib, "odbc32.lib")


//----------------------------------------------------------------------
//	ゲームの起動(wxWidgets用)
//----------------------------------------------------------------------
s32 wxLaunchGame(void)
{
	return wxEntry(GetModuleHandle(null), null, "", 1);
}
//----------------------------------------------------------------------
//	ゲームの終了(wxWidgets用)
//----------------------------------------------------------------------
void wxExitGame(void)
{
	wxExit();
	wxWakeUpIdle();
	kgl::Config::ExitRequest = true;
}

//	ゲームスレッドの起動
extern bool LaunchGameThread(void);
//	ゲームスレッドの終了待ち
extern s32 ExitWaitGameThread(void);

//======================================================================
//	wxWidgets用のアプリケーションクラス
//======================================================================
class kglLaunchApp
	: public wxApp
{
public:
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	virtual bool OnInit()
	{
		wxApp::OnInit();

		MyFrame *frame = new MyFrame(TEXT("wxWidgets Test!"), wxPoint(100, 100), wxSize(640, 480));
		frame->Show(true);

		return LaunchGameThread();
	}
	//----------------------------------------------------------------------
	//	終了処理
	//----------------------------------------------------------------------
	virtual s32 OnExit()
	{
		wxApp::OnExit();

		return ExitWaitGameThread();
	}
};

//	アプリケーションの起動クラス生成
wxAppConsole * wxCreateApp(void)
{
	return new kglLaunchApp;
}
//	wxWidgetsの初期化
wxAppInitializer wxTheAppInitializer((wxAppInitializerFunction) wxCreateApp);
kglLaunchApp& wxGetApp() { return *(kglLaunchApp*)wxApp::GetInstance(); }

#endif    // ~#if    KGL_USE_WXWIDGETS

//======================================================================
//	END OF FILE
//======================================================================
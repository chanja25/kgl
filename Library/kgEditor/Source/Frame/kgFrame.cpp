//----------------------------------------------------------------------
//!
//!	@file	kgEditor.cpp
//!	@brief	Kisaragi Game Editor
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgEditor.h"

#if	KGL_USE_WXWIDGETS
#include "Frame/kgFrame.h"


MyFrame::MyFrame(const wxString& title, const wxPoint& pos, const wxSize& size)
	: wxFrame(null, wxID_ANY, title, pos, size, wxDEFAULT_FRAME_STYLE | wxWANTS_CHARS)
{
	wxMenu *menu = new wxMenu;
	menu->Append(ID_About, _("&About..."));
	menu->Append(ID_Quit, _("&Exit"));

	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append(menu, _("&File"));
	SetMenuBar(menuBar);

	wxPanel *panel = new wxPanel(this, wxID_ANY);
	wxGauge* gauge = new wxGauge;
	gauge->Create(panel, wxID_ANY, 100, wxPoint(50, 50), wxSize(120, 32), wxGA_HORIZONTAL);
	gauge->SetValue(20);

	CreateStatusBar();
	SetStatusText(_("This is Status Text."));
}

bool MyFrame::Destroy(void)
{
	wxExitGame();
	return wxFrame::Destroy();
}

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    Close(true);
}

void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxMessageBox( _("This is Hello World Sample in Marupeke."),
        _("About Hello World"), 
        wxOK | wxICON_INFORMATION, this );
}

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(ID_Quit, MyFrame::OnQuit)
    EVT_MENU(ID_About, MyFrame::OnAbout)
END_EVENT_TABLE()

#endif    // ~#if    KGL_USE_WXWIDGETS

//======================================================================
//	END OF FILE
//======================================================================
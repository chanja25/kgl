//----------------------------------------------------------------------
//!
//!	@file	kgFramework.h
//!	@brief	Kisaragi Game Framework �w�b�_�[
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KG_FRAMEWORK_H__
#define	__KG_FRAMEWORK_H__

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#if	defined(_DEBUG)
#define KG_FRAMEWORK_DEBUG	1
#else	// ~#if defined(_DEBUG)
#define KG_FRAMEWORK_DEBUG	0
#endif	// ~#if defined(_DEBUG)

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../../kgl/include/kgl.h"
#include "../../../kgl/include/System/kgl.Window.h"
#include "../../../Library/kgGraphics/include/kgGraphics.h"
#include "../../../Library/kgSound/include/kgSound.h"

//----------------------------------------------------------------------
// Kisaragi Game Framework
//----------------------------------------------------------------------
namespace kgFramework
{
}

#endif	// __KG_FRAMEWORK_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	main.cpp
//!	@brief	メイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <windows.h>

typedef	int (*FEntryPoint)(void);

//----------------------------------------------------------------------
//!	エントリーポイント
//!	@return 実行結果
//----------------------------------------------------------------------
#if	_CONSOLE
int main(int, const char**)
#else	// ~#if _CONSOLE
int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
#endif	// ~#if _CONSOLE
{
	HMODULE hDll = LoadLibraryEx("Data/dll/Launcher.dll", NULL, 0);
	if( !hDll )	return -1;

	FEntryPoint EntryPoint = (FEntryPoint)GetProcAddress(hDll, "KGLGameEntryPoint");
	int iExitCode = -1;
	if( EntryPoint )
	{
		iExitCode = EntryPoint();
	}

	FreeLibrary(hDll);
	return iExitCode;
}


//======================================================================
//	END OF FILE
//======================================================================

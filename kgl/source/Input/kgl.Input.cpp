//----------------------------------------------------------------------
//!
//!	@file	kgl.Input.h
//!	@brief	入力関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Input/kgl.Input.h"

//	DirectInputのバージョン設定
#define	DIRECTINPUT_VERSION	0x0800
#include <dinput.h>


namespace kgl
{
	namespace Input
	{
		//!	キーボード情報
		struct KEY_DATA
		{
			s8 cDisk[256];		//!< キー情報
			s8 cOldDisk[256];	//!< 前回のキー情報
		};

		//!	マウス情報
		struct MOUSE_DATA
		{
			s8  cButton;	//!< ボタン情報
			s8  cOldButton;	//!< 前回のボタン情報
			s8	pad[2];
			s32 iX, iY, iZ;	//!< X軸、Y軸、ホイール情報
		};

		//!	ジョイパッド情報
		struct JOY_DATA
		{
			u32		uiButton;		//!< ボタン情報
			u32		uiOldButton;	//!< 前回のボタン情報
			Vector2	LeftStick;		//!< 左スティック情報
			Vector2	RightStick;		//!< 右スティック情報
		};

		const s32 PAD_MAX = 4;	//!< パッド最大数

		static HWND s_hWnd;	//!< ウィンドウハンドル

		static IDirectInput8*		s_pDInput = null;		//!< DirectInputオブジェクト
		static IDirectInputDevice8*	s_pKeyboard = null;		//!< キーボードデバイス
		static IDirectInputDevice8*	s_pMouse = null;		//!< マウスデバイス
		static IDirectInputDevice8*	s_pJoyStick[PAD_MAX];	//!< パッドデバイス
		static DIDEVICEINSTANCE		s_DIDInstance[PAD_MAX];	//!< パッドのインスタンス

		static KEY_DATA		s_KeyState;				//!< キー情報
		static MOUSE_DATA	s_MouseState;			//!< マウス情報
		static JOY_DATA		s_JoyState[PAD_MAX];	//!< パッド情報

		static f32 s_fAxisMargin = 0.25f;	//!< スティックの遊び
		static u32 s_uiPadNum = 0;			//!< パッド数
		static u32 s_uiUsePadNum = 0;		//!< 使用中のパッド数

		//----------------------------------------------------------------------
		//!	デバイス毎に呼び出されるコールバック関数
		//!	@param pDIDInstance	[in] DirectInputデバイスのインスタンス
		//!	@param pvRef [in] リファレンスデータ
		//!	@return	結果
		//----------------------------------------------------------------------
		s32 CALLBACK EnumJoyCallBack(const DIDEVICEINSTANCE* pDIDInstance, void* pRef)
		{
			DIDEVICEINSTANCE* pTemp = (DIDEVICEINSTANCE*)pRef;
			pTemp[s_uiPadNum] = *pDIDInstance;
			s_uiPadNum++;
			return DIENUM_CONTINUE;
		}

		//----------------------------------------------------------------------
		//!	ジョイスティックの軸を列挙するコールバック関数
		//!	@param pDDOI [in] DirectInputDeviceインスタンス
		//!	@param pvRef [in] リファレンスデータ
		//!	@return	結果
		//----------------------------------------------------------------------
		s32 CALLBACK EnumAxesCallBack(LPCDIDEVICEOBJECTINSTANCE pDDOI, void* pvRef)
		{
			HRESULT hr;
			DIPROPRANGE diprg;

			//デバイス入力データの設定
			ZeroMemory(&diprg , sizeof(diprg));
			diprg.diph.dwSize = sizeof(DIPROPRANGE);
			diprg.diph.dwHeaderSize = sizeof(diprg.diph);
			diprg.diph.dwObj = pDDOI->dwType;
			diprg.diph.dwHow = DIPH_BYID;
			diprg.lMin = -1000;
			diprg.lMax = 1000;
			hr = ((IDirectInputDevice8*)pvRef)->SetProperty(DIPROP_RANGE, &diprg.diph);

			if( FAILED(hr) ) return DIENUM_STOP;
			return DIENUM_CONTINUE;
		}

		//----------------------------------------------------------------------
		//	初期化
		//!	@param s_hWnd [in] ウィンドウハンドル
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 Initialize(HWND hWnd)
		{
			HRESULT hr;
			//	DirectInputの生成
			hr = DirectInput8Create(GetModuleHandle(null), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&s_pDInput, null);
			if( FAILED(hr) )
			{
				WarningTrace("DirectInputの生成に失敗しました");
				return false;
			}

			s_hWnd = hWnd;

			//----------------------------------------------------------------------
			//	キーボードデバイスの作成
			//----------------------------------------------------------------------
			hr = s_pDInput->CreateDevice(GUID_SysKeyboard, &s_pKeyboard, null);
			if( FAILED(hr) )
			{
				WarningTrace("キーボードデバイスの生成に失敗しました");
				return false;
			}
			hr = s_pKeyboard->SetDataFormat(&c_dfDIKeyboard);
			if( FAILED(hr) )
			{
				WarningTrace("キーボードデバイスの設定に失敗しました");
				return false;
			}
			hr = s_pKeyboard->SetCooperativeLevel(s_hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
			if( FAILED(hr) )
			{
				WarningTrace("キーボードデバイスの強調レベルの設定に失敗しました");
				return false;
			}
			s_pKeyboard->Acquire();

			//----------------------------------------------------------------------
			//	マウスデバイスの作成
			//----------------------------------------------------------------------
			hr = s_pDInput->CreateDevice(GUID_SysMouse, &s_pMouse, null);
			if( FAILED(hr) )
			{
				WarningTrace("マウスデバイスの生成に失敗しました");
				return false;
			}
			//	マウスデバイスのフォーマットを設定
			hr = s_pMouse->SetDataFormat(&c_dfDIMouse2);
			if( FAILED(hr) )
			{
				WarningTrace("マウスデバイスの設定に失敗しました");
				return false;
			}
			//	マウスデバイスの協調レベルの設定
			hr = s_pMouse->SetCooperativeLevel(s_hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
			if( FAILED(hr) )
			{
				WarningTrace("マウスデバイスの強調レベルの設定に失敗しました");
				return false;
			}
			//	マウスデバイスの軸モード設定
			DIPROPDWORD diprop;
			diprop.diph.dwSize		 = sizeof(diprop);
			diprop.diph.dwHeaderSize = sizeof(diprop.diph);
			diprop.diph.dwObj		 = 0;
			diprop.diph.dwHow		 = DIPH_DEVICE;
			diprop.dwData			 = DIPROPAXISMODE_REL;	// 相対モード
			hr = s_pMouse->SetProperty(DIPROP_AXISMODE, &diprop.diph);
			if( FAILED(hr) )
			{
				WarningTrace("マウスデバイスの軸モードの設定に失敗しました");
				return false;
			}
			s_pMouse->Acquire();

			//----------------------------------------------------------------------
			//	ジョイスティックデバイスの作成
			//----------------------------------------------------------------------
			s_pDInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoyCallBack, &s_DIDInstance, DIEDFL_ATTACHEDONLY);

			for( u32 i=0; i < s_uiPadNum; i++ )
			{
				TKGLPtr<IDirectInputDevice8> pDevice;
				//	デバイス生成
				s_pDInput->CreateDevice(s_DIDInstance[i].guidInstance, pDevice.GetReference(), null);
				if( FAILED(hr) )	return false;

				//	デバイスフォーマットの設定
				pDevice->SetDataFormat(&c_dfDIJoystick2);
				//	強調レベルの設定
				hr = pDevice->SetCooperativeLevel(s_hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
				if( FAILED(hr) )	return false;

				//	ジョイスティックの軸の列挙
				hr = pDevice->EnumObjects(EnumAxesCallBack, pDevice, DIDFT_AXIS);
				if( FAILED(hr) )	return false;

				pDevice->Acquire();
				pDevice->AddRef();
				s_pJoyStick[i] = pDevice;
			}
			s_uiUsePadNum = 0;

			kgZeroMemory(&s_KeyState, sizeof(s_KeyState));
			kgZeroMemory(&s_MouseState, sizeof(s_MouseState));
			kgZeroMemory(&s_JoyState, sizeof(s_JoyState));

			return true;
		}

		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void Finalize(void)
		{
			SafeRelease(s_pDInput);
			SafeRelease(s_pKeyboard);
			SafeRelease(s_pMouse);

			for( u32 i=0; i < s_uiPadNum; i++ )
			{
				SafeRelease(s_pJoyStick[i]);
			}
			s_uiPadNum = 0;
		}


		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void Update(void)
		{
			HRESULT hr;
			u32 i, j;

			//	ウィンドウにフォーカスが無い場合
			if( s_hWnd != GetForegroundWindow() )
			{
				kgZeroMemory(&s_KeyState, sizeof(s_KeyState));
				kgZeroMemory(&s_MouseState, sizeof(s_MouseState));
				kgZeroMemory(&s_JoyState, sizeof(s_JoyState));

				return;
			}

			//----------------------------------------------------------------------
			//	キーボードの状態取得
			//----------------------------------------------------------------------
			kgMemcpy(s_KeyState.cOldDisk, s_KeyState.cDisk, sizeof(s_KeyState.cDisk));
			for( i = 0; i < 100; i++ )//　無限ループの発生を避ける為に100カウントで抜ける
			{
				hr = s_pKeyboard->GetDeviceState(sizeof(s_KeyState.cDisk), &s_KeyState.cDisk);

				if( hr == DIERR_INPUTLOST )
				{
					s_pKeyboard->Acquire();
					continue;
				}
				break;
			}

			static DIMOUSESTATE2 s_DIMouse;
			//----------------------------------------------------------------------
			//	マウスの状態取得
			//----------------------------------------------------------------------
			for( i = 0; i < 100; i++ )//　無限ループの発生を避ける為に100カウントで抜ける
			{
				hr = s_pMouse->GetDeviceState(sizeof(s_DIMouse), &s_DIMouse);

				if( hr == DIERR_INPUTLOST )
				{
					s_pMouse->Acquire();
					continue;
				}
				break;
			}

			//	前回の状態を退避
			s_MouseState.cOldButton = s_MouseState.cButton;
			s_MouseState.cButton = 0;
			//	ボタンの状態をセット
			for( i = 0; i < 8; i++ )
				s_MouseState.cButton |= ((s_DIMouse.rgbButtons[i] & 0x80)? 1: 0) << i;
			s_MouseState.iX = s_DIMouse.lX;
			s_MouseState.iY = s_DIMouse.lY;
			s_MouseState.iZ = s_DIMouse.lZ;
			
			static DIJOYSTATE2 s_DIJoyStick;
			//----------------------------------------------------------------------
			//	パッド毎のボタンの情報を取得する
			//----------------------------------------------------------------------
			for( i = 0; i < s_uiUsePadNum; i++ )
			{
				for( j = 0; j < 100; j++ )//　無限ループの発生を避ける為に100カウントで抜ける
				{
					s_pJoyStick[i]->Poll();
					//	パッドの状態取得
					hr = s_pJoyStick[i]->GetDeviceState(sizeof(s_DIJoyStick), &s_DIJoyStick);
					if( hr == DIERR_INPUTLOST )
					{
						s_pJoyStick[i]->Acquire();
						i --;
						continue;
					}
				}

				JOY_DATA& JoyState = s_JoyState[i];
				//	前回の状態を退避
				JoyState.uiOldButton = s_JoyState[i].uiButton;
				JoyState.uiButton = 0;

				//	ボタンの状態をセットする
				for( j = 0; j < 28; j++ )
				{
					JoyState.uiButton |= ((s_DIJoyStick.rgbButtons[j] & 0x80)? 1: 0) << j;
				}

				const dword& dwPOV = s_DIJoyStick.rgdwPOV[0];
				//	十字キーの状態セット
				JoyState.uiButton |= (((dwPOV == 0    ) | (dwPOV == 4500 ) | (dwPOV == 31500))? 1: 0) << EJoyStick::UP;
				JoyState.uiButton |= (((dwPOV == 13500) | (dwPOV == 18000) | (dwPOV == 22500))? 1: 0) << EJoyStick::DOWN;
				JoyState.uiButton |= (((dwPOV == 22500) | (dwPOV == 27000) | (dwPOV == 31500))? 1: 0) << EJoyStick::LEFT;
				JoyState.uiButton |= (((dwPOV == 4500 ) | (dwPOV == 9000 ) | (dwPOV == 13500))? 1: 0) << EJoyStick::RIGHT;

				//	左スティックの状態取得
				JoyState.LeftStick.x = static_cast<f32>(s_DIJoyStick.lX) * 0.001f;
				JoyState.LeftStick.y = static_cast<f32>(s_DIJoyStick.lY) * 0.001f;
				if( (abs(JoyState.LeftStick.x) < s_fAxisMargin) && 
					(abs(JoyState.LeftStick.y) < s_fAxisMargin) )
					JoyState.LeftStick.x = JoyState.LeftStick.y = 0.0f;

				//	右スティックの状態取得
				JoyState.RightStick.x = static_cast<f32>(s_DIJoyStick.lZ) * 0.001f;
				JoyState.RightStick.y = static_cast<f32>(s_DIJoyStick.lRz) * 0.001f;
				if( (abs(JoyState.RightStick.x) < s_fAxisMargin) &&
					(abs(JoyState.RightStick.y) < s_fAxisMargin) )
					JoyState.RightStick.x = JoyState.RightStick.y = 0.0f;
			}
		}

		//======================================================================
		//!	キーボードクラス
		//======================================================================
		class CKeyboard
			: public IKeyboard
		{
		public:
			//----------------------------------------------------------------------
			//!	キーが押されているかチェック
			//----------------------------------------------------------------------
			b8 IsKeyPress(u32 uiKey)
			{
				const s8& cNow = s_KeyState.cDisk[uiKey];

				return (cNow & 0x80)? true: false;
			}

			//----------------------------------------------------------------------
			//!	キーを押したかチェック
			//----------------------------------------------------------------------
			b8 IsKeyPush(u32 uiKey)
			{
				const s8& cNow = s_KeyState.cDisk[uiKey];
				const s8& cOld = s_KeyState.cOldDisk[uiKey];

				return (((cNow ^ cOld) & cNow) & 0x80)? true: false;
			}

			//----------------------------------------------------------------------
			//!	キーが離されたかチェック
			//----------------------------------------------------------------------
			b8 IsKeyRelease(u32 uiKey)
			{
				const s8& cNow = s_KeyState.cDisk[uiKey];
				const s8& cOld = s_KeyState.cOldDisk[uiKey];

				return (((cNow ^ cOld) & cOld) & 0x80)? true: false;
			}
		};

		//======================================================================
		//!	マウスクラス
		//======================================================================
		class CMouse 
			: public IMouse
		{
		public:
			//----------------------------------------------------------------------
			//!	ボタンが押されているかチェック
			//----------------------------------------------------------------------
			b8 IsButtonPress(u32 uiButton)
			{
				const s8& cNow = s_MouseState.cButton;

				return (cNow & (1 << uiButton))? true: false;
			}

			//----------------------------------------------------------------------
			//!	ボタンを押したかチェック
			//----------------------------------------------------------------------
			b8 IsButtonPush(u32 uiButton)
			{
				const s8& cNow = s_MouseState.cButton;
				const s8& cOld = s_MouseState.cOldButton;

				return (((cNow ^ cOld) & cNow) & (1 << uiButton))? true: false;
			}

			//----------------------------------------------------------------------
			//!	ボタンが離されたかチェック
			//----------------------------------------------------------------------
			b8 IsButtonRelease(u32 uiButton)
			{
				const s8& cNow = s_MouseState.cButton;
				const s8& cOld = s_MouseState.cOldButton;

				return (((cNow ^ cOld) & cOld) & (1 << uiButton))? true: false;
			}

			//----------------------------------------------------------------------
			//!	Xの移動量取得
			//----------------------------------------------------------------------
			s32 GetX(void)
			{
				return s_MouseState.iX;
			}

			//----------------------------------------------------------------------
			//!	Yの移動量取得
			//----------------------------------------------------------------------
			s32 GetY(void)
			{
				return s_MouseState.iY;
			}

			//----------------------------------------------------------------------
			//!	ホイールの回転量取得
			//----------------------------------------------------------------------
			s32 GetWheel(void)
			{
				return s_MouseState.iZ;
			}

			//----------------------------------------------------------------------
			//!	ポインターの座標取得
			//----------------------------------------------------------------------
			void GetPosition(s32& iX, s32& iY)
			{
				static POINT Point;
				GetCursorPos(&Point);
				ScreenToClient(s_hWnd, &Point);
				iX = Point.x;
				iY = Point.y;
			}
		};

		//======================================================================
		//!	ジョイスティックインターフェイス
		//======================================================================
		class CJoyStick
			: public IJoyStick
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(void)
			{
				m_uiNo = s_uiUsePadNum++;

				return ( s_uiPadNum < m_uiNo )? true: false;
			}


			//----------------------------------------------------------------------
			//!	ボタンが押されているかチェック
			//----------------------------------------------------------------------
			b8 IsButtonPress(u32 uiButton)
			{
				const u32& uiNow = s_JoyState[m_uiNo].uiButton;

				return (uiNow & (1 << uiButton))? true: false;
			}

			//----------------------------------------------------------------------
			//!	ボタンを押したかチェック
			//----------------------------------------------------------------------
			b8 IsButtonPush(u32 uiButton)
			{
				const u32& uiNow = s_JoyState[m_uiNo].uiButton;
				const u32& uiOld = s_JoyState[m_uiNo].uiOldButton;

				return (((uiNow ^ uiOld) & uiNow) & (1 << uiButton))? true: false;
			}

			//----------------------------------------------------------------------
			//!	ボタンが離されたかチェック
			//----------------------------------------------------------------------
			b8 IsButtonRelease(u32 uiButton)
			{
				const u32& uiNow = s_JoyState[m_uiNo].uiButton;
				const u32& uiOld = s_JoyState[m_uiNo].uiOldButton;

				return (((uiNow ^ uiOld) & uiOld) & (1 << uiButton))? true: false;
			}

			//----------------------------------------------------------------------
			//!	左スティックの状態取得
			//----------------------------------------------------------------------
			Vector2 GetAxisLeft(void)
			{
				return s_JoyState[m_uiNo].LeftStick;
			}

			//----------------------------------------------------------------------
			//!	右スティックの状態取得
			//----------------------------------------------------------------------
			Vector2 GetAxisRight(void)
			{
				return s_JoyState[m_uiNo].RightStick;
			}

			//----------------------------------------------------------------------
			//!	ジョイスティック番号の設定
			//----------------------------------------------------------------------
			void SetNo(u32 uiNo)
			{
				m_uiNo = uiNo;
			}

			//----------------------------------------------------------------------
			//!	ジョイスティック番号の取得
			//----------------------------------------------------------------------
			u32 GetNo(void)
			{
				return m_uiNo;
			}

		private:
			u32 m_uiNo;	//!< ジョイスティックの番号
		};

		//----------------------------------------------------------------------
		// キーボードの生成
		//----------------------------------------------------------------------
		b8 Create(IKeyboard** ppKeyboard)
		{
			*ppKeyboard = new CKeyboard;
			(*ppKeyboard)->AddRef();

			return true;
		}

		//----------------------------------------------------------------------
		// マウスの生成
		//----------------------------------------------------------------------
		b8 Create(IMouse** ppMouse)
		{
			*ppMouse = new CMouse;
			(*ppMouse)->AddRef();

			return true;
		}

		//----------------------------------------------------------------------
		// ジョイスティックの生成
		//----------------------------------------------------------------------
		b8 Create(IJoyStick** ppJoyStick)
		{
			CJoyStick* pJoyStick = new CJoyStick;
			pJoyStick->AddRef();
			*ppJoyStick = pJoyStick;

			return pJoyStick->Initialize();
		}

		//----------------------------------------------------------------------
		//	スティックの遊び設定
		//----------------------------------------------------------------------
		void SetAxisMargin(f32 fMargin)
		{
			s_fAxisMargin = fMargin;
		}

		//----------------------------------------------------------------------
		//	ジョイスティックの検出数取得
		//----------------------------------------------------------------------
		u32 GetJoyStickNum(void)
		{
			return s_uiPadNum;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Memory.cpp
//!	@brief	メモリ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Memory/kgl.Memory.h"
#include "Memory/Allocator/kgl.TLSF.h"

#if	KGL_DEBUG_MEMORY
#include "Render/Utility/kgl.Image.h"
#endif

#define	USE_ALLOCATOR	KGL_USE_TLSF_ALLOCATOR

namespace kgl
{
	namespace Memory
	{
		//	確保時の基本のアライメント
		const size_t MemAlign	= 16;

		//	Platform Dependent
		//======================================================================
		//!	アロケーター
		//======================================================================
		class CAllocator
		{
		private:
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			CAllocator(size_t size, const c8* pName)
#if	KGL_DEBUG_MEMORY && USE_ALLOCATOR
				: m_pName(pName)
				, m_MaxSize(0)
#endif	// ~#if KGL_DEBUG_MEMORY && USE_ALLOCATOR
			{
				(void)size;
				(void)pName;
#if	USE_ALLOCATOR
				Initialize(size);
				InitializeCriticalSection(&m_CriticalSection);

				SystemTrace("Allocator[%s]:0x%x", pName, this);
#endif	// ~#if	USE_ALLOCATOR
			}

			//----------------------------------------------------------------------
			//!	デストラクタ
			//----------------------------------------------------------------------
			~CAllocator(void)
			{
#if	USE_ALLOCATOR
				DeleteCriticalSection(&m_CriticalSection);
#endif	// ~#if	USE_ALLOCATOR
				Finalize();
			}

		private:
#if	USE_ALLOCATOR
			//!	ロック
			FORCEINLINE void Lock(void)
			{
				EnterCriticalSection(&m_CriticalSection);
			}

			//!	アンロック
			FORCEINLINE void Unlock(void)
			{
				LeaveCriticalSection(&m_CriticalSection);
			}
#endif	// ~#if	USE_ALLOCATOR

		private:
			//----------------------------------------------------------------------
			//!	初期化
			//----------------------------------------------------------------------
			void Initialize(size_t size)
			{
				(void)size;
#if	USE_ALLOCATOR
				m_pPool = _aligned_malloc(size, 16);
				kgAssert(m_pPool, "メモリーが足りません(CAllocator::Initialize)");
				m_tlsf.Initialize(m_pPool, size);
#endif	// ~#if	USE_ALLOCATOR

#if	KGL_DEBUG_MEMORY
				if( kgStricmp(m_pName, "Debug") == 0 )
				{
				}
				else
				{
				}
				////	メモリ情報
				////	アプリケーション内のメモリ情報
				//struct ApplicationMemoryInfo
				//{
				//	size_t		MemoryPool;				//!< メモリプールサイズ
				//	size_t		UseMemory;				//!< 使用メモリ
				//	size_t		DebugMemoryPool;		//!< メモリプールサイズ(Debug)
				//	size_t		UseDebugMemory;			//!< 使用メモリ(Debug)
				//	size_t		TextureMemoryPool;		//!< テクスチャーのメモリプール
				//	size_t		UseTextureMemory;		//!< テクスチャーの使用メモリ
				//};
#endif	// ~#if KGL_DEBUG_MEMORY
			}

			//----------------------------------------------------------------------
			//!	破棄
			//----------------------------------------------------------------------
			void Finalize(void)
			{
#if	KGL_DEBUG_MEMORY
#if	USE_ALLOCATOR
				WarningkMemoryLeack();

				m_tlsf.Finalize();
				_aligned_free(m_pPool);
#endif	// ~#if	USE_ALLOCATOR
#endif	// ~#if KGL_DEBUG_MEMORY
			}

		public:
#if	USE_ALLOCATOR
			//----------------------------------------------------------------------
			//!	使用中のメモリサイズ取得
			//!	@return メモリサイズ
			//----------------------------------------------------------------------
			FORCEINLINE size_t GetSize(void) { return m_tlsf.GetAllocSize(); }
#else	// ~#if	USE_ALLOCATOR
			FORCEINLINE size_t GetSize(void) { return 0; }
#endif	// ~#if	USE_ALLOCATOR

#if	KGL_DEBUG_MEMORY
			//----------------------------------------------------------------------
			//!	メモリリーク情報の出力
			//----------------------------------------------------------------------
			void WarningkMemoryLeack()
			{
#if	USE_ALLOCATOR
				//	メモリ情報出力
				SystemTrace("//==================================================");
				SystemTrace("// メモリ情報(%s)", m_pName);
				SystemTrace("//==================================================");
				//	最大確保メモリ数出力
				f32 fMaxSize = (f32)m_MaxSize / 1024.0f;
				f32 fPoolSize = (f32)m_tlsf.GetMemorySize() / 1024.0f;
				SystemTrace("最大確保メモリ:%.2f/%.2f MB", fMaxSize / 1024.0f, fPoolSize / 1024.0f);
				//	リークしているメモリ表示
				if( m_tlsf.GetAllocCount() != 0 )
				{
					WarningTrace("//==================================================");
					WarningTrace("// メモリリーク情報(%s)", m_pName);
					WarningTrace("//==================================================");
					WarningTrace("確保数:%d", m_tlsf.GetAllocCount());
					WarningTrace("確保メモリ量:%d", m_tlsf.GetAllocSize());
				}

				auto header = m_tlsf.GetTLSFHeader();
				auto firts = tlsf::GetBoundaryBlock(header);
				auto block = tlsf::NextBlock(firts);
				//	未開放のメモリを表示
				while( block < header->end )
				{
					if( tlsf::CheckBoundaryBlock(block) )
					{
						auto p = tlsf::GetMemory(block);
						WarningTrace("-----リークメモリ情報-----");
						WarningTrace("Address:0x%p(0x%p)", p, (s64)p - (s64)header);
						WarningTrace("Size:%d", block->size);
					}
					block = tlsf::NextBlock(block);
				}
#endif	// ~#if	USE_ALLOCATOR
			}
#endif	// ~#if KGL_DEBUG_MEMORY

#if	KGL_DEBUG_MEMORY
			//----------------------------------------------------------------------
			//!	確保中のメモリ表示
			//!	@param bDetail [in] 詳細表示フラグ
			//----------------------------------------------------------------------
			void ShowMemory(b8 bDetail)
			{
				(void)bDetail;
#if	USE_ALLOCATOR
				//	確保メモリ表示
				SystemTrace("//==================================================");
				SystemTrace("// 確保メモリ表示");
				SystemTrace("//==================================================");
				SystemTrace("確保数:%d", m_tlsf.GetAllocCount());
				SystemTrace("確保メモリ量:%d/%d(Byte)", m_tlsf.GetAllocSize(), m_tlsf.GetMemorySize());

				//	詳細表示
				if( bDetail )
				{
					Lock();
					auto header = m_tlsf.GetTLSFHeader();
					auto firts = tlsf::GetBoundaryBlock(header);
					auto block = tlsf::NextBlock(firts);
					s32 iCount = 0;
					//	確保中のメモリを表示
					while( block < header->end )
					{
						if( tlsf::CheckBoundaryBlock(block) )
						{
							SystemTrace("-----Allocate Info   -----");
							SystemTrace("Number:%d", iCount);
							SystemTrace("Size:%d", block->size);
							SystemTrace("Address:0x%p", tlsf::GetMemory(block));
						}
						block = tlsf::NextBlock(block);
						iCount ++;
					}
					Unlock();
				}

				SystemTrace("//==================================================");
				SystemTrace("// 確保メモリ表示終了");
				SystemTrace("//==================================================\n");
#endif	// ~#if	USE_ALLOCATOR
			}
#endif	// ~#if KGL_DEBUG_MEMORY

		public:
			//----------------------------------------------------------------------
			//!	メモリ確保
			//!	@param size [in] 確保するメモリのサイズ
			//!	@return 確保したメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void* Allocate(size_t size)
			{
				return Allocate(size, MemAlign);
			}
			//----------------------------------------------------------------------
			//!	メモリ確保
			//!	@param size		[in] 確保するメモリのサイズ
			//!	@param align	[in] アライメント
			//!	@return 確保したメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void* Allocate(size_t size, size_t align)
			{
#if	USE_ALLOCATOR
				Lock();

				//	メモリ確保
				void* ptr = m_tlsf.Allocate(size, align);
				kgAssert(ptr != null, "メモリ確保に失敗しました(Allocate)");
#if	KGL_DEBUG_MEMORY
				if( m_MaxSize < m_tlsf.GetAllocSize() )
				{
					m_MaxSize = m_tlsf.GetAllocSize();
				}
#endif	// ~#if KGL_DEBUG_MEMORY

				Unlock();

				return ptr;
#else	// ~#if	USE_ALLOCATOR
				return _aligned_malloc(size, align);
#endif	// ~#if	USE_ALLOCATOR
			}
			//----------------------------------------------------------------------
			//!	メモリ再確保
			//!	@param size [in] 確保するメモリのサイズ
			//!	@return 確保したメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void* Reallocate(void* ptr, size_t size)
			{
				return Reallocate(ptr, size, MemAlign);
			}
			//----------------------------------------------------------------------
			//!	メモリ再確保
			//!	@param size		[in] 確保するメモリのサイズ
			//!	@param align	[in] アライメント
			//!	@return 確保したメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void* Reallocate(void* mem, size_t size, size_t align)
			{
#if	USE_ALLOCATOR
				Lock();

				//	メモリ確保
				void* ptr = m_tlsf.Reallocate(mem, size, align);
				kgAssert(ptr != null, "メモリ確保に失敗しました(Allocate)");
#if	KGL_DEBUG_MEMORY
				if( m_MaxSize < m_tlsf.GetAllocSize() )
				{
					m_MaxSize = m_tlsf.GetAllocSize();
				}
#endif	// ~#if KGL_DEBUG_MEMORY

				Unlock();

				return ptr;
#else	// ~#if	USE_ALLOCATOR
				return _aligned_realloc(mem, size, align);
#endif	// ~#if	USE_ALLOCATOR
			}

			//----------------------------------------------------------------------
			//!	メモリ解放
			//!	@param pMemory [in] 解放するメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void Free(void* mem)
			{
#if	USE_ALLOCATOR
				if (m_tlsf.IsManaged(mem))
				{
					Lock();

					m_tlsf.Free(mem);

					Unlock();
				}
#if KGL_USE_WXWIDGETS
				else
				{
					free(mem);
				}
#endif // ~#if KGL_USE_WXWIDGETS

#else	// ~#if	USE_ALLOCATOR
				return _aligned_free(mem);
#endif	// ~#if	USE_ALLOCATOR
			}

#if KGL_USE_TLSF_ALLOCATOR && USE_TLSF_MEMORY_CHECK
			//!	アロケーター取得
			//!	@return アロケーター
			FORCEINLINE void EnableMemoryCheck(bool enable)
			{
				m_tlsf.EnableMemoryCheck(enable);
			}
#endif // ~#if KGL_USE_TLSF_ALLOCATOR && USE_TLSF_MEMORY_CHECK

		public:
			//!	アロケーター取得
			//!	@return アロケーター
			FORCEINLINE static CAllocator& GetAllocator(void)
			{
				size_t size = 0;
#if KGL_USE_TLSF_ALLOCATOR
				size = MemoryPoolSize();
#endif // ~#if KGL_USE_TLSF_ALLOCATOR
				static CAllocator Allocator(size, "System");

				return Allocator;
			}
#if	KGL_DEBUG_MEMORY
			//!	アロケーター取得
			//!	@return アロケーター
			FORCEINLINE static CAllocator& GetDebugAllocator(void)
			{
				size_t size = 0;
#if KGL_USE_TLSF_ALLOCATOR
				size = DebugMemoryPoolSize();
#endif // ~#if KGL_USE_TLSF_ALLOCATOR
				static CAllocator DebugAllocator(size, "Debug");

				return DebugAllocator;
			}
#endif	// ~#if KGL_DEBUG_MEMORY
			//!	アロケーター取得
			//!	@return アロケーター
			FORCEINLINE tlsf::TLSFHeader* GetTLSFAllocator(void) { return m_tlsf.GetTLSFHeader(); }

		private:
#if	KGL_DEBUG_MEMORY
			const c8*			m_pName;
			size_t				m_MaxSize;	//!< 最大確保メモリサイズ
#endif	// ~#if KGL_DEBUG_MEMORY
#if	USE_ALLOCATOR
			CRITICAL_SECTION	m_CriticalSection;	//!< クリティカルセクション管理用
#endif	// ~#if	USE_ALLOCATOR
			void*					m_pPool;			//!< メモリプール
			tlsf::CTlsfAllocator	m_tlsf;				//!< アロケータ
		};

		//----------------------------------------------------------------------
		//	メモリ確保
		//----------------------------------------------------------------------
		void* Allocate(size_t size)
		{
			if( size == 0 )	return null;
			return CAllocator::GetAllocator().Allocate(size);
		}
		//----------------------------------------------------------------------
		//	メモリ確保
		//----------------------------------------------------------------------
		void* Allocate(size_t size, size_t align)
		{
			if( size == 0 )	return null;
			return CAllocator::GetAllocator().Allocate(size, align);
		}
		//----------------------------------------------------------------------
		//	メモリ確保
		//----------------------------------------------------------------------
		void* Reallocate(void* memory, size_t size)
		{
			if( size == 0 )	return null;
			return CAllocator::GetAllocator().Reallocate(memory, size);
		}
		//----------------------------------------------------------------------
		//	メモリ確保
		//----------------------------------------------------------------------
		void* Reallocate(void* memory, size_t size, size_t align)
		{
			if( size == 0 )	return null;
			return CAllocator::GetAllocator().Reallocate(memory, size, align);
		}
		//----------------------------------------------------------------------
		//	解放
		//----------------------------------------------------------------------
		void Free(void* ptr)
		{
			if( !ptr )	return;
			CAllocator::GetAllocator().Free(ptr);
		}

#if	KGL_DEBUG_MEMORY
		//----------------------------------------------------------------------
		//	現在確保されているメモリ表示
		//----------------------------------------------------------------------
		void ShowMemory(void)
		{
			CAllocator::GetAllocator().ShowMemory(true);
		}
#endif	// ~#if KGL_DEBUG_MEMORY

		//----------------------------------------------------------------------
		//	現在確保しているサイズ取得
		//----------------------------------------------------------------------
		size_t GetAllocateSize(void)
		{
			return CAllocator::GetAllocator().GetSize();
		}

#if KGL_USE_TLSF_ALLOCATOR
		void EnableMemoryCheck(bool enable)
		{
			(void)enable;
#if USE_TLSF_MEMORY_CHECK
			CAllocator::GetAllocator().EnableMemoryCheck(enable);
#endif // ~#if USE_TLSF_MEMORY_CHECK
		}
#endif // ~#if KGL_USE_TLSF_ALLOCATOR

#if	KGL_DEBUG_MEMORY
		namespace Debug
		{
			//----------------------------------------------------------------------
			//	メモリ確保
			//----------------------------------------------------------------------
			void* Allocate(size_t size)
			{
				return CAllocator::GetDebugAllocator().Allocate(size);
			}
			//----------------------------------------------------------------------
			//	メモリ確保
			//----------------------------------------------------------------------
			void* Allocate(size_t size, size_t align)
			{
				if( size == 0 )	return null;
				return CAllocator::GetDebugAllocator().Allocate(size, align);
			}
			//----------------------------------------------------------------------
			//	解放
			//----------------------------------------------------------------------
			void Free(void* ptr)
			{
				if( !ptr )	return;
				CAllocator::GetDebugAllocator().Free(ptr);
			}

			//----------------------------------------------------------------------
			//	現在確保されているメモリ表示
			//----------------------------------------------------------------------
			void ShowMemory(void)
			{
				CAllocator::GetDebugAllocator().ShowMemory(true);
			}

			//----------------------------------------------------------------------
			//	現在確保しているサイズ取得
			//----------------------------------------------------------------------
			size_t GetAllocateSize(void)
			{
				return CAllocator::GetDebugAllocator().GetSize();
			}

#if KGL_USE_TLSF_ALLOCATOR
			void EnableMemoryCheck(bool enable)
			{
				(void)enable;
#if USE_TLSF_MEMORY_CHECK
				CAllocator::GetDebugAllocator().EnableMemoryCheck(enable);
#endif // ~#if USE_TLSF_MEMORY_CHECK
			}
#endif // ~#if KGL_USE_TLSF_ALLOCATOR
		}
#endif	// KGL_DEBUG_MEMORY
	}
}

//======================================================================
//	new / delete
//======================================================================
//	new
void* operator new(size_t size)
{
	//SystemTrace("new %d", size);
	return kgl::Memory::Allocate((u32)size);
}

//	delete
void operator delete(void* ptr)
{
	//SystemTrace("delete");
	kgl::Memory::Free(ptr);
}

//	new
void* operator new[](size_t size)
{
	//SystemTrace("new[] %d", size);
	return kgl::Memory::Allocate((u32)size);
}

//	delete []
void operator delete[](void* ptr)
{
	//SystemTrace("delete[]");
	kgl::Memory::Free(ptr);
}

//	new
void* operator new(std::size_t size, const std::nothrow_t&) throw ()
{
	//SystemTrace("new %d", size);
	return kgl::Memory::Allocate((u32)size);
}
void* operator new(std::size_t size, std::size_t)
{
	//SystemTrace("new %d", size);
	return kgl::Memory::Allocate((u32)size);
}
void* operator new(std::size_t size, std::size_t, const std::nothrow_t&)
{
	//SystemTrace("new %d", size);
	return kgl::Memory::Allocate((u32)size);
}
void* operator new[](std::size_t size, std::size_t)
{
	//SystemTrace("new %d", size);
	return kgl::Memory::Allocate((u32)size);
}
void* operator new[](std::size_t size, std::size_t, const std::nothrow_t&)
{
	//SystemTrace("new %d", size);
	return kgl::Memory::Allocate((u32)size);
}

//=======================================================================
//	END OF FILE
//=======================================================================
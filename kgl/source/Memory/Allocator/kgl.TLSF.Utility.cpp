//----------------------------------------------------------------------
//!
//!	@file	kgl.TLSF.Utility.h
//!	@brief	メモリプール
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Memory/Allocator/kgl.TLSF.h"

namespace kgl
{
	namespace tlsf
	{
#if USE_TLSF_MEMORY_CHECK
		void MemoryCheck(TLSFHeader* header, BoundaryBlockHeader* ptr)
		{
#if USE_TLSF_MEMORY_CHECK
			if( !header->memcheck )
			{
				return;
			}
#endif // ~#if USE_TLSF_MEMORY_CHECK
			const c8* error = "Memory破壊が発生している可能性が高いです";
			(void)error;

			bool assert = ptr == null;
			auto block = GetBoundaryBlock(header);
			auto oldBlock = block;
			while( true )
			{
				assert |= block == ptr;

				if( block->use )
				{
					kgAssert(block->id1 == FOURCC('t', 'l', 's', 'f'), error);
					kgAssert(block->id2 == FOURCC('t', 'l', 's', 'f'), error);
					auto mem = GetMemory(block);
					kgAssert(GetBoundaryBlock(mem) == block, "error");
				}
				else
				{
					kgAssert(block->id1 == FOURCC('n', 'o', 'n', 'e'), error);
					kgAssert(block->id2 == FOURCC('n', 'o', 'n', 'e'), error);
				}
				oldBlock = block;
				block = NextBlock(block);
				if( block < header->end )
				{
					kgAssert(oldBlock == block->prev, error);
					continue;
				}
				kgAssert(block == header->end, error);
				break;
			}
		}
#endif // USE_TLSF_MEMORY_CHECK

		//----------------------------------------------------------------------
		//	メモリプールの作成
		//----------------------------------------------------------------------
		TLSFHeader*	CreateTLSF(void* memory, size_t size)
		{
			if( memory == null )	return null;

			const size_t first = Align((size_t)memory, DefaultAlign);
			const size_t pad = first - (size_t)memory;
			const size_t mem_size = size - pad - BlockHeaderSize;

			BoundaryBlockHeader* block = (BoundaryBlockHeader*)first;
			//	全体の管理領域生成
			block = CreateBoundaryBlock(block, mem_size, BlockHeaderSize);

			const size_t offset = CalcBoundaryBlockOffset(block, DefaultAlign);
			//	データの手前にブロックへのオフセットを入れておく
			block->offset	= offset;

			auto header = (TLSFHeader*)GetMemory(block);
			header->size		= size - pad;
			header->alloc		= header->size;
			header->end			= (u8*)memory + size;
#if USE_TLSF_MEMORY_CHECK
			header->memcheck	= 0;
#endif // ~#if USE_TLSF_MEMORY_CHECK

			for( u8 i=0; i < FREE_LIST_MAX; i++ )
			{
				header->free_block_count[i] = 0;
				for( u8 j=0; j < FREE_SECOND_LIST_MAX; j ++ )
				{
					header->free_block[i][j] = null;
				}
			}
			const size_t headersize = Align(sizeof(TLSFHeader), DefaultAlign);
			LinkFreeBoundaryBlock(header, block);
			//	ヘッダー部分を分割
			SplitBoundaryBlock(header, block, headersize, DefaultAlign);
			UseBoundaryBlock(block);

#if USE_TLSF_MEMORY_CHECK
			MemoryCheck(header, block);
#endif // USE_TLSF_MEMORY_CHECK
			return header;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.zlib.cpp
//!	@brief	圧縮
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Compress/kgl.zlib.h"
#include <zlib.h>


namespace kgl
{
	namespace zlib
	{
		//----------------------------------------------------------------------
		//!	結果取得
		//!	@param iResult [in] 終了コード
		//!	@return 結果
		//----------------------------------------------------------------------
		INLINE s32 GetResult(s32 iResult)
		{
			switch( iResult )
			{
			case Z_OK:			return EResult::OK;
			case Z_MEM_ERROR:	return EResult::MemError;
			case Z_BUF_ERROR:	return EResult::BufError;
			}

			return EResult::Unknown;
		}

		//----------------------------------------------------------------------
		//	圧縮
		//----------------------------------------------------------------------
		s32 Compress(void* pBuffer, u32* pCompressSize, const void* pSource, u32 uiSourceSize, s32 iLevel)
		{
			iLevel = Clamp(iLevel, 1, 9);

			s32 iResult;
			//	ライブラリとやりとりするための構造体
			z_stream z;

			//	すべてのメモリ管理をライブラリに任せる
			z.zalloc	= Z_NULL;
			z.zfree		= Z_NULL;
			z.opaque	= Z_NULL;

			//	初期化
			iResult = deflateInit(&z, iLevel);
			if( iResult != Z_OK )
			{
				ErrorTrace("deflateInit: %s", (z.msg) ? z.msg : "???");
				return GetResult(iResult);
			}

			//	入力バッファ設定
			z.next_in	= (Bytef*)pSource;	//	入力ポインタを入力バッファの先頭に
			z.avail_in	= uiSourceSize;		//	データを読み込む

			//	出力バッファ設定
			z.next_out	= (Bytef*)pBuffer;	//	出力ポインタ
			z.avail_out	= *pCompressSize;	//	出力バッファのサイズ

			//	圧縮する
			iResult = deflate(&z, Z_FINISH);
			if( iResult != Z_STREAM_END )
			{
				ErrorTrace("deflate: %s", (z.msg) ? z.msg : "???");
				return GetResult(iResult);
			}
			*pCompressSize = z.total_out;

			//	後始末
			iResult = deflateEnd(&z);
			if( iResult != Z_OK )
			{
				ErrorTrace("deflateEnd: %s", (z.msg) ? z.msg : "???");
				return GetResult(iResult);
			}

			return EResult::OK;
		}

		//----------------------------------------------------------------------
		//	解凍
		//----------------------------------------------------------------------
		s32 Decompress(void* pBuffer, u32* pSize, const void* pSource, u32 uiSourceSize)
		{
			s32 iResult;
			//	ライブラリとやりとりするための構造体
			z_stream z;

			//	すべてのメモリ管理をライブラリに任せる
			z.zalloc	= Z_NULL;
			z.zfree		= Z_NULL;
			z.opaque	= Z_NULL;

			//	初期化
			iResult = inflateInit(&z);
			if( iResult != Z_OK )
			{
				ErrorTrace("deflateInit: %s", (z.msg) ? z.msg : "???");
				return GetResult(iResult);
			}

			//	入力バッファ設定
			z.next_in	= (Bytef*)pSource;	//	入力ポインタを入力バッファの先頭に
			z.avail_in	= uiSourceSize;		//	データを読み込む

			//	出力バッファ設定
			z.next_out	= (Bytef*)pBuffer;	//	出力ポインタ
			z.avail_out	= *pSize;			//	出力バッファサイズ

			//	展開
			iResult = inflate(&z, Z_FINISH);
			if( iResult != Z_STREAM_END )
			{
				ErrorTrace("deflate: %s", (z.msg) ? z.msg : "???");
				return GetResult(iResult);
			}
			*pSize = z.total_out;

			//	後始末
			iResult = inflateEnd(&z);
			if( iResult != Z_OK )
			{
				ErrorTrace("deflateEnd: %s", (z.msg) ? z.msg : "???");
				return GetResult(iResult);
			}

			return EResult::OK;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
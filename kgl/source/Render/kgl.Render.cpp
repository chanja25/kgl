//----------------------------------------------------------------------
//!
//!	@file	kgl.Render.cpp
//!	@brief	描画システム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "System/kgl.Window.h"
#include "Render/kgl.Render.h"
#include "Manager/kgl.GameManager.h"

namespace kgl
{
	namespace Render
	{
		//======================================================================
		//	描画システム
		//======================================================================
		class CRenderingSystem
			: public IRenderingSystem
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(IGraphicDriver* pDriver)
			{
				if( pDriver == null )
				{
					return false;
				}
				//	描画制御の生成
				TKGLPtr<IDrawer> pDrawer;
				if( !Create(pDrawer.GetReference(), pDriver) )
				{
					return false;
				}
				m_pDriver	= pDriver;
				m_pCommand	= pDriver->CommandCreator();
				m_pDrawer	= pDrawer;
#if	KGL_DEBUG_FONT
				Font::FontInfo Info;
				kgStrcpy(Info.cFont, "ＭＳ ゴシック");
				Info.iSize = 20;

				Font::Create(m_pDebugFont.GetReference(), Info);
#endif	// ~#if	KGL_DEBUG_FONT
				return true;
			}
		};

		//----------------------------------------------------------------------
		//	描画システムの生成
		//----------------------------------------------------------------------
		b8 Create(IRenderingSystem** ppSystem, IGraphicDriver* pDriver)
		{
			TKGLPtr<CRenderingSystem> pSystem = new CRenderingSystem;
			if( !pSystem->Initialize(pDriver) )
			{
				return false;
			}
			*ppSystem = pSystem;
			(*ppSystem)->AddRef();

			return true;
		}
	}
}
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Drawer.cpp
//!	@brief	描画管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Render/kgl.Drawer.h"
#include "Manager/kgl.GameManager.h"
#include "Math/kgl.Math.h"

namespace kgl
{
	namespace Render
	{
		//======================================================================
		//!	描画管理
		//======================================================================
		class CDrawer
			: public IDrawer
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(IGraphicDriver* pDriver)
			{
				const c8* pPath;
				//	頂点シェーダーのロード
				pPath = "Data/Shaders/simple2d.pvs";
				KGLResourceManager()->LoadBlock(pPath, Manager::EResource::VertexShader);
				m_pVertexShader = KGLResourceManager()->GetVertexShader(pPath);
				if (!m_pVertexShader.IsValid())
				{
					ErrorTrace("シェーダーの初期化に失敗しました(%s)", pPath);
					return false;
				}
				//	ピクセルシェーダーのロード
				pPath = "Data/Shaders/simple2d.pps";
				KGLResourceManager()->LoadBlock(pPath, Manager::EResource::PixelShader);
				m_pPixcelShader = KGLResourceManager()->GetPixelShader(pPath);
				if (!m_pPixcelShader.IsValid())
				{
					ErrorTrace("シェーダーの初期化に失敗しました(%s)", pPath);
					return false;
				}
				//	ピクセルシェーダー(フォント用)のロード
				pPath = "Data/Shaders/font.pps";
				KGLResourceManager()->LoadBlock(pPath, Manager::EResource::PixelShader);
				m_pFontShader = KGLResourceManager()->GetPixelShader(pPath);
				if (!m_pFontShader.IsValid())
				{
					ErrorTrace("シェーダーの初期化に失敗しました(%s)", pPath);
					return false;
				}

				m_pDriver		= pDriver;
				m_pCommand		= pDriver->CommandCreator();

				//	頂点フォーマットの作成
				{
					DeclarationInfo Info[] =
					{
						{ 0, STRUCT_OFFSET(SimpleVertex, Position),	EDeclType::Float4, EDeclUsage::Position, 0, 0 },
						{ 0, STRUCT_OFFSET(SimpleVertex, Color),	EDeclType::Float4, EDeclUsage::Color,	 0, 0 },
						{ 0, STRUCT_OFFSET(SimpleVertex, UV),		EDeclType::Float2, EDeclUsage::UV,		 0, 0 },
						KGG_DECL_END(),
					};
					pDriver->PrimitiveManager()->CreateDeclaration(m_pDecl.GetReference(), Info);
				}

				//	白テクスチャー作成(8x8)
				{
					TextureInfo TexInfo;
					TexInfo.iFormat		= ETextureFormat::A8R8G8B8;
					TexInfo.iType		= ETextureType::Normal;
					TexInfo.iUsage		= 0;
					TexInfo.iMipLevels	= 1;
					TexInfo.uiWidth		= 8;
					TexInfo.uiHeight	= 8;

					u32 uiData[8*8];
					kgMemset(uiData, 0xffffffff, sizeof(uiData));

					TKGLPtr<ITextureBase> pResource;
					pDriver->TextureManager()->CreateTexture(pResource.GetReference(), TexInfo, uiData);

					Render::Create(m_pWhiteTexture.GetReference(), pResource);
				}

				return true;
			}

		public:
			//----------------------------------------------------------------------
			//	描画モードの設定
			//----------------------------------------------------------------------
			void SetRenderState(ITexture* pTexture, s32 iDrawMode, IPixelShader* pShader = null)
			{
				//	シェーダー設定
				{
					Command::VertexShaderInfo VertexInfo;
					VertexInfo.pShader = m_pVertexShader->GetResource();
					m_pCommand->SetVertexShader(VertexInfo);

					if( pShader == null )
					{
						switch( iDrawMode )
						{
						case EDrawMode::Font:
						case EDrawMode::FontAdd:
							pShader = m_pFontShader;
							break;
						default:
							pShader = m_pPixcelShader;
							break;
						}
					}
					Command::PixelShaderInfo PixelShaderInfo;
					PixelShaderInfo.pShader = pShader->GetResource();
					m_pCommand->SetPixelShader(PixelShaderInfo);
				}
				//	テクスチャー設定
				{
					//	テクスチャーが無い場合は白テクスチャーを使用
					if( pTexture == null )
					{
						pTexture = m_pWhiteTexture;
					}
					Command::TextureInfo TextureInfo;
					TextureInfo.iStage		= 0;
					TextureInfo.pTexture	= pTexture->GetResource();
					m_pCommand->SetTexture(TextureInfo);

					const FilterInfo& Info = pTexture->GetFilterInfo();

					Command::SamplerStateInfo SamplerStateInfo;
					SamplerStateInfo.iStage		= 0;
					SamplerStateInfo.iFilterMin	= Info.iFilterMin;
					SamplerStateInfo.iFilterMag	= Info.iFilterMag;
					SamplerStateInfo.iFilterMip	= Info.iFilterMip;
					SamplerStateInfo.iWrapU		= Info.iWrapU;
					SamplerStateInfo.iWrapV		= Info.iWrapV;
					m_pCommand->SetSamplerState(SamplerStateInfo);
				}
				//	カリング設定
				{
					Command::CullModeInfo Info;

					Info.iMode = Command::ECullMode::CW;
					m_pCommand->SetCullMode(Info);
				}
				//	デプステスト設定
				{
					Command::DepthTestInfo Info;

					Info.iTest	= Command::ETest::Disable;
					Info.bWrite	= false;
					m_pCommand->SetDepthTest(Info);
				}
				//	ステンシルテスト設定
				{
					Command::StencilTestInfo Info;
					kgZeroMemory(&Info, sizeof(Info));
					Info.iTest	= Command::ETest::Disable;
					m_pCommand->SetStencilTest(Info);
				}
				//	描画モード設定
				{
					Command::AlphaTestInfo AlphaTestInfo;
					Command::BlendInfo BlendInfo;

					AlphaTestInfo.iTest	= Command::ETest::Greater;
					AlphaTestInfo.ucRef	= 0;
					switch( iDrawMode )
					{
					case EDrawMode::Clear:
						BlendInfo.iColorOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcColor	= Command::EBlendParam::One;
						BlendInfo.iDstColor	= Command::EBlendParam::Zero;
						BlendInfo.iAlphaOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcAlpha	= Command::EBlendParam::Zero;
						BlendInfo.iDstAlpha	= Command::EBlendParam::One;

						AlphaTestInfo.iTest	= Command::ETest::Disable;
						break;
					case EDrawMode::Add:
					case EDrawMode::FontAdd:
						BlendInfo.iColorOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcColor	= Command::EBlendParam::SrcAlpha;
						BlendInfo.iDstColor	= Command::EBlendParam::One;
						BlendInfo.iAlphaOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcAlpha	= Command::EBlendParam::Zero;
						BlendInfo.iDstAlpha	= Command::EBlendParam::One;
						break;
					case EDrawMode::Sub:
						BlendInfo.iColorOp	= Command::EBlendOp::Subtrace;
						BlendInfo.iSrcColor	= Command::EBlendParam::SrcAlpha;
						BlendInfo.iDstColor	= Command::EBlendParam::One;
						BlendInfo.iAlphaOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcAlpha	= Command::EBlendParam::Zero;
						BlendInfo.iDstAlpha	= Command::EBlendParam::One;
						break;
					case EDrawMode::Alpha:
					case EDrawMode::Font:
						BlendInfo.iColorOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcColor	= Command::EBlendParam::SrcAlpha;
						BlendInfo.iDstColor	= Command::EBlendParam::SrcAlpha1M;
						BlendInfo.iAlphaOp	= Command::EBlendOp::Add;
						BlendInfo.iSrcAlpha	= Command::EBlendParam::Zero;
						BlendInfo.iDstAlpha	= Command::EBlendParam::One;
						break;
					}
					m_pCommand->SetAlphaTest(AlphaTestInfo);
					m_pCommand->SetBlend(BlendInfo);
				}
			}

		public:
			//----------------------------------------------------------------------
			//	レクトの頂点を取得
			//----------------------------------------------------------------------
			void SetupLineVertex(SimpleVertex* pVertex, const Vector2& vStart, const Vector2& vEnd, const Vector4& vRectUV, const FColor& DrawColor)
			{
				f32 fInvResolutionX = Config::InvResolution.x * 2.0f;
				f32 fInvResolutionY = Config::InvResolution.y * 2.0f;

				Vector4 vRect;
				vRect.x = (vStart.x * fInvResolutionX) - 1.0f;
				vRect.y = 1.0f - (vStart.y * fInvResolutionY);
				vRect.z = vEnd.x * fInvResolutionX;
				vRect.w =-vEnd.y * fInvResolutionY;

				pVertex[0].Position.x = vRect.x;
				pVertex[0].Position.y = vRect.y;
				pVertex[0].Position.z = 0.0f;
				pVertex[0].Position.w = 1.0f;
				pVertex[0].Color = DrawColor;
				pVertex[0].UV.x = vRectUV.x;
				pVertex[0].UV.y = vRectUV.y;

				pVertex[1].Position.x = vRect.x + vRect.z;
				pVertex[1].Position.y = vRect.y + vRect.w;
				pVertex[1].Position.z = 0.0f;
				pVertex[1].Position.w = 1.0f;
				pVertex[1].Color = DrawColor;
				pVertex[1].UV.x = vRectUV.z;
				pVertex[1].UV.y = vRectUV.w;
			}
			//----------------------------------------------------------------------
			//	レクトの頂点を取得
			//----------------------------------------------------------------------
			void SetupRectVertex(SimpleVertex* pVertex, const Vector2& vPos, const Vector2& vSize, const Vector4& vRectUV, const FColor& DrawColor)
			{
				f32 fInvResolutionX = Config::InvResolution.x * 2.0f;
				f32 fInvResolutionY = Config::InvResolution.y * 2.0f;

				Vector4 vRect;
				vRect.x = (vPos.x * fInvResolutionX) - 1.0f;
				vRect.y = 1.0f - (vPos.y * fInvResolutionY);
				vRect.z = vSize.x * fInvResolutionX;
				vRect.w =-vSize.y * fInvResolutionY;

				pVertex[0].Position.x = vRect.x;
				pVertex[0].Position.y = vRect.y;
				pVertex[0].Position.z = 0.0f;
				pVertex[0].Position.w = 1.0f;
				pVertex[0].Color = DrawColor;
				pVertex[0].UV.x = vRectUV.x;
				pVertex[0].UV.y = vRectUV.y;

				pVertex[1].Position.x = vRect.x;
				pVertex[1].Position.y = vRect.y + vRect.w;
				pVertex[1].Position.z = 0.0f;
				pVertex[1].Position.w = 1.0f;
				pVertex[1].Color = DrawColor;
				pVertex[1].UV.x = vRectUV.x;
				pVertex[1].UV.y = vRectUV.w;

				pVertex[2].Position.x = vRect.x + vRect.z;
				pVertex[2].Position.y = vRect.y;
				pVertex[2].Position.z = 0.0f;
				pVertex[2].Position.w = 1.0f;
				pVertex[2].Color = DrawColor;
				pVertex[2].UV.x = vRectUV.z;
				pVertex[2].UV.y = vRectUV.y;

				pVertex[3].Position.x = vRect.x + vRect.z;
				pVertex[3].Position.y = vRect.y + vRect.w;
				pVertex[3].Position.z = 0.0f;
				pVertex[3].Position.w = 1.0f;
				pVertex[3].Color = DrawColor;
				pVertex[3].UV.x = vRectUV.z;
				pVertex[3].UV.y = vRectUV.w;
			}
			//----------------------------------------------------------------------
			//	レクトの頂点を取得
			//----------------------------------------------------------------------
			void SetupRotatedRectVertex(SimpleVertex* pVertex, const Vector2& vPos, const Vector2& vSize, const Vector4& vRectUV, f32 fAngle, const Vector2 vOrigin, const FColor& DrawColor)
			{
				const f32 fInvResolutionX = Config::InvResolution.x * 2.0f;
				const f32 fInvResolutionY = Config::InvResolution.y * 2.0f;

				pVertex[0].Position.x = vPos.x;
				pVertex[0].Position.y = vPos.y;
				pVertex[0].Position.z = 0.0f;
				pVertex[0].Position.w = 1.0f;
				pVertex[0].Color = DrawColor;
				pVertex[0].UV.x = vRectUV.x;
				pVertex[0].UV.y = vRectUV.y;

				pVertex[1].Position.x = vPos.x;
				pVertex[1].Position.y = vPos.y + vSize.y;
				pVertex[1].Position.z = 0.0f;
				pVertex[1].Position.w = 1.0f;
				pVertex[1].Color = DrawColor;
				pVertex[1].UV.x = vRectUV.x;
				pVertex[1].UV.y = vRectUV.w;

				pVertex[2].Position.x = vPos.x + vSize.x;
				pVertex[2].Position.y = vPos.y;
				pVertex[2].Position.z = 0.0f;
				pVertex[2].Position.w = 1.0f;
				pVertex[2].Color = DrawColor;
				pVertex[2].UV.x = vRectUV.z;
				pVertex[2].UV.y = vRectUV.y;

				pVertex[3].Position.x = vPos.x + vSize.x;
				pVertex[3].Position.y = vPos.y + vSize.y;
				pVertex[3].Position.z = 0.0f;
				pVertex[3].Position.w = 1.0f;
				pVertex[3].Color = DrawColor;
				pVertex[3].UV.x = vRectUV.z;
				pVertex[3].UV.y = vRectUV.w;

				Matrix3x3 mRot;
				Math::MatrixRotation(mRot, fAngle);

				Vector2 vNewPos;
				for( s32 i=0; i < 4; i++ )
				{
					Vector4& vVertex = pVertex[i].Position;

					vNewPos.x = vVertex.x - vPos.x - vOrigin.x;
					vNewPos.y = vVertex.y - vPos.y - vOrigin.y;
					Math::Vec2Transform(vNewPos, vNewPos, mRot);
					vVertex.x = (vNewPos.x + vPos.x) * fInvResolutionX - 1.0f;
					vVertex.y = 1.0f - (vNewPos.y + vPos.y) * fInvResolutionY;
				}
			}
		public:
			//----------------------------------------------------------------------
			//	頂点バッファによる描画(Strip)
			//----------------------------------------------------------------------
			FORCEINLINE void DrawPrimitiveUP(const SimpleVertex* pVertex, u32 uiCount, s32 iPrimitive = Command::EPrimitive::TriangleStrip)
			{
				//	頂点フォーマットの設定
				{
					Command::DeclarationInfo DeclInfo;
					DeclInfo.pDeclaration = m_pDecl;
					m_pCommand->SetDeclaration(DeclInfo);
				}
				//	描画
				{
					Command::DrawPrimitiveInfo Info;
					Info.iPrimitive	= iPrimitive;
					Info.iCount		= uiCount;
					Info.pVertex	= pVertex;
					Info.iStride	= sizeof(SimpleVertex);

					m_pCommand->DrawPrimitiveUP(Info);
				}
			}
			//----------------------------------------------------------------------
			//	頂点バッファによる描画(Strip)
			//----------------------------------------------------------------------
			FORCEINLINE void DrawPrimitiveIndexUP(const u16* pIndex, u32 uiCount, const SimpleVertex* pVertex, u32 uiVertexCount, s32 iPrimitive)
			{
				//	頂点フォーマットの設定
				{
					Command::DeclarationInfo DeclInfo;
					DeclInfo.pDeclaration = m_pDecl;
					m_pCommand->SetDeclaration(DeclInfo);
				}
				//	描画
				{
					Command::DrawIndexPrimitiveInfo Info;
					Info.iPrimitive		= iPrimitive;
					Info.iCount			= uiCount;
					Info.pIndex			= pIndex;
					Info.iIndexType		= EIndexType::Index16;
					Info.pVertex		= pVertex;
					Info.iVertexCount	= uiVertexCount;
					Info.iStride		= sizeof(SimpleVertex);

					m_pCommand->DrawIndexedPrimitiveUP(Info);
				}
			}

		public:
			//----------------------------------------------------------------------
			//	シザリング設定
			//----------------------------------------------------------------------
			void ScissorRect(s32 iX, s32 iY, s32 iWidth, s32 iHeight)
			{
				Command::ScissoringInfo Info;
				Info.iY			= iX;
				Info.iX			= iY;
				Info.iWidth		= iWidth;
				Info.iHeight	= iHeight;

				m_pCommand->SetScissoring(Info);
			}
	
		public:
			//----------------------------------------------------------------------
			//	ラインの描画
			//----------------------------------------------------------------------
			void DrawLine(const Vector2& vStart, const Vector2& vEnd, const FColor& DrawColor, s32 iDrawMode)
			{
				SimpleVertex Vertex[2];
				SetupLineVertex(Vertex, vStart, vEnd, Vector4(0, 0, 1, 1), DrawColor);

				SetRenderState(null, iDrawMode);
				DrawPrimitiveUP(Vertex, 1, Command::EPrimitive::LineStrip);
			}

			//----------------------------------------------------------------------
			//	レクトの描画
			//----------------------------------------------------------------------
			void DrawRect(const Vector2& vPos, const Vector2& vSize, const FColor& DrawColor, s32 iDrawMode)
			{
				SimpleVertex Vertex[4];
				SetupRectVertex(Vertex, vPos, vSize, Vector4(0, 0, 1, 1), DrawColor);

				DrawStrip(Vertex, 2, null, iDrawMode);
			}

			//----------------------------------------------------------------------
			//	レクトの描画(回転有り)
			//----------------------------------------------------------------------
			void DrawRotatedRect(const Vector2& vPos, const Vector2& vSize, f32 fAngle, const Vector2& vOrigin, const FColor& DrawColor, s32 iDrawMode)
			{
				SimpleVertex Vertex[4];
				SetupRotatedRectVertex(Vertex, vPos, vSize, Vector4(0, 0, 1, 1), fAngle, vOrigin, DrawColor);

				DrawStrip(Vertex, 2, null, iDrawMode);
			}

			//----------------------------------------------------------------------
			//	テクスチャー描画
			//----------------------------------------------------------------------
			void DrawTexture(ITexture* pTexture, const Vector2& vPos, const Vector2& vSize, const Vector2& vTexPos, const Vector2& vTexSize, const FColor& DrawColor, s32 iDrawMode)
			{
				Vector4 vRectUV(0, 0, 1, 1);
				if( pTexture != null )
				{
					const TextureInfo& Info = pTexture->GetInfo();
					const f32 fInvWidth  = 1.0f / (f32)Info.uiWidth;
					const f32 fInvHeight = 1.0f / (f32)Info.uiHeight;

					vRectUV.x = (vTexPos.x + 0.5f) * fInvWidth;
					vRectUV.y = (vTexPos.y + 0.5f) * fInvHeight;
					vRectUV.z = (vTexPos.x + vTexSize.x) * fInvWidth;
					vRectUV.w = (vTexPos.y + vTexSize.y) * fInvHeight;
				}
				SimpleVertex Vertex[4];
				SetupRectVertex(Vertex, vPos, vSize, vRectUV, DrawColor);

				DrawStrip(Vertex, 2, pTexture, iDrawMode);
			}

			//----------------------------------------------------------------------
			//	テクスチャー描画(回転有り)
			//----------------------------------------------------------------------
			void DrawRotatedTexture(ITexture* pTexture, const Vector2& vPos, const Vector2& vSize, const Vector2& vTexPos, const Vector2& vTexSize, f32 fAngle, const Vector2& vOrigin, const FColor& DrawColor, s32 iDrawMode)
			{
				Vector4 vRectUV(0, 0, 1, 1);
				if( pTexture != null )
				{
					const TextureInfo& Info = pTexture->GetInfo();
					vRectUV.x = (vTexPos.x + 0.5f) / (f32)Info.uiWidth;
					vRectUV.y = (vTexPos.y + 0.5f) / (f32)Info.uiHeight;
					vRectUV.z = (vTexPos.x + vTexSize.x);
					vRectUV.w = (vTexPos.y + vTexSize.y);
				}
				SimpleVertex Vertex[4];
				SetupRotatedRectVertex(Vertex, vPos, vSize, vRectUV, fAngle, vOrigin, DrawColor);

				DrawStrip(Vertex, 2, pTexture, iDrawMode);
			}

		public:
			//----------------------------------------------------------------------
			//	プリミティブ描画(TriangleStrip)
			//----------------------------------------------------------------------
			void DrawStrip(const SimpleVertex* pVertex, u32 uiCount, ITexture* pTexture = null, s32 iDrawMode = EDrawMode::Alpha)
			{
				SetRenderState(pTexture, iDrawMode);
				DrawPrimitiveUP(pVertex, uiCount);
			}
			//----------------------------------------------------------------------
			//	プリミティブ描画(TriangleList)
			//----------------------------------------------------------------------
			void DrawList(const u16* pIndex, u32 uiCount, const SimpleVertex* pVertex, u32 uiVertexCount, ITexture* pTexture = null, s32 iDrawMode = EDrawMode::Alpha)
			{
				SetRenderState(pTexture, iDrawMode);
				DrawPrimitiveIndexUP(pIndex, uiCount, pVertex, uiVertexCount, Command::EPrimitive::TriangleList);
			}

		public:
			//----------------------------------------------------------------------
			//	指定したウィンドウにテクスチャー描画
			//----------------------------------------------------------------------
			b8 DrawToWindow(Window::IWindow* pTarget, ITexture* pTexture)
			{
				if( pTarget && pTarget->IsActive() )
				{
					TKGLPtr<kgGraphics::ISwapChainBase> pSwapChain;
					if( m_pDriver->GraphicDevice()->CreateSwapChain(pSwapChain.GetReference(), pTarget) )
					{
						TKGLPtr<kgGraphics::ISurfaceBase> pSurface;
						pSwapChain->GetSurface(pSurface.GetReference());
						Command::RenderTargetInfo Info;
						Info.iIndex  = 0;
						Info.pTarget = pSurface;
						m_pCommand->SetRenderTarget(Info);

						DrawTexture(pTexture, ZeroVector2, pTarget->GetSize(), ZeroVector2, pTexture->GetSize(), FColorWhite, EDrawMode::Clear);

						Info.pTarget = null;
						m_pCommand->SetRenderTarget(Info);

						return pSwapChain->Present(false);
					}
				}
				return false;
			}

		private:
			TKGLPtr<IGraphicDriver>		m_pDriver;			//!< グラフィックドライバー
			TKGLPtr<ICommandCreator>	m_pCommand;			//!< グラフィックコマンド制御
			TKGLPtr<IDeclaration>		m_pDecl;			//!< 頂点フォーマット管理
			TKGLPtr<IVertexShader>		m_pVertexShader;	//!< 頂点シェーダー
			TKGLPtr<IPixelShader>		m_pPixcelShader;	//!< ピクセルシェーダー
			TKGLPtr<IPixelShader>		m_pFontShader;		//!< ピクセルシェーダー(フォント用)
			TKGLPtr<ITexture>			m_pWhiteTexture;	//!< 白いテクスチャー
		};

		//----------------------------------------------------------------------
		//	描画制御の生成
		//----------------------------------------------------------------------
		b8 Create(IDrawer** ppDrawer, IGraphicDriver* pDriver)
		{
			TKGLPtr<CDrawer> pDrawer = new CDrawer;
			if( !pDrawer->Initialize(pDriver) )
			{
				return false;
			}
			pDrawer->AddRef();
			*ppDrawer = pDrawer;

			return true;
		}
	}
}
//=======================================================================
//	END OF FILE
//=======================================================================
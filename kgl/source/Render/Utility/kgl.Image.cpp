//----------------------------------------------------------------------
//!
//!	@file	kgl.Image.cpp
//!	@brief	画像関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "File/kgl.File.h"
#include "Render/Utility/kgl.Image.h"

#define	KGL_USE_LIBPNG	1

#if	KGL_USE_LIBPNG
#include <png.h>

#pragma comment(lib, "libpng15.lib")
#endif	// ~#if KGL_USE_LIBPNG

namespace kgl
{
	namespace Image
	{
		//	Bitmap File Header
		struct BitmapFileHeader
		{
			u16	pad;		// Dummy
			u16	bfType;
			u32	bfSize;
			u16	bfReserved1;
			u16	bfReserved2;
			u32	bfOffBits;
		};
		const s32 BitmapFileHeaderSize = sizeof(BitmapFileHeader) - 2;
		INLINE void* GetBitmapFileHeaderAddr(BitmapFileHeader* p) { return &p->bfType; };

		//	Bitmap Info Header
		struct BitmapInfoHeader
		{
			u32	biSize;
			s32	biWidth;
			s32	biHeight;
			u16	biPlanes;
			u16	biBitCount;
			u32	biCompression;
			u32	biSizeImage;
			s32	biXPelsPerMeter;
			s32	biYPelsPerMeter;
			u32	biClrUsed;
			u32	biClrImportant;
		};

		//----------------------------------------------------------------------
		//	画像出力(bmp)
		//----------------------------------------------------------------------
		b8 OutputImageToBitmap(const c8* pPath, const void* pImage, s32 iWidth, s32 iHeight, s32 iType)
		{
			s32 iStride;
			switch( iType )
			{
			case EColorType::RGB:	iStride = 3;	break;
			case EColorType::RGBA:	iStride = 4;	break;
			case EColorType::ARGB:	iStride = 4;	break;
			default:	return false;
			}

			TKGLPtr<File::IFile> pFile;
			File::Create(pFile.GetReference());

			if( !pFile->Open(pPath, File::EOpen::Write, 0, File::ECreate::CreateAlways) )
			{
				ErrorTrace("%sを作成できませんでした(kgl::Render::OutputImageToBitmap)", pPath);
				return false;
			}

			// BMP ヘッダの作成
			{
				BitmapFileHeader bfh;
				kgZeroMemory(&bfh, sizeof(bfh));
				bfh.bfType		= ((u16) 'B' | 'M' << 8);
				bfh.bfSize		= BitmapFileHeaderSize;
				bfh.bfOffBits	= BitmapFileHeaderSize + sizeof(BitmapInfoHeader);

				BitmapInfoHeader bih;
				kgZeroMemory(&bih, sizeof(BitmapInfoHeader));
				bih.biSize		= sizeof(BitmapInfoHeader);
				bih.biWidth		= iWidth;
				bih.biHeight	= iHeight;
				bih.biPlanes	= 1;
				bih.biBitCount	= 24;

				void* ptr = GetBitmapFileHeaderAddr(&bfh);
				pFile->Write(ptr, BitmapFileHeaderSize);
				pFile->Write(&bih, sizeof(BitmapInfoHeader));
			}

			// バッファを書き込む
			{
				s32 iWriteSize = iWidth * iHeight * 3;

				{
					register s32 i, j, x, y;
					const u8* data = (u8*)pImage;
					TSmartPtr<u8> writeBuf = new u8[iWriteSize];

					for( x = 0; x < iWidth; x++ )
					{
						for( y = 0; y < iHeight; y++ )
						{
							i = ((iWidth - x - 1) + y * iWidth) * iStride;
							j = (iHeight * iWidth * 3) - ((x + y * iWidth + 1) * 3);

							switch( iType )
							{
							case EColorType::RGB:
							case EColorType::ARGB:
								writeBuf[j+2] = data[i+2];	// Red
								writeBuf[j+1] = data[i+1];	// Green
								writeBuf[j+0] = data[i+0];	// Blur
								break;
							case EColorType::RGBA:
								writeBuf[j+2] = data[i+3];	// Red
								writeBuf[j+1] = data[i+2];	// Green
								writeBuf[j+0] = data[i+1];	// Blur
								break;
							}
						}
					}
					pFile->Write(writeBuf, iWriteSize);
				}
			}
			pFile->Close();
			return true;
		}

#if	KGL_USE_LIBPNG
		//----------------------------------------------------------------------
		//	PNG書き込み
		//----------------------------------------------------------------------
		void WritePNG(png_structp png_ptr, png_bytep data, png_size_t length)
		{
			File::IFile* pFile = (File::IFile*)png_get_io_ptr(png_ptr);
			pFile->Write(data, (u32)length);
		}
#endif	// KGL_USE_LIBPNG

		//----------------------------------------------------------------------
		//	画像出力(png)
		//----------------------------------------------------------------------
		b8 OutputImageToPNG(const c8* pPath, const void* pImage, s32 iWidth, s32 iHeight, s32 iType)
		{
			(void)pPath;
			(void)pImage;
			(void)iWidth;
			(void)iHeight;
			(void)iType;
#if	KGL_USE_LIBPNG
			s32 iStride;
			switch( iType )
			{
			case EColorType::RGB:	iStride = 3;	break;
			case EColorType::RGBA:	iStride = 4;	break;
			case EColorType::ARGB:	iStride = 4;	break;
			default:	return false;
			}

			TKGLPtr<File::IFile> pFile;
			File::Create(pFile.GetReference());

			if( !pFile->Open(pPath, File::EOpen::Write, 0, File::ECreate::CreateAlways) )
			{
				ErrorTrace("%sを作成できませんでした(kgl::Render::OutputImageToPNG)", pPath);
				return false;
			}

			png_structp	png_ptr;
			png_infop	info_ptr;
			{
				png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, null, null, null);
				if( png_ptr == null )
				{
					pFile->Close();
					ErrorTrace("%sを作成できませんでした(kgl::Render::OutputImageToPNG)", pPath);
					return false;
				}

				info_ptr = png_create_info_struct(png_ptr);
				if( info_ptr == null )
				{
					png_destroy_write_struct(&png_ptr, null);
					pFile->Close();
					ErrorTrace("%sを作成できませんでした(kgl::Render::OutputImageToPNG)", pPath);
					return false;
				}
				png_set_write_fn(png_ptr, pFile, WritePNG, null);
			}

			{
				png_uint_32 width	= (png_uint_32)iWidth;
				png_uint_32 height	= (png_uint_32)iHeight;
				png_set_IHDR(png_ptr, info_ptr,
					width, height, 8,
					PNG_COLOR_TYPE_RGB_ALPHA,
					PNG_INTERLACE_NONE,
					PNG_COMPRESSION_TYPE_BASE,
					PNG_FILTER_TYPE_BASE);
				png_write_info(png_ptr, info_ptr);
			}

			register s32 i, j;
			u8* data = (u8*)pImage;

			s32 iPixelCount = iWidth * iHeight;
			TSmartPtr<png_byte>  png_image	= new png_byte[iPixelCount * 4];
			TSmartPtr<png_bytep> png_ptrs	= new png_bytep[iHeight];
			if( png_image == null || png_ptrs == null )
			{
				png_destroy_write_struct(&png_ptr, &info_ptr);
				pFile->Close();
				ErrorTrace("%sを作成できませんでした(kgl::Render::OutputImageToPNG)", pPath);
				return false;
			}

			for( i = 0; i < iPixelCount; i++ )
			{
				j = i * iStride;
				switch( iType )
				{
				case EColorType::RGB:
					png_image[i * 4 + 0] = data[j + 2];	// Red
					png_image[i * 4 + 1] = data[j + 1];	// Green
					png_image[i * 4 + 2] = data[j + 0];	// Blur
					png_image[i * 4 + 3] = 255;			// Alpha
					break;
				case EColorType::RGBA:
					png_image[i * 4 + 0] = data[j + 3];	// Red
					png_image[i * 4 + 1] = data[j + 2];	// Green
					png_image[i * 4 + 2] = data[j + 1];	// Blur
					png_image[i * 4 + 3] = data[j + 0];	// Alpha
					break;
				case EColorType::ARGB:
					png_image[i * 4 + 0] = data[j + 2];	// Red
					png_image[i * 4 + 1] = data[j + 1];	// Green
					png_image[i * 4 + 2] = data[j + 0];	// Blur
					png_image[i * 4 + 3] = data[j + 3];	// Alpha
					break;
				}
			}
			for( i = 0; i < iHeight; i++ )
			{
				png_ptrs[i] = &png_image[i * iHeight * 4];
			}
			png_write_image(png_ptr, png_ptrs);
			png_write_end(png_ptr, info_ptr);

			png_destroy_write_struct(&png_ptr, &info_ptr);

			pFile->Close();
#endif	// KGL_USE_LIBPNG

			return true;
		}
	}
}
//=======================================================================
//	END OF FILE
//=======================================================================
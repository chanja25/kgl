//----------------------------------------------------------------------
//!
//!	@file	kgl.Texture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Manager/kgl.GameManager.h"
#include "Render/Texture/kgl.Texture.h"

namespace kgl
{
	namespace Render
	{
		//======================================================================
		//!	テクスチャークラス
		//======================================================================
		class CTexture
			: public ITexture
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const void* pData)
			{
				if( pData == null )
				{
					return false;
				}

				TKGLPtr<ITextureManager> pTextureManager = KGLGameManager()->GraphicDriver()->TextureManager();

				b8 bRet = false;
				TKGLPtr<ITextureBase> pTexture;

				switch( *(u32*)pData )
				{
				case FOURCC('D', 'D', 'S', ' '):
					bRet = pTextureManager->CreateTextureFromDDS(pTexture.GetReference(), pData);
					break;

				case FOURCC('k', 't', 'e', 'x'):
					//	todo:未実装
					//TextureInfo Info;
					//bRet = pTextureManager->CreateTexture(pTexture.GetReference(), Info, pData);
					break;

				default:
					ErrorTrace("データが正しくないか対応していないフォーマットです(CFont::Initialize)");
					break;
				}

				m_pResource = pTexture;

				m_FilterInfo.iFilterMin	= Command::ETextureFilter::Linear;
				m_FilterInfo.iFilterMag	= Command::ETextureFilter::Linear;
				m_FilterInfo.iFilterMip	= Command::ETextureFilter::Linear;
				m_FilterInfo.iWrapU	= Command::ETextureWrap::Repeat;
				m_FilterInfo.iWrapV	= Command::ETextureWrap::Repeat;

				return bRet;
			}
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(ITextureBase* pResource)
			{
				if( pResource == null )
				{
					return false;
				}

				m_pResource = pResource;

				m_FilterInfo.iFilterMin	= Command::ETextureFilter::Linear;
				m_FilterInfo.iFilterMag	= Command::ETextureFilter::Linear;
				m_FilterInfo.iFilterMip	= Command::ETextureFilter::Linear;
				m_FilterInfo.iWrapU	= Command::ETextureWrap::Repeat;
				m_FilterInfo.iWrapV	= Command::ETextureWrap::Repeat;
				return true;
			}

			//----------------------------------------------------------------------
			//	フィルター情報の設定
			//----------------------------------------------------------------------
			void SetFilterInfo(const FilterInfo& Info)
			{
				m_FilterInfo = Info;
			}
			//----------------------------------------------------------------------
			//	フィルターの設定
			//----------------------------------------------------------------------
			void SetFilter(s32 iMin, s32 iMag, s32 iMip)
			{
				m_FilterInfo.iFilterMin	= iMin;
				m_FilterInfo.iFilterMag	= iMag;
				m_FilterInfo.iFilterMip	= iMip;
			}
			//----------------------------------------------------------------------
			//	ラップモードの設定
			//----------------------------------------------------------------------
			void SetWrapMode(s32 iU, s32 iV)
			{
				m_FilterInfo.iWrapU	= iU;
				m_FilterInfo.iWrapV	= iV;
			}

		public:
			//----------------------------------------------------------------------
			//	リソースの取得
			//----------------------------------------------------------------------
			ITextureBase* GetResource(void)
			{
				return m_pResource;
			}
			//----------------------------------------------------------------------
			//	フィルター情報の取得
			//----------------------------------------------------------------------
			const FilterInfo& GetFilterInfo(void)
			{
				return m_FilterInfo;
			}
			//----------------------------------------------------------------------
			//	テクスチャー情報の取得
			//----------------------------------------------------------------------
			const TextureInfo& GetInfo(void)
			{
				return m_pResource->GetInfo();
			}

		public:
			TKGLPtr<ITextureBase>	m_pResource;
			FilterInfo				m_FilterInfo;
		};

		//----------------------------------------------------------------------
		// テクスチャーの生成
		//----------------------------------------------------------------------
		b8 Create(ITexture** ppTexture, const void* pData)
		{
			TKGLPtr<CTexture> pTexture = new CTexture;
			if( !pTexture->Initialize(pData) )
			{
				return false;
			}
			*ppTexture = pTexture;
			(*ppTexture)->AddRef();
			return true;
		}

		//----------------------------------------------------------------------
		// テクスチャーの生成
		//----------------------------------------------------------------------
		b8 Create(ITexture** ppTexture, ITextureBase* pResource)
		{
			TKGLPtr<CTexture> pTexture = new CTexture;
			if( pTexture->Initialize(pResource) )
			{
				*ppTexture = pTexture;
				(*ppTexture)->AddRef();
				return true;
			}
			return false;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Font.cpp
//!	@brief	文字描画管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Render/kgl.Drawer.h"
#include "Data/Render/kgl.FontData.h"
#include "Utility/algorithm/kgl.find.h"
#include "Manager/kgl.GameManager.h"
#include "Render/Texture/kgl.Texture.h"

namespace kgl
{
	namespace Font
	{
#if WINDOWS
		//======================================================================
		//	デバイスコンテキスト管理クラス
		//======================================================================
		class DeviceContext
		{
		public:
			DeviceContext(HGDIOBJ hObj)
			{
				m_hDC = GetDC(null);
				m_hOldObj = SelectObject(m_hDC, hObj);
			}
			~DeviceContext(void)
			{
				SelectObject(m_hDC, m_hOldObj);
				ReleaseDC(null, m_hDC);
			}
			operator HDC(void) { return m_hDC; }
		private:
			HDC		m_hDC;
			HGDIOBJ	m_hOldObj;
		};
#endif // ~#if WINDOWS

		//	一度に描画する最大文字数
		const u32 DrawFontMax = 64;
		//======================================================================
		//	文字描画管理クラス
		//======================================================================
		class CFont
			: public IFont
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CFont(void)
				: m_vBaseScale(OneVector2)
			{}

			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const void* pData)
			{
				if( pData == null )
				{
					return false;
				}
				if( *(u32*)pData != FOURCC('k', 't', 'f', ' ') )
				{
					ErrorTrace("データが正しくないか対応していないフォーマットです(CFont::Initialize)");
					return false;
				}

				u8* pPtr = (u8*)pData;

				m_FontHeader = *(kgFontHeader*)pPtr;
				//	文字情報の作成
				m_pCharacter.resize(m_FontHeader.iCharCount);
				kgMemcpy(m_pCharacter.data(), pPtr + m_FontHeader.iCharOffset, sizeof(kgFontCharacter) * m_FontHeader.iCharCount);

				if( m_FontHeader.iConvertOffset > 0 )
				{
					//	文字コード変換用情報を作成
					m_pConvert.resize(m_FontHeader.iCharCount);
					kgMemcpy(m_pConvert.data(), pPtr + m_FontHeader.iConvertOffset, sizeof(kgFontConvert) * m_FontHeader.iCharCount);
				}

				auto pTextureManager = KGLGameManager()->GraphicDriver()->TextureManager();

				s32 iFormat = ETextureFormat::A8;
				switch( m_FontHeader.iFormat )
				{
				case Font::EFormat::Alpha:			iFormat = ETextureFormat::A8;	break;
				case Font::EFormat::AlphaOutline:	iFormat = ETextureFormat::A8L8;	break;
				case Font::EFormat::AlphaShadow:	iFormat = ETextureFormat::A8L8;	break;
				//case Font::EFormat::DistanceField:	iFormat = ETextureFormat::A8;	break;
				}
				kgFontTexture* pFontTexture = (kgFontTexture*)(pPtr + m_FontHeader.iTextureOffset);
				//	文字テクスチャーの生成
				m_pTextures.resize(m_FontHeader.iTextureCount);
				for( s32 i=0; i < m_FontHeader.iTextureCount; i++ )
				{
					TextureInfo Info;
					Info.iFormat	= iFormat;
					Info.iType		= ETextureType::Normal;
					Info.iUsage		= 0;
					Info.iMipLevels	= 1;
					Info.uiWidth	= (u32)pFontTexture[i].iWidth;
					Info.uiHeight	= (u32)pFontTexture[i].iHeight;

					TKGLPtr<ITextureBase> pResource;
					pTextureManager->CreateTexture(pResource.GetReference(), Info, pPtr + pFontTexture[i].iOffset);

					Render::Create(m_pTextures[i].GetReference(), pResource);
				}
				return true;
			}

		public:
			//----------------------------------------------------------------------
			//	余白の設定
			//----------------------------------------------------------------------
			void SetPadding(s32 iX, s32 iY)
			{
				m_FontHeader.iCharPaddingX = iX;
				m_FontHeader.iCharPaddingY = iY;
			}
			//----------------------------------------------------------------------
			//	余白の取得
			//----------------------------------------------------------------------
			void GetPadding(s32& iX, s32& iY)
			{
				iX = m_FontHeader.iCharPaddingX;
				iY = m_FontHeader.iCharPaddingY;
			}
			//----------------------------------------------------------------------
			//	余白の取得
			//----------------------------------------------------------------------
			Vector2 GetPadding(const Vector2& vScale)
			{
				Vector2 vPadding(vScale);
				vPadding.x *= m_FontHeader.iCharPaddingX;
				vPadding.y *= m_FontHeader.iCharPaddingY;
				return vPadding;
			}

		public:
			//----------------------------------------------------------------------
			//	スケール算出
			//----------------------------------------------------------------------
			Vector2 CalcScale(const Vector2& vScale)
			{
				Vector2 vDrawScale(vScale);
				vDrawScale.x *= m_vBaseScale.x;
				vDrawScale.y *= m_vBaseScale.y;
				return vDrawScale;
			}
			//----------------------------------------------------------------------
			//	文字列描画
			//----------------------------------------------------------------------
			void Draw(Render::IRenderingSystem* pRenderingSystem, const c8* pText, const Vector2& vPos, const Vector2& vScale, const FColor& DrawColor, s32 DrawAlign)
			{
				if( !m_pConvert.empty() )
				{
					DrawText(pRenderingSystem, pText, vPos, CalcScale(vScale), DrawColor, DrawAlign, &CFont::SearchCharacterShiftJIS);
				}
				else
				{
					u32 uiSize = kgStrlen(pText) * 2;
					TSmartPtr<c16> temp = new c16[uiSize];
					CharSet::ConvertShiftJISToUTF16(temp, pText, &uiSize);
					Draw(pRenderingSystem, (c16*)temp, vPos, vScale, DrawColor, DrawAlign);
				}
			}
			//----------------------------------------------------------------------
			//	文字列を描画した際の描画サイズ取得
			//----------------------------------------------------------------------
			Vector2 GetDrawTextSize(const c8* pText, const Vector2& vScale, Vector2* pNextDrawPos = null)
			{
				if( !m_pConvert.empty() )
				{
					return CalcDrawTextSize(pText, CalcScale(vScale), &CFont::SearchCharacterShiftJIS, pNextDrawPos);
				}
				else
				{
					u32 uiSize = kgStrlen(pText) * 2;
					TSmartPtr<c16> temp = new c16[uiSize];
					CharSet::ConvertShiftJISToUTF16(temp, pText, &uiSize);
					return GetDrawTextSize((c16*)temp, vScale, pNextDrawPos);
				}
			}
			//----------------------------------------------------------------------
			//	文字列描画
			//----------------------------------------------------------------------
			void Draw(Render::IRenderingSystem* pRenderingSystem, const c16* pText, const Vector2& vPos, const Vector2& vScale, const FColor& DrawColor, s32 DrawAlign)
			{
				DrawText(pRenderingSystem, pText, vPos, CalcScale(vScale), DrawColor, DrawAlign, &CFont::SearchCharacter);
			}
			//----------------------------------------------------------------------
			//	文字列を描画した際の描画サイズ取得
			//----------------------------------------------------------------------
			Vector2 GetDrawTextSize(const c16* pText, const Vector2& vScale, Vector2* pNextDrawPos = null)
			{
				return CalcDrawTextSize(pText, CalcScale(vScale), &CFont::SearchCharacter, pNextDrawPos);
			}

		private:
			typedef const kgFontCharacter& (CFont::*FSearchCharacter)(const c16& sChar);
			//----------------------------------------------------------------------
			//	文字列描画
			//----------------------------------------------------------------------
			template<typename Char>
			void DrawText(Render::IRenderingSystem* pRenderingSystem, const Char* pText, const Vector2& vPos, const Vector2& vScale, const FColor& DrawColor, s32 DrawAlign, FSearchCharacter SearchCharacter)
			{
				Vector2 vBasePos = CalcBasePos(pText, vPos, vScale, DrawAlign);

				TKGLPtr<Render::IDrawer> pDrawer = pRenderingSystem->Drawer();

				u16 usIndex[DrawFontMax * 6];
				Render::SimpleVertex Vertex[DrawFontMax * 4];

				const Char* pNext;
				Vector2 vDrawSize;
				Vector2 vDrawPos = vBasePos;
				while( *pText != 0 )
				{
					TKGLPtr<Render::ITexture> pTexture;
					// バッファの先頭を設定しておく
					u16* pIndex = usIndex;
					Render::SimpleVertex* pVertex = Vertex;

					while( *pText != 0 )
					{
						c16 sChar;
						pNext = GetNextChar(pText, &sChar);
						if( IsEOL<Char>(sChar) )
						{
							//	改行処理
							vDrawPos.x = vBasePos.x;
							vDrawPos.y += (f32)m_pCharacter[0].usSizeV * vScale.y;
							vDrawPos.y += (f32)m_FontHeader.iCharPaddingY * vScale.y;
							pText = pNext;
							continue;
						}

						const kgFontCharacter& CharInfo = (this->*SearchCharacter)(sChar);
						if( !pTexture.IsValid() )
						{
							pTexture = m_pTextures[CharInfo.sTextureIndex];
						}
						//	テクスチャーが違う場合は一度描画を行う
						if( pTexture != m_pTextures[CharInfo.sTextureIndex] )
						{
							break;
						}
						pText = pNext;

						Vector4 vRectUV;
						const TextureInfo& Info = pTexture->GetInfo();
						f32 fInvWidth  = 1.0f / (f32)Info.uiWidth;
						f32 fInvHeight = 1.0f / (f32)Info.uiHeight;

						f32 fU = (f32)CharInfo.usU;
						f32	fV = (f32)CharInfo.usV;
						f32	fW = (f32)CharInfo.usSizeU;
						f32	fH = (f32)CharInfo.usSizeV;

						vRectUV.x = (fU + 0.5f) * fInvWidth;
						vRectUV.y = (fV + 0.5f) * fInvHeight;
						vRectUV.z = (fU + fW) * fInvWidth;
						vRectUV.w = (fV + fH) * fInvHeight;

						vDrawSize.x = fW * vScale.x;
						vDrawSize.y = fH * vScale.y;

						if( !IsWhiteSpace<Char>(sChar) )
						{
							pDrawer->SetupRectVertex(pVertex, vDrawPos.SafeFloor(), vDrawSize.SafeFloor(), vRectUV, DrawColor);
							u16 uiIndex = (u16)(pVertex - Vertex);
							*(pIndex ++) = uiIndex + 0;
							*(pIndex ++) = uiIndex + 1;
							*(pIndex ++) = uiIndex + 2;
							*(pIndex ++) = uiIndex + 2;
							*(pIndex ++) = uiIndex + 1;
							*(pIndex ++) = uiIndex + 3;

							pVertex += 4;
						}
						vDrawPos.x += vDrawSize.x;
						vDrawPos.x += (f32)m_FontHeader.iCharPaddingX * vScale.x;
						//	最大描画数を超えている場合は一度描画する
						if( DrawFontMax <= (pVertex - Vertex) * 4 )
						{
							break;
						}
					}
					//	溜まっているバッファを描画する
					if( Vertex != pVertex )
					{
						u32 uiVertexCount = (u32)(pVertex - Vertex);
						u32 uiCount = uiVertexCount / 2;
						pDrawer->DrawList(usIndex, uiCount, Vertex, uiVertexCount, pTexture, Render::EDrawMode::Font);
					}
				}
			}
			//----------------------------------------------------------------------
			//	文字列を描画した際の描画サイズ取得
			//----------------------------------------------------------------------
			template<typename Char>
			Vector2 CalcDrawTextSize(const Char* pText, const Vector2& vScale, FSearchCharacter SearchCharacter, Vector2* pNextDrawPos)
			{
				f32 fWidthMax, fWidth, fHeight;
				fWidthMax = fWidth = fHeight = 0;
				b8 bCalc = false;
				while( *pText != 0 )
				{
					c16 sChar;
					pText = GetNextChar(pText, &sChar);
					if( IsEOL<Char>(sChar) )
					{
						fWidthMax = Max(fWidthMax, fWidth);
						fHeight += (f32)m_pCharacter[0].usSizeV;
						fHeight += (f32)m_FontHeader.iCharPaddingY;
						fWidth = 0;
						bCalc = false;
					}
					else
					{
						const kgFontCharacter& CharInfo = (this->*SearchCharacter)(sChar);
						fWidth += (f32)CharInfo.usSizeU;
						fWidth += (f32)m_FontHeader.iCharPaddingX;
						bCalc = true;
					}
				}
				if( pNextDrawPos )
				{
					pNextDrawPos->x = fWidth  * vScale.x;
					pNextDrawPos->y = fHeight * vScale.y;
				}
				if( bCalc )
				{
					fWidthMax = Max(fWidthMax, fWidth);
					fHeight += (f32)m_pCharacter[0].usSizeV;
					fHeight += (f32)m_FontHeader.iCharPaddingY;
				}
				return Vector2(fWidthMax * vScale.x, fHeight * vScale.y);
			}

		private:
			//----------------------------------------------------------------------
			//	ベース位置の算出
			//----------------------------------------------------------------------
			template<typename Char>
			kgl::Vector2 CalcBasePos(const Char* pText, const Vector2& vPos, const Vector2& vScale, s32 DrawAlign)
			{
				Vector2 vBasePos = vPos;
				switch( DrawAlign )
				{
				case EAlign::LeftBottom:
					vBasePos.y -= GetDrawTextSize(pText, vScale).y;
					break;
				case EAlign::CenterTop:
					vBasePos.x -= GetDrawTextSize(pText, vScale).x * 0.5f;
					break;
				case EAlign::Center:
					vBasePos -= GetDrawTextSize(pText, vScale) * 0.5f;
					break;
				case EAlign::CenterBottom:
					{
						Vector2 vDrawSize = GetDrawTextSize(pText, vScale);
						vBasePos.x -= vDrawSize.x * 0.5f;
						vBasePos.y -= vDrawSize.y;
					}
					break;
				case EAlign::RightTop:
					vBasePos.x -= GetDrawTextSize(pText, vScale).x;
					break;
				case EAlign::RightBottom:
					vBasePos -= GetDrawTextSize(pText, vScale);
					break;
				}
				return vBasePos;
			}

		private:
			//----------------------------------------------------------------------
			//	次の文字を取得
			//----------------------------------------------------------------------
			INLINE const c8* GetNextChar(const c8* pText, c16* pChar)
			{
				c16 sChar = (c16)pText[0] & 0xFF;
				if( IsEOL<c8>(sChar) )
				{
					if( (pText[0] == '\r' && pText[1] == '\n') )
					{
						++ pText;
					}
				}
				else if( CharSet::IsMultiByteChar(pText[0]) )
				{
					sChar = ((pText[1] << 8) & 0xFF00) | (sChar & 0x00FF);
					++ pText;
				}
				++ pText;
				*pChar = sChar;
				return pText;
			}
			//----------------------------------------------------------------------
			//	次の文字を取得
			//----------------------------------------------------------------------
			INLINE const c16* GetNextChar(const c16* pText, c16* pChar)
			{
				c16 sChar = pText[0];
				if( IsEOL<c16>(sChar) )
				{
					if( sChar == L'\r' && pText[1] == L'\n' )
					{
						++ pText;
					}
				}
				++ pText;
				*pChar = sChar;
				return pText;
			}
			//----------------------------------------------------------------------
			//	改行文字か？
			//----------------------------------------------------------------------
			template<typename Char>
			INLINE b8 IsEOL(s16 sChar)
			{
				return sChar == L'\n' || sChar == L'\r';
			}
			//----------------------------------------------------------------------
			//	改行文字か？
			//----------------------------------------------------------------------
			template<>
			INLINE b8 IsEOL<c8>(s16 sChar)
			{
				c8 cChar = (c8)(sChar & 0xff);
				return cChar == '\n' || cChar == '\r';
			}
			//----------------------------------------------------------------------
			//	改行文字か？
			//----------------------------------------------------------------------
			template<typename Char>
			INLINE b8 IsWhiteSpace(s16 sChar)
			{
				return sChar == L' ';
			}
			//----------------------------------------------------------------------
			//	改行文字か？
			//----------------------------------------------------------------------
			template<>
			INLINE b8 IsWhiteSpace<c8>(s16 sChar)
			{
				c8 cChar = (c8)(sChar & 0xff);
				return cChar == ' ';
			}

		protected:
			//----------------------------------------------------------------------
			//	比較用関数
			//----------------------------------------------------------------------
			static s32 CompareChar(const c16& sChar, const kgFontCharacter& CharInfo)
			{
				return sChar - CharInfo.sChar;
			}
			//----------------------------------------------------------------------
			//	比較用関数(Shift-JISのコンバート情報)
			//----------------------------------------------------------------------
			static s32 CompareChar(const c16& sChar, const kgFontConvert& ConvertInfo)
			{
				return sChar - ConvertInfo.sChar;
			}
			//----------------------------------------------------------------------
			//	文字情報の検索
			//----------------------------------------------------------------------
			virtual const kgFontCharacter& SearchCharacter(const c16& sChar)
			{
				u32 uiIndex = algorithm::binary_search<c16, kgFontCharacter>(sChar, m_pCharacter.data(), m_FontHeader.iCharCount-1, CompareChar);

				return m_pCharacter[uiIndex != algorithm::NotFind? uiIndex: m_FontHeader.iCharCount-1];
			}
			//----------------------------------------------------------------------
			//	文字情報の検索
			//----------------------------------------------------------------------
			const kgFontCharacter& SearchCharacterShiftJIS(const c16& sChar)
			{
				// コンバートテーブル内から文字情報へのインデックスを取得する
				u32 uiIndex = algorithm::binary_search<c16, kgFontConvert>(sChar, m_pConvert.data(), m_FontHeader.iCharCount-1, CompareChar);

				return m_pCharacter[uiIndex != algorithm::NotFind? m_pConvert[uiIndex].sIndex: m_FontHeader.iCharCount-1];
			}

		protected:
			kgFontHeader									m_FontHeader;	//!< ヘッダー情報
			algorithm::vector<kgFontCharacter>				m_pCharacter;	//!< 文字情報
			algorithm::vector<kgFontConvert>				m_pConvert;		//!< コンバート情報
			algorithm::vector<TKGLPtr<Render::ITexture>>	m_pTextures;	//!< テクスチャーデータ
			Vector2											m_vBaseScale;	//!< 基本のスケール
		};

		// Platform Dependent
		//======================================================================
		//	文字描画管理クラス(TTF)
		//======================================================================
		class CFontTTF
			: public CFont
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CFontTTF(void)
				: m_hTTF(null)
				, m_hFont(null)
				, m_uiPosX(0)
				, m_uiPosY(0)
			{}
			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CFontTTF(void)
			{
				if( m_hFont )
				{
					DeleteObject(m_hFont);
				}
				if( m_hTTF )
				{
					RemoveFontMemResourceEx(m_hTTF);
				}
			}
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const FontInfo& Info)
			{
				if( Info.pTTF )
				{
					dword dwFonts;
					m_hTTF = AddFontMemResourceEx((void*)Info.pTTF, Info.uiTTFSIze, null, &dwFonts);
					kgCheckErrorLog(m_hTTF, "フォントの追加に失敗しました[%s](CFontTTF::Initialize)", Info.cFont);
				}
				m_hFont = CreateFontA(
							Info.iSize,
							0,
							0,
							0,
							Info.bBold? FW_BOLD: FW_NORMAL,
							Info.bItalic,
							false,
							false,
							DEFAULT_CHARSET,
							OUT_TT_ONLY_PRECIS,
							CLIP_DEFAULT_PRECIS,
							PROOF_QUALITY,
							DEFAULT_PITCH | FF_MODERN,
							Info.cFont
							);
				if( m_hFont == null )
				{
					kgErrorLog("フォントの作成に失敗しました[%s](CFontTTF::Initialize)", Info.cFont);
					return false;
				}

				m_FontHeader.iCharPaddingX = Info.iCharPaddingX;
				m_FontHeader.iCharPaddingY = Info.iCharPaddingY;
				m_Info = Info;
				// サイズによってテクスチャーサイズを変更する
				m_uiTexSize = 256;
				m_uiTexSize = Info.iSize > 64?   512: m_uiTexSize;
				m_uiTexSize = Info.iSize > 128? 1024: m_uiTexSize;
				if( Info.iSize > 1024 )
				{
					kgErrorLog("フォントの作成に失敗しました(1024以下のサイズを指定してください)(CFontTTF::Initialize)", Info.cFont);
					return false;
				}
				CreateTexture();
				RegistCharacter(L' ');

				return true;
			}

		public:
			//----------------------------------------------------------------------
			//	テクスチャーの追加
			//----------------------------------------------------------------------
			Render::ITexture* CreateTexture(void)
			{
				auto pTextureManager = KGLGameManager()->GraphicDriver()->TextureManager();
				s32 iFormat = ETextureFormat::A8;
				s32 iStride = 1;
				if( m_Info.bOutline )
				{
					iFormat = ETextureFormat::A8L8;
					iStride = 2;
				}
				TextureInfo Info;
				Info.iFormat	= iFormat;
				Info.iType		= ETextureType::Normal;
				Info.iUsage		= ETextureUsage::Dynamic;
				Info.iMipLevels	= 1;
				Info.uiWidth	= m_uiTexSize;
				Info.uiHeight	= m_uiTexSize;

				u32 uiSize = m_uiTexSize * m_uiTexSize * iStride;
				TSmartPtr<u8> pInit = new u8[uiSize];
				kgZeroMemory(pInit, uiSize);

				TKGLPtr<ITextureBase> pResrouce;
				pTextureManager->CreateTexture(pResrouce.GetReference(), Info, pInit);
				TKGLPtr<Render::ITexture> pTexture;
				Render::Create(pTexture.GetReference(), pResrouce);

				m_pTextures.push_back(pTexture);
				return pTexture;
			}
			//----------------------------------------------------------------------
			//	文字の追加
			//----------------------------------------------------------------------
			const kgFontCharacter& RegistCharacter(c16 sChar)
			{
#if WINDOWS
				DeviceContext hDC(m_hFont);

				const MAT2 mat = {{0,1}, {0,0}, {0,0}, {0,1}};
				GLYPHMETRICS gm;
				dword size = GetGlyphOutline(hDC, sChar, GGO_GRAY8_BITMAP, &gm, 0, null, &mat);

				TSmartPtr<u8> pBmp = new u8[size];
				GetGlyphOutline(hDC, sChar, GGO_GRAY8_BITMAP, &gm, size, pBmp, &mat);
				TEXTMETRIC tm;
				GetTextMetrics(hDC, &tm);
				TKGLPtr<Render::ITexture> pTexture = m_pTextures.back();

				s32 ofsX = Max<s32>(gm.gmptGlyphOrigin.x, 0);
				s32 ofsY = Max<s32>((tm.tmAscent - gm.gmptGlyphOrigin.y), 0);
				s32 bmpW = gm.gmBlackBoxX + (4 - gm.gmBlackBoxX % 4) % 4;
				s32 bmpH = gm.gmBlackBoxY;
				s32 iWidth	= gm.gmCellIncX;
				s32 iHeight	= (ofsY + bmpH);
				if( size > 0 )
				{
					if( m_uiTexSize <= m_uiPosX + iWidth + m_FontHeader.iCharPaddingX )
					{
						m_uiPosX = 0;
						m_uiPosY += Align(m_Info.iSize, 4);
					}
					if( m_uiTexSize <= m_uiPosY + m_Info.iSize + m_FontHeader.iCharPaddingY )
					{
						pTexture = CreateTexture();
						m_uiPosY = m_uiPosX = 0;
					}
					const s32 Sample = Max(1, m_Info.iSize / 64);
					const f32 Power = 16.0f;

					auto pResource = pTexture->GetResource();
					u8* pBits = (u8*)pResource->Lock(0, kgGraphics::ELockType::Write);
					//	文字をテクスチャーに書き込む
					for( s32 y = ofsY; y < ofsY + bmpH; y++ )
					{
						for( s32 x = ofsX; x < ofsX + bmpW; x++ )
						{
							const s32 GrayLevel = 65;
							const f32 InvGrayLevel = 1.0f / (GrayLevel-1);
							s32 _x = (x - ofsX);
							s32 _y = (y - ofsY);
							f32 color = (f32)(pBmp[_x + _y * bmpW] * 255) * InvGrayLevel;
							if( color < 255.0f )
							{
								//	簡易的なスムージング処理
								for( s32 yy=-Sample; yy <= Sample; yy++ )
								{
									s32 posY = (_y + yy);
									if( posY < 0 || bmpH <= posY )continue;
									for( s32 xx=-Sample; xx <= Sample; xx++ )
									{
										s32 posX = (_x + xx);
										if( posX < 0 || bmpW <= posX )continue;
										if( yy == 0 && xx == 0 )continue;

										color += (f32)(pBmp[posX + posY * bmpW] * 255) * InvGrayLevel / ((Abs(xx) + Abs(yy)) * Power);
									}
								}
							}
							u32 uiPosX = m_uiPosX + x;
							u32 uiPosY = m_uiPosY + y;
							pBits[uiPosX + uiPosY * m_uiTexSize] = (u8)Min(color, 255.0f);
						}
					}
					pResource->Unlock(0);
				}
				else
				{
					iWidth	= m_Info.iSize / 2;
					iHeight	= m_Info.iSize;
				}
#else
				sChar = L" ";
				iWidth	= m_Info.iSize / 2;
				iHeight	= m_Info.iSize;
#endif

				kgFontCharacter CharInfo;
				CharInfo.sChar = sChar;
				CharInfo.sTextureIndex = (s16)(m_pTextures.size()-1);
				CharInfo.usU = (s16)m_uiPosX;
				CharInfo.usV = (s16)m_uiPosY;
				CharInfo.usSizeU = (s16)iWidth;
				CharInfo.usSizeV = (s16)iHeight;
				u32 index = 0;
				//	挿入位置の検索
				for( ; index < m_pCharacter.size(); index ++ )
				{
					if( sChar < m_pCharacter[index].sChar )
					{
						break;
					}
				}
				m_pCharacter.insert(index, CharInfo);
				m_uiPosX += Align(Max(iWidth, + ofsX + bmpW) + 4, 4);

				return m_pCharacter[index];
			}

		public:
			//----------------------------------------------------------------------
			//	文字情報の検索
			//----------------------------------------------------------------------
			const kgFontCharacter& SearchCharacter(const c16& sChar)
			{
				if( sChar <= L' ' ) return m_pCharacter[0];
				u32 uiIndex = algorithm::binary_search<c16, kgFontCharacter>(sChar, m_pCharacter.data(), m_pCharacter.size(), CFont::CompareChar);
				//	未登録の文字は新しく追加する
				if( uiIndex == algorithm::NotFind )
				{
					return RegistCharacter(sChar);
				}
				return m_pCharacter[uiIndex];
			}

		private:
			FontInfo	m_Info;			//!< フォント情報
			u32			m_uiTexSize;	//!< テクスチャーサイズ
			HANDLE		m_hTTF;			//!< TTF用ハンドル
			HFONT		m_hFont;		//!< フォントハンドル
			u32			m_uiPosX;		//!< 現在の書き込み位置(X軸)
			u32			m_uiPosY;		//!< 現在の書き込み位置(Y軸)
		};

		//----------------------------------------------------------------------
		// フォントの生成
		//----------------------------------------------------------------------
		b8 Create(IFont** ppFont, const void* pData)
		{
			TKGLPtr<CFont> pFont = new CFont;
			if( pFont->Initialize(pData) )
			{
				*ppFont = pFont;
				(*ppFont)->AddRef();
				return true;
			}
			return false;
		}

		//----------------------------------------------------------------------
		// フォントの生成
		//----------------------------------------------------------------------
		b8 Create(IFont** ppFont, const FontInfo& Info)
		{
			TKGLPtr<CFontTTF> pFont = new CFontTTF;
			if( pFont->Initialize(Info) )
			{
				*ppFont = pFont;
				(*ppFont)->AddRef();
				return true;
			}
			return false;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Texture.cpp
//!	@brief	テクスチャー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Manager/kgl.GameManager.h"
#include "Render/Texture/kgl.Texture.h"

namespace kgl
{
	namespace Render
	{
		//======================================================================
		//!	シェーダーインターフェイス
		//======================================================================
		class CVertexShader
			: public IVertexShader
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const void* pData)
			{
				if( pData == null )
				{
					return false;
				}
				TKGLPtr<IShaderManager> pShaderManager = KGLGameManager()->GraphicDriver()->ShaderManager();
				return pShaderManager->CreateVertexShader(m_pShader.GetReference(), pData);
			}

			//----------------------------------------------------------------------
			//	リソースの取得
			//----------------------------------------------------------------------
			kgGraphics::IVertexShader* GetResource(void)
			{
				return m_pShader;
			}

		private:
			TKGLPtr<kgGraphics::IVertexShader>	m_pShader;
		};
		//======================================================================
		//!	ピクセルシェーダークラス
		//======================================================================
		class CPixelShader
			: public IPixelShader
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const void* pData)
			{
				if( pData == null )
				{
					return false;
				}
				TKGLPtr<IShaderManager> pShaderManager = KGLGameManager()->GraphicDriver()->ShaderManager();
				return pShaderManager->CreatePixelShader(m_pShader.GetReference(), pData);
			}

			//----------------------------------------------------------------------
			//	リソースの取得
			//----------------------------------------------------------------------
			kgGraphics::IPixelShader* GetResource(void)
			{
				return m_pShader;
			}

		private:
			TKGLPtr<kgGraphics::IPixelShader>	m_pShader;
		};

		//----------------------------------------------------------------------
		//	頂点シェーダーの生成
		//----------------------------------------------------------------------
		b8 Create(IVertexShader** ppShader, const void* pData)
		{
			TKGLPtr<CVertexShader> pShader = new CVertexShader;
			if( !pShader->Initialize(pData) )
			{
				return false;
			}
			*ppShader = pShader;
			(*ppShader)->AddRef();
			return true;
		}
		//----------------------------------------------------------------------
		//	ピクセルシェーダーの生成
		//----------------------------------------------------------------------
		b8 Create(IPixelShader** ppShader, const void* pData)
		{
			TKGLPtr<CPixelShader> pShader = new CPixelShader;
			if( !pShader->Initialize(pData) )
			{
				return false;
			}
			*ppShader = pShader;
			(*ppShader)->AddRef();
			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
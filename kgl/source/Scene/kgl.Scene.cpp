//----------------------------------------------------------------------
//!
//!	@file	kgl.Scene.cpp
//!	@brief	シーン管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Scene/kgl.Scene.h"
#include "Thread/kgl.Thread.h"

namespace kgl
{
	namespace Scene
	{
		//!	シーン情報
		struct SceneData
		{
			FSceneCreate	Func;	//!< シーン作成関数
			const c8*		pName;	//!< シーン名
		};

		//======================================================================
		//!	シーンマネージャー
		//======================================================================
		class CSceneManager
			: public ISceneManager
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CSceneManager(void)
				: m_pCurrentScene(null), m_pNextScene(null)
				, m_iSceneNum(0), m_iScene(-1), m_iPrevScene(-1)
			{
				Initialize();
			}

			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CSceneManager(void)
			{
				if( m_pCurrentScene )
				{
					for( s32 i=0; i < m_iSceneNum; i++ )
					{
						m_pBeforeScene[i]->Finalize();
						SafeRelease(m_pBeforeScene[i]);
					}

					m_pCurrentScene->Finalize();
					SafeRelease(m_pCurrentScene);
				}
			}
			
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			void Initialize(void)
			{
				kgZeroMemory(m_pBeforeScene, sizeof(m_pBeforeScene));
				kgZeroMemory(m_SceneData, sizeof(m_SceneData));
			}

			//----------------------------------------------------------------------
			//	更新
			//----------------------------------------------------------------------
			void Update(f32 fElapsedTime)
			{
				if( m_pCurrentScene != null )
				{
					m_pCurrentScene->Update(fElapsedTime);
				}
			}

			//----------------------------------------------------------------------
			//	描画
			//----------------------------------------------------------------------
			void Render(Render::IRenderingSystem* pRenderingSystem)
			{
				if( m_pCurrentScene != null )
				{
					m_pCurrentScene->Render(pRenderingSystem);
				}
			}
			
			//----------------------------------------------------------------------
			//	シーン作成
			//----------------------------------------------------------------------
			b8 CreateScene(s32 iSceneNo)
			{
				b8 bResult = false;
				SceneData& SceneData = m_SceneData[iSceneNo];
				kgAssert(SceneData.Func != null, CharSet::Format("%s:シーンが存在しません", SceneName(iSceneNo)));
				if( SceneData.Func )
				{
					//	新しいシーンのセット
					m_pCurrentScene = SceneData.Func();
					if( m_pCurrentScene )
					{
						m_pCurrentScene->AddRef();
						m_iPrevScene = m_iScene;
						m_iScene = iSceneNo;
						//	シーンの初期化
						bResult = m_pCurrentScene->Initialize();
						kgAssert(bResult, CharSet::Format("%s:シーンの初期化に失敗しました", SceneName(iSceneNo)));
					}
				}
				return bResult;
			}

			//----------------------------------------------------------------------
			//	シーンのセット
			//----------------------------------------------------------------------
			void SetScene(s32 iSceneNo, FSceneCreate Func, const c8* pName)
			{
				(void)pName;
				//	既にシーンが存在していれば消しておく
				if( m_SceneData[iSceneNo].Func )
				{
					WarningTrace("既にシーンが存在する場所にシーンを追加しました");
					WarningTrace("シーンの上書きを行います");
				}
				//	シーンの生成用コールバックを設定
				m_SceneData[iSceneNo].Func = Func;
				//	シーン名の設定
				m_SceneData[iSceneNo].pName = pName;
			}

			//----------------------------------------------------------------------
			//	シーン変更
			//----------------------------------------------------------------------
			void JumpScene(s32 iSceneNo, b8 bAllClose)
			{
				//	保有中のシーン破棄
				if( bAllClose )
				{
					for( s32 i=0; i < m_iSceneNum; i++ )
					{
						m_pBeforeScene[i]->Finalize();
						SafeRelease(m_pBeforeScene[i]);
					}
					m_iSceneNum = 0;
				}
				//	シーン破棄
				if( m_pCurrentScene )
				{
					m_pCurrentScene->Finalize();
					SafeRelease(m_pCurrentScene);
				}

				CreateScene(iSceneNo);
			}

			//----------------------------------------------------------------------
			//	現在のシーンを保存したままシーン変更する
			//----------------------------------------------------------------------
			void CallScene(s32 iSceneNo)
			{
				kgAssert(m_iSceneNum == SCENE_MAX, CharSet::Format("保有可能なシーン数の限界です(最大%dシーンまで)", SCENE_MAX));

				m_pBeforeScene[m_iSceneNum] = m_pCurrentScene;

				CreateScene(iSceneNo);

				m_iSceneNum ++;
			}

			//----------------------------------------------------------------------
			//	保存したシーンに戻る
			//----------------------------------------------------------------------
			void ReturnScene(void)
			{
				if( m_pCurrentScene )
				{
					m_pCurrentScene->Finalize();
					SafeRelease(m_pCurrentScene);
				}

				kgAssert(m_iSceneNum == 0, "シーンが存在しません(ReturnScene)");

				if( m_iSceneNum > 0 )
				{
					//	保存されているシーンをセット
					m_pCurrentScene = m_pBeforeScene[m_iSceneNum];

					m_pBeforeScene[m_iSceneNum] = null;
					m_iSceneNum --;

					u32 uiScene = m_iScene;
					m_iScene = m_iPrevScene;
					m_iPrevScene = uiScene;
				}
			}

		public:
			//----------------------------------------------------------------------
			//	現在のシーンを取得
			//----------------------------------------------------------------------
			s32 NowScene(void)
			{
				return m_iScene;
			}

			//----------------------------------------------------------------------
			//	前回のシーンを取得
			//----------------------------------------------------------------------
			s32 PrevScene(void)
			{
				return m_iPrevScene;
			}
			//----------------------------------------------------------------------
			//	現在のシーン名を取得
			//----------------------------------------------------------------------
			const c8* NowSceneName(void)
			{
				return SceneName(NowScene());
			}

			//----------------------------------------------------------------------
			//	前回のシーン名を取得
			//----------------------------------------------------------------------
			const c8* PrevSceneName(void)
			{
				return SceneName(PrevScene());
			}

			//----------------------------------------------------------------------
			//	シーン名を取得
			//----------------------------------------------------------------------
			const c8* SceneName(s32 iSceneNo)
			{
				if( 0 <= iSceneNo && iSceneNo < SCENE_MAX )
				{
					return m_SceneData[iSceneNo].pName;
				}
				return "";
			}

		private:
			SceneData	m_SceneData[SCENE_MAX];				//!< シーン管理用
			ISceneBase*	m_pCurrentScene;					//!< 現在のシーン
			ISceneBase*	m_pNextScene;						//!< 次のシーン
			ISceneBase*	m_pBeforeScene[SCENE_CHAIN_MAX];	//!< 前回までのシーン
			s32			m_iScene;							//!< 次のシーン
			s32			m_iPrevScene;						//!< 前のシーン
			s32			m_iSceneNum;						//!< 保有中のシーン数
		};

		//----------------------------------------------------------------------
		//	シーンマネージャーの生成
		//----------------------------------------------------------------------
		b8 Create(ISceneManager** ppSceneManager)
		{
			*ppSceneManager = new CSceneManager;
			(*ppSceneManager)->AddRef();

			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
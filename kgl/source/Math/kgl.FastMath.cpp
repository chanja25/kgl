//----------------------------------------------------------------------
//!
//!	@file	kgl.FastMath.cpp
//!	@brief	高速計算関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

namespace kgl
{
	namespace FastMath
	{
		namespace Utility
		{
			s32 iInitCount = 0;
			f32* pSinTable = null;
		}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 Initialize(void)
		{
			Utility::iInitCount ++;
			//	初期化済み
			if( Utility::pSinTable )
			{
				return true;
			}
			const s32 CreateSinTableCount = Utility::SinTableCount;
			//	360度をテーブル分だけ分割して算出する
			f32 fInv = Utility::SinTableCountInv * Math::PI2;

			//	サインテーブルの作成
			Utility::pSinTable = new f32[CreateSinTableCount];
			for( s32 i = 0; i < CreateSinTableCount; i++ )
			{
				Utility::pSinTable[i] = sinf((f32)i * fInv);
			}
			return true;
		}
		//----------------------------------------------------------------------
		//	解放
		//----------------------------------------------------------------------
		void Finalize(void)
		{
			if( Utility::iInitCount <= 0 )return;
			Utility::iInitCount --;
			//	解放回数が初期化回数と一致していれば解放する
			if( Utility::iInitCount == 0 )
			{
				SafeDeleteArray(Utility::pSinTable);
			}
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
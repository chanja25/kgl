//---------------------------------------------------------------------------
//!
//!	@file	kgl.Script.cpp
//!	@brief	スクリプト関連
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Data/Script/kgl.Script.h"
#include "Data/Text/kgl.Lexser.h"

namespace kgl
{
	namespace Script
	{
		using namespace Text;

		//!	関数タイプ
		namespace EFunction
		{
			const s32 Unkown			=-1;	//!< 不明なタイプ
			const s32 Equal				= 0;	//!< 代入[=]
			const s32 Plus				= 1;	//!< 加算[+]
			const s32 Minus				= 2;	//!< 減算[-]
			const s32 Multi				= 3;	//!< 掛け算[*]
			const s32 Division			= 4;	//!< 割り算[/]
			const s32 PlusEqual			= 5;	//!< 加算代入[+=]
			const s32 MinusEqual		= 6;	//!< 減算代入[-=]
			const s32 MultiEqual		= 7;	//!< 掛け算代入[*=]
			const s32 DivisionEqual		= 8;	//!< 割り算代入[/=]

			const s32 Compare			= 20;	//!< 比較[==]
			const s32 NotCompare		= 21;	//!< 比較[!=]
			const s32 Not				= 22;	//!< 否定[!]
			const s32 Less				= 23;	//!< 比較[<]
			const s32 LessEqual			= 24;	//!< 比較[<=]
			const s32 Greater			= 25;	//!< 比較[>]
			const s32 GreaterEqual		= 26;	//!< 比較[>=]
			const s32 LeftShift			= 27;	//!< 左シフト[<<]
			const s32 RightShift		= 28;	//!< 右シフト[>>]
			const s32 LeftShiftEqual	= 29;	//!< 左シフト代入[<<=]
			const s32 RightShiftEqual	= 30;	//!< 右シフト代入[>>=]

			const s32 Cast				= 40;	//!< キャスト
			const s32 Sub				= 41;	//!< サブ関数
			const s32 Support			= 42;	//!< サブ関数
		};

		class CScriptBase;
		//!	スクリプト処理
		struct ScriptFunction
		{
			s32		iFunction;	//!< 処理
			s32		iPointer;	//!< 関数へのポインタ
		};

		struct VariableInfo
		{
			string	strLabel;	//!< ラベル
			s32		iType;		//!< 変数のタイプ
		};
		struct Stack
		{
			vector<MacroInfo>	m_Macro;	//!< マクロ
			vector<ValueInfo>	m_Global;	//!< グローバル関連
			vector<StackList>	m_Stack;	//!< スタックリスト
			StackList*			m_pStack;	//!< スタック
		};

#define	CheckCompare(x, y)	(kgStrcmp(x, y) == 0)
		//===========================================================================
		//!	スクリプトベース
		//===========================================================================
		class CScriptBase
			: public IKGLBase
		{
		public:
			//----------------------------------------------------------------------
			//!	コンパイル
			//----------------------------------------------------------------------
			b8 Compile(Token* pToken)
			{
				if( pToken == null )return false;

				if( Analyze(pToken) != null )
				{
					//	解析が全て完了していない場合はエラーとして処理する
					if( m_strError.length() == 0 )
					{
						m_strError += "Syntax Error[Token:";
						m_strError += pToken->pToken;
						m_strError += ", Line:";
						m_strError += pToken->uiLine;
						m_strError += "]";
					}
					ErrorTrace("コンパイルに失敗しました(CScript::Compile)");
					ErrorTrace("%s", m_strError.c_str());
					return false;
				}

				return true;
			}

			//----------------------------------------------------------------------
			//!	トークン解析
			//----------------------------------------------------------------------
			Token* Analyze(Token* pToken)
			{
				Token* pNext = pToken;
				do
				{
					pToken = pNext;
					//const c8* pStr = pToken->pToken;

					switch( pToken->iType )
					{
					case EType::AlNum:		pToken = AnalyzeAlnum(pToken);		break;
					case EType::Number:		pToken = AnalyzeNumber(pToken);		break;
					case EType::String:		pToken = AnalyzeString(pToken);		break;
					case EType::Operator:	pToken = AnalyzeOperator(pToken);	break;

					default:
						return pToken;	//Error
					}
					pNext = pToken->pNext;
				}
				while( (pToken = pToken->pNext) != null );

				return null;
			}
			//----------------------------------------------------------------------
			//!	トークン解析
			//----------------------------------------------------------------------
			Token* AnalyzeAlnum(Token* pToken)
			{
				return pToken;
			}
			//----------------------------------------------------------------------
			//!	トークン解析
			//----------------------------------------------------------------------
			Token* AnalyzeNumber(Token* pToken)
			{
				return pToken;
			}
			//----------------------------------------------------------------------
			//!	トークン解析
			//----------------------------------------------------------------------
			Token* AnalyzeString(Token* pToken)
			{
				return pToken;
			}
			//----------------------------------------------------------------------
			//!	トークン解析
			//----------------------------------------------------------------------
			Token* AnalyzeOperator(Token* pToken)
			{
				return pToken;
			}

		public:
			vector<ValueInfo>	m_Process;	//!< 処理
			string				m_strError;	//!< エラー情報
		};

		//===========================================================================
		//!	スクリプトクラス
		//===========================================================================
		class CScript
			: public IScript
		{
		public:
			//----------------------------------------------------------------------
			//	スクリプトの生成
			//----------------------------------------------------------------------
			b8 Create(const c8* pPath)
			{
				CLexer Lexer;
				if( !Lexer.LoadText(pPath) )
				{
					return false;
				}
				const Token* pToken = Lexer.AnalyzeAll();
				if( Lexer.IsError()  )
				{
					if( pToken != null )
					{
						while( pToken->pNext )
						{
							pToken = pToken->pNext;
						}
						ErrorTrace("%sの初期化に失敗しました(CScript::Create)", pPath);
						ErrorTrace("Syntax Error[Token:%s, Line:%d]", pToken->pToken, pToken->uiLine);
					}
					else
					{
						ErrorTrace("%sはテキストが存在しません(CScript::Create)", pPath);
					}
					return false;
				}

				return Compile(pToken);
			}
			//----------------------------------------------------------------------
			//	コンパイル
			//----------------------------------------------------------------------
			b8 Compile(const Token* pToken)
			{
				return pToken == null;
			}

		public:
			//----------------------------------------------------------------------
			//	関数呼び出し
			//----------------------------------------------------------------------
			ValueInfo* Process(ValueInfo* pFunc, vector<string> strArg)
			{
				(void)strArg;
				return pFunc;
			}
			//----------------------------------------------------------------------
			//	関数呼び出し
			//----------------------------------------------------------------------
			ValueInfo* CallFunction(string strFunc, vector<string> strArg)
			{
				ValueInfo* pFunc = null;

				StackList* pStack = m_Stack.m_pStack;
				while( pStack )
				{
					u32 count = pStack->Value.size();
					for( u32 i=0; i < count; i++ )
					{
						if( pStack->Value[i].iType == EVariable::Function )
						{
							if( pStack->Value[i].strLabel == strFunc )
							{
								pFunc = &pStack->Value[i];
								break;
							}
						}
					}
					pStack = pStack->pPrev;
				}
				if( pFunc == null )
				{
					u32 count = m_Stack.m_Global.size();
					for( u32 i=0; i < count; i++ )
					{
						if( m_Stack.m_Global[i].iType == EVariable::Function )
						{
							if( m_Stack.m_Global[i].strLabel == strFunc )
							{
								pFunc = &m_Stack.m_Global[i];
								break;
							}
						}
					}
				}

				if( pFunc != null )
				{
					return Process(pFunc, strArg);
				}
				return null;
			}
			//----------------------------------------------------------------------
			//	関数呼び出し
			//----------------------------------------------------------------------
			void ReleaseValue(ValueInfo* pValue)
			{
				if( pValue == null )return;

				pValue->iCount --;
				if( pValue->iCount <= 0 )
				{
					switch( pValue->iType )
					{
					case 0:
						break;
					case 1:
						break;
					}
					SafeDelete(pValue->pValue);
					SafeDelete(pValue);
				}
			}
			//----------------------------------------------------------------------
			//	現在のスタックを取得
			//----------------------------------------------------------------------
			const StackList& GetStack(void)
			{
				return *m_Stack.m_pStack;
			}
			//----------------------------------------------------------------------
			//	全体のスタックを取得
			//----------------------------------------------------------------------
			const vector<StackList>& GetStackAll(void)
			{
				return m_Stack.m_Stack;
			}
			//----------------------------------------------------------------------
			//	グローバルスタックを取得
			//----------------------------------------------------------------------
			const vector<ValueInfo>& GetGlobalStack(void)
			{
				return m_Stack.m_Global;
			}

		private:
			Stack	m_Stack;	//!< スタック
		};

		//----------------------------------------------------------------------
		//	スクリプトの生成
		//----------------------------------------------------------------------
		b8 Create(IScript** ppScript)
		{
			*ppScript = new CScript();
			(*ppScript)->AddRef();

			return true;
		}
	}
}

//============================================================================
//	END OF FILE
//============================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.xml.cpp
//!	@brief	XML関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Data/xml/kgl.xml.h"
#include "Utility/algorithm/kgl.find.h"

namespace kgl
{
	namespace Xml
	{
		namespace utility
		{
			//----------------------------------------------------------------------
			//	属性の検索用
			//----------------------------------------------------------------------
			static b8 compareAttributeTag(const string& strTag, const CAttribute& target)
			{
				return strTag == target.GetTag();
			}
			//----------------------------------------------------------------------
			//	ノードの検索用
			//----------------------------------------------------------------------
			static b8 compareNodeTag(const string& strTag, CNode* const& target)
			{
				return target != null && strTag == (*target).GetTag();
			}
		}

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CNode::CNode(void)
			: m_pParent(null)
			, m_pNext(null)
			, m_bContent(false)
		{}
		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CNode::CNode(const string& strTag)
			: m_strTag(strTag)
			, m_pParent(null)
			, m_pNext(null)
			, m_bContent(false)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CNode::~CNode(void)
		{
			Delete();
		}

		//----------------------------------------------------------------------
		//	コンテントの設定
		//----------------------------------------------------------------------
		CNode* CNode::SetContent(const string& strContent)
		{
			m_strContent = strContent;
			m_bContent = true;
			return this;
		}
		//----------------------------------------------------------------------
		//	属性の追加
		//----------------------------------------------------------------------
		CNode* CNode::AddAttribute(const string& strTag, const string& strValue)
		{
			if( !strTag.empty() )
			{
				CAttribute NewAttribute(strTag, strValue);
				m_Attributes.push_back(NewAttribute);
			}
			return this;
		}
		//----------------------------------------------------------------------
		//	属性の削除
		//----------------------------------------------------------------------
		CNode* CNode::DeleteAttribute(const string& strTag)
		{
			if( !strTag.empty() )
			{
				u32 uiIndex = linear_search(strTag, &m_Attributes[0], m_Attributes.size(), utility::compareAttributeTag);
				m_Attributes.erase(uiIndex);
			}
			return this;
		}
		//----------------------------------------------------------------------
		//	子ノードの追加
		//----------------------------------------------------------------------
		CNode* CNode::AddChild(const string& strTag)
		{
			CNode* pNode = new CNode(strTag);
			pNode->m_pParent = this;
			if( !m_pChildren.empty() )
			{
				m_pChildren.back()->m_pNext = pNode;
			}
			m_pChildren.push_back(pNode);
			return pNode;
		}
		//----------------------------------------------------------------------
		//	ノードの削除
		//----------------------------------------------------------------------
		void CNode::Delete(void)
		{
			u32 uiCount = m_pChildren.size();
			for( u32 i=0; i < uiCount; i++ )
			{
				if( m_pChildren[i] != null )
				{
					m_pChildren[i]->Delete();
					delete m_pChildren[i];
				}
			}
			m_pChildren.clear();
		}

		//----------------------------------------------------------------------
		//	属性の有無を取得
		//----------------------------------------------------------------------
		b8 CNode::HasAttribute(const string& strTag) const
		{
			if( !strTag.empty() && !m_Attributes.empty() )
			{
				u32 uiIndex = linear_search(strTag, &m_Attributes[0], m_Attributes.size(), utility::compareAttributeTag);
				return uiIndex != NotFind;
			}
			return false;
		}
		//----------------------------------------------------------------------
		//	属性の検索
		//----------------------------------------------------------------------
		const string& CNode::FindAttribute(const string& strTag) const
		{
			if( !strTag.empty() )
			{
				u32 uiIndex = linear_search(strTag, &m_Attributes[0], m_Attributes.size(), utility::compareAttributeTag);
				if( uiIndex != NotFind )
				{
					return m_Attributes[uiIndex].GetValue();
				}
			}
			static string None = "";
			return None;
		}
		//----------------------------------------------------------------------
		//	子ノードの検索
		//----------------------------------------------------------------------
		const CNode* CNode::FindChildNode(const string& strTag) const
		{
			if (!strTag.empty() && !m_pChildren.empty())
			{
				u32 uiIndex = linear_search(strTag, &m_pChildren[0], m_pChildren.size(), utility::compareNodeTag);
				if (uiIndex != NotFind)
				{
					return m_pChildren[uiIndex];
				}
			}
			return null;
		}
		//----------------------------------------------------------------------
		//	子ノードの検索
		//----------------------------------------------------------------------
		CNode* CNode::FindChildNode(const string& strTag)
		{
			return const_cast<CNode*>(const_cast<const CNode*>(this)->FindChildNode(strTag));
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
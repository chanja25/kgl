//----------------------------------------------------------------------
//!
//!	@file	kgl.xml.cpp
//!	@brief	XML関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Data/xml/kgl.xml.h"
#include "File/kgl.File.h"


namespace kgl
{
	namespace Xml
	{
		namespace utility
		{
			//----------------------------------------------------------------------
			//	タグの開始・終了が定義されているか？
			//----------------------------------------------------------------------
			static b8 IsTagOperator(const string& strInput)
			{
				if( strInput == "<" ||
					strInput == ">" ||
					strInput == "</" ||
					strInput == "/>" )
				{
					return true;
				}
				return false;
			}
			//----------------------------------------------------------------------
			//	指定された文字が正しいタグかチェックする
			//----------------------------------------------------------------------
			static b8 CheckTagOperator(const string& strInput, u32 uiIndex)
			{
				if( strInput[uiIndex] == '/' )
				{
					if( uiIndex < strInput.length() - 1 && strInput[uiIndex + 1] == '>' )
					{
						return true;
					}
					else if( uiIndex > 0 && strInput[uiIndex - 1] == '<' )
					{
						return true;
					}
				}
				else if( strInput[uiIndex] == '<' || strInput[uiIndex] == '>' )
				{
					return true;
				}
				return false;
			}
		}

		class CXml
			: public IXml
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CXml(void)
				: m_pRoot(null), m_bFailed(false)
			{}
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CXml(const c8* pPath)
				: m_pRoot(null), m_bFailed(false)
			{
				Create(pPath);
			}
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CXml(const void* pBuffer, u32 uiSize)
				: m_pRoot(null), m_bFailed(false)
			{
				Create(pBuffer, uiSize);
			}

			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CXml(void)
			{
				Delete();
			}

		public:
			//----------------------------------------------------------------------
			//	ファイルからXMLノードの作成
			//----------------------------------------------------------------------
			b8 Create(const c8* pPath)
			{
				TKGLPtr<File::IFile> pFile;
				File::Create(pFile.GetReference());

				m_strError.clear();
				if( !pFile->Open(pPath) )
				{
					m_strError = "ファイルが開けませんでした(Create)\n";
					m_strError += "Path[";
					m_strError += pPath;
					m_strError += "]";
					return false;
				}
				u32 uiSize = pFile->GetSize();
				TSmartPtr<c8> pBuffer = new c8[uiSize * 2];

				pFile->Read(pBuffer, uiSize);
				pFile->Close();

				uiSize *= 2;
				CharSet::ConvertUTF8ToShiftJIS(pBuffer, pBuffer, &uiSize);

				return Create(pBuffer, uiSize);
			}
			//----------------------------------------------------------------------
			//	バッファからXMLノードの作成
			//----------------------------------------------------------------------
			b8 Create(const void* pBuffer, u32 uiSize)
			{
				m_strError.clear();
				if( pBuffer == null || uiSize == 0 )
				{
					m_strError = "指定されたバッファが空です(Create)\n";
					return false;
				}
				Delete();

				string strXml((const c8*)pBuffer, uiSize);

				vector<string> Lines;
				Split(Lines, strXml.c_str(), '\n');

				PreProcessInput(Lines);

				vector<string> Tokens = Tokenize(Lines);

				CreateNodes(Tokens);

				if( m_bFailed )
				{
					Delete();
				}
				return !m_bFailed;
			}
			//----------------------------------------------------------------------
			//	XMLノードの作成
			//----------------------------------------------------------------------
			CNode* CreateRoot(string strTag)
			{
				m_strError.clear();
				if( strTag.empty() )
				{
					m_strError = "指定されたタグが空文字です(Create)\n";
					return false;
				}
				Delete();
				m_pRoot = new CNode(strTag);
				return m_pRoot;
			}
			//----------------------------------------------------------------------
			//	ノードの削除
			//----------------------------------------------------------------------
			void Delete(void)
			{
				if( m_pRoot != null )
				{
					m_pRoot->Delete();
					delete m_pRoot;
					m_pRoot = null;
				}
			}

			//----------------------------------------------------------------------
			//	XMLデータの保存
			//----------------------------------------------------------------------
			b8 Save(const c8* pPath)
			{
				string strXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" EOL;

				vector<const CNode*> pNodeStack;
				const c8 Tab[] = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
				auto Indent = &Tab[ArrayCount(Tab) - 1];

				b8 bInContent = false;
				const auto* pCurrent = GetRoot();
				while( pCurrent )
				{
					//	子階層の出力
					if(auto pChild = pCurrent->GetFirstChild())
					{
						bInContent = false;
						pNodeStack.push_back(pCurrent);
						auto& Attributes = pCurrent->GetAttributes();

						strXml += Indent;
						strXml += "<" + pCurrent->GetTag();
						for( u32 i=0; i < Attributes.size(); i++ )
						{
							strXml += " " + Attributes[i].GetTag() + "=\"" + Attributes[i].GetValue() + "\"";
						}
						strXml += ">" EOL;

						pCurrent = pChild;
						-- Indent;
						continue;
					}
					auto& strTag = pCurrent->GetTag();
					auto& Attributes = pCurrent->GetAttributes();
					auto& strContent = pCurrent->GetContent();
					b8 bMultiLine = strContent.contains('\r') || strContent.contains('\n');
					//	ノードの出力
					if( !strTag.empty() && CharSet::IsEOL(strXml[strXml.length()-1]) )
					{
						strXml += Indent;
					}
					if( !strTag.empty() )
					{
						strXml += "<" + strTag;
						for( u32 i=0; i < Attributes.size(); i++ )
						{
							strXml += " " + Attributes[i].GetTag() + "=\"" + Attributes[i].GetValue() + "\"";
						}
					}
					if( bMultiLine )
					{
						if( !strTag.empty() )
						{
							strXml += ">" EOL;
							-- Indent;
						}
						vector<string> Contents;
						Split(Contents, strContent.c_str(), EOL, false);
						for( u32 i=0; i < Contents.size() - 1; i++ )
						{
							strXml += Indent + Contents[i] + EOL;
						}
						if( !Contents[Contents.size()-1].empty() )
						{
							strXml += Indent + Contents[Contents.size()-1];
						}
						if( !strTag.empty() )
						{
							++ Indent;
							strXml += Indent;
							strXml += "</" + strTag + ">" EOL;
						}
					}
					else
					{
						if( !strTag.empty() )
						{
							strXml += ">" + strContent + "</" + strTag + ">" EOL;
						}
						else
						{
							strXml += strContent;
						}
					}
					pCurrent = pCurrent->GetNext();
					//	ノードの終端を出力
					while( pCurrent == null && !pNodeStack.empty() )
					{
						++ Indent;
						pCurrent = pNodeStack.back();
						pNodeStack.pop_back();

						strXml += Indent;
						strXml += "</" + pCurrent->GetTag() + ">" EOL;
						pCurrent = pCurrent->GetNext();
					}
				}
				//	ファイルの出力
				TKGLPtr<File::IFile> pFile;
				File::Create(pFile.GetReference());
				if( !pFile->Open(pPath, File::EOpen::Write, 0, File::ECreate::CreateAlways) )
				{
					m_strError = "ファイルの書き込みに失敗しました(Save)\n";
					m_strError += "Path[";
					m_strError += pPath;
					m_strError += "]";
					return false;
				}
				u32 uiSize = strXml.size() * 2;
				TSmartPtr<s8> pUTF8 = new s8[uiSize];
				CharSet::ConvertShiftJISToUTF8(pUTF8, strXml.c_str(), &uiSize);

				pFile->Write(pUTF8, uiSize - 1);
				pFile->Close();
				return true;
			}
		public:
			//----------------------------------------------------------------------
			//	ルートノードの取得
			//----------------------------------------------------------------------
			CNode* GetRoot(void)
			{
				return m_pRoot;
			}
			//----------------------------------------------------------------------
			//	エラーの有無を取得
			//----------------------------------------------------------------------
			b8 IsFailed(void)
			{
				return !!m_bFailed;
			}
			//----------------------------------------------------------------------
			//	エラーメッセージの取得
			//----------------------------------------------------------------------
			const c8* GetError(void)
			{
				return m_strError.c_str();
			}

		private:
			//----------------------------------------------------------------------
			//	コメント等の不要な文を削除
			//----------------------------------------------------------------------
			void PreProcessInput(vector<string>& Input)
			{
				u32 uiCount = Input.size();
				//	空白の削除
				for( u32 i=0; i < uiCount; i++ )
				{
					u32 uiNum = 0;
					for( u32 j=0; j < Input[i].length(); j++ )
					{
						if( !CharSet::IsWhiteSpace(Input[i][j]) ||
							CharSet::IsMultiByteChar(Input[i][j]) )
						{
							break;
						}
						++ uiNum;
					}
					if( uiNum > 0 )
					{
						Input[i] = Input[i].substr(uiNum);
					}
				}
				//	定型文の削除
				for( u32 i=0; i < uiCount; i++ )
				{
					if( Input[i].startwith("<!DOCTYPE") || Input[i].startwith("<?xml") )
					{
						Input[i] = "";
					}
				}
				b8 bComment = false;
				u32 uiCommentLineStart, uiCommentIndexStart;
				uiCommentLineStart = uiCommentIndexStart = 0;
				//	コメントの削除
				for( u32 iLine=0; iLine < uiCount; iLine++ )
				{
					const auto strInput = Input[iLine];
					if( strInput.length() == 3 )
					{
						if( bComment )
						{
							if( strInput[0] == '-' && strInput[1] == '-' && strInput[2] == '>' )
							{
								bComment = false;
								u32 uiCommentLineEnd = iLine;
								u32 uiCommentIndexEnd = 2;
								WhiteOut(Input, uiCommentLineStart, uiCommentLineEnd, uiCommentIndexStart, uiCommentIndexEnd);
							}
						}
					}
					for( u32 iIdx=0; iIdx + 3 < strInput.length(); iIdx++ )
					{
						if( !bComment )
						{
							if( strInput[iIdx] == '<' && strInput[iIdx+1] == '!' && strInput[iIdx+2] == '-' && strInput[iIdx+3] == '-' )
							{
								bComment = true;
								uiCommentLineStart = iLine;
								uiCommentIndexStart = iIdx;
							}
						}
						else
						{
							if( (strInput[iIdx]   == '<' && strInput[iIdx+1] == '!' && strInput[iIdx+2] == '-') ||
								(strInput[iIdx+1] == '<' && strInput[iIdx+2] == '!' && strInput[iIdx+3] == '-') )
							{
								bComment = false;
								u32 uiCommentLineEnd = iLine;
								u32 uiCommentIndexEnd = iIdx + 3;
								WhiteOut(Input, uiCommentLineStart, uiCommentLineEnd, uiCommentIndexStart, uiCommentIndexEnd);
							}
						}
					}
				}
			}
			//----------------------------------------------------------------------
			//	指定範囲を空白で埋める
			//----------------------------------------------------------------------
			void WhiteOut(vector<string>& Input, u32 uiLineStart, u32 uiLineEnd, u32 uiIndexStart, u32 uiIndexEnd)
			{
				if( uiLineEnd - uiLineStart > 0 )
				{
					for( u32 i=uiIndexStart; i < Input[uiLineStart].length(); i++ )
					{
						Input[uiLineStart][i] = ' ';
					}
				}
				if( uiLineEnd - uiLineStart > 1 )
				{
					u32 uiNumLines = uiLineEnd - uiLineStart - 1;
					u32 uiMidStart = uiLineStart + 1;
					for( u32 i=0; i < uiNumLines; i++ )
					{
						Input[uiMidStart + i] = "";
					}
				}
				if( uiLineEnd - uiLineStart > 0 )
				{
					for( u32 i=0; i <= uiIndexEnd; i++ )
					{
						Input[uiLineEnd][i] = ' ';
					}
				}
				if( uiLineEnd == uiLineStart )
				{
					for( u32 i=uiIndexStart; i <= uiIndexEnd; i++ )
					{
						Input[uiLineEnd][i] = ' ';
					}
				}
			}
			//----------------------------------------------------------------------
			//	トークン解析
			//----------------------------------------------------------------------
			vector<string> Tokenize(string& strInput)
			{
				vector<string> Tokens;
				string strWork;
				enum TOKENTYPE { OPERATOR, STRING, NONE } eType = NONE;

				b8 bInToken = false;
				b8 bInQuote = false;
				b8 bInOperator = false;
				for( u32 i=0; i < strInput.length(); i++ )
				{
					if( CharSet::IsWhiteSpace(strInput[i]) &&
						!CharSet::IsMultiByteChar(strInput[i]) &&
						!bInQuote &&
						bInOperator )
					{
						if( strWork.length() )
						{
							Tokens.push_back(strWork);
							strWork.clear();
						}
						bInToken = false;
						eType = NONE;
						continue;
					}

					if( !bInToken )
					{
						strWork.clear();
						strWork += strInput[i];
						if( CharSet::IsMultiByteChar(strInput[i]) )
						{
							strWork += strInput[++ i];
						}
						bInToken = true;
						if( utility::CheckTagOperator(strInput, i) )
						{
							eType = OPERATOR;
							bInOperator = true;
						}
						else
						{
							eType = STRING;
						}
					}
					else
					{
						if( eType == OPERATOR )
						{
							if( utility::CheckTagOperator(strInput, i) )
							{
								strWork += strInput[i];
							}
							else
							{
								if( strWork.length() > 0 )
								{
									Tokens.push_back(strWork);
									strWork.clear();
								}
								strWork += strInput[i];
								if( CharSet::IsMultiByteChar(strInput[i]) )
								{
									strWork += strInput[++ i];
								}
								eType = STRING;
							}
						}
						else
						{
							if( strInput[i] == '"' && !bInQuote )
							{
								bInQuote = true;
							}
							else if( strInput[i] == '"' && bInQuote )
							{
								bInQuote = false;
							}

							if( bInQuote || !utility::CheckTagOperator(strInput, i) )
							{
								strWork += strInput[i];
								if( CharSet::IsMultiByteChar(strInput[i]) )
								{
									strWork += strInput[++ i];
								}
							}
							else
							{
								if( strWork.length() > 0 )
								{
									Tokens.push_back(strWork);
									strWork.clear();
								}
								strWork += strInput[i];
								eType = OPERATOR;
								bInOperator = true;
							}
						}
					}
					if( strWork.length() > 0 )
					{
						if( strWork[strWork.length()-1] == '>' )
						{
							Tokens.push_back(strWork);
							strWork.clear();
							bInToken = false;
							bInOperator = false;
							eType = NONE;
						}
					}
				}

				if( strWork.length() > 0 || Tokens.size() == 0 )
				{
					Tokens.push_back(strWork);
				}
				return Tokens;
			}
			//----------------------------------------------------------------------
			//	トークン解析
			//----------------------------------------------------------------------
			vector<string> Tokenize(vector<string>& Input)
			{
				vector<string> Tokens;
				for( u32 i=0; i < Input.size(); i++ )
				{
					Tokens += Tokenize(Input[i]);
				}
				return Tokens;
			}
			//----------------------------------------------------------------------
			//	属性の追加
			//----------------------------------------------------------------------
			void AddAttribute(vector<CAttribute>& Out, const string& strToken)
			{
				vector<string> Tokens;
				if( strToken.contains('=') )
				{
					Split(Tokens, strToken.c_str(), '=');
					if( Tokens.size() == 2 )
					{
						string Value = TrimQuotes(Tokens[1]);

						Out.push_back(CAttribute(Tokens[0], Value));
					}
				}
			}
			//----------------------------------------------------------------------
			//	ノードの生成(再帰処理)
			//----------------------------------------------------------------------
			CNode* CreateNodeRecursive(const vector<string>& Tokens, u32 uiStart, u32* pNextIndex = null)
			{
				u32 uiSavedIndex = uiStart;

				string strTag;
				vector<CAttribute> Attributes;
				b8 bInTag = false;

				for( u32 i=uiStart; i < Tokens.size(); i++ )
				{
					if( m_bFailed )
					{
						break;
					}
					auto& strToken = Tokens[i];
					if( strToken.length() == 0 || strToken == "\n" || strToken == "\r" || strToken == EOL )
					{
						continue;
					}
					if( !bInTag )
					{
						if( strToken == "<" )
						{
							bInTag = true;
						}
						else
						{
							m_bFailed = true;
							m_strError = "XMLファイルが壊れているか対応していないフォーマットです(CreateNodeRecursive)\n";

						}
					}
					else
					{
						if( !utility::IsTagOperator(strToken) )
						{
							if( strTag.empty() )
							{
								strTag = strToken;
							}
							else
							{
								AddAttribute(Attributes, strToken);
							}
						}
						else
						{
							if( strToken == ">" )
							{
								uiSavedIndex = i + 1;
								break;
							}
							else if( strToken == "/>" )
							{
								CNode* pNewNode = new CNode(strTag);
								pNewNode->m_Attributes = Attributes;
								*pNextIndex = i + 1;
								return pNewNode;
							}
							else
							{
								m_bFailed = true;
								m_strError = "XMLファイルが壊れているか対応していないフォーマットです(CreateNodeRecursive)\n";
							}
						}
					}
				}
				CNode* pNewNode = new CNode(strTag);
				pNewNode->m_Attributes = Attributes;

				string strContent;
				string strFinalTag;
				b8 bInContent = false;
				bInTag = false;
				for( u32 i=uiSavedIndex; i < Tokens.size(); i++ )
				{
					if( m_bFailed )
					{
						break;
					}
					auto& strToken = Tokens[i];
					if( uiSavedIndex != i &&
						!bInContent && 
						(strToken.length() == 0 ||
							strToken == "\n" ||
							strToken == "\r" ||
							strToken == EOL) )
					{
						continue;
					}
					if( !bInTag )
					{
						if( strToken == "<" )
						{
							vector<string> Temp;
							Split(Temp, strContent.c_str(), EOL);
							if( !Temp.empty() )
							{
								CNode* pContent = new CNode();
								pContent->SetContent(strContent);
								pNewNode->m_bContent = true;
								pNewNode->m_pChildren.push_back(pContent);
							}
							strContent.clear();
							CNode* pChild = CreateNodeRecursive(Tokens, i, &uiSavedIndex);

							if( pChild )
							{
								pNewNode->m_pChildren.push_back(pChild);
							}
							else
							{
								m_bFailed = true;
								m_strError = "XMLファイルが壊れているか対応していないフォーマットです(CreateNodeRecursive)\n";
							}
							i = uiSavedIndex - 1;
							continue;
						}
						else if( strToken == "</" )
						{
							bInTag = true;
						}
						else if( utility::IsTagOperator(strToken) )
						{
							m_bFailed = true;
							m_strError = "XMLファイルが壊れているか対応していないフォーマットです(CreateNodeRecursive)\n";
						}
						else
						{
							if( strToken.empty() )
							{
								strContent += EOL;
							}
							else
							{
								if( strToken.contains(EOL) )
								{
									strContent += strToken;
								}
								else if( strToken.contains('\r') || strToken.contains('\n') )
								{
									strContent += strToken.substr(0, strToken.length() - 1) + EOL;
								}
								else
								{
									strContent += strToken;
								}
							}
							bInContent = true;
						}
					}
					else
					{
						if( !utility::IsTagOperator(strToken) )
						{
							if( strFinalTag.length() > 0 )
							{
								// 既に設定されているので無視
							}
							else
							{
								strFinalTag = strToken;
							}
						}
						else
						{
							if( strToken == ">" )
							{
								if( strTag != strFinalTag )
								{
									m_bFailed = true;
									m_strError = "XMLファイルが壊れているか対応していないフォーマットです(CreateNodeRecursive)\n";
								}

								uiSavedIndex = i + 1;
								if( pNextIndex )
								{
									*pNextIndex = uiSavedIndex;
								}
								vector<string> Temp;
								Split(Temp, strContent.c_str(), EOL);
								if( !Temp.empty() )
								{
									if( pNewNode->m_pChildren.empty() )
									{
										pNewNode->SetContent(strContent);
									}
									else
									{
										CNode* pChild = new CNode();
										pChild->SetContent(strContent);
										pNewNode->m_bContent = true;
										pNewNode->m_pChildren.push_back(pChild);
									}
								}
								return pNewNode;
							}
							else
							{
								m_bFailed = true;
								m_strError = "XMLファイルが壊れているか対応していないフォーマットです(CreateNodeRecursive)\n";
							}
						}
					}
				}

				delete pNewNode;
				return null;
			}
			//----------------------------------------------------------------------
			//	子階層の接続
			//----------------------------------------------------------------------
			void HookUpNextPtrs(CNode* pNode)
			{
				if( pNode == null )
				{
					return;
				}
				for( u32 i=0; i < pNode->m_pChildren.size(); i++ )
				{
					HookUpNextPtrs(pNode->m_pChildren[i]);
					if( i != pNode->m_pChildren.size()-1 )
					{
						pNode->m_pChildren[i]->m_pParent = pNode;
						pNode->m_pChildren[i]->m_pNext = pNode->m_pChildren[i + 1];
					}
				}
			}
			//----------------------------------------------------------------------
			//	ノードの生成
			//----------------------------------------------------------------------
			void CreateNodes(const vector<string>& Tokens)
			{
				m_bFailed = false;
				CNode* pRoot = CreateNodeRecursive(Tokens, 0);

				if( pRoot )
				{
					HookUpNextPtrs(pRoot);
					m_pRoot = pRoot;
				}
				else
				{
					m_bFailed = false;
					m_strError = "XMLファイルのノード解析に失敗しました(CreateNodes)\n";
				}
			}

		private:
			string		m_strError;
			CNode*		m_pRoot;
			bool		m_bFailed;
		};

		//----------------------------------------------------------------------
		//	XML管理の生成
		//----------------------------------------------------------------------
		b8 Create(IXml** ppXml)
		{
			*ppXml = new CXml();
			(*ppXml)->AddRef();
			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
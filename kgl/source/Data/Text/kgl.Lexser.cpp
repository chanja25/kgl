//----------------------------------------------------------------------
//!
//!	@file	kgl.Lexser.cpp
//!	@brief	字句解析
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Data/text/kgl.Lexser.h"
#include "Manager/kgl.GameManager.h"

namespace kgl
{
	namespace Text
	{
		namespace Extra
		{
			//!	特殊なトークン
			static const c8* pExtraTokens[] =
			{
				"++", "+=", null,
				"--", "-=", null,
				"*=", null,
				"/=", null,
				"%=", null,
				"&&", "&=", null,
				"||", "|=", null,
				"^=", null,
				"~=", null,
				"==", null,
				"<<", "<=", "<<=", null,
				">>", ">=", ">>=", null,
				"!=", null,
				"::", null,
			};
			//!	特殊なトークン
			static const c8** pExtraTokenList[256];
			//!	初期化フラグ
			static b8 bInitialized = false;

			//----------------------------------------------------------------------
			//	特殊なトークンのリスト作成
			//----------------------------------------------------------------------
			INLINE void SetupExtraToken(void)
			{
				if( bInitialized )return;
				bInitialized = true;

				s32 iCount = ArrayCount(pExtraTokens);
				for( s32 i=0; i < iCount; i++ )
				{
					if( pExtraTokens[i] != null )
					{
						c8 c = pExtraTokens[i][0];
						if( pExtraTokenList[c] == null )
						{
							pExtraTokenList[c] = &pExtraTokens[i];
						}
					}
				}
			}
		}

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CLexer::CLexer(void)
			: m_pText(null)
			, m_uiSize(0)
			, m_uiIndex(0)
			, m_uiLine(0)
			, m_uiPrevIndex(0)
			, m_uiPrevLine(0)
			, m_pRoot(null)
			, m_pEnd(null)
		{
			Extra::SetupExtraToken();
		}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CLexer::~CLexer(void)
		{
			Release();
		}

		//----------------------------------------------------------------------
		//	読み込み
		//----------------------------------------------------------------------
		b8 CLexer::LoadText(const c8* pFileName)
		{
			TKGLPtr<File::IFile> pFile;
			File::Create(pFile.GetReference());

			if( !pFile->Open(pFileName) )
			{
				return false;
			}

			m_uiSize		= pFile->GetSize();
			m_pText			= new c8[m_uiSize+1];

			pFile->Read(m_pText, m_uiSize);
			m_pText[m_uiSize] = '\0';

			m_uiIndex		= 0;
			m_uiLine		= 1;
			m_uiPrevIndex	= 0;
			m_uiPrevLine	= 1;
			m_bError		= false;
			m_pRoot			= null;
			m_pEnd			= null;

			pFile->Close();
			return true;
		}
		//----------------------------------------------------------------------
		//	テキスト設定
		//----------------------------------------------------------------------
		b8 CLexer::SetText(const c8* pText, u32 uiSize)
		{
			if( pText == null )return false;

			m_pText = new c8[uiSize+1];
			kgMemcpy(m_pText, pText, uiSize);
			m_pText[uiSize] = '\0';

			m_uiSize		= uiSize;
			m_uiIndex		= 0;
			m_uiLine		= 1;
			m_uiPrevIndex	= 0;
			m_uiPrevLine	= 1;
			m_bError		= false;
			m_pRoot			= null;
			m_pEnd			= null;

			return true;
		}

		//----------------------------------------------------------------------
		//	解放
		//----------------------------------------------------------------------
		void CLexer::Release(void)
		{
			if( m_pText == null )return;

			SafeDeleteArray(m_pText);

			m_uiSize	= 0;
			m_bError	= false;

			Token* pToken = m_pRoot;
			Token* pNext;
			while( pToken )
			{
				pNext = pToken->pNext;

				SafeDeleteArray(pToken->pToken);
				SafeDelete(pToken);

				pToken = pNext;
			}
			m_pRoot	= null;
			m_pEnd	= null;
		}

		//----------------------------------------------------------------------
		//	トークンを全て解析
		//----------------------------------------------------------------------
		const Token* CLexer::AnalyzeAll(void)
		{
			if( m_pText == null )return null;
			if( m_bError )return null;

			if( m_pRoot == null )
			{
				m_pRoot = NextToken();
				if( m_pRoot == null )
				{
					return null;
				}
				m_pEnd = m_pRoot;
			}

			Token* pToken = m_pEnd;
			Token* pNext;
			while( (pNext = NextToken()) != null )
			{
				pToken->pNext = pNext;
				pToken = pNext;
			}
			m_pEnd = pToken;

			return m_pRoot;
		}

		//----------------------------------------------------------------------
		//	次のトークンを解析
		//----------------------------------------------------------------------
		const Token* CLexer::AnalyzeToken(void)
		{
			if( m_pText == null )return null;
			if( m_bError )return null;

			if( m_pRoot == null )
			{
				m_pRoot = NextToken();
				m_pEnd	= m_pRoot;
				return m_pRoot;
			}

			Token* pToken	= m_pEnd;
			Token* pNext	= NextToken();
			if( pNext != null )
			{
				pToken->pNext = pNext;
				m_pEnd = pNext;
			}

			return pNext;
		}
		//----------------------------------------------------------------------
		//	トークンの取得
		//----------------------------------------------------------------------
		Token* CLexer::NextToken(void)
		{
			c8 c = m_pText[m_uiIndex];
			//	文字が来るまで進める
			while( c == ' ' || c == '\t' || c == '\r' || c == '\n' )
			{
				if( c == '\r' )
				{
					m_uiLine ++;
					m_uiIndex += (m_pText[m_uiIndex+1] == '\n')? 1: 0;
				}
				else if( c == '\n' )
				{
					m_uiLine ++;
				}
				c = m_pText[++ m_uiIndex];
			}
			m_uiPrevIndex	= m_uiIndex;
			m_uiPrevLine	= m_uiLine;

			s32 iType = CharType(c);
			Token* pToken = null;
			switch( iType )
			{
			case EType::AlNum:		pToken = NextTokenAlnum();		break;
			case EType::Number:		pToken = NextTokenNumber();		break;
			case EType::String:		pToken = NextTokenString();		break;
			case EType::AlNumString:pToken = NextTokenAlnumString();break;
			case EType::Operator:	pToken = NextTokenOperator();	break;
			case EType::EscapeSeq:	pToken = NextTokenEscapeSeq();	break;
			}
			return pToken;
		}

		//----------------------------------------------------------------------
		//	トークンの取得(英数字)
		//----------------------------------------------------------------------
		Token* CLexer::NextTokenAlnum(void)
		{
			b8 bError = false;
			u32 uiLength = 0;
			c8 c = m_pText[m_uiIndex];
			do
			{
				kgAssert(uiLength < TokenLenMax, "トークンの文字数が最大数を超えました");
				m_cToken[uiLength ++] = c;
				c = m_pText[++ m_uiIndex];
			}
			while( IsAlnum(c) );

			m_cToken[uiLength] = '\0';
			return CreateToken(EType::AlNum, m_cToken, uiLength, bError);
		}
		//----------------------------------------------------------------------
		//	トークンの取得(数字)
		//----------------------------------------------------------------------
		Token* CLexer::NextTokenNumber(void)
		{
			b8 bError = false;
			u32 uiLength = 0;
			c8 c = m_pText[m_uiIndex];

			do
			{
				kgAssert(uiLength < TokenLenMax, "トークンの文字数が最大数を超えました");
				m_cToken[uiLength ++] = c;
				c = m_pText[++ m_uiIndex];
			}
			while( (c >= '0' && c <= '9') || ( c == '.' ) ||
				   (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f') );

			m_cToken[uiLength] = '\0';
			return CreateToken(EType::Number, m_cToken, uiLength, bError);
		}
		//----------------------------------------------------------------------
		//	トークンの取得(文字列)
		//----------------------------------------------------------------------
		Token* CLexer::NextTokenString(void)
		{
			b8 bError = false;
			//	["]か[']
			c8 cExit = m_pText[m_uiIndex];

			u32 uiLength = 0;
			c8 c = m_pText[++ m_uiIndex];
			do
			{
				if( c == '\\' )
				{
					c8 cNext = m_pText[m_uiIndex+1];
					if( cNext == '\r' )
					{
						m_uiLine ++;
						m_uiIndex += (m_pText[m_uiIndex+2] == '\n')? 3: 2;
						c = m_pText[m_uiIndex];
					}
					else if( cNext == '\n' )
					{
						m_uiLine ++;
						m_uiIndex += 2;
						c = m_pText[m_uiIndex];
					}
					else if( cNext == cExit )
					{
						m_uiIndex += 2;
						m_cToken[uiLength ++] = cNext;
						c = m_pText[m_uiIndex];
					}
				}
				//	エラー(文字列の終端がない)
				if( c == '\r' || c == '\n' || c == '\0' )
				{
					bError = true;
					break;
				}

				kgAssert(uiLength < TokenLenMax, "トークンの文字数が最大数を超えました");
				m_cToken[uiLength ++] = c;
				c = m_pText[++ m_uiIndex];
			}
			while( c != cExit );

			m_uiIndex ++;
			m_cToken[uiLength] = '\0';
			return CreateToken(EType::String, m_cToken, uiLength, bError);
		}
		//----------------------------------------------------------------------
		//	トークンの取得(全角含む文字列+英数字)
		//----------------------------------------------------------------------
		Token* CLexer::NextTokenAlnumString(void)
		{
			b8 bError = false;
			u32 uiLength = 0;
			c8 c = m_pText[m_uiIndex];
			do
			{
				if( CharSet::IsMultiByteChar(c) )
				{
					kgAssert(uiLength < TokenLenMax, "トークンの文字数が最大数を超えました");
					m_cToken[uiLength ++] = c;
					c = m_pText[++ m_uiIndex];
				}
				kgAssert(uiLength < TokenLenMax, "トークンの文字数が最大数を超えました");
				m_cToken[uiLength ++] = c;
				c = m_pText[++ m_uiIndex];
			}
			while( CharType(c) != EType::Unkown );

			m_cToken[uiLength] = '\0';
			return CreateToken(EType::AlNumString, m_cToken, uiLength, bError);
		}
		//----------------------------------------------------------------------
		//	トークンの取得(オペレータ)
		//----------------------------------------------------------------------
		Token* CLexer::NextTokenOperator(void)
		{
			b8 bError = false;
			u32 uiLength = 0;
			c8 c = m_pText[m_uiIndex];

			if( c == '/' )
			{
				//	コメント(//)
				if( m_pText[m_uiIndex+1] == '/' )
				{
					m_uiIndex ++;
					do
					{
						c = m_pText[++ m_uiIndex];
						if( c == '\\' )
						{
							c = m_pText[++ m_uiIndex];
							if( c == '\r' )
							{
								m_uiLine ++;
								m_uiIndex += (m_pText[m_uiIndex+1] == '\n')? 1: 0;
								continue;
							}
							else if( c == '\n' )
							{
								m_uiLine ++;
								continue;
							}
						}
					}
					while( c != '\n' && c != '\r' && c != '\0' );

					if( c == '\r' )
					{
						m_uiLine ++;
						m_uiIndex += (m_pText[m_uiIndex+1] == '\n')? 2: 1;
					}
					else if( c == '\n' )
					{
						m_uiLine ++;
						m_uiIndex ++;
					}
					return NextToken();
				}
				//	コメント(/**/)
				else if( m_pText[m_uiIndex+1] == '*' )
				{
					m_uiIndex ++;
					do
					{
						c = m_pText[++ m_uiIndex];
						if( c == '\r' )
						{
							m_uiLine ++;
							m_uiIndex += (m_pText[m_uiIndex+1] == '\n')? 1: 0;
						}
						else if( c == '\n' )
						{
							m_uiLine ++;
						}
						else if( c == '\0' )
						{
							//	エラー(コメントの終端がない)
							bError = true;
							break;
						}
					}
					while( c != '*' && m_pText[m_uiIndex+1] != '/' );

					m_uiIndex += 3;
					return NextToken();
				}
			}
			//	少数値
			if( c == '.' )
			{
				if( IsNumber(m_pText[m_uiIndex+1]) )
				{
					return NextTokenNumber();
				}
			}

			m_cToken[uiLength ++] = c;
			const c8** pExtraTokenList = Extra::pExtraTokenList[c];
			if( pExtraTokenList != null )
			{
				s32 i, j;
				for( i = 0; pExtraTokenList[i] != null; i++ )
				{
					const c8* pExtraToken = pExtraTokenList[i];
					for( j = 1; pExtraToken[j] != '\0'; j++ )
					{
						if( m_pText[m_uiIndex+j] != pExtraToken[j] )
						{
							break;
						}
					}
					if( pExtraToken[j] == '\0' )
					{
						for( j = 1; pExtraToken[j] != '\0'; j++ )
						{
							m_cToken[uiLength ++] = m_pText[++ m_uiIndex];
						}
						break;
					}
				}
			}

			m_uiIndex ++;
			m_cToken[uiLength] = '\0';
			return CreateToken(EType::Operator, m_cToken, uiLength, bError);
		}
		//----------------------------------------------------------------------
		//	トークンの取得(特殊文字)
		//----------------------------------------------------------------------
		Token* CLexer::NextTokenEscapeSeq(void)
		{
			b8 bError = false;
			c8 c = m_pText[m_uiIndex];

			if( c == '\"' || c == '\'' )
			{
				return NextTokenString();
			}
			else if( IsEscapeSeq(c) )
			{
				u32 uiLength = 0;
				c8 c = m_pText[m_uiIndex ++];
				m_cToken[uiLength ++] = c;
				m_cToken[uiLength] = '\0';
				return CreateToken(EType::EscapeSeq, m_cToken, uiLength, bError);
			}
			//	未対応のエスケープシーケンス
			m_bError = true;
			return null;
		}
		//----------------------------------------------------------------------
		//	トークンの生成
		//----------------------------------------------------------------------
		Token* CLexer::CreateToken(s32 iType, const c8* pToken, u32 uiLength, b8 bError)
		{
			c8* pString = new c8[uiLength+1];
			kgMemcpy(pString, pToken, uiLength);
			pString[uiLength] = '\0';

			Token* pNewToken = new Token;
			pNewToken->iType	= iType;
			pNewToken->uiLine	= m_uiLine;
			pNewToken->pToken	= pString;
			pNewToken->pNext	= null;
			pNewToken->bError	= !!bError;

			if( bError )
			{
				m_bError = true;
			}
			return pNewToken;
		}

		//----------------------------------------------------------------------
		//	文字タイプの取得
		//----------------------------------------------------------------------
		s32 CLexer::CharType(c8 c)
		{
			if( IsNumber(c) )
			{
				return EType::Number;
			}
			else if( IsAlnum(c) )
			{
				return EType::AlNum;
			}
			else if( IsOperator(c) )
			{
				return EType::Operator;
			}
			else if( IsEscapeSeq(c) )
			{
				return EType::EscapeSeq;
			}
			else if( CharSet::IsMultiByteChar(c) )
			{
				return EType::AlNumString;
			}
			return EType::Unkown;
		}
		//----------------------------------------------------------------------
		//	英数字か？
		//----------------------------------------------------------------------
		b8 CLexer::IsAlnum(c8 c)
		{
			return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') ||
					(c >= '0' && c <= '9') || c == '_' );
		}
		//----------------------------------------------------------------------
		//	数字か？
		//----------------------------------------------------------------------
		b8 CLexer::IsNumber(c8 c)
		{
			return (c >= '0' && c <= '9');
		}
		//----------------------------------------------------------------------
		//	オペレータか？
		//----------------------------------------------------------------------
		b8 CLexer::IsOperator(c8 c)
		{
			return (c == '=' || c == '+' || c == '-' || c == '*' || c == '/' || c == '%' ||
					c == '&' || c == '|' || c == '^' || c == '~' || c == '!' || c == '?' || 
					c == '<' || c == '>' || c == '(' || c == ')' || c == '[' || c == ']' ||
					c == '{' || c == '}' || c == '#' || c == '@' || c == '$' || c == '`' ||
					c == ':' || c == ';' || c == '.' || c == ',');
		}
		//----------------------------------------------------------------------
		//	特殊文字か？
		//----------------------------------------------------------------------
		b8 CLexer::IsEscapeSeq(c8 c)
		{
			return (c == '\"' || c == '\'');
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
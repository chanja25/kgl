//----------------------------------------------------------------------
//!
//!	@file	kgl.Dll.cpp
//!	@brief	DLL管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Dll/kgl.Dll.h"

namespace kgl
{
	namespace Dll
	{
		//!	DLL情報
		struct DllInfo
		{
			void* hDll;	//!< DLL
		};

		//	Platform Dependent
		//======================================================================
		//!	DLLマネージャークラス
		//======================================================================
		class CDllManager
			: public IDllManager
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CDllManager(s32 iMax)
			: m_pInfo(null), m_iMax(iMax)
			{
				m_pInfo	= new DllInfo[iMax];
			}
			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CDllManager(void)
			{
				//	全てのDLLを解放しておく(保険処理)
				for( s32 i=0; i < m_iMax; i++ )
				{
					Unload(i);
				}

				SafeDeleteArray(m_pInfo);
			}

			//----------------------------------------------------------------------
			//	DLLの読み込み
			//----------------------------------------------------------------------
			s32 Load(const c8* pPath)
			{
				HMODULE hDll = LoadLibraryExA(pPath, null, 0);
				if( hDll == null )
				{
					WarningTrace("[%s]の読み込みに失敗しました(CDllManager::Load)", pPath);
					return -1;
				}
				//	空いているワークにDLLをセットする
				for( s32 i=0; i < m_iMax; i++ )
				{
					if( m_pInfo[i].hDll != null )	continue;

					m_pInfo[i].hDll = hDll;
					return i;
				}

				FreeLibrary(hDll);
				WarningTrace("最大管理数を超えています(CDllManager::Load)");
				return -1;
			}

			//----------------------------------------------------------------------
			//	DLLの解放
			//----------------------------------------------------------------------
			void Unload(s32 iId)
			{
				if( m_pInfo[iId].hDll != null )	return;

				FreeLibrary((HMODULE)m_pInfo[iId].hDll);
				m_pInfo[iId].hDll = null;
			}

			//----------------------------------------------------------------------
			//	関数取得
			//----------------------------------------------------------------------
			FDllFunction GetFunction(s32 iId, const c8* pFunction)
			{
				if( m_pInfo[iId].hDll != null )	return null;

				return (FDllFunction)GetProcAddress((HMODULE)m_pInfo[iId].hDll, pFunction);
			}

		private:
			s32			m_iMax;		//!< 最大管理数
			DllInfo*	m_pInfo;	//!< DLL情報
		};

		//----------------------------------------------------------------------
		//	ファイルマネージャーの生成
		//----------------------------------------------------------------------
		b8 Create(IDllManager** ppManager, s32 iMax)
		{
			*ppManager = new CDllManager(iMax);
			(*ppManager)->AddRef();
			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
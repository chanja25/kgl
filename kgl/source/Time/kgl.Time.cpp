//----------------------------------------------------------------------
//!
//!	@file	kgl.Time.cpp
//!	@brief	時間関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Time/kgl.Time.h"
#include <time.h>

namespace kgl
{
	namespace Time
	{
//	Platform Dependent
#if	WINDOWS
		//----------------------------------------------------------------------
		//	Tickの取得
		//----------------------------------------------------------------------
		u64	Tick(void)
		{
			return (u64)clock();
		}
		//----------------------------------------------------------------------
		//	1秒間に変化するTickの値
		//----------------------------------------------------------------------
		u64	TickPerSecond(void)
		{
			return CLOCKS_PER_SEC;
		}
		//----------------------------------------------------------------------
		//	日時取得
		//----------------------------------------------------------------------
		void DateTime(Date* pDate)
		{
			SYSTEMTIME stTime;
			GetSystemTime(&stTime);

			pDate->usYeah			= (u16)stTime.wYear;
			pDate->usMonth			= (u16)stTime.wMonth;
			pDate->usDay			= (u16)stTime.wDay;
			pDate->usWeek			= (u16)stTime.wDayOfWeek;
			pDate->usHour			= (u16)stTime.wHour;
			pDate->usMinute			= (u16)stTime.wMinute;
			pDate->usSecond			= (u16)stTime.wSecond;
			pDate->usMilliSecond	= (u16)stTime.wMilliseconds;
		}

		//----------------------------------------------------------------------
		//!	ファイルタイムから日時取得
		//!	@param pData	[out] 日時情報
		//!	@param pTime	[in]  ファイルの日時情報
		//----------------------------------------------------------------------
		void FileTimeToDate(Date* pDate, const FILETIME* pTime)
		{
			SYSTEMTIME stTime;
			FileTimeToSystemTime(pTime, &stTime);

			pDate->usYeah			= (u16)stTime.wYear;
			pDate->usMonth			= (u16)stTime.wMonth;
			pDate->usDay			= (u16)stTime.wDay;
			pDate->usWeek			= (u16)stTime.wDayOfWeek;
			pDate->usHour			= (u16)stTime.wHour;
			pDate->usMinute			= (u16)stTime.wMinute;
			pDate->usSecond			= (u16)stTime.wSecond;
			pDate->usMilliSecond	= (u16)stTime.wMilliseconds;
		}
#else	// ~#if WINDOWS
		u64	Tick(void){ return 0; }
		u64	TickPerSecond(void){ return 1; }
		void DateTime(Date*){}
		void FileTimeToDate(Date*, const FILETIME*){}
#endif	// ~#if WINDOWS


		//	Platform Dependent
#if	WINDOWS
		//======================================================================
		//	タイマークラス
		//======================================================================
		class CStopwatch
			: public IStopwatch
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CStopwatch::CStopwatch(void)
			{
				LARGE_INTEGER li;
				QueryPerformanceFrequency(&li);
				m_u64Freq = li.QuadPart;
				Reset();
			}

			//----------------------------------------------------------------------
			//	リセット
			//----------------------------------------------------------------------
			void CStopwatch::Reset(void)
			{
				LARGE_INTEGER li;
				QueryPerformanceCounter(&li);
				m_u64Begin = li.QuadPart;
			}

			//----------------------------------------------------------------------
			//	時間取得
			//----------------------------------------------------------------------
			f64 CStopwatch::Get(void) const
			{
				LARGE_INTEGER li;
				QueryPerformanceCounter(&li);

				const u64 TICK_DELTA		= 1000000000;
				const f64 INV_TICK_DELTA	= 1.0 / TICK_DELTA;
				return (f64)(((li.QuadPart - m_u64Begin) * TICK_DELTA) / m_u64Freq) * INV_TICK_DELTA;
			}

		private:
			u64 m_u64Freq;	//!< カウンター周波数
			u64 m_u64Begin;	//!< 開始時間
		};
#else
		//======================================================================
		//	タイマークラス
		//======================================================================
		class CStopwatch
			: public IStopwatch
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CStopwatch::CStopwatch(void)
			{
				Reset();
			}

			//----------------------------------------------------------------------
			//	リセット
			//----------------------------------------------------------------------
			void CStopwatch::Reset(void)
			{
				m_u64Begin = Tick();
			}

			//----------------------------------------------------------------------
			//	時間取得
			//----------------------------------------------------------------------
			f64 CStopwatch::Get(void) const
			{
				const f64 TickToMillisecond = 1.0 / (f64)TickPerSecond();
				return (f64)(Tick() - m_u64Begin) * TickToMillisecond;
			}

		private:
			u64 m_u64Begin;	//!< 開始時間
		};
#endif

		//----------------------------------------------------------------------
		// ストップウォッチの生成
		//----------------------------------------------------------------------
		b8 Create(IStopwatch** ppStopwatch)
		{
			*ppStopwatch = new CStopwatch;
			(*ppStopwatch)->AddRef();

			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
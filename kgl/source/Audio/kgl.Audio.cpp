//----------------------------------------------------------------------
//!
//!	@file	kgl.Audio.cpp
//!	@brief	オーディオシステム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Audio/kgl.Audio.h"
#include "Audio/kgl.StreamSound.h"

namespace kgl
{
	namespace Audio
	{
		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		extern b8 Create(IStreamManager** ppManager);

		//======================================================================
		//	描画システム
		//======================================================================
		class CAudioManager
			: public IAudioManager
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(void)
			{
#if	USE_XAUDIO2
				CreateXAudio2Driver(m_pDriver.GetReference());
#endif
				if (!m_pDriver.IsValid())
				{
					return false;
				}

				Create(m_pStreamManager.GetReference());
				return true;
			}
		};

		//----------------------------------------------------------------------
		//	オーディオ管理の生成
		//----------------------------------------------------------------------
		b8 Create(IAudioManager** ppAudio)
		{
			TKGLPtr<CAudioManager> pAudio = new CAudioManager;
			if( !pAudio->Initialize() )
			{
				return false;
			}
			*ppAudio = pAudio;
			(*ppAudio)->AddRef();

			return true;
		}
	}
}
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Render.cpp
//!	@brief	描画システム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Audio/kgl.Audio.h"
#include "Manager/kgl.GameManager.h"
#include "Thread/kgl.Thread.h"
#include "Thread/kgl.Event.h"
#include "Thread/kgl.CriticalSection.h"
#include "Utility/Manager/kgl.Manager.h"

namespace kgl
{
	namespace Audio
	{
		const u32 StreamMax = 8;

		typedef Utility::TManager<IStreamSound*> CStreamSoundList;
		//======================================================================
		//!	ストリーム管理インターフェイス
		//======================================================================
		class CStreamManager
			: public IStreamManager
		{
		public:
			//	コンストラクタ
			CStreamManager(void)
				: m_bExit(false)
			{}
			//	デストラクタ
			~CStreamManager(void)
			{
				Finalize();
			}

			//	初期化
			b8 Initialize(void);
			//	破棄
			void Finalize(void);

		public:
			//	サウンドの生成
			b8 Create(IStreamSound** ppStream, kgSound::IDecoder* pDecoder);
			//	ストリームの開放
			void ReleaseStream(IStreamSound* ppStream);

			//	更新
			void Update(void);
			//	更新
			void UpdateStream(void);

		public:
			//	ストリーム管理
			void StreamThreadFunction(void);
			//	ストリーム管理スレッド
			static u32 THREADFUNC StreamThreadFunction(void* pArg);

		private:
			CStreamSoundList					m_StreamList;		//!< ストリームリスト
			TKGLPtr<Thread::IThread>			m_pStreamThread;	//!< ストリームスレッド
			TKGLPtr<Thread::IEvent>				m_pUpdateEvent;		//!< 更新チェック用イベント
			TKGLPtr<Thread::ICriticalSection>	m_pCS;				//!< クリティカルセクション
			bool								m_bExit;			//!< 終了判定
		};

		//======================================================================
		//!	サウンド管理クラス
		//======================================================================
		class CStreamSound
			: public IStreamSound
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(IDecoder* pDecoder)
			{
				if( KGLGameManager() == null )
				{
					return false;
				}
				const u32 uiBufferTime = 4;

				WaveInfo Info = pDecoder->GetWaveInfo();
				//	ストリーム用に4秒間分のバッファを指定する
				Info.BufferSize = Info.Format.AvgBytesPerSecond * uiBufferTime;

				TKGLPtr<ISoundBufferManager> pSoundBufferManager = KGLGameManager()->AudioManager()->SoundDriver()->SoundBufferManager();

				if( !pSoundBufferManager->CreateSoundBuffer(m_pResource.GetReference(), Info) )
				{
					return false;
				}

				Thread::Create(m_pCS.GetReference());

				m_fBufferTime = (f32)uiBufferTime;
				m_fOffset = 0.0f;
				m_bFlip = false;
				m_bLoop = false;
				m_pDecoder = pDecoder;

				//	初回に全バッファ分の読み込みを行っておく
				ReadBuffer(0, Info.BufferSize);

				return true;
			}

			//----------------------------------------------------------------------
			//	開放
			//----------------------------------------------------------------------
			s32 Release(void)
			{
				s32 iRefCount = IStreamSound::Release();
				if( iRefCount <= 0 )
				{
					CStreamManager* pManager = Cast<CStreamManager>(KGLGameManager()->AudioManager()->StreamManager());
					pManager->ReleaseStream(this);
				}

				return iRefCount;
			}
		public:
			//----------------------------------------------------------------------
			//	ストリームバッファの更新が必要か？
			//----------------------------------------------------------------------
			b8 IsUpdate(void)
			{
				f32 fCurrent = Math::Repeat(m_pResource->CurrentTime(), 0.0f, m_fBufferTime);

				if( m_bFlip )
				{
					return (fCurrent < m_fBufferTime * 0.5f);
				}
				else
				{
					return (fCurrent > m_fBufferTime * 0.5f);
				}
			}
			//----------------------------------------------------------------------
			//	ストリームバッファの更新
			//----------------------------------------------------------------------
			void Update(void)
			{
				if( IsUpdate() )
				{
					u32 uiHalfSize = m_pResource->GetWaveInfo().BufferSize / 2;
					u32 uiOffset = m_bFlip? uiHalfSize: 0;

					ReadBuffer(uiOffset, uiHalfSize);

					m_bFlip = !m_bFlip;
				}
				if( !m_bLoop )
				{
					//	終端まで再生されていれば停止する
					if( m_pResource->GetWaveInfo().PlaySecond <= m_pResource->CurrentTime() )
					{
						if( IsPlaying() )
						{
							Stop();
						}
					}
				}
			}
			//----------------------------------------------------------------------
			//	ストリームバッファの読み込み
			//----------------------------------------------------------------------
			void ReadBuffer(u32 uiOffset, u32 uiSize)
			{
				SCOPE_LOCK_CRITICALSECTION(m_pCS);

				void* pBuf1;
				void* pBuf2;
				u32	uiBufSize1, uiBufSize2;
				//	バッファの残りサイズから読み込むサイズを指定する
				u32 uiBufferSize = Min(uiSize, m_pDecoder->GetWaveInfo().BufferSize - m_pDecoder->Tell());

				if( uiBufferSize > 0 )
				{
					m_pResource->Lock(uiOffset, uiBufferSize, &pBuf1, &uiBufSize1, &pBuf2, &uiBufSize2);

					m_pDecoder->Read(pBuf1, uiBufSize1);
					if( uiBufSize2 > 0 )
					{
						m_pDecoder->Read(pBuf2, uiBufSize2);
					}
					m_pResource->Unlock();
				}

				//	全部のバッファが再生済みか？
				if( uiBufferSize < uiSize )
				{
					m_pResource->Lock(uiOffset + uiBufferSize, uiSize - uiBufferSize, &pBuf1, &uiBufSize1, &pBuf2, &uiBufSize2);

					if( m_bLoop )
					{
						//	バッファの先頭に戻してループさせる
						m_pDecoder->Seek(0);
						m_pDecoder->Read(pBuf1, uiBufSize1);
						if( uiBufSize2 > 0 )
						{
							m_pDecoder->Read(pBuf2, uiBufSize2);
						}
					}
					else
					{
						//	無音を埋め込んでおく
						kgZeroMemory(pBuf1, uiBufSize1);
						if( uiBufSize2 > 0 )
						{
							kgZeroMemory(pBuf2, uiBufSize2);
						}
					}
					m_pResource->Unlock();
				}
			}

		public:
			//----------------------------------------------------------------------
			//	再生
			//----------------------------------------------------------------------
			b8 Play(b8 bLoop, f32 fOffset)
			{
				SCOPE_LOCK_CRITICALSECTION(m_pCS);

				Stop();
				u32 uiOffset = (u32)((f32)m_pDecoder->GetWaveInfo().Format.AvgBytesPerSecond * fOffset);
				//	再生位置を変更する場合はバッファ分だけ先に読み込んでおく
				if( uiOffset != 0 || m_pResource->GetWaveInfo().BufferSize != m_pDecoder->Tell() )
				{
					m_pDecoder->Seek(uiOffset);
					ReadBuffer(0, m_pResource->GetWaveInfo().BufferSize);
				}

				m_bLoop = !!bLoop;
				m_bFlip = false;
				m_fOffset = fOffset;
				return m_pResource->Play(true);
			}
			//----------------------------------------------------------------------
			//	停止
			//----------------------------------------------------------------------
			void Stop(void)
			{
				m_pResource->Stop();
			}
			//----------------------------------------------------------------------
			//	一時停止
			//----------------------------------------------------------------------
			void Pause(void)
			{
				m_pResource->Pause();
			}
			//----------------------------------------------------------------------
			//	一時停止の解除
			//----------------------------------------------------------------------
			void Resume(void)
			{
				m_pResource->Resume();
			}

			//----------------------------------------------------------------------
			//	再生中か？
			//----------------------------------------------------------------------
			b8 IsPlaying(void)
			{
				return m_pResource->GetState() == kgSound::EState::Playing;
			}
			//----------------------------------------------------------------------
			//	ループフラグ取得
			//----------------------------------------------------------------------
			b8 IsLoop(void)
			{
				return m_bLoop;
			}

			//----------------------------------------------------------------------
			//	現在の再生時間を取得
			//----------------------------------------------------------------------
			f32 CurrentTime(void)
			{
				return m_pResource->CurrentTime() + m_fOffset;
			}

		public:
			//----------------------------------------------------------------------
			//	リソースの取得
			//----------------------------------------------------------------------
			ISoundBuffer* GetResource(void)
			{
				return m_pResource;
			}
			//----------------------------------------------------------------------
			//	Wave情報の取得
			//----------------------------------------------------------------------
			const WaveInfo& GetWaveInfo(void)
			{
				return m_pResource->GetWaveInfo();
			}

		private:
			TKGLPtr<Thread::ICriticalSection>	m_pCS;			//!< クリティカルセクション
			TKGLPtr<ISoundBuffer>				m_pResource;	//!< サウンドバッファ
			TKGLPtr<IDecoder>					m_pDecoder;		//!< デコーダー
			f32									m_fBufferTime;	//!< バッファの再生時間
			f32									m_fOffset;		//!< 再生開始位置
			BITFIELD							m_bFlip: 1;		//!< フリップフラグ
			BITFIELD							m_bLoop: 1;		//!< ループフラグ
		};

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CStreamManager::Initialize(void)
		{
			m_StreamList.Initialize(StreamMax);

			Thread::Create(m_pUpdateEvent.GetReference());
			Thread::Create(m_pCS.GetReference());

			Thread::Create(m_pStreamThread.GetReference());
			m_pStreamThread->Start(StreamThreadFunction, this, KB(4), "StreamThread");

			return true;
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CStreamManager::Finalize(void)
		{
			m_bExit = true;
			m_pUpdateEvent->SetSignal();
			m_pStreamThread->Exit();

			m_pStreamThread = null;
			m_pUpdateEvent = null;
			m_pCS = null;
			m_StreamList.Finalize();
		}

		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		b8 CStreamManager::Create(IStreamSound** ppStream, kgSound::IDecoder* pDecoder)
		{
			TKGLPtr<CStreamSound> pStream = new CStreamSound;
			if( !pStream->Initialize(pDecoder) )
			{
				return false;
			}
			pStream->AddRef();

			{
				SCOPE_LOCK_CRITICALSECTION(m_pCS);

				CStreamSoundList::CItem* pItem = m_StreamList.CreateItem();
				if( !pItem )
				{
					SafeRelease(pStream);
					return false;
				}

				pItem->GetObject() = pStream;
			}

			*ppStream = pStream;

			return true;
		}

		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		void CStreamManager::ReleaseStream(IStreamSound* pStream)
		{
			SCOPE_LOCK_CRITICALSECTION(m_pCS);

			CStreamSoundList::CItem* pItem = m_StreamList.GetFirst();

			while( pItem )
			{
				if( pItem->GetObject() == pStream )
				{
					m_StreamList.ReleaseItem(pItem);
					break;
				}

				pItem = pItem->GetNext();
			}
		}

		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CStreamManager::Update(void)
		{
			//	ストリームスレッドの更新要請を行う
			m_pUpdateEvent->SetSignal();
		}
		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CStreamManager::UpdateStream(void)
		{
			SCOPE_LOCK_CRITICALSECTION(m_pCS);

			CStreamSoundList::CItem* pItem = m_StreamList.GetFirst();

			while( pItem )
			{
				IStreamSound* pStream = pItem->GetObject();
				Cast<CStreamSound>(pStream)->Update();

				pItem = pItem->GetNext();
			}
		}

		//----------------------------------------------------------------------
		//	ストリーム管理
		//----------------------------------------------------------------------
		void CStreamManager::StreamThreadFunction(void)
		{
			while( !m_bExit )
			{
				m_pUpdateEvent->Wait();

				UpdateStream();
			}
		}
			
		//----------------------------------------------------------------------
		//	ストリーム管理スレッド
		//----------------------------------------------------------------------
		u32 THREADFUNC CStreamManager::StreamThreadFunction(void* pArg)
		{
			CStreamManager* _this = (CStreamManager*)pArg;

			_this->StreamThreadFunction();

			return 0;
		}

		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		b8 Create(IStreamManager** ppManager)
		{
			TKGLPtr<CStreamManager> pManager = new CStreamManager;
			if( !pManager->Initialize() )
			{
				return false;
			}
			*ppManager = pManager;
			(*ppManager)->AddRef();

			return true;
		}

	}
}
//=======================================================================
//	END OF FILE
//=======================================================================
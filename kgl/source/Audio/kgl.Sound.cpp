//----------------------------------------------------------------------
//!
//!	@file	kgl.Sound.cpp
//!	@brief	サウンド関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Audio/kgl.Audio.h"
#include "Manager/kgl.GameManager.h"

namespace kgl
{
	namespace Audio
	{
		//======================================================================
		//!	サウンド管理クラス
		//======================================================================
		class CSound
			: public ISound
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const kgSound::WaveInfo& Info, const void* pData)
			{
				TKGLPtr<ISoundBufferManager> pSoundBufferManager = KGLGameManager()->AudioManager()->SoundDriver()->SoundBufferManager();

				if( !pSoundBufferManager->CreateSoundBuffer(m_pResource.GetReference(), Info, pData) )
				{
					return false;
				}

				return true;
			}
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(IDecoder* pDecoder)
			{
				TKGLPtr<ISoundBufferManager> pSoundBufferManager = KGLGameManager()->AudioManager()->SoundDriver()->SoundBufferManager();

				if( !pSoundBufferManager->CreateSoundBuffer(m_pResource.GetReference(), pDecoder) )
				{
					return false;
				}

				return true;
			}

		public:
			//----------------------------------------------------------------------
			//	再生
			//----------------------------------------------------------------------
			b8 Play(b8 bLoop, f32 fOffset)
			{
				return m_pResource->Play(bLoop, fOffset);
			}
			//----------------------------------------------------------------------
			//	停止
			//----------------------------------------------------------------------
			void Stop(void)
			{
				m_pResource->Stop();
			}
			//----------------------------------------------------------------------
			//	一時停止
			//----------------------------------------------------------------------
			void Pause(void)
			{
				m_pResource->Pause();
			}
			//----------------------------------------------------------------------
			//	一時停止の解除
			//----------------------------------------------------------------------
			void Resume(void)
			{
				m_pResource->Resume();
			}

			//----------------------------------------------------------------------
			//	再生中か？
			//----------------------------------------------------------------------
			b8 IsPlaying(void)
			{
				return m_pResource->GetState() == kgSound::EState::Playing;
			}
			//----------------------------------------------------------------------
			//	ループフラグ取得
			//----------------------------------------------------------------------
			b8 IsLoop(void)
			{
				return m_pResource->IsLoop();
			}

		public:
			//----------------------------------------------------------------------
			//	リソースの取得
			//----------------------------------------------------------------------
			ISoundBuffer* GetResource(void)
			{
				return m_pResource;
			}
			//----------------------------------------------------------------------
			//	Wave情報の取得
			//----------------------------------------------------------------------
			const WaveInfo& GetWaveInfo(void)
			{
				return m_pResource->GetWaveInfo();
			}

		private:
			TKGLPtr<ISoundBuffer>	m_pResource;	//!< サウンドバッファ
		};

		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		b8 Create(ISound** ppSound, const WaveInfo& Info, const void* pData)
		{
			TKGLPtr<CSound> pSound = new CSound;
			if( !pSound->Initialize(Info, pData) )
			{
				return false;
			}
			*ppSound = pSound;
			(*ppSound)->AddRef();

			return true;
		}
		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		b8 Create(ISound** ppSound, IDecoder* pDecoder)
		{
			TKGLPtr<CSound> pSound = new CSound;
			if( !pSound->Initialize(pDecoder) )
			{
				return false;
			}
			*ppSound = pSound;
			(*ppSound)->AddRef();

			return true;
		}
	}
}
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Entry.cpp
//!	@brief	起動シーケンス関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/algorithm/kgl.cio.h"
#include "Utility/Path/kgl.Path.h"
#include "File/kgl.Directory.h"
#include "Thread/kgl.Thread.h"
#include "Utility/Task/kgl.Task.h"

#if	!KGL_FINAL_RELEASE
#include "Render/Shader/kgl.ShaderCompileHelper.h"
#endif	// ~#if	!KGL_FINAL_RELEASE

#if	KGL_USE_WXWIDGETS
#include "kgEditor.h"
#endif	// ~#if	KGL_USE_WXWIDGETS


namespace kgl
{
#if	KGL_DEBUG_TRACE
	namespace Debug
	{
		extern void SetupOutputTrace(void);
		extern void Initialize(void);
		extern void Finalize(void);
	}
#endif	// ~#if KGL_DEBUG_TRACE

	namespace Task
	{
		//!	初期化
		//!	@param iMaxThread  [in] 最大スレッド数
		//!	@param iMaxTask    [in] スレッド毎の最大タスク数
		//!	@return 結果
		extern b8 Initialize(u32 uiMaxThread, u32 uiMaxTask);
		//!	終了処理
		extern void Finalize(void);
	}

	namespace FastMath
	{
		//!	初期化
		//!	@return 結果
		extern b8 Initialize(void);
		//!	終了処理
		extern void Finalize(void);
	}


#if defined(WINDOWS)
	static STICKYKEYS g_StartupStickyKeys = {sizeof(STICKYKEYS), 0};
	static TOGGLEKEYS g_StartupToggleKeys = {sizeof(TOGGLEKEYS), 0};
	static FILTERKEYS g_StartupFilterKeys = {sizeof(FILTERKEYS), 0};

	//----------------------------------------------------------------------
	//!	ショートカットキーの抑制
	//----------------------------------------------------------------------
	void AllowAccessibilityShortcutKeys(bool bAllowKeys)
	{
		if( bAllowKeys )
		{
			// Restore StickyKeys/etc to original state and enable Windows key
			SystemParametersInfo(SPI_SETSTICKYKEYS, sizeof(STICKYKEYS), &g_StartupStickyKeys, 0);
			SystemParametersInfo(SPI_SETTOGGLEKEYS, sizeof(TOGGLEKEYS), &g_StartupToggleKeys, 0);
			SystemParametersInfo(SPI_SETFILTERKEYS, sizeof(FILTERKEYS), &g_StartupFilterKeys, 0);
		}
		else
		{
			// Disable StickyKeys/etc shortcuts but if the accessibility feature is on, 
			// then leave the settings alone as its probably being usefully used

			STICKYKEYS skOff = g_StartupStickyKeys;
			if( (skOff.dwFlags & SKF_STICKYKEYSON) == 0 )
			{
				// Disable the hotkey and the confirmation
				skOff.dwFlags &= ~SKF_HOTKEYACTIVE;
				skOff.dwFlags &= ~SKF_CONFIRMHOTKEY;

				SystemParametersInfo(SPI_SETSTICKYKEYS, sizeof(STICKYKEYS), &skOff, 0);
			}
		
			TOGGLEKEYS tkOff = g_StartupToggleKeys;
			if( (tkOff.dwFlags & TKF_TOGGLEKEYSON) == 0 )
			{
				// Disable the hotkey and the confirmation
				tkOff.dwFlags &= ~TKF_HOTKEYACTIVE;
				tkOff.dwFlags &= ~TKF_CONFIRMHOTKEY;

				SystemParametersInfo(SPI_SETTOGGLEKEYS, sizeof(TOGGLEKEYS), &tkOff, 0);
			}
		
			FILTERKEYS fkOff = g_StartupFilterKeys;
			if( (fkOff.dwFlags & FKF_FILTERKEYSON) == 0 )
			{
				// Disable the hotkey and the confirmation
				fkOff.dwFlags &= ~FKF_HOTKEYACTIVE;
				fkOff.dwFlags &= ~FKF_CONFIRMHOTKEY;
			
				SystemParametersInfo(SPI_SETFILTERKEYS, sizeof(FILTERKEYS), &fkOff, 0);
			}
		}
	}
#endif // !defined(_WIN32)

	//----------------------------------------------------------------------
	//!	ビルド情報表示
	//----------------------------------------------------------------------
	void BuildInfo(b8 bWriteLog = false)
	{
		(void)bWriteLog;
#if	KGL_DEBUG_TRACE
#define	Output(_str_)	(( bWriteLog )? (cout << _str_): SystemTrace(_str_))
#else	// ~#if KGL_DEBUG_TRACE
#define	Output(_str_)	(( bWriteLog )? (cout << _str_): (void)_str_)
#endif	// ~#if KGL_DEBUG_TRACE

#if	!KGL_FINAL_RELEASE
		const c8* pPath = "./Log/buildlog.txt";
		algorithm::cout cout;
		if( bWriteLog )
		{
			c8 cTemp[256];
			Path::GetDirectory(cTemp, pPath);
			Directory::Create(cTemp);
			cout.write(pPath);
		}

//	Platform Dependent
		Output(CharSet::Format("============== Build Log =============="));
		Output(CharSet::Format("Compile Date   : %s(%s)", __DATE__, __TIME__));
		Output(CharSet::Format("Build Mode     : %s(%s)", KGL_DEBUG? "debug": "release", Is64bit()? "64bit": "32bit"));
		Output(CharSet::Format("Platform       : %s", PLATFORM));
		Output(CharSet::Format("Core           : %d", Config::NumberOfProcessors));
		if( Config::PhysicalMemory )
		{
			Output(CharSet::Format("PhycalMemory   : %llu(%uGB)", Config::PhysicalMemory, (u32)((f32)Config::PhysicalMemory/1024.0f/1024.0f/1024.0f+0.1f)));
		}
		else
		{
			Output(CharSet::Format("PhycalMemory   : No Info"));
		}
#if	WINDOWS
		Output(CharSet::Format("App Mode       : %s", CONSOLE_TOOL? "Console Tool": "WinApp"));
		Output(CharSet::Format("Windows Ver    : 0x%04x", _WIN32_WINNT));
#endif	// ~#if WINDOWS
		Output(CharSet::Format("======================================="));
#endif	// ~#if !KGL_FINAL_RELEASE
	}

	//----------------------------------------------------------------------
	//!	実行情報表示
	//----------------------------------------------------------------------
	void RunInfo(Utility::ICommandLine* pCommandLine)
	{
		(void)pCommandLine;
#if	KGL_DEBUG_TRACE
		u32 uiMax = pCommandLine->GetCount();

		SystemTrace("[Command Line]");
		SystemTrace("Command : %d", uiMax);
		for( u32 i=0; i < uiMax; i++ )
		{
			SystemTrace("%s", pCommandLine->GetCommand(i));
		}
#if KGL_USE_TLSF_ALLOCATOR
		SystemTrace("[Memory Info]");
#if	X64
		SystemTrace("Memory Size       : %llu(%lluM) Byte", MemoryPoolSize(), MemoryPoolSize()/1024/1024);
		SystemTrace("Debug Memory Size : %llu(%lluM) Byte", DebugMemoryPoolSize(), DebugMemoryPoolSize()/1024/1024);
#else
		SystemTrace("Memory Size       : %u(%uM) Byte", MemoryPoolSize(), MemoryPoolSize()/1024/1024);
		SystemTrace("Debug Memory Size : %u(%uM) Byte", DebugMemoryPoolSize(), DebugMemoryPoolSize()/1024/1024);
#endif
#endif // ~#if KGL_USE_TLSF_ALLOCATOR

#endif	// ~#if KGL_DEBUG_TRACE
	}

#define	CheckCommand(command)	(kgStrcmp(&cCommand[1], command) == 0)
	//----------------------------------------------------------------------
	//!	コマンド確認
	//!	@param pCommandLine [in] コマンドライン引数
	//----------------------------------------------------------------------
	void CheckCommandLine(Utility::ICommandLine* pCommandLine)
	{
		s32 iRemove;
		c8 cCommand[256];
		for( u32 i=1; i < pCommandLine->GetCount(); )
		{
			iRemove = 0;
			CharSet::ToUpper(cCommand, pCommandLine->GetCommand(i));

			switch( cCommand[0] )
			{
			case '-':
			case '/':
#if	KGL_DEBUG_TRACE
				//	トレースをオフにする(エラー、警告を除く)
				if( CheckCommand("TRACEOFF") )
				{
					EnableSystemTrace(false);
					EnableTrace(false);
					iRemove = 1;
				}
				//	全てのトレースをオフにする
				if( CheckCommand("TRACEOFFALL") )
				{
					EnableSystemTrace(false);
					EnableErrorTrace(false);
					EnableWarningTrace(false);
					EnableTrace(false);
					iRemove = 1;
				}
#endif	// ~#if KGL_DEBUG_TRACE
#if	!KGL_FINAL_RELEASE
				//	ビルド情報を出力
				if( CheckCommand("BUILDLOG") )
				{
					BuildInfo(true);
					iRemove = 1;
				}
#endif	// ~#if !KGL_FINAL_RELEASE
#if	KGL_USE_TOOLS
				//	エディターを起動する
				if( CheckCommand("EDITOR") )
				{
					Config::Editor = true;
					iRemove = 1;
				}
#endif	// ~#if	KGL_USE_TOOLS
				break;
				
			default:
				break;
			}

			for( s32 j=0; j < iRemove; j ++ )
			{
				pCommandLine->EraseCommand(i);
			}

			if( iRemove == 0 )	i++;
		}

#if	KGL_DEBUG_TRACE
		//	トレースのログをテキストで出力
		{
			Debug::SetupOutputTrace();
		}
#endif	// ~#if KGL_DEBUG_TRACE
	}

#if	!KGL_FINAL_RELEASE
	//----------------------------------------------------------------------
	//!	KGLシステムのセットアップ
	//----------------------------------------------------------------------
	void BuildResource(void)
	{
		Shader::ShaderCompile(Config::ShaderRebuild);
	}
#endif	// ~#if	!KGL_FINAL_RELEASE
	//----------------------------------------------------------------------
	//!	KGLシステムのセットアップ
	//----------------------------------------------------------------------
	b8 SetupSystem(Utility::ICommandLine* pCommandLine)
	{
//	Platform Dependent
#if	WINDOWS
		if( !Config::PhysicalMemory )
		{
			//	最大コア数取得
			SYSTEM_INFO sysinfo;
			GetSystemInfo(&sysinfo);
			Config::NumberOfProcessors = sysinfo.dwNumberOfProcessors;
			//	メモリサイズ取得
			MEMORYSTATUS memstate;
			memstate.dwLength = sizeof(memstate);
			GlobalMemoryStatus(&memstate);
			Config::PhysicalMemory = memstate.dwTotalPhys;
		}
#else
		Config::NumberOfProcessors = 1;
		Config::PhysicalMemory = MB(256);
#endif

		//	コマンド確認
		CheckCommandLine(pCommandLine);
		//	ビルド情報表示
		BuildInfo();
		//	実行情報表示
		RunInfo(pCommandLine);

#if	!KGL_FINAL_RELEASE
		BuildResource();

		if( Config::DataConvertOnly )
		{
			return false;
		}
#endif	// ~#if	!KGL_FINAL_RELEASE

		//	タスクシステムの初期化
		Task::Initialize(Config::NumberOfProcessors, Task::AsyncTaskMax);
		//	高速演算処理の初期化
		FastMath::Initialize();

		//	ショートカットキーの無効化
		AllowAccessibilityShortcutKeys(false);

		return true;
	}

	//----------------------------------------------------------------------
	//!	KGLシステムの解放
	//----------------------------------------------------------------------
	void ReleaseSyatem(void)
	{
		//	ショートカットキーの無効化
		AllowAccessibilityShortcutKeys(true);

		//	高速演算処理の破棄
		FastMath::Finalize();
		//	タスクシステムの破棄
		Task::Finalize();
	}

	//----------------------------------------------------------------------
	//!	ゲームスレッド
	//!	@return 実行結果
	//----------------------------------------------------------------------
	u32 THREADFUNC GameThreadMain(void* pArg)
	{
		(void)pArg;

		s32 iExitCode = 0;

		{
#if	KGL_DEBUG_TRACE
			Debug::Initialize();
#endif	// ~#if KGL_DEBUG_TRACE
			TKGLPtr<kgl::Utility::ICommandLine> pCommandLine;
			Utility::Create(pCommandLine.GetReference());

			//	KGLシステムのセットアップ
			if( SetupSystem(pCommandLine) )
			{
				//	アプリケーション実行
				iExitCode = AppRun(pCommandLine);
			}
			//	KGLシステムの解放
			ReleaseSyatem();

#if	KGL_DEBUG_TRACE
			Debug::Finalize();
#endif	// ~#if KGL_DEBUG_TRACE
		}

#if	KGL_USE_WXWIDGETS
		wxExitGame();
#endif	// ~#if	KGL_USE_WXWIDGETS

		return iExitCode;
	}
}

#define	CREATE_GAME_THREAD	0

//	ゲームスレッド
kgl::Thread::IThread* GameThread = null;
//----------------------------------------------------------------------
//	ゲームスレッドの起動
//----------------------------------------------------------------------
bool LaunchGameThread(void)
{
#if CREATE_GAME_THREAD
	if( GameThread )
	{
		return false;
	}
	kgl::Thread::Create(&GameThread);

	GameThread->Start(kgl::GameThreadMain, null, MB(1), "GameThread");
	GameThread->SetPriority(kgl::Thread::EPriority::Highest);
#endif // CREATE_GAME_THREAD

	return true;
}
//----------------------------------------------------------------------
//	ゲームスレッドの終了待ち
//----------------------------------------------------------------------
s32 ExitWaitGameThread(void)
{
	s32 iExitCode = 0;
#if CREATE_GAME_THREAD
	if( GameThread )
	{
		GameThread->Exit();
		iExitCode = GameThread->GetExitCode();
		kgl::SafeRelease(GameThread);
	}
#else	// CREATE_GAME_THREAD
	iExitCode = kgl::GameThreadMain(null);
#endif	// CREATE_GAME_THREAD
	return iExitCode;
}

//----------------------------------------------------------------------
//	エントリーポイント(KGL)
//----------------------------------------------------------------------
s32 EntryPoint(void)
{
#if	KGL_USE_WXWIDGETS

	return wxLaunchGame();

#else	// ~#if	KGL_USE_WXWIDGETS

	if( LaunchGameThread() )
	{
		return ExitWaitGameThread();
	}
	return 0;

#endif	// ~#if	KGL_USE_WXWIDGETS
}

//	Platform Dependent
//----------------------------------------------------------------------
//	エントリーポイント
//----------------------------------------------------------------------
#if	!WINDOWS || CONSOLE_TOOL
int main(void)
#else	// ~#if !CONSOLE_TOOL
int APIENTRY WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
#endif	// ~#if !CONSOLE_TOOL
{
	return EntryPoint();
}


//=======================================================================
//	END OF FILE
//=======================================================================
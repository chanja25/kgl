//----------------------------------------------------------------------
//!
//!	@file	kgl.Archive.cpp
//!	@brief	アーカイブ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Hash/Kgl.Hash.h"
#include "File/kgl.Archive.h"
#include "File/kgl.Directory.h"
#include "Compress/kgl.zlib.h"

using namespace kgl::algorithm;

namespace kgl
{

	namespace Archive
	{
		static c8 s_cTemp[256];

		//	アーカイブヘッダー
		struct ArchiveHeader
		{
			s32	iID;				//!< 識別子
			s32	iVersion;			//!< バージョン
			s16 sFolderNum;			//!< フォルダー情報数
			s16 sFileNum;			//!< ファイル情報数
			s32 iInfoSize;			//!< フォルダー・ファイル情報サイズ
			s32 iArchiveSize;		//!< アーカイブサイズ
			s32	iArchiveOffset;		//!< ファイルの先頭位置
		};

		//	フォルダー情報
		struct FolderInfo
		{
			s32	iHash;		//!< フォルダー名のハッシュ値
			s16	sHead;		//!< ファイル情報の先頭位置
			s16	sFileNum;	//!< ファイル数
			s16	sChild;		//!< 子のフォルダー
			s16	sBrother;	//!< 次のフォルダー
		};

		//	ファイル情報
		struct FileInfo
		{
			s32	iHash;		//!< ファイル名のハッシュ値
			s32	iHead;		//!< ファイルの先頭位置
			s32	iArcSize;	//!< 圧縮ファイルサイズ
			s32	iSize;		//!< ファイルサイズ
		};

		//!	リソースデータ
		struct ResourceData
		{
			s32		iCount;			//!< ロード数
			s32		iHead;			//!< ファイルの先頭位置(検索用)
			void*	pBuffer;		//!< ファイルデータ

			ResourceData* pNext;	//!< 次のデータ
#if	KGL_DEBUG_FILE
			c8		cName[256];		//!< ファイル名
#endif	// ~#if KGL_DEBUG_FILE
		};

		//	直接読み込みリソースデータ
		struct DirectResourceData
		{
			s32		iCount;			//!< ロード数
			c8		cName[256];		//!< ファイル名
			void*	pBuffer;		//!< ファイルデータ
			s32		iSize;			//!< ファイルサイズ

			DirectResourceData* pNext;	//!< 次のデータ
		};

		//======================================================================
		//!	アーカイブマネージャーインターフェイス
		//======================================================================
		class CArchiveManager
			: public IArchiveManager
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CArchiveManager(void)
				: m_pFile(null), m_pReadFile(null)
				, m_pFolderInfo(null), m_pFileInfo(null)
				, m_sResourceNum(0)
			{
				kgZeroMemory(&m_ArcHeader, sizeof(ArchiveHeader));
				kgZeroMemory(&m_Resource,  sizeof(ResourceData) );
				kgZeroMemory(&m_DirectResource, sizeof(DirectResourceData));
			}

			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const c8* pFileName)
			{
				SystemTrace("Archiveの初期化");
				kgAssert(m_sResourceNum == 0 && m_pFileInfo == null, "既に初期化されています");

				File::Create(&m_pFile);
				File::Create(&m_pReadFile);
				Hash::CreateCRC32(&m_pHash);

				//	ファイル読み込み
				if( !m_pFile->Open(pFileName, File::EOpen::Read) )
				{
					WarningTrace("アーカイブデータが存在しません");
					return false;
				}

				m_sFileName = pFileName;
				//	ヘッダー読み込み
				m_pFile->Read(&m_ArcHeader, sizeof(ArchiveHeader));

				if( m_ArcHeader.iID != FOURCC('k', 'g', 'a', ' ') )
				{
					c8 cTemp[256];
					sprintf(cTemp, "kgaファイルではありません(%s)", pFileName);
					kgAssert(false, cTemp);
					return false;
				}

#if	KGL_DEBUG_FILE
				{
					s16* pVersion = (s16*)&m_ArcHeader.iVersion;

					SystemTrace("kgaファイル");
					SystemTrace("Version %2d.%02d", *pVersion, *(pVersion + 1));
				}
#endif	// ~#if KGL_DEBUG_FILE

				//	読み込み用領域生成
				s8* pBuffer = (s8*)kgl::Memory::Allocate(m_ArcHeader.iInfoSize);
				//	アーカイブ情報の取得
				m_pFile->Read(pBuffer, m_ArcHeader.iInfoSize);
				
				s32 iFolderInfoSize	= sizeof(FolderInfo) * m_ArcHeader.sFolderNum;
				s32 iFileInfoSize	= sizeof(FileInfo) * m_ArcHeader.sFileNum;

				//	情報解凍
				u32 uiSize = iFolderInfoSize + iFileInfoSize;
				s8* pInfo = (s8*)kgl::Memory::Allocate(uiSize);
				zlib::Decompress(pInfo, &uiSize, pBuffer, m_ArcHeader.iInfoSize);

				//	フォルダー情報設定
				m_pFolderInfo = reinterpret_cast<FolderInfo*>(pInfo);
				//	ファイル情報設定
				m_pFileInfo = reinterpret_cast<FileInfo*>(pInfo + iFolderInfoSize);
				
				m_pEndResource = &m_Resource;
				m_pDirectEndResource = &m_DirectResource;
				m_sResourceNum = 0;

				//	読み込み用領域の解放
				SafeDeleteArray(pBuffer);

				return true;
			}

			//----------------------------------------------------------------------
			//	破棄
			//----------------------------------------------------------------------
			void Finalize(void)
			{
				m_sFileName.clear();

				{
					DirectResourceData* pResource = m_DirectResource.pNext;
					//	未開放リソースの解放
					while( pResource )
					{
						WarningTrace("(%s)がUnloadされていません", pResource->cName);
						UnloadData(pResource->pBuffer);

						pResource = m_DirectResource.pNext;
					}
				}

				{
					ResourceData* pResource = m_Resource.pNext;
					//	未開放リソースの解放
					while( pResource )
					{
#if	KGL_DEBUG_FILE
						for( s32 i = 0; i < m_ArcHeader.sFileNum; i++ )
						{
							if( pResource->iHead == m_pFileInfo[i].iHead )
							{
								WarningTrace("(%s)がUnloadされていません", pResource->cName);
								break;
							}
						}
#endif	// ~#if KGL_DEBUG_FILE
						UnloadData(pResource->pBuffer);

						pResource = m_Resource.pNext;
					}
				}
				
#if	KGL_DEBUG_FILE
				if( m_sResourceNum )
				{
					WarningTrace("解放後のリソースの数が正しくありません(例外エラー)");
					WarningTrace("想定外の処理(メモリ破壊等)が行われている可能性があります");
				}
#endif	// ~#if KGL_DEBUG_FILE

				//	解放
				SafeDeleteArray(m_pFolderInfo);
				m_pFileInfo = null;

				m_pFile->Close();

				SafeRelease(m_pFile);
				SafeRelease(m_pReadFile);
			}

			//----------------------------------------------------------------------
			//	ロード
			//----------------------------------------------------------------------
			s32 Load(const c8* pFileName)
			{
				SystemTrace("アーカイブデータロード(%s)", pFileName);

				s32 iSize = -1;
				FileInfo* pFileInfo = GetFileInfo(pFileName);
				
				//	アーカイブ内にファイルが存在しない場合は直接フォルダ内から読み込む
				if( pFileInfo == null )
				{
					DirectResourceData* pResource = &m_DirectResource;
					DirectResourceData* pNext = m_DirectResource.pNext;
					//	既に読み込まれているか確認
					while( pNext )
					{
						pResource = pNext;
						//	読み込まれている場合それを返す
						if( strcmpi(pNext->cName, pFileName) == 0 )
						{
							pNext->iCount ++;
							return pNext->iSize;
						}
						pNext = pNext->pNext;
					}

					ReadFile(pFileName, &iSize);

					return iSize;
				}

				ResourceData* pResource = &m_Resource;
				//	ファイル検索
				while( pResource->pNext )
				{
					ResourceData* pNext = pResource->pNext;
					//	既に読み込まれている場合それを返す
					if( pNext->iHead == pFileInfo->iHead )
					{
						pNext->iCount ++;
						return pFileInfo->iSize;
					}

					//	次へ進める
					pResource = pNext;
				}

				Read(pFileInfo);
				iSize = pFileInfo->iSize;

				return iSize;
			}

			//----------------------------------------------------------------------
			//	ロード(フォルダー単位)
			//----------------------------------------------------------------------
			s32 LoadPath(const c8* pPath)
			{
				s32 iLoadCount = 0;
				string strPath = pPath;
				FolderInfo* pFolderInfo = GetFolderInfo((strPath + "/").c_str());

				if( pFolderInfo == null )
				{
					TKGLPtr<Directory::IDirectory> pDirectory;
					Directory::Create(pDirectory.GetReference());

					const c8* pPath = pDirectory->FindFile((strPath + "/*").c_str(), null, Directory::EAttribute::File);
					while( pPath )
					{
						if( Load((strPath + "/" + pPath).c_str()) > 0 )
							iLoadCount ++;

						pPath = pDirectory->NextFile();
					}

					return iLoadCount;
				}

				for( s16 i=0; i < pFolderInfo->sFileNum; i++ )
				{
					FileInfo* pFileInfo = &m_pFileInfo[pFolderInfo->sHead + i];

					if( Read(pFileInfo) )
						iLoadCount ++;
				}

				return iLoadCount;
			}

			//----------------------------------------------------------------------
			//	アンロード
			//----------------------------------------------------------------------
			void Unload(const c8* pFileName)
			{
				//	ファイル情報の取得
				FileInfo* pFileInfo = GetFileInfo(pFileName);
				if( pFileInfo == null )
				{
					DirectResourceData* pResource = &m_DirectResource;
					//	ファイル検索
					while( pResource->pNext )
					{
						DirectResourceData* pNext = pResource->pNext;
						//	ファイル名確認
						if( strcmpi(pNext->cName, pFileName) == 0 )
						{
							pNext->iCount --;
							if( pNext->iCount > 0 )
								return;

							pResource->pNext = pNext->pNext;

							if( pNext == m_pDirectEndResource )
								m_pDirectEndResource = pResource;

							//	解放
							SafeDeleteArray(pNext);

							m_sResourceNum --;
							return;
						}
						pResource = pNext;
					}
				}

				Unload(pFileInfo);

				WarningTrace("%sは読み込まれていないか、存在しません(CArchive::UnloadData)", pFileName);
			}


			//----------------------------------------------------------------------
			//	アンロード
			//!	@param pFileInfo [in] ファイル情報
			//----------------------------------------------------------------------
			void Unload(const FileInfo* pFileInfo)
			{
				ResourceData* pResource = &m_Resource;
				//	ファイル検索
				while( pResource->pNext )
				{
					ResourceData* pNext = pResource->pNext;
					if( pNext->iHead == pFileInfo->iHead )
					{
						pNext->iCount --;
						if( pNext->iCount > 0 )
							return;

						pResource->pNext = pNext->pNext;

						if( pNext == m_pEndResource )
							m_pEndResource = pResource;
						//	解放
						SafeDeleteArray(pNext);

						m_sResourceNum --;
						return;
					}

					pResource = pNext;
				}
			}

			//----------------------------------------------------------------------
			//	アンロード
			//----------------------------------------------------------------------
			void UnloadData(const void* pBuffer)
			{
				if( pBuffer == null )
					return;

				{
					DirectResourceData* pResource = &m_DirectResource;
					//	ファイル検索
					while( pResource->pNext )
					{
						DirectResourceData* pNext = pResource->pNext;
						if( pNext->pBuffer == pBuffer )
						{
							pNext->iCount --;
							if( pNext->iCount > 0 )
								return;

							pResource->pNext = pNext->pNext;
							//	解放
							SafeDeleteArray(pNext);

							m_sResourceNum --;
							return;
						}
						pResource = pNext;
					}
				}

				{
					ResourceData* pResource = &m_Resource;
					//	ファイル検索
					while( pResource->pNext )
					{
						ResourceData* pNext = pResource->pNext;
						if( pNext->pBuffer == pBuffer )
						{
							pNext->iCount --;
							if( pNext->iCount > 0 )
								return;

							pResource->pNext = pNext->pNext;
							//	解放
							SafeDeleteArray(pNext);

							m_sResourceNum --;
							return;
						}
						pResource = pNext;
					}
				}
				
				WarningTrace("管理されていないデータをアンロードしようとしました(CArchive::UnloadData)");
			}

			//----------------------------------------------------------------------
			//	ロード(フォルダー単位)
			//----------------------------------------------------------------------
			void UnloadPath(const c8* pPath)
			{
				string strPath = pPath;
				FolderInfo* pFolderInfo = GetFolderInfo((strPath + "/").c_str());

				if( pFolderInfo == null )
				{
					TKGLPtr<Directory::IDirectory> pDirectory;
					Directory::Create(pDirectory.GetReference());

					const c8* pPath = pDirectory->FindFile((strPath + "/*").c_str(), null, Directory::EAttribute::File);
					while( pPath )
					{
						Unload((strPath + "/" + pPath).c_str());

						pPath = pDirectory->NextFile();
					}

					return;
				}

				for( s16 i=0; i < pFolderInfo->sFileNum; i++ )
				{
					FileInfo* pFileInfo = &m_pFileInfo[pFolderInfo->sHead + i];
					Unload(pFileInfo);
				}
			}

			//----------------------------------------------------------------------
			//	読み込み
			//!	@param pFileName [in] ファイル名
			//!	@return データ
			//----------------------------------------------------------------------
			void* Read(FileInfo* pFileInfo)
			{
				//	ファイル位置取得
				s32 iHead = m_ArcHeader.iArchiveOffset + pFileInfo->iHead;
				//	ファイル位置に移動
				m_pFile->Seek(iHead, File::EFilePointer::Begin);

				//	ファイルサイズ取得
				u32 uiSize = pFileInfo->iSize;
				//	ファイル領域生成
				s8* pBuffer = (s8*)kgl::Memory::Allocate(uiSize + sizeof(ResourceData));

				ResourceData* pResource = m_pEndResource;

				//	リソース情報をセットする
				pResource->pNext = (ResourceData*)pBuffer;
				pResource = pResource->pNext;
				kgZeroMemory(pResource, sizeof(ResourceData));

				pResource->iCount	= 1;
				pResource->iHead	= pFileInfo->iHead;
				//	リソース情報分進める
				pBuffer += sizeof(ResourceData);

				//	圧縮無しファイル
				if( pFileInfo->iArcSize <= 0 )
				{
					m_pFile->Read(pBuffer, uiSize);
					pResource->pBuffer = pBuffer;
				}
				//	圧縮ファイル
				else
				{
					void* pArcBuffer = (s8*)kgl::Memory::Allocate(pFileInfo->iArcSize);
					m_pFile->Read(pArcBuffer, pFileInfo->iArcSize);

					//	解凍
					zlib::Decompress(pBuffer, &uiSize, pArcBuffer, pFileInfo->iArcSize);

					pResource->pBuffer = pBuffer;

					SafeDeleteArray(pArcBuffer);
				}

				m_pEndResource = pResource;
				//	リソース数追加
				m_sResourceNum ++;

				return pBuffer;
			}

			//----------------------------------------------------------------------
			//	読み込み(単体のファイル読み込み)
			//!	@param pFileName [in] ファイル名
			//!	@param pSize [out] ファイルのサイズ
			//!	@return データ
			//----------------------------------------------------------------------
			void* ReadFile(const c8* pFileName, s32* pSize = null)
			{
#if	KGL_DEBUG_FILE
				if( m_pFileInfo )
				{
					SystemTrace("アーカイブ内にファイルが存在しませんでした(CArchive::ReadFile)");
					SystemTrace("直接フォルダ内から読み込みます(%s)", pFileName);
				}
#endif	// ~#if KGL_DEBUG_FILE

				//	ファイル読み込み
				if( !m_pReadFile->Open(pFileName, File::EOpen::Read) )
				{
					return null;
				}

				u32 uiSize = m_pReadFile->GetSize();

				if( pSize )
					*pSize = uiSize;

				DirectResourceData* pResource = m_pDirectEndResource;

				//	ファイル領域生成
				s8* pBuffer = (s8*)kgl::Memory::Allocate(*pSize + sizeof(DirectResourceData));
				//	リソース情報をセットする
				pResource->pNext = (DirectResourceData*)pBuffer;
				pResource = pResource->pNext;
				kgZeroMemory(pResource, sizeof(DirectResourceData));

				pBuffer += sizeof(DirectResourceData);
				m_pReadFile->Read(pBuffer, uiSize);

				//	データセット
				kgStrcpy(pResource->cName, pFileName);
				pResource->iCount	= 1;
				pResource->pBuffer	= pBuffer;
				pResource->iSize	= uiSize;

				m_pDirectEndResource = pResource;
				m_sResourceNum ++;

				m_pReadFile->Close();

				return pBuffer;
			}

			//----------------------------------------------------------------------
			//!	データ取得(未ロードの場合は読み込んでから取得)
			//----------------------------------------------------------------------
			const void* GetData(const c8* pFileName, s32* pSize)
			{
				//	ファイル情報の取得
				FileInfo* pFileInfo = GetFileInfo(pFileName);
				//	アーカイブ内にファイルが存在しない場合は直接フォルダ内から読み込む
				if( pFileInfo == null )
				{
					DirectResourceData* pResource = &m_DirectResource;
					DirectResourceData* pNext = m_DirectResource.pNext;
					//	既に読み込まれているか確認
					while( pNext )
					{
						pResource = pNext;
						//	読み込まれている場合それを返す
						if( strcmpi(pNext->cName, pFileName) == 0 )
						{
							*pSize = pNext->iSize;
							return pNext->pBuffer;
						}
						pNext = pNext->pNext;
					}

					WarningTrace("ロードしていないデータを取得しようとしました(CArchive::GetData)");
					//	読み込み
					return ReadFile(pFileName, pSize);
				}

				//	サイズ取得
				if( pSize )
					*pSize = pFileInfo->iSize;

				ResourceData* pResource = &m_Resource;
				//	ファイル検索
				while( pResource->pNext )
				{
					ResourceData* pNext = pResource->pNext;
					//	既に読み込まれている場合それを返す
					if( pNext->iHead == pFileInfo->iHead )
						return pNext->pBuffer;

					//	次へ進める
					pResource = pNext;
				}

				WarningTrace("ロードしていないデータを取得しようとしました(CArchive::GetData)");
				WarningTrace("データをロードし、取得します(%s)", pFileName);
				return Read(pFileInfo);
			}

			//----------------------------------------------------------------------
			//!	ハンドル取得
			//----------------------------------------------------------------------
			File::IFile* GetFile(const c8* pFileName, s32* pSize)
			{
				TKGLPtr<File::IFile> pFile;
				File::Create(pFile.GetReference());

				//	ファイル情報の取得
				FileInfo* pFileInfo = GetFileInfo(pFileName);
				if( pFileInfo == null )
				{
					if( !pFile->Open(pFileName, File::EOpen::Read, File::EShare::Read, File::ECreate::OpenExisting) )
					{
						WarningTrace("%sの読み込みに失敗しました(CArchive::GetFile)", pFileName);
						return null;
					}
					
					//	サイズ取得
					if( pSize )
						*pSize = pFile->GetSize();
				}
				else
				{
					//	開けているか確認
					if( !pFile->Open(m_sFileName.c_str(), File::EOpen::Read, File::EShare::Read, File::ECreate::OpenExisting) )
					{
						WarningTrace("%sの読み込みに失敗しました(CArchive::GetFile)", pFileName);
						return null;
					}

					//	ファイルサイズの取得
					*pSize = pFileInfo->iArcSize? pFileInfo->iSize: pFileInfo->iArcSize;

					//	ファイル位置に移動
					pFile->Seek(pFileInfo->iHead, File::EFilePointer::Begin);
				}

				pFile->AddRef();
				return pFile;
			}

			//----------------------------------------------------------------------
			//!	アーカイブ数の取得
			//----------------------------------------------------------------------
			s16 GetArchiveNum(void)
			{
				return m_ArcHeader.sFileNum;
			}

			//----------------------------------------------------------------------
			//!	読み込ん済みのファイル数取得
			//----------------------------------------------------------------------
			s16 GetFileNum(void)
			{
				return m_sResourceNum;
			}

			//----------------------------------------------------------------------
			//!	読み込み済みのファイル情報取得(デバッグ処理)
			//----------------------------------------------------------------------
			ArchiveInfo GetArchiveInfo(s16 sNo)
			{
				ArchiveInfo Info;
				kgZeroMemory(&Info, sizeof(Info));

				if( sNo >= m_sResourceNum )
					return Info;

				{
					ResourceData* pResource = m_Resource.pNext;
					//	sNo番目のリソースデータ取得
					while( pResource )
					{
						if( sNo -- <= 0 )
							break;

						ResourceData* pNext = pResource->pNext;
						pResource = pNext;
					}

					if( sNo <= 0 )
					{
						s16 i;
						//	ファイル検索
						for( i = 0; i < m_ArcHeader.sFileNum; i++ )
						{
							if( m_pFileInfo[i].iHead == pResource->iHead )
								break;
						}

						//	ファイル情報セット
						Info.iSize = m_pFileInfo[i].iSize;
#if	KGL_DEBUG_FILE
						Info.pName = pResource->cName;
#endif	// KGL_DEBUG_FILE

						return Info;
					}
				}

				{
					DirectResourceData* pResource = m_DirectResource.pNext;
					//	ファイル検索
					for( s16 i=0; i < sNo; i++ )
						pResource = pResource->pNext;
					//	ファイル情報セット
					Info.iSize = pResource->iSize;
					Info.pName = pResource->cName;
				}

				return Info;
			}

		private:
			//----------------------------------------------------------------------
			//	ファイル情報の取得
			//!	@param pPath [in] パス
			//!	@return ファイル情報
			//----------------------------------------------------------------------
			FileInfo* GetFileInfo(const c8* pPath)
			{
				//	アーカイブが存在しなければNULLを返す
				if( m_pFileInfo == null )
					return null;

				//	フォルダー検索
				FolderInfo* pFolderInfo = GetFolderInfo(pPath);
				if( pFolderInfo == null )
				{
					//SystemTrace("フォルダーが見つかりませんでした(%s)", pPath);
					return null;
				}

				const c8* pTemp = strstr(pPath, ".");

				//	ファイル名の取得
				while( pTemp[-1] != '/' )
					pTemp --;
				//	ハッシュ値取得
				s32 iHash = m_pHash->Get(pTemp);
				//	ファイル情報取得
				FileInfo* pFileInfo = &m_pFileInfo[pFolderInfo->sHead];

				s16 i;
				//	ファイル検索
				for( i = 0; i < pFolderInfo->sFileNum; i++ )
				{
					if( iHash == pFileInfo[i].iHash )
						break;
				}

				//	ファイル検索失敗
				if( i == pFolderInfo->sFileNum )
				{
					//SystemTrace("ファイルが見つかりませんでした(%s)", pPath);
					return null;
				}

				return &pFileInfo[i];
			}

			//----------------------------------------------------------------------
			//	フォルダー情報の取得
			//!	@param pPath [in] パス
			//!	@param pFolderInfo [in] 検索中のフォルダー情報
			//!	@return フォルダー情報
			//----------------------------------------------------------------------
			FolderInfo* GetFolderInfo(const c8* pPath, FolderInfo* pFolderInfo = null)
			{
				kgStrcpy(s_cTemp, pPath);

				//	フォルダー名取得
				c8* pTemp = strstr(s_cTemp, "/");
				if( pTemp == null )
					return pFolderInfo;
				pTemp[0] = '\0';
				pTemp ++;

				s32 iHash = m_pHash->Get(s_cTemp);
				//	初期フォルダー確認
				if( pFolderInfo == null )
				{
					pFolderInfo = m_pFolderInfo;
					return (iHash == pFolderInfo->iHash)? GetFolderInfo(pTemp, pFolderInfo): null;
				}

				//	子フォルダーをセット
				pFolderInfo = &m_pFolderInfo[pFolderInfo->sChild];

				//	フォルダー検索
				while( true )
				{
					if( iHash == pFolderInfo->iHash == 0 )
						return GetFolderInfo(pTemp, pFolderInfo);

					if( pFolderInfo->sBrother < 0 )
						break;

					pFolderInfo = &m_pFolderInfo[pFolderInfo->sBrother];
				}

				return null;
			}

		private:
			string				m_sFileName;			//!< ファイル名
			File::IFile*		m_pFile;				//!< ファイルマネージャー
			File::IFile*		m_pReadFile;			//!< 単体ファイル読み込み用
			Hash::IHash*		m_pHash;				//!< ハッシュ
			ArchiveHeader		m_ArcHeader;			//!< アーカイブヘッダー
			FolderInfo*			m_pFolderInfo;			//!< フォルダー情報
			FileInfo*			m_pFileInfo;			//!< ファイル情報
			s16					m_sLoadCount;			//!< ロード中のファイル数
			s16					m_sResourceNum;			//!< 読み込み済みのファイル数
			ResourceData		m_Resource;				//!< リソースデータ
			ResourceData*		m_pEndResource;			//!< リソースデータ
			DirectResourceData	m_DirectResource;		//!< リソースデータ2
			DirectResourceData*	m_pDirectEndResource;	//!< リソースデータ2
		};

		//----------------------------------------------------------------------
		//	アーカイブマネージャーの生成
		//----------------------------------------------------------------------
		b8 Create(IArchiveManager** ppArchiveManager)
		{
			*ppArchiveManager = new CArchiveManager;
			(*ppArchiveManager)->AddRef();
			return true;
		}
	}
}
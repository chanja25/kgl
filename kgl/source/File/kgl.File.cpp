//----------------------------------------------------------------------
//!
//!	@file	kgl.File.cpp
//!	@brief	ファイル管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "File/kgl.File.h"

namespace kgl
{
	namespace File
	{
		//	Platform Dependent
		//======================================================================
		//!	ファイルマネージャークラス
		//======================================================================
		class CFile
			: public IFile
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CFile(void)
				: m_hFile(null)
				, m_uiSize(0)
			{}

		public:
			//----------------------------------------------------------------------
			//	ファイルを開く
			//----------------------------------------------------------------------
			b8 Open(const c8* pPath, s32 iOpen, s32 iShare, s32 iCreate, s32 iAttribute)
			{
				if( m_hFile )
				{
					Close();
					kgAssert(false, "ファイルが閉じられていません(CFile::Open)");
				}
				//	ファイルのアクセス権限を設定
				u32 uiAccess = 0;
				uiAccess |= (iOpen & EOpen::Read)?	GENERIC_READ:	0;
				uiAccess |= (iOpen & EOpen::Write)? GENERIC_WRITE:	0;
				//	ファイルの共有モードを設定
				u32 uiShare = 0;
				uiShare |= (iShare & EShare::Read)?		FILE_SHARE_READ:	0;
				uiShare |= (iShare & EShare::Write)?	FILE_SHARE_WRITE:	0;
				uiShare |= (iShare & EShare::Delete)?	FILE_SHARE_DELETE:	0;
				//	ファイルの生成モードを設定
				u32 uiCreate = 0;
				switch( iCreate )
				{
				case ECreate::CreateNew:	uiCreate = CREATE_NEW;		break;
				case ECreate::CreateAlways:	uiCreate = CREATE_ALWAYS;	break;
				case ECreate::OpenExisting:	uiCreate = OPEN_EXISTING;	break;
				case ECreate::OpenAlways:	uiCreate = OPEN_ALWAYS;		break;
				default:
					kgAssert(false, "Createモードに対応していない値が指定されました(CFile::Open)");
					uiCreate = OPEN_EXISTING;
					break;
				}
				//	ファイルの属性を設定
				u32 uiAttribute = 0;
				uiAttribute |= (iAttribute & EAttribute::Normal)?			FILE_ATTRIBUTE_NORMAL:		0;
				uiAttribute |= (iAttribute & EAttribute::Hidden)?			FILE_ATTRIBUTE_HIDDEN:		0;
				uiAttribute |= (iAttribute & EAttribute::ReadOnly)?			FILE_ATTRIBUTE_READONLY:	0;
				uiAttribute |= (iAttribute & EAttribute::System)?			FILE_ATTRIBUTE_SYSTEM:		0;
				uiAttribute |= (iAttribute & EAttribute::Temporary)?		FILE_ATTRIBUTE_TEMPORARY:	0;
				uiAttribute |= (iAttribute & EAttribute::DeleteOnClose)?	FILE_FLAG_DELETE_ON_CLOSE:	0;
				//	ファイルを開く
				m_hFile = CreateFileA(pPath,
									  uiAccess,
									  uiShare,
									  null,
									  uiCreate,
									  uiAttribute,
									  null);
				//	開けているか確認
				if( m_hFile == INVALID_HANDLE_VALUE )
				{
					WarningTrace("下記のファイルのオープンに失敗しました(CFile::Open)");
					WarningTrace("[%s]", pPath);
					m_hFile = null;
					return false;
				}
				return true;
			}

			//----------------------------------------------------------------------
			//	ファイルを閉じる
			//----------------------------------------------------------------------
			void Close(void)
			{
				if( m_hFile == null )
					return;

				CloseHandle(m_hFile);
				m_hFile		= null;
				m_uiSize	= 0;
			}

			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			u32 Read(void* pBuffer, u32 uiSize)
			{
				dword dwReadSize;
				//	データの読み込み
				::ReadFile(m_hFile, pBuffer, uiSize, &dwReadSize, null);

				return (u32)dwReadSize;
			}

			//----------------------------------------------------------------------
			//	書き込み
			//----------------------------------------------------------------------
			u32 Write(const void* pBuffer, u32 uiSize)
			{
				dword dwWriteSize;
				//	データの書き出し
				WriteFile(m_hFile, pBuffer, uiSize, &dwWriteSize, null);

				return (u32)dwWriteSize;
			}
			
			//----------------------------------------------------------------------
			//	ファイルサイズ取得
			//----------------------------------------------------------------------
			u32 GetSize(void)
			{
				if( m_uiSize == 0 )
				{
					m_uiSize = (u32)::GetFileSize(m_hFile, null);
				}
				return m_uiSize;
			}

			//----------------------------------------------------------------------
			//	ファイル位置のセット
			//----------------------------------------------------------------------
			u32 Seek(s32 iMove, s32 iPosition)
			{
				const s32 iPoint[] =
				{
					FILE_BEGIN,
					FILE_CURRENT,
					FILE_END,
				};

				return ::SetFilePointer(m_hFile, iMove, null, iPoint[iPosition]);
			}

			//----------------------------------------------------------------------
			//	ファイル位置の取得
			//----------------------------------------------------------------------
			u32 Tell(void)
			{
				return ::SetFilePointer(m_hFile, 0, null, FILE_CURRENT);
			}

		public:
			//----------------------------------------------------------------------
			//!	ファイルハンドルの取得
			//----------------------------------------------------------------------
			INLINE HANDLE GetHandle(void) { return m_hFile; }

		private:
			HANDLE	m_hFile;	//!< ファイルハンドル
			u32		m_uiSize;	//!< ファイルサイズ
		};

		//----------------------------------------------------------------------
		//	ファイルマネージャーの生成
		//----------------------------------------------------------------------
		b8 Create(IFile** ppFile)
		{
			*ppFile = new CFile;
			(*ppFile)->AddRef();
			return true;
		}

		//----------------------------------------------------------------------
		//	ファイル削除
		//----------------------------------------------------------------------
		b8 DeleteFile(const c8* pPath)
		{
			return ::DeleteFileA(pPath) != false;
		}

		//----------------------------------------------------------------------
		//	システムタイムに変換
		//----------------------------------------------------------------------
		void SetFileTime(FILETIME* pFileTime, const Date* pDate)
		{
			SYSTEMTIME SystemTime;
			FILETIME LocalFileTime;

			SystemTime.wYear			= pDate->usYeah;
			SystemTime.wMonth			= pDate->usMonth;
			SystemTime.wDayOfWeek		= pDate->usWeek;
			SystemTime.wDay				= pDate->usDay;
			SystemTime.wHour			= pDate->usHour;
			SystemTime.wMinute			= pDate->usMinute;
			SystemTime.wSecond			= pDate->usSecond;
			SystemTime.wMilliseconds	= pDate->usMilliSecond;

			SystemTimeToFileTime(&SystemTime, &LocalFileTime);
			LocalFileTimeToFileTime(&LocalFileTime, pFileTime);
		}

		//----------------------------------------------------------------------
		//	タイムスタンプの変更
		//----------------------------------------------------------------------
		b8 SetTimeStamp(const c8* pPath, const Date* pCreate, const Date* pAccess, const Date* pUpdate)
		{
			CFile File;
			//	ファイルを開く
			if( !File.Open(pPath, EOpen::Read|EOpen::Write, 0, ECreate::OpenExisting, EAttribute::Normal) )
			{
				return false;
			}

			FILETIME CreateTime, AccessTime, UpdateTime;
			FILETIME* pCreateTime = pCreate? &CreateTime: null;
			FILETIME* pAccessTime = pAccess? &AccessTime: null;
			FILETIME* pUpdateTime = pUpdate? &UpdateTime: null;

			//	タイムスタンプの取得
			GetFileTime(File.GetHandle(), pCreateTime, pAccessTime, pUpdateTime);

			if( pCreateTime )	SetFileTime(pCreateTime, pCreate);
			if( pAccessTime )	SetFileTime(pAccessTime, pAccess);
			if( pUpdateTime )	SetFileTime(pUpdateTime, pUpdate);

			//	更新時間の更新（更新不要な情報はNULLでＯＫ）
			SetFileTime(File.GetHandle(), pCreateTime, pAccessTime, pUpdateTime);

			//	ファイルを閉じる
			File.Close();

			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
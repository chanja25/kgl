//----------------------------------------------------------------------
//!
//!	@file	kgl.Directory.cpp
//!	@brief	ディレクトリ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Time/kgl.Time.h"
#include "File/kgl.Directory.h"
#include <imagehlp.h>

using namespace kgl::algorithm;

//----------------------------------------------------------------------
// lib
//----------------------------------------------------------------------
#pragma comment(lib, "imagehlp.lib")

namespace kgl
{
	namespace Time
	{
		//	ファイルタイムから日時取得
		extern void FileTimeToDate(Date* pDate, const FILETIME* pTime);
	}

	namespace Directory
	{
		//	Platform Dependent
		//----------------------------------------------------------------------
		//	ディレクトリ作成
		//----------------------------------------------------------------------
		b8 Create(const c8* pPath)
		{
			string strPath = pPath;
			u32 iLength = strPath.length();
			for( u32 i=0; i < iLength; i++ )
			{
				if( strPath[i] == '/' )	strPath[i] = '\\';
			}
			if( strPath[iLength - 1] != '\\' ) strPath += '\\';

			return MakeSureDirectoryPathExists(strPath.c_str()) != false;
		}

		//----------------------------------------------------------------------
		//	ディレクトリ削除(ファイルが存在する場合は失敗)
		//----------------------------------------------------------------------
		b8 Remove(const c8* pPath)
		{
			string strPath = pPath;
			u32 iLength = strPath.length();
			for( u32 i=0; i < iLength; i++ )
			{
				if( strPath[i] == '/' )	strPath[i] = '\\';
			}
			if( strPath[iLength - 1] != '\\' ) strPath += '\\';

			return RemoveDirectoryA(strPath.c_str()) != false;
		}

		//======================================================================
		//!	ディレクトリマネージャークラス
		//======================================================================
		class CDirectory
			: public IDirectory
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CDirectory(void)
				: m_hFind(null), m_uiAttribute(EAttribute::All)
			{
			}

			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CDirectory(void)
			{
				Close();
			}

		public:
			//----------------------------------------------------------------------
			//	ファイル情報設定
			//----------------------------------------------------------------------
			void SetupInfo(const WIN32_FIND_DATAA& fd, Info* pInfo, u32 uiAttribute)
			{
				if( pInfo == null )	return;
				kgZeroMemory(pInfo, sizeof(Info));
				//	情報設定
				kgStrcpy(pInfo->cName, fd.cFileName);
				pInfo->u64Size		= ((u64)fd.nFileSizeLow & 0x00000000ffffffffull) | (((u64)fd.nFileSizeHigh << 32) & 0xffffffff00000000ull);
				pInfo->uiAttribute	= uiAttribute;
				//	時刻取得
				Time::FileTimeToDate(&pInfo->Create, &fd.ftCreationTime);
				Time::FileTimeToDate(&pInfo->Access, &fd.ftLastAccessTime);
				Time::FileTimeToDate(&pInfo->Update, &fd.ftLastWriteTime);
			}

			//----------------------------------------------------------------------
			//	ファイル検索
			//----------------------------------------------------------------------
			const c8* FindFile(const c8* pPath, Info* pInfo, u32 uiAttribute)
			{
				Close();

				WIN32_FIND_DATAA fd;
				//	ファイル検索
				m_hFind = FindFirstFileA(pPath, &fd);
				//	ファイルの有無確認
				b8 bResult = (m_hFind != INVALID_HANDLE_VALUE);

				m_uiAttribute = uiAttribute;
				//	ファイルを検索していく
				while( bResult )
				{
					if( kgStrcmp(fd.cFileName, ".") && kgStrcmp(fd.cFileName, "..") )
					{
						dword dwAttribute = fd.dwFileAttributes;
						uiAttribute = 0;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_DIRECTORY)?	EAttribute::Directory:		EAttribute::File;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_HIDDEN)?		EAttribute::Hidden:			0;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_READONLY)?		EAttribute::ReadOnly:		0;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_SYSTEM)?		EAttribute::System:			0;

						if( m_uiAttribute & uiAttribute )
						{
							kgStrcpy(m_cFileName, fd.cFileName);

							SetupInfo(fd, pInfo, uiAttribute);

							return m_cFileName;
						}
					}
					//	次のファイルを取得
					bResult = !!FindNextFileA(m_hFind, &fd);
				}
				//	閉じる
				Close();

				return null;
			}

			//----------------------------------------------------------------------
			//	ファイル検索
			//----------------------------------------------------------------------
			const c8* NextFile(Info* pInfo)
			{
				WIN32_FIND_DATAA fd;
				//	次のファイルを取得
				b8 bResult = FindNextFileA(m_hFind, &fd) != false;

				u32 uiAttribute = 0;
				//	ファイルを検索していく
				while( bResult )
				{
					if( kgStrcmp(fd.cFileName, ".") && kgStrcmp(fd.cFileName, "..") )
					{
						dword dwAttribute = fd.dwFileAttributes;
						uiAttribute = 0;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_DIRECTORY)?	EAttribute::Directory:		EAttribute::File;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_HIDDEN)?		EAttribute::Hidden:			0;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_READONLY)?		EAttribute::ReadOnly:		0;
						uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_SYSTEM)?		EAttribute::System:			0;

						if( m_uiAttribute & uiAttribute )
						{
							kgStrcpy(m_cFileName, fd.cFileName);

							SetupInfo(fd, pInfo, uiAttribute);

							return m_cFileName;
						}
					}
					//	次のファイルを取得
					bResult = !!FindNextFileA(m_hFind, &fd);
				}

				return null;
			}

		private:
			//----------------------------------------------------------------------
			//	検索用ハンドルを閉じる
			//----------------------------------------------------------------------
			void Close(void)
			{
				if( m_hFind )
				{
					FindClose(m_hFind);
					m_hFind = null;
				}
			}

		private:
			HANDLE	m_hFind;			//!< ファイル検索用ハンドル
			c8		m_cFileName[256];	//!< ファイル名
			u32		m_uiAttribute;		//!< 検索する属性
		};

		//----------------------------------------------------------------------
		//!	ディレクトリマネージャーの生成
		//----------------------------------------------------------------------
		b8 Create(IDirectory** ppDirectory)
		{
			CDirectory* pDirectory = new CDirectory;
			pDirectory->AddRef();
			*ppDirectory = pDirectory;
			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
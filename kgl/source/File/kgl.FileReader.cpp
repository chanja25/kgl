//----------------------------------------------------------------------
//!
//!	@file	kgl.FileReader.cpp
//!	@brief	ファイル読み込み管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "File/kgl.FileReader.h"
#include "File/kgl.File.h"
#include "File/kgl.Directory.h"
#include "Thread/kgl.Thread.h"
#include "Thread/kgl.Event.h"
#include "Thread/kgl.CriticalSection.h"
#include "Compress/kgl.zlib.h"
#include "Utility/Hash/Kgl.Hash.h"
#include "Utility/Path/kgl.Path.h"

namespace kgl
{
	namespace File
	{
		using namespace Directory;

		namespace Archive
		{
			//	アーカイブヘッダー
			struct ArchiveHeader
			{
				s32	iID;			//!< 識別子
				s32	iVersion;		//!< バージョン
				s16 sFolderNum;		//!< フォルダー情報数
				s16 sFileNum;		//!< ファイル情報数
				s32 iInfoSize;		//!< フォルダー・ファイル情報サイズ
				s32 iArchiveSize;	//!< アーカイブサイズ
				s32	iArchiveOffset;	//!< ファイルの先頭位置
			};

			//	フォルダー情報
			struct FolderInfo
			{
				s32	iHash;		//!< フォルダー名のハッシュ値
				s16	sHead;		//!< ファイル情報の先頭位置
				s16	sFileNum;	//!< ファイル数
				s16	sChild;		//!< 子のフォルダー
				s16	sBrother;	//!< 次のフォルダー
			};

			//	ファイル情報
			struct FileInfo
			{
				s32	iHash;		//!< ファイル名のハッシュ値
				s32	iHead;		//!< ファイルの先頭位置
				s32	iArcSize;	//!< 圧縮ファイルサイズ
				s32	iSize;		//!< ファイルサイズ
			};
		}

		//!	モード
		namespace EModeFlag
		{
			const s32 Exit		= 0x00000001;	//!< 終了
			const s32 Block		= 0x00000002;	//!< ブロック読み要求
		}

		//!	ロード情報
		struct LOAD_INFO
		{
			s32				iIndex;				//!< ロード番号
			c8				cPath[LoadPathMax];	//!< パス
			u32				uiFileSize;			//!< ファイルのサイズ
			u32				uiSize;				//!< 解凍後のサイズ
			void*			pBuffer;			//!< 解凍用バッファ
			FLoadCallback	Callback;			//!< コールバック
			void*			pArg;				//!< コールバックの引数
		};

		//!	ファイル管理を行っていない情報
		struct FILE_INFO
		{
			void*						pAddress;			//!< ファイルのアドレス
			TKGLPtr<IExtraObject>	pExtra;				//!< 拡張データ
			s32							iHash;				//!< ハッシュ値(crc32)
			u32							uiSize;				//!< サイズ
			c8							cPath[LoadPathMax];	//!< パス
			FILE_INFO*					pNext;				//!< 次のファイル
		};

		//!	ファイル情報
		struct ARCHIVE_INFO
		{
			void*						pAddress;			//!< ファイルのアドレス
			TKGLPtr<IExtraObject>	pExtra;				//!< 拡張データ
			s32							iHead;				//!< ファイルの先頭位置
			u32							uiSize;				//!< サイズ
			FILE_INFO*					pNext;				//!< 次のファイル
#if	KGL_DEBUG_FILE
			c8							cPath[LoadPathMax];	//!< パス(デバッグ用)
#endif	// ~#if KGL_DEBUG_FILE
		};

		//!	バインド情報
		struct BindInfo
		{
			algorithm::string	strPath;	//!< パス
			algorithm::string	strBind;	//!< バインド後のパス
		};

		//======================================================================
		//!	ファイル読み込み管理インターフェイス
		//======================================================================
		class CFileReader
			: public IFileReader
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CFileReader(const c8* pPath, const c8* pRootDirectory)
				: m_pFile(null)
				, m_pArchiveFile(null)
				, m_pLoadThread(null)
				, m_pLoadEvent(null)
				, m_pLoadCS(null)
				, m_uiNowRequest(0)
				, m_uiRequestCount(0)
				, m_uiNow(0)
				, m_iFlag(0)
				, m_pCS(null)
				, m_pBlockEvent(null)
				, m_pDecompressThread(null)
				, m_pDecompressEvent(null)
				, m_uiDecompressCount(0)
				, m_uiNowDecompress(0)
				, m_pFileInfo(null)
				, m_pFileInfoEnd(null)
				, m_pArchiveInfo(null)
				, m_pArchiveInfoEnd(null)
				, m_uiArchiveCount(0)
			{
				kgZeroMemory(m_Info, sizeof(m_Info));
				kgZeroMemory(&m_BlockLoad, sizeof(m_BlockLoad));

				Initialize(pPath, pRootDirectory);
			}
			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CFileReader(void)
			{
				Finalize();
			}

		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const c8* pPath, const c8* pRootDirectory)
			{
				if (m_pFile.IsValid())
				{
					WarningTrace("既に初期化されています(CFileReader::Initialize)");
					return true;
				}

				m_uiNowRequest		= 0;
				m_uiRequestCount	= 0;
				m_uiNow				= 0;
				m_iFlag				= 0;

				m_uiDecompressCount	= 0;
				m_uiNowDecompress	= 0;

				kgZeroMemory(m_Info, sizeof(m_Info));
				kgZeroMemory(&m_BlockLoad, sizeof(m_BlockLoad));

				m_pFileInfo			= null;
				m_pFileInfoEnd		= null;

				if( pRootDirectory && pRootDirectory[0] != '\0' )
				{
					CharSet::SFormat(m_cRootDir, "%s/", pRootDirectory);
				}
				else
				{
					kgZeroMemory(m_cRootDir, sizeof(m_cRootDir));
				}

				if( pPath )
				{
					//	使用するアーカイブファイルの読み込み
					Create(m_pArchiveFile.GetReference());
					if( !m_pArchiveFile->Open(pPath) )
					{
						WarningTrace("アーカイブファイルが見つかりませんでした(CFileReader::Initialize)", pPath);
						m_pArchiveFile = null;
						//return false;
					}
				}

				Create(m_pFile.GetReference());
				Create(m_pDirectory.GetReference());
				Hash::CreateCRC32(m_pHash.GetReference());
				Thread::Create(m_pLoadCS.GetReference());
				Thread::Create(m_pLoadEvent.GetReference());
				Thread::Create(m_pLoadThread.GetReference());
				Thread::Create(m_pCS.GetReference());
				Thread::Create(m_pBlockEvent.GetReference());
				Thread::Create(m_pDecompressEvent.GetReference());
				Thread::Create(m_pDecompressThread.GetReference());
				//	ロードスレッドの起動
				m_pLoadThread->Start(LoadThreadFunction, this, KB(4), "LoadThread");
				//	解凍スレッドの起動
				m_pDecompressThread->Start(DecompressThreadFunction, this, KB(4), "DecompressThread");

				return true;
			}

			//----------------------------------------------------------------------
			//	解放
			//----------------------------------------------------------------------
			void Finalize(void)
			{
				if( m_pFile == null )
				{
					WarningTrace("初期化されていません(CFileReader::Finalize)");
					return;
				}
				//	ロードスレッドの終了要求
				SetFlag(EModeFlag::Exit);
				m_pDecompressEvent->SetSignal();
				m_pLoadEvent->SetSignal();
				//	ロードスレッドの終了待ち
				m_pDecompressThread->Exit();
				m_pLoadThread->Exit();

				//	ロード済みのデータを全て解放する
				UnloadAll();

				m_pDecompressThread	= null;
				m_pDecompressEvent	= null;
				m_pBlockEvent		= null;
				m_pCS				= null;
				m_pLoadThread		= null;
				m_pLoadEvent		= null;
				m_pLoadCS			= null;
				m_pFile				= null;

				if (m_pArchiveFile.IsValid())
				{
					m_pArchiveFile->Close();
					m_pArchiveFile = null;
				}
			}

		public:
			//----------------------------------------------------------------------
			//	読み込み要求(非同期読み込み)
			//----------------------------------------------------------------------
			b8 Load(const c8* pPath, FLoadCallback Callback, void* pArg)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				if( IsLoaded(cPath) )
				{
					WarningTrace("%sは既に読み込み済みです(CFileReader::Load)", pPath);
					return false;
				}

				{
					SCOPE_LOCK_CRITICALSECTION(m_pCS);
					if( m_uiRequestCount == LoadRequestMax )
					{
						WarningTrace("ロードリクエスト数が最大を超えました(CFileReader::Load)");
						return false;
					}

					s32 iIndex = GetIndex(cPath);
					//	ロード情報をスタックしておく
					LOAD_INFO* pInfo = &m_Info[m_uiNow];
					kgZeroMemory(pInfo, sizeof(LOAD_INFO));
					//	アーカイブ内に存在しない場合はパスから直接読み込む
					if( iIndex == NoManagerIndex )
					{
						kgStrcpy(pInfo->cPath, cPath);
					}
					pInfo->iIndex = iIndex;
					pInfo->Callback = Callback;
					pInfo->pArg = pArg;

					m_uiRequestCount ++;
					m_uiNow ++;
					if( m_uiNow == LoadRequestMax )
					{
						m_uiNow = 0;
					}
				}

				m_pLoadEvent->SetSignal();

				return true;
			}

			//----------------------------------------------------------------------
			//	読み込み要求(非同期読み込み)
			//----------------------------------------------------------------------
			b8 Load(s32 iIndex, FLoadCallback Callback, void* pArg)
			{
				if( IsLoaded(iIndex) )
				{
					WarningTrace("%dは既に読み込み済みです(CFileReader::Load)", iIndex);
					return false;
				}

				{
					SCOPE_LOCK_CRITICALSECTION(m_pCS);
					if( m_uiRequestCount == LoadRequestMax )
					{
						WarningTrace("ロードリクエスト数が最大を超えました(CFileReader::Load)");
						return false;
					}

					//	ロード情報をスタックしておく
					LOAD_INFO* pInfo = &m_Info[m_uiNow];
					kgZeroMemory(pInfo, sizeof(LOAD_INFO));
					pInfo->iIndex = iIndex;
					pInfo->Callback = Callback;
					pInfo->pArg = pArg;

					m_uiRequestCount ++;
					m_uiNow ++;
					if( m_uiNow == LoadRequestMax )
					{
						m_uiNow = 0;
					}
				}

				m_pLoadEvent->SetSignal();

				return true;
			}

			//----------------------------------------------------------------------
			//	読み込み(ブロック関数)
			//----------------------------------------------------------------------
			const void* LoadBlock(const c8* pPath, FLoadCallback Callback, void* pArg)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				const void* pData = GetAddr(cPath);
				if( pData )
				{
					WarningTrace("%sは既に読み込み済みです(CFileReader::LoadBlock)", pPath);
					return pData;
				}

				s32 iIndex = GetIndex(cPath);
				{
					LOAD_INFO* pInfo = &m_BlockLoad;
					kgZeroMemory(pInfo, sizeof(LOAD_INFO));
					//	アーカイブ内に存在しない場合はパスから直接読み込む
					if( iIndex == NoManagerIndex )
					{
						kgStrcpy(pInfo->cPath, cPath);
					}
					pInfo->iIndex = iIndex;
					pInfo->Callback = Callback;
					pInfo->pArg = pArg;
					//	ブロック読み要求
					SetFlag(EModeFlag::Block);
					m_pLoadEvent->SetSignal();

					m_pBlockEvent->Wait();
					//	圧縮されている場合は解凍しておく
					if( m_BlockLoad.pBuffer )
					{
						Decompress(&m_BlockLoad);
					}
				}
				//	読み込が成功した場合はデータを返す
				if( iIndex == NoManagerIndex )
				{
					return GetAddr(cPath);
				}
				return GetAddr(iIndex);
			}

			//----------------------------------------------------------------------
			//	読み込み(ブロック関数)
			//----------------------------------------------------------------------
			const void* LoadBlock(s32 iIndex, FLoadCallback Callback, void* pArg)
			{
				const void* pData = GetAddr(iIndex);
				if( pData )
				{
					WarningTrace("%dは既に読み込み済みです(CFileReader::LoadBlock)", iIndex);
					return pData;
				}

				{
					LOAD_INFO* pInfo = &m_BlockLoad;
					kgZeroMemory(pInfo, sizeof(LOAD_INFO));
					pInfo->iIndex = iIndex;
					pInfo->Callback = Callback;
					pInfo->pArg = pArg;
					//	ブロック読み要求
					SetFlag(EModeFlag::Block);
					m_pLoadEvent->SetSignal();

					m_pBlockEvent->Wait();
				}
				//	読み込が成功した場合はデータを返す
				return GetAddr(iIndex);
			}

			//----------------------------------------------------------------------
			//	ファイル取得
			//----------------------------------------------------------------------
			b8 GetFile(File::IFile** ppFile, const c8* pPath, s32* pSize)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				//	アーカイブ内に存在しない場合はパスから直接読み込む
				if( iIndex == NoManagerIndex )
				{
					*ppFile = OpenFile(cPath, pSize);
				}
				else
				{
					*ppFile = OpenFile(iIndex, pSize);
				}

				return *ppFile != null;
			}

			//----------------------------------------------------------------------
			//	ファイル取得
			//----------------------------------------------------------------------
			b8 GetFile(File::IFile** ppFile, s32 iIndex, s32* pSize)
			{
				*ppFile = OpenFile(iIndex, pSize);
				return *ppFile != null;
			}

			//----------------------------------------------------------------------
			//	解放要求
			//----------------------------------------------------------------------
			void Unload(const c8* pPath)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				if( iIndex == NoManagerIndex )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);
					//	文字列比較に掛る負荷を軽減するために先にハッシュ値を使用する
					s32 iHash = m_pHash->Get(cPath);
					FILE_INFO* pInfo		= m_pFileInfo;
					FILE_INFO* pInfoPrev	= null;
					while( pInfo )
					{
						if( pInfo->iHash == iHash )
						{
							if( kgStrcmp(pInfo->cPath, cPath) == 0 )
							{
								DeleteFileInfo(pInfo, pInfoPrev);
								break;
							}
						}
						pInfoPrev	= pInfo;
						pInfo		= pInfo->pNext;
					}
				}
				else
				{
					Unload(iIndex);
				}
			}

			//----------------------------------------------------------------------
			//	解放要求
			//----------------------------------------------------------------------
			void Unload(s32 iIndex)
			{
				(void)iIndex;
				//	todo: 未実装
			}

			//----------------------------------------------------------------------
			//	解放要求
			//----------------------------------------------------------------------
			void Unload(const void* pData)
			{
				if( pData == null )
				{
					return;
				}

				SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);

				{
					FILE_INFO* pInfo		= m_pFileInfo;
					FILE_INFO* pInfoPrev	= null;
					while( pInfo )
					{
						//	アドレスが一致したらデータを削除する
						if( pInfo->pAddress == pData )
						{
							DeleteFileInfo(pInfo, pInfoPrev);
							break;
						}
						pInfoPrev	= pInfo;
						pInfo		= pInfo->pNext;
					}
				}

				//	todo: 未実装
				{
				}
			}

			//----------------------------------------------------------------------
			//	解放要求
			//----------------------------------------------------------------------
			void Unload(IExtraObject* pExtra)
			{
				if( pExtra == null )
				{
					return;
				}

				SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);

				{
					FILE_INFO* pInfo		= m_pFileInfo;
					FILE_INFO* pInfoPrev	= null;
					while( pInfo )
					{
						//	アドレスが一致したらデータを削除する
						if( pInfo->pExtra == pExtra )
						{
							DeleteFileInfo(pInfo, pInfoPrev);
							break;
						}
						pInfoPrev	= pInfo;
						pInfo		= pInfo->pNext;
					}
				}

				//	todo: 未実装
				{
				}
			}

			//----------------------------------------------------------------------
			//	全てのデータを破棄する
			//----------------------------------------------------------------------
			void UnloadAll(void)
			{
				//	ロード済みのデータを解放する(保険処理)
				while( m_pFileInfo )
				{
					if (m_pFileInfo->pExtra.IsValid())	Unload(m_pFileInfo->pExtra);
					else								Unload(m_pFileInfo->pAddress);
				}
				while( m_pArchiveInfo )
				{
					if (m_pArchiveInfo->pExtra.IsValid())	Unload(m_pArchiveInfo->pExtra);
					else									Unload(m_pArchiveInfo->pAddress);
				}
			}

			//----------------------------------------------------------------------
			//	データ取得
			//----------------------------------------------------------------------
			const void* GetAddr(const c8* pPath)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				if( iIndex == NoManagerIndex )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);
					//	単体で読み込みが行われたデータ
					FILE_INFO* pInfo = FindFileInfo(cPath);
					if( pInfo )
					{
						return pInfo->pAddress? pInfo->pAddress: pInfo->pExtra;
					}
					return null;
				}
				//	アーカイブで管理されているデータ
				return GetAddr(iIndex);
			}

			//----------------------------------------------------------------------
			//	データ取得
			//----------------------------------------------------------------------
			const void* GetAddr(s32 iIndex)
			{
				(void)iIndex;
				//	todo: 未実装
				return null;
			}

			//----------------------------------------------------------------------
			//	データサイズ取得
			//----------------------------------------------------------------------
			u32 GetSize(const c8* pPath)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				if( iIndex == NoManagerIndex )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);
					//	単体で読み込みが行われたデータ
					FILE_INFO* pInfo = FindFileInfo(cPath);
					if( pInfo )
					{
						return pInfo->uiSize;
					}
					return 0;
				}
				//	アーカイブで管理されているデータ
				return GetSize(iIndex);
			}

			//----------------------------------------------------------------------
			//	データサイズ取得
			//----------------------------------------------------------------------
			u32 GetSize(s32 iIndex)
			{
				(void)iIndex;
				//	todo: 未実装
				return 0;
			}

			//----------------------------------------------------------------------
			//	データサイズ取得
			//----------------------------------------------------------------------
			u32 GetSize(const void* pData)
			{
				if( pData == null )
				{
					return 0;
				}

				SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);

				{
					FILE_INFO* pInfo		= m_pFileInfo;
					FILE_INFO* pInfoPrev	= null;
					while( pInfo )
					{
						//	アドレスが一致したらデータを削除する
						if( pInfo->pAddress == pData )
						{
							return pInfo->uiSize;
						}
						pInfoPrev	= pInfo;
						pInfo		= pInfo->pNext;
					}
				}

				//	todo: 未実装
				{
				}

				return 0;
			}

			//----------------------------------------------------------------------
			//	拡張データの設定
			//----------------------------------------------------------------------
			b8 SetLoadedObject(const c8* pPath, IExtraObject* pExtra, b8 bDelete)
			{
				if( pExtra == null )
				{
					return false;
				}

				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				if( iIndex == NoManagerIndex )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);
					//	単体で読み込みが行われたデータ
					FILE_INFO* pInfo = FindFileInfo(cPath);
					if( pInfo && !pInfo->pExtra.IsValid() )
					{
						//	データの解放
						if( bDelete && pInfo->pAddress )
						{
							Memory::Free(pInfo->pAddress);
							pInfo->pAddress = null;
						}
						pInfo->pExtra = pExtra;
						pExtra->SetOwner(this, pInfo->pAddress);
						return true;
					}
					return false;
				}
				//	アーカイブで管理されているデータ
				return SetLoadedObject(iIndex, pExtra, bDelete);
			}
			//----------------------------------------------------------------------
			//	拡張データの設定
			//----------------------------------------------------------------------
			b8 SetLoadedObject(s32 iIndex, IExtraObject* pExtra, b8 bDelete)
			{
				if( pExtra == null )
				{
					return false;
				}

				(void)iIndex;
				(void)bDelete;
				//	todo: 未実装
				return false;
			}
			//----------------------------------------------------------------------
			//	データ取得(拡張データを含む)
			//----------------------------------------------------------------------
			LoadedObject GetLoadedObject(const c8* pPath)
			{
				LoadedObject Object;
				Object.pData	= null;
				Object.pExtra	= null;

				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				if( iIndex == NoManagerIndex )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);
					//	単体で読み込みが行われたデータ
					FILE_INFO* pInfo = FindFileInfo(cPath);
					if( pInfo )
					{
						Object.pData	= pInfo->pAddress;
						Object.pExtra	= pInfo->pExtra;
					}
					return Object;
				}
				//	アーカイブで管理されているデータ
				return GetLoadedObject(iIndex);
			}
			//----------------------------------------------------------------------
			//	データ取得(拡張データを含む)
			//----------------------------------------------------------------------
			LoadedObject GetLoadedObject(s32 iIndex)
			{
				LoadedObject Object;

				Object.pData	= null;
				Object.pExtra	= null;

				(void)iIndex;
				//	todo: 未実装
				return Object;
			}

		public:
			//----------------------------------------------------------------------
			//	ロード中か？
			//----------------------------------------------------------------------
			b8 IsLoading(void)
			{
				return (m_uiRequestCount != 0 || m_uiDecompressCount != 0);
			}

			//----------------------------------------------------------------------
			//	ロードされているか？
			//----------------------------------------------------------------------
			b8 IsLoaded(const c8* pPath)
			{
				c8 cPath[LoadPathMax];
				ConvertSearchPath(cPath, pPath);

				s32 iIndex = GetIndex(cPath);
				//	単体で読み込みが行われたデータ
				if( iIndex == NoManagerIndex )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);

					return FindFileInfo(cPath) != null;
				}
				//	アーカイブで管理されているデータ
				return IsLoaded(iIndex);
			}

			//----------------------------------------------------------------------
			//	ロードされているか？
			//----------------------------------------------------------------------
			b8 IsLoaded(s32 iIndex)
			{
				(void)iIndex;
				//	todo:未実装
				return false;
			}

		public:
			//----------------------------------------------------------------------
			//	ロード終了待ち
			//----------------------------------------------------------------------
			void WaitLoad(void)
			{
				while( IsLoading() )
				{
					kgSleep(0.001f);
				}
			}

		public:
			//----------------------------------------------------------------------
			//	パスのバインド
			//----------------------------------------------------------------------
			void BindPath(const c8* pPath, const c8* pBind)
			{
				c8 cPath[LoadPathMax];
				c8 cBind[LoadPathMax];
				ConvertSearchPath(cPath, pPath, false);
				ConvertSearchPath(cBind, pBind, false);

				for( u32 i=0; i < m_BindInfo.size(); i++ )
				{
					if( m_BindInfo[i].strPath == cPath )
					{
						m_BindInfo[i].strBind = cBind;
						return;
					}
				}

				BindInfo Info;
				Info.strPath = cPath;
				Info.strBind = cBind;
				m_BindInfo.push_back(Info);
			}

		private:
			//----------------------------------------------------------------------
			//	フラグの設定
			//----------------------------------------------------------------------
			FILE_INFO* FindFileInfo(const c8* pPath)
			{
				//	文字列比較に掛る負荷を軽減するためにハッシュ値を使用する
				s32 iHash = m_pHash->Get(pPath);
				FILE_INFO* pInfo = m_pFileInfo;
				while( pInfo )
				{
					//	パスが一致したらデータを返す
					if( pInfo->iHash == iHash )
					{
						if( kgStrcmp(pInfo->cPath, pPath) == 0 )
						{
							return pInfo;
						}
					}
					pInfo = pInfo->pNext;
				}
				return null;
			}

		private:
			//----------------------------------------------------------------------
			//	フラグの設定
			//----------------------------------------------------------------------
			INLINE void SetFlag(s32 iFlag)		{ m_iFlag |= iFlag; }
			//----------------------------------------------------------------------
			//	フラグのクリア
			//----------------------------------------------------------------------
			INLINE void ClearFlag(s32 iFlag)	{ m_iFlag &= ~iFlag; }
			//----------------------------------------------------------------------
			//	フラグのチェック
			//----------------------------------------------------------------------
			INLINE b8 CheckFlag(s32 iFlag)		{ return !!(m_iFlag & iFlag); }

		private:
			//----------------------------------------------------------------------
			//	解放要求
			//----------------------------------------------------------------------
			void DeleteFileInfo(FILE_INFO* pInfo, FILE_INFO* pInfoPrev)
			{
				m_uiFileCount --;
				//	読み込み済みのファイルリストから除外しておく
				if( m_pFileInfo	== pInfo )
				{
					m_pFileInfo = pInfo->pNext;
				}
				if( pInfo == m_pFileInfoEnd )
				{
					m_pFileInfoEnd = pInfoPrev;
				}

				if( pInfoPrev )
				{
					pInfoPrev->pNext = pInfo->pNext;
				}
				//	データの解放
				if( pInfo->pAddress && !pInfo->pExtra )
				{
					Memory::Free(pInfo->pAddress);
				}
				SafeDelete(pInfo);
			}

			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 ReadFile(LOAD_INFO* pInfo)
			{
				if( pInfo->iIndex == NoManagerIndex )
				{
					c8 cPath[LoadPathMax];
					ConvertFilePath(cPath, pInfo->cPath);

					Directory::Info Info;
					m_pDirectory->FindFile(cPath, &Info);
					if( !(Info.uiAttribute & Directory::EAttribute::Directory) )
					{
						if( !ReadFileDirect(pInfo) )
						{
							return false;
						}
					}
					else
					{
						c8 cDirectory[LoadPathMax];
						CharSet::SFormat(cDirectory, "%s/*", cPath);
						const c8* pPath = m_pDirectory->FindFile(cDirectory, null, Directory::EAttribute::File);
						cDirectory[kgStrlen(cDirectory)-2] = '\0';
						while( pPath )
						{
							LOAD_INFO* pLoadInfo = null;
							CharSet::SFormat(cPath, "%s/%s", cDirectory, pPath);
							{
								SCOPE_LOCK_CRITICALSECTION(m_pCS);
								//	ロード情報のワークを取得
								pLoadInfo = &m_Info[m_uiNow];
								kgZeroMemory(pLoadInfo, sizeof(LOAD_INFO));
								ConvertSearchPath(pLoadInfo->cPath, cPath);
								pLoadInfo->iIndex	= pInfo->iIndex;
								pLoadInfo->Callback	= pInfo->Callback;
								pLoadInfo->pArg		= pInfo->pArg;

								m_uiRequestCount ++;
								m_uiNow ++;
								if( m_uiNow == LoadRequestMax )
								{
									m_uiNow = 0;
								}
							}
							ReadFileDirect(pLoadInfo);

							pPath = m_pDirectory->NextFile();
						}
						if( pInfo->Callback )
						{
							pInfo->Callback(null, pInfo->pArg, pInfo->uiSize, pInfo->cPath, pInfo->iIndex);
						}
					}
				}
				else
				{
					//	todo:アーカイブ内のデータを読み込む
				}

				return true;
			}
			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 ReadFileDirect(LOAD_INFO* pInfo)
			{
				void* pBuffer = null;
				b8 bDecompress = false;

				c8 cPath[LoadPathMax];
				ConvertFilePath(cPath, pInfo->cPath);
				//	パスから直接ファイルを読み込む
				if( !m_pFile->Open(cPath) )
				{
					return false;
				}
				pInfo->uiSize = m_pFile->GetSize();
				pBuffer = Memory::Allocate(pInfo->uiSize);

				if( pBuffer )
				{
					u8* pRead = (u8*)pBuffer;
					u32 uiBufferSize = pInfo->uiFileSize? pInfo->uiFileSize: pInfo->uiSize;
					do
					{
						u32 uiReadSize = Min<u32>(1024 * 128, uiBufferSize);
						m_pFile->Read(pRead, uiReadSize);

						uiBufferSize -= uiReadSize;
						pRead += uiReadSize;
					}
					while( uiBufferSize > 0  && !CheckFlag(EModeFlag::Exit) );
				}
				m_pFile->Close();

				if( pBuffer == null || CheckFlag(EModeFlag::Exit) )
				{
					if( pBuffer )
					{
						Memory::Free(pBuffer);
					}
					return false;
				}

				//	圧縮されているファイルの場合は解凍要請を出す
				if( bDecompress )
				{
					kgAtomicIncrement(m_uiDecompressCount);
					m_pDecompressEvent->SetSignal();
				}
				else
				{
					//	データを管理リストに追加
					SetFileInfo(pInfo, pBuffer);
				}
				return true;
			}
			//----------------------------------------------------------------------
			//	ファイルを開く
			//----------------------------------------------------------------------
			File::IFile* OpenFile(const c8* pPath, s32* pSize)
			{
				c8 cPath[LoadPathMax];
				ConvertFilePath(cPath, pPath);

				Directory::Info Info;
				if( m_pDirectory->FindFile(cPath, &Info) != null )
				{
					if( Info.uiAttribute & Directory::EAttribute::File )
					{
						TKGLPtr<File::IFile> pFile;
						File::Create(pFile.GetReference());
						if( pFile->Open(cPath) )
						{
							if( pSize )
							{
								*pSize = pFile->GetSize();
							}
							pFile->AddRef();
							return pFile;
						}
					}
				}
				return null;
			}
			//----------------------------------------------------------------------
			//	ファイルを開く
			//----------------------------------------------------------------------
			File::IFile* OpenFile(s32 iIndex, s32* pSize)
			{
				(void)iIndex;
				(void)pSize;
				//	todo: 未実装
				return null;
			}

			//----------------------------------------------------------------------
			//	ファイル情報の登録
			//----------------------------------------------------------------------
			void SetFileInfo(LOAD_INFO* pInfo, void* pBuffer)
			{
				{
					SCOPE_LOCK_CRITICALSECTION(m_pLoadCS);

					if( pInfo->iIndex == NoManagerIndex )
					{
						//	未管理のファイル情報登録
						FILE_INFO* pFileInfo = new FILE_INFO;
						kgStrcpy(pFileInfo->cPath, pInfo->cPath);
						pFileInfo->iHash	= m_pHash->Get(pFileInfo->cPath);
						pFileInfo->uiSize	= pInfo->uiSize;
						pFileInfo->pAddress	= pBuffer;
						pFileInfo->pExtra	= null;
						pFileInfo->pNext	= null;

						if( m_pFileInfo == null )
						{
							m_pFileInfo = m_pFileInfoEnd = pFileInfo;
						}
						else
						{
							m_pFileInfoEnd->pNext = pFileInfo;
							m_pFileInfoEnd = pFileInfo;
						}
						m_uiFileCount ++;
					}
					else
					{
						//	todo: 未実装
					}
				}
				
				if( pInfo->Callback )
				{
					pInfo->Callback(pBuffer, pInfo->pArg, pInfo->uiSize, pInfo->cPath, pInfo->iIndex);
				}
			}

		private:
			//----------------------------------------------------------------------
			//	インデックスの取得
			//----------------------------------------------------------------------
			s32 GetIndex(const c8* pPath)
			{
				(void)pPath;
				//	todo: 未実装
				return NoManagerIndex;
			}
			//----------------------------------------------------------------------
			//	検索用ファイルパスへの変換
			//----------------------------------------------------------------------
			void ConvertSearchPath(c8* pOut, const c8* pPath, b8 bBind = true)
			{
				if ( pPath[0] == '@' )
				{
					kgStrcpy(pOut, pPath);
				}
				else
				{
					*pOut = '@';
					kgStrcpy(pOut + 1, pPath);
					//	文字を全て大文字にしていく
					for( u32 i=1; pOut[i] != '\0'; i++ )
					{
						//	2byte文字の場合はそのままにしておく
						if( CharSet::IsMultiByteChar(pOut[i]) )
						{
							i ++;
						}
						else if( pOut[i] == '\\' )
						{
							pOut[i] = '/';
						}
						else
						{
							pOut[i] = (pOut[i] >= 'a' && pOut[i] <= 'z')? pOut[i] - 'a' + 'A': pOut[i];
						}
					}

					if( bBind && m_BindInfo.size() )
					{
						algorithm::string strPath = pOut;
						for( u32 i=0; i < m_BindInfo.size(); i++ )
						{
							BindInfo& Info = m_BindInfo[i];
							if( strPath.startwith(Info.strPath) )
							{
								CharSet::SFormat(pOut, "%s%s", Info.strBind.c_str(), pOut + Info.strPath.length());
								break;
							}
						}
					}
				}
			}
			//----------------------------------------------------------------------
			//	読み込み用ファイルパスへの変換
			//----------------------------------------------------------------------
			void ConvertFilePath(c8* pOut, const c8* pPath)
			{
				if ( pPath[0] == '@' )
				{
					++ pPath;
				}
				CharSet::SFormat(pOut, "%s%s", m_cRootDir, pPath);
			}

		private:
			//----------------------------------------------------------------------
			//!	ロード処理
			//----------------------------------------------------------------------
			void LoadThreadFunction(void)
			{
				LOAD_INFO* pInfo;

				while( true )
				{
					//	終了要求チェック
					if( CheckFlag(EModeFlag::Exit) )	break;
					//	ロードイベント待ち
					m_pLoadEvent->Wait();

					while( true )
					{
						//	終了要求チェック
						if( CheckFlag(EModeFlag::Exit) )	break;
						//	ファイル読み込み
						if( !CheckFlag(EModeFlag::Block) )
						{
							//	ロード要求数
							if( m_uiRequestCount == 0 )	break;

							pInfo = &m_Info[m_uiNowRequest];

							if( !ReadFile(pInfo) )
							{
								WarningTrace("Load Error[%s]", pInfo->cPath);
							}

							m_uiNowRequest ++;
							if( m_uiNowRequest == LoadRequestMax )
							{
								m_uiNowRequest = 0;
							}

							if( pInfo->pBuffer == null )
							{
								SCOPE_LOCK_CRITICALSECTION(m_pCS);
								m_uiRequestCount --;
							}
						}
						else
						{
							pInfo = &m_BlockLoad;
							//	非同期読み込み(読み込み中は呼び出し元のスレッドが止まる)
							if( !ReadFile(pInfo) )
							{
								WarningTrace("Load Error[%s]", pInfo->cPath);
							}

							while( m_uiDecompressCount > 0 )
							{
								kgSleep(0);
							}
							ClearFlag(EModeFlag::Block);
							m_pBlockEvent->SetSignal();
						}
					}
				}
				m_uiRequestCount = 0;
			}

			//----------------------------------------------------------------------
			//!	ロードスレッド
			//----------------------------------------------------------------------
			static u32 THREADFUNC LoadThreadFunction(void* pArg)
			{
				CFileReader* _this = (CFileReader*)pArg;

				_this->LoadThreadFunction();

				return 0;
			}
		private:
			//----------------------------------------------------------------------
			//!	解凍処理
			//----------------------------------------------------------------------
			b8 Decompress(LOAD_INFO* pInfo)
			{
				void* pBuffer = Memory::Allocate(pInfo->uiSize);
				if( pBuffer == null )
				{
					return false;
				}
				u32 uiSize = pInfo->uiSize;
				//	ファイルの解凍
				if( zlib::Decompress(pBuffer, &uiSize, pInfo->pBuffer, pInfo->uiFileSize) != zlib::EResult::OK )
				{
					return false;
				}
				//	圧縮ファイルの解放
				Memory::Free(pInfo->pBuffer);
				pInfo->pBuffer = null;
				//	データを管理リストに追加
				SetFileInfo(pInfo, pBuffer);
				return true;
			}

			//----------------------------------------------------------------------
			//!	解凍処理
			//----------------------------------------------------------------------
			void DecompressThreadFunction(void)
			{
				LOAD_INFO* pInfo;

				while( true )
				{
					//	終了要求チェック
					if( CheckFlag(EModeFlag::Exit) )	break;
					//	ロードイベント待ち
					m_pDecompressEvent->Wait();

					while( true )
					{
						//	終了要求チェック
						if( CheckFlag(EModeFlag::Exit) )	break;
						//	解凍要求数
						if( m_uiDecompressCount == 0 )		break;

						//	解凍
						{
							pInfo = &m_Info[m_uiNowDecompress];

							m_uiNowDecompress ++;
							if( m_uiNowDecompress == LoadRequestMax )
							{
								m_uiNowDecompress = 0;
							}

							if( pInfo->pBuffer == null )
							{
								continue;
							}
							//	解凍
							if( !Decompress(pInfo) )
							{
								WarningTrace("Decompress Error[%s]", pInfo->cPath);
							}

							kgAtomicDecrement(m_uiDecompressCount);
							if( pInfo->pBuffer == null )
							{
								SCOPE_LOCK_CRITICALSECTION(m_pCS);
								m_uiRequestCount --;
							}
						}
					}
				}
				m_uiDecompressCount = 0;
			}

			//----------------------------------------------------------------------
			//!	解凍スレッド
			//----------------------------------------------------------------------
			static u32 THREADFUNC DecompressThreadFunction(void* pArg)
			{
				CFileReader* _this = (CFileReader*)pArg;

				_this->DecompressThreadFunction();

				return 0;
			}

		private:
			TKGLPtr<IFile>						m_pArchiveFile;			//!< アーカイブファイル管理
			TKGLPtr<IFile>						m_pFile;				//!< ファイル管理
			TKGLPtr<IDirectory>					m_pDirectory;			//!< ディレクトリ管理
			TKGLPtr<Hash::IHash>				m_pHash;				//!< ハッシュ
			TKGLPtr<Thread::IThread>			m_pLoadThread;			//!< ロードスレッド
			TKGLPtr<Thread::IEvent>				m_pLoadEvent;			//!< ロードイベント
			TKGLPtr<Thread::ICriticalSection>	m_pLoadCS;				//!< 同期管理用
			u32									m_uiRequestCount;		//!< ロードリクエスト数
			u32									m_uiNowRequest;			//!< 現在のリクエスト位置
			u32									m_uiNow;				//!< 現在のリクエスト設定位置
			s32									m_iFlag;				//!< フラグ
			TKGLPtr<Thread::ICriticalSection>	m_pCS;					//!< 読み込み要求用
			TKGLPtr<Thread::IEvent>				m_pBlockEvent;			//!< ブロック読み用イベント

			TKGLPtr<Thread::IThread>			m_pDecompressThread;	//!< 解凍スレッド
			TKGLPtr<Thread::IEvent>				m_pDecompressEvent;		//!< 解凍イベント
			u32									m_uiDecompressCount;	//!< 解凍リクエスト数
			u32									m_uiNowDecompress;		//!< 現在の解凍位置

			LOAD_INFO							m_Info[LoadRequestMax];	//!< ロード情報
			LOAD_INFO							m_BlockLoad;			//!< ブロック読み用情報

			FILE_INFO*							m_pFileInfo;			//!< ロード済みのファイル情報
			FILE_INFO*							m_pFileInfoEnd;			//!< ロード済みのファイル情報の末尾
			u32									m_uiFileCount;			//!< ロード済みのファイル数

			Archive::ArchiveHeader				m_ArchiveHeader;		//!< アーカイブヘッダー
			Archive::FolderInfo*				m_pFolderInfo;			//!< アーカイブヘッダー
			Archive::FileInfo*					m_pArchiveHeader;		//!< アーカイブヘッダー

			ARCHIVE_INFO*						m_pArchiveInfo;			//!< ロード済みのファイル情報
			ARCHIVE_INFO*						m_pArchiveInfoEnd;		//!< ロード済みのファイル情報の末尾
			u32									m_uiArchiveCount;		//!< ロード済みのファイル数
			c8									m_cRootDir[LoadPathMax];//!< ルートディレクトリ
			algorithm::vector<BindInfo>			m_BindInfo;				//!< バインド情報
		};

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		IExtraObject::IExtraObject(void)
			: m_pData(null)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		IExtraObject::~IExtraObject(void)
		{
			if( m_pData )
			{
				Memory::Free(m_pData);
			}
		}
		//----------------------------------------------------------------------
		//	オーナーの設定
		//----------------------------------------------------------------------
		void IExtraObject::SetOwner(IFileReader* pOwner, void* pData)
		{
			m_pOwner = pOwner;
			m_pData	= pData;
		}
		//----------------------------------------------------------------------
		//	解放
		//----------------------------------------------------------------------
		void IExtraObject::Unload(void)
		{
			if( m_pOwner )
			{
				m_pOwner->Unload(this);
				m_pOwner = null;
			}
		}

		//----------------------------------------------------------------------
		// ファイルリーダーの生成
		//----------------------------------------------------------------------
		b8 Create(IFileReader** ppFileReader, const c8* pPath, const c8* pRootDirectory)
		{
			*ppFileReader = new CFileReader(pPath, pRootDirectory);
			(*ppFileReader)->AddRef();
			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
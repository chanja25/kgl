//----------------------------------------------------------------------
//!
//!	@file	Script.cpp
//!	@brief	スクリプト関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Script/kgl.Script.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Utility/algorithm/kgl.find.h"


namespace kgl
{
	namespace Script
	{
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		CKSLexer::CKSLexer(void)
			: m_pFile(null)
			, m_iLineNo(0)
			, m_cChar(0)
			, m_cQuotChar(0)
		{
			InitCharType();
			InitKeyword();
		}
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CKSLexer::Initialize(const c8* pPath)
		{
			m_cChar = ' ';
			m_cQuotChar = 0;

			m_pFile = null;
			File::Create(m_pFile.GetReference());
			if( !m_pFile->Open(pPath) )
			{
				return false;
			}

			Hash::CreateCRC32(m_pHash.GetReference());

			return true;
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CKSLexer::Finalize(void)
		{
			if (m_pFile.IsValid())
			{
				m_pFile->Close();
				m_pFile = null;
			}
		}

		//----------------------------------------------------------------------
		//	次のトークンへ進める
		//----------------------------------------------------------------------
		const Token& CKSLexer::NextToken(void)
		{
			Token& token = m_Token;

			token.iKind		= NulKind;
			token.strText	= "";
			token.iValue	= 0;

			c8 c = m_cChar;
			//	スペースを読み飛ばす
			while( isspace(c) )
				c = NextChar();

			if( c == EOF ) { token.iKind = EofToken; return m_Token; }

			switch( m_eTknKindTbl[c] )
			{
			case Letter:
				for( ; m_eTknKindTbl[c] == Letter || m_eTknKindTbl[c] == Digit; c = NextChar() )
				{
					token.strText += c;
				}
				break;
			case Digit:
				{
					c = AnalizeNumber(token);
				}
				break;
			case Period:
				{
					c = NextChar();
					if( m_eTknKindTbl[c] != Digit )
					{
						token.iKind = Period;
						token.strText = ".";
					}
					else
					{
						m_cChar = '.';
						SeekPrevChar();
						c = AnalizeNumber(token);
					}
				}
				break;

			case SngQ:
				{
					b8 bErr = false;
					s32 ct = 0;
					for( c = NextChar(); c != EOF && c != '\n' && c != '\''; c = NextChar() )
					{
						if( c == '\\' )
						{
							if( (c = NextChar()) == 'n' ) c = '\n';
						}
						if( ++ ct == 1 ) token.iValue = c;
					}
					if( ct != 1 )	bErr = true;

					if( c == '\'' ) c = NextChar();
					else			bErr = true;

					if( bErr )
					{
						SetError(EError::SyntaxError, m_iLineNo, token.strText.c_str());
					}
					token.iKind = CharNum;
					token.strText = (c8)token.iValue;
				}
				break;
			case DblQ:
				c = NextChar();
				while( c != EOF && c != '\n' && c != '"' )
				{
					if( CharSet::IsMultiByteChar(c) )
					{
						token.strText += c;
						token.strText += NextChar();
						c = NextChar();
						continue;
					}
					if( c == '\\' )
					{
						if( (c = NextChar()) == 'n' ) c = '\n';
					}
					token.strText += c;
					c = NextChar();
				}
				if( c == '"' )	c = NextChar();
				else			SetError(EError::StringNoClose, m_iLineNo, token.strText.c_str());

				token.iKind		= String;
				token.iValue	= 0;//AllocateS(token.strText.c_str());
				break;
			default:
				if( c < 0 || 127 < c )
				{
					SetError(EError::Unknown, m_iLineNo, "");
				}
				if( CharSet::IsMultiByteChar(c) )
				{
					token.strText += c;
					c = NextChar();
				}
				token.strText += c;
				c = NextChar();

				if( IsOp2(token.strText[0], c) )
				{
					token.strText += c;
					c = NextChar();
					if( IsOp3(token.strText, c) )
					{
						token.strText += c;
						c = NextChar();
					}
				}
				break;
			}
			if( token.iKind == NulKind )
			{
				SetKind(token);
			}
			if( token.iKind == Others )
			{
				SetError(EError::SyntaxError, m_iLineNo, token.strText.c_str());
			}
			return m_Token;
		}
		//----------------------------------------------------------------------
		//	エラー情報の取得
		//----------------------------------------------------------------------
		const ErrorInfo* CKSLexer::GetError(void)
		{
			return &m_Error;
		}

		//----------------------------------------------------------------------
		//	文字タイプの初期化
		//----------------------------------------------------------------------
		void CKSLexer::InitCharType(void)
		{
			s32 i;
			for( i = 0;   i < 256;  i++ )	m_eTknKindTbl[i] = Others;
			for( i = '0'; i <= '9'; i++ )	m_eTknKindTbl[i] = Digit;
			for( i = 'A'; i <= 'Z'; i++ )	m_eTknKindTbl[i] = Letter;
			for( i = 'a'; i <= 'z'; i++ )	m_eTknKindTbl[i] = Letter;

			m_eTknKindTbl['_']	= Digit;	m_eTknKindTbl['_']	= Letter;
			m_eTknKindTbl['(']	= Lparen;	m_eTknKindTbl[')']	= Rparen;
			m_eTknKindTbl['{']	= Lbrace;	m_eTknKindTbl['}']	= Rbrace;
			m_eTknKindTbl['[']	= Lbracket;	m_eTknKindTbl[']']	= Rbracket;
			m_eTknKindTbl['+']	= Plus;		m_eTknKindTbl['-']	= Minus;
			m_eTknKindTbl['*']	= Multi;	m_eTknKindTbl['/']	= Division;
			m_eTknKindTbl['%']	= Mod;		m_eTknKindTbl['!']	= Not;
			m_eTknKindTbl['&']	= And;		m_eTknKindTbl['|']	= Or;
			m_eTknKindTbl['^']	= Xor;		m_eTknKindTbl['~']	= NotBit;
			m_eTknKindTbl['=']	= Assign;	m_eTknKindTbl['?']	= Question;
			m_eTknKindTbl[':']	= Colon;	m_eTknKindTbl[';']	= Semicolon;
			m_eTknKindTbl['#']	= Sharp;	m_eTknKindTbl['\\']	= Yen;
			m_eTknKindTbl[',']	= Comma;	m_eTknKindTbl['.']	= Period;
			m_eTknKindTbl['\'']	= SngQ;		m_eTknKindTbl['\"']	= DblQ;
			m_eTknKindTbl['<']	= Less;		m_eTknKindTbl['>']	= Greater;
		}
		//----------------------------------------------------------------------
		//	数値の解析
		//----------------------------------------------------------------------
		c8 CKSLexer::AnalizeNumber(Token& token)
		{
			c8 c = m_cChar;

			s32 iValue = 0;
			for( ; m_eTknKindTbl[c] == Digit; c = NextChar() )
			{
				iValue = iValue * 10 + (c - '0');
			}
			if( m_eTknKindTbl[c] == Period )
			{
				c = NextChar();

				f32 fPower = 1.0f;
				f32 fValue = 0;
				for( ; m_eTknKindTbl[c] == Digit; c = NextChar() )
				{
					fValue = fValue * 10 + (c - '0');
					fPower *= 10.0f;
				}
				fValue /= fPower;
				fValue += (f32)iValue;

				string strText;
				for( ; m_eTknKindTbl[c] == Letter || m_eTknKindTbl[c] == Digit; c = NextChar() )
				{
					strText += c;
				}
				if( strText[0] != 'f' || strText[0] != 'F' )
				{
					//todo:本来はここにきていない場合はdouble値となる
				}

				token.iKind		= FloatNum;
				token.iValue	= *(s32*)&fValue;
				token.strText	= fValue;
			}
			else
			{
				token.iKind		= IntNum;
				token.iValue	= iValue;
				token.strText	= iValue;
			}

			return c;
		}

		//----------------------------------------------------------------------
		//	キーワード
		//----------------------------------------------------------------------
		b8 compareHash(const Keyword& key1, const Keyword& key2)
		{
			return key1.iHash < key2.iHash;
		}
		//----------------------------------------------------------------------
		//	キーワードの初期化
		//----------------------------------------------------------------------
		void CKSLexer::InitKeyword(void)
		{
#define	DECL_KEYWORD(id, word)	case id: key.iKind = id; key.iHash = m_pHash->Get(word); key.strKey = word; break

			Keyword key;
			for( s32 i=0; i < TknKindMax; i++ )
			{
				switch( i )
				{
					//	変数型関連
					DECL_KEYWORD(Void,		"void");
					DECL_KEYWORD(Char,		"char");
					//DECL_KEYWORD(Short,		"short");
					DECL_KEYWORD(Int,		"int");
					//DECL_KEYWORD(Int64,		"__int64");
					DECL_KEYWORD(Float,		"float");
					//DECL_KEYWORD(Double,		"double");

					//	制御文関連
					DECL_KEYWORD(If,		"if");
					DECL_KEYWORD(Else,		"else");
					DECL_KEYWORD(For,		"for");
					DECL_KEYWORD(While,		"while");
					DECL_KEYWORD(Do,		"do");
					DECL_KEYWORD(Switch,	"switch");
					DECL_KEYWORD(Case,		"case");
					DECL_KEYWORD(Default,	"default");
					DECL_KEYWORD(Break,		"break");
					DECL_KEYWORD(Continue,	"continue");
					DECL_KEYWORD(Return,	"return");

					//	区切り文字関連
					DECL_KEYWORD(Lparen,	"(");
					DECL_KEYWORD(Rparen,	")");
					DECL_KEYWORD(Lbrace,	"{");
					DECL_KEYWORD(Rbrace,	"}");
					DECL_KEYWORD(Lbracket,	"[");
					DECL_KEYWORD(Rbracket,	"]");
					DECL_KEYWORD(Plus,		"+");
					DECL_KEYWORD(Minus,		"-");
					DECL_KEYWORD(Multi,		"*");
					DECL_KEYWORD(Division,	"/");
					DECL_KEYWORD(Mod,		"%");
					DECL_KEYWORD(Not,		"!");
					DECL_KEYWORD(And,		"&");
					DECL_KEYWORD(Or,		"|");
					DECL_KEYWORD(Xor,		"^");
					DECL_KEYWORD(NotBit,	"~");
					DECL_KEYWORD(Assign,	"=");
					DECL_KEYWORD(Question,	"?");

					//	その他
					DECL_KEYWORD(Colon,		":");
					DECL_KEYWORD(Semicolon,	";");
					DECL_KEYWORD(Sharp,		"#");
					DECL_KEYWORD(Yen,		"\\");
					DECL_KEYWORD(Comma,		",");
					DECL_KEYWORD(SngQ,		"\'");
					DECL_KEYWORD(DblQ,		"\"");

					//	複数文字からなる演算子関連
					DECL_KEYWORD(Incre,		"++");
					DECL_KEYWORD(Decre,		"--");
					DECL_KEYWORD(AddEq,		"+=");
					DECL_KEYWORD(SubEq,		"-=");
					DECL_KEYWORD(MulEq,		"*=");
					DECL_KEYWORD(DivEq,		"/=");
					DECL_KEYWORD(ModEq,		"%=");
					DECL_KEYWORD(AndEq,		"&=");
					DECL_KEYWORD(OrEq,		"|=");
					DECL_KEYWORD(XorEq,		"^=");
					DECL_KEYWORD(NotEq,		"!=");
					DECL_KEYWORD(Equal,		"==");
					DECL_KEYWORD(AndCmp,	"&&");
					DECL_KEYWORD(OrCmp,		"||");
					DECL_KEYWORD(Less,		"<");
					DECL_KEYWORD(LessEq,	"<=");
					DECL_KEYWORD(Greater,	">");
					DECL_KEYWORD(GreaterEq,	">=");
					DECL_KEYWORD(Lshift,	"<<");
					DECL_KEYWORD(LShiftEq,	"<<=");
					DECL_KEYWORD(Rshift,	">>");
					DECL_KEYWORD(RShiftEq,	">>=");

					//	組み込み関数
					DECL_KEYWORD(Printf,	"printf");
					DECL_KEYWORD(Input,		"input");
					DECL_KEYWORD(Exit,		"exit");

				default:
					//	設定するものが無い場合は次へ進める
					continue;
				}
				m_Keyword.push_back(key);
			}
			//	キーワードをハッシュ値でソートしておく
			sort::quick_sort(&m_Keyword[0], 0, m_Keyword.size()-1, compareHash);
		}

		//----------------------------------------------------------------------
		//	トークン種別の設定
		//----------------------------------------------------------------------
		void CKSLexer::SetKind(Token& token)
		{
			token.iKind = Others;

			s32 iHash = m_pHash->Get(token.strText.c_str());
			for( u32 i = 0; i < m_Keyword.size(); i++ )
			{
				if( m_Keyword[i].iHash == iHash )
				{
					if( token.strText == m_Keyword[i].strKey )
					{
						token.iKind = m_Keyword[i].iKind;
						return;
					}
				}
			}
			switch( m_eTknKindTbl[token.strText[0]] )
			{
			case Letter:	token.iKind = Ident;	break;
			case Digit:		token.iKind = IntNum;	break;
			}
		}
		//----------------------------------------------------------------------
		//	次の文字を取得
		//----------------------------------------------------------------------
		c8 CKSLexer::NextChar(void)
		{
			c8& c = m_cChar;
			if( c == EOF ) return c;
			if( c == '\n' ) ++ m_iLineNo;

			c = ReadNextChar();
			if( c == EOF )
			{
				return c;
			}
			c8& quot_c = m_cQuotChar;
			if( quot_c != 0 )
			{
				if( c == quot_c || c== '\n' )
				{
					quot_c = 0;
				}
				return c;
			}
			if( c == '\'' || c == '"' )
			{
				quot_c = c;
			}
			else if( c == '/' )
			{
				c8 c2 = ReadNextChar();
				switch( c2 )
				{
				case '/':
					while( (c = ReadNextChar()) != EOF && c != '\n' );
					return c;
				case '*':
					{
						s32 iLine = m_iLineNo;
						for( c = 0; (c2 = ReadNextChar()) != EOF; c = c2 )
						{
							if( c2 == '\n' ) ++ m_iLineNo;
							if( c == '*' && c2 == '/' )
							{
								c = ' ';
								return c;
							}
						}
						SetError(EError::CommentNoClose, iLine, "/* のコメントが閉じられていません");
					}
					c = EOF;
					return EOF;
				}
				SeekPrevChar();
			}
			else if( c == '*' )
			{
				c8 c2 = ReadNextChar();
				if( c2 == '/' )
				{
					SetError(EError::CommentNoStart, m_iLineNo, "*/ に対する /* が見つかりませんでした");
					c = EOF;
					return EOF;
				}
				SeekPrevChar();
			}
			return c;
		}

		//----------------------------------------------------------------------
		//	次の文字の読み込み
		//----------------------------------------------------------------------
		c8 CKSLexer::ReadNextChar(void)
		{
			c8 c = EOF;
			m_pFile->Read(&c, sizeof(c));

			return c;
		}
		//----------------------------------------------------------------------
		//	前の文字の読み込み
		//----------------------------------------------------------------------
		void CKSLexer::SeekPrevChar(void)
		{
			m_pFile->Seek(-1, File::EFilePointer::Current);
		}
		//----------------------------------------------------------------------
		//	2文字からなる演算子か？
		//----------------------------------------------------------------------
		b8 CKSLexer::IsOp2(c8 c1, c8 c2)
		{
			c8 s[] = "    ";
			s[1] = c1; s[2] = c2;

			return strstr(" ++ -- += -= *= /= %= &= |= ^= != == && || <= >= << >> ", s) != null;
		}
		//----------------------------------------------------------------------
		//	3文字からなる演算子か？
		//----------------------------------------------------------------------
		b8 CKSLexer::IsOp3(const string& str, c8 c)
		{
			c8 s[] = "     ";
			s[1] = str[0]; s[2] = str[1]; s[3] = c;

			return strstr(" <<= >>= ", s) != null;
		}
		//----------------------------------------------------------------------
		//	エラー情報の設定
		//----------------------------------------------------------------------
		void CKSLexer::SetError(s32 iError, s32 iLine, const string& strError)
		{
			m_Error.iError		= iError;
			m_Error.iLine		= iLine;
			m_Error.strError	= strError;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Profile.cpp
//!	@brief	プロファイラー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

#if	KGL_DEBUG_PROFILE
#include "Debug/kgl.Profiler.h"
#include "Utility/algorithm/kgl.find.h"

namespace kgl
{
	namespace Profiler
	{
		//----------------------------------------------------------------------
		//	比較(インデックス)
		//----------------------------------------------------------------------
		INLINE s32 compareIndex(const s32& index, const ProfileInfo& Info)
		{
			return index - Info.iIndex;
		}

		//	インスタンス生成
		CProfiler* CProfiler::m_pInstance = null;

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CProfiler::CProfiler(void)
			: m_pInfo(null)
			, m_pRoot(null)
			, m_pCurrent(null)
			, m_iInfoCount(0)
			, m_iLogCount(0)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CProfiler::~CProfiler(void)
		{}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CProfiler::InitializeInstance(void)
		{
			if( m_pInstance )
			{
				return false;
			}
			m_pInstance = new CProfiler;

			return m_pInstance->Initialize();
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CProfiler::FinalizeInstance(void)
		{
			if( m_pInstance == null )
			{
				return;
			}
			m_pInstance->Finalize();

			delete m_pInstance;
			m_pInstance = null;
		}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CProfiler::Initialize(void)
		{
			if( m_pInfo )
			{
				return false;
			}

			//	プロファイル情報
			m_pInfo = (ProfileInfo*)kgl::Memory::Debug::Allocate(sizeof(ProfileInfo) * LOG_MAX);
			kgZeroMemory(m_pInfo, sizeof(ProfileInfo) * LOG_MAX);
			//	ログ情報の取得スペースを生成する
			m_pRoot = (Log*)kgl::Memory::Debug::Allocate(sizeof(Log) * LOG_MAX);
			kgZeroMemory(m_pRoot, sizeof(Log) * LOG_MAX);

			Time::Create(m_pStopwatch.GetReference());

			SetLogInfo(EProfile_Root,					ECategory_System, "Root");
			SetLogInfo(EProfile_SystemUpdate,			ECategory_System, "System Update");
			SetLogInfo(EProfile_SystemInput,			ECategory_System, "System Input");
			SetLogInfo(EProfile_SystemRendering,		ECategory_System, "System Render");
			SetLogInfo(EProfile_SystemDebugRendering,	ECategory_System, "System DebugRender");

			//	ルートログを生成する
			m_pCurrent = CreateLog(EProfile_Root, null);
			return true;
		}

		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CProfiler::Finalize(void)
		{
			m_pStopwatch = null;
			if( m_pRoot )
			{
				Memory::Debug::Free(m_pRoot);
				m_pRoot = null;
			}
			if( m_pInfo )
			{
				Memory::Debug::Free(m_pInfo);
				m_pInfo = null;
			}
		}

		//----------------------------------------------------------------------
		//	フレームの開始
		//----------------------------------------------------------------------
		void CProfiler::StartFrame(void)
		{
			//	全てのログを更新する
			UpdateLog(m_pRoot);

			//	プロファイル情報の更新
            for( s32 i=0; i < m_iInfoCount; i++ )
            {
                ProfileInfo& Info = m_pInfo[i];
				if( Info.iTmpCount > 0 )
				{
					Info.fAverageTime		/= (f32)Info.iTmpCount;
					Info.iTmpCount			= 0;
				}
				else
				{
					Info.iFrameCount		/= 2;
					Info.fFrameTime			*= 0.5f;
					Info.fAverageTime		*= 0.5f;
					Info.fAveragePerFrame	*= 0.5f;
				}
            }

			m_pRoot->fBeginTime = 0.0f;

			m_pStopwatch->Reset();
		}

		//----------------------------------------------------------------------
		//	フレームの終了
		//----------------------------------------------------------------------
		void CProfiler::EndFrame(void)
		{
			Log* pLog = m_pRoot;
			pLog->fEndTime = (f32)GetTime();
			f32 fTime = pLog->fEndTime - pLog->fBeginTime;

			//	情報更新
			pLog->iTmpCount ++;
			pLog->fTmpFrameTime += fTime;
			pLog->fTmpMinTime = Min(fTime, pLog->fTmpMinTime);
			pLog->fTmpMaxTime = Max(fTime, pLog->fTmpMaxTime);
		}

		//----------------------------------------------------------------------
		//	計測開始
		//----------------------------------------------------------------------
		void CProfiler::Begin(s32 iIndex)
		{
			//	現在のカレントログを親に設定する
			Log* pParent  = m_pCurrent;
			Log* pBrother = null;

			//	指定のプロファイル名を持つ既存のログが存在するか探す
			Log* pLog = pParent->pChild;
			while( pLog )
			{
				if( pLog->iIndex == iIndex )
				{
					break;
				}
				pBrother = pLog;
				pLog	 = pLog->pBrother;
			}

			if( pLog == null )
			{
				//	指定のプロファイルが存在しないので、新規ログを生成
				pLog = CreateLog(iIndex, pParent);

				(pParent->pChild)?
					pBrother->pBrother = pLog:
					pParent->pChild = pLog;
			}

			pLog->fBeginTime = (f32)GetTime();
			//	カレントログに自分をセット
			m_pCurrent = pLog;
		}

		//----------------------------------------------------------------------
		//	計測終了
		//----------------------------------------------------------------------
		void CProfiler::End(s32 iIndex)
		{
			if( m_pCurrent->iIndex != iIndex )
			{
				c8 cTemp[256];
				sprintf(cTemp, "計測開始と計測終了が対になっていません(%d:%s)", iIndex, GetProfileInfo(iIndex)->cName);
				kgAssert(false, cTemp);
			}

			Log* pLog = m_pCurrent;
			pLog->fEndTime = (f32)GetTime();
			f32 fTime = pLog->fEndTime - pLog->fBeginTime;

			//	情報更新
			pLog->iTmpCount ++;
			pLog->fTmpFrameTime += fTime;
			pLog->fTmpMinTime = (pLog->fTmpMinTime > fTime)? fTime: pLog->fTmpMinTime;
			pLog->fTmpMaxTime = (pLog->fTmpMaxTime < fTime)? fTime: pLog->fTmpMaxTime;

			//	カレントを親に戻す
			m_pCurrent = m_pCurrent->pParent;
		}
		//----------------------------------------------------------------------
		//	ログ情報の設定
		//----------------------------------------------------------------------
		void CProfiler::SetLogInfo(s32 iIndex, s32 iCategory, c8* pName)
		{
			kgAssert(m_iInfoCount + 1 < LOG_MAX, "これ以上のログの追加はできません");
			ProfileInfo* pRoot = m_pInstance->m_pInfo;

			s32 low, high, mid;
			low  = mid = 0;
			high = m_iInfoCount-1;
			while( low <= high )
			{
				mid = (low + high) >> 1;

				s32 ret = compareIndex(iIndex, pRoot[mid]);
				if( ret == 0 )
				{
					low = mid;
					break;
				}
				(ret < 0)? (high = mid - 1): (low = mid + 1);
			}
			if( low < m_iInfoCount )
			{
				kgMemmove(&pRoot[low+1], &pRoot[low], sizeof(ProfileInfo) * (m_iInfoCount-low));
			}

			ProfileInfo* pInfo = &pRoot[low];
			pInfo->iIndex		= iIndex;
			pInfo->iCategory	= iCategory;
			kgStrcpy(pInfo->cName, pName);

			m_iInfoCount ++;
		}

		//----------------------------------------------------------------------
		//	ルートログの取得
		//----------------------------------------------------------------------
		Log* CProfiler::GetRootLog(void)
		{
			return m_pRoot;
		}

		//----------------------------------------------------------------------
		//	指定ログの取得
		//----------------------------------------------------------------------
		Log* CProfiler::GetLog(s32 iIndex, Log* pLog)
		{
			if( pLog == null )
			{
				pLog = m_pRoot;
			}

			while( pLog )
			{
				if( pLog->pChild != null )
				{
					Log* pChild = GetLog(iIndex, pLog->pChild);
					if( pChild )
					{
						pLog = pChild;
					}
				}
				if( pLog->iIndex == iIndex )
				{
					break;
				}
				pLog = pLog->pBrother;
			}

			return pLog;
		}
		//----------------------------------------------------------------------
		//	プロファイル情報を取得
		//----------------------------------------------------------------------
		ProfileInfo* CProfiler::GetProfileInfo(s32 iIndex)
		{
			ProfileInfo* pInfo = m_pInstance->m_pInfo;

			s32 ret = algorithm::binary_search<s32, ProfileInfo>(iIndex, pInfo, m_iInfoCount, compareIndex);
			if( ret == algorithm::NotFind )
			{
				return null;
			}
			return &pInfo[ret];
		}

		//----------------------------------------------------------------------
		//	ログの生成
		//----------------------------------------------------------------------
		Log* CProfiler::CreateLog(s32 iIndex, Log* pParent)
		{
			kgAssert(m_iLogCount + 1 < LOG_MAX, "これ以上のログの追加はできません");

			//	ログの初期化
			Log* pLog = &m_pRoot[m_iLogCount];
			pLog->pParent	= pParent;
			pLog->pBrother	= null;
			pLog->pChild	= null;

			pLog->fBeginTime			= 0;
			pLog->fEndTime				= 0;
			pLog->iTmpCount				= 0;
			pLog->fTmpFrameTime			= 0;
			pLog->fTmpMinTime			= FLT_MAX;
			pLog->fTmpMaxTime			= FLT_MIN;
			pLog->iTmpTotalTimeIndex	= 0;
			kgZeroMemory(pLog->iTmpTotalCount, sizeof(pLog->iTmpTotalCount));
			kgZeroMemory(pLog->fTmpTotalTime, sizeof(pLog->fTmpTotalTime));

			pLog->iIndex				= iIndex;
			pLog->iFrameCount			= 0;
			pLog->fFrameTime			= 0;
			pLog->fAverageTime			= 0;
			pLog->fAveragePerFrame		= 0;
			pLog->fMinTime				= FLT_MAX;
			pLog->fMaxTime				= FLT_MIN;

			m_iLogCount ++;

			return pLog;
		}

		//----------------------------------------------------------------------
		//	ログ情報の更新
		//----------------------------------------------------------------------
		void CProfiler::UpdateLog(Log* pLog)
		{
			s32 i, iCount;
			f32 fTotalTime;
			while( pLog )
			{
				iCount = 0;
				fTotalTime = 0;
				for( i = 0; i < AVG_FRAME; i++ )
				{
					iCount += pLog->iTmpTotalCount[i];
					fTotalTime += pLog->fTmpTotalTime[i];
				}
				pLog->iTmpTotalCount[pLog->iTmpTotalTimeIndex] = pLog->iTmpCount;
				pLog->fTmpTotalTime[pLog->iTmpTotalTimeIndex] = pLog->fTmpFrameTime;
				pLog->iTmpTotalTimeIndex = (pLog->iTmpTotalTimeIndex+1) & (AVG_FRAME-1);

				//	ログ情報の更新
				pLog->iFrameCount		= pLog->iTmpCount;
				pLog->fFrameTime		= pLog->fTmpFrameTime;
				pLog->fAverageTime		= (iCount != 0)? fTotalTime / (f32)iCount: 0.0f;
				pLog->fAveragePerFrame	= fTotalTime / (f32)AVG_FRAME;
				pLog->fMinTime			= pLog->fTmpMinTime;
				pLog->fMaxTime			= pLog->fTmpMaxTime;

				if( true )//pLog->iTmpCount > 0 )
				{
					//	計測用変数の初期化
					pLog->fBeginTime	= 0;
					pLog->fEndTime		= 0;
					pLog->iTmpCount		= 0;
					pLog->fTmpFrameTime	= 0;

					if( pLog->pProfileInfo == null )
					{
						pLog->pProfileInfo = GetProfileInfo(pLog->iIndex);
					}
					if( pLog->pProfileInfo != null )
					{
						ProfileInfo* pInfo = pLog->pProfileInfo;
						if( pInfo->iTmpCount == 0 )
						{
							pInfo->iFrameCount		= pLog->iFrameCount;
							pInfo->fFrameTime		= pLog->fFrameTime;
							pInfo->fAverageTime		= pLog->fAverageTime;
							pInfo->fAveragePerFrame	= pLog->fAveragePerFrame;
						}
						else
						{
							pInfo->iFrameCount		+= pLog->iFrameCount;
							pInfo->fFrameTime		+= pLog->fFrameTime;
							pInfo->fAverageTime		+= pLog->fAverageTime;
							pInfo->fAveragePerFrame	+= pLog->fAveragePerFrame;
						}
						pInfo->fMinTime	= Min(pInfo->fMinTime, pLog->fMinTime);
						pInfo->fMaxTime	= Min(pInfo->fMaxTime, pLog->fMaxTime);

						pInfo->iTmpCount ++;
					}
				}

				//	子がいれば再帰的に更新する
				if( pLog->pChild )
					UpdateLog(pLog->pChild);

				pLog = pLog->pBrother;
			}
		}
	}
}

#endif	// ~#if KGL_DEBUG_PROFILE

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Debug.cpp
//!	@brief	デバッグ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Debug/kgl.Debug.h"
#include "Thread/kgl.Thread.h"
#include "Thread/kgl.Event.h"
#include "Thread/kgl.CriticalSection.h"
#include "Utility/Path/kgl.Path.h"
#include "Utility/algorithm/kgl.cio.h"
#include "File/kgl.Directory.h"

#if	KGL_DEBUG_TRACE

#include <io.h>
#include <fcntl.h>
#include <crtdbg.h>

namespace kgl
{
	namespace Debug
	{
		const c8* OutputTracePath	= "./Log/Log.txt";
		const s32 TraceBufferCount	= 0x00000400;

		//!	トレース情報
		struct TraceInfo
		{
			union
			{
				c8			cString[512];
				c16			sString[512];
			};
			bool	bEnable;
			bool	bUnicode;
		};

		//----------------------------------------------------------------------
		//	デバッグ出力
		//----------------------------------------------------------------------
		static void DebugPrint(const c8* str, b8 bConsole = false)
		{
			(void)bConsole;
#if	CONSOLE_TOOL || !WINDOWS
			printf(str);
#else
			bConsole? printf(str): OutputDebugStringA(str);
#endif
		}
		static void DebugPrint(const c16* str, b8 bConsole = false)
		{
			(void)bConsole;
#if	CONSOLE_TOOL || !WINDOWS
			wprintf(str);
#else
			bConsole? wprintf(str): OutputDebugStringW(str);
#endif
		}

		//======================================================================
		//!	プロファイラクラス(システムで初期化と終了処理を行う)
		//======================================================================
		class CDebugTrace
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CDebugTrace(void)
				: m_pTraceInfo(null)
				, m_iNowIndex(0)
				, m_iTraceIndex(0)
				, m_iTraceCount(0)
				, m_bTraceOutput(false)
				, m_bConsole(false)
				, m_bAsync(false)
				, m_bExit(false)
			{}
			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CDebugTrace(void)
			{
				if( m_pTraceInfo != null )
				{
					Memory::Debug::Free(m_pTraceInfo);
				}
			}
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(void)
			{
				Thread::Create(m_pThread.GetReference());
				Thread::Create(m_pEvent.GetReference());
				Thread::Create(m_pCriticalSection.GetReference());

				m_pTraceInfo = (TraceInfo*)Memory::Debug::Allocate(sizeof(TraceInfo) * TraceBufferCount);
				kgZeroMemory(m_pTraceInfo, sizeof(TraceInfo) * TraceBufferCount);
				m_pThread->Start(TraceThreadFunction, this, KB(4));
				return true;
			}
			//----------------------------------------------------------------------
			//	解放
			//----------------------------------------------------------------------
			void Finalize(void)
			{
				m_bExit = true;
				m_pEvent->SetSignal();
				m_pThread->Exit();

				DeleteConsole();

				m_pCriticalSection = null;
				m_pEvent = null;
				m_pThread = null;
			}

		public:
			//----------------------------------------------------------------------
			//	コンソール作成
			//----------------------------------------------------------------------
			void CreateConsole(void)
			{
#if	!CONSOLE_TOOL && WINDOWS
				if( m_bConsole )return;
				AllocConsole();
				*stdout = *_fdopen( _open_osfhandle( ( intptr_t )GetStdHandle( STD_OUTPUT_HANDLE ), _O_TEXT ), "w" );
				setvbuf( stdout, 0, _IONBF, 0 );

				m_bConsole = true;
#endif	// ~#if CONSOLE_TOOL
			}
			//----------------------------------------------------------------------
			//	コンソール作成
			//----------------------------------------------------------------------
			void DeleteConsole(void)
			{
#if	!CONSOLE_TOOL && WINDOWS
				if( !m_bConsole )return;
				m_bConsole = false;
				system("@pause");
				FreeConsole();
#endif	// ~#if CONSOLE_TOOL
			}

			//----------------------------------------------------------------------
			//	トレースの外部出力設定
			//----------------------------------------------------------------------
			void SetupOutputTrace(void)
			{
				if( m_bTraceOutput )return;
				m_bTraceOutput = true;

				c8 cTemp[256];
				Path::GetDirectory(cTemp, OutputTracePath);
				Directory::Create(cTemp);
				algorithm::cout out(OutputTracePath);
				out << "[DebugTraceOutput]" EOL;
			}

		public:
			//----------------------------------------------------------------------
			//	デバッグ出力
			//----------------------------------------------------------------------
			FORCEINLINE void Trace(TraceInfo& Info)
			{
				if( Info.bUnicode )
				{
					if( m_bTraceOutput )
					{
						algorithm::wcout out(OutputTracePath, true);
						out << Info.sString;
					}
					if( Info.bEnable )
					{
						DebugPrint(Info.sString, m_bConsole);
					}
				}
				else
				{
					if( m_bTraceOutput )
					{
						algorithm::cout out(OutputTracePath, true);
						out << Info.cString;
					}
					if( Info.bEnable )
					{
						DebugPrint(Info.cString, m_bConsole);
					}
				}
			}

			//----------------------------------------------------------------------
			//	デバッグ出力
			//----------------------------------------------------------------------
			FORCEINLINE void SetTrace(const c8* pString, const c8* pTag, b8 bEnable)
			{
				SCOPE_LOCK_CRITICALSECTION(m_pCriticalSection);

				TraceInfo& Info = m_pTraceInfo[m_iTraceIndex];
				CharSet::SFormat(Info.cString, "[%s]:%s\n", pTag, pString);
				Info.bEnable = bEnable;
				Info.bUnicode = false;

				if( m_bAsync )
				{
					m_iTraceIndex = (m_iTraceIndex+1) & (TraceBufferCount-1);
					m_iTraceCount ++;

					m_pEvent->SetSignal();
				}
				else
				{
					Trace(m_pTraceInfo[m_iTraceIndex]);
				}
			}
			//----------------------------------------------------------------------
			//	デバッグ出力
			//----------------------------------------------------------------------
			FORCEINLINE void SetTrace(const c16* pString, const c16* pTag, b8 bEnable)
			{
				SCOPE_LOCK_CRITICALSECTION(m_pCriticalSection);

				TraceInfo& Info = m_pTraceInfo[m_iTraceIndex];
				CharSet::SFormat(Info.sString, L"[%s]:%s\n", pTag, pString);
				Info.bEnable = bEnable;
				Info.bUnicode = true;

				if( m_bAsync )
				{
					m_iTraceIndex = (m_iTraceIndex+1) & (TraceBufferCount-1);
					m_iTraceCount ++;

					m_pEvent->SetSignal();
				}
				else
				{
					Trace(m_pTraceInfo[m_iTraceIndex]);
				}
			}
			//----------------------------------------------------------------------
			//	デバッグ出力
			//----------------------------------------------------------------------
			void TraceThreadFunction(void)
			{
				while( !m_bExit )
				{
					m_pEvent->Wait();

					while( m_iTraceCount > 0 )
					{
						Trace(m_pTraceInfo[m_iNowIndex]);
						m_iNowIndex = (m_iNowIndex+1) & (TraceBufferCount-1);
						m_iTraceCount --;
					}
				}
			}
			//----------------------------------------------------------------------
			//	デバッグ出力の非同期化
			//----------------------------------------------------------------------
			void AsyncTrace(b8 bEnable)
			{
				if( m_bAsync && !bEnable )
				{
					WaitTrace();
				}
				m_bAsync = !!bEnable;
			}
			//----------------------------------------------------------------------
			//	デバッグ出力待ち
			//----------------------------------------------------------------------
			void WaitTrace(void)
			{
				while( m_iTraceCount > 0 )
				{
					kgSleep(0);
				}
			}

			//----------------------------------------------------------------------
			//	デバッグ出力用スレッド
			//----------------------------------------------------------------------
			static u32 THREADFUNC TraceThreadFunction(void* pArg)
			{
				CDebugTrace* _this = (CDebugTrace*)pArg;

				_this->TraceThreadFunction();
				return 0;
			}

		private:
			TKGLPtr<Thread::IThread>			m_pThread;
			TKGLPtr<Thread::IEvent>				m_pEvent;
			TKGLPtr<Thread::ICriticalSection>	m_pCriticalSection;
			TraceInfo*							m_pTraceInfo;
			s32									m_iNowIndex;
			s32									m_iTraceIndex;
			s32									m_iTraceCount;
			bool								m_bConsole;
			bool								m_bTraceOutput;
			bool								m_bAsync;
			bool								m_bExit;
		};

		static CDebugTrace* pDebugTarace = null;
		//----------------------------------------------------------------------
		//	コンソール作成
		//----------------------------------------------------------------------
		void CreateConsole(void)
		{
			if( pDebugTarace == null )return;
			pDebugTarace->CreateConsole();
		}
		//----------------------------------------------------------------------
		//	トレースの外部出力設定
		//----------------------------------------------------------------------
		void SetupOutputTrace(void)
		{
			if( pDebugTarace == null )return;
			pDebugTarace->SetupOutputTrace();
		}

		//----------------------------------------------------------------------
		//!	初期化
		//----------------------------------------------------------------------
		void Initialize(void)
		{
			if( pDebugTarace )return;

#if	!KGL_FINAL_RELEASE && WINDOWS
			_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif	// ~#if	!KGL_FINAL_RELEASE && WINDOWS

			CDebugTrace* pDebug = new CDebugTrace;
			pDebug->Initialize();
			pDebugTarace = pDebug;
		}
		//----------------------------------------------------------------------
		//!	終了処理
		//----------------------------------------------------------------------
		void Finalize(void)
		{
			if( pDebugTarace == null )return;

			pDebugTarace->Finalize();
			SafeDelete(pDebugTarace);
		}

		//	トレース
		void Trace(const c8* str, const c8* tag, b8 bEnable)
		{
			if( pDebugTarace == null )
			{
				c8 temp[512];
				CharSet::SFormat(temp, "[%s]:%s\n", tag, str);
				DebugPrint(temp);
				return;
			}
			pDebugTarace->SetTrace(str, tag, bEnable);
		}
		//	トレース
		void Trace(const c16* str, const c16* tag, b8 bEnable)
		{
			if( pDebugTarace == null )
			{
				c16 temp[512];
				CharSet::SFormat(temp, L"[%s]:%s\n", tag, str);
				DebugPrint(temp);
				return;
			}
			pDebugTarace->SetTrace(str, tag, bEnable);
		}
	}
}

//	トレース定義用マクロ
#define	TRACE_FUNC(func, tag)						\
	static b8 b##func = true;						\
	void Enable##func(b8 bEnable)					\
	{												\
		b##func = bEnable;							\
	}												\
	void func(const c8* format, ...)				\
	{												\
		c8 temp[1024];								\
		va_list list;								\
		va_start(list, format);						\
		vsprintf(temp, format, list);				\
		va_end(list);								\
		kgl::Debug::Trace(temp, tag, b##func);		\
	}												\
	void func(const c16* format, ...)				\
	{												\
		c16 temp[1024];								\
		va_list list;								\
		va_start(list, format);						\
		vswprintf(temp, format, list);				\
		va_end(list);								\
		kgl::Debug::Trace(temp, L##tag, b##func);	\
	}

TRACE_FUNC(SystemTrace, "System");
TRACE_FUNC(ErrorTrace, "Error");
TRACE_FUNC(WarningTrace, "Warning");
TRACE_FUNC(Trace, "Trace");

//	トレース出力待ち
void AsyncTrace(b8 bEnable)
{
	if( kgl::Debug::pDebugTarace != null )
	{
		kgl::Debug::pDebugTarace->AsyncTrace(bEnable);
	}
}
//	トレース出力待ち
void WaitTrace(void)
{
	if( kgl::Debug::pDebugTarace != null )
	{
		kgl::Debug::pDebugTarace->WaitTrace();
	}
}

#endif	// ~#if KGL_DEBUG_TRACE

//======================================================================
//	END OF FILE
//======================================================================
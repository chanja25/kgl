//----------------------------------------------------------------------
//!
//!	@file	kgl.DebugMenu.cpp
//!	@brief	デバッグメニュー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

#if	KGL_DEBUG_MENU
#include "Debug/kgl.DebugMenu.h"
#include "Data/Text/kgl.Lexser.h"
#include "Utility/algorithm/kgl.cio.h"
#include "Render/kgl.Render.h"
#include "Input/kgl.Input.h"
#include "Utility/Path/kgl.Path.h"

#define	DEBUG_TEST_OUTPUT	1

namespace kgl
{
	namespace Debug
	{
		namespace Utility
		{
#if	DEBUG_TEST_OUTPUT
			//----------------------------------------------------------------------
			//	デバッグメニューの構成出力
			//----------------------------------------------------------------------
			void TestOutput(DebugMenuValue* pMenu, s32 iTab = 0, b8 bInit = true)
			{
				if( pMenu == null )return;

				const c8* pPath = "./Config/Debug/DebugMenuResult.txt";
				if( bInit )
				{
					algorithm::cout out(pPath);
					out << "DebugMenu" EOL;
				}
				DebugMenuValue* pNext;
				while( pMenu != null )
				{
					pNext = pMenu->pNext;

					{
						algorithm::cout out(pPath, true);

						c8 cTest[256] = {0};
						for( s32 i=0; i < iTab; i++ )
						{
							cTest[i]	= '\t';
							cTest[i+1]	= '\0';
						}
						switch( pMenu->iValueType )
						{
						case EValue_Group:
							CharSet::SFormat(cTest, "%s[%s]", cTest, pMenu->cTag);
							break;
						case EValue_S32:
							CharSet::SFormat(cTest, "%s<%s, type:s32, default:%d, min:%d, max:%d>", cTest, pMenu->cTag, pMenu->iValue, pMenu->iValueMin, pMenu->iValueMax);
							if( pMenu->iLabelCount > 0 )
							{
								CharSet::SFormat(cTest, "%s(%s", cTest, pMenu->pLabel[0]);
								for( s32 i=1; i < pMenu->iLabelCount; i++ )
								{
									CharSet::SFormat(cTest, "%s, %s", cTest, pMenu->pLabel[i]);
								}
								CharSet::SFormat(cTest, "%s)", cTest);
							}
							break;
						case EValue_F32:
							CharSet::SFormat(cTest, "%s<%s, type:f32, default:%f, min:%f, max:%f>", cTest, pMenu->cTag, pMenu->fValue, pMenu->fValueMin, pMenu->fValueMax);
							break;
						case EValue_Bool:
							CharSet::SFormat(cTest, "%s<%s, type:bool, default:%s>", cTest, pMenu->cTag, pMenu->iValue? "true": "false");
							break;
						}
						CharSet::SFormat(cTest, "%s{%u}" EOL, cTest, pMenu->uiHash);
						out << cTest;
					}
					TestOutput(pMenu->pChild, iTab+1, false);
					
					pMenu = pNext;
				}
			}
#endif	// ~#if	DEBUG_TEST_OUTPUT
			//----------------------------------------------------------------------
			//	デバッグメニューの構成出力
			//----------------------------------------------------------------------
			void OutputHeader(const c8* pPath, const DebugGroupList& List)
			{
				if( List.iCount == 0 )return;

				algorithm::cout out(Path::GetDirectory(pPath) + "/DebugMenuValue.h");
				out << "//----------------------------------------------------------------------" EOL;
				out << "//!" EOL;
				out << "//!	@file	DebugMenuValue.h" EOL;
				out << "//!	@brief	デバッグメニュー関連" EOL;
				out << "//!" EOL;
				out << "//!	@author S.Kisaragi" EOL;
				out << "//----------------------------------------------------------------------" EOL;
				out << "#ifndef	__DEBUGMENU_VALUE_H__" EOL;
				out << "#define	__DEBUGMENU_VALUE_H__" EOL;
				out << EOL;
				out << EOL;
				out << "//----------------------------------------------------------------------" EOL;
				out << "//	デバッグメニューの項目一覧" EOL;
				out << "//----------------------------------------------------------------------" EOL;
				out << "enum DEBUGMENU" EOL;
				out << "{" EOL;

				c8 cTemp[256];
				algorithm::string sParent;
				for( s32 i=0; i < List.iCount; i++ )
				{
					DebugMenuValue* pValue	= List.ppList[i];
					DebugMenuValue* pParent	= List.pGroup;
					DebugMenuValue* pTag	= pValue;

					sParent = "\tDEBUGMENU";
					while( pParent != pTag->pParent )
					{
						while( pParent != pTag->pParent )
						{
							pTag = pTag->pParent;
						}
						sParent += "_";
						sParent += CharSet::ToUpper(cTemp, pTag->cTag);

						pParent	= pTag;
						pTag	= pValue;
					}
					sParent += "_";
					sParent += CharSet::ToUpper(cTemp, pValue->cTag);
					sParent += "," EOL;
					out << sParent;
				}
				out << "};" EOL;
				out << EOL;
				out << EOL;
				out << "#endif	// __DEBUGMENU_VALUE_H__" EOL;
				out << "//=======================================================================" EOL;
				out << "//	END OF FILE" EOL;
				out << "//=======================================================================" EOL;
			}

			void SetupDebugMenu(const Text::Token** ppToken, DebugMenuValue* pParent = null);
			b8 SetupDebugMenuSub(const Text::Token** ppToken, DebugMenuValue* pParent = null);
			void SetupDebugMenuValue(const Text::Token** ppToken, DebugMenuValue* pParent = null);

			//----------------------------------------------------------------------
			//	デバッグメニューのセットアップ
			//----------------------------------------------------------------------
			void SetupDebugMenu(const Text::Token** ppToken, DebugMenuValue* pParent)
			{
				const Text::Token* pToken = *ppToken;
				while( pToken != null )
				{
					if( kgStrcmp(pToken->pToken, "[") == 0 )
					{
						//	階層の追加(終了)
						if( !SetupDebugMenuSub(&pToken, pParent) )
						{
							break;
						}
					}
					else if( kgStrcmp(pToken->pToken, "<") == 0 )
					{
						//	デバッグ値の追加
						SetupDebugMenuValue(&pToken, pParent);
					}
					else
					{
						WarningTrace("構文に謝りが存在します(Token:%s, line:%d)", pToken->pToken, pToken->uiLine);
					}
					if( pToken != null )
					{
						pToken = pToken->pNext;
					}
				}

				*ppToken = pToken;
			}

			//----------------------------------------------------------------------
			//	サブ階層のセットアップ
			//----------------------------------------------------------------------
			b8 SetupDebugMenuSub(const Text::Token** ppToken, DebugMenuValue* pParent)
			{
				const Text::Token* pToken = *ppToken;
				if( pToken == null || kgStrcmp(pToken->pToken, "[") != 0 )
				{
					return false;
				}
				DebugMenuValue* pMenu = null;

				c8 cTemp[TagLengthMax] = {0};
				while( (pToken = pToken->pNext) != null )
				{
					if( kgStrcmp(pToken->pToken, "]") == 0 )
					{
						//	階層の終了判定
						if( kgStricmp(cTemp, "END") == 0 )
						{
							break;
						}
						u32 uiHash;
						{
							TKGLPtr<Hash::IHash> pHash;
							Hash::CreateCRC32(pHash.GetReference());
							uiHash = pHash->Get(cTemp);
						}
						//	階層の追加
						pMenu = (DebugMenuValue*)Memory::Debug::Allocate(sizeof(DebugMenuValue));
						kgZeroMemory(pMenu, sizeof(DebugMenuValue));
						kgStrcpy(pMenu->cTag, cTemp);
						pMenu->uiHash		= uiHash;
						pMenu->pParent		= pParent;
						pMenu->iValueType	= EValue_Group;

						//	親子関係の設定
						if( pParent->pChild == null )
						{
							pParent->pChild = pMenu;
						}
						else
						{
							DebugMenuValue* pValue = pParent->pChild;
							while( pValue->pNext != null )
							{
								pValue = pValue->pNext;
							}
							pValue->pNext	= pMenu;
							pMenu->pPrev	= pValue;
						}
						pToken = pToken->pNext;
						break;
					}
					else if( cTemp[0] == '\0' )
					{
						kgStrcpy(cTemp, pToken->pToken);
					}
				}

				if( pMenu != null )
				{
					//	子階層の設定
					SetupDebugMenu(&pToken, pMenu);
				}
				*ppToken = pToken;
				return (pMenu != null);
			}
			//----------------------------------------------------------------------
			//	デバッグ値のセットアップ
			//----------------------------------------------------------------------
			void SetupDebugMenuValue(const Text::Token** ppToken, DebugMenuValue* pParent)
			{
				const Text::Token* pToken = *ppToken;
				if( pToken == null || kgStrcmp(pToken->pToken, "<") != 0 )
				{
					return;
				}
				DebugMenuValue* pMenu = null;

				s32 iType;
				s32 iDefault, iMin, iMax;
				f32 fDefault, fMin, fMax;
				iType = EValue_S32;
				iDefault = iMin = iMax = 0;
				fDefault = fMin = fMax = 0;

				b8 bSave = false;
				s32 iLabelCount = 0;
				c8 cLabel[32][TagLengthMax] = {0};

				c8 cTemp[TagLengthMax] = {0};
				while( (pToken = pToken->pNext) != null )
				{
					if( kgStrcmp(pToken->pToken, ">") == 0 )
					{
						u32 uiHash;
						{
							TKGLPtr<Hash::IHash> pHash;
							Hash::CreateCRC32(pHash.GetReference());
							uiHash = pHash->Get(cTemp);
						}
						//	デバッグ値の追加
						pMenu = (DebugMenuValue*)Memory::Debug::Allocate(sizeof(DebugMenuValue));
						kgZeroMemory(pMenu, sizeof(DebugMenuValue));
						kgStrcpy(pMenu->cTag, cTemp);
						pMenu->uiHash		= uiHash;
						pMenu->pParent		= pParent;
						pMenu->iValueType	= iType;
						pMenu->bEnableSave	= !!bSave;
						pMenu->iLabelCount	= iLabelCount;
						switch( iType )
						{
						case EValue_F32:
							pMenu->fValue		= Clamp(fDefault, fMin, fMax);
							pMenu->fValueMin	= fMin;
							pMenu->fValueMax	= fMax;
							break;
						case EValue_Bool:
							pMenu->iValue		= Clamp(iDefault, 0, 1);
							pMenu->iValueMin	= 0;
							pMenu->iValueMax	= 1;
							break;
						default:
							pMenu->iValue		= Clamp(iDefault, iMin, iMax);
							pMenu->iValueMin	= iMin;
							pMenu->iValueMax	= iMax;
							break;
						}
						//	ラベルの設定
						if( iLabelCount > 0 )
						{
							u32 uiSize = sizeof(c8[TagLengthMax]) * iLabelCount;
							c8 (*pLabel)[TagLengthMax] = (c8(*)[TagLengthMax])Memory::Debug::Allocate(uiSize);
							kgMemcpy(pLabel, cLabel, uiSize);
							pMenu->pLabel = pLabel;
						}
						//	親子関係の設定
						if( pParent->pChild == null )
						{
							pParent->pChild = pMenu;
						}
						else
						{
							DebugMenuValue* pValue = pParent->pChild;
							while( pValue->pNext != null )
							{
								pValue = pValue->pNext;
							}
							pValue->pNext	= pMenu;
							pMenu->pPrev	= pValue;
						}
						break;
					}
					//	値の名前を設定
					else if( cTemp[0] == '\0' )
					{
						kgStrcpy(cTemp, pToken->pToken);
					}
					//	値の種類設定
					else if( kgStricmp(pToken->pToken, "Type") == 0 )
					{
						if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
						pToken = pToken->pNext->pNext;

						if( kgStricmp(pToken->pToken, "s32") == 0 )			iType = EValue_S32;
						else if( kgStricmp(pToken->pToken, "f32") == 0 )	iType = EValue_F32;
						else if( kgStricmp(pToken->pToken, "bool") == 0 )	iType = EValue_Bool;
					}
					//	デフォルト値を設定
					else if( kgStricmp(pToken->pToken, "Default") == 0 )
					{
						if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
						pToken = pToken->pNext->pNext;

						b8 bSub = false;
						if( kgStrcmp(pToken->pToken, "-") == 0 )
						{
							pToken = pToken->pNext;
							bSub = true;
						}
						switch( iType )
						{
						case EValue_F32:
							fDefault = (f32)atof(pToken->pToken);
							if( bSub ) fDefault =-fDefault;
							break;
						default:
							iDefault = atoi(pToken->pToken);
							if( bSub ) iDefault =-iDefault;
							break;
						}
					}
					//	最大値を設定
					else if( kgStricmp(pToken->pToken, "Max") == 0 )
					{
						if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
						pToken = pToken->pNext->pNext;

						b8 bSub = false;
						if( kgStrcmp(pToken->pToken, "-") == 0 )
						{
							pToken = pToken->pNext;
							bSub = true;
						}
						switch( iType )
						{
						case EValue_F32:
							fMax = (f32)atof(pToken->pToken);
							if( bSub ) fMax =-fMax;
							break;
						default:
							iMax = atoi(pToken->pToken);
							if( bSub ) iMax =-iMax;
							break;
						}
					}
					//	最小値を設定
					else if( kgStricmp(pToken->pToken, "Min") == 0 )
					{
						if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
						pToken = pToken->pNext->pNext;

						b8 bSub = false;
						if( kgStrcmp(pToken->pToken, "-") == 0 )
						{
							pToken = pToken->pNext;
							bSub = true;
						}
						switch( iType )
						{
						case EValue_F32:
							fMin = (f32)atof(pToken->pToken);
							if( bSub ) fMin =-fMin;
							break;
						default:
							iMin = atoi(pToken->pToken);
							if( bSub ) iMin =-iMin;
							break;
						}
					}
					//	ラベルを設定
					else if( kgStricmp(pToken->pToken, "Label") == 0 )
					{
						if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
						pToken = pToken->pNext->pNext;
						if( kgStrcmp(pToken->pToken, "(") != 0 )break;

						iLabelCount = 0;
						while( (pToken = pToken->pNext) != null )
						{
							if( kgStrcmp(pToken->pToken, ")") == 0 )break;
							if( kgStrcmp(pToken->pToken, ",") == 0 )
							{
								iLabelCount ++;
							}
							else
							{
								kgStrcpy(cLabel[iLabelCount], pToken->pToken);
							}
						}
						iLabelCount ++;
					}
					//	セーブの有無を設定
					else if( kgStricmp(pToken->pToken, "Save") == 0 )
					{
						if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
						pToken = pToken->pNext->pNext;

						bSave = (kgStrcmp(pToken->pToken, "true") == 0);
					}
				}

				*ppToken = pToken;
			}

			//----------------------------------------------------------------------
			//	デバッグメニューのセットアップ
			//----------------------------------------------------------------------
			DebugMenuValue* CreateDebugMenu(const c8* pPath)
			{
				Text::CLexer Lexser;
				if( !Lexser.LoadText(pPath) )
				{
					return null;
				}
				const Text::Token* pRoot = Lexser.AnalyzeAll();
				if( Lexser.IsError() )
				{
					return null;
				}

				u32 uiHash;
				{
					TKGLPtr<Hash::IHash> pHash;
					Hash::CreateCRC32(pHash.GetReference());
					uiHash = pHash->Get("root");
				}
				DebugMenuValue* pRootMenu = (DebugMenuValue*)Memory::Debug::Allocate(sizeof(DebugMenuValue));
				kgZeroMemory(pRootMenu, sizeof(DebugMenuValue));
				kgStrcpy(pRootMenu->cTag, "root");
				pRootMenu->uiHash		= uiHash;
				pRootMenu->iValueType	= EValue_Group;

				SetupDebugMenu(&pRoot, pRootMenu);

#if	DEBUG_TEST_OUTPUT
				Utility::TestOutput(pRootMenu);
#endif	// ~#if	DEBUG_TEST_OUTPUT

				return pRootMenu;
			}

			//----------------------------------------------------------------------
			//	デバッグメニューのセットアップ
			//----------------------------------------------------------------------
			void DeleteDebugMenu(DebugMenuValue* pMenu)
			{
				if( pMenu == null )return;

				DebugMenuValue* pNext;
				while( pMenu != null )
				{
					pNext = pMenu->pNext;
					DeleteDebugMenu(pMenu->pChild);

					if( pMenu->pLabel != null )
					{
						Memory::Debug::Free(pMenu->pLabel);
					}
					Memory::Debug::Free(pMenu);
					pMenu = pNext;
				}
			}

			//----------------------------------------------------------------------
			//	要素数を取得
			//----------------------------------------------------------------------
			s32 GetValueCount(DebugMenuValue* pMenu, bool child)
			{
				if( pMenu == null )return 0;

				s32 iCount = 0;
				DebugMenuValue* pNext;
				while( pMenu != null )
				{
					pNext = pMenu->pNext;
					iCount ++;
					if (child)
					{
						iCount += GetValueCount(pMenu->pChild, child);
					}					
					pMenu = pNext;
				}
				return iCount;
			}
			//----------------------------------------------------------------------
			//	値のリストをセットアップ
			//----------------------------------------------------------------------
			s32 SetupValueList(DebugMenuValue*** pppList, DebugMenuValue* pMenu, bool child, s32 iIndex = 0)
			{
				if( pMenu == null )return iIndex;

				DebugMenuValue* pNext;
				while( pMenu != null )
				{
					pNext = pMenu->pNext;

					(*pppList)[iIndex ++] = pMenu;
					if (child)
					{
						iIndex = SetupValueList(pppList, pMenu->pChild, child, iIndex);
					}
					
					pMenu = pNext;
				}
				return iIndex;
			}
			//----------------------------------------------------------------------
			//	値の数を取得
			//----------------------------------------------------------------------
			DebugGroupList CreateDebugValueList(DebugMenuValue* pMenu, bool child)
			{
				DebugGroupList GroupList;
				GroupList.pGroup = pMenu;
				GroupList.iCount = 0;
				GroupList.ppList = null;

				if( pMenu == null )return GroupList;

				s32 iCount = GetValueCount(pMenu->pChild, child);
				if( iCount == 0 )return GroupList;

				DebugMenuValue** ppList = (DebugMenuValue**)Memory::Debug::Allocate(sizeof(DebugMenuValue*) * iCount);
				SetupValueList(&ppList, pMenu->pChild, child, 0);

				GroupList.pGroup = pMenu;
				GroupList.iCount = iCount;
				GroupList.ppList = ppList;
				return GroupList;
			}
			//----------------------------------------------------------------------
			//	値に対応した文字列の取得
			//----------------------------------------------------------------------
			const c8* GetValueString(DebugMenuValue* pMenu, b8 bValueOnly = false, b8 bRoot = false)
			{
				static c8 cTemp[256];

				if( !bValueOnly )
				{
					switch( pMenu->iValueType )
					{
					case EValue_Group:
						CharSet::SFormat(cTemp, bRoot? "[%s]": "%s\t>>", pMenu->cTag);
						break;
					case EValue_S32:
						{
							if( pMenu->iValue >= 0 && pMenu->iValue < pMenu->iLabelCount )
							{
								CharSet::SFormat(cTemp, "%s:\t%s", pMenu->cTag, pMenu->pLabel[pMenu->iValue]);
							}
							else
							{
								CharSet::SFormat(cTemp, "%s:\t%d", pMenu->cTag, pMenu->iValue);
							}
						}
						break;
					case EValue_F32:
						CharSet::SFormat(cTemp, "%s:\t%f", pMenu->cTag, pMenu->fValue);
						break;
					case EValue_Bool:
						CharSet::SFormat(cTemp, "%s:\t%s", pMenu->cTag, pMenu->bValue? "true": "false");
						break;
					}
				}
				else
				{
					switch( pMenu->iValueType )
					{
					case EValue_Group:
						CharSet::SFormat(cTemp, ">>");
						break;
					case EValue_S32:
						{
							if( pMenu->iValue >= 0 && pMenu->iValue < pMenu->iLabelCount )
							{
								CharSet::SFormat(cTemp, "%s", pMenu->pLabel[pMenu->iValue]);
							}
							else
							{
								CharSet::SFormat(cTemp, "%d", pMenu->iValue);
							}
						}
						break;
					case EValue_F32:
						CharSet::SFormat(cTemp, "%f", pMenu->fValue);
						break;
					case EValue_Bool:
						CharSet::SFormat(cTemp, "%s", pMenu->bValue? "true": "false");
						break;
					}
				}

				return cTemp;
			}
		}

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CDebugMenu::CDebugMenu(void)
			: m_pKeyboard(null)
			, m_pRoot(null)
			, m_pSelect(null)
			, m_iGroupCount(0)
			, m_iAddRate(0)
			, m_bEnable(true)
			, m_bVisible(false)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CDebugMenu::~CDebugMenu(void)
		{}

		//----------------------------------------------------------------------
		//	ヘッダーファイルの作成
		//----------------------------------------------------------------------
		b8 CDebugMenu::CreateHeader(const c8* pPath)
		{
			DebugMenuValue* pRoot = Utility::CreateDebugMenu(pPath);
			if( pRoot == null )return false;

			DebugGroupList List = Utility::CreateDebugValueList(pRoot, true);

			Utility::OutputHeader(pPath, List);

			Memory::Debug::Free(List.ppList);
			Utility::DeleteDebugMenu(pRoot);
			return true;
		}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CDebugMenu::Initalize(const c8* pPath)
		{
			if( m_pRoot != null )return false;

			m_pRoot = Utility::CreateDebugMenu(pPath);
			if( m_pRoot == null )return false;
			m_pSelect = m_pRoot->pChild->pChild;

			m_List = Utility::CreateDebugValueList(m_pRoot, true);

			s32 iCount = 0;
			for( s32 i=0; i < m_List.iCount; i++ )
			{
				if( m_List.ppList[i]->iValueType == EValue_Group )
				{
					iCount ++;
				}
			}
			m_pGroupList = (DebugGroupList*)Memory::Debug::Allocate(sizeof(DebugGroupList) * iCount);
			iCount = 0;
			for( s32 i=0; i < m_List.iCount; i++ )
			{
				if( m_List.ppList[i]->iValueType == EValue_Group )
				{
					m_pGroupList[iCount ++] = Utility::CreateDebugValueList(m_List.ppList[i], false);
				}
			}
			m_iGroupCount = iCount;

			Utility::OutputHeader(pPath, m_List);
			Input::Create(m_pKeyboard.GetReference());

			Hash::CreateCRC32(m_pHash.GetReference());

			return true;
		}

		//----------------------------------------------------------------------
		//	解放
		//----------------------------------------------------------------------
		void CDebugMenu::Finalize(void)
		{
			if( m_pRoot == null )return;

			m_pKeyboard = null;
			Memory::Debug::Free(m_List.ppList);
			for( s32 i=0; i < m_iGroupCount; i++ )
			{
				Memory::Debug::Free(m_pGroupList[i].ppList);
			}
			Memory::Debug::Free(m_pGroupList);
			m_iGroupCount = 0;

			Utility::DeleteDebugMenu(m_pRoot);
			m_pRoot		= null;
			m_pSelect	= null;
		}
		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CDebugMenu::Update(void)
		{
#if	KGL_DEBUG_FONT
			if( !m_bEnable )
			{
				SetVisible(false);
			}
			else if( m_pKeyboard->IsKeyPush(Input::EKey::KEY_BACK) )
			{
				SetVisible(!IsVisible());
			}
			if( !IsVisible() )return;

			b8 bValue	= m_pKeyboard->IsKeyPress(Input::EKey::KEY_RCONTROL);
			b8 bUP		= m_pKeyboard->IsKeyPush(Input::EKey::KEY_UP);
			b8 bDown	= m_pKeyboard->IsKeyPush(Input::EKey::KEY_DOWN);
			b8 bLeft	= m_pKeyboard->IsKeyPush(Input::EKey::KEY_LEFT);
			b8 bRight	= m_pKeyboard->IsKeyPush(Input::EKey::KEY_RIGHT);

			if( bValue )
			{
				s32 iAddValue = 0;
				if( bUP )			m_iAddRate++;
				else if( bDown )	m_iAddRate--;
				else if( bLeft )	iAddValue =-1;
				else if( bRight )	iAddValue = 1;

				m_iAddRate = Clamp(m_iAddRate, 0, 7);

				switch( m_pSelect->iValueType )
				{
				case EValue_S32:
					{
						s32 iValue = m_pSelect->iValue;
						m_pSelect->iValue += iAddValue * (s32)Math::Pow(10.0f, (f32)m_iAddRate);
						if( m_pSelect->iValue < m_pSelect->iValueMin )
						{
							m_pSelect->iValue = m_pSelect->iValueMax;
						}
						else if( m_pSelect->iValue > m_pSelect->iValueMax )
						{
							m_pSelect->iValue = m_pSelect->iValueMin;
						}
						if( iValue != m_pSelect->iValue )
						{
							if( m_pSelect->ChangeCallback )
							{
								m_pSelect->ChangeCallback(m_pSelect, m_pSelect->pArg);
							}
						}
					}
					break;
				case EValue_F32:
					{
						f32 fValue = m_pSelect->fValue;
						m_pSelect->fValue += (f32)iAddValue * Math::Pow(10.0f, (f32)m_iAddRate) * 0.0001f;
						if( m_pSelect->fValue < m_pSelect->fValueMin )
						{
							m_pSelect->fValue = m_pSelect->fValueMax;
						}
						else if( m_pSelect->fValue > m_pSelect->fValueMax )
						{
							m_pSelect->fValue = m_pSelect->fValueMin;
						}
						if( fValue != m_pSelect->fValue )
						{
							if( m_pSelect->ChangeCallback )
							{
								m_pSelect->ChangeCallback(m_pSelect, m_pSelect->pArg);
							}
						}
					}
					break;
				case EValue_Bool:
					if( iAddValue != 0 )
					{
						m_pSelect->bValue = !m_pSelect->bValue;
						if( m_pSelect->ChangeCallback )
						{
							m_pSelect->ChangeCallback(m_pSelect, m_pSelect->pArg);
						}
					}
					break;
				}
			}
			else
			{
				if( bUP )
				{
					if( m_pSelect->pPrev != null )
					{
						m_pSelect = m_pSelect->pPrev;
					}
					else
					{
						while( m_pSelect->pNext != null )
						{
							m_pSelect = m_pSelect->pNext;
						}
					}
				}
				else if( bDown )
				{
					if( m_pSelect->pNext != null )
					{
						m_pSelect = m_pSelect->pNext;
					}
					else
					{
						m_pSelect = m_pSelect->pParent->pChild;
					}
				}
				else if( bLeft )
				{
					if( m_pSelect->pParent != m_pRoot )
					{
						m_pSelect = m_pSelect->pParent;
					}
				}
				else if( bRight )
				{
					if( m_pSelect->pChild != null )
					{
						m_pSelect = m_pSelect->pChild;
					}
				}
			}

#endif	// ~#if	KGL_DEBUG_FONT
		}
		//----------------------------------------------------------------------
		//	値変更時のコールバック設定
		//----------------------------------------------------------------------
		void CDebugMenu::Draw(Render::IRenderingSystem* pRenderingSystem)
		{
			(void)pRenderingSystem;
#if	KGL_DEBUG_FONT
			if( !IsVisible() )return;

			s32 iAddValue = (s32)Math::Pow(10.0f, (f32)m_iAddRate);
			f32 fAddValue = Math::Pow(10.0f, (f32)m_iAddRate) * 0.0001f;

			TKGLPtr<Render::IDrawer> pDrawer = pRenderingSystem->Drawer();
			TKGLPtr<Font::IFont> pDebugFont = pRenderingSystem->DebugFont();

			Vector2 vPosTmp;
			Vector2 vPos, vRect, vSize;
			Vector2 vScale(1.0f, 1.0f);
			Vector2 vStartPos(8, 8);
			FColor DrawColor;

			DebugMenuValue* pDraw;
			DebugMenuValue* pValue	= m_pSelect;
			DebugMenuValue* pSelect	= m_pSelect;
			DebugMenuValue* pParent	= m_pRoot;
			//	Rootから順にデバッグメニューを描画していく
			while( pParent != pValue )
			{
				while( pParent != pValue->pParent )
				{
					pSelect	= pValue;
					pValue	= pValue->pParent;
				}
				if( pValue->iValueType != EValue_Group && pSelect == pValue )break;

				vPos = vStartPos;
				vRect.ZeroClear();
				//	描画範囲の確認
				if( pParent == m_pRoot )//ルート直下のグループ名([System]等)
				{
					const c8* pTemp = Utility::GetValueString(pValue, false, true);
					c8 cTemp[256];
					CharSet::SFormat(cTemp, "%s [%d/%.4f]", pTemp, iAddValue, fAddValue);
					vRect = pDebugFont->GetDrawTextSize(cTemp, vScale);
				}
				pDraw = pValue->pChild;
				while( pDraw != null )//グループ内の値
				{
					const c8* pTemp = Utility::GetValueString(pDraw, false);
					vSize = pDebugFont->GetDrawTextSize(pTemp, vScale);
					vRect.x = Max(vRect.x, vSize.x);
					vRect.y += vSize.y;

					pDraw = pDraw->pNext;
				}

				const f32 PaddingX = 3.0f;
				//	下地の描画(左右に多少の余裕を持たせておく)
				vRect.x += PaddingX * 2.0f;
				pDrawer->DrawRect(vStartPos, vRect, FColor(0.125f, 0.125f, 0.125f, 0.75f));
				vStartPos.x += vRect.x;
				vPos.x += PaddingX;

				//	項目の描画
				if( pParent == m_pRoot )//ルート直下のグループ名([System]等)
				{
					const c8* pTemp = Utility::GetValueString(pValue, false, true);
					c8 cTemp[256];
					CharSet::SFormat(cTemp, "%s [%d/%.4f]", pTemp, iAddValue, fAddValue);

					pDebugFont->Draw(pRenderingSystem, cTemp, vPos, vScale, (pValue == pSelect)? FColorRed: FColorOrange);
					vPos.y += pDebugFont->GetDrawTextSize(cTemp, vScale).y;
				}
				pDraw = pValue->pChild;
				while( pDraw != null )//グループ内の値
				{
					//	選択中のグループ以外はグレーで表示
					DrawColor = (pSelect == m_pSelect && pSelect != pValue)? FColorWhite: FColorGray;
					if( pDraw == pSelect )
					{
						vStartPos.y = vPos.y;
						//	選択中の値は赤、親階層の選択項目はピンクで表示
						DrawColor = (pDraw == m_pSelect)? FColorRed: FColorPink;
					}
					pDebugFont->Draw(pRenderingSystem, pDraw->cTag, vPos, vScale, DrawColor);

					const c8* pTemp = Utility::GetValueString(pDraw, true);
					vPosTmp = vPos;
					vPosTmp.x += vRect.x - PaddingX * 2.0f - pDebugFont->GetDrawTextSize(pTemp, vScale).x;
					pDebugFont->Draw(pRenderingSystem, pTemp, vPosTmp, vScale, DrawColor);

					vPos.y += pDebugFont->GetDrawTextSize(pDraw->cTag, vScale).y;
					pDraw = pDraw->pNext;
				}

				pParent	= pValue;
				pValue	= m_pSelect;
			}
#endif	// ~#if	KGL_DEBUG_FONT
		}

		//----------------------------------------------------------------------
		//	有効フラグ設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetEnable(b8 bEnable)
		{
			m_bEnable = !!bEnable;
		}
		//----------------------------------------------------------------------
		//	表示フラグ設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetVisible(b8 bVisible)
		{
			m_bVisible = !!bVisible;
		}
		//----------------------------------------------------------------------
		//	表示フラグ取得
		//----------------------------------------------------------------------
		b8 CDebugMenu::IsVisible(void)
		{
			return !!m_bVisible;
		}
		//----------------------------------------------------------------------
		//	デバッグ項目の検索
		//----------------------------------------------------------------------
		DebugGroupList* CDebugMenu::SearchGroup(const c8* group)
		{
			DebugGroupList* pGroupList = null;

			algorithm::vector<algorithm::string> groups;
			algorithm::Split(groups, group, '.');

			for (auto& group : groups)
			{
				u32 uiHash = m_pHash->Get(group.c_str());
				if (pGroupList == null)
				{
					u32 uiGroupHash = m_pHash->Get(groups[0].c_str());
					for (s32 i = 0; i < m_iGroupCount; i++)
					{
						if (m_pGroupList[i].pGroup->uiHash == uiGroupHash)
						{
							pGroupList = &m_pGroupList[i];
							break;
						}
					}
					continue;
				}
				for (s32 i = 0; i < pGroupList->iCount; i++)
				{
					DebugMenuValue* pList = pGroupList->ppList[i];
					if (pList->uiHash != uiHash)
					{
						continue;
					}
					if (pList->iValueType != EValue_Group)
					{
						continue;
					}
					for (s32 j = 0; j < m_iGroupCount; j++)
					{
						if (m_pGroupList[j].pGroup == pList)
						{
							pGroupList = &m_pGroupList[j];
							break;
						}
					}
					break;
				}
			}
			return pGroupList;
		}
		//----------------------------------------------------------------------
		//	デバッグ項目の検索
		//----------------------------------------------------------------------
		DebugMenuValue* CDebugMenu::SearchValue(const c8* tag, const c8* group, s32 type)
		{
			s32 iCount = 0;
			DebugMenuValue** ppList = null;

			if (group != null)
			{
				auto groupList = SearchGroup(group);
				if (groupList)
				{
					ppList = groupList->ppList;
					iCount = groupList->iCount;
				}
				else
				{
					ErrorTrace("指定されたデバッグメニューが見つかりません(%s)", group);
				}
			}
			else
			{
				ppList = m_List.ppList;
				iCount = m_List.iCount;
			}

			if( ppList != null )
			{
				u32 uiHash = m_pHash->Get(tag);
				for( s32 i=0; i < iCount; i++ )
				{
					if( ppList[i]->uiHash == uiHash )
					{
						if( type == -1 || type == ppList[i]->iValueType )
						{
							return ppList[i];
						}
					}
				}
				ErrorTrace("指定されたデバッグメニューが見つかりません(%s)", tag);
			}
			return null;
		}
		//----------------------------------------------------------------------
		//	値変更時のコールバック設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetChangeCallback(FChangeCallback func, void* arg, const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group);
			if( pValue != null )
			{
				pValue->ChangeCallback	= func;
				pValue->pArg			= arg;
			}
		}
		//----------------------------------------------------------------------
		//	値設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetValue(s32 value, const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_S32);
			if( pValue != null )
			{
				if( pValue->iValue != value )
				{
					pValue->iValue = value;
					if( pValue->ChangeCallback != null )
					{
						pValue->ChangeCallback(pValue, pValue->pArg);
					}
				}
			}
		}
		//----------------------------------------------------------------------
		//	値設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetValueF32(f32 value, const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_F32);
			if( pValue != null )
			{
				if( pValue->fValue != value )
				{
					pValue->fValue = value;
					if( pValue->ChangeCallback != null )
					{
						pValue->ChangeCallback(pValue, pValue->pArg);
					}
				}
			}
		}
		//----------------------------------------------------------------------
		//	文字列設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetValueBool(b8 value, const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_Bool);
			if( pValue != null )
			{
				if( pValue->bValue != value )
				{
					pValue->bValue = value;
					if( pValue->ChangeCallback != null )
					{
						pValue->ChangeCallback(pValue, pValue->pArg);
					}
				}
			}
		}

		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		s32 CDebugMenu::GetValue(const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_S32);
			if( pValue != null )
			{
				return pValue->iValue;
			}
			return 0;
		}
		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		f32 CDebugMenu::GetValueF32(const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_F32);
			if( pValue != null )
			{
				return pValue->fValue;
			}
			return 0;
		}
		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		b8 CDebugMenu::GetValueBool(const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_Bool);
			if( pValue != null )
			{
				return !!pValue->bValue;
			}
			return 0;
		}
		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		const c8* CDebugMenu::GetValueString(const c8* tag, const c8* group)
		{
			DebugMenuValue* pValue = SearchValue(tag, group, EValue_S32);
			if( pValue != null )
			{
				if( pValue->iValue >= 0 && pValue->iValue < pValue->iLabelCount )
				{
					return pValue->pLabel[pValue->iValue];
				}
			}
			return null;
		}

		//----------------------------------------------------------------------
		//	値設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetValue(s32 value, s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				if( pValue->iValue != value )
				{
					pValue->iValue = value;
					if( pValue->ChangeCallback != null )
					{
						pValue->ChangeCallback(pValue, pValue->pArg);
					}
				}
			}
		}
		//----------------------------------------------------------------------
		//	値設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetValueF32(f32 value, s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				if( pValue->fValue != value )
				{
					pValue->fValue = value;
					if( pValue->ChangeCallback != null )
					{
						pValue->ChangeCallback(pValue, pValue->pArg);
					}
				}
			}
		}
		//----------------------------------------------------------------------
		//	文字列設定
		//----------------------------------------------------------------------
		void CDebugMenu::SetValueBool(b8 value, s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				if( pValue->bValue != value )
				{
					pValue->bValue = value;
					if( pValue->ChangeCallback != null )
					{
						pValue->ChangeCallback(pValue, pValue->pArg);
					}
				}
			}
		}

		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		s32 CDebugMenu::GetValue(s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				return pValue->iValue;
			}
			return 0;
		}
		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		f32 CDebugMenu::GetValueF32(s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				return pValue->fValue;
			}
			return 0;
		}
		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		b8 CDebugMenu::GetValueBool(s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				return !!pValue->bValue;
			}
			return 0;
		}
		//----------------------------------------------------------------------
		//	値取得
		//----------------------------------------------------------------------
		const c8* CDebugMenu::GetValueString(s32 index)
		{
			DebugMenuValue* pValue = m_List.ppList[index];
			if( pValue != null )
			{
				if( pValue->iValue >= 0 && pValue->iValue < pValue->iLabelCount )
				{
					return pValue->pLabel[pValue->iValue];
				}
			}
			return null;
		}
	}
}

#endif	// ~#if KGL_DEBUG_MENU

//======================================================================
//	END OF FILE
//======================================================================
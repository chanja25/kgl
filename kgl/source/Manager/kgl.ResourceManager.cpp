//----------------------------------------------------------------------
//!
//!	@file	kgl.GameManager.cpp
//!	@brief	ゲーム管理関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Manager/kgl.ResourceManager.h"
#include "Manager/kgl.GameManager.h"
#include "Utility/Path/kgl.Path.h"

//	リソースの取得用マクロ
#define	IMPLEMENT_GETLOADED_FUNCTION(_class_, _function_)	\
	_class_* _function_(const c8* pPath)					\
	{														\
		return Cast<_class_>(GetLoadedObject(pPath));		\
	}														\
	_class_* _function_(s32 iIndex)							\
	{														\
		return Cast<_class_>(GetLoadedObject(iIndex));		\
	}

namespace kgl
{
	namespace Audio
	{
		//----------------------------------------------------------------------
		//	サウンドの生成
		//----------------------------------------------------------------------
		b8 Create(ISound** ppSound, const void* pData)
		{
			TKGLPtr<IDecoderManager> pDecoderManager;
			pDecoderManager = KGLAudioManager()->SoundDriver()->DecoderManager();

			TKGLPtr<IDecoder> pDecoder;
			if( !pDecoderManager->CreateDecoderFromWave(pDecoder.GetReference(), pData) )
			{
				return false;
			}
			return Create(ppSound, pDecoder);
		}
	}

	namespace Xml
	{
		//----------------------------------------------------------------------
		//	XMLオブジェクトの生成
		//----------------------------------------------------------------------
		b8 Create(IXml** ppXml, const void* pData)
		{
			TKGLPtr<IXml> pXml;
			Create(pXml.GetReference());

			if( !pXml->Create(pData, KGLFileReader()->GetSize(pData)) )
			{
				return false;
			}
			pXml->AddRef();
			*ppXml = pXml;
			return true;
		}
	}

	namespace Manager
	{
		class CResourceManager;
		//	バインド情報
		struct BindInfo
		{
			CResourceManager*	pThis;			//!< リソース管理(自身のポインタ)
			algorithm::string	strExtension;	//!< 拡張子
			s32					iResource;		//!< リソースID
			FCreateResource		func;			//!< ファイル読み込み
		};

		//======================================================================
		//!	リソース管理クラス
		//======================================================================
		class CResourceManager
			: public IResourceManager
		{
		public:
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(IGameManager* pGameManager)
			{
				if( pGameManager == null )
				{
					return false;
				}
				m_pGameManager		= pGameManager;
				m_pFileReader		= pGameManager->FileReader();

				SetupBind();

				return true;
			}

		public:
			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 Load(const c8* pPath, s32 iResource)
			{
				return Load(pPath, -1, iResource, false);
			}
			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 Load(s32 iIndex, s32 iResource)
			{
				return Load(null, iIndex, iResource, false);
			}
			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 LoadBlock(const c8* pPath, s32 iResource)
			{
				return Load(pPath, -1, iResource, true);
			}
			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 LoadBlock(s32 iIndex, s32 iResource)
			{
				return Load(null, iIndex, iResource, true);
			}
			//----------------------------------------------------------------------
			//	解放
			//----------------------------------------------------------------------
			void Unload(const c8* pPath)
			{
				m_pFileReader->Unload(pPath);
			}
			//----------------------------------------------------------------------
			//	解放
			//----------------------------------------------------------------------
			void Unload(s32 iIndex)
			{
				m_pFileReader->Unload(iIndex);
			}

		public:
			//----------------------------------------------------------------------
			//	読み込み中か？
			//----------------------------------------------------------------------
			b8 IsLoading(void)
			{
				return m_pFileReader->IsLoading();
			}

		public:
			//----------------------------------------------------------------------
			//	リソースのバインド
			//----------------------------------------------------------------------
			void BindResource(const c8* pExtension, s32 iResource, FCreateResource CreateFunc)
			{
				for( u32 i=0; i < m_BindInfo.size(); i++ )
				{
					if( pExtension == m_BindInfo[i].strExtension )
					{
						m_BindInfo[i].iResource = iResource;
						m_BindInfo[i].func = CreateFunc;
						return;
					}
				}
				BindInfo Info;
				Info.pThis = this;
				Info.strExtension = pExtension;
				Info.iResource = iResource;
				Info.func = CreateFunc;

				m_BindInfo.push_back(Info);
			}

		public:
			//----------------------------------------------------------------------
			//	ロード済みのオブジェクトを取得
			//----------------------------------------------------------------------
			INLINE File::IExtraObject* GetLoadedObject(const c8* pPath)
			{
				return m_pFileReader->GetLoadedObject(pPath).pExtra;
			}
			//----------------------------------------------------------------------
			//	ロード済みのオブジェクトを取得
			//----------------------------------------------------------------------
			INLINE File::IExtraObject* GetLoadedObject(s32 iIndex)
			{
				return m_pFileReader->GetLoadedObject(iIndex).pExtra;
			}

		public:
			IMPLEMENT_GETLOADED_FUNCTION(Render::IVertexShader, GetVertexShader);
			IMPLEMENT_GETLOADED_FUNCTION(Render::IPixelShader, GetPixelShader);
			IMPLEMENT_GETLOADED_FUNCTION(Render::ITexture, GetTexture);
			IMPLEMENT_GETLOADED_FUNCTION(Font::IFont, GetFont);
			IMPLEMENT_GETLOADED_FUNCTION(Audio::ISound, GetSound);
			IMPLEMENT_GETLOADED_FUNCTION(Xml::IXml, GetXml);

		private:
			//----------------------------------------------------------------------
			//	読み込みのコールバック(リソースの初期化)
			//----------------------------------------------------------------------
			template<typename T, b8 bDelete, b8 (*CreateFunc)(T**, const void*)>
			static File::IExtraObject* CreateResource(const void* pData, u32 uiSize, b8& bDeleteData)
			{
				(void)uiSize;
				T* pObject = null;
				CreateFunc(&pObject, pData);
				bDeleteData = bDelete;
				return pObject;
			}
			//----------------------------------------------------------------------
			//	バインド情報のセットアップ
			//----------------------------------------------------------------------
			void SetupBind(void)
			{
				BindResource("PVS",		EResource::VertexShader,	CreateResource<Render::IVertexShader,	false,	Render::Create>);
				BindResource("PPS",		EResource::PixelShader,		CreateResource<Render::IPixelShader,	true,	Render::Create>);
				BindResource("KTEX",	EResource::Texture,			CreateResource<Render::ITexture,		true,	Render::Create>);
				BindResource("DDS",		EResource::Texture,			CreateResource<Render::ITexture,		true,	Render::Create>);
				BindResource("KTF",		EResource::Font,			CreateResource<Font::IFont,				true,	Font::Create>);
				BindResource("WAV",		EResource::Sound,			CreateResource<Audio::ISound,			true,	Audio::Create>);
				BindResource("XML",		EResource::Xml,				CreateResource<Xml::IXml,				true,	Xml::Create>);

				m_NullBindInfo.pThis = this;
				m_NullBindInfo.iResource = EResource::Unknown;
				m_NullBindInfo.func = null;
			}

			//----------------------------------------------------------------------
			//	パスからリソースタイプの取得
			//----------------------------------------------------------------------
			s32 GetResourceType(const c8* pPath)
			{
				c8 cExtension[32];
				CharSet::ToUpper(cExtension, Path::GetExtension(pPath));

				for( u32 i=0; i < m_BindInfo.size(); i++ )
				{
					if( cExtension == m_BindInfo[i].strExtension )
					{
						return m_BindInfo[i].iResource;
					}
				}
				return EResource::Unknown;
			}
			//----------------------------------------------------------------------
			//	ロードコールバック関数の取得
			//----------------------------------------------------------------------
			BindInfo* GetBindInfo(s32 iResource)
			{
				if( iResource != EResource::Unknown )
				{
					for( u32 i=0; i < m_BindInfo.size(); i++ )
					{
						if( iResource == m_BindInfo[i].iResource )
						{
							return &m_BindInfo[i];
						}
					}
				}
				return &m_NullBindInfo;
			}

		private:
			//----------------------------------------------------------------------
			//	読み込み
			//----------------------------------------------------------------------
			b8 Load(const c8* pPath, s32 iIndex, s32 iResource, b8 bBlock)
			{
				BindInfo* pInfo = GetBindInfo(iResource);

				b8 bRet = false;
				if( bBlock )
				{
					auto pData = pPath?	m_pFileReader->LoadBlock(pPath, LoadedCallback, pInfo):
										m_pFileReader->LoadBlock(iIndex, LoadedCallback, pInfo);

					bRet = pData != null;
				}
				else
				{
					bRet = pPath?	m_pFileReader->Load(pPath, LoadedCallback, pInfo):
									m_pFileReader->Load(iIndex, LoadedCallback, pInfo);
				}
				return bRet;
			}

		private:
			//----------------------------------------------------------------------
			//	読み込みのコールバック(リソースの初期化)
			//----------------------------------------------------------------------
			static void THREADFUNC LoadedCallback(const void* pData, void* pArg, u32 uiSize, const c8* pPath, s32 iIndex)
			{
				(void)uiSize;
				if( pData )
				{
					BindInfo* pInfo = (BindInfo*)pArg;
					TKGLPtr<File::IFileReader> pFileReader = pInfo->pThis->m_pFileReader;
					if( pInfo->iResource == EResource::Unknown )
					{
						s32 iResource = pInfo->pThis->GetResourceType(pPath);
						pInfo = pInfo->pThis->GetBindInfo(iResource);
					}

					b8 bPath = iIndex == File::NoManagerIndex;
					b8 bDeleteData = false;
					TKGLPtr<File::IExtraObject> pObject;
					if( pInfo->func && (*pObject.GetReference() = pInfo->func(pData, uiSize, bDeleteData)) != null )
					{
						bPath?	pFileReader->SetLoadedObject(pPath, pObject, bDeleteData):
								pFileReader->SetLoadedObject(iIndex, pObject, bDeleteData);
					}
					else
					{
						bPath?	pFileReader->Unload(pPath):
								pFileReader->Unload(iIndex);
					}
				}
			}

		private:
			TKGLPtr<IGameManager>			m_pGameManager;		//!< ゲーム管理
			TKGLPtr<File::IFileReader>		m_pFileReader;		//!< ファイル管理
			algorithm::vector<BindInfo>		m_BindInfo;			//!< バインド情報
			BindInfo						m_NullBindInfo;		//!< バインド情報(生成するオブジェクトが未定)
		};

		//----------------------------------------------------------------------
		//	リソース管理の生成
		//----------------------------------------------------------------------
		b8 Create(IResourceManager** pResourceManager, IGameManager* pGameManager)
		{
			TKGLPtr<CResourceManager> pManager = new CResourceManager;
			if( !pManager->Initialize(pGameManager) )
			{
				false;
			}
			*pResourceManager = pManager;
			(*pResourceManager)->AddRef();
			return true;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
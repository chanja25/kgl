//----------------------------------------------------------------------
//!
//!	@file	kgl.GameManager.cpp
//!	@brief	ゲーム管理関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Manager/kgl.GameManager.h"
#include "System/kgl.System.h"

namespace kgl
{
	namespace Manager
	{
		//	リソース管理の生成
		extern b8 Create(IResourceManager** pResourceManager, IGameManager* pGameManager);

		//======================================================================
		//	ゲーム管理クラス
		//======================================================================
		class CGameManager
			: public IGameManager
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CGameManager(void)
			{
#if	KGL_DEBUG_MENU
				m_pDebugMenu = null;
#endif	// ~#if	KGL_DEBUG_MENU
			}

			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(Window::IWindow* pWindow, File::IFileReader* pFileReader, IGraphicDriver* pGraphicDriver, Frame::IFrameControl* pFrameControl)
			{
				m_pWindow			= pWindow;
				m_pFileReader		= pFileReader;
				m_pGraphicDriver	= pGraphicDriver;
				m_pFrameControl		= pFrameControl;

				//	オーディオ管理の生成
				if( !Audio::Create(m_pAudioManager.GetReference()) )
				{
					return false;
				}
				//	リソース管理の生成
				Create(m_pResourceManager.GetReference(), this);
				//	シーン管理の生成
				Scene::Create(m_pSceneManager.GetReference());

#if	KGL_DEBUG_PROFILE
				Frame::Create(m_pFPSCounter.GetReference());
				Frame::Create(m_pRenderFPSCounter.GetReference());
#endif	// ~#if	KGL_DEBUG_PROFILE
#if	KGL_DEBUG_MENU
				m_pDebugMenu = new Debug::CDebugMenu;
				m_pDebugMenu->Initalize();
#endif	// ~#if	KGL_DEBUG_MENU

				return true;
			}

			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			void Finalize(void)
			{
				if( m_pGraphicDriver.IsValid() )
				{
					m_pGraphicDriver->Finalize();
				}
				m_pGraphicDriver = null;
				m_pSceneManager = null;
				m_pWindow = null;
				m_pGameInfo = null;
				m_pAudioManager = null;

#if	KGL_DEBUG_MENU
				m_pDebugMenu->Finalize();
				SafeDelete(m_pDebugMenu);
#endif	// ~#if	KGL_DEBUG_MENU

				m_pFileReader->WaitLoad();
				m_pFileReader->UnloadAll();
				m_pFileReader = null;
				m_pResourceManager = null;
			}

		public:
			//----------------------------------------------------------------------
			//	ゲーム管理の生成
			//----------------------------------------------------------------------
			static b8 CreateGameManager(Window::IWindow* pWindow, File::IFileReader* pFileReader, IGraphicDriver* pGraphicDriver, Frame::IFrameControl* pFrameControl)
			{
				if( pGameManager != null ||
					pWindow == null ||
					pFileReader == null ||
					pGraphicDriver == null ||
					pFrameControl == null )
				{
					return false;
				}
				pGameManager = new CGameManager;
				pGameManager->AddRef();
				if( !pGameManager->Initialize(pWindow, pFileReader, pGraphicDriver, pFrameControl) )
				{
					SafeRelease(pGameManager);
					return false;
				}
				return true;
			}
			//----------------------------------------------------------------------
			//	ゲーム管理の解放
			//----------------------------------------------------------------------
			static void DeleteGameManager(void)
			{
				if( pGameManager != null )
				{
					pGameManager->Finalize();
					SafeRelease(pGameManager);
				}
			}
			//----------------------------------------------------------------------
			//	インスタンスの取得
			//----------------------------------------------------------------------
			static FORCEINLINE CGameManager* GetInstance(void)
			{
				return pGameManager;
			}

		private:
			//	ゲーム管理
			static CGameManager* pGameManager;
		};
		//	ゲーム管理
		CGameManager* CGameManager::pGameManager	= null;

		//----------------------------------------------------------------------
		//	ゲーム管理の生成
		//----------------------------------------------------------------------
		b8 CreateGameManager(Window::IWindow* pWindow, File::IFileReader* pFileReader, IGraphicDriver* pGraphicDriver, Frame::IFrameControl* pFrameControl)
		{
			return CGameManager::CreateGameManager(pWindow, pFileReader, pGraphicDriver, pFrameControl);
		}
		//----------------------------------------------------------------------
		//	ゲーム管理の解放
		//----------------------------------------------------------------------
		void DeleteGameManager(void)
		{
			CGameManager::DeleteGameManager();
		}
		//----------------------------------------------------------------------
		//	ゲーム管理の取得
		//----------------------------------------------------------------------
		IGameManager* GameManager(void)
		{
			return CGameManager::GetInstance();
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
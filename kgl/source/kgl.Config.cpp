//----------------------------------------------------------------------
//!
//!	@file	kgl.Config.cpp
//!	@brief	コンフィグ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

//----------------------------------------------------------------------
//	システム設定
//----------------------------------------------------------------------
namespace kgl
{
	namespace Config
	{
		//	更新処理関連
		b8		FPSFix				= false;
		s32		TargetFPS			= 60;

		//	描画処理関連
		s32		ResolutionWidth		= 1280;
		s32		ResolutionHeight	= 720;
		Vector2	Resolution			= Vector2(1280.0f, 720.0f);
		Vector2	InvResolution		= Vector2(1.0f / Resolution.x, 1.0f / Resolution.y);
		b8		EnableVsync			= false;

		//	ハード情報
		u64		PhysicalMemory		= 0;
		s32		NumberOfProcessors	= 1;

		//	アプリケーション関連
		b8		ExitRequest			= false;

#if	!KGL_FINAL_RELEASE
		//	エディター関連
		b8		Editor				= false;
		b8		DataConvertOnly		= false;
		b8		ShaderRebuild		= false;
#endif	// ~#if	!KGL_FINAL_RELEASE

		//	デバッグ用
#if	KGL_DEBUG_MEMORY
		ApplicationMemoryInfo	MemoryInfo;
#endif	// ~#if KGL_DEBUG_MEMORY
	}
}

#if KGL_USE_TLSF_ALLOCATOR
//----------------------------------------------------------------------
//	メモリプールサイズの取得
//----------------------------------------------------------------------
size_t MemoryPoolSize(void)
{
	return MB(128);
}
#if	KGL_DEBUG_MEMORY
//----------------------------------------------------------------------
//	デバッグメモリプールサイズの取得
//----------------------------------------------------------------------
size_t DebugMemoryPoolSize(void)
{
	return MB(128);
}
#endif	// ~#if KGL_DEBUG_MEMORY
#endif // ~#if KGL_USE_TLSF_ALLOCATOR


//=======================================================================
//	END OF FILE
//=======================================================================
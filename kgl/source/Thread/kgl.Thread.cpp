//----------------------------------------------------------------------
//!
//!	@file	kgl.Thread.cpp
//!	@brief	スレッド
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Thread/kgl.Thread.h"
#include <process.h>

#define	FORCE_SINGLE_THREAD	0

namespace kgl
{
	namespace Thread
	{
#if	!KGL_FINAL_RELEASE
		typedef struct tagTHREADNAME_INFO
		{
		   DWORD dwType; // must be 0x1000
		   LPCSTR szName; // pointer to name (in user addr space)
		   DWORD dwThreadID; // thread ID (-1=caller thread)
		   DWORD dwFlags; // reserved for future use, must be zero
		} THREADNAME_INFO;
		//----------------------------------------------------------------------
		//	スレッド名の設定
		//----------------------------------------------------------------------
		void SetThreadName(DWORD dwThreadID, LPCSTR szThreadName)
		{
			(void)dwThreadID;
			(void)szThreadName;
#if	WINDOWS
			THREADNAME_INFO info;
			info.dwType = 0x1000;
			info.szName = szThreadName;
			info.dwThreadID = dwThreadID;
			info.dwFlags = 0;

			__try
			{
				//デバッグ実行の場合はスレッド名を設定しておく
				RaiseException(0x406D1388, 0, sizeof(info)/sizeof(DWORD), (const ULONG_PTR*)&info);
			}
			__except(EXCEPTION_EXECUTE_HANDLER)
			{
			}
#endif	// ~#if	WINDOWS
		}
#endif	// ~#if !KGL_FINAL_RELEASE

		//----------------------------------------------------------------------
		//	呼び出し元のスレッドIDの取得
		//----------------------------------------------------------------------
		u32 GetThreadId(void)
		{
			return GetCurrentThreadId();
		}

		const u32 NonExitCode	= 0xffffffff;
		//	Platform Dependent
		//======================================================================
		//!	スレッドクラス
		//======================================================================
		class CThread
			: public IThread
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CThread(void)
				: m_hThread(null)
				, m_uiExitCode(NonExitCode)
				, m_iPriority(-1)
				, m_iMask(0)
			{}
			//----------------------------------------------------------------------
			//	起動
			//----------------------------------------------------------------------
			b8 Start(FThreadFunction Function, void* pArg, u32 uiStackSize, const c8* pName)
			{
				(void)pName;
				if( m_hThread != null )
					return false;

				m_hThread = (HANDLE)_beginthreadex(null, uiStackSize, Function, pArg, 0, &m_uiThreadID);

#if	!KGL_FINAL_RELEASE
				if( pName )
				{
					SetThreadName(m_uiThreadID, pName);
				}
#endif	// ~#if !KGL_FINAL_RELEASE
				if( m_iPriority != -1 )
				{
					SetPriority(m_iPriority);
				}
#if !FORCE_SINGLE_THREAD
				if( m_iMask != 0 )
#endif
				{
					SetAffinityMask(m_iMask);
				}
				return m_hThread != null;
			}
			//----------------------------------------------------------------------
			//	終了
			//----------------------------------------------------------------------
			void Exit(b8 bWait)
			{
				if( m_hThread == null )
					return;

				if( bWait )
				{
					WaitForSingleObject(m_hThread, INFINITE);
					GetExitCode();
				}
				else
				{
					TerminateThread(m_hThread, 0);
				}

				CloseHandle(m_hThread);
				m_hThread = null;
			}
			//----------------------------------------------------------------------
			//	終了確認
			//----------------------------------------------------------------------
			b8 IsExit(void)
			{
				dword dwCode;
				GetExitCodeThread(m_hThread, &dwCode);
				return dwCode != STILL_ACTIVE;
			}
			//----------------------------------------------------------------------
			//	終了コード取得(IsExitがtrueの場合に有効)
			//----------------------------------------------------------------------
			u32 GetExitCode(void)
			{
				if( m_uiExitCode == NonExitCode )
				{
					dword dwCode;
					GetExitCodeThread(m_hThread, &dwCode);
					m_uiExitCode = (u32)dwCode;
				}
				return m_uiExitCode;
			}
			//----------------------------------------------------------------------
			//	スレッドの優先設定
			//----------------------------------------------------------------------
			void SetPriority(s32 iPriority)
			{
				m_iPriority = iPriority-15;
				if( m_hThread != null )
				{
					SetThreadPriority(m_hThread, m_iPriority);
				}
			}
			//----------------------------------------------------------------------
			//	スレッドを実行可能なプロセッサ設定
			//----------------------------------------------------------------------
			void SetAffinityMask(s32 iMask)
			{
#if FORCE_SINGLE_THREAD
				iMask = 0x01;
#endif
				m_iMask = iMask;
				if( m_hThread != null )
				{
					SetThreadAffinityMask(m_hThread, (DWORD_PTR)m_iMask);
				}
			}

		private:
			u32		m_uiThreadID;		//!< スレッドID
			HANDLE	m_hThread;			//!< スレッドハンドル
			u32		m_uiExitCode;		//!< 終了コード
			s32		m_iPriority;		//!< スレッドの優先度
			s32		m_iMask;			//!< スレッドを実行可能なプロセッサ
		};

		//----------------------------------------------------------------------
		// スレッドの生成
		//----------------------------------------------------------------------
		b8 Create(IThread** ppThread)
		{
			*ppThread = new CThread;
			(*ppThread)->AddRef();

			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
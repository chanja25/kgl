//----------------------------------------------------------------------
//!
//!	@file	kgl.CharSet.cpp
//!	@brief	文字関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//	include
//----------------------------------------------------------------------
#include "kgl.h"
#include "CharSet/kgl.CharSet.h"

#if	WINDOWS
#include <atlbase.h>
#include <atlconv.h>
#endif	// ~#if	WINDOWS

namespace kgl
{
	namespace CharSet
	{
		//	Platform Dependent
		//----------------------------------------------------------------------
		//	ShiftJISをUTF16に変換
		//----------------------------------------------------------------------
		b8 ConvertShiftJISToUTF16(c16* pDst, const c8* pSrc, u32* pSize)
		{
			(void)pDst;
			(void)pSrc;
			(void)pSize;
#if	WINDOWS
			//	ShiftJISからUTF-16へ変換
			u32 nSize = MultiByteToWideChar(CP_ACP, 0, pSrc, -1, null, 0);
			if( *pSize < nSize ) return false;

			MultiByteToWideChar(CP_ACP, 0, pSrc, -1, pDst, nSize);
			*pSize = nSize;
			return true;
#else	// ~#if	WINDOWS
			return false;
#endif	// ~#if	WINDOWS
		}
		//----------------------------------------------------------------------
		//	ShiftJISをUTF8に変換
		//----------------------------------------------------------------------
		b8 ConvertShiftJISToUTF8(c8* pDst, const c8* pSrc, u32* pSize)
		{
			(void)pDst;
			(void)pSrc;
			(void)pSize;
#if	WINDOWS
			//	ShiftJISからUTF-16へ変換
			u32 nSize = MultiByteToWideChar(CP_ACP, 0, pSrc, -1, null, 0);

			TSmartPtr<s8> pBuffUTF16 = new s8[nSize * 2 + 2];
			MultiByteToWideChar(CP_ACP, 0, pSrc, -1, (c16*)pBuffUTF16, nSize);

			//	UTF-16からUTF-8へ変換
			u32 nSizeUTF8 = WideCharToMultiByte(CP_UTF8, 0, pBuffUTF16, -1, null, 0, null, null);
			if( *pSize < nSizeUTF8 ) return false;

			WideCharToMultiByte(CP_UTF8, 0, pBuffUTF16, -1, pDst, nSizeUTF8, null, null);
			*pSize = nSizeUTF8;
			return true;
#else	// ~#if	WINDOWS
			return false;
#endif	// ~#if	WINDOWS
		}
		//----------------------------------------------------------------------
		//	UTF16をShiftJISに変換
		//----------------------------------------------------------------------
		b8 ConvertUTF16ToShiftJIS(c8* pDst, const c16* pSrc, u32* pSize)
		{
			(void)pDst;
			(void)pSrc;
			(void)pSize;
#if	WINDOWS
			//	UTF-16からShift-JISへ変換
			u32 nSize = WideCharToMultiByte(CP_ACP, 0, pSrc, -1, null, 0, null, null);
			if( *pSize < nSize ) return false;

			WideCharToMultiByte(CP_ACP, 0, pSrc, -1, pDst, nSize, null, null);
			*pSize = nSize;
			return true;
#else	// ~#if	WINDOWS
			return false;
#endif	// ~#if	WINDOWS
		}
		//----------------------------------------------------------------------
		//	UTF8をShiftJISに変換
		//----------------------------------------------------------------------
		b8 ConvertUTF8ToShiftJIS(c8* pDst, const c8* pSrc, u32* pSize)
		{
			(void)pDst;
			(void)pSrc;
			(void)pSize;
#if	WINDOWS
			//	UTF-8からUTF-16へ変換
			u32 nSize = MultiByteToWideChar(CP_UTF8, 0, pSrc, -1, null, 0);
			TSmartPtr<s8> pBuffUTF16 = new s8[nSize * 2 + 2];
			MultiByteToWideChar(CP_UTF8, 0, pSrc, -1, pBuffUTF16, nSize);

			//	UTF-16からShift-JISへ変換
			u32 nSizeSJis = WideCharToMultiByte(CP_ACP, 0, pBuffUTF16, -1, null, 0, null, null);
			if( *pSize < nSizeSJis ) return false;

			WideCharToMultiByte(CP_ACP, 0, (c16*)pBuffUTF16, -1, pDst, nSizeSJis, null, null);
			*pSize = nSizeSJis;
			return true;
#else	// ~#if	WINDOWS
			return false;
#endif	// ~#if	WINDOWS
		}

		//----------------------------------------------------------------------
		//	ShiftJISをUTF16に変換
		//----------------------------------------------------------------------
		algorithm::wstring ToUTF16(const algorithm::string& str)
		{
			(void)str;
			algorithm::wstring out;
#if	WINDOWS
			//	ShiftJISからUTF-16へ変換
			u32 nSize = MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, null, 0);
			out.resize(nSize / 2);

			MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, &out[0], nSize);
#endif	// ~#if	WINDOWS
			return out;
		}
		//----------------------------------------------------------------------
		//	UTF16をUTF8に変換
		//----------------------------------------------------------------------
		algorithm::string ToUTF8(const algorithm::wstring& str)
		{
			(void)str;
			algorithm::string out;
#if	WINDOWS
			//	UTF-16からUTF-8へ変換
			u32 nSize = WideCharToMultiByte(CP_UTF8, 0, str.c_str(), -1, null, 0, null, null);
			out.resize(nSize);

			WideCharToMultiByte(CP_UTF8, 0, str.c_str(), -1, &out[0], nSize, null, null);
#endif	// ~#if	WINDOWS
			return out;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
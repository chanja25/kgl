//----------------------------------------------------------------------
//!
//!	@file	kgl.Path.cpp
//!	@brief	パス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Path/kgl.Path.h"

namespace kgl
{
	namespace Path
	{
		//----------------------------------------------------------------------
		//	拡張子取得
		//----------------------------------------------------------------------
		const c8* GetExtension(const c8* pPath)
		{
			u32 uiLen = kgStrlen(pPath);
			u32 i;

			for( i = uiLen - 1; i > 0; i-- )
			{
				if( pPath[i - 1] == '.' )
					break;
			}

			return ( i > 0 )? &pPath[i]: "";
		}

		//----------------------------------------------------------------------
		//	ファイル名取得
		//----------------------------------------------------------------------
		const c8* GetFileName(const c8* pPath)
		{
			u32 uiLen = kgStrlen(pPath);
			u32 i;

			for( i=uiLen - 1; i > 0; i-- )
			{
				if( pPath[i - 1] == '/' || pPath[i - 1] == '\\' )
					break;
			}

			return &pPath[i];
		}
		//----------------------------------------------------------------------
		//!	ファイル名(拡張子除く)取得
		//----------------------------------------------------------------------
		const c8* GetFileNameWithoutExtension(c8* pOut, const c8* pPath)
		{
			u32 uiLen = kgStrlen(pPath);
			u32 i, j;

			j = 0;
			for( i=uiLen - 1; i > 0; i-- )
			{
				if( pPath[i - 1] == '.' && j == 0 )
					j = i - 1;

				if( pPath[i - 1] == '/' || pPath[i - 1] == '\\' )
					break;
			}

			if( j <= i )	j = uiLen-i;

			kgMemcpy(pOut, &pPath[i], j);

			pOut[j] = '\0';
			return pOut;
		}

		//----------------------------------------------------------------------
		//	ディレクトリ取得
		//----------------------------------------------------------------------
		const c8* GetDirectory(c8* pOut, const c8* pPath)
		{
			u32 uiLen = kgStrlen(pPath);
			u32 i;

			for( i=uiLen - 1; i > 0; i-- )
			{
				if( pPath[i - 1] == '/' || pPath[i - 1] == '\\' )
					break;
			}

			kgMemcpy(pOut, pPath, i);
			pOut[i] = '\0';
			return pOut;
		}

		//----------------------------------------------------------------------
		//	ファイル名(拡張子除く)取得
		//----------------------------------------------------------------------
		algorithm::string GetFileNameWithoutExtension(const c8* pPath)
		{
			u32 uiLen = kgStrlen(pPath);
			u32 i, j;

			j = 0;
			for( i=uiLen - 1; i > 0; i-- )
			{
				if( pPath[i - 1] == '.' && j == 0 )
					j = i - 1;

				if( pPath[i - 1] == '/' || pPath[i - 1] == '\\' )
					break;
			}

			if( j <= i )	j = uiLen-i;

			algorithm::string strName = "";
			strName.reserve(j - i);
			for( ; i < j; i++ )
			{
				strName += pPath[i];
			}

			return strName;
		}

		//----------------------------------------------------------------------
		//	ディレクトリ取得
		//----------------------------------------------------------------------
		algorithm::string GetDirectory(const c8* pPath)
		{
			u32 uiLen = kgStrlen(pPath);
			u32 i, j;

			for( i=uiLen - 1; i > 0; i-- )
			{
				if( pPath[i - 1] == '/' || pPath[i - 1] == '\\' )
					break;
			}

			algorithm::string strName = "";
			strName.reserve(i);
			for( j = 0; j < i; j++ )
			{
				strName += pPath[j];
			}
			return strName;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
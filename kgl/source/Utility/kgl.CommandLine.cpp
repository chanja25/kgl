//----------------------------------------------------------------------
//!
//!	@file	kgl.CommandLine.cpp
//!	@brief	コマンドライン管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Manager/kgl.Manager.h"
#include "Utility/kgl.CommandLine.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	コマンドライン管理クラス
		//======================================================================
		class CCommandLine
			: public ICommandLine
		{
		private:
			//!	コマンド情報
			struct CommandInfo
			{
				c8	cCommand[COMMAND_MAX];
			};
			//!	管理用マネージャ定義
			typedef TManager<CommandInfo>	CManager;

		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CCommandLine(const c8* pCommandLine)
			{
				Initialize(pCommandLine);
			}
			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CCommandLine(void)
			{
				m_Manager.Finalize();
			}

			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Initialize(const c8* pCommandLine)
			{
				if( pCommandLine == null )
				{
#if WINDOWS
					pCommandLine = GetCommandLineA();
#else	// ~#if WINDOWS
					return false;
#endif	// ~#if WINDOWS
				}
				m_Manager.Initialize(COMMAND_COUNT_MAX);

				u32 uiLen = kgStrlen(pCommandLine);

				c8 cTemp;
				c8 cCommand[COMMAND_MAX];
				u32 uiNow = 0;
				//	コマンドを単語単位に切り分ける
				for( u32 i=0; i < uiLen; i++ )
				{
					cTemp = pCommandLine[i];

					if( cTemp == ' ' || cTemp == '\t' || cTemp == '\0' )
					{
						continue;
					}
					if( cTemp == '\"' )
					{
						for( i ++; pCommandLine[i] != '\"'; i++ )
						{
							cTemp = pCommandLine[i];
							if( cTemp == '\0' )	break;
							cCommand[uiNow ++] = cTemp;
						}
					}
					else
					{
						for( ; i < uiLen; i++ )
						{
							cTemp = pCommandLine[i];
							if( cTemp == ' ' || cTemp == '\t' || cTemp == '\"' || cTemp == '\0' )	break;
							cCommand[uiNow ++] = cTemp;
						}
					}

					cCommand[uiNow ++] = '\0';
					AddCommand(cCommand);
					uiNow = 0;
				}

				return true;
			}

			//----------------------------------------------------------------------
			//	コマンド数の取得
			//----------------------------------------------------------------------
			u32 GetCount(void)
			{
				return m_Manager.UseCount();
			}

			//----------------------------------------------------------------------
			//	コマンドの取得
			//----------------------------------------------------------------------
			const c8* GetCommand(u32 uiNo)
			{
				if( uiNo >= m_Manager.UseCount() )	return null;
				CManager::CItem* pItem = m_Manager.GetFirst();
				for( u32 i=0; i < uiNo; i ++ )
				{
					pItem = pItem->GetNext();
				}

				return pItem->GetObject().cCommand;
			}

			//----------------------------------------------------------------------
			//	コマンドの追加
			//----------------------------------------------------------------------
			b8 AddCommand(const c8* pCommand)
			{
				CManager::CItem* pItem = m_Manager.CreateItem();
				if( pItem == null )	return false;

				c8* pTemp = pItem->GetObject().cCommand;
				kgStrcpy(pTemp, pCommand);
				return true;
			}

			//----------------------------------------------------------------------
			//	コマンドの削除
			//----------------------------------------------------------------------
			b8 EraseCommand(u32 uiNo)
			{
				if( uiNo >= GetCount() )	return false;
				CManager::CItem* pItem = m_Manager.GetFirst();
				for( u32 i=0; i < uiNo; i ++ )
				{
					pItem = pItem->GetNext();
				}

				m_Manager.ReleaseItem(pItem);
				return true;
			}

		private:
			CManager	m_Manager;
		};

		//----------------------------------------------------------------------
		// コマンドライン管理の生成
		//----------------------------------------------------------------------
		b8 Create(ICommandLine** ppCommandLine, const c8* pCommandLine)
		{
			*ppCommandLine = new CCommandLine(pCommandLine);
			(*ppCommandLine)->AddRef();

			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
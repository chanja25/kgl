//----------------------------------------------------------------------
//!
//!	@file	kgl.Utility.cpp
//!	@brief	ユーティリティ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Sort/kgl.Sort.h"
#include "Utility/algorithm/kgl.sort.h"

namespace kgl
{
	namespace Sort
	{
		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		template<typename T>
		TSortSystem<T>::TSortSystem(b8 bStable, b8 bThread)
			: m_pWork(null)
			, m_pBegin(null)
			, m_uiCount(0)
			, m_compare(null)
			, m_bExit(false)
			, m_bInitThread(false)
			, m_bStable(!!bStable)
			, m_bThread(!!bThread)
			, m_uiLimitSingle(LimitSimgleDefault)
			, m_uiLimitDual(LimitDualDefault)
			, m_uiLimitTriple(LimitTripleDefault)
			, m_uiRunThreadNum(0)
			, m_uiUseCoreNum(0)
		{
			if( bThread )
			{
				kgZeroMemory(m_SortInfo, sizeof(m_SortInfo));

				for( u32 i=0; i < ThreadMax-1; i++ )
				{
					Thread::Create(m_pEvent[i].GetReference());
				}
				for( u32 i=0; i < ThreadMax; i++ )
				{
					Thread::Create(m_pThread[i].GetReference());
					m_pThread[i]->SetPriority(Thread::EPriority::Highest);
					m_pThread[i]->Start((FThreadFunction)SortThreadMain, this);
				}
				m_bInitThread = true;
			}
		}

		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		template<typename T>
		TSortSystem<T>::~TSortSystem(void)
		{}

		//----------------------------------------------------------------------
		//	ソート
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::SortReqest(T* pBegin, u32 uiCount, compare Func)
		{
			//	スレッドが有効でかつシングルスレッドの制限を超えていればスレッドを使用する
			if( m_bThread && m_uiLimitSingle >= uiCount )
			{
				//m_pBegin;
				//m_pWork;
				if( m_uiLimitDual < uiCount )
				{
				}
				else if( m_uiLimitTriple < uiCount )
				{
				}
				else
				{
				}
			}
			else
			{
				if( m_bStable )
				{
					StableSort(pBegin, m_pWork, uiCount, Func);
				}
				else
				{
					Sort(pBegin, uiCount, Func);
				}
			}
		}

		//----------------------------------------------------------------------
		//	安定ソートの有効設定
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::SetEnableStable(b8 bEnable)
		{
			m_bStable = !!bThread;
		}
		//----------------------------------------------------------------------
		//	スレッドの有効設定
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::SetEnableThread(b8 bEnable)
		{
			m_bThread = !!bThread;

			if( bThread && m_bInitThread )
			{
				kgZeroMemory(m_SortInfo, sizeof(m_SortInfo));

				for( u32 i=0; i < ThreadMax-1; i++ )
				{
					Thread::Create(m_pEvent[i].GetReference());
				}
				for( u32 i=0; i < ThreadMax; i++ )
				{
					Thread::Create(m_pThread[i].GetReference());
					m_pThread[i]->SetPriority(Thread::EPriority::Highest);
					m_pThread[i]->Start(SortThread, this);
				}
				m_bInitThread = true;
			}
		}
		//----------------------------------------------------------------------
		//	スレッドの有効設定
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::SetThreadLimit(u32 uiSingle, u32 uiDual, u32 ucTriple)
		{
			m_uiLimitSingle	= uiSingle;
			m_uiLimitDual	= uiDual;
			m_uiLimitTriple	= ucTriple;
		}

		//----------------------------------------------------------------------
		//	ソート
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::Sort(T* pBegin, u32 uiCount, compare Func)
		{
			algorithm::qsort(pBegin, uiCount, Func);
		}
		//----------------------------------------------------------------------
		//	ソート
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::StableSort(T* pBegin, T* pWork, u32 uiCount, compare Func)
		{
			(void)pWork;
			algorithm::msort(pBegin, uiCount, Func);
		}

		//----------------------------------------------------------------------
		//	ソートスレッド
		//----------------------------------------------------------------------
		template<typename T>
		u32 TSortSystem<T>::SortThreadMain(void* pArg)
		{
			TSortSystem<T>* pThis = (TSortSystem<T>*)pArg;
			pThis->SortThread();

			return 0;
		}

		//----------------------------------------------------------------------
		//	ソートスレッド
		//----------------------------------------------------------------------
		template<typename T>
		void TSortSystem<T>::SortThread(void)
		{
			u32 uiId = m_uiRunThreadNum ++;

			TKGLPtr<Thread::IEvent> pEvent = m_pEvent[uiId];
			SortInfo& Info = m_SortInfo[uiId];
			while( m_bExit )
			{
				pEvent->Wait();

				if( Info.pBegin )
				{
					if( m_bStable )
					{
						StableSort(Info.pBegin, Info.pWork, Info.uiCount, m_compare);
					}
					else
					{
						Sort(Info.pBegin, Info.uiCount, m_compare);
					}
					T* pWork = Info.pWork;
					u32 uiCount = Info.uiCount;
					if( (uiId & 0x1) == 0 )
					{
						for( u32 i = 0; i < uiCount; ++ i )
						{
							pWork[i] = Info.pBegin[i];
						}
					}
					else
					{
						u32 j = Info.uiCount-1;
						for( u32 i = 0; i < uiCount; ++ i )
						{
							pWork[j-i] = Info.pBegin[i];
						}
					}
				}
			}
		}

		void Test()
		{
			TSortSystem<u32> Sort;

			u32 uiTest[256];
			for( u32 i=0; i < ArrayCount(uiTest); i++ )
			{
				uiTest[i] = rand();
			}
			Sort.SortReqest(uiTest, ArrayCount(uiTest), algorithm::utility::lessequal);
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
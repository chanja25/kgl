//----------------------------------------------------------------------
//!
//!	@file	kgl.Hash.cpp
//!	@brief	ハッシュ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Hash/Kgl.Hash.h"

namespace kgl
{
	namespace Hash
	{
		//======================================================================
		//!	ハッシュ(CRC32)クラス
		//======================================================================
		class CHashCRC32
			: public IHash
		{
		private:
			static bool m_bInit;
			static u32 m_uiTable[256];

			INLINE u32 Reflect(u32 reflect, s8 cc)
			{
				u32 result_value = 0;
				// Swap bit 0 for bit 7, bit 1 For bit 6, etc....
				for( int pos = 1; pos < (cc + 1); pos++ )
				{
					if( reflect & 1 )
					{
						result_value |= (1 << (cc - pos));
					}
					reflect >>= 1;
				}
				return result_value;
			}
		public:
			//!	コンストラクタ
			CHashCRC32(void)
			{
				if( m_bInit == true ) return;
				m_bInit = true;

				const u32 uiPoly = 0x04c11db7;
				u32 i, j, uiTemp;
				for( i = 0; i < 256; i++ )
				{
					uiTemp = Reflect(i, 8) << 24;
					for( j = 0; j < 8; j++ )
					{
						uiTemp = (uiTemp << 1) ^ ((uiTemp & (1 << 31)) ? uiPoly: 0);
					}
					m_uiTable[i] = Reflect(uiTemp, 32);
				}
			}
			//!	ハッシュ値取得
			//!	@param pString	[in] 文字列
			//!	@return ハッシュ値
			u32 Get(const c8* pString)
			{
				if( !pString )	return 0;

				register u32 uiHash	= 0xffffffff;
				while( *pString != '\0' )
				{
					uiHash = ((uiHash >> 8) & 0x00ffffff) ^ m_uiTable[(uiHash ^ *pString ++) & 0xff];
				}
				return uiHash ^ 0xffffffff;
			}
			//!	ハッシュ値取得
			//!	@param pPtr		[in] ハッシュ値を作成するデータの先頭アドレス
			//!	@param uiSize	[in] データサイズ
			//!	@return ハッシュ値
			u32 Get(const void* pPtr, u32 uiSize)
			{
				if( !pPtr )	return 0;

				register u32 uiHash	= 0xffffffff;
				register const u8* p = (const u8*)pPtr;
				while( uiSize-- )
				{
					uiHash = (uiHash >> 8) ^ m_uiTable[(uiHash & 0xFF) ^ *p++];
				}
				return uiHash ^ 0xffffffff;
			}
		};
		bool CHashCRC32::m_bInit = false;
		u32 CHashCRC32::m_uiTable[256];

		//----------------------------------------------------------------------
		//	ハッシュクラスの生成
		//----------------------------------------------------------------------
		b8 CreateCRC32(IHash** ppHash)
		{
			*ppHash = new CHashCRC32;
			(*ppHash)->AddRef();
			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
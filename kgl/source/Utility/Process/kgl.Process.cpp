//----------------------------------------------------------------------
//!
//!	@file	kgl.Process.cpp
//!	@brief	プロセス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Process/kgl.Process.h"

namespace kgl
{
	namespace Process
	{
#if	WINDOWS
		//======================================================================
		//!	プロセス制御
		//======================================================================
		class CProcess
			: public IProcess
		{
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CProcess(void)
				: m_hThread(null)
				, m_hProcess(null)
			{}
			//----------------------------------------------------------------------
			//	別プロセスの起動
			//----------------------------------------------------------------------
			b8 Create(ProcessInfo& Info)
			{
				if( m_hThread )
				{
					CloseHandle(m_hThread);
					CloseHandle(m_hProcess);
				}

				PROCESS_INFORMATION pi;
				STARTUPINFOA si;

				ZeroMemory(&si,sizeof(si));
				si.cb = sizeof(si);
				//	指定されたプロセスを起動する
				if( CreateProcessA(
					Info.cExeName,
					Info.cCommandLine,
					null,
					null,
					false,
					NORMAL_PRIORITY_CLASS,
					null,
					Info.cCurrentDir,
					&si,
					&pi
					) )
				{
					return false;
				}

				m_hThread	= pi.hThread;
				m_hProcess	= pi.hProcess;
				return true;
			}
			//----------------------------------------------------------------------
			//	解放処理
			//----------------------------------------------------------------------
			s32 Release(void)
			{
				if( ReferenceCount() == 1 )
				{
					CloseHandle(m_hThread);
					CloseHandle(m_hProcess);
				}
				return IKGLBase::Release();
			}
			//----------------------------------------------------------------------
			//	プロセスの終了待ち
			//----------------------------------------------------------------------
			b8 WaitProcess(u32 uiTimeout)
			{
				return WaitForSingleObject(m_hProcess, uiTimeout) == WAIT_OBJECT_0;
			}

		private:
			HANDLE	m_hThread;	//!< スレッド
			HANDLE	m_hProcess;	//!< プロセス
		};
#endif	// ~#if WINDOWS

		//	プロセス制御の生成
		b8 Create(IProcess** ppProcess)
		{
#if	WINDOWS
			*ppProcess = new CProcess;
			(*ppProcess)->AddRef();
			return true;
#else	// ~#if WINDOWS
			return false;
#endif	// ~#if WINDOWS
		}
	}
}


//=======================================================================
//	END OF FILE
//=======================================================================
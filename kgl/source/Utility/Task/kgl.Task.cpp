//----------------------------------------------------------------------
//!
//!	@file	kgl.Task.cpp
//!	@brief	非同期タスク管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Task/kgl.Task.h"
#include "Utility/Manager/kgl.Manager.h"
#include "Utility/algorithm/kgl.Find.h"
#include "Thread/kgl.Thread.h"
#include "Thread/kgl.Event.h"
#include "Thread/kgl.CriticalSection.h"

namespace kgl
{
	namespace Task
	{

		//!	作業情報
		struct TaskManageInfo
			: public TaskInfo
		{
			s32	iTaskId;	//!< タスクに割り当てられたID
		};
		//	タスク情報管理
		typedef	Utility::TManager<TaskManageInfo>	CTaskInfoManager;

		//	作業スレッド情報
		struct WorkThreadInfo
		{
			s32							iThreadNo;		//!< スレッド番号
			bool						bAutoSelect;	//!< スレッドの自動割り当てフラグ
			Thread::IThread*			pThread;		//!< スレッド管理
			Thread::IEvent*				pEvent;			//!< イベント管理

			CTaskInfoManager			TaskManager;	//!< タスク管理
			CTaskInfoManager::CItem*	pTask;			//!< タスク情報
		};
		//	待ち中のタスク情報
		struct WaitTaskInfo
		{
			CTaskInfoManager			TaskManager;	//!< タスク管理
			CTaskInfoManager::CItem*	pTask;			//!< タスク情報
		};

		static Thread::ICriticalSection*		s_pCS		= null;
		static Utility::TNArray<WorkThreadInfo>	s_WorkThreadInfo;
		static Utility::TNArray<s32>			s_iTaskIds;
		static s32								s_iNowTaskId;
		static WaitTaskInfo						s_WaitTaskInfo;
		static b8								s_bInit		= false;
		static b8								s_bExit		= true;

#define	SCOPE_LOCK()	SCOPE_LOCK_CRITICALSECTION(s_pCS)

		//----------------------------------------------------------------------
		//	タスクの追加
		//----------------------------------------------------------------------
		INLINE b8 AddTask(TaskInfo& Info, TaskState& State)
		{
			WorkThreadInfo*	pInfo = null;
			CTaskInfoManager::CItem* pTask;

			SCOPE_LOCK();

			s32 iThreadNo = Info.iThreadNo;
			//	-1の場合は空いているスレッドを検索して実行する
			if( Info.iThreadNo == -1 )
			{
				u32 i, uiSize;
				uiSize = s_WorkThreadInfo.size();
				//	スレッドの空きを検索
				for( i = 0; i < uiSize; i++ )
				{
					if( !IsAutoSelect((s32)i) )	continue;

					pInfo = &s_WorkThreadInfo[i];
					if( pInfo->pTask == null ) { pInfo->iThreadNo = (s32)i; break; }
				}

				//	空いているスレッドが無い場合は実行待ちリストに追加する
				if( i == uiSize )
				{
					pTask = s_WaitTaskInfo.TaskManager.CreateItem();
					if( pTask == null )	return false;

					TaskManageInfo& WorkInfo = pTask->GetObject();
					WorkInfo.Func		= Info.Func;
					WorkInfo.pArg		= Info.pArg;
					WorkInfo.iThreadNo	= Info.iThreadNo;
					WorkInfo.iTaskId	= s_iNowTaskId;
					s_iTaskIds.push_back(s_iNowTaskId);
					s_iNowTaskId ++;

					State.iThreadNo = iThreadNo;
					if( s_WaitTaskInfo.pTask == null )	s_WaitTaskInfo.pTask = pTask;
					return true;
				}
			}
			else
			{
				pInfo = &s_WorkThreadInfo[Info.iThreadNo];
			}
			//	実行用のリストに追加する
			pTask = pInfo->TaskManager.CreateItem();
			if( pTask == null ) return false;

			State.iThreadNo = iThreadNo;
			State.iTaskId	= s_iNowTaskId;
			{
				TaskManageInfo& WorkInfo = pTask->GetObject();
				WorkInfo.Func		= Info.Func;
				WorkInfo.pArg		= Info.pArg;
				WorkInfo.iThreadNo	= Info.iThreadNo;
				WorkInfo.iTaskId	= s_iNowTaskId;
			}
			s_iTaskIds.push_back(s_iNowTaskId);
			s_iNowTaskId ++;

			if( pInfo->pTask == null )
			{
				pInfo->pTask = pTask;
				pInfo->pEvent->SetSignal();
			}

			return true;
		}

		//----------------------------------------------------------------------
		//	タスクの追加
		//----------------------------------------------------------------------
		INLINE b8 AddTaskNoLock(TaskInfo& Info)
		{
			WorkThreadInfo*	pInfo;
			CTaskInfoManager::CItem* pTask;

			pInfo = &s_WorkThreadInfo[Info.iThreadNo];

			pTask = pInfo->TaskManager.CreateItem();
			if( pTask == null )	return false;

			kgMemcpy(&pTask->GetObject(), &Info, sizeof(Info));

			pInfo->pTask = pTask;

			return true;
		}

		//----------------------------------------------------------------------
		//	作業用スレッド
		//----------------------------------------------------------------------
		u32 THREADFUNC WorkThread(void* pArg)
		{
			WorkThreadInfo*	pWorkInfo = (WorkThreadInfo*)pArg;
			TaskManageInfo* pTask;
			CTaskInfoManager::CItem* pItem;

			while( true )
			{
				//	作業が無ければ待ち状態に移行する
				if( pWorkInfo->pTask == null )
				{
					pWorkInfo->pEvent->Wait();
				}
				if( s_bExit )	break;

				if( pWorkInfo->pTask )
				{
					pTask = &pWorkInfo->pTask->GetObject();
					//	タスクの実行
					pTask->Func(pTask->pArg, pWorkInfo->iThreadNo);

					{
						SCOPE_LOCK();
						//	タスクを削除して次のタスクへ進める
						u32 uiNo = algorithm::binary_search(pTask->iTaskId, &s_iTaskIds[0], s_iTaskIds.size());
						s_iTaskIds.erase(uiNo);

						pItem = pWorkInfo->pTask;
						pWorkInfo->pTask = pItem->GetNext();
						pWorkInfo->TaskManager.ReleaseItem(pItem);
						//	タスクが無くなれば待ちリストからタスクを取得する
						if( pWorkInfo->pTask == null &&
							pWorkInfo->bAutoSelect )
						{
							if( s_WaitTaskInfo.pTask )
							{
								pItem = s_WaitTaskInfo.pTask;
								pTask = &pItem->GetObject();
								pTask->iThreadNo = pWorkInfo->iThreadNo;
								AddTaskNoLock(*pTask);
								s_WaitTaskInfo.pTask = pItem->GetNext();
								s_WaitTaskInfo.TaskManager.ReleaseItem(pItem);
							}
						}
					}
				}
			}

			return 0;
		}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 Initialize(u32 uiMaxThread, u32 uiMaxTask)
		{
			if( s_bInit )	return false;
			s_bInit = true;
			s_bExit = false;

			Thread::Create(&s_pCS);

			//	タスクID管理領域確保
			s_iTaskIds.reserve((uiMaxThread+1) * uiMaxTask);
			//	タスク管理初期化(スレッド指定無し)
			s_WaitTaskInfo.pTask = null;
			s_WaitTaskInfo.TaskManager.Initialize(uiMaxTask);

			//	タスク管理初期化(スレッド指定無し)
			s_WorkThreadInfo.reserve(uiMaxThread);
			for( u32 i=0; i < uiMaxThread; i++ )
			{
				WorkThreadInfo* pInfo = &s_WorkThreadInfo[i];
				s_WorkThreadInfo.push_back();

				pInfo->iThreadNo	= (s32)i;
				pInfo->pTask		= null;
				pInfo->TaskManager.Initialize(uiMaxTask);

				Thread::Create(&pInfo->pEvent);

				Thread::Create(&pInfo->pThread);
				pInfo->pThread->Start(WorkThread, pInfo, KB(32), "TaskThread");

				EnableAutoSelect(pInfo->iThreadNo);
			}

			return true;
		}

		//----------------------------------------------------------------------
		//	終了処理
		//----------------------------------------------------------------------
		void Finalize(void)
		{
			if( !s_bInit )	return;

			s_bInit = false;
			s_bExit = true;
			u32 uiSize = s_WorkThreadInfo.size();
			for( u32 i=0; i < uiSize; i++ )
			{
				WorkThreadInfo*	pInfo = &s_WorkThreadInfo[i];

				pInfo->pEvent->SetSignal();
				pInfo->pThread->Exit();
				SafeRelease(pInfo->pEvent);
				SafeRelease(pInfo->pThread);

				pInfo->TaskManager.Finalize();
				pInfo->pTask = null;
			}
			s_WorkThreadInfo.release();
			s_WaitTaskInfo.TaskManager.Finalize();
			s_iTaskIds.release();

			SafeRelease(s_pCS);
		}

		//----------------------------------------------------------------------
		//	作業リクエスト
		//----------------------------------------------------------------------
		b8 RequestTask(TaskInfo* pInfo, TaskState* pState)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::RequestTask)");

			return AddTask(*pInfo, *pState);
		}

		//----------------------------------------------------------------------
		//	作業が終了しているか？
		//----------------------------------------------------------------------
		INLINE b8 IsFinish(s32 iTaskId)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::IsFinish)");

			SCOPE_LOCK();
			if( s_iTaskIds.size() == 0 )	return true;
			return algorithm::binary_search(iTaskId, &s_iTaskIds[0], (s32)s_iTaskIds.size()) == algorithm::NotFind;
		}

		//----------------------------------------------------------------------
		//	スレッドが作業中か？
		//----------------------------------------------------------------------
		INLINE b8 IsWorking(s32 iThreadNo)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::IsWaiting)");

			return s_WorkThreadInfo[iThreadNo].pTask != null;
		}

		//----------------------------------------------------------------------
		//!	指定タスクが終了するまで待つ
		//----------------------------------------------------------------------
		void WaitTask(s32 iTaskId)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::WaitTask)");
			while( !IsFinish(iTaskId) )	kgSleep(0);
		}

		//----------------------------------------------------------------------
		//	指定スレッドのタスクが全て終了するまで待つ
		//----------------------------------------------------------------------
		void WaitThreadTask(s32 iThreadNo)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::WaitThreadTask)");
			while( !IsWorking(iThreadNo) )	kgSleep(0);
		}

		//----------------------------------------------------------------------
		//	スレッドのタスクが全て終了するまで待つ
		//----------------------------------------------------------------------
		void WaitThreadAllTask(void)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::WaitThreadTask)");
			s32 i, iSize;
			iSize = (s32)s_WorkThreadInfo.size();
			while( true )
			{
				for( i = 0; i < iSize; i++ )
				{
					if( IsWorking(i) )	break;
				}
				if( i == iSize )	break;
				kgSleep(0);
			}
		}

		//----------------------------------------------------------------------
		//	スレッドの自動割り当てをオンにする
		//----------------------------------------------------------------------
		void EnableAutoSelect(s32 iThreadNo)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::EnableAutoSelect)");
			s_WorkThreadInfo[iThreadNo].bAutoSelect = true;
		}
		//----------------------------------------------------------------------
		//	スレッドの自動割り当てをオフにする
		//----------------------------------------------------------------------
		void DisableAutoSelect(s32 iThreadNo)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::DisableAutoSelect)");
			s_WorkThreadInfo[iThreadNo].bAutoSelect = false;
		}
		//----------------------------------------------------------------------
		//	スレッドの自動割り当ての有無を取得
		//----------------------------------------------------------------------
		b8 IsAutoSelect(s32 iThreadNo)
		{
			kgAssert(s_bInit, "kgl::Task::Initializeが呼ばれていません(kgl::Task::IsAutoSelect)");
			return !!s_WorkThreadInfo[iThreadNo].bAutoSelect;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Window.cpp
//!	@brief	ウィンドウ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "System/kgl.Window.h"
#include "Thread/kgl.Thread.h"
#include "Thread/kgl.Event.h"
#include <functional>

#include <imm.h>
#pragma comment(lib, "imm32.lib")

namespace kgl
{
	namespace Window
	{
		//----------------------------------------------------------------------
		//	EStyleからウィンドウスタイルの取得
		//----------------------------------------------------------------------
		u32 GetWindowStyle(u32 uiStyle)
		{
			u32 uiWindowStyle = 0;
			uiWindowStyle |= (uiStyle & EStyle::Overlapped		)?	WS_OVERLAPPED: 0;
			uiWindowStyle |= (uiStyle & EStyle::OverlappedWindow)?	WS_OVERLAPPEDWINDOW: 0;
			uiWindowStyle |= (uiStyle & EStyle::Child			)?	WS_CHILD: 0;
			uiWindowStyle |= (uiStyle & EStyle::Popup			)?	WS_POPUP: 0;
			uiWindowStyle |= (uiStyle & EStyle::PopupWindow		)?	WS_POPUPWINDOW: 0;
			uiWindowStyle |= (uiStyle & EStyle::ClipChildren	)?	WS_CLIPCHILDREN: 0;
			uiWindowStyle |= (uiStyle & EStyle::ClipSiblings	)?	WS_CLIPSIBLINGS: 0;
			uiWindowStyle |= (uiStyle & EStyle::Caption			)?	WS_CAPTION: 0;
			uiWindowStyle |= (uiStyle & EStyle::SystemMenu		)?	WS_SYSMENU: 0;
			uiWindowStyle |= (uiStyle & EStyle::MinimizeBox		)?	WS_MINIMIZEBOX: 0;
			uiWindowStyle |= (uiStyle & EStyle::MaximizeBox		)?	WS_MAXIMIZEBOX: 0;
			uiWindowStyle |= (uiStyle & EStyle::Border			)?	WS_BORDER: 0;
			uiWindowStyle |= (uiStyle & EStyle::DlgFrame		)?	WS_DLGFRAME: 0;
			uiWindowStyle |= (uiStyle & EStyle::ThickFrame		)?	WS_THICKFRAME: 0;
			uiWindowStyle |= (uiStyle & EStyle::HScroll			)?	WS_HSCROLL: 0;
			uiWindowStyle |= (uiStyle & EStyle::VScroll			)?	WS_VSCROLL: 0;
			uiWindowStyle |= (uiStyle & EStyle::Visible			)?	WS_VISIBLE: 0;
			uiWindowStyle |= (uiStyle & EStyle::Disable			)?	WS_DISABLED: 0;
			uiWindowStyle |= (uiStyle & EStyle::Maximize		)?	WS_MAXIMIZE: 0;
			uiWindowStyle |= (uiStyle & EStyle::Minimize		)?	WS_MINIMIZE: 0;
			uiWindowStyle |= (uiStyle & EStyle::TabStop			)?	WS_TABSTOP: 0;
			return uiWindowStyle;
		}
		//----------------------------------------------------------------------
		//	EStyleから拡張ウィンドウスタイルの取得
		//----------------------------------------------------------------------
		u32 GetWindowStyleEx(u32 uiStyle)
		{
			u32 uiWindowStyle = 0;
			uiWindowStyle |= (uiStyle & EStyle::AbsPosition	)?	WS_EX_ACCEPTFILES: 0;
			uiWindowStyle |= (uiStyle & EStyle::AcceptFiles	)?	WS_EX_TOOLWINDOW: 0;
			uiWindowStyle |= (uiStyle & EStyle::TopMost		)?	WS_EX_TOPMOST: 0;
			uiWindowStyle |= (uiStyle & EStyle::ToolWindow	)?	WS_EX_TOOLWINDOW: 0;
			uiWindowStyle |= (uiStyle & EStyle::Transparent	)?	WS_EX_TRANSPARENT: 0;
			uiWindowStyle |= (uiStyle & EStyle::Layred	    )?	WS_EX_LAYERED: 0;
			return uiWindowStyle;
		}

		//	カスタムメッセージ
		namespace ECustomMsg
		{
			const s32 ShowCursor		= 0x00001000;	//!< カーソルの表示制御
			const s32 DeviceLost		= 0x00001001;	//!< デバイスロスト(DirectX9用)
			const s32 EnableIME			= 0x00001002;	//!< IMEを有効化
			const s32 ChangeWindowMode	= 0x00002001;	//!< ウィドウモードの変更
		}

		//	Platform Dependent
		//======================================================================
		//	ウィンドウクラス
		//======================================================================
		class CWindow
			: public IWindow
		{
		private:
			typedef	TKGLPtr<Thread::IThread>	IThread;
			typedef	TKGLPtr<Thread::IEvent>		IEvent;

			//!	ウィンドウ情報
			struct WindowInfo
			{
				CWindow*	pThis;
				IEvent		Event;
				const c16*	pName;
				BITFIELD	bFullScreen:1;
				BITFIELD	bShow:1;
			};
		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CWindow(void)
				: m_hWnd(null)
				, m_hImc(null)
				, m_hIcon(null)
				, m_hMenu(null)
				, m_iX(-1)
				, m_iY(-1)
				, m_iWidth(Config::ResolutionWidth)
				, m_iHeight(Config::ResolutionHeight)
				, m_bFullScreen(false)
				, m_bShowCursor(true)
				, m_bMoveCursor(true)
				, m_bIME(true)
				, m_bEscapeExit(false)
				, m_uiExit(0)
				, m_uiStyle(EStyle::DefaultWindow)
			{}
			//----------------------------------------------------------------------
			//	デストラクタ
			//----------------------------------------------------------------------
			~CWindow(void)
			{
				Close();
			}
			//----------------------------------------------------------------------
			//	初期化
			//----------------------------------------------------------------------
			b8 Create(const c16* pName, b8 bShow, b8 bFullScreen)
			{
				if( m_hWnd )
				{
					return false;
				}

				IEvent Event;
				Thread::Create(Event.GetReference(), true);
				{
					WindowInfo Info;
					Info.pThis			= this;
					Info.Event			= Event;
					Info.bFullScreen	= bFullScreen;
					Info.pName			= pName;
					Info.bShow			= bShow;
					Info.bFullScreen	= bFullScreen;

					Thread::Create(m_Event.GetReference());
					Thread::Create(m_Thread.GetReference());
					m_Thread->SetPriority(Thread::EPriority::Highest);
					m_Thread->Start(WindowMessageLoop, &Info, KB(4), "WindowThread");

					Event->Wait();
				}

				return true;
			}

			//----------------------------------------------------------------------
			//	ウィンドウ破棄
			//----------------------------------------------------------------------
			b8 Open(WindowInfo* pInfo)
			{
				m_bFullScreen = !!pInfo->bFullScreen;

				WNDCLASSEX wcex;
				//	ウインドウクラスの設定
				wcex.cbSize			= sizeof(WNDCLASSEX);
				wcex.style			= CS_HREDRAW | CS_VREDRAW;
				wcex.lpfnWndProc	= WindowProcedure;
				wcex.cbClsExtra		= 0;
				wcex.cbWndExtra		= 0;
				wcex.hInstance		= GetModuleHandle(null);
				wcex.hCursor		= LoadCursor(null, IDC_ARROW);
				wcex.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
				wcex.lpszMenuName	= null;
				wcex.lpszClassName	= TEXT("Kisaragi Game Library");
				wcex.hIcon			= m_hIcon;
				wcex.hIconSm		= m_hIcon;

				RegisterClassExW(&wcex);

				RECT Rect;
				Rect.left	= 0;
				Rect.top	= 0;
				Rect.right	= m_iWidth;
				Rect.bottom	= m_iHeight;

				dword dwStyle = GetWindowStyle(m_bFullScreen ? EStyle::FullScreen: m_uiStyle);
				dword dwStyleEx = GetWindowStyleEx(m_uiStyle);
				AdjustWindowRectEx(&Rect, dwStyle, false, dwStyleEx);

				int iWidth	= Rect.right - Rect.left;
				int iHeight = Rect.bottom - Rect.top;
				if( m_iX == -1 && m_iY == -1 )
				{
					// ウィンドウ位置を画面中央にする
					m_iX = Max((GetSystemMetrics(SM_CXFULLSCREEN) - iWidth) / 2, 0);
					m_iY = Max((GetSystemMetrics(SM_CYFULLSCREEN) - iHeight) / 2, 0);
				}

				//	ウインドウの生成
				m_hWnd = CreateWindowExW(	dwStyleEx,
											wcex.lpszClassName,
											pInfo->pName,
											dwStyle,
											m_iX,
											m_iY,
											iWidth,
											iHeight,
											null,
											null,
											GetModuleHandle(null),
											null);
				//	作成失敗
				if( m_hWnd == null ) return false;

				m_uiThreadId = Thread::GetThreadId();
				//	IMEの初期化(無効化)
				m_hImc = ImmGetContext(m_hWnd);
				SetIME(false);
				//	メニュー設定
				::SetMenu(m_hWnd, m_hMenu);
				//	ウインドウの表示
				Show(!!pInfo->bShow);

				pInfo->Event->SetSignal();
				return true;
			}

			//----------------------------------------------------------------------
			//	ウィンドウ破棄
			//----------------------------------------------------------------------
			void Close(void)
			{
				if( m_hImc )
				{
					ImmAssociateContext(m_hWnd, m_hImc);
					ImmReleaseContext(m_hWnd, m_hImc);
				}
				if( m_hWnd )
				{
					SendMessage(m_hWnd, WM_CLOSE, 0, 0);
				}
				if( m_Thread.IsValid() )
				{
					m_Thread->Exit();
					m_Thread = null;
					m_hWnd = null;
					m_hImc = null;
				}
			}

			//----------------------------------------------------------------------
			//	ウィンドウハンドルの取得
			//----------------------------------------------------------------------
			void* GetHandle(void)
			{
				return m_hWnd;
			}

			//----------------------------------------------------------------------
			//	位置設定
			//----------------------------------------------------------------------
			void SetPosition(s32 iX, s32 iY)
			{
				m_iX = iX;
				m_iY = iY;
				SetWindowPosition();
			}

			//----------------------------------------------------------------------
			//	位置取得
			//----------------------------------------------------------------------
			void GetPosition(s32& iX, s32& iY)
			{
				RECT Rect;
				GetWindowRect(m_hWnd, &Rect);
				iX = Rect.left;
				iY = Rect.top;
			}

			//----------------------------------------------------------------------
			//	サイズ設定
			//----------------------------------------------------------------------
			void SetSize(s32 iWidth, s32 iHeight)
			{
				m_iWidth = iWidth;
				m_iHeight = iHeight;

				if( m_hWnd )
				{
					RECT DispRect;
					GetWindowRect(m_hWnd, &DispRect);
					m_iX = DispRect.left;
					m_iY = DispRect.top;
					SetWindowPosition();
				}
			}

			//----------------------------------------------------------------------
			//	サイズ取得
			//----------------------------------------------------------------------
			void GetSize(s32& iWidth, s32& iHeight)
			{
				iWidth = m_iWidth;
				iHeight = m_iHeight;
			}
			//----------------------------------------------------------------------
			//	サイズ取得
			//----------------------------------------------------------------------
			Vector2 GetSize(void)
			{
				return Vector2((f32)m_iWidth, (f32)m_iHeight);
			}

			//----------------------------------------------------------------------
			//	サイズ取得
			//----------------------------------------------------------------------
			void GetDisplaySize(s32& iWidth, s32& iHeight)
			{
				iWidth = GetSystemMetrics(SM_CXFULLSCREEN);
				iHeight = GetSystemMetrics(SM_CYFULLSCREEN);
			}
			
			//----------------------------------------------------------------------
			//	ウィンドウ名設定
			//----------------------------------------------------------------------
			void SetName(const c16* pName)
			{
				if( m_hWnd )
				{
					SetWindowText(m_hWnd, pName);
				}
			}

			//----------------------------------------------------------------------
			//	IMEを有効にするか？
			//----------------------------------------------------------------------
			void SetIME(b8 bEnable)
			{
				if( !m_bIME == bEnable )
				{
					if( m_uiThreadId == Thread::GetThreadId() )
					{
						ImmAssociateContext(m_hWnd, bEnable? m_hImc: null);
					}
					else
					{
						SendMessage(m_hWnd, ECustomMsg::EnableIME, bEnable, 0);
					}
					m_bIME = !!bEnable;
				}
			}

			//----------------------------------------------------------------------
			//	Espaceキーによる終了を有効にするか？
			//----------------------------------------------------------------------
			void SetEscapeExit(b8 bEnable)
			{
				m_bEscapeExit = !!bEnable;
			}
			//----------------------------------------------------------------------
			//	ウィンドウスタイルの設定
			//----------------------------------------------------------------------
			void SetStyle(u32 uiStyle)
			{
				m_uiStyle = uiStyle;

				if( m_hWnd )
				{
					SetWindowLong(m_hWnd, GWL_STYLE, GetWindowStyle(m_uiStyle));
					SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowStyleEx(m_uiStyle));
					SetWindowPosition();
				}
			}

			//----------------------------------------------------------------------
			//	ウィンドウスタイルの取得
			//----------------------------------------------------------------------
			u32 GetStyle(void)
			{
				return m_uiStyle;
			}

			//----------------------------------------------------------------------
			//	アイコンの設定
			//----------------------------------------------------------------------
			void SetIcon(void* pHandle)
			{
				m_hIcon = (HICON)pHandle;
				if( m_hWnd )
				{
					SendMessage(m_hWnd, WM_SETICON, ICON_BIG,   (LPARAM)m_hIcon);
					SendMessage(m_hWnd, WM_SETICON, ICON_SMALL, (LPARAM)m_hIcon);
				}
			}

			//----------------------------------------------------------------------
			//	メニューの設定
			//----------------------------------------------------------------------
			void SetMenu(void* pHandle)
			{
				m_hMenu = (HMENU)pHandle;
				if( m_hWnd )
				{
					::SetMenu(m_hWnd, m_hMenu);
				}
			}

			//----------------------------------------------------------------------
			//!	アイコンハンドルの取得
			//----------------------------------------------------------------------
			void* GetIconHandle(void)
			{
				return m_hIcon;
			}
			
			//----------------------------------------------------------------------
			//	メニューハンドルの取得
			//----------------------------------------------------------------------
			void* GetMenuHandle(void)
			{
				return m_hMenu;
			}
			
			//----------------------------------------------------------------------
			//	カーソルを消すか？
			//----------------------------------------------------------------------
			void ShowCursor(b8 bShow, b8 bMove)
			{
				s32 iParam = 0;
				iParam |= bShow? (1 << 0): 0;
				iParam |= bMove? (1 << 1): 0;

				PostMessage(m_hWnd, ECustomMsg::ShowCursor, iParam, 0);
			}

		public:
			//----------------------------------------------------------------------
			//	ウィンドウにフォーカスを当てる
			//----------------------------------------------------------------------
			void Forcus(void)
			{
				SetForegroundWindow(m_hWnd);
			}
			//----------------------------------------------------------------------
			//	ウィンドウの表示制御
			//----------------------------------------------------------------------
			void Show(b8 bShow)
			{
				s32 iShow = SW_SHOWDEFAULT;
				iShow = !m_bFullScreen? iShow: SW_MAXIMIZE;
				iShow = bShow ? iShow: SW_HIDE;
				if( m_iShow != iShow )
				{
					ShowWindow(m_hWnd, iShow);
					UpdateWindow(m_hWnd);
				}
			}
			//----------------------------------------------------------------------
			//	ウィンドウモード変更
			//----------------------------------------------------------------------
			void ChangeWindowMode(void)
			{
				if( !m_hWnd )
				{
					return;
				}
				SendMessage(m_hWnd, ECustomMsg::ChangeWindowMode, 0, 0);
				m_Event->Wait();
			}

			//----------------------------------------------------------------------
			//	フルスクリーンフラグの取得
			//----------------------------------------------------------------------
			b8 IsFullScreen(void) { return !!m_bFullScreen; }
			//----------------------------------------------------------------------
			//	フォーカスフラグの取得
			//----------------------------------------------------------------------
			b8 IsForcus(void) { return m_hWnd == GetForegroundWindow(); }
			//----------------------------------------------------------------------
			//	ウィンドウが生成さているか？
			//----------------------------------------------------------------------
			b8 IsActive(void) { return m_hWnd != null; }
			//----------------------------------------------------------------------
			//	終了コードの取得
			//----------------------------------------------------------------------
			u32 ExitCode(void) { return m_uiExit; }

		public:
			//----------------------------------------------------------------------
			//	オーナーウィンドウの設定(オーナーの裏に回る事が不可)
			//----------------------------------------------------------------------
			void SetOwner(IWindow* pOwner)
			{
				SetWindowLongPtr(m_hWnd, GWLP_HWNDPARENT, (LONG_PTR)pOwner->GetHandle());
			}

			//----------------------------------------------------------------------
			//	親ウィンドウの設定(子は親のウィンドウ外へ移動不可)
			//----------------------------------------------------------------------
			void SetParent(IWindow* pParent)
			{
				::SetParent(m_hWnd, (HWND)pParent->GetHandle());
			}

			//----------------------------------------------------------------------
			//	ウィンドウイベントの設定
			//----------------------------------------------------------------------
			void SetWindowEvent(IWindowEvent* pEvent)
			{
				m_Events.push_back(pEvent);
			}
			//----------------------------------------------------------------------
			//	ウィンドウイベントのクリア
			//----------------------------------------------------------------------
			void ClearWindowEvent(IWindowEvent* pEvent)
			{
				if( pEvent == null )
				{
					m_Events.clear();
				}
				for( u32 i=0; i < m_Events.size(); i++ )
				{
					if( m_Events[i] == pEvent )
					{
						m_Events.erase(i);
						break;
					}
				}
			}

		private:
			//----------------------------------------------------------------------
			//	ウィンドウ位置のセット
			//----------------------------------------------------------------------
			void SetWindowPosition(void)
			{
				if( !m_hWnd )
				{
					return;
				}

				if( m_iX == -1 && m_iY == -1 )
				{
					// ウィンドウ位置を画面中央にする
					m_iX = (GetSystemMetrics(SM_CXFULLSCREEN) - m_iWidth) / 2;
					m_iY = (GetSystemMetrics(SM_CYFULLSCREEN) - m_iHeight) / 2;
				}

				RECT Rect;
				Rect.left = 0;
				Rect.top = 0;
				Rect.right = m_iWidth;
				Rect.bottom = m_iHeight;

				dword dwStyle = GetWindowStyle(m_bFullScreen ? EStyle::FullScreen : m_uiStyle);
				AdjustWindowRect(&Rect, dwStyle, false);

				SetWindowPos(m_hWnd,
					HWND_TOP,
					m_iX,
					m_iY,
					Rect.right - Rect.left,
					Rect.bottom - Rect.top,
					SWP_DRAWFRAME | SWP_SHOWWINDOW);
			}

		private:
			//----------------------------------------------------------------------
			//	ウインドウプロシージャー
			//----------------------------------------------------------------------
			static LRESULT CALLBACK WindowProcedure(HWND hWnd, UINT uiMsg, WPARAM wParam, LPARAM lParam)
			{
				switch( uiMsg )
				{
				case WM_CLOSE:
					DestroyWindow(hWnd);
					return 0;
				case WM_DESTROY:
					//	ウインドウの破棄
					PostQuitMessage(0);
					return 0;
				}
				CWindow* pWindow = null;
				{
					algorithm::string strName = "kgWindow";
					strName += (u32)GetCurrentThreadId();
					HANDLE hProp = GetPropA(hWnd, strName.c_str());
					if (hProp != null)
					{
						pWindow = (CWindow*)hProp;
					}
				}
				bool bDefault = true;
				if( pWindow )
				{
					bDefault = pWindow->WindowMessageProc(uiMsg, wParam, lParam);
				}
				return bDefault? DefWindowProc(hWnd, uiMsg, wParam, lParam): 0;
			}
			//----------------------------------------------------------------------
			//!	ウィンドウメッセージ処理用
			//----------------------------------------------------------------------
			static u32 THREADFUNC WindowMessageLoop(void* pArg)
			{
				TKGLPtr<CWindow> pWindow;
				{
					WindowInfo* pInfo = (WindowInfo*)pArg;
					pWindow = pInfo->pThis;
					pWindow->Open(pInfo);

					algorithm::string strName = "kgWindow";
					strName += (u32)GetCurrentThreadId();
					SetPropA(pWindow->m_hWnd, strName.c_str(), pWindow);
					//	タイミングの都合上の問題で再度Create命令を呼び出す
					WindowProcedure(pWindow->m_hWnd, WM_CREATE, 0, 0);
				}

				MSG Msg;
				kgZeroMemory(&Msg, sizeof(Msg));
				while( Msg.message != WM_QUIT )
				{
					if( PeekMessage(&Msg, null, 0, 0, PM_REMOVE) )
					{
						TranslateMessage(&Msg);
						DispatchMessage(&Msg);
					}
					else
					{
						kgSleep(0.001f);
					}
				}
				pWindow->m_uiExit	= (u32)Msg.wParam;
				pWindow->m_hWnd		= null;

				return (u32)Msg.wParam;
			}
			//----------------------------------------------------------------------
			//!	ウィンドウメッセージ処理用
			//----------------------------------------------------------------------
			bool WindowMessageProc(UINT uiMsg, WPARAM& wParam, LPARAM& lParam)
			{
				bool ret = true;
				s32 iEvent, iMsg, iParam1, iParam2;
				iEvent = iMsg = iParam1 = iParam2 = -1;
				switch( uiMsg )
				{
				case WM_CREATE:
					iEvent = EEvent::Create;
					break;
				case WM_QUIT:
					iEvent = EEvent::Quit;
					break;

				case ECustomMsg::ShowCursor:
				{
					b8 bShow = !!(wParam & (1 << 0));
					if( !!bShow != !!m_bShowCursor )
					{
						::ShowCursor(bShow);
						m_bShowCursor = !!bShow;
						m_bMoveCursor = !!(wParam & (1 << 1));
					}
				}
				break;
				case ECustomMsg::DeviceLost:
				{
					std::function<void(void)> func = *(std::function<void(void)>*)wParam;
					func();
				}
				break;
				case ECustomMsg::EnableIME:
					ImmAssociateContext(m_hWnd, wParam? m_hImc: null);
					break;
				case ECustomMsg::ChangeWindowMode:
				{
					m_bFullScreen = !m_bFullScreen;
					/*
					u32 uiStyle;
					if( m_bFullScreen )
					{
						//	フルスクリーンに変更する
						::SetMenu(m_hWnd, null);
						RECT DispRect;
						GetWindowRect(m_hWnd, &DispRect);
						m_iX = DispRect.left;
						m_iY = DispRect.top;

						uiStyle = EStyle::FullScreen;
					}
					else
					{
						//	ウインドウ画面に変更する
						::SetMenu(m_hWnd, m_hMenu);
						uiStyle = m_uiStyle;
					}
					dword dwStyle = GetWindowStyle(uiStyle);
					dword dwStyleEx = GetWindowStyleEx(uiStyle);
					SetWindowLong(m_hWnd, GWL_STYLE, dwStyle);
					SetWindowLong(m_hWnd, GWL_EXSTYLE, dwStyleEx);

					RECT Rect;
					Rect.left = 0;
					Rect.top = 0;
					Rect.right = m_iWidth;
					Rect.bottom = m_iHeight;
					// クライアント領域を含めたサイズを取得する
					AdjustWindowRectEx(&Rect, dwStyle, false, dwStyleEx);

					int height = Rect.right - Rect.left;
					int width = Rect.bottom - Rect.top;
					// ウィンドウ位置を画面中央にする
					int x = m_iX;
					int y = m_iY;

					SetWindowPos(m_hWnd,
						HWND_TOP,
						x, y, height, width,
						SWP_DRAWFRAME | SWP_SHOWWINDOW);

					ShowWindow(m_hWnd, m_bFullScreen? SW_MAXIMIZE: SW_SHOWDEFAULT);
					UpdateWindow(m_hWnd);

					SetFocus(m_hWnd);
					*/
					m_Event->SetSignal();
				}
				break;

				case WM_IME_SETCONTEXT:
					lParam = 0;
				case WM_IME_STARTCOMPOSITION:
				case WM_IME_ENDCOMPOSITION:
				case WM_IME_COMPOSITION:
					ret = false;
					break;

				case WM_KEYDOWN:
					switch( wParam )
					{
					case VK_ESCAPE:
						if( m_bEscapeExit )
						{
							SendMessage(m_hWnd, WM_CLOSE, 0, 0);
						}
						break;
					case VK_NUMLOCK:
						break;
					}
					break;

				}

				if( uiMsg != WM_NULL )
				{
					iMsg = (s32)uiMsg;
					iParam1 = (s32)wParam;
					iParam2 = (s32)lParam;
				}
				if( !m_bShowCursor && !m_bMoveCursor )
				{
					s32 iX, iY, iWidth, iHeight;
					GetPosition(iX, iY);
					GetSize(iWidth, iHeight);
					SetCursorPos(iX + iWidth / 2, iY + iHeight / 2);
				}
				for( u32 i=0; i < m_Events.size(); i++ )
				{
					auto Event = m_Events[i];
					if( iEvent != -1 )
					{
						FWindowEvent Func = Event->GetEventCallBack(iEvent);
						if( Func ) Func(this, Event, iParam1);
					}
					else if (iMsg != -1)
					{
						FWindowCommonEvent Func = Event->GetCommonEventCallBack();
						if( Func ) Func(this, Event, iMsg, iParam1, iParam2);
					}
				}
				return ret;
			}

		private:
			HWND		m_hWnd;				//!< ウィンドウハンドル
			HIMC		m_hImc;				//!< IMEコンテキスト
			HICON		m_hIcon;			//!< アイコンハンドル
			HMENU		m_hMenu;			//!< メニューハンドル
			s32			m_iX;				//!< X座標
			s32			m_iY;				//!< Y座標
			s32			m_iWidth;			//!< 横幅
			s32			m_iHeight;			//!< 縦幅
			s32			m_iShow;			//!< 表示モード
			u32			m_uiStyle;			//!< ウィンドウスタイル
			u32			m_uiExit;			//!< 終了コード
			u32			m_uiThreadId;		//!< スレッドID
			BITFIELD	m_bFullScreen: 1;	//!< フルスクリーンか？
			BITFIELD	m_bForcus: 1;		//!< フォーカスされているか？
			BITFIELD	m_bShowCursor: 1;	//!< カーソルが表示されているか？
			BITFIELD	m_bMoveCursor: 1;	//!< カーソルの移動が許可されているか？
			BITFIELD	m_bIME: 1;			//!< IMEが有効か？
			BITFIELD	m_bEscapeExit: 1;	//!< エスケープキーによる終了が有効か？
			IThread		m_Thread;			//!< メッセージ受付スレッド用
			IEvent		m_Event;			//!< メッセージ制御用

			algorithm::vector<TKGLPtr<IWindowEvent>>	m_Events;	//!< ウィンドウイベント
		};

		//----------------------------------------------------------------------
		// ウィンドウの生成
		//----------------------------------------------------------------------
		b8 Create(IWindow** ppWindow)
		{
			*ppWindow = new CWindow;
			(*ppWindow)->AddRef();

			return true;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
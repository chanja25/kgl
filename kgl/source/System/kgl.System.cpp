//----------------------------------------------------------------------
//!
//!	@file	kgl.System.cpp
//!	@brief	Kisaragi Game Library
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "System/kgl.System.h"
#include "Data/xml/kgl.xml.h"
#include <functional>

//	Platform Dependent
#if	WINDOWS
#include <DirectXMath.h>
#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "winmm.lib")

#include <Render/Utility/kgl.Image.h>
#endif	// ~#if WINDOWS

//	ZLIB
#pragma comment(lib, "zlib.lib")
//	OGG
#pragma comment(lib, "libvorbis_static.lib")
#pragma comment(lib, "libogg_static.lib")
#pragma comment(lib, "libvorbisfile_static.lib")


namespace kgl
{
#if	KGL_DEBUG_TRACE
	namespace Debug
	{
		extern void CreateConsole(void);
	}
#endif	// ~#if KGL_DEBUG_TRACE

	namespace Input
	{
		//	初期化
		extern b8 Initialize(HWND hWnd);
		//	破棄
		extern void Finalize(void);
		//	更新
		extern void Update(void);
	}

	namespace Manager
	{
		//	ゲーム管理の生成
		extern b8 CreateGameManager(Window::IWindow* pWindow, File::IFileReader* pFileReader, IGraphicDriver* pGraphicDriver, Frame::IFrameControl* pFrameControl);
		//	ゲーム管理の解放
		extern void DeleteGameManager(void);
	}

	namespace System
	{
		namespace EInitialize
		{
			const s32 SUCCESS		= 0;	// 成功
			const s32 FAILED		= 1;	// 失敗
			const s32 NOT_INIT		= 2;	// 初期化されていない
			const s32 APP_FAILED	= 3;	// アプリ側のエラー
		}

#if	KGL_DEBUG_MENU
		//----------------------------------------------------------------------
		//	描画スキップモードの変更
		//----------------------------------------------------------------------
		void DrawSkipModeChange(const Debug::DebugMenuValue* pValue, void* pArg)
		{
			ISystem* pSystem = (ISystem*)pArg;
			pSystem->FrameControl()->SetSkip(!!pValue->bValue);
		}
		//----------------------------------------------------------------------
		//	トレースモードの変更
		//----------------------------------------------------------------------
		void TraceModeChange(const Debug::DebugMenuValue* pValue, void* pArg)
		{
			(void)pArg;
			b8 bEnable = !!pValue->bValue;
			EnableSystemTrace(bEnable);
			EnableErrorTrace(bEnable);
			EnableWarningTrace(bEnable);
			EnableTrace(bEnable);
		}
		//----------------------------------------------------------------------
		//	VSyncモードの変更
		//----------------------------------------------------------------------
		void VSyncModeChange(const Debug::DebugMenuValue* pValue, void* pArg)
		{
			b8 bEnable = !!pValue->bValue;

			ISystem* pSystem = (ISystem*)pArg;
			auto pRenderSystem = pSystem->RenderingSystem();
			auto pDevice = pRenderSystem->GraphicDriver()->GraphicDevice();

			pDevice->SetVSync(bEnable);
			Config::EnableVsync = bEnable;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		ISystem::ISystem(void)
			: m_iState(EInitialize::NOT_INIT)
			, m_iExitCode(0)
		{}
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 ISystem::InitializeKGL(Window::IWindow* pWindow, const c8* pPackagePath, const c8* pRootDir)
		{
			if( m_iState == EInitialize::SUCCESS )
			{
				WarningTrace("既にKGLシステムは初期化されています(InitializeKGL)");
				return false;
			}
			if( pWindow == null )
			{
				WarningTrace("ウィンドウがnullです(InitializeKGL)");
				return false;
			}
#if WINDOWS && USE_SIMD
			if( !DirectX::XMVerifyCPUSupport() )
			{
				WarningTrace("SIMDに対応したCPUが必要です(InitializeKGL)");
				return false;
			}
#endif // WINDOWS && USE_SIMD

			TKGLPtr<Xml::IXml> pXmlConfig;
			Xml::Create(pXmlConfig.GetReference());
			//	設定情報の読み込み
			if( !pXmlConfig->Create("Config/Config.xml") )
			{
				pXmlConfig->Create("Config");
			}
			Xml::CNode* pConfig = pXmlConfig->GetRoot();

			m_pFrameControl = null;
			m_pWindow = null;
			m_pRenderingSystem = null;
			m_bComInit = false;

			SystemTrace("//==================================================");
			SystemTrace("//	KGL Initialize");
			SystemTrace("//==================================================");

			m_iState = EInitialize::FAILED;

			//	COMの初期化
			if( FAILED(CoInitializeEx(null, COINIT_MULTITHREADED)) )
			{
				return false;
			}
			m_bComInit = true;
#if	KGL_DEBUG
			Xml::CNode* pDebug = pConfig->FindChildNode("Debug");
			//	デバッグ用コンソール作成
			if( pDebug )
			{
#if	KGL_DEBUG_TRACE
				auto pConsol = pDebug->FindChildNode("Consol");
				auto pTrace  = pDebug->FindChildNode("Trace");
				//	デバッグ関連の設定
				if( pConsol && pConsol->GetContent() == "true" )
				{
					Debug::CreateConsole();
					pWindow->Forcus();
				}
				//	トレースをオフにする(エラー、警告を除く)
				if( pTrace && pTrace->GetContent() == "false" )
				{
					EnableSystemTrace(false);
					EnableTrace(false);
				}
#endif	// ~#if KGL_DEBUG_TRACE
			}
#endif	// ~#if	KGL_DEBUG

			Xml::CNode* pFrameWork = pConfig->FindChildNode("FrameWork");
			//	フレームワーク関連の設定
			if( pFrameWork )
			{
				auto pFPSFix	= pFrameWork->FindChildNode("FPSFix");
				auto pFPS		= pFrameWork->FindChildNode("FPS");

				Config::FPSFix		= pFPSFix && pFPSFix->GetContent() == "true";
				Config::TargetFPS	= pFPS? atoi(pFPS->GetContent().c_str()): 60;
			}

			Xml::CNode* pGraphics = pConfig->FindChildNode("Graphics");
			algorithm::string strDevice = "DirectX9";
			//	グラフィック関連の設定
			if( pGraphics )
			{
				auto pWidth  = pGraphics->FindChildNode("Width");
				auto pHeight = pGraphics->FindChildNode("Height");
				s32 iWidth	= pWidth?  atoi(pWidth->GetContent().c_str()) : 1280;
				s32 iHeight	= pHeight? atoi(pHeight->GetContent().c_str()): 720;
				pWindow->SetSize(iWidth, iHeight);

				s32 iDisplayWidth, iDisplayHeight;
				pWindow->GetDisplaySize(iDisplayWidth, iDisplayHeight);

				auto pX = pGraphics->FindChildNode("X");
				auto pY = pGraphics->FindChildNode("Y");
				s32 iX = Max(pX? atoi(pX->GetContent().c_str()): (iDisplayWidth  - iWidth)  / 2, 0);
				s32 iY = Max(pY? atoi(pY->GetContent().c_str()): (iDisplayHeight - iHeight) / 2, 0);
				pWindow->SetPosition(iX, iY);

				auto pFullScreen = pGraphics->FindChildNode("FullScreen");
				b8 bIsFullScreen = pFullScreen && pFullScreen->GetContent() == "true";
				if (pWindow->IsFullScreen() != bIsFullScreen)
				{
					pWindow->ChangeWindowMode();
				}

				auto pVSync  = pGraphics->FindChildNode("VSync");
				auto pDevice = pGraphics->FindChildNode("Device");

				Config::EnableVsync = pVSync && pVSync->GetContent() == "true";
				strDevice = pDevice? pDevice->GetContent(): "DirextX11";
			}

			pWindow->GetSize(Config::ResolutionWidth, Config::ResolutionHeight);
			Config::Resolution.x = (f32)Config::ResolutionWidth;
			Config::Resolution.y = (f32)Config::ResolutionHeight;
			Config::InvResolution.x = 1.0f / Config::Resolution.x;
			Config::InvResolution.y = 1.0f / Config::Resolution.y;

			//	Platform Dependent
			//	グラフィックデバイスの初期化
			TKGLPtr<IGraphicDriver> pDriver;
#if	USE_DIRECTX11
			if( strDevice == "DirectX11" )
			{
				if( !CreateDX11Driver(pDriver.GetReference()) )
				{
					strDevice = "DirectX9";
				}
			}
#endif	// ~#if	USE_DIRECTX11
#if	USE_DIRECTX9
			if( strDevice == "DirectX9" )
			{
				CreateDX9Driver(pDriver.GetReference());
			}
#endif	// ~#if	USE_DIRECTX9
#if USE_OPENGL
			if( strDevice == "OpenGL" )
			{
				CreateOpenGLDriver(pDriver.GetReference());
			}
#endif	// ~#if	USE_OPENGL
			if( pDriver == null )
			{
				return false;
			}
			//	グラフィックデバイスの初期化
			GRAPHICS_INFO DeviceInfo;
			DeviceInfo.bFullScreen	= pWindow->IsFullScreen();
			DeviceInfo.bUseVSync	= !!Config::EnableVsync;
			DeviceInfo.pWindow		= pWindow;
			DeviceInfo.iWidth		= Config::ResolutionWidth;
			DeviceInfo.iHeight		= Config::ResolutionHeight;
			if( !pDriver->Initialize(DeviceInfo) )
			{
				return false;
			}

			TKGLPtr<File::IFileReader> pFileReader;
			//	ファイル管理の生成
			if( !File::Create(pFileReader.GetReference(), pPackagePath, pRootDir) )
			{
				return false;
			}

			switch( pDriver->GetType() )
			{
			case kgGraphics::EDriverType::DirectX9:
				pFileReader->BindPath("Data/Shaders", "Data/Shaders/SM3");
				break;
			case kgGraphics::EDriverType::DirectX11:
				pFileReader->BindPath("Data/Shaders", "Data/Shaders/SM5");
				break;
			case kgGraphics::EDriverType::OpenGL:
				pFileReader->BindPath("Data/Shaders", "Data/Shaders/GL");
				break;
			}
			TKGLPtr<Frame::IFrameControl> pFrameControl;
			//	フレーム制御クラス生成
			if( !Frame::Create(pFrameControl.GetReference()) )
			{
				return false;
			}
			pFrameControl->SetTargetFPS(Config::TargetFPS);

			//	ゲーム管理の生成
			if( !Manager::CreateGameManager(pWindow, pFileReader, pDriver, pFrameControl) )
			{
				return false;
			}
			//	描画システムを生成
			TKGLPtr<Render::IRenderingSystem> pRenderingSystem;
			if( !Render::Create(pRenderingSystem.GetReference(), pDriver) )
			{
				return false;
			}
			//	描画用レンダーバッファの生成
			{
				auto pManager = pRenderingSystem->GraphicDriver()->RenderTargetManager();
				TKGLPtr<kgGraphics::IRenderTargetBase> pTarget;
				TKGLPtr<kgGraphics::ITextureBase> pResource;

				kgGraphics::RenderTargetInfo TargetInfo;
				TargetInfo.iFormat				= kgGraphics::ETextureFormat::A8R8G8B8;
				TargetInfo.iUsage				= kgGraphics::ETextureUsage::RenderTarget;
				TargetInfo.uiWidth				= Config::ResolutionWidth;
				TargetInfo.uiHeight				= Config::ResolutionHeight;
				TargetInfo.SampleDesc.iCount	= 1;
				TargetInfo.SampleDesc.iQuality	= 0;

				if( !pManager->CreateRenderTarget(pTarget.GetReference(), TargetInfo) )
				{
					return false;
				}
				pTarget->GetRenderTarget(pResource.GetReference());
				if( !Render::Create(m_pFrameBuffer.GetReference(), pResource) )
				{
					return false;
				}
				pTarget = null;
				pResource = null;
				TargetInfo.iFormat	= kgGraphics::ETextureFormat::D24S8;
				TargetInfo.iUsage	= kgGraphics::ETextureUsage::Depth;
				if( !pManager->CreateRenderTarget(pTarget.GetReference(), TargetInfo) )
				{
					return false;
				}
				pTarget->GetRenderTarget(pResource.GetReference());
				if( !Render::Create(m_pDepthBuffer.GetReference(), pResource) )
				{
					return false;
				}

				s32 iFilter = Command::ETextureFilter::Nearest;
				m_pFrameBuffer->SetFilter(iFilter, iFilter, iFilter);
			}
			//	入力初期化
			if( !Input::Initialize((HWND)pWindow->GetHandle()) )
			{
				return false;
			}
#if	KGL_DEBUG_PROFILE
			//	プロファイラーの初期化
			if( !Profiler::CProfiler::InitializeInstance() )
			{
				return false;
			}
#endif	// ~#if KGL_DEBUG_PROFILE

			m_iState = EInitialize::APP_FAILED;
			//	初期化
			if( !Initialize() )
			{
				return false;
			}

			m_iState = EInitialize::SUCCESS;

			m_pWindow = pWindow;
			m_pRenderingSystem = pRenderingSystem;
			m_pFrameControl = pFrameControl;

#if	KGL_DEBUG_MENU
			//	初期値の設定
			KGLDebugMenu()->SetValue(Config::TargetFPS, "TargetFPS", "System.Debug");
			KGLDebugMenu()->SetValueBool(Config::EnableVsync, "VSync", "System.Debug");

			//	コールバックの設定
			KGLDebugMenu()->SetChangeCallback(DrawSkipModeChange, this, "DrawSkip", "System.Debug");
			KGLDebugMenu()->SetChangeCallback(TraceModeChange, this, "Trace", "System.Debug");
			KGLDebugMenu()->SetChangeCallback(VSyncModeChange, this, "VSync", "System.Debug");
#endif	// ~#if	KGL_DEBUG_MENU

			return true;
		}

		//----------------------------------------------------------------------
		// 破棄
		//----------------------------------------------------------------------
		void ISystem::FinalizeKGL(void)
		{
			if( m_iState == EInitialize::NOT_INIT )
			{
				return;
			}

			SystemTrace("//==================================================");
			SystemTrace("//	KGL Finalize");
			SystemTrace("//==================================================");

			//	ウィンドウ解放
			m_pWindow = null;
			//	破棄
			Finalize();

			//	フレーム制御クラス解放
			m_pFrameControl = null;
#if	KGL_DEBUG_PROFILE
			//	プロファイラの破棄
			Profiler::CProfiler::FinalizeInstance();
#endif	// ~#if PROFILE_DEBUGa
			//	入力破棄
			Input::Finalize();
			//	描画管理の解放
			if( m_pRenderingSystem.IsValid() )
			{
				m_pRenderingSystem->GraphicDriver()->GraphicDevice()->WaitForCommand();
				m_pRenderingSystem = null;
			}
			//	ゲーム管理の解放
			Manager::DeleteGameManager();
			//	COMの終了処理
			if( m_bComInit )
			{
				CoUninitialize();
			}
			m_iState = EInitialize::NOT_INIT;
		}

		//----------------------------------------------------------------------
		//	実行
		//----------------------------------------------------------------------
		void ISystem::Run(void)
		{
			//	初期化の確認
			switch( m_iState )
			{
			case EInitialize::SUCCESS:
				break;
			case EInitialize::NOT_INIT:
				WarningTrace("ライブラリーが初期化されていません");
				return;
			case EInitialize::FAILED:
				WarningTrace("ライブラリーの初期化に失敗しています");
				return;
			case EInitialize::APP_FAILED:
				WarningTrace("アプリケーションの初期化に失敗しています");
				return;
			}

			SystemTrace("//==================================================");
			SystemTrace("//	KGL Running");
			SystemTrace("//==================================================");

			TKGLPtr<IGraphicDevice> pDevice = m_pRenderingSystem->GraphicDriver()->GraphicDevice();

			while( !Config::ExitRequest )
			{
				//	ウィンドウが閉じていれば終了する
				if( !m_pWindow->IsActive() )
				{
					break;
				}

				//	フレーム更新
				if( !m_pFrameControl->Update() )
				{
					f32 fSleep	= m_pFrameControl->GetTargetFrameTime() - m_pFrameControl->GetElapsedTime();
					if( fSleep > 0.0f )
					{
						//	フレームに余裕がある場合は次の更新までスリープする
						kgSleep(fSleep > 0.001f? Min(fSleep - 0.001f, 1.0f): 0.0f);
					}
					continue;
				}

#if	KGL_DEBUG_PROFILE
				//	プロファイリング開始
				PROFILER->StartFrame();
#endif	// ~#if KGL_DEBUG_PROFILE
				{
					SCOPE_PROFILE(Profiler::EProfile_SystemUpdate);
					{
						SCOPE_PROFILE(Profiler::EProfile_SystemInput);
						//	入力更新
						Input::Update();
					}
					{
						//	オーディオ更新
						KGLAudioManager()->StreamManager()->Update();
					}

					f32 fElapsedTime = m_pFrameControl->GetElapsedTime();
					if( Config::FPSFix )
					{
						fElapsedTime = 1.0f / (f32)m_pFrameControl->GetTargetFPS();
					}
#if	KGL_DEBUG_MENU
					//	処理速度変更
					s32 iTargetFPS = KGLDebugMenu()->GetValue("TargetFPS", "System.Debug");
					if( iTargetFPS != m_pFrameControl->GetTargetFPS() )
					{
						Config::TargetFPS = iTargetFPS;
						m_pFrameControl->SetTargetFPS(iTargetFPS);
					}
					//	処理速度変更
					fElapsedTime *= KGLDebugMenu()->GetValueF32("RunSpeed", "System.Debug");

					if( !KGLDebugMenu()->GetValueBool("Pause", "System.Debug") )
#endif	// ~#if	KGL_DEBUG_MENU
					{
						//	更新
						Update(fElapsedTime);
					}
#if	KGL_DEBUG_PROFILE
					//	FPSの更新
					KGLGameManager()->FPSCounter()->Update(m_pFrameControl->GetElapsedTime());
#endif	// ~#if	KGL_DEBUG_PROFILE

#if	KGL_DEBUG_MENU
					KGLDebugMenu()->Update();
#endif	// ~#if	KGL_DEBUG_MENU

				}

#define	USE_FRAME_BUFFER	1
				//	描画フレーム更新
				if( m_pFrameControl->UpdateRender() )
				{
					BEGIN_PROFILE(Profiler::EProfile_SystemRendering);
#if	KGL_DEBUG_PROFILE
					//	FPSの更新
					KGLGameManager()->RenderFPSCounter()->Update(m_pFrameControl->GetRenderElapsedTime());
#endif	// ~#if	KGL_DEBUG_PROFILE
					pDevice->BeginScene();

					TKGLPtr<ICommandCreator> pCommandCreator = m_pRenderingSystem->CommandCreator();
					pCommandCreator->Begin();
					{
#if USE_FRAME_BUFFER
						TKGLPtr<kgGraphics::ISurfaceBase> pSurface;
						m_pFrameBuffer->GetResource()->GetSurface(pSurface.GetReference());
						Command::RenderTargetInfo TargetInfo;
						TargetInfo.iIndex = 0;
						TargetInfo.pTarget = pSurface;
						pCommandCreator->SetRenderTarget(TargetInfo);

						pSurface = null;
						m_pDepthBuffer->GetResource()->GetSurface(pSurface.GetReference());
						Command::DepthTargetInfo DepthTargetInfo;
						DepthTargetInfo.pDepthTarget = pSurface;
						pCommandCreator->SetDepthTarget(DepthTargetInfo);
#else
						Command::RenderTargetInfo TargetInfo;
						TargetInfo.iIndex = 0;
						TargetInfo.pTarget = null;
						pCommandCreator->SetRenderTarget(TargetInfo);

						TKGLPtr<kgGraphics::ISurfaceBase> pSurface;
						m_pFrameBuffer->GetResource()->GetSurface(pSurface.GetReference());
						TargetInfo.iIndex = 1;
						TargetInfo.pTarget = pSurface;
						pCommandCreator->SetRenderTarget(TargetInfo);
#endif // ~#if USE_FRAME_BUFFER
						Command::ViewPortInfo ViewPortInfo;
						ViewPortInfo.iX			= 0;
						ViewPortInfo.iY			= 0;
						ViewPortInfo.iWidth		= kgl::Config::ResolutionWidth;
						ViewPortInfo.iHeight	= kgl::Config::ResolutionHeight;
						ViewPortInfo.fMinZ		= 0.0f;
						ViewPortInfo.fMaxZ		= 1.0f;

						pCommandCreator->SetViewPort(ViewPortInfo);

						Command::ClearTargetInfo ClearInfo;
						ClearInfo.iTarget	= Command::EClearTarget::Color|Command::EClearTarget::Depth|Command::EClearTarget::Stencil;
						ClearInfo.iStencil	= 0;
						ClearInfo.uiColor	= 0x00ff0000;
						ClearInfo.fDepth	= 1.0f;
						pCommandCreator->ClearTarget(ClearInfo);
					}
					//	描画
					Render(m_pRenderingSystem);
#if	KGL_DEBUG_FONT
					//	デバッグ描画
					DebugRender(m_pRenderingSystem);
#endif	// ~#if	KGL_DEBUG_FONT

#if USE_FRAME_BUFFER
					//	フレームの描画
					{
						Command::RenderTargetInfo TargetInfo;
						TargetInfo.iIndex = 0;
						TargetInfo.pTarget = null;
						pCommandCreator->SetRenderTarget(TargetInfo);

						Command::DepthTargetInfo DepthTargetInfo;
						DepthTargetInfo.pDepthTarget = null;
						pCommandCreator->SetDepthTarget(DepthTargetInfo);

						auto& Info = m_pFrameBuffer->GetInfo();
						Vector2 vFrameSize((f32)Info.uiWidth, (f32)Info.uiHeight);
						Vector2 vDisplaySize(vFrameSize);
						FColor Bright(FColorWhite * 1.f);
						TKGLPtr<Render::IDrawer> pDrawer = m_pRenderingSystem->Drawer();
						pDrawer->DrawTexture(m_pFrameBuffer, ZeroVector2, vDisplaySize, ZeroVector2, vFrameSize, Bright, Render::EDrawMode::Clear);
					}
#endif // ~#if USE_FRAME_BUFFER

					pCommandCreator->End();
					pDevice->WaitForCommand();

					END_PROFILE(Profiler::EProfile_SystemRendering);
					pDevice->EndScene();
				}
#if	KGL_DEBUG_PROFILE
				//	プロファイリング開始
				PROFILER->EndFrame();
#endif	// ~#if KGL_DEBUG_PROFILE
			}

			m_iExitCode = m_pWindow->ExitCode();
		}

#if	KGL_DEBUG
		//----------------------------------------------------------------------
		//	デバッグ描画
		//----------------------------------------------------------------------
		void ISystem::DebugRender(Render::IRenderingSystem* pRenderingSystem)
		{
			(void)pRenderingSystem;
			SCOPE_PROFILE(Profiler::EProfile_SystemDebugRendering);

#if	KGL_DEBUG_MENU && KGL_DEBUG_FONT
			TKGLPtr<Font::IFont> pDebugFont = pRenderingSystem->DebugFont();

			b8 bShowFPS			= KGLDebugMenu()->GetValueBool("ShowFPS",		"System.Show");
			b8 bShowMemory		= KGLDebugMenu()->GetValueBool("ShowMemory",	"System.Show");
			b8 bShowProfile		= KGLDebugMenu()->GetValueBool("ShowProfile",	"System.Show");
			b8 bShowBuildInfo	= KGLDebugMenu()->GetValueBool("ShowBuildInfo",	"System.Show");
			b8 bShowRunInfo		= KGLDebugMenu()->GetValueBool("ShowRunInfo",	"System.Show");
			b8 bShowScene		= KGLDebugMenu()->GetValueBool("ShowScene",		"System.Show");
			{
				const Vector2 vScale(1.0f, 1.0f);

				c8 cTemp[256];
				Vector2 vPos(8.0f, 8.0f);
				FColor DrawColor;

				std::function<void ()> DebugDrawList[] = {
					[&] ()
					{
						if( bShowFPS )
						{
							CharSet::SFormat(cTemp, "Game %2.1f FPS(%2.3f ms)", KGLGameManager()->FPSCounter()->GetFPS(), KGLGameManager()->FPSCounter()->GetElapsedTime() * 1000.0f);
						}
					},
					[&] ()
					{
						if( bShowFPS )
						{
							CharSet::SFormat(cTemp, "Game %2.1f FPS(%2.3f ms)", KGLGameManager()->FPSCounter()->GetFPS(), KGLGameManager()->FPSCounter()->GetElapsedTime() * 1000.0f);
						}
					},
					[&] ()
					{
						if( bShowFPS && m_pFrameControl->IsEnableSkip() )
						{
							CharSet::SFormat(cTemp, "Draw %2.1f FPS(%2.3f ms)", KGLGameManager()->RenderFPSCounter()->GetFPS(), KGLGameManager()->RenderFPSCounter()->GetElapsedTime() * 1000.0f);
						}
					},
					[&] ()
					{
						if( bShowBuildInfo )
						{
							CharSet::SFormat(cTemp, "CompileDate: %s(%s)", __DATE__, __TIME__);
						}
					},
					[&] ()
					{
						if( bShowBuildInfo )
						{
							CharSet::SFormat(cTemp, "BuildMode: %s(%s)", KGL_DEBUG? "debug": "release", Is64bit()? "64bit": "32bit");
						}
					},
					[&] ()
					{
						if( bShowRunInfo )
						{
							u32 uiTime	= m_pFrameControl->GetSec();
							u32 uiHour	= uiTime / 60 / 60;
							u32 uiMin	= (uiTime / 60) % 60;
							u32 uiSec	= uiTime % 60;
							CharSet::SFormat(cTemp, "RunTime:%02u:%02u:%02u", uiHour, uiMin, uiSec);
						}
					},
					[&] ()
					{
						if( bShowRunInfo )
						{
							CharSet::SFormat(cTemp, "Target FPS:%d%s", Config::TargetFPS, Config::EnableVsync? "(UseVSync)": "");
						}
					},
					[&] ()
					{
						if( bShowRunInfo )
						{
							CharSet::SFormat(cTemp, "Driver:%s", pRenderingSystem->GraphicDriver()->Name());
						}
					},
					[&] ()
					{
						if( bShowRunInfo )
						{
							s32 iWidth, iHeight;
							m_pWindow->GetSize(iWidth, iHeight);
							CharSet::SFormat(cTemp, "Width:%d, Height:%d", iWidth, iHeight);
						}
					},
					[&] ()
					{
						if( bShowRunInfo )
						{
							for( int i=0; ; i++ )
							{
								DISPLAY_DEVICE display = {0};
								display.cb = sizeof(display);
								if( !EnumDisplayDevices(NULL, i, &display, 0) )
								{
									break;
								}

								if( display.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE )
								{
									DEVMODE mode = {0};
									mode.dmSize = sizeof(DEVMODE);
									if( EnumDisplaySettings(display.DeviceName, ENUM_CURRENT_SETTINGS, &mode) )
									{
										CharSet::SFormat(cTemp, "Display Resolution:%d x %d", mode.dmPelsWidth, mode.dmPelsHeight);
									}
									break;
								}
							}
						}
					},
					[&] ()
					{
						if( bShowMemory )
						{
#if KGL_DEBUG_MEMORY && KGL_USE_TLSF_ALLOCATOR
							f32 fSize = (f32)Memory::GetAllocateSize() / 1024.0f;
							f32 fPoolSize = (f32)MemoryPoolSize() / 1024.0f;
							CharSet::SFormat(cTemp, "Memory %.2f/%.2f MB", fSize/1024.0f, fPoolSize/1024.0f);
							if( fSize / fPoolSize > 0.9f )
							{
								DrawColor = FColorRed;
							}
#else
							CharSet::SFormat(cTemp, "Memory ???/??? MB");
#endif // ~#if KGL_DEBUG_MEMORY && KGL_USE_TLSF_ALLOCATOR
						}
					},
					[&] ()
					{
						if( bShowMemory )
						{
#if KGL_DEBUG_MEMORY && KGL_USE_TLSF_ALLOCATOR
							f32 fSize = (f32)Memory::Debug::GetAllocateSize() / 1024.0f;
							f32 fPoolSize = (f32)DebugMemoryPoolSize() / 1024.0f;
							CharSet::SFormat(cTemp, "Memory(Debug) %.2f/%.2f MB", fSize/1024.0f, fPoolSize/1024.0f);
							if( fSize / fPoolSize > 0.9f )
							{
								DrawColor = FColorRed;
							}
#else
							CharSet::SFormat(cTemp, "Memory ???/??? MB");
#endif // ~#if KGL_DEBUG_MEMORY && KGL_USE_TLSF_ALLOCATOR
						}
					},
					[&] ()
					{
						if( bShowScene )
						{
							const c8* pName = KGLSceneManager()->NowSceneName();
							if( pName )
							{
								CharSet::SFormat(cTemp, "Scene:%s", pName);
							}
						}
					},
				};

				for( auto DebugDraw:DebugDrawList )
				{
					DrawColor = FColorWhite;
					cTemp[0] = '\0';
					DebugDraw();
					if( cTemp[0] != '\0' )
					{
						pDebugFont->Draw(pRenderingSystem, cTemp, vPos, vScale, DrawColor);
						vPos.y += pDebugFont->GetDrawTextSize(cTemp, vScale).y;
					}
				}
				if( bShowProfile )
				{
					for( s32 i=0; i < Profiler::EProfile_Costom; i++ )
					{
						Profiler::ProfileInfo* pInfo = PROFILER->GetProfileInfo(Profiler::EProfile_Root+i);
						CharSet::SFormat(cTemp, "%s %2.3f ms Count:%d", pInfo->cName, pInfo->fAveragePerFrame * 1000.0f, pInfo->iFrameCount);
						pDebugFont->Draw(pRenderingSystem, cTemp, vPos, vScale);
						vPos.y += pDebugFont->GetDrawTextSize(cTemp, vScale).y;
					}
				}
			}
			KGLDebugMenu()->Draw(pRenderingSystem);
#endif	// ~#if	KGL_DEBUG_MENU && KGL_DEBUG_FONT
		}
#endif	// ~#if	KGL_DEBUG
		//----------------------------------------------------------------------
		//	終了コードの取得
		//----------------------------------------------------------------------
		s32 ISystem::GetExitCode(void)
		{
			return m_iExitCode;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Archive.h
//!	@brief	アーカイブ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ARCHIVE_H__
#define	__KGL_ARCHIVE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.File.h"

namespace kgl
{
	namespace Archive
	{
		//!	アーカイブ情報
		struct ArchiveInfo
		{
			const c8* pName;	//!< ファイル名
			s32       iSize;	//!< サイズ
		};

		//======================================================================
		//!	アーカイブマネージャーインターフェイス
		//======================================================================
		interface IArchiveManager
			: public IKGLBase
		{
		public:
			//!	初期化(ハッシュ(crc32)が初期されている必要があります)
			//!	@param sFileName	[in] ファイル名
			//!	@return 結果
			virtual b8 Initialize(const c8* pFileName) = 0;
			//!	破棄
			virtual void Finalize(void) = 0;

			//!	ロード
			//!	@param pFileName	[in] ファイル名
			//!	@return ファイルサイズ(-1:エラー)
			virtual s32 Load(const c8* pFileName) = 0;
			//!	ロード(フォルダ単位)
			//!	@param pPath	[in] パス
			//!	@return ロードしたファイル数
			virtual s32 LoadPath(const c8* pPath) = 0;

			//!	アンロード
			//!	@param pFileName	[in] ファイル名
			virtual void Unload(const c8* pFileName) = 0;
			//!	アンロード(フォルダ単位)
			//!	@param pPath	[in] パス
			//!	@return ファイルサイズ(-1:エラー)
			virtual void UnloadPath(const c8* pPath) = 0;
			//!	アンロード
			//!	@param pBuffer	[in] ファイルデータ
			virtual void UnloadData(const void* pBuffer) = 0;

			//!	データ取得(未ロードの場合は読み込んでから取得)
			//!	@param sFileName	[in]  ファイル名
			//!	@param pSize		[out] ファイルのサイズ
			//!	@return データ
			virtual const void* GetData(const c8* pFileName, s32* pSize = null) = 0;

			//!	ファイル取得
			//!	@param sFileName	[in]  ファイル名
			//!	@param pSize		[out] ファイルのサイズ
			//!	@return ファイルハンドル(nullでエラー)
			virtual File::IFile* GetFile(const c8* pFileName, s32* pSize = null) = 0;

			//!	アーカイブ数の取得
			//!	@return アーカイブ数
			virtual s16 GetArchiveNum(void) = 0;

			//!	読み込ん済みのファイル数取得
			//!	@return 読み込み済みのファイル数
			virtual s16 GetFileNum(void) = 0;
			//!	読み込み済みのファイル情報取得(デバッグ処理)
			//!	@param sNo	[in] 番号
			//!	@return アーカイブ情報
			virtual ArchiveInfo GetArchiveInfo(s16 sNo) = 0;
		};

		//!	アーカイブマネージャーの生成
		//!	@param ppArchiveManager	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IArchiveManager** ppArchiveManager);
	}
}

#endif	// __KGL_ARCHIVE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.FileReader.h
//!	@brief	ファイル読み込み管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FILEREADER_H__
#define	__KGL_FILEREADER_H__


//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.File.h"

namespace kgl
{
	namespace File
	{
		//!	管理されていないデータ
		const s32 NoManagerIndex	= -1;

		//!	最大ロードリクエスト数
		const u32 LoadRequestMax	= 256;
		//!	最大パス数
		const u32 LoadPathMax		= 256;

		interface IExtraObject;
		//!	外部から設定した拡張情報を含むデータ取得用構造体
		struct LoadedObject
		{
			const void*		pData;		//!< ロードデータ(元データを解放している場合はnull)
			IExtraObject*	pExtra;		//!< 拡張データ(ロード後に外部から設定出来るデータ)
		};

		//======================================================================
		//!	ファイル読み込み管理インターフェイス
		//======================================================================
		interface IFileReader
			: public IKGLBase
		{
		public:
			//!	読み込み要求(非同期読み込み)
			//!	@param pPath	[in] パス
			//!	@param Callback	[in] 読み込み後のコールバック
			//!	@param pArg		[in] コールバックの引数
			//!	@return 結果
			virtual b8 Load(const c8* pPath, FLoadCallback Callback = null, void* pArg = null) = 0;
			//!	読み込み要求(非同期読み込み)
			//!	@param iIndex	[in] ファイル番号
			//!	@param Callback	[in] 読み込み後のコールバック
			//!	@param pArg		[in] コールバックの引数
			//!	@return 結果
			virtual b8 Load(s32 iIndex, FLoadCallback Callback = null, void* pArg = null) = 0;
			//!	読み込み(ブロック関数)
			//!	@param pPath	[in] パス
			//!	@param Callback	[in] 読み込み後のコールバック
			//!	@param pArg		[in] コールバックの引数
			//!	@return 結果
			virtual const void* LoadBlock(const c8* pPath, FLoadCallback Callback = null, void* pArg = null) = 0;
			//!	読み込み(ブロック関数)
			//!	@param iIndex	[in] ファイル番号
			//!	@param Callback	[in] 読み込み後のコールバック
			//!	@param pArg		[in] コールバックの引数
			//!	@return 結果
			virtual const void* LoadBlock(s32 iIndex, FLoadCallback Callback = null, void* pArg = null) = 0;

		public:
			//!	解放要求
			//!	@param pPath	[in] パス
			virtual void Unload(const c8* pPath) = 0;
			//!	解放要求
			//!	@param iIndex	[in] ファイル番号
			virtual void Unload(s32 iIndex) = 0;
			//!	解放要求
			//!	@param iIndex	[in] データ
			virtual void Unload(const void* pData) = 0;

			//!	全てのデータを破棄する
			virtual void UnloadAll(void) = 0;

		public:
			//!	データ取得
			//!	@param pPath	[in] パス
			//!	@return データ
			virtual const void* GetAddr(const c8* pPath) = 0;
			//!	データ取得
			//!	@param iIndex	[in] ファイル番号
			//!	@return データ
			virtual const void* GetAddr(s32 iIndex) = 0;
			//!	データサイズ取得
			//!	@param pPath	[in] パス
			//!	@return データサイズ
			virtual u32 GetSize(const c8* pPath) = 0;
			//!	データサイズ取得
			//!	@param iIndex	[in] ファイル番号
			//!	@return データサイズ
			virtual u32 GetSize(s32 iIndex) = 0;
			//!	データサイズ取得
			//!	@param pData	[in] データ
			//!	@return データサイズ
			virtual u32 GetSize(const void* pData) = 0;

		public:
			//!	ファイル取得
			//!	@param ppFile	[out] ファイル
			//!	@param pPath	[in] パス
			//!	@param pSize	[out] ファイルサイズ
			//!	@return ファイルハンドル(nullでエラー)
			virtual b8 GetFile(File::IFile** ppFile, const c8* pPath, s32* pSize = null) = 0;
			//!	ファイル取得
			//!	@param ppFile	[out] ファイル
			//!	@param iIndex	[in] ファイル番号
			//!	@param pSize	[out] ファイルサイズ
			//!	@return ファイルハンドル(nullでエラー)
			virtual b8 GetFile(File::IFile** ppFile, s32 iIndex, s32* pSize = null) = 0;

		public:
			//!	拡張データの設定
			//!	@param pPath	[in] パス
			//!	@param pExtra	[in] 拡張データ
			//!	@param bDelete	[in] 元データを解放するか？
			//!	@return 結果
			virtual b8 SetLoadedObject(const c8* pPath, IExtraObject* pExtra, b8 bDelete) = 0;
			//!	拡張データの設定
			//!	@param iIndex	[in] ファイル番号
			//!	@param pExtra	[in] 拡張データ
			//!	@param bDelete	[in] 元データを解放するか？
			//!	@return 結果
			virtual b8 SetLoadedObject(s32 iIndex, IExtraObject* pExtra, b8 bDelete) = 0;

			//!	データ取得(拡張データを含む)
			//!	@param pPath	[in] パス
			//!	@return データ
			virtual LoadedObject GetLoadedObject(const c8* pPath) = 0;
			//!	データ取得(拡張データを含む)
			//!	@param iIndex	[in] ファイル番号
			//!	@return データ
			virtual LoadedObject GetLoadedObject(s32 iIndex) = 0;

		public:
			//!	ロード中か？
			//!	@return trueの場合、ロード中
			virtual b8 IsLoading(void) = 0;
			//!	ロードされているか？
			//!	@param pPath	[in] パス
			//!	@return ロードの有無
			virtual b8 IsLoaded(const c8* pPath) = 0;
			//!	ロードされているか？
			//!	@param iIndex	[in] ファイル番号
			//!	@return ロードの有無
			virtual b8 IsLoaded(s32 iIndex) = 0;

		public:
			//!	ロード終了待ち
			virtual void WaitLoad(void) = 0;

		public:
			//!	パスのバインド
			//!	@param pDest	[in] 元のパス
			//!	@param pBind	[in] バインド後のパス
			virtual void BindPath(const c8* pDest, const c8* pBind) = 0;
		};

		//======================================================================
		//!	拡張データインターフェイス
		//======================================================================
		interface IExtraObject
			: public IKGLBase
		{
		public:
			//!	コンストラクタ
			IExtraObject(void);
			//!	デストラクタ
			virtual ~IExtraObject(void);

		public:
			//!	オーナーの設定
			void SetOwner(IFileReader* pOwner, void* pData);
			//!	解放
			void Unload(void);

		protected:
			TKGLPtr<IFileReader>	m_pOwner;	//!< オーナー
			void*					m_pData;	//!< データ
		};

		//!	ファイルリーダーの生成
		//!	@param ppFileReader		[out] 実体格納用ポインタ
		//!	@param pPath			[in] ファイル名
		//!	@param pRootDirectory	[in] ルートディレクトリ
		//!	@return 結果
		b8 Create(IFileReader** ppFileReader, const c8* pPath = null, const c8* pRootDirectory = null);

	}
}

#endif	// __KGL_FILEREADER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
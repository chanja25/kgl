//----------------------------------------------------------------------
//!
//!	@file	kgl.Directory.h
//!	@brief	ディレクトリ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DIRECTORY_H__
#define	__KGL_DIRECTORY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Directory
	{
		//!	フォルダー作成
		//!	@param pPath [in] パス
		//!	@return 結果
		b8 Create(const c8* pPath);
		//!	フォルダー削除(ファイルが存在する場合は失敗)
		//!	@param pPath [in] パス
		//!	@return 結果
		b8 Remove(const c8* pPath);

		//!	属性
		namespace EAttribute
		{
			const u32 All		= 0xffffffff;
			const u32 File		= (1 << 0);	//!< ファイル
			const u32 Directory	= (1 << 1);	//!< ディレクトリ
			const u32 Hidden	= (1 << 2);	//!< 隠しファイル
			const u32 ReadOnly	= (1 << 3);	//!< 読み取り専用
			const u32 System	= (1 << 4);	//!< システムファイル
		}

		//======================================================================
		//!	ファイル情報
		//======================================================================
		struct Info
		{
			c8		cName[256];			//!< ファイル名
			u64		u64Size;			//!< ファイルサイズ
			u32		uiAttribute;		//!< 属性
			u32		uiReserved;
			Date	Create;				//!< 作成日時
			Date	Access;				//!< アクセス日時
			Date	Update;				//!< 更新日時
		};

		//======================================================================
		//!	ディレクトリマネージャーインターフェイス
		//======================================================================
		interface IDirectory
			: public IKGLBase
		{
		public:
			//!	ファイル検索
			//!	@param pPath [in] パス
			//!	@param pInfo [in] ファイル情報
			//!	@param uiAttribute [in] 属性
			//!	@return ファイル名(失敗時はnull)
			virtual const c8* FindFile(const c8* pPath, Info* pInfo = null, u32 uiAttribute = EAttribute::All) = 0;
			//!	次のファイル取得
			//!	@param pPath [in] パス
			//!	@param pInfo [out] ファイル情報
			//!	@return ファイル名(失敗時はnull)
			virtual const c8* NextFile(Info* pInfo = null) = 0;
		};

		//!	ディレクトリマネージャーの生成
		//!	@param ppDirectory [out] 実体格納用ポインタ
		//!	@param 結果
		b8 Create(IDirectory** ppDirectory);
	}
}

#endif	// __KGL_DIRECTORY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
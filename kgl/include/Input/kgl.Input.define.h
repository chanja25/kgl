//----------------------------------------------------------------------
//!
//!	@file	kgl.Input.define.h
//!	@brief	入力定義
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_INPUT_DEFINE_H__
#define	__KGL_INPUT_DEFINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Input
	{
		//	マウスコード
		namespace EMouse
		{
			const s32 LEFT   = 0;
			const s32 RIGHT  = 1;
			const s32 CENTER = 2;
		}

		//	ジョイステックコード
		namespace EJoyStick
		{
			const s32 UP    = 28;
			const s32 DOWN  = 29;
			const s32 LEFT  = 30;
			const s32 RIGHT = 31;
		}

		//	キー
		namespace EKey
		{
			const u8 KEY_ESCAPE        =  0x01;
			const u8 KEY_1             =  0x02;
			const u8 KEY_2             =  0x03;
			const u8 KEY_3             =  0x04;
			const u8 KEY_4             =  0x05;
			const u8 KEY_5             =  0x06;
			const u8 KEY_6             =  0x07;
			const u8 KEY_7             =  0x08;
			const u8 KEY_8             =  0x09;
			const u8 KEY_9             =  0x0A;
			const u8 KEY_0             =  0x0B;
			const u8 KEY_MINUS         =  0x0C;
			const u8 KEY_EQUALS        =  0x0D;
			const u8 KEY_BACK          =  0x0E;
			const u8 KEY_TAB           =  0x0F;
			const u8 KEY_Q             =  0x10;
			const u8 KEY_W             =  0x11;
			const u8 KEY_E             =  0x12;
			const u8 KEY_R             =  0x13;
			const u8 KEY_T             =  0x14;
			const u8 KEY_Y             =  0x15;
			const u8 KEY_U             =  0x16;
			const u8 KEY_I             =  0x17;
			const u8 KEY_O             =  0x18;
			const u8 KEY_P             =  0x19;
			const u8 KEY_LBRACKET      =  0x1A;
			const u8 KEY_RBRACKET      =  0x1B;
			const u8 KEY_RETURN        =  0x1C;
			const u8 KEY_LCONTROL      =  0x1D;
			const u8 KEY_A             =  0x1E;
			const u8 KEY_S             =  0x1F;
			const u8 KEY_D             =  0x20;
			const u8 KEY_F             =  0x21;
			const u8 KEY_G             =  0x22;
			const u8 KEY_H             =  0x23;
			const u8 KEY_J             =  0x24;
			const u8 KEY_K             =  0x25;
			const u8 KEY_L             =  0x26;
			const u8 KEY_SEMICOLON     =  0x27;
			const u8 KEY_APOSTROPHE    =  0x28;
			const u8 KEY_GRAVE         =  0x29;
			const u8 KEY_LSHIFT        =  0x2A;
			const u8 KEY_BACKSLASH     =  0x2B;
			const u8 KEY_Z             =  0x2C;
			const u8 KEY_X             =  0x2D;
			const u8 KEY_C             =  0x2E;
			const u8 KEY_V             =  0x2F;
			const u8 KEY_B             =  0x30;
			const u8 KEY_N             =  0x31;
			const u8 KEY_M             =  0x32;
			const u8 KEY_COMMA         =  0x33;
			const u8 KEY_PERIOD        =  0x34;
			const u8 KEY_SLASH         =  0x35;
			const u8 KEY_RSHIFT        =  0x36;
			const u8 KEY_MULTIPLY      =  0x37;
			const u8 KEY_LMENU         =  0x38;
			const u8 KEY_SPACE         =  0x39;
			const u8 KEY_CAPITAL       =  0x3A;
			const u8 KEY_F1            =  0x3B;
			const u8 KEY_F2            =  0x3C;
			const u8 KEY_F3            =  0x3D;
			const u8 KEY_F4            =  0x3E;
			const u8 KEY_F5            =  0x3F;
			const u8 KEY_F6            =  0x40;
			const u8 KEY_F7            =  0x41;
			const u8 KEY_F8            =  0x42;
			const u8 KEY_F9            =  0x43;
			const u8 KEY_F10           =  0x44;
			const u8 KEY_NUMLOCK       =  0x45;
			const u8 KEY_SCROLL        =  0x46;
			const u8 KEY_NUMPAD7       =  0x47;
			const u8 KEY_NUMPAD8       =  0x48;
			const u8 KEY_NUMPAD9       =  0x49;
			const u8 KEY_SUBTRACT      =  0x4A;
			const u8 KEY_NUMPAD4       =  0x4B;
			const u8 KEY_NUMPAD5       =  0x4C;
			const u8 KEY_NUMPAD6       =  0x4D;
			const u8 KEY_ADD           =  0x4E;
			const u8 KEY_NUMPAD1       =  0x4F;
			const u8 KEY_NUMPAD2       =  0x50;
			const u8 KEY_NUMPAD3       =  0x51;
			const u8 KEY_NUMPAD0       =  0x52;
			const u8 KEY_DECIMAL       =  0x53;
			const u8 KEY_OEM_102       =  0x56;
			const u8 KEY_F11           =  0x57;
			const u8 KEY_F12           =  0x58;
			const u8 KEY_F13           =  0x64;
			const u8 KEY_F14           =  0x65;
			const u8 KEY_F15           =  0x66;
			const u8 KEY_KANA          =  0x70;
			const u8 KEY_ABNT_C1       =  0x73;
			const u8 KEY_CONVERT       =  0x79;
			const u8 KEY_NOCONVERT     =  0x7B;
			const u8 KEY_YEN           =  0x7D;
			const u8 KEY_ABNT_C2       =  0x7E;
			const u8 KEY_NUMPADEQUALS  =  0x8D;
			const u8 KEY_PREVTRACK     =  0x90;
			const u8 KEY_AT            =  0x91;
			const u8 KEY_COLON         =  0x92;
			const u8 KEY_UNDERLINE     =  0x93;
			const u8 KEY_KANJI         =  0x94;
			const u8 KEY_STOP          =  0x95;
			const u8 KEY_AX            =  0x96;
			const u8 KEY_UNLABELED     =  0x97;
			const u8 KEY_NEXTTRACK     =  0x99;
			const u8 KEY_NUMPADENTER   =  0x9C;
			const u8 KEY_RCONTROL      =  0x9D;
			const u8 KEY_MUTE          =  0xA0;
			const u8 KEY_CALCULATOR    =  0xA1;
			const u8 KEY_PLAYPAUSE     =  0xA2;
			const u8 KEY_MEDIASTOP     =  0xA4;
			const u8 KEY_VOLUMEDOWN    =  0xAE;
			const u8 KEY_VOLUMEUP      =  0xB0;
			const u8 KEY_WEBHOME       =  0xB2;
			const u8 KEY_NUMPADCOMMA   =  0xB3;
			const u8 KEY_DIVIDE        =  0xB5;
			const u8 KEY_SYSRQ         =  0xB7;
			const u8 KEY_RMENU         =  0xB8;
			const u8 KEY_PAUSE         =  0xC5;
			const u8 KEY_HOME          =  0xC7;
			const u8 KEY_UP            =  0xC8;
			const u8 KEY_PRIOR         =  0xC9;
			const u8 KEY_LEFT          =  0xCB;
			const u8 KEY_RIGHT         =  0xCD;
			const u8 KEY_END           =  0xCF;
			const u8 KEY_DOWN          =  0xD0;
			const u8 KEY_NEXT          =  0xD1;
			const u8 KEY_INSERT        =  0xD2;
			const u8 KEY_DELETE        =  0xD3;
			const u8 KEY_LWIN          =  0xDB;
			const u8 KEY_RWIN          =  0xDC;
			const u8 KEY_APPS          =  0xDD;
			const u8 KEY_POWER         =  0xDE;
			const u8 KEY_SLEEP         =  0xDF;
			const u8 KEY_WAKE          =  0xE3;
			const u8 KEY_WEBSEARCH     =  0xE5;
			const u8 KEY_WEBFAVORITES  =  0xE6;
			const u8 KEY_WEBREFRESH    =  0xE7;
			const u8 KEY_WEBSTOP       =  0xE8;
			const u8 KEY_WEBFORWARD    =  0xE9;
			const u8 KEY_WEBBACK       =  0xEA;
			const u8 KEY_MYCOMPUTER    =  0xEB;
			const u8 KEY_MAIL          =  0xEC;
			const u8 KEY_MEDIASELECT   =  0xED;
		};
	}
}

#endif	// __KGL_INPUT_DEFINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================

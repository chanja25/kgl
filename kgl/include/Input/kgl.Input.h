//----------------------------------------------------------------------
//!
//!	@file	kgl.Input.h
//!	@brief	入力関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_INPUT_H__
#define	__KGL_INPUT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.Input.define.h"

namespace kgl
{
	namespace Input
	{
		//======================================================================
		//!	キーボードインターフェイス
		//======================================================================
		interface IKeyboard
			: public IKGLBase
		{
		public:
			//!	キーが押されているかチェック
			//!	@param uiKey [in] キー
			//!	@return 結果
			virtual b8 IsKeyPress(u32 uiKey) = 0;
			//!	キーを押したかチェック
			//!	@param uiKey [in] キー
			//!	@return 結果
			virtual b8 IsKeyPush(u32 uiKey) = 0;
			//!	キーが離されたかチェック
			//!	@param uiKey [in] キー
			//!	@return 結果
			virtual b8 IsKeyRelease(u32 uiKey) = 0;
		};

		//======================================================================
		//!	マウスインターフェイス
		//======================================================================
		interface IMouse
			: public IKGLBase
		{
		public:
			//!	ボタンが押されているかチェック
			//!	@param uiButton [in] キー
			//!	@return 結果
			virtual b8 IsButtonPress(u32 uiButton) = 0;
			//!	ボタンを押したかチェック
			//!	@param uiButton [in] キー
			//!	@return 結果
			virtual b8 IsButtonPush(u32 uiButton) = 0;
			//!	ボタンが離されたかチェック
			//!	@param uiButton [in] キー
			//!	@return 結果
			virtual b8 IsButtonRelease(u32 uiButton) = 0;

			//!	Xの移動量取得
			//!	@return	Xの移動量
			virtual s32 GetX(void) = 0;
			//!	Yの移動量取得
			//!	@return	Yの移動量
			virtual s32 GetY(void) = 0;
			//!	ホイールの回転量取得
			//!	@return	ホイールの回転量
			virtual s32 GetWheel(void) = 0;
			//	ポインターの座標取得
			//!	@param iX [out] X座標
			//!	@param iY [out] Y座標
			virtual void GetPosition(s32& iX, s32& iY) = 0;
		};

		//======================================================================
		//!	ジョイスティックインターフェイス
		//======================================================================
		interface IJoyStick
			: public IKGLBase
		{
		public:
			//!	ボタンが押されているかチェック
			//!	@param uiButton [in] キー
			//!	@return 結果
			virtual b8 IsButtonPress(u32 uiButton) = 0;
			//!	ボタンを押したかチェック
			//!	@param uiButton [in] キー
			//!	@return 結果
			virtual b8 IsButtonPush(u32 uiButton) = 0;
			//!	ボタンが離されたかチェック
			//!	@param uiButton [in] キー
			//!	@return 結果
			virtual b8 IsButtonRelease(u32 uiButton) = 0;

			//!	左スティックの状態取得
			//!	@return 左スティックの状態
			virtual Vector2 GetAxisLeft(void) = 0;
			//!	右スティックの状態取得
			//!	@return 右スティックの状態
			virtual Vector2 GetAxisRight(void) = 0;

			//!	ジョイスティック番号の設定
			//!	@param uiNo [in] 番号
			virtual void SetNo(u32 uiNo) = 0;
			//!	ジョイスティック番号の取得
			//!	@return	番号
			virtual u32 GetNo(void) = 0;
		};

		//	スティックの遊び設定
		//!	@param fMargin [in] スティックの遊び
		void SetAxisMargin(f32 fMargin);
		//	ジョイスティックの検出数取得
		//!	@return ジョイステックの検出数
		u32 GetJoyStickNum(void);

		//!	キーボードの生成
		//!	@param ppKeyboard [out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IKeyboard** ppKeyboard);
		//!	マウスの生成
		//!	@param ppMouse [out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IMouse** ppMouse);
		//!	ジョイスティックの生成
		//!	@param ppJoyStick [out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IJoyStick** ppJoyStick);
	}
}

#endif	// __KGL_INPUT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.CharSet.inline.h
//!	@brief	文字関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_CHARSET_ININE_H__
#define	__KGL_CHARSET_ININE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace CharSet
	{
		//----------------------------------------------------------------------
		//	文字コード判定
		//----------------------------------------------------------------------
		//	指定された文字が2byte文字の先行バイトか判定
		template<>
		FORCEINLINE b8 IsMultiByteChar(c8 c)
		{
			u8 u = (u8)c;
			//	ShiftJisの全角判定
			return ((0x81 <= u && u <=0x9f) || (0xe0 <= u && u <= 0xfc));
		}
		template<> FORCEINLINE b8 IsMultiByteChar(c16 c) { (void)c; return false; }
		//	指定された文字が空白文字か判定
		template<> FORCEINLINE b8 IsWhiteSpace(c8 c) { return c == ' ' || c == '\t' || c < 32; }
		template<> FORCEINLINE b8 IsWhiteSpace(c16 c) { return c == L' ' || c == L'\t' || c < 32; }
		//	指定された文字が改行コードか判定
		template<> FORCEINLINE b8 IsEOL(c8 c) { return c == '\r' || c == '\n'; }
		template<> FORCEINLINE b8 IsEOL(c16 c) { return c == L'\r' || c == L'\n'; }
		//	指定された文字が小文字か判定
		template<> FORCEINLINE b8 IsLower(c8 c) { return 'a' <= c && c <= 'z'; }
		template<> FORCEINLINE b8 IsLower(c16 c) { return L'a' <= c && c <= L'z'; }
		//	指定された文字が大文字か判定
		template<> FORCEINLINE b8 IsUpper(c8 c) { return 'A' <= c && c <= 'Z'; }
		template<> FORCEINLINE b8 IsUpper(c16 c) { return L'A' <= c && c <= L'Z'; }
		//	指定された文字が英数字か判定
		template<typename T>
		FORCEINLINE b8 IsAlnum(T c) { return IsLower(c) || IsUpper(c) || IsNumber(c); }
		//	指定された文字が数字か判定
		template<> FORCEINLINE b8 IsNumber(c8 c) { return '0' <= c && c <= '9'; }
		template<> FORCEINLINE b8 IsNumber(c16 c) { return L'0' <= c && c <= L'9'; }

		namespace Utility
		{
			FORCEINLINE void Format(c8* out, const c8* format, va_list list)
			{
				vsprintf(out, format, list);
			}
			FORCEINLINE void Format(c16* out, const c16* format, va_list list)
			{
				vswprintf(out, format, list);
			}
		}
		//----------------------------------------------------------------------
		//	文字列処理
		//----------------------------------------------------------------------
		//	文字列生成(マルチスレッドセーフではない)
		template<typename T>
		INLINE const T* Format(const T* format, ...)
		{
			static T temp[512];
			va_list list;
			va_start(list, format);
			Utility::Format(temp, format, list);
			va_end(list);
			return temp;
		}
		//	文字列生成
		template<typename T>
		INLINE const T* SFormat(T* out, const T* format, ...)
		{
			T temp[512];
			va_list list;
			va_start(list, format);
			Utility::Format(temp, format, list);
			va_end(list);
			return kgStrcpy(out, temp);
		}
		//	文字列中の大文字を小文字に変換
		template<typename T>
		INLINE const T* ToLower(const T* str)
		{
			static T temp[512];
			return ToLower(temp, str);
		}
		//	文字列中の小文字を大文字に変換
		template<typename T>
		INLINE const T* ToUpper(const T* str)
		{
			static T temp[512];
			return ToUpper(temp, str);
		}
		//	文字列中の大文字を小文字に変換
		template<typename T>
		INLINE const T* ToLower(T* out, const T* str)
		{
			kgStrcpy(out, str);
			for( u32 i=0; out[i] != '\0'; i++ )
			{
				if( IsMultiByteChar(out[i]) )
				{
					i ++;
				}
				else if( IsUpper(out[i]) )
				{
					out[i] = out[i] - 'A' + 'a';
				}
			}
			return out;
		}
		//	文字列中の小文字を大文字に変換
		template<typename T>
		INLINE const T* ToUpper(T* out, const T* str)
		{
			kgStrcpy(out, str);
			for( u32 i=0; out[i] != '\0'; i++ )
			{
				if( IsMultiByteChar(out[i]) )
				{
					i ++;
				}
				else if( IsLower(out[i]) )
				{
					out[i] = out[i] - 'a' + 'A';
				}
			}
			return out;
		}
	}
}

#endif	// __KGL_CHARSET_ININE_H__
//======================================================================
//	END OF FILE
//======================================================================
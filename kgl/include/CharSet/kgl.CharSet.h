//----------------------------------------------------------------------
//!
//!	@file	kgl.CharSet.h
//!	@brief	文字関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_CHARSET_H__
#define	__KGL_CHARSET_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.StringUtility.h"

namespace kgl
{
	namespace CharSet
	{
		//----------------------------------------------------------------------
		//	文字コード判定
		//----------------------------------------------------------------------
		//!	指定された文字が2byte文字の先行バイトか判定
		//!	@param c [in] 文字
		//!	@return 結果 
		template<typename T>
		b8 IsMultiByteChar(T c);
		//!	指定された文字が空白文字か判定
		//!	@param c [in] 文字
		//!	@return 結果
		template<typename T>
		b8 IsWhiteSpace(T c);
		//!	指定された文字が改行コードか判定
		//!	@param c [in] 文字
		//!	@return 結果
		template<typename T>
		b8 IsEOL(T c);
		//!	指定された文字が小文字か判定
		//!	@param c [in] 文字
		//!	@return 結果
		template<typename T>
		b8 IsLower(T c);
		//!	指定された文字が大文字か判定
		//!	@param c [in] 文字
		//!	@return 結果
		template<typename T>
		b8 IsUpper(T c);
		//!	指定された文字が英数字か判定
		//!	@param c [in] 文字
		//!	@return 結果
		template<typename T>
		b8 IsAlnum(T c);
		//!	指定された文字が数字か判定
		//!	@param c [in] 文字
		//!	@return 結果
		template<typename T>
		b8 IsNumber(T c);

		//----------------------------------------------------------------------
		//	文字列処理
		//----------------------------------------------------------------------
		//!	文字列生成(マルチスレッドセーフではない)
		//!	@param format	[in] 生成する文字列
		//!	@param ...		[in] printfの様に使う。例("%d", 100)
		//!	@return 生成した文字列(256文字まで対応 ※\0含む)
		template<typename T>
		const T* Format(const T* format, ...);
		//!	文字列生成
		//!	@param out		[out] 生成した文字列
		//!	@param format	[in]  生成する文字列
		//!	@param ...		[in]  printfの様に使う。例("%d", 100)
		//!	@return pOutの先頭アドレス
		template<typename T>
		const T* SFormat(T* out, const T* format, ...);
		//!	文字列中の大文字を小文字に変換(マルチスレッドセーフではない)
		//!	@param pString	[in]  文字列
		//!	@return pOutの先頭アドレス
		template<typename T>
		const T* ToLower(const T* str);
		//!	文字列中の小文字を大文字に変換(マルチスレッドセーフではない)
		//!	@param pString	[in]  文字列
		//!	@return pOutの先頭アドレス
		template<typename T>
		const T* ToUpper(const T* str);
		//!	文字列中の大文字を小文字に変換
		//!	@param pOut		[out] 出力文字列
		//!	@param pString	[in]  文字列
		//!	@return pOutの先頭アドレス
		template<typename T>
		const T* ToLower(T* out, const T* str);
		//!	文字列中の小文字を大文字に変換
		//!	@param pOut		[out] 出力文字列
		//!	@param pString	[in]  文字列
		//!	@return pOutの先頭アドレス
		template<typename T>
		const T* ToUpper(T* out, const T* str);

		//----------------------------------------------------------------------
		//	文字コード変換
		//----------------------------------------------------------------------
		//!	ShiftJISをUTF16に変換
		//!	@param pDst  [out] UTF16の文字列
		//!	@param pSrc  [in]  文字列(ShiftJIS)
		//!	@param pSize [in/out] pDstのサイズ/変換後の文字列サイズ
		//!	@return 結果
		b8 ConvertShiftJISToUTF16(c16* pDst, const c8* pSrc, u32* pSize);
		//!	ShiftJISをUTF8に変換
		//!	@param pDst  [out] UTF8の文字列
		//!	@param pSrc  [in]  文字列(ShiftJIS)
		//!	@param pSize [in/out] pDstのサイズ/変換後の文字列サイズ
		//!	@return 結果
		b8 ConvertShiftJISToUTF8(c8* pDst, const c8* pSrc, u32* pSize);
		//!	UTF16をShiftJISに変換
		//!	@param pDst  [out] ShiftJISの文字列
		//!	@param pSrc  [in]  文字列(UTF16)
		//!	@param pSize [in/out] pDstのサイズ/変換後の文字列サイズ
		//!	@return 結果
		b8 ConvertUTF16ToShiftJIS(c8* pDst, const c16* pSrc, u32* pSize);
		//!	UTF8をShiftJISに変換
		//!	@param pDst  [out] ShiftJISの文字列
		//!	@param pSrc  [in]  文字列(UTF16)
		//!	@param pSize [in/out] pDstのサイズ/変換後の文字列サイズ
		//!	@return 結果
		b8 ConvertUTF8ToShiftJIS(c8* pDst, const c8* pSrc, u32* pSize);

		//!	ShiftJISをUTF16に変換
		//!	@param str [in] 文字列(ShiftJIS)
		//!	@return 文字列(UTF16)
		algorithm::wstring ToUTF16(const algorithm::string& str);
		//!	UTF16をShiftJISに変換
		//!	@param str [in] 文字列(UTF16)
		//!	@return 文字列(ShiftJIS)
		algorithm::string ToShiftJIS(const algorithm::wstring& str);
		//!	UTF16をShiftJISに変換
		//!	@param str [in] 文字列(UTF16)
		//!	@return 文字列(ShiftJIS)
		algorithm::string ToUTF8(const algorithm::wstring& str);
	}
}

#include "kgl.CharSet.inline.h"

#endif	// __KGL_CHARSET_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.StringUtility.h
//!	@brief	文字関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_STRING_UTILITY_INLINE_H__
#define	__KGL_STRING_UTILITY_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	//	文字列の長さ取得
	template<>
	FORCEINLINE u32 kgStrlen(const c8* str) { return (u32)strlen(str); }
	template<>
	FORCEINLINE u32 kgStrlen(const c16* str) { return (u32)wcslen(str); }
	//	文字列のコピー
	template<>
	FORCEINLINE c8* kgStrcpy(c8* dst, const c8* src) { return strcpy(dst, src); }
	template<>
	FORCEINLINE c16* kgStrcpy(c16* dst, const c16* src) { return wcscpy(dst, src); }
	//	文字列の比較
	template<>
	FORCEINLINE s32 kgStrcmp(const c8* dst, const c8* src) { return strcmp(dst, src); }
	template<>
	FORCEINLINE s32 kgStrcmp(const c16* dst, const c16* src) { return wcscmp(dst, src); }
	//	文字列の比較(大文字小文字を区別しない)
	template<>
	FORCEINLINE s32 kgStricmp(const c8* dst, const c8* src) { return stricmp(dst, src); }
	template<>
	FORCEINLINE s32 kgStricmp(const c16* dst, const c16* src) { return wcsicmp(dst, src); }
	//	文字列の連結
	template<>
	FORCEINLINE c8* kgStrcat(c8* dst, const c8* src) { return strcat(dst, src); }
	template<>
	FORCEINLINE c16* kgStrcat(c16* dst, const c16* src) { return wcscat(dst, src); }
	//	文字列の検索
	template<>
	FORCEINLINE const c8* kgStrstr(const c8* dst, const c8* src) { return strstr(dst, src); }
	template<>
	FORCEINLINE const c16* kgStrstr(const c16* dst, const c16* src) { return wcsstr(dst, src); }
	//	長さを指定した文字列のコピー
	template<>
	FORCEINLINE c8* kgStrncpy(c8* dst, const c8* src, size_t size) { return strncpy(dst, src, size); }
	template<>
	FORCEINLINE c16* kgStrncpy(c16* dst, const c16* src, size_t size) { return wcsncpy(dst, src, size); }
	//	長さを指定した文字列のコピー
	template<>
	FORCEINLINE s32 kgStrncmp(const c8* dst, const c8* src, size_t size) { return strncmp(dst, src, size); }
	template<>
	FORCEINLINE s32 kgStrncmp(const c16* dst, const c16* src, size_t size) { return wcsncmp(dst, src, size); }
	//	長さを指定した文字列のコピー
	template<>
	FORCEINLINE s32 kgStrnicmp(const c8* dst, const c8* src, size_t size) { return strnicmp(dst, src, size); }
	template<>
	FORCEINLINE s32 kgStrnicmp(const c16* dst, const c16* src, size_t size) { return wcsnicmp(dst, src, size); }
	//	文字列の連結(大文字小文字を区別しない)
	template<>
	FORCEINLINE c8* kgStrncat(c8* dst, const c8* src, size_t size) { return strncat(dst, src, size); }
	template<>
	FORCEINLINE c16* kgStrncat(c16* dst, const c16* src, size_t size) { return wcsncat(dst, src, size); }
}

#endif	// __KGL_STRING_UTILITY_INLINE_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.StringUtility.h
//!	@brief	文字関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_STRING_UTILITY_H__
#define	__KGL_STRING_UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	文字列操作
	//----------------------------------------------------------------------
	//!	文字列の長さを取得
	template<typename T>
	FORCEINLINE u32 kgStrlen(const T* str);
	//!	文字列のコピー
	template<typename T>
	FORCEINLINE T* kgStrcpy(T* dst, const T* src);
	//!	文字列の比較
	template<typename T>
	FORCEINLINE s32 kgStrcmp(const T* str1, const T* str2);
	//!	文字列の比較(大文字小文字を区別しない)
	template<typename T>
	FORCEINLINE s32 kgStricmp(const T* str1, const T* str2);
	//!	文字列の連結
	template<typename T>
	FORCEINLINE T* kgStrcat(T* dst, const T* src);
	//!	文字列の検索
	template<typename T>
	FORCEINLINE const T* kgStrstr(const T* dst, const T* src);
	//!	長さを指定した文字列のコピー
	template<typename T>
	FORCEINLINE T* kgStrncpy(T* dst, const T* src, size_t size);
	//!	長さを指定した文字列の比較
	template<typename T>
	FORCEINLINE s32 kgStrncmp(const T* str1, const T* str2, size_t size);
	//!	長さを指定した文字列の比較(大文字小文字を区別しない)
	template<typename T>
	FORCEINLINE s32 kgStrnicmp(const T* str1, const T* str2, size_t size);
	//!	長さを指定した文字列の連結(大文字小文字を区別しない)
	template<typename T>
	FORCEINLINE T* kgStrncat(T* dst, const T* src, size_t size);
}

#include "kgl.StringUtility.inline.h"

#endif	// __KGL_STRING_UTILITY_H__
//======================================================================
//	END OF FILE
//======================================================================
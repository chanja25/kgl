//----------------------------------------------------------------------
//!
//!	@file	kgl.config.h
//!	@brief	コンフィグ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_CONFIG_H__
#define	__KGL_CONFIG_H__

//----------------------------------------------------------------------
//	システム設定
//----------------------------------------------------------------------
namespace kgl
{
	class Vector2;

	namespace Config
	{
		//	更新処理関連
		extern b8		FPSFix;					//!	固定フレームレートを使用するか？
		extern s32		TargetFPS;				//!	目標フレームレート

		//	描画処理関連
		extern s32		ResolutionWidth;		//!	横の解像度
		extern s32		ResolutionHeight;		//!	縦の解像度
		extern Vector2	Resolution;				//!	解像度
		extern Vector2	InvResolution;			//!	解像度の逆数
		extern b8		EnableVsync;			//!	VSyncの有無

		//	ハード情報関連
		extern u64		PhysicalMemory;			//!	メモリサイズ
		extern s32		NumberOfProcessors;		//!	プロセッサー数

		//	アプリケーション関連
		extern b8		ExitRequest;			//!	終了要求が発行されているか？

#if	!KGL_FINAL_RELEASE
		//	エディター関連
		extern b8		Editor;					//!	エディター起動か？
		extern b8		DataConvertOnly;		//!	データコンバートのみで終了するか？
		extern b8		ShaderRebuild;			//!	シェーダーを全てコンパイルし直すか？
#endif	// ~#if	!KGL_FINAL_RELEASE

#if	KGL_DEBUG_MEMORY
		//	アプリケーション内のメモリ情報
		struct ApplicationMemoryInfo
		{
			size_t		MemorySize;				//!< メモリサイズ
			size_t		UseMemory;				//!< 使用メモリ
			size_t		DebugMemorySize;		//!< メモリサイズ(Debug)
			size_t		UseDebugMemory;			//!< 使用メモリ(Debug)
			size_t		TextureMemorySize;		//!< テクスチャーのメモリサイズ
			size_t		UseTextureMemory;		//!< テクスチャーの使用メモリ
		};
		extern ApplicationMemoryInfo	MemoryInfo;
#endif	// ~#if	KGL_DEBUG_MEMORY
	}
}

#if KGL_USE_TLSF_ALLOCATOR
//----------------------------------------------------------------------
//!	メモリプールサイズ取得(ゲーム側でオーバーライドする事で管理サイズを変更可能)
//!	@return メモリプールサイズ
//----------------------------------------------------------------------
extern size_t MemoryPoolSize(void);
#if	KGL_DEBUG_MEMORY
//----------------------------------------------------------------------
//!	デバッグ用メモリプールサイズ取得(ゲーム側でオーバーライドする事で管理サイズを変更可能)
//!	@return デバッグ用メモリプールサイズ
//----------------------------------------------------------------------
extern size_t DebugMemoryPoolSize(void);
#endif	// ~#if KGL_DEBUG_MEMORY
#endif // ~#if KGL_USE_TLSF_ALLOCATOR


#endif	// __KGL_CONFIG_H__
//=======================================================================
//	END OF FILE
//=======================================================================
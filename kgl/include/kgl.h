//----------------------------------------------------------------------
//!
//!	@file	kgl.h
//!	@brief	Kisaragi Game Library ヘッダー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_H__
#define	__KGL_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.version.h"
#include "kgl.build.h"

//	Platform Dependent
#if	WINDOWS
#include "Platform/kgl.win32.h"
#endif	// ~#if WINDOWS

#include "kgl.typedef.h"
#include "kgl.define.h"
#include "kgl.struct.h"
#include "kgl.function.h"
#include "kgl.config.h"

#include "Platform/kgl.atom.h"
#include "kgl.base.h"

#include "Template/kgl.template.h"
#include "Debug/kgl.Debug.h"
#include "Utility/kgl.Utility.h"
#include "Math/kgl.Math.h"
#include "Memory/kgl.Memory.h"
#include "Object/kgl.Object.h"
#include "Time/kgl.Time.h"
#include "Debug/kgl.DebugMenu.h"
#include "Debug/kgl.Profiler.h"
#include "Utility/algorithm/kgl.vector.h"
#include "Utility/algorithm/kgl.string.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Utility/kgl.CommandLine.h"
#include "Utility/kgl.endian.h"
#include "CharSet/kgl.CharSet.h"

#if	CONSOLE_TOOL
#include "kgl.console.h"
#endif	// ~#if CONSOLE_TOOL

//!	エントリーポイント(ゲーム側で実装)
extern s32 AppRun(kgl::Utility::ICommandLine* pCommandLine);
//!	エントリーポイント(Launcherから呼び出される)
#define	IMPLEMENT_ENTRYPOINT		\
	extern s32 EntryPoint(void);	\
	EXPORTFUNC s32 KGLGameEntryPoint(void){ return EntryPoint(); }

#endif	// __KGL_H__
//======================================================================
//	END OF FILE
//======================================================================
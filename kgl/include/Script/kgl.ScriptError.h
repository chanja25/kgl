//---------------------------------------------------------------------------
//!
//!	@file	kgl.ScriptError.h
//!	@brief	スクリプトエラー関連
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
#ifndef	__KGL_SCRIPT_ERROR_H__
#define __KGL_SCRIPT_ERROR_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Script
	{
		namespace EError
		{
			//!	エラー情報
			enum
			{
				None,			//!< エラー無し

				CommentNoStart,	//!< */ に対する /* が見つかりません
				CommentNoClose,	//!< /* に対する */ が見つかりません

				CharDeclError,	//!< 不正な文字定数
				StringNoClose,	//!< 文字列リテラルが閉じていない

				SyntaxError,	//!< 不明な構文
				Unknown,		//!< 不明なエラー
			};
		}
	}
}


#endif	//~#if __KGL_SCRIPT_ERROR_H__
//============================================================================
//	END OF FILE
//============================================================================
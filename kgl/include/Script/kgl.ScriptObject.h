//---------------------------------------------------------------------------
//!
//!	@file	kgl.ScriptObject.h
//!	@brief	スクリプトデータ関連
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
#ifndef	__KGL_SCRIPT_OBJECT_H__
#define __KGL_SCRIPT_OBJECT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "../Utility/Hash/Kgl.Hash.h"
#include "kgl.ScriptError.h"

namespace kgl
{
	namespace Script
	{
		using namespace algorithm;

		//!	未確定のアドレス管理用(return)
		const s32 NoFixReturnAddress	= -1000001;
		//!	未確定のアドレス管理用(break)
		const s32 NoFixBreakAddress		= -1000002;

		//!	基本のデータタイプ
		enum EDataType
		{
			Non_T,		//!< 不明なタイプ
			Void_T,		//!< void型
			//Char_T,		//!< char型
			//Short_T,	//!< short型
			Int_T,		//!< Int型
			//Int64_T,	//!< Int64型
			//Float_T,	//!< float型
			//Double_T,	//!< double型

			BaseDataTypeMax,
		};
		//!	トークンの種類
		enum ETokenKind
		{
			//	区切り文字関連
			Lparen		= '(',	Rparen		= ')',
			Lbrace		= '{',	Rbrace		= '}',
			Lbracket	= '[',	Rbracket	= ']',

			//	演算子関連
			Plus		= '+',	Minus		= '-',
			Multi		= '*',	Division	= '/',
			Mod			= '%',	Not			= '!',
			And			= '&',	Or			= '|',
			Xor			= '^',	NotBit		= '~',
			Assign		= '=',	Question	= '?',
			//	その他
			Colon		= ':',	Semicolon	= ';',
			Sharp		= '#',	Yen			= '\\',
			Comma		= ',',	Period		= '.',
			SngQ		= '\'',	DblQ		= '\"',

			TextTokenStart = 160,
			//	変数型関連
			Void, Char, Short, Int, Int64, Float, Double,
			//	制御文関連.
			If, Else, For, While, Do, Switch, Case,
			Default, Break, Continue, Return,
			//	複数文字からなる演算子関連
			Incre, Decre, AddEq, SubEq, MulEq, DivEq, ModEq,
			AndEq, OrEq, XorEq, NotEq, Equal, AndCmp, OrCmp,
			Less, LessEq, Greater, GreaterEq,
			Lshift,	LShiftEq, Rshift, RShiftEq,

			//	組み込み関数
			Printf, Input, Exit,
			//	トークン種別
			Ident, Letter, Digit,
			CharNum, String,
			IntNum, /*Int64Num,*/
			FloatNum, /*DoubleNum,*/
			//	その他
			NulKind, EofToken, Others,

			TknKindMax,
		};

		//!	シンボルの種類
		enum ESymbolKind
		{
			NoId,		//!< 不明な種類
			VarId,		//!< 変数
			FuncId,		//!< 関数
			ProtId,		//!< プロトタイプ宣言
			ParamId,	//!< 関数引数
		};

		//!	基本の命令コード
		enum EOpCode
		{
			NOP,		//!< 命令なし
			INC,		//!< ++
			DEC,		//!< --
			NEG,		//!< -
			NOT,		//!< !
			NOTBIT,		//!< ~
			ADD,		//!< +
			SUB,		//!< -
			MUL,		//!< *
			DIV,		//!< /
			MOD,		//!< %
			AND,		//!< &
			OR,			//!< |
			XOR,		//!< ^
			LSFT,		//!< <<
			RSFT,		//!< >>
			LESS,		//!< <
			LSEQ,		//!< <=
			GRT,		//!< >
			GTEQ,		//!< >=
			EQU,		//!< ==
			NTEQ,		//!< !=
			LDA,		//!< アドレスをスタックに入れる
			LDI,		//!< 値を入れる
			LOD,		//!< アドレスから値を取得してスタックに入れる
			STO,		//!< アドレスの位置に値を入れる

			ANDCMP,		//!< &&
			ORCMP,		//!< ||
			CALL,		//!< 関数呼び出し
			DEL,		//!< スタック削除
			JMP,		//!< ジャンプ命令
			JPT,		//!< スタックが新ならジャンプ
			JPF,		//!< スタックが偽ならジャンプ
			EQCMP,		//!< 比較して新なら値を消して1、偽ならそのまま0を入れる
			ADBR,		//!< メモリの確保(開放)制御
			RET,		//!< 関数から戻る
			ASS,		//!< 代入
			ASSV,		//!< 代入後に値を残す
			VAL,		//!< アドレスを値に変更してスタックに入れなおす
			LIB,		//!< 組み込み関数呼び出し
			STOP,		//!< 終了命令
		};

		//======================================================================
		//!	トークン情報
		//======================================================================
		struct Token
		{
			string	strText;	//!< 文字列
			s32		iKind;		//!< 種類
			s32		iValue;		//!< 値(又はアドレス)
		};
		//======================================================================
		//!	キーワード情報
		//======================================================================
		struct Keyword
		{
			string	strKey;		//!< キー
			s32		iHash;		//!< ハッシュ値
			s32		iKind;		//!< トークンの種類
		};
		//======================================================================
		//!	シンボル情報
		//======================================================================
		struct Symbol
		{
			string			strName;	//!< シンボル名
			s32				iHash;		//!< ハッシュ値
			s32				iKind;		//!< トークンの種類
			s32				iDataType;	//!< データタイプ
			vector<s32>		iArrayLen;	//!< 配列長
			vector<Symbol>	Args;		//!< 引数情報
			c8				cLevel;		//!< 定義レベル(0:大域, 1〜:局所)
			c8				pad[3];
			s32				iAdrs;		//!< 番地
		};
		//======================================================================
		//!	コード情報
		//======================================================================
		struct Code
		{
			u8	ucOpCode;	//!< 命令コード
			b8	bFlag: 1;	//!< フラグ
			s8	pad[2];
			s32	iOpData;	//!< 数値、又は番地
		};

		//======================================================================
		//!	ループ文の情報
		//======================================================================
		struct LoopBlock
		{
			s32			iKind;		//!< 文の種類
			s32			iLoopTop;	//!< ループの開始番地
			BITFIELD	bBreak: 1;	//!< breakの有無
		};
		//======================================================================
		//!	switch文の情報
		//======================================================================
		struct SwitchBlock
		{
			s32	iDefAdrs;		//!< defaultの対応番地
			s32	iStartCaseList;	//!< caseListの開始番地
		};
		//======================================================================
		//!	case文の情報
		//======================================================================
		struct CaseList
		{
			s32	iValue;			//!< case対応する値
			s32	iAdrs;			//!< case処理の対応番地
		};

		//======================================================================
		//!	エラー情報
		//======================================================================
		struct ErrorInfo
		{
			string	strError;	//!< エラー内容
			s32		iError;		//!< エラー番号
			s32		iLine;		//!< エラーの行数
		};
	}
}


#endif	//~#if __KGL_SCRIPT_OBJECT_H__
//============================================================================
//	END OF FILE
//============================================================================
//---------------------------------------------------------------------------
//!
//!	@file	kgl.ScriptCompiler.h
//!	@brief	スクリプトコンパイル関連
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
#ifndef	__KGL_SCRIPT_COMPILER_H__
#define __KGL_SCRIPT_COMPILER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.ScriptObject.h"
#include "kgl.ScriptLexser.h"

namespace kgl
{
	namespace Script
	{
		//======================================================================
		//!	文字描画管理インターフェイス
		//======================================================================
		class CKSCompiler
		{
		public:
			//!	コンストラクタ
			CKSCompiler(void);

		public:
			//!	初期化
			//! @param pPath [in] コンパイルするデータのパス(ディレクトリ　)
			b8 Initialize(const c8* pPath);
			//!	破棄
			void Finalize(void);

		public:
			//!	コンパイル
			b8 Compile(const c8* pPath);

		private:
			CKSLexer*	m_pLexser;	//!< 字句解析用
		};
	}
}


#endif	//~#if __KGL_SCRIPT_COMPILER_H__
//============================================================================
//	END OF FILE
//============================================================================
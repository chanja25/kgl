//---------------------------------------------------------------------------
//!
//!	@file	kgl.ScriptLexser.h
//!	@brief	スクリプトの字句解析
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
#ifndef	__KGL_SCRIPT_LEXSER_H__
#define __KGL_SCRIPT_LEXSER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.ScriptObject.h"
#include "../File/kgl.File.h"

namespace kgl
{
	namespace Script
	{
		//======================================================================
		//!	文字描画管理インターフェイス
		//======================================================================
		class CKSLexer
		{
		public:
			//!	コンストラクタ
			CKSLexer(void);

			//!	初期化
			//!	@param pPath [in] ファイルパス
			b8 Initialize(const c8* pPath);
			//!	破棄
			void Finalize(void);

		public:
			//!	次のトークンへ進める
			//!	@return 次のトークン情報
			const Token& NextToken(void);

			//!	エラー情報の取得
			//!	@return エラー情報(エラーが無ければnull)
			const ErrorInfo* GetError(void);

		private:
			//!	文字タイプの初期化
			void InitCharType(void);
			//!	キーワードの初期化
			void InitKeyword(void);

		private:
			//!	数値の解析
			//!	@param token [in] トークン情報
			//!	@return 次の文字
			c8 AnalizeNumber(Token& token);
			//!	トークン種別の設定
			//!	@param token [in/out] トークン情報
			void SetKind(Token& token);
			//!	次の文字を取得
			//!	@return 次の文字
			c8 NextChar(void);

			//!	次の文字の読み込み
			//!	@return 次の文字
			c8 ReadNextChar(void);
			//!	前の文字の読み込み
			void SeekPrevChar(void);

		private:
			//!	文字コードの取得
			//!	@return 文字コード
			c8 CheckCharCode(void);
			//!	2文字からなる演算子か？
			b8 IsOp2(c8 c1, c8 c2);
			//!	3文字からなる演算子か？
			b8 IsOp3(const string& str, c8 c);

		private:
			//!	エラー情報の設定
			//!	@param iError	[in] エラーコード
			//!	@param iLine	[in] 行番号
			//!	@oaram strError	[in] エラー情報
			void SetError(s32 iError, s32 iLine, const string& strError);

		private:
			TKGLPtr<File::IFile>	m_pFile;			//!< ファイル操作用
			TKGLPtr<Hash::IHash>	m_pHash;			//!< ハッシュ値の算出用
			ErrorInfo				m_Error;			//!< エラー情報
			Token					m_Token;			//!< トークン情報
			ETokenKind				m_eTknKindTbl[256];	//!< トークン種別解析用
			vector<Keyword>			m_Keyword;			//!< キーワードリスト
			s32						m_iLineNo;			//!< ライン番号
			c8						m_cChar;			//!< 解析用
			c8						m_cQuotChar;		//!< "'の解析用文字
			c8						m_pad[2];
		};
	}
}


#endif	//~#if __KGL_SCRIPT_LEXSER_H__
//============================================================================
//	END OF FILE
//============================================================================
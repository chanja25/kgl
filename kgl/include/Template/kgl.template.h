//----------------------------------------------------------------------
//!
//!	@file	kgl.template.h
//!	@brief	テンプレート関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TEMPLATE_H__
#define	__KGL_TEMPLATE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.template.utility.h"
#include "kgl.template.check.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	解放用テンプレート
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE void SafeRelease(T& Pointer)
	{
		if( Pointer != null )
		{
			Pointer->Release();
			Pointer = null;
		}
	}
	template<typename T>
	FORCEINLINE void SafeDelete(T& Pointer)
	{
		if( Pointer != null )
		{
			delete Pointer;
			Pointer = null;
		}
	}
	template<typename T>
	FORCEINLINE void SafeDeleteArray(T& Pointer)
	{
		if( Pointer != null )
		{
			delete [] Pointer;
			Pointer = null;
		}
	}

	//----------------------------------------------------------------------
	//!	絶対値を返す
	//!	@param value [in] 値
	//!	@return 結果
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T Abs(const T& value)
	{
		return (value >= 0)? value: -value;
	}
	//----------------------------------------------------------------------
	//!	低い方の値を返す
	//!	@param value [in] 値
	//!	@param min   [in] 最小値
	//!	@return 結果
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T Min(const T& value, const T& min)
	{
		return (value < min)? value: min;
	}
	//----------------------------------------------------------------------
	//!	高い方の値を返す
	//!	@param value [in] 値
	//!	@param max   [in] 最大値
	//!	@return 結果
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T Max(const T& value, const T& max)
	{
		return (value >= max)? value: max;
	}
	//----------------------------------------------------------------------
	//!	値を範囲内に収める
	//!	@param value [in] 値
	//!	@param min   [in] 最小値
	//!	@param max   [in] 最大値
	//!	@return 結果
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T Clamp(const T& value, const T& min, const T& max)
	{
		return (value <= min)? min: (value >= max)? max: value;
	}
	//----------------------------------------------------------------------
	//!	値が範囲内にあるか確認
	//!	@param value [in] 値
	//!	@param min   [in] 最小値
	//!	@param max   [in] 最大値
	//!	@return 結果
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE b8 IsRange(const T& value, const T& min, const T& max)
	{
		return (min <= value && value <= max)? true: false;
	}
	//----------------------------------------------------------------------
	//!	値の範囲内の割合を取得(0〜1にClampされる)
	//!	@param value [in] 値
	//!	@param min   [in] 最小値
	//!	@param max   [in] 最大値
	//!	@return 結果
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE f32 RangeRate(const T& value, const T& min, const T& max)
	{
		return	(value <= min)? 0.0f:
				(value >= max)? 1.0f:
				(f32)(value - min) / (f32)(max - min);
	}

	template<typename T>
	FORCEINLINE void ArrayFill(T (&target), const T& value)	{ target = value; }
	//----------------------------------------------------------------------
	//!	指定した値で配列の値を埋める
	//!	@param target	[in] ターゲット配列
	//!	@param value	[in] 値
	//----------------------------------------------------------------------
	template<typename T, typename U, u32 N>
	FORCEINLINE void ArrayFill(T (&target)[N], const U& value)
	{
		for( u32 i=0; i < N; i++ )
		{
			ArrayFill(target[i], value);
		}
	}
	//----------------------------------------------------------------------
	//!	配列の要素数を取得する
	//!	@return 要素数
	//----------------------------------------------------------------------
	template<typename T, u32 N>
	FORCEINLINE u32 ArrayCount(const T (&)[N])
	{
		return N;
	}
	//----------------------------------------------------------------------
	//!	アラインメント算出
	//!	@return アライメントした値
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T Align(T size, T align)
	{
		return (size + align - 1) & ~(align - 1);
	}
	//----------------------------------------------------------------------
	//!	キャスト
	//!	@return キャスト結果
	//----------------------------------------------------------------------
	template<typename T1, typename T2>
	FORCEINLINE T1* Cast(T2* Dst)
	{
		return dynamic_cast<T1*>(Dst);
	}
}

#endif	// __KGL_TEMPLATE_H__
//======================================================================
//	END OF FILE
//======================================================================
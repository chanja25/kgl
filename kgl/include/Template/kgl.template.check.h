//----------------------------------------------------------------------
//!
//!	@file	kgl.template.check.h
//!	@brief	テンプレート関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TEMPLATE_CHECK_H__
#define	__KGL_TEMPLATE_CHECK_H__


namespace kgl
{
	namespace templateUtil
	{
		//	ポインタ判定用
		template<typename T> struct IsPointer : isFalse{};
		template<typename T> struct IsPointer<T*> : isTrue{};
	}

	template<typename T>
	struct HasTrivialConstructor { enum { value = __has_trivial_constructor(T) }; };
	template<typename T>
	struct HasTrivialDestructor { enum { value = __has_trivial_destructor(T) }; };
	template<typename T>
	struct HasTrivialAssign { enum { value = __has_trivial_assign(T) }; };
	template<typename T>
	struct HasTrivialCopy { enum { value = __has_trivial_copy(T) }; };
	template<typename T>
	struct IsPODType { enum { value = __is_pod(T) }; };
	template<typename T>
	struct IsEnumType { enum { value = __is_enum(T) }; };
	template<typename T>
	struct IsEmptyType { enum { value = __is_empty(T) }; };
	template<typename T>
	struct IsAbstractType { enum { value = __is_abstract(T) }; };
	template<typename T>
	struct IsBaseOf { enum { value = __is_base_of(T) }; };

	//----------------------------------------------------------------------
	//!	型のタイプ判定
	//----------------------------------------------------------------------
	template<typename T>
	struct TypeInfo
	{
		enum
		{
			//!	POD型か？
			IsPOD = IsPODType<T>::value,
			//!	Enum型か？
			IsEnum = IsEnumType<T>::value,
			//!	実体(変数)を持たない型か？
			IsEmpty = IsEmptyType<T>::value,
			//!	Pointer型か？
			IsPointer = templateUtil::IsPointer<T>::value,
			//!	抽象型か？
			IsAbstract = IsAbstractType<T>::value,

			//!	明示的なコンストラクタの呼び出しが必要か？
			NeedsConstructor = !HasTrivialConstructor<T>::value && !IsPOD,
			//!	明示的なデストラクタの呼び出しが必要か？
			NeedsDestructor = !HasTrivialDestructor<T>::value && !IsPOD,
			//!	明示的なコピーコンストラクタを持っているか？
			NeedsCopyConstructor = !HasTrivialCopy<T>::value,
			//!	明示的なコピー関数を持っているか？
			NeedsCopyAssignment = !HasTrivialAssign<T>::value,
		};
	};
}

#endif	// __KGL_TEMPLATE_CHECK_H__
//======================================================================
//	END OF FILE
//======================================================================
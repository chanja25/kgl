//----------------------------------------------------------------------
//!
//!	@file	kgl.template.utility.h
//!	@brief	テンプレート関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TEMPLATE_UTILITY_H__
#define	__KGL_TEMPLATE_UTILITY_H__


namespace kgl
{
	namespace templateUtil
	{
		template<b8, typename T = void> struct _enableIf { typedef T type; };
		template<> struct _enableIf<false> {};
		template<b8, typename T = void>	struct _disableIf{ typedef T type; };
		template<>	struct _disableIf<true>{};
	}

	//	比較用構造体
	struct isTrue  { typedef isTrue  type; static const b8 value = true;  };
	struct isFalse { typedef isFalse type; static const b8 value = false; };

	//	判定用構造体
	template<b8> struct condition{ typedef isTrue type; };
	template<> struct condition<false>{ typedef isFalse type; };

	//	判定用(if)構造体
	template<typename Condition, typename T = void>
	struct enableIf  : public templateUtil::_enableIf<Condition::value, T>{};
	template<typename Condition, typename T = void>
	struct disableIf : public templateUtil::_disableIf<Condition::value, T>{};
}

#endif	// __KGL_TEMPLATE_UTILITY_H__
//======================================================================
//	END OF FILE
//======================================================================
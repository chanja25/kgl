//----------------------------------------------------------------------
//!
//!	@file	kgl.Debug.h
//!	@brief	デバッグ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DEBUG_H__
#define	__KGL_DEBUG_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


//----------------------------------------------------------------------
//	アサート関連
//----------------------------------------------------------------------
#if	KGL_DEBUG_ASSERT

//	強制ブレーク
#if	WINDOWS
#define	kgDebugBreak()					DebugBreak()
#else	// ~#if	WINDOWS
#define	kgDebugBreak()					(*(s32*)0 = -1)
#endif	// ~#if	WINDOWS
//	アサート
#define	kgAssert(_condition_, _string_)	if( !(_condition_) ) { kgErrorLog(_string_); WaitTrace(); kgDebugBreak(); }
#define	kgAssertionFailed(_condition_)	kgAssert(_condition_, #_condition_)

#else	// ~#if KGL_DEBUG_ASSERT

#define	kgDebugBreak()					((void)(0))
//	アサート
#define	kgAssert(...)					((void)(0))
#define	kgAssertionFailed(...)			((void)(0))

#endif	// ~#if KGL_DEBUG_ASSERT

//----------------------------------------------------------------------
//	トレース関連
//----------------------------------------------------------------------
#if	KGL_DEBUG_TRACE

//	エラーログ出力
#define	kgErrorLog(...)			ErrorTrace("================== ERROR LOG ============================");	\
								ErrorTrace("File : %s", __FILE__);											\
								ErrorTrace("Line : %d", __LINE__);											\
								ErrorTrace(__VA_ARGS__);													\
								ErrorTrace("=========================================================");

#define	kgCheckErrorLog(_condition_, ...)	if( !(_condition_) ) { kgErrorLog(__VA_ARGS__); }

//	トレース
void SystemTrace(const c8* pString, ...);
void SystemTrace(const c16* pString, ...);
void ErrorTrace(const c8* pString, ...);
void ErrorTrace(const c16* pString, ...);
void WarningTrace(const c8* pString, ...);
void WarningTrace(const c16* pString, ...);
void Trace(const c8* pString, ...);
void Trace(const c16* pString, ...);

//	トレースの有効設定
void EnableSystemTrace(b8 bEnable);
void EnableErrorTrace(b8 bEnable);
void EnableWarningTrace(b8 bEnable);
void EnableTrace(b8 bEnable);

//	トレースの非同期化
void AsyncTrace(b8 bEnable);
void WaitTrace(void);

#else // ~#if KGL_DEBUG_TRACE

#define	kgErrorLog(...)			((void)(0))
#define	kgCheckErrorLog(...)	((void)(0))

#define SystemTrace(...)		((void)(0))
#define ErrorTrace(...)			((void)(0))
#define WarningTrace(...)		((void)(0))
#define Trace(...)				((void)(0))

#define EnableSystemTrace(...)	((void)(0))
#define EnableErrorTrace(...)	((void)(0))
#define EnableWarningTrace(...)	((void)(0))
#define EnableTrace(...)		((void)(0))

#define AsyncTrace(...)			((void)(0))
#define WaitTrace()				((void)(0))

#endif	// ~#if KGL_DEBUG_TRACE

#endif	// __KGL_DEBUG_H__
//======================================================================
//	END OF FILE
//======================================================================
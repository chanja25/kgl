//----------------------------------------------------------------------
//!
//!	@file	kgl.DebugMenu.h
//!	@brief	デバッグメニュー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DEBUGMENU_H__
#define	__KGL_DEBUGMENU_H__

#if	KGL_DEBUG_MENU

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "Utility/Hash/kgl.Hash.h"


namespace kgl
{
	namespace Render
	{
		interface IRenderingSystem;
	}
	namespace Input
	{
		interface IKeyboard;
	}

	namespace Debug
	{
		//!	デバッグメニューでの最大文字列数
		const s32 TagLengthMax	= 32;
		//!	親がいない場合のインデックス
		const s32 IndexNone = -1;
		//!	デバッグ値
		enum EValue
		{
			EValue_Group,	//!< メニュー階層
			EValue_S32,		//!< 整数値
			EValue_F32,		//!< 小数値
			EValue_Bool,	//!< bool値

			EValue_Max,
		};
		//!	値変更時のコールバック
		typedef void (*FChangeCallback)(const struct DebugMenuValue*, void* pArg);
		//!	デバッグメニューの値
		struct DebugMenuValue
		{
			c8				cTag[TagLengthMax];			//!< タグ名
			u32				uiHash;						//!< ハッシュ値(タグ名)
			s32				iValueType;					//!< 値の種類
			s32				iLabelCount;				//!< ラベル数
			c8				(*pLabel)[TagLengthMax];	//!< ラベル(値に対応した文字列)
			BITFIELD		bEnableSave:1;				//!< セーブの有無
			DebugMenuValue*	pParent;					//!< 親
			DebugMenuValue*	pNext;						//!< 次の値
			DebugMenuValue*	pPrev;						//!< 前の値
			DebugMenuValue*	pChild;						//!< 子の値
			FChangeCallback	ChangeCallback;				//!< 値変更時のコールバック
			void*			pArg;						//!< コールバックの引数

			union
			{
				s32	iValue;		//!< 値
				f32	fValue;		//!< 値
				s32	bValue;		//!< 値
			};
			union
			{
				s32	iValueMin;	//!< 最少値
				f32	fValueMin;	//!< 最少値
			};
			union
			{
				s32	iValueMax;	//!< 最大値
				f32	fValueMax;	//!< 最大値
			};
		};
		//!	メニュー項目のリスト
		struct DebugGroupList
		{
			DebugMenuValue*		pGroup;	//!< グループ
			s32					iCount;	//!< 項目数
			DebugMenuValue**	ppList;	//!< メニューリスト
		};

		//======================================================================
		//!	デバッグメニュー
		//======================================================================
		class CDebugMenu
		{
		public:
			//!	コンストラクタ
			CDebugMenu(void);
			//!	デストラクタ
			~CDebugMenu(void);

		public:
			//!	ヘッダーファイルの作成
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 CreateHeader(const c8* pPath = "./Config/Debug/DebugMenu.txt");

			//!	初期化
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 Initalize(const c8* pPath = "./Config/Debug/DebugMenu.txt");
			//!	解放
			void Finalize(void);

			//!	更新
			void Update(void);
			//!	描画
			//!	@param pRenderingSystem	[in] 描画システム
			void Draw(Render::IRenderingSystem* pRenderingSystem);

		public:
			//!	有効フラグ設定
			//!	@param bEnable [in] 有効フラグ
			void SetEnable(b8 bEnable);
			//!	表示フラグ設定
			//!	@param bVisible	[in] 表示フラグ
			void SetVisible(b8 bVisible);
			//!	表示フラグ取得
			//!	@return 表示フラグ
			b8 IsVisible(void);

		public:
			//!	ルートのデバッグメニュー値を取得
			//!	@return ルートのデバッグメニュー値
			INLINE const DebugMenuValue* Root(void){ return m_pRoot; }
			//!	ルートのデバッグメニュー値を取得
			//!	@return ルートのデバッグメニュー値
			INLINE const DebugMenuValue* Select(void){ return m_pSelect; }

		private:
			//!	デバッググループの検索
			//!	@param group	[in] グループ
			//!	@return デバッググループ
			DebugGroupList* CDebugMenu::SearchGroup(const c8* group);
			//!	デバッグ項目の検索
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@param type		[in] 値の種類
			//!	@return デバッグ項目
			DebugMenuValue* SearchValue(const c8* tag, const c8* group = null, s32 type = -1);

		public:
			//!	値変更時のコールバック設定
			//!	@param func		[in] コールバック
			//!	@param arg		[in] コールバックの引数
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			void SetChangeCallback(FChangeCallback func, void* arg, const c8* tag, const c8* group = null);
			//!	値設定
			//!	@param value	[in] 値
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			void SetValue(s32 value, const c8* tag, const c8* group = null);
			//!	値設定
			//!	@param value	[in] 値
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			void SetValueF32(f32 value, const c8* tag, const c8* group = null);
			//!	文字列設定
			//!	@param value	[in] 値
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 文字列
			void SetValueBool(b8 value, const c8* tag, const c8* group = null);

		public:
			//!	値取得
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			s32 GetValue(const c8* tag, const c8* group = null);
			//!	値取得
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			f32 GetValueF32(const c8* tag, const c8* group = null);
			//!	値取得
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			b8 GetValueBool(const c8* tag, const c8* group = null);
			//!	値取得
			//!	@param tag		[in] タグ
			//!	@param group	[in] グループ
			//!	@return 値
			const c8* GetValueString(const c8* tag, const c8* group = null);

		public:
			//!	値設定
			//!	@param value	[in] 値
			//!	@param index	[in] インデックス
			//!	@return 値
			void SetValue(s32 value, s32 index);
			//!	値設定
			//!	@param value	[in] 値
			//!	@param index	[in] インデックス
			//!	@return 値
			void SetValueF32(f32 value, s32 index);
			//!	文字列設定
			//!	@param value	[in] 値
			//!	@param index	[in] インデックス
			//!	@return 文字列
			void SetValueBool(b8 value, s32 index);

		public:
			//!	値取得
			//!	@param index	[in] インデックス
			//!	@return 値
			s32 GetValue(s32 index);
			//!	値取得
			//!	@param index	[in] インデックス
			//!	@return 値
			f32 GetValueF32(s32 index);
			//!	値取得
			//!	@param index	[in] インデックス
			//!	@return 値
			b8 GetValueBool(s32 index);
			//!	値取得
			//!	@param index	[in] インデックス
			//!	@return 値
			const c8* GetValueString(s32 index);

		private:
			TKGLPtr<Input::IKeyboard>	m_pKeyboard;	//!< デバッグ入力用
			TKGLPtr<Hash::IHash>		m_pHash;		//!< ハッシュ生成用
			DebugMenuValue*				m_pRoot;		//!< デバッグメニュールート
			DebugMenuValue*				m_pSelect;		//!< 選択中の値
			DebugGroupList				m_List;			//!< 検索用のリスト
			s32							m_iGroupCount;	//!< 項目数
			DebugGroupList*				m_pGroupList;	//!< グループリスト
			s32							m_iAddRate;		//!< 加算レート
			BITFIELD					m_bEnable:1;	//!< 有効フラグ
			BITFIELD					m_bVisible:1;	//!< 表示フラグ
		};
	}
}

#endif	// ~#if KGL_DEBUG_MENU

#endif	// __KGL_DEBUGMENU_H__
//======================================================================
//	END OF FILE
//======================================================================
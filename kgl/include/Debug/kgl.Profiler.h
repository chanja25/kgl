//----------------------------------------------------------------------
//!
//!	@file	kgl.Profile.h
//!	@brief	プロファイラー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_PROFILER_H__
#define	__KGL_PROFILER_H__


#if	KGL_DEBUG_PROFILE
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


namespace kgl
{
	namespace Profiler
	{
		//!	カテゴリー
		enum ECategory
		{
			ECategory_System,

			ECategory_Costom,	//	ゲーム側で宣言する場合はECategory_Costom以降を使用する
		};
		enum EProfile
		{
			EProfile_Root,
			EProfile_SystemUpdate,
			EProfile_SystemInput,
			EProfile_SystemRendering,
			EProfile_SystemDebugRendering,

			EProfile_Costom,	//	ゲーム側で宣言する場合はECategory_Costom以降を使用する
		};

		//!	ログの最大数
		const s32 LOG_MAX = 512;
		//!	平均時間の計測フレーム数
#if	WINDOWS
		const s32 AVG_FRAME = 0x40;
#else	// ~#if	WINDOWS
		const s32 AVG_FRAME = 0x20;
#endif	// ~#if	WINDOWS

		//!	プロファイル情報
		struct ProfileInfo
		{
			s32		iIndex;					//!< インデックス
			s32		iCategory;				//!< カテゴリー

			// 計測用値
			s32		iTmpCount;				//!< 計測地の出現回数

			// 表示用パラメータ
			c8		cName[32];				//!< プロファイル名
			s32		iFrameCount;			//!< 計測回数(フレーム間)
			f32		fFrameTime;				//!< 処理時間(フレーム間)
			f32		fAverageTime;			//!< 平均処理時間
			f32		fAveragePerFrame;		//!< 平均処理時間
			f32		fMinTime;				//!< 最小処理時間
			f32		fMaxTime;				//!< 最大処理時間
		};
		//!	ログ
		struct Log
		{
			Log* pParent;					//!< 親
			Log* pBrother;					//!< 兄弟
			Log* pChild;					//!< 子供

			// 計測用パラメータ
			f32 fBeginTime;					//!< 計測開始時間
			f32 fEndTime;					//!< 計測終了時間
			s32 iTmpCount;					//!< 計測回数
			f32 fTmpFrameTime;				//!< 処理時間(フレーム間)
			f32 fTmpMinTime;				//!< 最小処理時間
			f32 fTmpMaxTime;				//!< 最大処理時間
			s32 iTmpTotalTimeIndex;			//!< 合計処理時間用のインデックス
			s32 iTmpTotalCount[AVG_FRAME];	//!< 合計処理時間
			f32 fTmpTotalTime[AVG_FRAME];	//!< 合計処理時間

			// 表示用パラメータ
			s32	iIndex;						//!< インデックス
			s32 iFrameCount;				//!< 計測回数(フレーム間)
			f32 fFrameTime;					//!< 処理時間(フレーム間)
			f32 fAverageTime;				//!< 平均処理時間
			f32 fAveragePerFrame;			//!< 平均処理時間
			f32 fMinTime;					//!< 最小処理時間
			f32 fMaxTime;					//!< 最大処理時間

			ProfileInfo* pProfileInfo;		//!< プロファイル情報
		};

		//======================================================================
		//!	プロファイラクラス(システムで初期化と終了処理を行う)
		//======================================================================
		class CProfiler
		{
		public:
			//!	初期化
			//!	@return 結果
			static b8 InitializeInstance(void);
			//!	終了処理
			static void FinalizeInstance(void);
			//!	初期化
			//!	@return 結果
			b8 Initialize(void);
			//!	終了処理
			void Finalize(void);

		public:
			//!	フレームの開始
			void StartFrame(void);
			//!	フレームの終了
			void EndFrame(void);
			//!	計測開始
			//!	@param iIndex [in] インデックス
			void Begin(s32 iIndex);
			//!	計測終了
			//!	@param iIndex [in] インデックス
			void End(s32 iIndex);

		public:
			//!	ログ情報の設定
			//!	@param iIndex		[in] インデックス
			//!	@param iCategory	[in] カテゴリー
			//!	@param pName		[in] ログ名
			void SetLogInfo(s32 iIndex, s32 iCategory, c8* pName);
			//!	プロファイル情報を取得
			//!	@param iIndex [in] インデックス
			//!	@return	ログ
			ProfileInfo* GetProfileInfo(s32 iIndex);

		public:
			//!	ルートログの取得
			//!	@return	ルートログ
			Log* GetRootLog(void);
			//!	ログの取得
			//!	@param iIndex [in] インデックス
			//!	@return	ログ
			Log* GetLog(s32 iIndex, Log* pLog = null);

		public:
			//!	インスタンスの取得
			//!	@return インスタンス
			INLINE static CProfiler* GetInstance(void) { return m_pInstance; }

			//!	経過時間取得
			//!	@return 経過時間
			INLINE f64 GetTime(void) { return m_pStopwatch->Get(); }

		private:
			//!	コンストラクタ
			CProfiler(void);
			//!	デストラクタ
			~CProfiler(void);

			//!	ログの情報生成
			//!	@param iIndex	[in] インデックス
			//!	@param pParent	[in] 親となるログ情報ポインタ
			//!	@return 新規のログ情報ポインタ
			Log* CreateLog(s32 iIndex, Log* pParent);
			//!	ログの更新
			//!	@param pLog [in/out] 更新されるログ情報
			void UpdateLog(Log* pLog);

		private:
			static CProfiler*			m_pInstance;	//!< 唯一のインスタンス

			TKGLPtr<Time::IStopwatch>	m_pStopwatch;	//!< ストップウォッチ
			ProfileInfo*				m_pInfo;		//!< カテゴリー情報
			Log*						m_pRoot;		//!< ルートログ
			Log*						m_pCurrent;		//!< カレントログ
			s32							m_iInfoCount;	//!< プロファイル情報の数
			s32							m_iLogCount;	//!< ログの数
		};

		//======================================================================
		//!	スコープ内処理時間計測プロファイラ
		//======================================================================
		class CScopeProfiler
		{
		public:
			//!	コンストラクタ
			//!	@param iIndex [in] インデックス
			INLINE CScopeProfiler(s32 iIndex)
				: m_iIndex(iIndex)
			{
				CProfiler::GetInstance()->Begin(iIndex);
			}
			//!	デストラクタ
			INLINE ~CScopeProfiler(void)
			{
				CProfiler::GetInstance()->End(m_iIndex);
			}
		private:
			s32	m_iIndex;	//!< インデックス
		};

		//======================================================================
		//!	スコープ内処理時間計測
		//======================================================================
		class CScopeCycleTime
		{
		public:
			//!	コンストラクタ
			//!	@param dTime [out] 計測結果
			INLINE CScopeCycleTime(f64& dTime)
				: m_dTime(dTime)
			{
				m_dStart = CProfiler::GetInstance()->GetTime();
			}
			//!	デストラクタ
			INLINE ~CScopeCycleTime(void)
			{
				m_dTime += CProfiler::GetInstance()->GetTime() - m_dStart;
			}
		private:
			f64&	m_dTime;	//!< 計測結果
			f64		m_dStart;	//!< 計測開始時間
		};
	}
}

#define	PROFILER				kgl::Profiler::CProfiler::GetInstance()

//	計測用マクロ
#define BEGIN_PROFILE(index)	PROFILER->Begin(index)
#define END_PROFILE(index)		PROFILER->End(index)

#define	SCOPE_PROFILE(index)	kgl::Profiler::CScopeProfiler ScopeProfiler(index)
#define	SCOPE_CYCLE_TIME(time)	kgl::Profiler::CScopeCycleTime ScopeCycleTime(time)

#else // ~#if KGL_DEBUG_PROFILE

#define	PROFILER

#define BEGIN_PROFILE(...)		((void)(0))
#define END_PROFILE(...)		((void)(0))

#define	SCOPE_PROFILE(...)		((void)(0))
#define	SCOPE_CYCLE_TIME(...)	((void)(0))

#endif	// ~#if KGL_DEBUG_PROFILE

#endif	// __KGL_PROFILER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
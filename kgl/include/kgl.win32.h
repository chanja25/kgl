//----------------------------------------------------------------------
//!
//! @file	kgl.win32.h
//! @brief	WIN32
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_WIN32_H__
#define	__KGL_WIN32_H__

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#if	defined(_WIN32_WINNT)
#undef  _WIN32_WINNT
#define _WIN32_WINNT 0x0502
#endif	// ~#if	defined(_WIN32_WINNT)

#define WIN32_LEAN_AND_MEAN

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <ObjBase.h>	//	interfaceマクロを使用する為
#include <new>			//	placement newを使用する為

//	警告対策
#pragma	warning(disable:4996)	//	kgStrcpy等の対応
#pragma	warning(disable:4201)	//	無名structの対応
#pragma	warning(disable:4127)	//	if(true)の対応

#endif	// __KGL_WIN32_H__
//======================================================================
//	END OF FILE
//======================================================================
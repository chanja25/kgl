//----------------------------------------------------------------------
//!
//!	@file	kgl.function.h
//!	@brief	関数
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FUNCTION_H__
#define	__KGL_FUNCTION_H__

namespace kgl
{
	namespace Window
	{
		interface IWindow;
		interface IWindowEvent;
	}

	//----------------------------------------------------------------------
	//	関数型
	//----------------------------------------------------------------------
	//!	ウィンドウイベント用
	typedef void (*FWindowEvent)(Window::IWindow* pWindow, Window::IWindowEvent* pEvent, s32 iEvent);
	//!	ウィンドウイベント用
	typedef void (*FWindowCommonEvent)(Window::IWindow* pWindow, Window::IWindowEvent* pEvent, s32 iMsg, s32 wParam, s32 lParam);
	//!	ファイルロードのコールバック用
	typedef void (THREADFUNC *FLoadCallback)(const void* pData, void* pArg, u32 uiSize, const c8* pPath, s32 iIndex);

	//!	スレッド用
	typedef u32 (THREADFUNC *FThreadFunction)(void* pArg);
	//!	スレッドタスク用
	typedef u32 (*FTaskFunction)(void* pArg, s32 iThreadID);
	//!	DLL関数呼び出し用
	typedef s32 (*FDllFunction)(void);
}

#endif	// __KGL_FUNCTION_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.base.h
//!	@brief	ベースクラス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_BASE_H__
#define	__KGL_BASE_H__

//----------------------------------------------------------------------
// Kisaragi Game Library
//----------------------------------------------------------------------
namespace kgl
{
	//======================================================================
	//!	IKGLBase
	//======================================================================
	interface IKGLBase
	{
	public:
		//!	コンストラクタ
		IKGLBase(void) : m_iReferenceCounter(0) {}
		//!	デストラクタ
		virtual ~IKGLBase(void) {}

		//!	参照数の追加
		//!	@return 参照数
		virtual s32 AddRef(void)
		{
			return kgAtomicIncrement(m_iReferenceCounter);
		}
		//!	解放
		//!	@return 参照数
		virtual s32 Release(void)
		{
			s32 iRef = kgAtomicDecrement(m_iReferenceCounter);
			if( iRef == 0 )
			{
				delete this;
				return 0;
			}
			return m_iReferenceCounter;
		}
		//!	参照数の取得
		//!	@return 参照数
		virtual s32 ReferenceCount(void) const
		{
			return m_iReferenceCounter;
		}

	private:
		s32 m_iReferenceCounter;	//!< 参照カウンタ
	};
}

#endif	// __KGL_BASE_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.version.h
//!	@brief	バージョン定義
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_VERSION_H__
#define	__KGL_VERSION_H__


//----------------------------------------------------------------------
//	ライブラリバージョン
//----------------------------------------------------------------------
#define	KGL_MAJOR_VERSION	0
#define	KGL_MINOR_VERSION	0


#endif	// __KGL_VERSION_H__
//======================================================================
//	END OF FILE
//======================================================================
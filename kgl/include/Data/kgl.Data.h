//----------------------------------------------------------------------
//!
//!	@file	kgl.Data.h
//!	@brief	データ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DATA_H__
#define	__KGL_DATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.typedef.h"
#include "../Math/kgl.Math.h"
#include "../Object/kgl.Object.h"

//	Data include
#include "Render/kgl.TextureData.h"
#include "Render/kgl.FontData.h"
#include "kgl.LayoutData.h"
#include "xml/kgl.xml.h"
#include "text/kgl.Lexser.h"

#endif	// __KGL_DATA_H__
//=======================================================================
//	END OF FILE
//=======================================================================
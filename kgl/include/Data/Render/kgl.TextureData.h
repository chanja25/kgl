//----------------------------------------------------------------------
//!
//!	@file	kgl.TextureData.h
//!	@brief	テクスチャーデータ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TEXTURE_DATA_H__
#define	__KGL_TEXTURE_DATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.typedef.h"

namespace kgl
{
	namespace Texture
	{
		//!	テクスチャータイプ
		namespace EType
		{
			const s32 Normal	= 0;	//!< 2Dテクスチャー
			const s32 Cube		= 1;	//!< キューブテクスチャー
			const s32 Volume	= 2;	//!< ボリュームテクスチャー
		}
		//!	テクスチャーフォーマット
		namespace EFormat
		{
			const s32 Unkown		= -1;	//!< Unknown
			const s32 R8G8B8		= 0;	//!< R8G8B8(24bit)
			const s32 A8R8G8B8		= 1;	//!< A8R8G8B8(32bit)
			const s32 A16R16G16B16	= 2;	//!< A16R16G16B16(64bit)
			const s32 A16R16G16B16F	= 3;	//!< A16R16G16B16F(64bit)
			const s32 R5G6B5		= 4;	//!< R5G6B5(16bit)
			const s32 A1R5G5B5		= 5;	//!< A1R5G5B5(16bit)
			const s32 A4R4G4B4		= 6;	//!< A4R4G4B4(16bit)
			const s32 A1			= 7;	//!< A8(8bit)
			const s32 A8			= 8;	//!< A8(8bit)
			const s32 L8			= 9;	//!< A8(8bit)
			const s32 A8L8			= 10;	//!< A8L8(16bit)
			const s32 A4L4			= 11;	//!< A4L4(8bit)
			const s32 R16F			= 12;	//!< R16F(16bit)
			const s32 R32F			= 13;	//!< R32F(32bit)
			const s32 D16			= 14;	//!< D16(16bit)
			const s32 D32			= 15;	//!< D32(32bit)
			const s32 D15S1			= 16;	//!< D15S1(16bit)
			const s32 D24S8			= 17;	//!< D24S8(32bit)

			const s32 DXT1			= 100;	//!< DXT1
			const s32 DXT2			= 101;	//!< DXT2
			const s32 DXT3			= 102;	//!< DXT3
			const s32 DXT4			= 103;	//!< DXT4
			const s32 DXT5			= 104;	//!< DXT5
		}
		//======================================================================
		//!	kgl用テクスチャーデータヘッダー
		//======================================================================
		struct kgTextureHeader
		{
			s32	iID;		//!< FOURCC('k', 't', 'e', 'x')
			s32 iVersion;	//!< バージョン
			s32	iCount;		//!< テクスチャー数
			s32	iOffset;	//!< テクスチャー情報へのオフセット(データの先頭アドレスを基準とする)
		};
		//======================================================================
		//!	kgl用テクスチャー情報
		//======================================================================
		struct kgTextureInfo
		{
			s32	iType;			//!< タイプ(EType参照)
			s32	iFormat;		//!< フォーマット(EFormat参照)
			u32	uiWidth;		//!< 横幅
			u32	uiHeight;		//!< 縦幅
			s32	iMipLevels;		//!< ミップマップ数
			s32	iSize;			//!< データサイズ
			s32	iOffset;		//!< データへのオフセット(データの先頭アドレスを基準とする)
			s32	iReserved;
		};
	}
}

#endif	// __KGL_TEXTURE_DATA_H__
//=======================================================================
//	END OF FILE
//=======================================================================
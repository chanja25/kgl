//----------------------------------------------------------------------
//!
//!	@file	kgl.FontData.h
//!	@brief	テクスチャーフォント関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FONT_DATA_H__
#define	__KGL_FONT_DATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.typedef.h"

namespace kgl
{
	namespace Font
	{
		//!	テクスチャーフォーマット
		namespace EFormat
		{
			const s32 Unkown		= -1;	//!< Unknown
			const s32 Alpha			= 0;	//!< Alpha
			const s32 AlphaOutline	= 1;	//!< Alpha(アウトライン付き)
			const s32 AlphaShadow	= 2;	//!< Alpha(影付き)
			const s32 DistanceField	= 3;	//!< ディスタンスフィールド
		}
		//!	文字コード
		namespace ECharSet
		{
			const s32 Unkown		= -1;	//!< Unknown
			const s32 ShiftJIS		= 0;	//!< Shift-JIS
			const s32 UTF16			= 1;	//!< UTF-16
		}
		//======================================================================
		//!	ヘッダー
		//======================================================================
		struct kgFontHeader
		{
			s32	iID;			//!< FOURCC('k', 't', 'f', ' ')
			s32 iVersion;		//!< バージョン
			s32	iCharSet;		//!< 文字コードの種類
			s32	iCharCount;		//!< フォント数
			s32	iCharOffset;	//!< 文字情報へのオフセット
			s32	iConvertOffset;	//!< コンバート情報へのオフセット(0の場合は無し)
			s32 iCharPaddingX;	//!< X軸の余白
			s32 iCharPaddingY;	//!< Y軸の余白
			s32	iFormat;		//!< フォーマット(EFormat参照)
			s32	iTextureCount;	//!< テクスチャー数
			s32	iTextureOffset;	//!< テクスチャーへのオフセット
		};
		//======================================================================
		//!	文字情報
		//======================================================================
		struct kgFontCharacter
		{
			c16	sChar;			//!< 文字コード
			s16	sTextureIndex;	//!< テクスチャーのインデックス
			u16	usU;			//!< U値
			u16	usV;			//!< V値
			u16	usSizeU;		//!< 横幅
			u16	usSizeV;		//!< 縦幅
		};
		//======================================================================
		//!	文字のコンバート情報(Shift-JISからUTF-16へ)
		//======================================================================
		struct kgFontConvert
		{
			c16	sChar;	//!< 文字コード(Shift-JIS)
			s16	sIndex;	//!< 文字情報のインデックス
		};
		//======================================================================
		//!	テクスチャー情報
		//======================================================================
		struct kgFontTexture
		{
			s32	iWidth;		//!< 横幅
			s32	iHeight;	//!< 縦幅
			s32	iSize;		//!< データサイズ
			s32	iOffset;	//!< データへのオフセット(データの先頭アドレスを基準とする)
		};
	}
}

#endif	// __KGL_FONT_DATA_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.LayoutData.h
//!	@brief	レイアウトデータ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_LAYOUT_DATA_H__
#define	__KGL_LAYOUT_DATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.typedef.h"
#include "../Math/kgl.Math.h"
#include "../Object/kgl.Object.h"

namespace kgl
{
	namespace Layout
	{
		//!	レイアウト用コールバック
		typedef	s32 (* FCallback)(void*);

		//!	描画モード
		namespace ERenderMode
		{
			const u8 Add		= 0;	//!< 加算
			const u8 Subtrace	= 1;	//!< 減算
			const u8 AlphaMask	= 2;	//!< アルファマスク
			const u8 Alpha		= 3;	//!< アルファ
		}
		//!	補間モード
		namespace EInterpolation
		{
			const u16 Const		= 0;	//!< 定数
			const u16 Linear	= 1;	//!< リニア補間
			const u16 Hermite	= 2;	//!< エルミート補間
		}

		//======================================================================
		//!	kgl用レイアウトデータヘッダー
		//======================================================================
		struct kgLayoutHeader
		{
			s32	iID;				//!< FOURCC('k, 'l', 'y', 't')
			s32	iVersion;			//!< バージョン
			f32	fWidth;				//!< X軸の解像度
			f32	fHeight;			//!< Y軸の解像度
			u16	usTextureCount;		//!< 使用するテクスチャーの数
			u16	usAnimationCount;	//!< アニメーション数
			u16	usNullCount;		//!< Nullの数
			u16	usPrimitiveCount;	//!< 描画情報数
			u32	uiTextureOffset;	//!< テクスチャー情報へのオフセット
			u32	uiAnimationIffset;	//!< アニメーション情報へのオフセット
			u32	uiLayoutOffset;		//!< Null情報へのオフセット
			u32	uiPrimitiveOffset;	//!< 描画情報へのオフセット
		};
		//======================================================================
		//!	テクスチャー情報
		//======================================================================
		struct kgTextureInfo
		{
			c8*	pPath;		//!< パス
		};
		//======================================================================
		//!	アニメーション情報
		//======================================================================
		struct kgAnimationInfo
		{
			u16	usNullIndex;		//!< nullのインデックス
			u16	usChainAnimation;	//!< 
			u16	usMoveCount;		//!< 移動アニメの数
			u16	usScaleCount;		//!< スケールアニメ数
			u16	usRotationCount;	//!< 回転アニメ数
			u16	usColorCount;		//!< カラーアニメ数
			u16	usUVCount;			//!< UVアニメ数
			u16	usEventCount;		//!< イベント数
			u32	uiMoveOffset;		//!< 移動アニメへのオフセット
			u32	uiScaleOffset;		//!< スケールアニメへのオフセット
			u32	uiRotationOffset;	//!< 回転アニメへのオフセット
			u32	uiColorOffset;		//!< カラーアニメへのオフセット
			u32	uiUVOffset;			//!< UVアニメへのオフセット
			u32	uiEventOffset;		//!< イベントへのオフセット
		};
		//======================================================================
		//!	null情報
		//======================================================================
		struct kgNullInfo
		{
			c8			cTag[32];			//!< タグ名
			u16			usParent;			//!< 親のインデックス(-1の場合はRoot)
			u16			usPrimitiveIndex;	//!< 描画情報へのオフセット(-1の場合はnullのみ)
			f32			fRotation;			//!< 回転
			Vector2		vLocation;			//!< 位置
			Vector2		vScale;				//!< スケール
		};
		//======================================================================
		//!	描画情報
		//======================================================================
		struct kgPrimitiveInfo
		{
			u8		ucLayer;		//!< レイヤー
			u8		ucRenderMode;	//!< 描画モード(ERenderMode参照)
			u16		usTextureIndex;	//!< 使用するテクスチャーのインデックス
			f32		fRotation;		//!< 回転
			Vector2	vOrigin;		//!< 原点
			Vector2	vSize;			//!< スケール
			Vector4	vUv;			//!< UV
			Vector4	vColor;			//!< カラー
		};
		//======================================================================
		//!	位置情報
		//======================================================================
		struct LocationInfo
		{
			u16		usKeyframe;	//!< キーフレーム
			u16		usInterp;	//!< 補間方法(EInterpolation参照)
			s32		reserved;
			Vector2	vLocation;	//!< 位置
			Vector2	vInterp;	//!< 曲線補間用
		};
		//======================================================================
		//!	スケール情報
		//======================================================================
		struct ScaleInfo
		{
			u16		usKeyframe;	//!< キーフレーム
			u16		usInterp;	//!< 補間方法
			s32		reserved;
			Vector2	vScale;		//!< スケール
		};
		//======================================================================
		//!	回転情報
		//======================================================================
		struct RotationInfo
		{
			u16	usKeyframe;		//!< キーフレーム
			u16	usInterp;		//!< 補間方法
			f32	fRotation;		//!< 回転
		};
		//======================================================================
		//!	カラー情報
		//======================================================================
		struct UvInfo
		{
			u16		usKeyframe;		//!< キーフレーム
			u16		usInterp;		//!< 補間方法
			s32		reserved[3];
			Vector4	vUV;			//!< UV
		};
		//======================================================================
		//!	カラー情報
		//======================================================================
		struct ColorInfo
		{
			u16		usKeyframe;		//!< キーフレーム
			u16		usInterp;		//!< 補間方法
			s32		reserved[3];
			Vector4	vColor;			//!< カラー
		};
		//======================================================================
		//!	イベント情報
		//======================================================================
		struct EventInfo
		{
			u16			usKeyframe;		//!< キーフレーム
			u16			reserved;
			BITFIELD	bParam:1;		//!< パラメータの変更
			BITFIELD	bCallback:1;	//!< コールバックの呼び出し
			BITFIELD	bVisible:1;		//!< 表示
			BITFIELD	bHide:1;		//!< 非表示
			s32			iParam[3];		//!< パラメータ
		};
	}
}

#endif	// __KGL_LAYOUT_DATA_H__
//=======================================================================
//	END OF FILE
//=======================================================================
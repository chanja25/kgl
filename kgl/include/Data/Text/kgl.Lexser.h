//---------------------------------------------------------------------------
//!
//!	@file	kgl.Lexser.h
//!	@brief	字句解析
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
#ifndef	__KGL_LEXSER_H__
#define __KGL_LEXSER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Text
	{
		//!	トークンの最大文字数
		const s32 TokenLenMax	= 512;

		//!	文字のタイプ
		namespace EType
		{
			const s32 Unkown		=-1;	//!< 不明なトークン
			const s32 AlNum			= 1;	//!< 英数字
			const s32 Number		= 2;	//!< 数字
			const s32 AlNumString	= 3;	//!< 英数字文字列
			const s32 String		= 4;	//!< 文字列
			const s32 Operator		= 5;	//!< オペレータ
			const s32 EscapeSeq		= 6;	//!< 特殊文字
		}

		//!	トークン情報
		struct Token
		{
			s32			iType;		//!< タイプ
			u32			uiLine;		//!< 行数
			c8*			pToken;		//!< トークン
			Token*		pNext;		//!< 次のトークン
			BITFIELD	bError: 1;	//!< エラーフラグ
		};

		//===========================================================================
		//!	字句解析クラス
		//===========================================================================
		class CLexer
		{
		public:
			//!	コンストラクタ
			CLexer(void);
			//!	デストラクタ
			~CLexer(void);

		public:
			//!	読み込み
			//!	@param	pFileName	[in] 読み込むファイル名
			b8 LoadText(const c8* pFileName);
			//!	テキスト設定
			//!	@param	pText	[in] テキストデータ
			//!	@param	uiSize	[in] データサイズ
			b8 SetText(const c8* pText, u32 uiSize);

			//!	解放
			void Release(void);

		public:
			//!	トークンを全て解析
			//!	@return ルートのトークン
			const Token* AnalyzeAll(void);
			//!	次のトークンを解析
			//!	@return 解析結果
			const Token* AnalyzeToken(void);

		public:
			//!	ルートのトークン取得
			//!	@return ルートのトークン
			FORCEINLINE const Token* Root(void) { return m_pRoot; }
			//!	エラー判定
			//!	@return エラーか？
			FORCEINLINE b8 IsError(void) { return m_bError; }

		private:
			//!	トークンの取得
			//!	@return トークン
			Token* NextToken(void);
			//!	トークンの取得(英数字)
			//!	@return トークン
			Token* NextTokenAlnum(void);
			//!	トークンの取得(数字)
			//!	@return トークン
			Token* NextTokenNumber(void);
			//!	トークンの取得(文字列)
			//!	@return トークン
			Token* NextTokenString(void);
			//!	トークンの取得(Unknown)
			//!	@return トークン
			Token* NextTokenAlnumString(void);
			//!	トークンの取得(オペレータ)
			//!	@return トークン
			Token* NextTokenOperator(void);
			//!	トークンの取得(特殊文字)
			//!	@return トークン
			Token* NextTokenEscapeSeq(void);
			//!	トークンの生成
			//!	@param iType	[in] タイプ
			//!	@param pToken	[in] 文字列
			//!	@param uiLength	[in] 文字数
			//!	@param bError	[in] エラーか？
			//!	@return トークン
			Token* CreateToken(s32 iType, const c8* pToken, u32 uiLength, b8 bError);
			//!	文字タイプの取得
			//!	@param c [in] 文字
			//!	@return 文字タイプ
			s32 CharType(c8 c);
			//!	英数字か？
			//!	@param c [in] 文字
			//!	@return 結果
			b8 IsAlnum(c8 c);
			//!	数字か？
			//!	@param c [in] 文字
			//!	@return 結果
			b8 IsNumber(c8 c);
			//!	オペレータか？
			//!	@param c [in] 文字
			//!	@return 結果
			b8 IsOperator(c8 c);
			//!	特殊文字か？
			//!	@param c [in] 文字
			//!	@return 結果
			b8 IsEscapeSeq(c8 c);

		private:
			c8*			m_pText;				//!< テキストデータ
			u32			m_uiSize;				//!< データサイズ
			u32			m_uiIndex;				//!< 現在の位置
			u32			m_uiLine;				//!< 現在の行
			u32			m_uiPrevIndex;			//!< 前回の位置
			u32			m_uiPrevLine;			//!< 前回の行
			c8			m_cToken[TokenLenMax];	//!< 解析用バッファ
			bool		m_bError;				//!< 解析が完了したか？
			Token*		m_pRoot;				//!< ルートのトークン情報
			Token*		m_pEnd;					//!< 末尾のトークン情報
		};
	}
}


#endif	//~#if __KGL_LEXSER_H__
//============================================================================
//	END OF FILE
//============================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.xml.define.h
//!	@brief	XML関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_XML_DEFINE_H__
#define	__KGL_XML_DEFINE_H__


namespace kgl
{
	namespace Xml
	{
		using namespace algorithm;

		//======================================================================
		//!	Attribute
		//======================================================================
		class CAttribute
		{
		public:
			//!	コンストラクタ
			//!	@param inTag	[in] タグ
			//!	@param inValue	[in] 値
			CAttribute(string strTag, string strValue)
				: m_strTag(strTag), m_strValue(strValue)
			{}
			//!	タグの取得
			//!	@return タグ
			INLINE const string& GetTag(void) const { return m_strTag; }
			//!	値の取得
			//!	@return 値
			INLINE const string& GetValue(void) const { return m_strValue; }

		private:
			string	m_strTag;	//!< タグ
			string	m_strValue;	//!< 値
		};

		//======================================================================
		//!	Node
		//======================================================================
		class CNode
		{
			friend class CXml;

		private:
			//!	コンストラクタ
			CNode(void);
			//!	コンストラクタ
			CNode(const string& strTag);
			//!	コンストラクタ
			CNode(const CNode& ref) { (void)ref; }
			//!	デストラクタ
			~CNode();

		public:
			//!	コンテントの設定
			//!	@param strContent	[in] 内容
			CNode* SetContent(const string& strContent);
			//!	属性の追加
			//!	@param Tag		[in] タグ
			//!	@param Value	[in] 値
			CNode* AddAttribute(const string& strTag, const string& strValue);
			//!	属性の削除
			//!	@param Tag		[in] タグ
			CNode* DeleteAttribute(const string& strTag);
			//!	子ノードの追加
			//!	@param pNode	[in] ノード
			CNode* AddChild(const string& strTag);
			//!	ノードの削除
			void Delete(void);

		public:
			//!	属性の有無を取得
			//!	@param Tag	[in] タグ
			//!	@return 有無
			b8 HasAttribute(const string& strTag) const;
			//!	属性の検索
			//!	@param Tag	[in] タグ
			//!	@return ノード
			const string& FindAttribute(const string& strTag) const;
			//!	子ノードの検索
			//!	@param Tag	[in] タグ
			//!	@return ノード
			const CNode* FindChildNode(const string& strTag) const;
			//!	子ノードの検索
			//!	@param Tag	[in] タグ
			//!	@return ノード
			CNode* FindChildNode(const string& strTag);

		public:
			//!	タグの取得
			//!	@return タグ
			INLINE const string& GetTag(void) const { return m_strTag; }
			//!	コンテントか判定
			//!	@return コンテントか?
			INLINE b8 IsContent(void) const { return !!m_bContent; }
			//!	コンテントの取得
			//!	@return コンテント
			INLINE const string& GetContent(void) const { return m_strContent; }
			//!	次のノードの取得
			//!	@return ノード
			INLINE CNode* GetParent(void) { return m_pParent; }
			//!	次のノードの取得
			//!	@return ノード
			INLINE const CNode* GetParent(void) const { return m_pParent; }
			//!	次のノードの取得
			//!	@return ノード
			INLINE CNode* GetNext(void) { return m_pNext; }
			//!	次のノードの取得
			//!	@return ノード
			INLINE const CNode* GetNext(void) const { return m_pNext; }
			//!	子ノードの取得
			//!	@return ノード
			INLINE vector<CNode*>& GetChildren(void) { return m_pChildren; }
			//!	子ノードの取得
			//!	@return ノード
			INLINE const vector<CNode*>& GetChildren(void) const { return m_pChildren; }
			//!	先頭の子ノードの取得
			//!	@return ノード
			INLINE CNode* GetFirstChild(void) { return m_pChildren.empty()? null: m_pChildren[0]; }
			//!	先頭の子ノードの取得
			//!	@return ノード
			INLINE const CNode* GetFirstChild(void) const { return m_pChildren.empty()? null: m_pChildren[0]; }
			//!	属性の取得
			//!	@return 属性
			INLINE const vector<CAttribute>& GetAttributes(void) const { return m_Attributes; }

		private:
			string				m_strTag;		//!< タグ
			string				m_strContent;	//!< 内容
			CNode*				m_pParent;		//!< 親ノード
			vector<CNode*>		m_pChildren;	//!< 子ノード
			vector<CAttribute>	m_Attributes;	//!< 属性
			CNode*				m_pNext;		//!< 次のノード
			bool				m_bContent;		//!< コンテントがあるか?
		};
	}
}

#endif	// __KGL_XML_DEFINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.xml.h
//!	@brief	XML関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_XML_H__
#define	__KGL_XML_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.xml.node.h"
#include "File/kgl.FileReader.h"


namespace kgl
{
	namespace Xml
	{
		//======================================================================
		//!	XML管理
		//======================================================================
		interface IXml
			: public File::IExtraObject
		{
		public:
			//!	ファイルからXMLノードの作成
			//!	@param pPath	[in] パス
			//!	@return 結果
			virtual b8 Create(const c8* pPath) = 0;
			//!	バッファからXMLノードの作成
			//!	@param pBuffer	[in] バッファ
			//!	@param uiSize	[in] バッファサイズ
			//!	@return 結果
			virtual b8 Create(const void* pBuffer, u32 uiSize) = 0;
			//!	ルートノードの作成
			//!	@param strTag	[in] ルートタグ
			//!	@return 結果
			virtual CNode* CreateRoot(string strTag) = 0;

			//!	ノードの削除
			virtual void Delete(void) = 0;

			//!	XMLデータの保存
			//!	@param pPath	[in] パス
			//!	@return 結果
			virtual b8 Save(const c8* pPath) = 0;

		public:
			//!	ルートノードの取得
			//!	@return ルートノード
			virtual CNode* GetRoot(void) = 0;
			//!	エラーの有無を取得
			//!	@return 結果
			virtual b8 IsFailed(void) = 0;
			//!	エラーメッセージの取得
			//!	@return エラー内容
			virtual const c8* GetError(void) = 0;
		};

		//!	XML管理の生成
		//!	@param ppXml [out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IXml** ppXml);
	}
}

#endif	// __KGL_XML_H__
//=======================================================================
//	END OF FILE
//=======================================================================
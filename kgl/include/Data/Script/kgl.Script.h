//---------------------------------------------------------------------------
//!
//!	@file	kgl.Script.h
//!	@brief	スクリプト関連
//!
//!	@author S.Kisaragi
//---------------------------------------------------------------------------
#ifndef	__KGL_SCRIPT_H__
#define __KGL_SCRIPT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Script
	{
		using namespace algorithm;

		//!	値のタイプ
		namespace EVariable
		{
			const s32 Unkown		=-1;	//!< 不明なタイプ
			const s32 Function		= 0;	//!< 関数
			const s32 Pointer		= 1;	//!< 文字列
			const s32 String		= 2;	//!< 文字列
			const s32 Boolean		= 3;	//!< Bool値
			const s32 ValueS8		= 4;	//!< 符号あり  8bit整数
			const s32 ValueU8		= 5;	//!< 符号なし  8bit整数
			const s32 ValueS16		= 6;	//!< 符号あり 16bit整数
			const s32 ValueU16		= 7;	//!< 符号なし 16bit整数
			const s32 ValueS32		= 8;	//!< 符号あり 32bit整数
			const s32 ValueU32		= 9;	//!< 符号なし 32bit整数
			const s32 ValueS64		= 10;	//!< 符号あり 64bit整数
			const s32 ValueU64		= 11;	//!< 符号なし 64bit整数
			const s32 ValueF32		= 12;	//!< 単精度浮動少数点数
			const s32 ValueF64		= 13;	//!< 倍精度浮動少数点数

			const s32 CustomValue	= 100;	//!< ここ以降はスクリプト内で使用される
		}

		//!	値情報
		struct ValueInfo
		{
			string				strLabel;		//!< ラベル
			s32					iType;			//!< 変数のタイプ
			s32					iCount;			//!< 値の数(配列)
			void*				pValue;			//!< 値
			s32*				pRefCount;		//!< 参照カウンタ
			BITFIELD			bConst:1;		//!< 変更不可
			BITFIELD			bArray:1;		//!< 配列
			BITFIELD			bDynamic:1;		//!< 動的配列
			BITFIELD			bReference:1;	//!< 参照
			BITFIELD			bStruct:1;		//!< 構造体
		};
		//!	スタック情報
		struct StackList
		{
			vector<ValueInfo>	Value;	//!< 値
			StackList*			pPrev;	//!< 次の値
			StackList*			pNext;	//!< 前の値
		};
		//!	スタック情報
		struct MacroInfo
		{
			string	strDst;	//!< 変換する文字列
			string	strSrc;	//!< 変換後の文字列
		};

		//===========================================================================
		//!	スクリプトインターフェイス
		//===========================================================================
		class IScript
			: public IKGLBase
		{
		public:
			//!	スクリプトの生成
			virtual b8 Create(const c8* pPath) = 0;

		public:
			//!	関数呼び出し
			virtual ValueInfo* CallFunction(string strFunc, vector<string> strArg) = 0;
			//!	値の解放
			virtual void ReleaseValue(ValueInfo* pValue) = 0;
			//!	現在のスタックを取得
			virtual const StackList& GetStack(void) = 0;
			//!	全体のスタックを取得
			virtual const vector<StackList>& GetStackAll(void) = 0;
			//!	グローバル情報を取得
			virtual const vector<ValueInfo>& GetGlobalStack(void) = 0;
		};

		//!	スクリプトの生成
		//!	@param ppScript	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IScript** ppScript);
	}
}


#endif	//~#if __KGL_SCRIPT_H__
//============================================================================
//	END OF FILE
//============================================================================
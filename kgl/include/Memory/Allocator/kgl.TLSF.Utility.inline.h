//----------------------------------------------------------------------
//!
//!	@file	kgl.TLSF.Utility.inline.h
//!	@brief	TLSF
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TLSF_UTILITY_INLINE_H__
#define	__KGL_TLSF_UTILITY_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace tlsf
	{
		//	基本的のブロックサイズ
		const size_t BlockHeaderSize	= sizeof(BoundaryBlockHeader);

		//----------------------------------------------------------------------
		//	第二カテゴリーの取得
		//----------------------------------------------------------------------
		FORCEINLINE u32 GetSecondLevelIndex(size_t size, u8 MSB, u8 N)
		{
			if( MSB < N ) N = MSB;

			return ((size & ((1 << MSB) - 1)) >> (MSB - N));
		}

		//----------------------------------------------------------------------
		//	境界ブロックの作成
		//----------------------------------------------------------------------
		FORCEINLINE BoundaryBlockHeader* CreateBoundaryBlock(BoundaryBlockHeader* block, size_t size, size_t offset, BoundaryBlockHeader* prev)
		{
#if USE_TLSF_MEMORY_CHECK
			block->id1			= FOURCC('n', 'o', 'n', 'e');
			block->id2			= FOURCC('n', 'o', 'n', 'e');
#endif // ~#if USE_TLSF_MEMORY_CHECK
			block->prev			= prev;
			block->use			= 0;
			block->size			= size;
			block->offset		= offset;
			block->free_next	= null;
			block->free_prev	= null;

			return block;
		}

		//----------------------------------------------------------------------
		//	ブロックの管理メモリ取得
		//----------------------------------------------------------------------
		FORCEINLINE void* GetMemory(BoundaryBlockHeader* block)
		{
			return (u8*)block + block->offset;
		}
		//----------------------------------------------------------------------
		//	管理メモリからブロック取得
		//----------------------------------------------------------------------
		FORCEINLINE BoundaryBlockHeader* GetBoundaryBlock(void* ptr)
		{
			size_t offset = *((size_t*)ptr-1);
			return (BoundaryBlockHeader*)((u8*)ptr - offset);
		}

		//----------------------------------------------------------------------
		//	次のブロック取得
		//----------------------------------------------------------------------
		FORCEINLINE BoundaryBlockHeader* NextBlock(BoundaryBlockHeader* block)
		{
			return (BoundaryBlockHeader*)((u8*)block + block->size + block->offset);
		}
		//----------------------------------------------------------------------
		//	前のブロック取得
		//----------------------------------------------------------------------
		FORCEINLINE BoundaryBlockHeader* PrevBlock(BoundaryBlockHeader* block)
		{
			return block->prev;
		}

		//----------------------------------------------------------------------
		//	ブロックのマージ
		//----------------------------------------------------------------------
		FORCEINLINE void MargeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block)
		{
			BoundaryBlockHeader* next = NextBlock(block);

			block->size = block->size + next->size + next->offset;
			LinkFreeBoundaryBlock(header, block);

			next = NextBlock(block);
			if( next < header->end ) next->prev = block;
		}
		//----------------------------------------------------------------------
		//	ブロックのオフセット算出
		//----------------------------------------------------------------------
		FORCEINLINE size_t CalcBoundaryBlockOffset(BoundaryBlockHeader* block, size_t align)
		{
			const size_t ptr = (size_t)block + BlockHeaderSize;
			const size_t pad = Align(ptr, align) - ptr;
			return BlockHeaderSize + pad;
		}
		//----------------------------------------------------------------------
		//	ブロックの分割
		//----------------------------------------------------------------------
		FORCEINLINE BoundaryBlockHeader* SplitBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block, size_t size, size_t align)
		{
			BreakLinkFreeBoundaryBlock(header, block);

			const size_t offset = CalcBoundaryBlockOffset(block, align);
			//	データの手前にブロックへのオフセットを入れておく
			size_t* blocksize = (size_t*)((u8*)block + offset - sizeof(size_t));
			*blocksize		= offset;
			block->offset	= offset;

			if( block->size < size + block->offset )
			{
				block->size -= block->offset - BlockHeaderSize;
				return null;
			}
			register size_t newSize = block->size - size - block->offset;
			block->size = size;

			BoundaryBlockHeader* newBlock = NextBlock(block);
			newBlock = CreateBoundaryBlock(newBlock, newSize, BlockHeaderSize, block);
			LinkFreeBoundaryBlock(header, newBlock);

			BoundaryBlockHeader* newNext = NextBlock(newBlock);
			if( newNext < header->end ) newNext->prev = newBlock;

			return newBlock;
		}

		//----------------------------------------------------------------------
		//	フリーリストからブロック取得
		//----------------------------------------------------------------------
		FORCEINLINE BoundaryBlockHeader* FindFreeBoundaryBlock(TLSFHeader* header, size_t size, size_t align)
		{
			u32 msb	= Math::MSB((u32)size);
			u32	idx	= GetSecondLevelIndex(size, (u8)msb);

			BoundaryBlockHeader* block;
			for( ; msb < FREE_LIST_MAX; msb++ )
			{
				if( header->free_block_count[msb] > 0 )
				{
					for( ; idx < FREE_SECOND_LIST_MAX; idx++ )
					{
						block = header->free_block[msb][idx];
						while( block )
						{
							size_t ptr		= (size_t)block + BlockHeaderSize;
							size_t padding	= Align(ptr, align) - ptr;
							if( block->size >= size + padding )
							{
								return block;
							}
							block = block->free_next;
						}
					}
				}
				idx = 0;
			}
			return null;
		}
		//----------------------------------------------------------------------
		//	フリーリストにブロックを追加
		//----------------------------------------------------------------------
		FORCEINLINE void LinkFreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block)
		{ 
			u32 msb = Math::MSB((u32)block->size);
			u32	idx	= GetSecondLevelIndex(block->size, (u8)msb);
			if( header->free_block[msb][idx] == null )
			{
				header->free_block[msb][idx] = block;
				block->free_next = block->free_prev = null;
			}
			else
			{
				BoundaryBlockHeader* next = header->free_block[msb][idx];
				header->free_block[msb][idx] = block;

				block->free_next = next;
				block->free_prev = null;
				next->free_prev  = block;
			}

			header->alloc -= block->size;
			header->free_block_count[msb] ++;
		}
		//----------------------------------------------------------------------
		//	フリーリストからブロックを外す
		//----------------------------------------------------------------------
		FORCEINLINE void BreakLinkFreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block)
		{
			u32 msb = Math::MSB((u32)block->size);
			u32	idx	= GetSecondLevelIndex(block->size, (u8)msb);

			if( block == header->free_block[msb][idx] )
			{
				header->free_block[msb][idx] = block->free_next;
			}
			if( block->free_next )
			{
				block->free_next->free_prev = block->free_prev;
			}
			if( block->free_prev )
			{
				block->free_prev->free_next = block->free_next;
			}
			block->free_next = null;
			block->free_prev = null;

			kgAssert(header->free_block_count[msb] > 0, "存在しないリストから削除されました");
			header->alloc += block->size;
			header->free_block_count[msb] --;
		}
		//----------------------------------------------------------------------
		//	ブロックを解放
		//----------------------------------------------------------------------
		FORCEINLINE void FreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block)
		{
			BoundaryBlockHeader* next = NextBlock(block);
			b8 bMarge = false;
			if( next < header->end )
			{
				if( CheckBoundaryBlock(next) == false )
				{
					BreakLinkFreeBoundaryBlock(header, next);
					MargeBoundaryBlock(header, block);
					bMarge = true;
				}
			}
			if( CheckBoundaryBlock(block->prev) == false )
			{
				if( bMarge ) BreakLinkFreeBoundaryBlock(header, block);
				BreakLinkFreeBoundaryBlock(header, block->prev);
				MargeBoundaryBlock(header, block->prev);
				bMarge = true;
			}
			if( !bMarge )	LinkFreeBoundaryBlock(header, block);
		}

		//----------------------------------------------------------------------
		//	境界ブロックの使用フラグ設定
		//----------------------------------------------------------------------
		FORCEINLINE void UseBoundaryBlock(BoundaryBlockHeader* block)
		{
#if USE_TLSF_MEMORY_CHECK
			block->id1 = FOURCC('t', 'l', 's', 'f');
			block->id2 = FOURCC('t', 'l', 's', 'f');
#endif // ~#if USE_TLSF_MEMORY_CHECK
			block->use = 1;
		}
		//----------------------------------------------------------------------
		//	境界ブロックの使用フラグ解除
		//----------------------------------------------------------------------
		FORCEINLINE void FreeBoundaryBlock(BoundaryBlockHeader* block)
		{
			size_t padding = block->offset - BlockHeaderSize;
			block->size += padding;
			block->offset = BlockHeaderSize;
#if USE_TLSF_MEMORY_CHECK
			block->id1 = FOURCC('n', 'o', 'n', 'e');
			block->id2 = FOURCC('n', 'o', 'n', 'e');
#endif // ~#if USE_TLSF_MEMORY_CHECK
			block->use = 0;
		}
		//----------------------------------------------------------------------
		//	境界ブロックの使用フラグ確認
		//----------------------------------------------------------------------
		FORCEINLINE b8 CheckBoundaryBlock(BoundaryBlockHeader* block)
		{
#if USE_TLSF_MEMORY_CHECK
			if( block->use )
			{
				if (block->id1 != FOURCC('t', 'l', 's', 'f')) return false;
				if (block->id2 != FOURCC('t', 'l', 's', 'f')) return false;
			}
#endif // ~#if USE_TLSF_MEMORY_CHECK
			return block->use != 0;
		}
#if USE_TLSF_MEMORY_CHECK
		//----------------------------------------------------------------------
		//	境界ブロックの使用フラグ確認
		//----------------------------------------------------------------------
		FORCEINLINE void EnableMemoryCheck(TLSFHeader* header, bool enable)
		{
			header->memcheck = !!enable;
		}
#endif // ~#if USE_TLSF_MEMORY_CHECK
	}
}

#endif	// __KGL_TLSF_UTILITY_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
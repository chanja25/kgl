//----------------------------------------------------------------------
//!
//!	@file	kgl.TLSF.h
//!	@brief	TLSF
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TLSF_H__
#define	__KGL_TLSF_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.TLSF.Utility.h"

namespace kgl
{
	namespace tlsf
	{
#if USE_TLSF_MEMORY_CHECK
		extern void MemoryCheck(TLSFHeader* header, BoundaryBlockHeader* ptr = null);
#endif

		//======================================================================
		//!	メモリプールクラス(TLSF)
		//======================================================================
		class CTlsfAllocator
		{
		public:
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			FORCEINLINE CTlsfAllocator(void)
				: first(null), header(null), count(0)
			{}

		public:
			//----------------------------------------------------------------------
			//!	初期化
			//!	@param size [in] 管理メモリサイズ
			//----------------------------------------------------------------------
			FORCEINLINE b8 Initialize(void* memory, size_t size)
			{
				if( header != null ) return false;

				first = memory;
				header = CreateTLSF(memory, size);
				count = 0;
				return true;
			}
			//----------------------------------------------------------------------
			//!	終了処理
			//----------------------------------------------------------------------
			FORCEINLINE void Finalize(void)
			{
#if	KGL_DEBUG_MEMORY
				if( count > 0 )
				{
					WarningTrace("解放されていないメモリが%d個あります(CTlsfAllocator::Finalize)", count);
				}
#endif	// ~#if KGL_DEBUG_MEMORY
				header = null;
				first = null;
			}
			
		public:
			//----------------------------------------------------------------------
			//!	メモリ確保
			//!	@param size		[in] 確保するサイズ
			//!	@param align	[in] アライメント
			//!	@return 確保したメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void* Allocate(size_t size, size_t align = DefaultAlign)
			{
				align = Max(align, DefaultAlign);
				size = Align(size, align);

				auto block = FindFreeBoundaryBlock(header, size, align);
				if( block == null ) return null;

				count++;
				SplitBoundaryBlock(header, block, size, align);
				UseBoundaryBlock(block);

#if USE_TLSF_MEMORY_CHECK
				MemoryCheck(header, block);
#endif // USE_TLSF_MEMORY_CHECK
				return GetMemory(block);
			}

			//----------------------------------------------------------------------
			//!	メモリ再確保
			//!	@param size		[in] 確保するサイズ
			//!	@param align	[in] アライメント
			//!	@return 確保したメモリの先頭アドレス
			//----------------------------------------------------------------------
			INLINE void* Reallocate(void* ptr, size_t size, size_t align = DefaultAlign)
			{
				if( ptr == null )
				{
					return Allocate(size, align);
				}
				align = Max(align, DefaultAlign);
				size = Align(size, align);

				auto block = GetBoundaryBlock(ptr);
				const size_t nowSize = block->size;
				const size_t nowOffset = block->offset;
				const size_t offset = CalcBoundaryBlockOffset(block, align);
				if( nowSize == size && nowOffset == offset )
				{
					return ptr;
				}
				auto prev = block->prev;
				FreeBoundaryBlock(block);
				FreeBoundaryBlock(header, block);

				void* mem = ptr;
				if( CheckBoundaryBlock(prev) == false || nowSize < size || nowOffset != offset )
				{
					auto newblock = FindFreeBoundaryBlock(header, size, align);
					if(	newblock == null ) return null;
						
					mem = (u8*)newblock + CalcBoundaryBlockOffset(newblock, align);
					if( mem != ptr )
					{
						kgMemmove(mem, ptr, Min(nowSize, size));
					}
					block = newblock;
				}
				SplitBoundaryBlock(header, block, size, align);
				UseBoundaryBlock(block);
#if USE_TLSF_MEMORY_CHECK
				MemoryCheck(header, block);
#endif // USE_TLSF_MEMORY_CHECK
				return mem;
			}
			//----------------------------------------------------------------------
			//!	メモリ解放
			//!	@param ptr [in] 解放するメモリの先頭アドレス
			//----------------------------------------------------------------------
			FORCEINLINE void Free(void* ptr)
			{
				if( ptr == null ) return;
				auto block = GetBoundaryBlock(ptr);
				kgAssert(CheckBoundaryBlock(block), "確保していないアドレスを解放しようとしています");

				FreeBoundaryBlock(block);
				FreeBoundaryBlock(header, block);
				count--;
#if USE_TLSF_MEMORY_CHECK
				MemoryCheck(header, block);
#endif // USE_TLSF_MEMORY_CHECK
			}

#if USE_TLSF_MEMORY_CHECK
			//----------------------------------------------------------------------
			//	境界ブロックの使用フラグ確認
			//----------------------------------------------------------------------
			FORCEINLINE void EnableMemoryCheck(bool enable)
			{
				tlsf::EnableMemoryCheck(header, enable);
			}
#endif // ~#if USE_TLSF_MEMORY_CHECK

		public:
			//----------------------------------------------------------------------
			//!	TLSFヘッダーの取得
			//!	@return TLSFヘッダー
			//----------------------------------------------------------------------
			FORCEINLINE TLSFHeader* GetTLSFHeader(void)	{ return header; }
			//----------------------------------------------------------------------
			//!	全体のメモリサイズの取得
			//!	@return メモリサイズ
			//----------------------------------------------------------------------
			FORCEINLINE size_t GetMemorySize(void)	{ return header->size; }
			//----------------------------------------------------------------------
			//!	メモリ確保サイズの取得
			//!	@return メモリ確保サイズ
			//----------------------------------------------------------------------
			FORCEINLINE size_t GetAllocSize(void)	{ return header->alloc; }
			//----------------------------------------------------------------------
			//!	メモリ確保数の取得
			//!	@return メモリ確保数
			//----------------------------------------------------------------------
			FORCEINLINE u32 GetAllocCount(void) { return count; }
			//----------------------------------------------------------------------
			//!	メモリが管理領域内に存在するか？
			//!	@param ptr	[in] アドレス
			//!	@return 結果
			//----------------------------------------------------------------------
			FORCEINLINE b8 IsManaged(void* ptr) { return (first < ptr && ptr < header->end); }

		private:
			void*		first;	//!< メモリプールの先頭アドレス
			TLSFHeader*	header;	//!< TLSFヘッダー
			u32			count;	//!< 確保数
		};
	}
}

#endif	// __KGL_TLSF_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.TLSF.Utility.h
//!	@brief	TLSF
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TLSF_UTILITY_H__
#define	__KGL_TLSF_UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define	USE_TLSF_MEMORY_CHECK	1 && KGL_DEBUG_MEMORY

namespace kgl
{
	namespace tlsf
	{
		//	確保時の基本のアライメント
		const size_t DefaultAlign	= 16;

#if	X64
		const u8 FREE_LIST_MAX	= 32;		//!< リストの管理数
#else	// ~#if X64
		const u8 FREE_LIST_MAX	= 30;		//!< リストの管理数
#endif	// ~#if X64

		const u8 FREE_SECOND_LIST_LOG_MAX = 4;
		const u8 FREE_SECOND_LIST_MAX = (1 << FREE_SECOND_LIST_LOG_MAX);

		const u8 SIZE_BIT = sizeof(size_t) * 8;
		//======================================================================
		//!	境界ブロックヘッダー
		//======================================================================
		struct BoundaryBlockHeader
		{
#if USE_TLSF_MEMORY_CHECK
			size_t					id1;				//!< メモリチェック用
#endif // ~#if USE_TLSF_MEMORY_CHECK
			BoundaryBlockHeader*	prev;				//!< 前のメモリブロック
			size_t					use: 1;				//!< フラグ
			size_t					size: SIZE_BIT-1;	//!< サイズ
			size_t					offset;				//!< ポインターへのオフセット(ヘッダーサイズ)

			BoundaryBlockHeader*	free_next;			//!< 前のフリーブロック
			BoundaryBlockHeader*	free_prev;			//!< 次のフリーブロック
#if USE_TLSF_MEMORY_CHECK
			size_t					id2;				//!< メモリチェック用
#endif // ~#if USE_TLSF_MEMORY_CHECK
			size_t					reserved;			//!< データ側のオフセット保持用(直接参照はしていないが必須)
		};

		//======================================================================
		//!	TLSFヘッダー
		//======================================================================
		struct TLSFHeader
		{
			size_t					size;			//!< サイズ
			size_t					alloc;			//!< 使用中のサイズ
			void*					end;			//!< 末尾
#if USE_TLSF_MEMORY_CHECK
			size_t					memcheck: 1;	//!< フラグ
#endif // ~#if USE_TLSF_MEMORY_CHECK


			u32						free_block_count[FREE_LIST_MAX];					//!< フリーブロック格納数
			BoundaryBlockHeader*	free_block[FREE_LIST_MAX][FREE_SECOND_LIST_MAX];	//!< フリーブロック格納用
		};

		//!	第二カテゴリーの取得
		u32 GetSecondLevelIndex(size_t size, u8 MSB, u8 N = FREE_SECOND_LIST_LOG_MAX);
		//!	メモリプールの作成
		TLSFHeader* CreateTLSF(void* memory, size_t size);
		//!	ブロックの作成
		BoundaryBlockHeader* CreateBoundaryBlock(BoundaryBlockHeader* ptr, size_t size, size_t offset, BoundaryBlockHeader* prev = null);

		//!	ブロックの管理メモリ取得
		void* GetMemory(BoundaryBlockHeader* block);
		//!	管理メモリからブロック取得
		BoundaryBlockHeader* GetBoundaryBlock(void* ptr);

		//!	次のブロック取得
		BoundaryBlockHeader* NextBlock(BoundaryBlockHeader* block);
		//!	前のブロック取得
		BoundaryBlockHeader* PrevBlock(BoundaryBlockHeader* block);
		//!	ブロックのマージ
		void MargeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);
		//	ブロックのオフセット算出
		size_t CalcBoundaryBlockOffset(BoundaryBlockHeader* block, size_t align);
		//!	ブロックの分割
		BoundaryBlockHeader* SplitBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block, size_t size, size_t align);

		//!	フリーリストからブロック取得
		BoundaryBlockHeader* FindFreeBoundaryBlock(TLSFHeader* header, size_t size, size_t align);
		//!	フリーリストにブロックを追加
		void LinkFreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);
		//	フリーリストからブロックを外す
		void BreakLinkFreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);
		//!	ブロックを解放
		void FreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);
		//!	フリーリストにブロックを追加
		void LinkFreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);
		//	フリーリストからブロックを外す
		void BreakLinkFreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);
		//!	ブロックを解放
		void FreeBoundaryBlock(TLSFHeader* header, BoundaryBlockHeader* block);

		//!	ブロックの使用フラグ設定
		void UseBoundaryBlock(BoundaryBlockHeader* block);
		//!	ブロックの使用フラグ解除
		void FreeBoundaryBlock(BoundaryBlockHeader* block);
		//!	ブロックの使用フラグ確認
		b8 CheckBoundaryBlock(BoundaryBlockHeader* block);

#if USE_TLSF_MEMORY_CHECK
		//!	メモリチェックの有効設定
		void EnableMemoryCheck(TLSFHeader* header, bool enable);
#endif // ~#if USE_TLSF_MEMORY_CHECK
	}
}

#include "kgl.TLSF.Utility.inline.h"

#endif	// __KGL_TLSF_UTILITY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Memory.h
//!	@brief	メモリ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MEMORY_H__
#define	__KGL_MEMORY_H__


//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Memory
	{
		//!	メモリ確保
		//!	@param size		[in] 確保するメモリのサイズ
		//!	@return 確保したメモリの先頭アドレス
		void* Allocate(size_t size);
		//!	メモリ確保
		//!	@param size		[in] 確保するメモリのサイズ
		//!	@param align	[in] アライメント
		//!	@return 確保したメモリの先頭アドレス
		void* Allocate(size_t size, size_t align);
		//!	メモリ再確保
		//!	@param memory	[in] 現在のメモリ
		//!	@param size		[in] 確保するメモリのサイズ
		//!	@return 確保したメモリの先頭アドレス
		void* Reallocate(void* memory, size_t size);
		//!	メモリ再確保
		//!	@param memory	[in] 現在のメモリ
		//!	@param size		[in] 確保するメモリのサイズ
		//!	@param align	[in] アライメント
		//!	@return 確保したメモリの先頭アドレス
		void* Reallocate(void* memory, size_t size, size_t align);
		//!	解放
		//!	@param pMemory	[in] 解放するメモリの先頭アドレス
		void Free(void* ptr);

		//!	現在確保しているサイズ取得
		//!	@return サイズ
		size_t GetAllocateSize(void);

#if KGL_USE_TLSF_ALLOCATOR
		void EnableMemoryCheck(bool enable);
#endif // ~#if KGL_USE_TLSF_ALLOCATOR


#if	KGL_DEBUG_MEMORY
		//!	メモリ状況をビットマップで画像として保存する
		void SaveMemoryFragment(const c8* pPath, s32 iWidth = 1024);
		//!	現在確保されているメモリ表示
		void ShowMemory(void);

		namespace Debug
		{
			//!	メモリ確保
			//!	@param uiSize	[in] 確保するメモリのサイズ
			//!	@return 確保したメモリの先頭アドレス
			void* Allocate(size_t size);
			//!	メモリ確保
			//!	@param size		[in] 確保するメモリのサイズ
			//!	@param align	[in] アライメント
			//!	@return 確保したメモリの先頭アドレス
			void* Allocate(size_t size, size_t align);
			//!	解放
			//!	@param pMemory	[in] 解放するメモリの先頭アドレス
			void Free(void* ptr);

			//!	現在確保しているサイズ取得
			//!	@return サイズ
			size_t GetAllocateSize(void);

			//!	現在確保されているメモリ表示
			void ShowMemory(void);

#if KGL_USE_TLSF_ALLOCATOR
			void EnableMemoryCheck(bool enable);
#endif // ~#if KGL_USE_TLSF_ALLOCATOR
		}
#endif	// KGL_DEBUG_MEMORY
	}
}


#endif	// __KGL_MEMORY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
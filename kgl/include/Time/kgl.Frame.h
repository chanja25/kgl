//----------------------------------------------------------------------
//!
//!	@file	kgl.Frame.h
//!	@brief	フレーム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FRAME_H__
#define	__KGL_FRAME_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Frame
	{
		//======================================================================
		//!	フレーム制御インターフェイス
		//======================================================================
		interface IFrameControl
			: public IKGLBase
		{
		public:
			//!	更新
			//!	@return 更新の有無
			virtual b8 Update(void) = 0;
			//!	描画フレームの更新
			//!	@return 更新の有無
			virtual b8 UpdateRender(void) = 0;

			//!	描画スキップが有効か？
			//!	@return	スキップフラグ
			virtual b8 IsEnableSkip(void) const = 0;
			//!	スキップモード変更(trueの場合、速度が遅い時に描画の回数を減らす)
			//!	@param bSkip	[in] 描画スキップモードの設定
			virtual void SetSkip(b8 bSkip) = 0;
			//!	1秒間に実行する目標フレーム数の設定
			//!	@param iTargetFPS	[in] 目標フレームレート
			virtual void SetTargetFPS(s32 iTargetFPS) = 0;
			//!	1秒間に実行する目標フレーム数の取得
			//!	@return 目標フレームレート
			virtual s32 GetTargetFPS(void) const = 0;
			//!	1フレームの経過時間
			//!	@return 経過時間
			virtual f32 GetElapsedTime(void) const = 0;
			//!	1フレームの描画の経過時間
			//!	@return 経過時間
			virtual f32 GetRenderElapsedTime(void) const = 0;
			//!	1フレームにかける目標時間
			//!	@return 目標フレームタイム
			virtual f32	GetTargetFrameTime(void) const = 0;
			//!	経過時間の取得
			//!	@return 経過時間(秒単位)
			virtual u32 GetSec(void) const = 0;
			//!	経過時間の取得(1時間毎にリセットされる)
			//!	@return 経過時間(秒単位)
			virtual f32 GetTime(void) const = 0;
		};

		//======================================================================
		//!	FPSカウンターインターフェイス
		//======================================================================
		interface IFPSCounter
			: public IKGLBase
		{
		public:
			//!	更新
			//!	@param fElapsedTime	[in] 経過時間
			virtual void Update(f32 fElapsedTime) = 0;
			//!	フレームレートの取得(1秒間の平均)
			//!	@return フレームレート
			virtual f32 GetFPS(void) = 0;
			//!	経過時間の取得(1秒間の平均)
			//!	@return 経過時間
			virtual f32 GetElapsedTime(void) = 0;
		};

		//!	フレームコントロールの生成
		//!	@param ppFrameControl	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IFrameControl** ppFrameControl);
		//!	FPSカウンターの生成
		//!	@param ppFPSCounter	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IFPSCounter** ppFPSCounter);
	}
}

#endif	// __KGL_FRAME_H__
//=======================================================================
//	END OF FILE
//=======================================================================
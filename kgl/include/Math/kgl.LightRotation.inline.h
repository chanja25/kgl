//----------------------------------------------------------------------
//!
//!	@file	kgl.LightRotation.inline.h
//!	@brief	軽量回転関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_LIGHT_ROTATION_INLINE_H__
#define	__KGL_LIGHT_ROTATION_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Math/kgl.LightRotation.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation::LightRotation(void)
		: yaw(0), pitch(0), roll(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation::LightRotation(const LightRotation& r)
		: yaw(r.yaw), pitch(r.pitch), roll(r.roll)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation::LightRotation(const Rotation& r)
		: yaw(Math::RadianToLightRot(r.yaw)), pitch(Math::RadianToLightRot(r.pitch)), roll(Math::RadianToLightRot(r.roll))
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation::LightRotation(s32 _yaw, s32 _pitch, s32 _roll)
		: yaw(_yaw), pitch(_pitch), roll(_roll)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation::LightRotation(f32 _yaw, f32 _pitch, f32 _roll)
		: yaw(Math::RadianToLightRot(_yaw)), pitch(Math::RadianToLightRot(_pitch)), roll(Math::RadianToLightRot(_roll))
	{}

	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation& LightRotation::operator = (const LightRotation& r)
	{
		yaw 	= r.yaw;
		pitch	= r.pitch;
		roll	= r.roll;

		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation LightRotation::operator - (void) const
	{
		return LightRotation(-yaw, -pitch, -roll);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation LightRotation::operator + (const LightRotation& r) const
	{
		return LightRotation(yaw + r.yaw, pitch + r.pitch, roll + r.roll);
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation LightRotation::operator - (const LightRotation& r) const
	{
		return LightRotation(yaw - r.yaw, pitch - r.pitch, roll - r.roll);
	}
	//----------------------------------------------------------------------
	//	加算後代入
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation& LightRotation::operator += (const LightRotation& r)
	{
		yaw		+= r.yaw;
		pitch	+= r.pitch;
		roll	+= r.roll;
		return *this;
	}
	//----------------------------------------------------------------------
	//	減算後代入
	//----------------------------------------------------------------------
	FORCEINLINE LightRotation& LightRotation::operator -= (const LightRotation& r)
	{
		yaw		-= r.yaw;
		pitch	-= r.pitch;
		roll	-= r.roll;
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 LightRotation::operator == (const LightRotation& r) const
	{
		return (yaw == r.yaw && pitch == r.pitch && roll == r.roll);
	}
	//----------------------------------------------------------------------
	//	不等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 LightRotation::operator != (const LightRotation& r) const
	{
		return (yaw != r.yaw || pitch != r.pitch || roll != r.roll);
	}

	//----------------------------------------------------------------------
	//	クリア
	//----------------------------------------------------------------------
	FORCEINLINE void LightRotation::ZeroClear(void)
	{
		yaw = pitch	= roll = 0;
	}

	//----------------------------------------------------------------------
	//	角度を-πからπの範囲にラップする
	//----------------------------------------------------------------------
	FORCEINLINE void LightRotation::Wrap(void)
	{
		pitch = Math::WrapLightRotPI(pitch);

		const f32 pitch_rad = Math::LightRotToRadian(pitch);
		if( pitch_rad < -Math::InvPI2 )
		{
			pitch	 =-Math::LightRotPI - pitch;
			yaw		+= Math::LightRotPI;
			roll	+= Math::LightRotPI;
		}
		else if( pitch_rad > Math::InvPI2 )
		{
			pitch	 = Math::LightRotPI - pitch;
			yaw		+= Math::LightRotPI;
			roll	+= Math::LightRotPI;
		}

		if( Abs(Math::LightRotToRadian(pitch)) > (Math::InvPI2-1e-4f) )
		{
			yaw += roll;
			roll = 0;
		}
		else
		{
			roll = Math::WrapLightRotPI(roll);
		}
		yaw = Math::WrapLightRotPI(yaw);
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	回転角設定
		//----------------------------------------------------------------------
		FORCEINLINE void RotationYawPitchRoll(LightRotation& r, f32 yaw, f32 pitch, f32 roll)
		{
			r.yaw	= RadianToLightRot(yaw);
			r.pitch	= RadianToLightRot(pitch);
			r.roll	= RadianToLightRot(roll);
		}
		//----------------------------------------------------------------------
		//	回転角設定
		//----------------------------------------------------------------------
		FORCEINLINE void RotationYawPitchRoll(LightRotation& r, s32 yaw, s32 pitch, s32 roll)
		{
			r.yaw	= yaw;
			r.pitch	= pitch;
			r.roll	= roll;
		}
		//----------------------------------------------------------------------
		//	線形補間
		//----------------------------------------------------------------------
		FORCEINLINE void RotationLerp(LightRotation& r, const LightRotation& r1, const LightRotation& r2, f32 t)
		{
			r.yaw	= (s32)Linear((f32)r1.yaw,	(f32)r2.yaw,	t);
			r.pitch	= (s32)Linear((f32)r1.pitch,(f32)r2.pitch,	t);
			r.roll	= (s32)Linear((f32)r1.roll,	(f32)r2.roll,	t);
		}
		//----------------------------------------------------------------------
		//	クオータニオンから回転角作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromQuaternion(LightRotation& r, const Quaternion& q)
		{
			//	sin(pitch)を取り出す
			register f32 sp = -2.0f * (q.y*q.z - q.w*q.x);

			//	ジンバルロックをチェックする
			//	数字の不正確さのためにわずかな許容範囲を与える
			if( fabs(sp) > 0.9999f )
			{
				//	真上かましたを向いている
				r.pitch	= Math::RadianToLightRot(Math::PI * sp);
				//	ヨーを計算し、ロールを0に設定する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(-q.x*q.z + q.w*q.y, 0.5f - q.y*q.y - q.z*q.z));
				r.roll	= 0;
			}
			else
			{
				//	角度を計算する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(q.x*q.z + q.w*q.y, 0.5f - q.x*q.x - q.y*q.y));
				r.pitch	= Math::RadianToLightRot(Math::ASin(sp));
				r.roll	= Math::RadianToLightRot(Math::ATan2(q.x*q.y + q.w*q.z, 0.5f - q.x*q.x - q.z*q.z));
			}
		}
		//----------------------------------------------------------------------
		//	行列から回転角を作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromMatrix(LightRotation& r, const Matrix& m)
		{
			//	m._23からsin(pitch)を取り出す
			register f32 sp = m._23;

			//	ジンバルロックをチェックする
			//	数字の不正確さのためにわずかな許容範囲を与える
			if( fabs(sp) > 0.9999f )
			{
				//	真上かましたを向いている
				r.pitch	= Math::RadianToLightRot(Math::PI * sp);
				//	ヨーを計算し、ロールを0に設定する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(-m._31, m._11));
				r.roll	= 0;
			}
			else
			{
				//	角度を計算する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(m._13, m._33));
				r.pitch	= Math::RadianToLightRot(Math::ASin(sp));
				r.roll	= Math::RadianToLightRot(Math::ATan2(m._21, m._22));
			}
		}
		//----------------------------------------------------------------------
		//	行列から回転角を作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromMatrix(LightRotation& r, const Matrix4x3& m)
		{
			//	m._23からsin(pitch)を取り出す
			register f32 sp = m._23;

			//	ジンバルロックをチェックする
			//	数字の不正確さのためにわずかな許容範囲を与える
			if( fabs(sp) > 0.9999f )
			{
				//	真上かましたを向いている
				r.pitch	= Math::RadianToLightRot(Math::PI * sp);
				//	ヨーを計算し、ロールを0に設定する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(-m._31, m._11));
				r.roll	= 0;
			}
			else
			{
				//	角度を計算する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(m._13, m._33));
				r.pitch	= Math::RadianToLightRot(Math::ASin(sp));
				r.roll	= Math::RadianToLightRot(Math::ATan2(m._21, m._22));
			}
		}
		//----------------------------------------------------------------------
		//	行列から回転角を作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromMatrix(LightRotation& r, const RotationMatrix& m)
		{
			//	m._23からsin(pitch)を取り出す
			register f32 sp = m._23;

			//	ジンバルロックをチェックする
			//	数字の不正確さのためにわずかな許容範囲を与える
			if( fabs(sp) > 0.9999f )
			{
				//	真上かましたを向いている
				r.pitch	= Math::RadianToLightRot(Math::PI * sp);
				//	ヨーを計算し、ロールを0に設定する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(-m._31, m._11));
				r.roll	= 0;
			}
			else
			{
				//	角度を計算する
				r.yaw	= Math::RadianToLightRot(Math::ATan2(m._13, m._33));
				r.pitch	= Math::RadianToLightRot(Math::ASin(sp));
				r.roll	= Math::RadianToLightRot(Math::ATan2(m._21, m._22));
			}
		}
	}
}

#endif	// __KGL_LIGHT_ROTATION_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Vector4.h
//!	@brief	ベクトル4関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_VECTOR4_H__
#define	__KGL_VECTOR4_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	//======================================================================
	//!	ベクター4
	//======================================================================
	ALIGNED(16)
	class Vector4
	{
	public:
		//!	コンストラクタ
		Vector4(void);
		//!	コンストラクタ			
		Vector4(const Vector4& v);
		//!	コンストラクタ
		Vector4(f32 x, f32 y, f32 z, f32 w);

		//!	キャスト
		operator f32* (void);
		//!	キャスト
		operator const f32* (void);
		//!	代入
		Vector4& operator = (const Vector4& v);
		//!	反転
		Vector4 operator - (void) const;
		//!	加算
		Vector4 operator + (const Vector4& v) const;
		//!	減算
		Vector4 operator - (const Vector4& v) const;
		//!	乗算
		Vector4 operator * (const Vector4& v) const;
		//!	除算
		Vector4 operator / (const Vector4& v) const;
		//!	乗算
		Vector4 operator * (f32 s) const;
		//!	除算
		Vector4 operator / (f32 s) const;
		//!	加算・代入
		Vector4& operator += (const Vector4& v);
		//!	減算・代入
		Vector4& operator -= (const Vector4& v);
		//!	乗算・代入
		Vector4& operator *= (const Vector4& v);
		//!	除算・代入
		Vector4& operator /= (const Vector4& v);
		//!	乗算・代入
		Vector4& operator *= (f32 s);
		//!	除算・代入
		Vector4& operator /= (f32 s);

		//!	等号
		b8 operator == (const Vector4& v) const;
		//!	不等号
		b8 operator != (const Vector4& v) const;

		//!	クリア
		void ZeroClear(void);
		//!	正規化
		void Normalize(void);
		//!	正規化(値を書き換えずに結果だけを返す)
		Vector4 SafeNormalize(void) const;
		//!	現在の値以上の最低の整数値に変換
		void Ceil(void);
		//!	現在の値以上の最低の整数値に変換(値を書き換えずに結果だけを返す)
		Vector4 SafeCeil(void) const;
		//!	現在の値以下の最大の整数値に変換
		void Floor(void);
		//!	現在の値以下の最大の整数値に変換(値を書き換えずに結果だけを返す)
		Vector4 SafeFloor(void) const;

		//!	長さの2乗取得
		f32 LengthSq(void) const;
		//!	長さ取得
		f32 Length(void) const;
		//!	距離の2乗取得
		f32 DistanceSq(const Vector4& v) const;
		//!	距離の取得
		f32 Distance(const Vector4& v) const;

		//!	Nanチェック
		b8 IsNan(void) const;

	public:
		union
		{
			f32	v[4];

			struct
			{
				f32	x, y, z, w;
			};
#if	USE_SIMD
			__m128	simd;
#endif	// ~#if USE_SIMD
		};
	};
	//!	乗算
	Vector4 operator * (f32 s, const Vector4& v);

	template<>
	struct IsPODType<Vector4> { enum { value = true }; };

	namespace Math
	{
		//!	加算
		//!	@param vOut	[out] 出力
		//!	@param v1	[in]  ベクトル1
		//!	@param v2	[in]  ベクトル2
		void Vec4Add(Vector4& vOut, const Vector4& v1, const Vector4& v2);
		//!	減算
		//!	@param vOut	[out] 出力
		//!	@param v1	[in]  ベクトル1
		//!	@param v2	[in]  ベクトル2
		void Vec4Subtract(Vector4& vOut, const Vector4& v1, const Vector4& v2);
		//!	乗算
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param s	[in]  スケール
		void Vec4Scale(Vector4& vOut, const Vector4& v, f32 s);
		//!	除算
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param s	[in]  除算値
		void Vec4Division(Vector4& vOut, const Vector4& v, f32 s);
		//!	乗算
		//!	@param vOut	[out] 出力
		//!	@param v1	[in]  ベクトル
		//!	@param v2	[in]  ベクトル
		void Vec4Multiple(Vector4& vOut, const Vector4& v1, const Vector4& v2);
		//!	除算
		//!	@param vOut	[out] 出力
		//!	@param v1	[in]  ベクトル
		//!	@param v2	[in]  ベクトル
		void Vec4Division(Vector4& vOut, const Vector4& v1, const Vector4& v2);
		//!	長さの2乗取得
		//!	@param v	[in]  ベクトル
		//!	@return 長さ
		f32 Vec4LengthSq(const Vector4& v);
		//!	長さ取得
		//!	@param v	[in]  ベクトル
		//!	@return 長さ
		f32 Vec4Length(const Vector4& v);
		//!	2点の距離の2乗取得
		//!	@param v1	[in]  ベクトル
		//!	@param v2	[in]  ベクトル
		//!	@return 長さ
		f32 Vec4DistanceSq(const Vector4& v1, const Vector4& v2);
		//!	2点の距離取得
		//!	@param v1	[in]  ベクトル
		//!	@param v2	[in]  ベクトル
		//!	@return 長さ
		f32 Vec4Distance(const Vector4& v1, const Vector4& v2);
		//!	正規化
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		void Vec4Normalize(Vector4& vOut, const Vector4& v);
		//!	外積
		//!	@param vOut	[out] 出力
		//!	@param v1	[in]  ベクトル1
		//!	@param v2	[in]  ベクトル2
		void Vec4Cross(Vector4& vOut, const Vector4& v1, const Vector4& v2);
		//!	内積
		//!	@param v1	[in] ベクトル1
		//!	@param v2	[in] ベクトル2
		//!	@return 内積値
		f32 Vec4Dot(const Vector4& v1, const Vector4& v2);
		//!	線形補間
		//!	@param vOut	[out] 出力
		//!	@param v1	[in]  ベクトル1
		//!	@param v2	[in]  ベクトル2
		//!	@param t	[in]  補間値(0.0〜1.0)
		void Vec4Lerp(Vector4& vOut, const Vector4& v1, const Vector4& v2, f32 t);
		//!	エルミート補間
		//!	@param vOut	[out] 出力
		//!	@param p1	[in]  位置1
		//!	@param t1	[in]  傾き1
		//!	@param p2	[in]  位置2
		//!	@param t2	[in]  傾き2
		//!	@param s	[in]  補間値(0.0〜1.0)
		void Vec4Hermite(Vector4& vOut, const Vector4& p1, const Vector4& t1, const Vector4& p2, const Vector4& t2, f32 s);
		//!	ベジエ補間
		//!	@param vOut	[out] 出力
		//!	@param p1	[in]  始点
		//!	@param p2	[in]  制御点
		//!	@param p3	[in]  終点
		//!	@param s	[in]  補間値(0.0〜1.0)
		void Vec4Bezier(Vector4& vOut, const Vector4& p1, const Vector4& p2, const Vector4& p3, f32 s);
		//!	ベジエ補間
		//!	@param vOut	[out] 出力
		//!	@param p1	[in]  始点
		//!	@param p2	[in]  制御点1
		//!	@param p3	[in]  制御点2
		//!	@param p4	[in]  終点
		//!	@param s	[in]  補間値(0.0〜1.0)
		void Vec4Bezier(Vector4& vOut, const Vector4& p1, const Vector4& p2, const Vector4& p3, const Vector4& p4, f32 s);
		//!	ベジエ補間
		//!	@param vOut		[out] 出力
		//!	@param iCount	[in]  座標数
		//!	@param p		[in]  始点-制御点-終点
		//!	@param s		[in]  補間値(0.0〜1.0)
		void Vec4Bezier(Vector4& vOut, s32 iCount, const Vector4* p, f32 s);

		//!	トランスフォーム
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param n	[in]  マトリックス
		void Vec4Transform(Vector4& vOut, const Vector4& v, const Matrix& m);
		//!	トランスフォーム
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param m	[in]  マトリックス
		void Vec4Transform(Vector4& vOut, const Vector4& v, const Matrix4x3& m);
		//!	トランスフォーム
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param m	[in]  マトリックス
		void Vec4Transform(Vector4& vOut, const Vector4& v, const RotationMatrix& m);
		//!	トランスフォーム
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param n	[in]  マトリックス
		void Vec4Transform(Vector4& vOut, const Vector3& v, const Matrix& m);
		//!	トランスフォーム
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param m	[in]  マトリックス
		void Vec4Transform(Vector4& vOut, const Vector3& v, const Matrix4x3& m);
		//!	トランスフォーム
		//!	@param vOut	[out] 出力
		//!	@param v	[in]  ベクトル
		//!	@param m	[in]  マトリックス
		void Vec4Transform(Vector4& vOut, const Vector3& v, const RotationMatrix& m);
	}
}

#endif	// __KGL_VECTOR4_H__
//=======================================================================
//	END OF FILE
//=======================================================================
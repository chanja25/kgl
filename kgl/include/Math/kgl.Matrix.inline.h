//----------------------------------------------------------------------
//!
//!	@file	kgl.Matrix.inline.h
//!	@brief	行列関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATRIX_INLINE_H__
#define	__KGL_MATRIX_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Matrix::Matrix(void)
		: _11(1.0f), _12(0.0f), _13(0.0f), _14(0.0f)
		, _21(0.0f), _22(1.0f), _23(0.0f), _24(0.0f)
		, _31(0.0f), _32(0.0f), _33(1.0f), _34(0.0f)
		, _41(0.0f), _42(0.0f), _43(0.0f), _44(1.0f)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Matrix::Matrix(const Matrix& m)
		: _11(m._11), _12(m._12), _13(m._13), _14(m._14)
		, _21(m._21), _22(m._22), _23(m._23), _24(m._24)
		, _31(m._31), _32(m._32), _33(m._33), _34(m._34)
		, _41(m._41), _42(m._42), _43(m._43), _44(m._44)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Matrix::Matrix(	f32 _11, f32 _12, f32 _13, f32 _14,
								f32 _21, f32 _22, f32 _23, f32 _24,
								f32 _31, f32 _32, f32 _33, f32 _34,
								f32 _41, f32 _42, f32 _43, f32 _44)
		: _11(_11), _12(_12), _13(_13), _14(_14)
		, _21(_21), _22(_22), _23(_23), _24(_24)
		, _31(_31), _32(_32), _33(_33), _34(_34)
		, _41(_41), _42(_42), _43(_43), _44(_44)
	{}

	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE f32& Matrix::operator() (u32 row, u32 col)
	{
		return m[row][col];
	}
	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE f32  Matrix::operator() (u32 row, u32 col) const
	{
		return m[row][col];
	}
	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Matrix::operator() (u32 row)
	{
		return v[row];
	}
	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE const Vector4& Matrix::operator() (u32 row) const
	{
		return v[row];
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Matrix::operator f32* (void)
	{
		return (f32*)this;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Matrix::operator const f32* (void) const
	{
		return (f32*)this;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Matrix& Matrix::operator = (const Matrix& m)
	{
		v[0] = m.v[0];
		v[1] = m.v[1];
		v[2] = m.v[2];
		v[3] = m.v[3];
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Matrix Matrix::operator * (const Matrix& m) const
	{
		register Matrix mm;
		Math::MatrixMultiply(mm, *this, m);
		return mm;
	}
	//----------------------------------------------------------------------
	//	乗算後代入
	//----------------------------------------------------------------------
	FORCEINLINE Matrix& Matrix::operator *= (const Matrix& m)
	{
		Math::MatrixMultiply(*this, *this, m);
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Matrix::operator == (const Matrix& m) const
	{
		return v[0] == m.v[0] && v[1] == m.v[1] && v[2] == m.v[2] && v[3] == m.v[3];
	}
	//----------------------------------------------------------------------
	//	不等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Matrix::operator != (const Matrix& m) const
	{
		return v[0] != m.v[0] || v[1] != m.v[1] || v[2] != m.v[2] || v[3] != m.v[3];
	}
	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Matrix::IsNan(void) const
	{
		return v[0].IsNan() || v[1].IsNan() || v[2].IsNan() || v[3].IsNan();
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	単位行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixIdentity(Matrix& m)
		{
			m._12 = m._13 = m._14 =
			m._21 = m._23 = m._24 =
			m._31 = m._32 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;
			m._11 = m._22 = m._33 = m._44 = 1.0f;
		}
		//----------------------------------------------------------------------
		//	行列の転置
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranspose(Matrix& m, const Matrix& mat)
		{
			register Matrix mm;
			mm._11 = mat._11; mm._12 = mat._21; mm._13 = mat._31; mm._14 = mat._41;
			mm._21 = mat._12; mm._22 = mat._22; mm._23 = mat._32; mm._24 = mat._42;
			mm._31 = mat._13; mm._32 = mat._23; mm._33 = mat._33; mm._34 = mat._43;
			mm._41 = mat._14; mm._42 = mat._24; mm._43 = mat._34; mm._44 = mat._44;

			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の転置
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranspose(Matrix& m, const Matrix4x3& mat)
		{
			m._11 = mat._11; m._12 = mat._21; m._13 = mat._31; m._14 = mat._41;
			m._21 = mat._12; m._22 = mat._22; m._23 = mat._32; m._24 = mat._42;
			m._31 = mat._13; m._32 = mat._23; m._33 = mat._33; m._34 = mat._43;
			m._41 = m._42 = m._43 = 0.0f;
			m._44 = 1.0f;
		}
		//----------------------------------------------------------------------
		//	逆行列作成
		//----------------------------------------------------------------------
		INLINE void MatrixInverse(Matrix& m, const Matrix& mat)
		{
			register f32 det = MatrixDeterminant(mat);
			if( det == 0.0f )	return;

			register f32 fInvDet = 1.0f / det;
			register Matrix mm;
#if	USE_SIMD
			__m128 scale = _mm_set_ps1(fInvDet);
			__m128 vec1, vec2, vec3;
			vec1 = _mm_set_ps(0.0f, mat._12, mat._13, mat._22);
			vec2 = _mm_set_ps(0.0f, mat._23, mat._32, mat._33);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, mat._13, mat._12, mat._23);
			vec2 = _mm_set_ps(0.0f, mat._22, mat._33, mat._32);
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			mm.v[0].simd = _mm_mul_ps(vec1, scale);

			vec1 = _mm_set_ps(0.0f, mat._13, mat._11, mat._23);
			vec2 = _mm_set_ps(0.0f, mat._21, mat._33, mat._31);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, mat._11, mat._13, mat._21);
			vec2 = _mm_set_ps(0.0f, mat._23, mat._31, mat._33);
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			mm.v[1].simd = _mm_mul_ps(vec1, scale);

			vec1 = _mm_set_ps(0.0f, mat._11, mat._12, mat._21);
			vec2 = _mm_set_ps(0.0f, mat._23, mat._31, mat._32);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, mat._12, mat._11, mat._22);
			vec2 = _mm_set_ps(0.0f, mat._21, mat._32, mat._31);
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			mm.v[2].simd = _mm_mul_ps(vec1, scale);

			vec1 = _mm_set_ps(0.0f, mat._13, mat._12, mat._11);
			vec3 = _mm_mul_ps(vec1, _mm_set_ps1(-mat._41));
			vec1 = _mm_set_ps(0.0f, mat._23, mat._22, mat._21);
			vec2 = _mm_mul_ps(vec1, _mm_set_ps1(-mat._42));
			vec3 = _mm_add_ps(vec2, vec3);
			vec1 = _mm_set_ps(0.0f, mat._33, mat._32, mat._31);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(-mat._43));
			mm.v[3].simd = _mm_add_ps(vec1, vec3);
			mm._44 = 1.0f;
#else	// ~#if USE_SIMD
			mm._11 = (mat._22 * mat._33 - mat._23 * mat._32) * fInvDet;
			mm._12 = (mat._13 * mat._32 - mat._12 * mat._33) * fInvDet;
			mm._13 = (mat._12 * mat._23 - mat._13 * mat._22) * fInvDet;

			const float a0 = src.m[0][0]*src.m[1][1] - src.m[0][1]*src.m[1][0];
			const float a1 = src.m[0][0]*src.m[1][2] - src.m[0][2]*src.m[1][0];
			const float a2 = src.m[0][0]*src.m[1][3] - src.m[0][3]*src.m[1][0];
			const float a3 = src.m[0][1]*src.m[1][2] - src.m[0][2]*src.m[1][1];
			const float a4 = src.m[0][1]*src.m[1][3] - src.m[0][3]*src.m[1][1];
			const float a5 = src.m[0][2]*src.m[1][3] - src.m[0][3]*src.m[1][2];
			const float b0 = src.m[2][0]*src.m[3][1] - src.m[2][1]*src.m[3][0];
			const float b1 = src.m[2][0]*src.m[3][2] - src.m[2][2]*src.m[3][0];
			const float b2 = src.m[2][0]*src.m[3][3] - src.m[2][3]*src.m[3][0];
			const float b3 = src.m[2][1]*src.m[3][2] - src.m[2][2]*src.m[3][1];
			const float b4 = src.m[2][1]*src.m[3][3] - src.m[2][3]*src.m[3][1];
			const float b5 = src.m[2][2]*src.m[3][3] - src.m[2][3]*src.m[3][2];
			mm.m[0][0] = ( mat.m[1][1]*b5 - mat.m[1][2]*b4 + mat.m[1][3]*b3) * invDet;
			mm.m[1][0] = (-mat.m[1][0]*b5 + mat.m[1][2]*b2 - mat.m[1][3]*b1) * invDet;
			mm.m[2][0] = ( mat.m[1][0]*b4 - mat.m[1][1]*b2 + mat.m[1][3]*b0) * invDet;

			mm.m[3][0] = (-mat.m[1][0]*b3 + mat.m[1][1]*b1 - mat.m[1][2]*b0) * invDet;
			mm.m[0][1] = (-mat.m[0][1]*b5 + mat.m[0][2]*b4 - mat.m[0][3]*b3) * invDet;
			mm.m[1][1] = ( mat.m[0][0]*b5 - mat.m[0][2]*b2 + mat.m[0][3]*b1) * invDet;
			mm.m[2][1] = (-mat.m[0][0]*b4 + mat.m[0][1]*b2 - mat.m[0][3]*b0) * invDet;
			mm.m[3][1] = ( mat.m[0][0]*b3 - mat.m[0][1]*b1 + mat.m[0][2]*b0) * invDet;
			mm.m[0][2] = ( mat.m[3][1]*a5 - mat.m[3][2]*a4 + mat.m[3][3]*a3) * invDet;
			mm.m[1][2] = (-mat.m[3][0]*a5 + mat.m[3][2]*a2 - mat.m[3][3]*a1) * invDet;
			mm.m[2][2] = ( mat.m[3][0]*a4 - mat.m[3][1]*a2 + mat.m[3][3]*a0) * invDet;
			mm.m[3][2] = (-mat.m[3][0]*a3 + mat.m[3][1]*a1 - mat.m[3][2]*a0) * invDet;
			mm.m[0][3] = (-mat.m[2][1]*a5 + mat.m[2][2]*a4 - mat.m[2][3]*a3) * invDet;
			mm.m[1][3] = ( mat.m[2][0]*a5 - mat.m[2][2]*a2 + mat.m[2][3]*a1) * invDet;
			mm.m[2][3] = (-mat.m[2][0]*a4 + mat.m[2][1]*a2 - mat.m[2][3]*a0) * invDet;
			mm.m[3][3] = ( mat.m[2][0]*a3 - mat.m[2][1]*a1 + mat.m[2][2]*a0) * invDet;

			mm._21 = (mat._23 * mat._31 - mat._21 * mat._33) * fInvDet;
			mm._22 = (mat._11 * mat._33 - mat._13 * mat._31) * fInvDet;
			mm._23 = (mat._13 * mat._21 - mat._11 * mat._23) * fInvDet;

			mm._31 = (mat._21 * mat._32 - mat._22 * mat._31) * fInvDet;
			mm._32 = (mat._12 * mat._31 - mat._11 * mat._32) * fInvDet;
			mm._33 = (mat._11 * mat._23 - mat._12 * mat._21) * fInvDet;

			mm._41 = -(mat._41 * mat._11 + mat._42 * mat._21 + mat._43 * mat._31);
			mm._42 = -(mat._41 * mat._12 + mat._42 * mat._22 + mat._43 * mat._32);
			mm._43 = -(mat._41 * mat._13 + mat._42 * mat._23 + mat._43 * mat._33);

			mm._14 = mm._24 = mm._34 = 0.0f;
			mm._44 = 1.0f;
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const Matrix& m1, const Matrix& m2)
		{
			register Matrix mm;
#if	USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 4; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd);
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd);
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd);
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][3]), m2.v[3].simd);
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
			}
#else	// ~#if USE_SIMD
			for( u32 i=0; i < 4; i++ )
			{
				mm.v[i]  = m1.m[i][0] * m2.v[0];
				mm.v[i] += m1.m[i][1] * m2.v[1];
				mm.v[i] += m1.m[i][2] * m2.v[2];
				mm.v[i] += m1.m[i][3] * m2.v[3];
			}
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const Matrix4x3& m1, const Matrix& m2)
		{
			register Matrix mm;
#if	USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 4; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd);
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd);
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd);
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
			}
			mm.v[3].simd = _mm_add_ps(mm.v[3].simd, m2.v[3].simd);
#else	// ~#if USE_SIMD
			for( u32 i=0; i < 4; i++ )
			{
				mm.v[i]  = m1.m[i][0] * m2.v[0];
				mm.v[i] += m1.m[i][1] * m2.v[1];
				mm.v[i] += m1.m[i][2] * m2.v[2];
			}
			mm.v[3] += m2.v[3];
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const Matrix& m1, const Matrix4x3& m2)
		{
			register Matrix mm;
#if	USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 4; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd());
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd());
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd());
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][3]), m2.v[3].simd(1.0f));
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
			}
#else	// ~#if USE_SIMD
			register Vector3 v;
			for( u32 i=0; i < 4; i++ )
			{
				v  = m1.m[i][0] * m2.v[0];
				v += m1.m[i][1] * m2.v[1];
				v += m1.m[i][2] * m2.v[2];
				v += m1.m[i][3] * m2.v[3];
				mm.v[i].x = v.x;
				mm.v[i].y = v.y;
				mm.v[i].z = v.z;
				mm.v[i].w = m1.m[i][3];
			}
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const Matrix4x3& m1, const Matrix4x3& m2)
		{
			register Matrix mm;
#if	USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 4; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd());
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd());
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd());
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
			}
			mm.v[3].simd = _mm_add_ps(mm.v[3].simd, m2.v[3].simd(1.0f));
#else	// ~#if USE_SIMD
			register Vector3 v;
			for( u32 i=0; i < 4; i++ )
			{
				v  = m1.m[i][0] * m2.v[0];
				v += m1.m[i][1] * m2.v[1];
				v += m1.m[i][2] * m2.v[2];
				mm.v[i].x = v.x;
				mm.v[i].y = v.y;
				mm.v[i].z = v.z;
				mm.v[i].w = 0.0f;
			}
			mm.v[3].x += m2.v[3].x;
			mm.v[3].y += m2.v[3].y;
			mm.v[3].z += m2.v[3].z;
			mm.v[3].w = 1.0f;
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const RotationMatrix& m1, const Matrix& m2)
		{
			register Matrix mm;
#if USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 3; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd);
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd);
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd);
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
			}
			mm.v[3].simd = m2.v[3].simd;
#else	// ~#if USE_SIMD
			for( u32 i=0; i < 3; i++ )
			{
				mm.v[i]  = m1.m[i][0] * m2.v[0];
				mm.v[i] += m1.m[i][1] * m2.v[1];
				mm.v[i] += m1.m[i][2] * m2.v[2];
			}
			mm.v[3] = m2.v[3];
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const Matrix& m1, const RotationMatrix& m2)
		{
			register Matrix mm;
#if USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 4; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd(1.0f));
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd());
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd());
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
				mm.v[i].w = m1.m[i][3];
			}
#else	// ~#if USE_SIMD
			register Vector3 v;
			for( u32 i=0; i < 4; i++ )
			{
				v  = m1.m[i][0] * m2.v[0];
				v += m1.m[i][1] * m2.v[1];
				v += m1.m[i][2] * m2.v[2];
				mm.v[i].x = v.x;
				mm.v[i].y = v.y;
				mm.v[i].z = v.z;
				mm.v[i].w = m1.m[i][3];
			}
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix& m, const RotationMatrix& m1, const RotationMatrix& m2)
		{
			register Matrix mm;
#if USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 3; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd());
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd());
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd());
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd = vec1;
			}
			mm.v[3] = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
#else	// ~#if USE_SIMD
			register Vector3 v;
			for( u32 i=0; i < 3; i++ )
			{
				v  = m1.m[i][0] * m2.v[0];
				v += m1.m[i][1] * m2.v[1];
				v += m1.m[i][2] * m2.v[2];
				mm.v[i].x = v.x;
				mm.v[i].y = v.y;
				mm.v[i].z = v.z;
				mm.v[i].w = 0.0f;
			}
			mm.v[3] = Vector4(0.0f, 0.0f, 0.0f, 1.0f);
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix));
		}
		//----------------------------------------------------------------------
		//	3x3部分の行列式の算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 MatrixDeterminant(const Matrix& m)
		{
#if USE_SIMD
			Vector4 v;
			__m128 vec1, vec2, vec3;
			vec1 = _mm_shuffle_ps(m.v[1].simd, m.v[1].simd, _MM_SHUFFLE(3, 0, 2, 1));
			vec2 = _mm_shuffle_ps(m.v[2].simd, m.v[2].simd, _MM_SHUFFLE(3, 1, 0, 2));
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_shuffle_ps(m.v[1].simd, m.v[1].simd, _MM_SHUFFLE(3, 1, 0, 2));
			vec2 = _mm_shuffle_ps(m.v[2].simd, m.v[2].simd, _MM_SHUFFLE(3, 0, 2, 1));
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			v.simd = _mm_add_ps(m.v[0].simd, vec1);

			return v.x + v.y + v.z;
#else	// ~#if USE_SIMD
			return	m._11 + (m._22 * m._33 - m._23 * m._32) +
					m._12 + (m._23 * m._31 - m._21 * m._33) +
					m._13 + (m._21 * m._32 - m._22 * m._31);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	座標変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranslation(Matrix& m, const Vector4& v)
		{
			MatrixTranslation(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	座標変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranslation(Matrix& m, const Vector3& v)
		{
			MatrixTranslation(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	座標変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranslation(Matrix& m, f32 x, f32 y, f32 z)
		{
#if USE_SIMD
			m.v[0].simd = _mm_set_ps(0.0f, 0.0f, 0.0f, 1.0f);
			m.v[1].simd = _mm_set_ps(0.0f, 0.0f, 1.0f, 0.0f);
			m.v[2].simd = _mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);
			m.v[3].simd = _mm_set_ps(1.0f,    z,    y,    x);
#else	// ~#if USE_SIMD
			m._12 = m._13 = m._14 =
			m._21 = m._23 = m._24 =
			m._31 = m._32 = m._34 = 0.0f;
			m._11 = m._22 = m._33 = m._44 = 1.0f;

			m._41 = x;
			m._42 = y;
			m._43 = z;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix& m, f32 s)
		{
			MatrixScaling(m, s, s, s);
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix& m, const Vector3& v)
		{
			MatrixScaling(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix& m, const Vector4& v)
		{
			MatrixScaling(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix& m, f32 x, f32 y, f32 z)
		{
#if USE_SIMD
			m.v[0].simd = _mm_set_ps(0.0f, 0.0f, 0.0f,    x);
			m.v[1].simd = _mm_set_ps(0.0f, 0.0f,    y, 0.0f);
			m.v[2].simd = _mm_set_ps(0.0f,    z, 0.0f, 0.0f);
			m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
			m._12 = m._13 = m._14 =
			m._21 = m._23 = m._24 =
			m._31 = m._32 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;

			m._11 = x; m._22 = y; m._33 = z;
			m._44 = 1.0f;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	X軸回りの回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationX(Matrix& m, f32 rot)
		{
			register f32 s, c;
			SinCos(s, c, rot);
#if USE_SIMD
			m.v[0].simd = _mm_set_ps(0.0f, 0.0f, 0.0f, 1.0f);
			m.v[1].simd = _mm_set_ps(0.0f,    s,    c, 0.0f);
			m.v[2].simd = _mm_set_ps(0.0f,    c,   -s, 0.0f);
			m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
			m._12 = m._13 = m._14 =
			m._21 = m._24 =
			m._31 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;
			m._11 = m._44 = 1.0f;

			m._22 = c;
			m._23 = s;
			m._32 = -s;
			m._33 = c;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	Y軸回りの回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationY(Matrix& m, f32 rot)
		{
			register f32 s, c;
			SinCos(s, c, rot);
#if USE_SIMD
			m.v[0].simd = _mm_set_ps(0.0f,   -s, 0.0f,    c);
			m.v[1].simd = _mm_set_ps(0.0f, 0.0f, 1.0f, 0.0f);
			m.v[2].simd = _mm_set_ps(0.0f,    s, 0.0f,    c);
			m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
			m._12 = m._14 =
			m._21 = m._23 = m._24 =
			m._32 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;
			m._22 = m._44 = 1.0f;

			m._11 = c;
			m._13 = -s;
			m._31 = s;
			m._33 = c;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	Z軸回りの回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationZ(Matrix& m, f32 rot)
		{
			register f32 s, c;
			SinCos(s, c, rot);
#if USE_SIMD
			m.v[0].simd = _mm_set_ps(0.0f, 0.0f,    s,    c);
			m.v[1].simd = _mm_set_ps(0.0f, 0.0f,    c,   -s);
			m.v[2].simd = _mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);
			m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
			m._13 = m._14 =
			m._23 = m._24 =
			m._31 = m._32 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;
			m._33 = m._44 = 1.0f;

			m._11 = c;
			m._12 = s;
			m._21 = -s;
			m._22 = c;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotation(Matrix& m, const Rotation& r)
		{
			MatrixRotationYawPitchRoll(m, r.yaw, r.pitch, r.roll);
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotation(Matrix& m, const LightRotation& r)
		{
			MatrixRotationYawPitchRoll(m, r.yaw, r.pitch, r.roll);
		}
		namespace Utility
		{
			//	三軸角度で回転する回転行列作成
			INLINE void MatrixRotationYawPitchRollHelper(Matrix& m, const f32& sinYaw, const f32& cosYaw, const f32& sinPitch, const f32& cosPitch, const f32& sinRoll, const f32& cosRoll)
			{
#if USE_SIMD
				__m128 vec1, vec2, vec3;
				vec1 = _mm_set_ps(0.0f, -sinYaw,   cosYaw,  cosYaw);
				vec2 = _mm_set_ps(0.0f, cosPitch, sinRoll, cosRoll);
				vec3 = _mm_mul_ps(vec1, vec2);
				vec1 = _mm_set_ps(0.0f, 0.0f,   sinYaw,   sinYaw);
				vec2 = _mm_set_ps(0.0f, 0.0f, sinPitch, sinPitch);
				vec1 = _mm_mul_ps(vec1, vec2);
				vec2 = _mm_set_ps(0.0f, 0.0f,  cosRoll, -sinRoll);
				vec1 = _mm_mul_ps(vec1, vec2);
				vec1 = _mm_add_ps(vec3, vec1);
				m.v[0].simd = vec1;

				vec1 = _mm_set_ps(0.0f, sinPitch, cosPitch, cosPitch);
				vec2 = _mm_set_ps(0.0f,     1.0f,  cosRoll, -sinRoll);
				vec1 = _mm_mul_ps(vec1, vec2);
				m.v[1].simd = vec1;

				vec1 = _mm_set_ps(0.0f,   sinYaw,   sinYaw,   sinYaw);
				vec2 = _mm_set_ps(0.0f, sinPitch, cosPitch, cosPitch);
				vec3 = _mm_mul_ps(vec1, vec2);

				vec1 = _mm_set_ps(0.0f,   cosYaw,    cosYaw,    cosYaw);
				vec2 = _mm_set_ps(0.0f, cosPitch, -sinPitch, -sinPitch);
				vec1 = _mm_mul_ps(vec1, vec2);
				vec3 = _mm_add_ps(vec3, vec1);
				vec1 = _mm_set_ps(0.0f, 1.0f,  cosRoll, -sinRoll);
				vec1 = _mm_mul_ps(vec3, vec1);
				m.v[2].simd = vec1;
				m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
				m._11 =  cosYaw * cosRoll + -sinYaw * -sinPitch * -sinRoll;
				m._12 =  cosYaw * sinRoll + -sinYaw * -sinPitch *  cosRoll;
				m._13 = -sinYaw * cosPitch;

				m._21 =  cosPitch * -sinRoll;
				m._22 =  cosPitch *  cosRoll;
				m._23 =  sinPitch;

				m._31 = (sinYaw * cosPitch + cosYaw * -sinPitch) * -sinRoll;
				m._32 = (sinYaw * cosPitch + cosYaw * -sinPitch) *  cosRoll;
				m._33 = (sinYaw * sinPitch + cosYaw *  cosPitch);

				m._14 = m._24 = m._34 =
				m._41 = m._42 = m._43 = 0.0f;
				m._44 = 1.0f;
#endif	// ~#if USE_SIMD
			}
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationYawPitchRoll(Matrix& m, f32 yaw, f32 pitch, f32 roll)
		{
			register f32 sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
			SinCos(sinYaw,   cosYaw,   yaw);
			SinCos(sinPitch, cosPitch, pitch);
			SinCos(sinRoll,  cosRoll,  roll);

			Utility::MatrixRotationYawPitchRollHelper(m, sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll);
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationYawPitchRoll(Matrix& m, s32 yaw, s32 pitch, s32 roll)
		{
			register f32 sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
			FastMath::SinCos(sinYaw,   cosYaw,   yaw);
			FastMath::SinCos(sinPitch, cosPitch, pitch);
			FastMath::SinCos(sinRoll,  cosRoll,  roll);

			Utility::MatrixRotationYawPitchRollHelper(m, sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll);
		}
		//----------------------------------------------------------------------
		//	任意軸回転行列作成
		//----------------------------------------------------------------------
		INLINE void MatrixRotationAxis(Matrix& m, const Vector4& v, f32 rot)
		{
			register f32 s, c, rc;
			SinCos(s, c, rot);
			rc = 1.0f - c;
#if USE_SIMD
			register Vector4 vv;
			__m128 vec1, vec2, vrc;
			vrc  = _mm_set_ps1(rc);
			vec1 = _mm_set_ps(0.0f, v.z, v.y, v.x);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec1 = _mm_mul_ps(vec1, vrc);
			vec2 = _mm_set_ps(0.0f, v.y, -v.z, c);
			vec2 = _mm_mul_ps(vec2, _mm_set_ps(0.0f, s, s, 1.0f));
			vec1 = _mm_add_ps(vec1, vec2);
			m.v[0].simd = vec1;

			vec1 = _mm_set_ps(0.0f, v.z, v.y, v.x);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.y));
			vec1 = _mm_mul_ps(vec1, vrc);
			vec2 = _mm_set_ps(0.0f, -v.x, c, v.z);
			vec2 = _mm_mul_ps(vec2, _mm_set_ps(0.0f, s, 1.0f, s));
			vec1 = _mm_add_ps(vec1, vec2);
			m.v[1].simd = vec1;

			vec1 = _mm_set_ps(0.0f, v.z, v.y, v.x);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.z));
			vec1 = _mm_mul_ps(vec1, vrc);
			vec2 = _mm_set_ps(0.0f, c, v.x, -v.y);
			vec2 = _mm_mul_ps(vec2, _mm_set_ps(0.0f, 1.0f, s, s));
			vec1 = _mm_add_ps(vec1, vec2);
			m.v[2].simd = vec1;
			m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
			m._11 = v.x * v.x * rc + c;
			m._12 = v.x * v.y * rc - v.z * s;
			m._13 = v.x * v.z * rc + v.y * s;

			m._21 = v.y * v.x * rc + v.z * s;
			m._22 = v.y * v.y * rc + c;
			m._23 = v.y * v.z * rc - v.x * s;

			m._31 = v.z * v.x * rc - v.y * s;
			m._32 = v.z * v.y * rc + v.x * s;
			m._33 = v.z * v.z * rc + c;

			m._14 = m._24 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;
			m._44 = 1.0f;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	1つ目のベクトルから2つ目のベクトルに変換する行列作成
		//----------------------------------------------------------------------
		INLINE void MatrixRotationVector(Matrix& m, const Vector4& v1, const Vector4& v2)
		{
			register Vector4 v;
			//	外積により2つのベクトルに垂直なベクトルを取得
			Vec4Cross(v, v2, v1);
			Vec4Normalize(v, v);

			register f32 len1, len2, s;
			len1 = Vec4Length(v1);
			len2 = Vec4Length(v2);
			//	内積により2つのベクトルのcosθを取得
			s = Vec4Dot(v2, v1) / (len1 * len2);
			//	cos値によりラジアン角取得
			s = Math::ACos(s);

			//	変換行列の生成
			MatrixRotationAxis(m, v, s);
		}
		//----------------------------------------------------------------------
		//	クオータニオンからマトリックス作成
		//----------------------------------------------------------------------
		INLINE void MatrixRotationQuaternion(Matrix& m, const Quaternion& q)
		{
#if USE_SIMD
			register Vector4 v, vv;
			v.simd = _mm_mul_ps(q.simd, _mm_set_ps1(2.0f));

			__m128 vec1, vec2, vec3;
			vec1 = _mm_set_ps(0.0f, v.x, v.x, -v.y);
			vec2 = _mm_set_ps(0.0f, q.z, q.y,  q.y);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, -v.w, v.w, -v.z);
			vec2 = _mm_set_ps(0.0f,  q.x, q.z,  q.z);
			vec1 = _mm_mul_ps(vec1, vec2);
			vv.simd = _mm_add_ps(vec3, vec1);
			vv.x = 1.0f + vv.x;
			m.v[0] = vv;

			vec1 = _mm_set_ps(0.0f, v.y, -v.x, v.x);
			vec2 = _mm_set_ps(0.0f, q.z,  q.x, q.y);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, v.w, -v.z, -v.w);
			vec2 = _mm_set_ps(0.0f, q.x,  q.z,  q.z);
			vec1 = _mm_mul_ps(vec1, vec2);
			vv.simd = _mm_add_ps(vec3, vec1);
			vv.y = 1.0f + vv.y;
			m.v[1] = vv;

			vec1 = _mm_set_ps(0.0f, -v.x, v.y, v.x);
			vec2 = _mm_set_ps(0.0f,  q.x, q.z, q.z);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, -v.y, -v.w, v.w);
			vec2 = _mm_set_ps(0.0f,  q.y,  q.x, q.y);
			vec1 = _mm_mul_ps(vec1, vec2);
			vv.simd = _mm_add_ps(vec3, vec1);
			vv.z = 1.0f + vv.z;
			m.v[2] = vv;
			m.v[3].simd = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
#else	// ~#if USE_SIMD
			register f32 ww, xx, yy, zz;
			ww = 2.0f * q.w;
			xx = 2.0f * q.x;
			yy = 2.0f * q.y;
			zz = 2.0f * q.z;

			m._11 = 1.0f - yy * q.y - zz * q.z;
			m._12 =        xx * q.y + ww * q.z;
			m._13 =        xx * q.z - ww * q.x;

			m._21 =        xx * q.y - ww * q.z;
			m._22 = 1.0f - xx * q.x - zz * q.z;
			m._23 =        yy * q.z + ww * q.x;

			m._31 =        xx * q.z + ww * q.y;
			m._32 =        yy * q.z - ww * q.x;
			m._33 = 1.0f - xx * q.x - yy * q.y;

			m._14 = m._24 = m._34 =
			m._41 = m._42 = m._43 = 0.0f;
			m._44 = 1.0f;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ビュー行列作成
		//----------------------------------------------------------------------
		INLINE void MatrixLoockAt(Matrix& m, const Vector4& eye, const Vector4& at, const Vector4& up)
		{
			register Vector4 x, y, z;
			Vec4Subtract(z, at, eye);
			Vec4Normalize(z, z);

			Vec4Cross(x, up, z);
			Vec4Normalize(x, x);

			Vec4Cross(y, z, x);

			m._11 = x.x; m._12 = y.x; m._13 = z.x;
			m._21 = x.y; m._22 = y.y; m._23 = z.y;
			m._31 = x.z; m._32 = y.z; m._33 = z.z;

			register Vector4 sub_eye = -eye;
			m._41 = Vec4Dot(sub_eye, x);
			m._42 = Vec4Dot(sub_eye, y);
			m._43 = Vec4Dot(sub_eye, z);

			m._14 = m._24 = m._34 = 0.0f;
			m._44 = 1.0f;
		}
		//----------------------------------------------------------------------
		//	射影行列作成
		//----------------------------------------------------------------------
		INLINE void MatrixPerspectiveFovLH(Matrix& m, f32 fFovy, f32 fAspect, f32 fZNear, f32 fZFar)
		{
			register f32 f, s, c;
			Math::SinCos(s, c, fFovy * 0.5f);
			f = c / s;

			register f32 nf1, nf2;
			nf1 = 1.0f / (fZNear - fZFar);
			nf2 = fZNear + fZFar;

			m.m[0][0] = f / fAspect;
			m.m[0][1] = 0.0f;
			m.m[0][2] = 0.0f;
			m.m[0][3] = 0.0f;
			m.m[1][0] = 0.0f;
			m.m[1][1] = f;
			m.m[1][2] = 0.0f;
			m.m[1][3] = 0.0f;
			m.m[2][0] = 0.0f;
			m.m[2][1] = 0.0f;
			m.m[2][2] = nf1 * nf2;
			m.m[2][3] = -1.0f;
			m.m[3][0] = 0.0f;
			m.m[3][1] = 0.0f;
			m.m[3][2] = 2.0f * fZNear * fZFar * nf1;
			m.m[3][3] = 0.0f;
		}
	}
}

#endif	// __KGL_MATRIX_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
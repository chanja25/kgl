//----------------------------------------------------------------------
//!
//!	@file	kgl.Matrix4x3.inline.h
//!	@brief	行列関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATRIX4x3_INLINE_H__
#define	__KGL_MATRIX4x3_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3::Matrix4x3(void)
		: _11(1.0f), _12(0.0f), _13(0.0f)
		, _21(0.0f), _22(1.0f), _23(0.0f)
		, _31(0.0f), _32(0.0f), _33(1.0f)
		, _41(0.0f), _42(0.0f), _43(0.0f)
	{
	}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3::Matrix4x3(const Matrix4x3& m)
		: _11(m._11), _12(m._12), _13(m._13)
		, _21(m._21), _22(m._22), _23(m._23)
		, _31(m._31), _32(m._32), _33(m._33)
		, _41(m._41), _42(m._42), _43(m._43)
	{
	}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3::Matrix4x3(	f32 _11, f32 _12, f32 _13,
										f32 _21, f32 _22, f32 _23,
										f32 _31, f32 _32, f32 _33,
										f32 _41, f32 _42, f32 _43)
		: _11(_11), _12(_12), _13(_13)
		, _21(_21), _22(_22), _23(_23)
		, _31(_31), _32(_32), _33(_33)
		, _41(_41), _42(_42), _43(_43)
	{
	}

	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE f32& Matrix4x3::operator() (u32 row, u32 col)
	{
		return m[row][col];
	}
	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE f32 Matrix4x3::operator() (u32 row, u32 col) const
	{
		return m[row][col];
	}
	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Matrix4x3::operator() (u32 row)
	{
		return v[row];
	}
	//----------------------------------------------------------------------
	//	参照
	//----------------------------------------------------------------------
	FORCEINLINE const Vector3& Matrix4x3::operator() (u32 row) const
	{
		return v[row];
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3::operator f32* (void)
	{
		return (f32*)this;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3::operator const f32* (void) const
	{
		return (f32*)this;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3& Matrix4x3::operator = (const Matrix4x3& m)
	{
		v[0] = m.v[0];
		v[1] = m.v[1];
		v[2] = m.v[2];
		v[3] = m.v[3];
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3 Matrix4x3::operator * (const Matrix4x3& m) const
	{
		register Matrix4x3 mm;
		Math::MatrixMultiply(mm, *this, m);
		return mm;
	}
	//----------------------------------------------------------------------
	//	乗算後代入
	//----------------------------------------------------------------------
	FORCEINLINE Matrix4x3& Matrix4x3::operator *= (const Matrix4x3& m)
	{
		Math::MatrixMultiply(*this, *this, m);
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Matrix4x3::operator == (const Matrix4x3& m) const
	{
		return v[0] == m.v[0] && v[1] == m.v[1] && v[2] == m.v[2] && v[3] == m.v[3];
	}
	//----------------------------------------------------------------------
	//	不等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Matrix4x3::operator != (const Matrix4x3& m) const
	{
		return v[0] != m.v[0] || v[1] != m.v[1] || v[2] != m.v[2] || v[3] != m.v[3];
	}
	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Matrix4x3::IsNan(void) const
	{
		return v[0].IsNan() || v[1].IsNan() || v[2].IsNan() || v[3].IsNan();
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	単位行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixIdentity(Matrix4x3& m)
		{
			m._12 = m._13 =
			m._21 = m._23 =
			m._31 = m._32 =
			m._41 = m._42 = m._43 = 0.0f;
			m._11 = m._22 = m._33 = 1.0f;
		}
		//----------------------------------------------------------------------
		//	逆行列作成
		//----------------------------------------------------------------------
		INLINE void MatrixInverse(Matrix4x3& m, const Matrix4x3& mat)
		{
			register f32 det =	MatrixDeterminant(mat);
			if( det == 0.0f )	return;

			register f32 fInvDet = 1.0f / det;
			register Matrix4x3 mm;
#if	USE_SIMD
			__m128 scale = _mm_set_ps1(fInvDet);
			__m128 vec1, vec2, vec3;
			vec1 = _mm_set_ps(0.0f, mat._12, mat._13, mat._22);
			vec2 = _mm_set_ps(0.0f, mat._23, mat._32, mat._33);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, mat._13, mat._12, mat._23);
			vec2 = _mm_set_ps(0.0f, mat._22, mat._33, mat._32);
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			mm.v[0].simd(_mm_mul_ps(vec1, scale));

			vec1 = _mm_set_ps(0.0f, mat._13, mat._11, mat._23);
			vec2 = _mm_set_ps(0.0f, mat._21, mat._33, mat._31);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, mat._11, mat._13, mat._21);
			vec2 = _mm_set_ps(0.0f, mat._23, mat._31, mat._33);
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			mm.v[1].simd(_mm_mul_ps(vec1, scale));

			vec1 = _mm_set_ps(0.0f, mat._11, mat._12, mat._21);
			vec2 = _mm_set_ps(0.0f, mat._23, mat._31, mat._32);
			vec3 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, mat._12, mat._11, mat._22);
			vec2 = _mm_set_ps(0.0f, mat._21, mat._32, mat._31);
			vec1 = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_sub_ps(vec3, vec1);
			mm.v[2].simd(_mm_mul_ps(vec1, scale));

			vec1 = _mm_set_ps(0.0f, mat._13, mat._12, mat._11);
			vec3 = _mm_mul_ps(vec1, _mm_set_ps1(-mat._41));
			vec1 = _mm_set_ps(0.0f, mat._23, mat._22, mat._21);
			vec2 = _mm_mul_ps(vec1, _mm_set_ps1(-mat._42));
			vec3 = _mm_add_ps(vec2, vec3);
			vec1 = _mm_set_ps(0.0f, mat._33, mat._32, mat._31);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(-mat._43));
			mm.v[3].simd(_mm_add_ps(vec1, vec3));
#else	// ~#if USE_SIMD
			mm._11 = (mat._22 * mat._33 - mat._23 * mat._32) * fInvDet;
			mm._12 = (mat._13 * mat._32 - mat._12 * mat._33) * fInvDet;
			mm._13 = (mat._12 * mat._23 - mat._13 * mat._22) * fInvDet;

			mm._21 = (mat._23 * mat._31 - mat._21 * mat._33) * fInvDet;
			mm._22 = (mat._11 * mat._33 - mat._13 * mat._31) * fInvDet;
			mm._23 = (mat._13 * mat._21 - mat._11 * mat._23) * fInvDet;

			mm._31 = (mat._21 * mat._32 - mat._22 * mat._31) * fInvDet;
			mm._32 = (mat._12 * mat._31 - mat._11 * mat._32) * fInvDet;
			mm._33 = (mat._11 * mat._23 - mat._12 * mat._21) * fInvDet;

			mm._41 = -(mat._41 * mat._11 + mat._42 * mat._21 + mat._43 * mat._31);
			mm._42 = -(mat._41 * mat._12 + mat._42 * mat._22 + mat._43 * mat._32);
			mm._43 = -(mat._41 * mat._13 + mat._42 * mat._23 + mat._43 * mat._33);
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix4x3));
		}
		//----------------------------------------------------------------------
		//	行列の積の算出(Out = M1 * M2)
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixMultiply(Matrix4x3& m, const Matrix4x3& m1, const Matrix4x3& m2)
		{
			register Matrix4x3 mm;
#if	USE_SIMD
			__m128 vec1, vec2;
			for( u32 i=0; i < 4; i++ )
			{
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][0]), m2.v[0].simd());
				vec2 = _mm_mul_ps(_mm_set_ps1(m1.m[i][1]), m2.v[1].simd());
				vec2 = _mm_add_ps(vec1, vec2);
				vec1 = _mm_mul_ps(_mm_set_ps1(m1.m[i][2]), m2.v[2].simd());
				vec1 = _mm_add_ps(vec1, vec2);
				mm.v[i].simd(vec1);
			}
#else	// ~#if USE_SIMD
			for( u32 i=0; i < 4; i++ )
			{
				mm.v[i]  = m1.m[i][0] * m2.v[0];
				mm.v[i] += m1.m[i][1] * m2.v[1];
				mm.v[i] += m1.m[i][2] * m2.v[2];
			}
#endif	// ~#if USE_SIMD
			kgMemcpy(&m, &mm, sizeof(Matrix4x3));
		}
		//----------------------------------------------------------------------
		//	3x3部分の行列式の算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 MatrixDeterminant(const Matrix4x3& m)
		{
#if USE_SIMD
			Vector4 v;
			__m128 vec1, vec2;
			vec1 = _mm_set_ps(0.0f, m.v[1].x, m.v[1].z, m.v[1].y);
			vec2 = _mm_set_ps(0.0f, m.v[2].y, m.v[2].x, m.v[2].z);
			v.simd = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, m.v[1].y, m.v[1].x, m.v[1].z);
			vec2 = _mm_set_ps(0.0f, m.v[2].x, m.v[2].z, m.v[2].y);
			vec1 = _mm_mul_ps(vec1, vec2);
			v.simd = _mm_sub_ps(v.simd, vec1);
			v.simd = _mm_add_ps(m.v[0].simd(), v.simd);

			return v.x + v.y + v.z;
#else	// ~#if USE_SIMD
			return	m._11 + (m._22 * m._33 - m._23 * m._32) +
					m._12 + (m._23 * m._31 - m._21 * m._33) +
					m._13 + (m._21 * m._32 - m._22 * m._31);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	座標変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranslation(Matrix4x3& m, const Vector4& v)
		{
			MatrixTranslation(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	座標変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranslation(Matrix4x3& m, const Vector3& v)
		{
			MatrixTranslation(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	座標変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixTranslation(Matrix4x3& m, f32 x, f32 y, f32 z)
		{
			m._12 = m._13 =
			m._21 = m._23 =
			m._31 = m._32 = 0.0f;
			m._11 = m._22 = m._33 = 1.0f;

			m._41 = x;
			m._42 = y;
			m._43 = z;
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix4x3& m, f32 s)
		{
			MatrixScaling(m, s, s, s);
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix4x3& m, const Vector3& v)
		{
			MatrixScaling(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix4x3& m, const Vector4& v)
		{
			MatrixScaling(m, v.x, v.y, v.z);
		}
		//----------------------------------------------------------------------
		//	スケール変換行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixScaling(Matrix4x3& m, f32 x, f32 y, f32 z)
		{
			m._12 = m._13 = 
			m._21 = m._23 = 
			m._31 = m._32 = 
			m._41 = m._42 = m._43 = 0.0f;

			m._11 = x; m._22 = y; m._33 = z;
		}
		//----------------------------------------------------------------------
		//	X軸回りの回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationX(Matrix4x3& m, f32 rot)
		{
			register f32 s, c;
			SinCos(s, c, rot);

			m._12 = m._13 = m._21 = m._31 = 
			m._41 = m._42 = m._43 = 0.0f;
			m._11 = 1.0f;

			m._22 = c;
			m._23 = s;
			m._32 = -s;
			m._33 = c;
		}
		//----------------------------------------------------------------------
		//	Y軸回りの回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationY(Matrix4x3& m, f32 rot)
		{
			register f32 s, c;
			SinCos(s, c, rot);

			m._12 = m._21 = m._23 = m._32 = 
			m._41 = m._42 = m._43 = 0.0f;
			m._22 = 1.0f;

			m._11 = c;
			m._13 = -s;
			m._31 = s;
			m._33 = c;
		}
		//----------------------------------------------------------------------
		//	Z軸回りの回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationZ(Matrix4x3& m, f32 rot)
		{
			register f32 s, c;
			SinCos(s, c, rot);

			m._13 = m._23 = m._31 = m._32 =
			m._41 = m._42 = m._43 = 0.0f;
			m._33 = 1.0f;

			m._11 = c;
			m._12 = s;
			m._21 = -s;
			m._22 = c;
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotation(Matrix4x3& m, const Rotation& r)
		{
			MatrixRotationYawPitchRoll(m, r.yaw, r.pitch, r.roll);
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotation(Matrix4x3& m, const LightRotation& r)
		{
			MatrixRotationYawPitchRoll(m, r.yaw, r.pitch, r.roll);
		}
		namespace Utility
		{
			//	三軸角度で回転する回転行列作成
			INLINE void MatrixRotationYawPitchRollHelper(Matrix4x3& m, const f32& sinYaw, const f32& cosYaw, const f32& sinPitch, const f32& cosPitch, const f32& sinRoll, const f32& cosRoll)
			{
#if USE_SIMD
				__m128 vec1, vec2, out;
				vec1 = _mm_set_ps(0.0f, -sinYaw,   cosYaw,  cosYaw);
				vec2 = _mm_set_ps(0.0f, cosPitch, sinRoll, cosRoll);
				out = _mm_mul_ps(vec1, vec2);
				vec1 = _mm_set_ps(0.0f, 0.0f,   sinYaw,   sinYaw);
				vec2 = _mm_set_ps(0.0f, 0.0f, sinPitch, sinPitch);
				vec1 = _mm_mul_ps(vec1, vec2);
				vec2 = _mm_set_ps(0.0f, 0.0f,  cosRoll, -sinRoll);
				vec1 = _mm_mul_ps(vec1, vec2);
				out = _mm_add_ps(out, vec1);
				m.v[0].simd(out);

				vec1 = _mm_set_ps(0.0f, sinPitch, cosPitch, cosPitch);
				vec2 = _mm_set_ps(0.0f,     1.0f,  cosRoll, -sinRoll);
				out = _mm_mul_ps(vec1, vec2);
				m.v[1].simd(out);

				vec1 = _mm_set_ps(0.0f,   sinYaw,   sinYaw,   sinYaw);
				vec2 = _mm_set_ps(0.0f, sinPitch, cosPitch, cosPitch);
				out = _mm_mul_ps(vec1, vec2);

				vec1 = _mm_set_ps(0.0f,   cosYaw,    cosYaw,    cosYaw);
				vec2 = _mm_set_ps(0.0f, cosPitch, -sinPitch, -sinPitch);
				vec1 = _mm_mul_ps(vec1, vec2);
				out = _mm_add_ps(out, vec1);
				vec1 = _mm_set_ps(0.0f, 1.0f,  cosRoll, -sinRoll);
				out = _mm_mul_ps(out, vec1);
				m.v[2].simd(out);
				m.v[3].simd(_mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f));
#else	// ~#if USE_SIMD
				m._11 =  cosYaw * cosRoll + -sinYaw * -sinPitch * -sinRoll;
				m._12 =  cosYaw * sinRoll + -sinYaw * -sinPitch *  cosRoll;
				m._13 = -sinYaw * cosPitch;

				m._21 =  cosPitch * -sinRoll;
				m._22 =  cosPitch *  cosRoll;
				m._23 =  sinPitch;

				m._31 = (sinYaw * cosPitch + cosYaw * -sinPitch) * -sinRoll;
				m._32 = (sinYaw * cosPitch + cosYaw * -sinPitch) *  cosRoll;
				m._33 = (sinYaw * sinPitch + cosYaw *  cosPitch);

				m._41 = m._42 = m._43 = 0.0f;
#endif	// ~#if USE_SIMD
			}
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationYawPitchRoll(Matrix4x3& m, f32 yaw, f32 pitch, f32 roll)
		{
			register f32 sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
			SinCos(sinYaw,   cosYaw,   yaw);
			SinCos(sinPitch, cosPitch, pitch);
			SinCos(sinRoll,  cosRoll,  roll);

			Utility::MatrixRotationYawPitchRollHelper(m, sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll);
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationYawPitchRoll(Matrix4x3& m, s32 yaw, s32 pitch, s32 roll)
		{
			register f32 sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
			FastMath::SinCos(sinYaw,   cosYaw,   yaw);
			FastMath::SinCos(sinPitch, cosPitch, pitch);
			FastMath::SinCos(sinRoll,  cosRoll,  roll);

			Utility::MatrixRotationYawPitchRollHelper(m, sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll);
		}
		//----------------------------------------------------------------------
		//	任意軸回転行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationAxis(Matrix4x3& m, const Vector4& v, f32 rot)
		{
			register f32 s, c, rc;
			SinCos(s, c, rot);
			rc = 1.0f - c;
#if USE_SIMD
			register Vector4 vv;
			__m128 vec1, vec2, vrc;
			vrc  = _mm_set_ps1(rc);
			vec1 = _mm_set_ps(0.0f, v.z, v.y, v.x);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec1 = _mm_mul_ps(vec1, vrc);
			vec2 = _mm_set_ps(0.0f, v.y, -v.z, c);
			vec2 = _mm_mul_ps(vec2, _mm_set_ps(0.0f, s, s, 1.0f));
			m.v[0].simd(_mm_add_ps(vec1, vec2));

			vec1 = _mm_set_ps(0.0f, v.z, v.y, v.x);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.y));
			vec1 = _mm_mul_ps(vec1, vrc);
			vec2 = _mm_set_ps(0.0f, -v.x, c, v.z);
			vec2 = _mm_mul_ps(vec2, _mm_set_ps(0.0f, s, 1.0f, s));
			m.v[1].simd(_mm_add_ps(vec1, vec2));

			vec1 = _mm_set_ps(0.0f, v.z, v.y, v.x);
			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.z));
			vec1 = _mm_mul_ps(vec1, vrc);
			vec2 = _mm_set_ps(0.0f, c, v.x, -v.y);
			vec2 = _mm_mul_ps(vec2, _mm_set_ps(0.0f, 1.0f, s, s));
			m.v[2].simd(_mm_add_ps(vec1, vec2));

			m._41 = m._42 = m._43 = 0.0f;
#else	// ~#if USE_SIMD
			m._11 = v.x * v.x * rc + c;
			m._12 = v.x * v.y * rc - v.z * s;
			m._13 = v.x * v.z * rc + v.y * s;

			m._21 = v.y * v.x * rc + v.z * s;
			m._22 = v.y * v.y * rc + c;
			m._23 = v.y * v.z * rc - v.x * s;

			m._31 = v.z * v.x * rc - v.y * s;
			m._32 = v.z * v.y * rc + v.x * s;
			m._33 = v.z * v.z * rc + c;

			m._41 = m._42 = m._43 = 0.0f;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	1つ目のベクトルから2つ目のベクトルに変換する行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationVector(Matrix4x3& m, const Vector4& v1, const Vector4& v2)
		{
			register Vector4 v;
			//	外積により2つのベクトルに垂直なベクトルを取得
			Vec4Cross(v, v2, v1);
			Vec4Normalize(v, v);

			register f32 len1, len2, s;
			len1 = Vec4Length(v1);
			len2 = Vec4Length(v2);
			//	内積により2つのベクトルのcosθを取得
			s = Vec4Dot(v2, v1) / (len1 * len2);
			//	cos値によりラジアン角取得
			s = Math::ACos(s);

			//	変換行列の生成
			MatrixRotationAxis(m, v, s);
		}
		//----------------------------------------------------------------------
		//	クオータニオンからマトリックス作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixRotationQuaternion(Matrix4x3& m, const Quaternion& q)
		{
#if USE_SIMD
			register Vector4 v, vv;
			v.simd = _mm_mul_ps(q.simd, _mm_set_ps1(2.0f));

			__m128 vec1, vec2;
			vec1 = _mm_set_ps(0.0f, v.x, v.x, -v.y);
			vec2 = _mm_set_ps(0.0f, q.z, q.y,  q.y);
			vv.simd = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, -v.w, v.w, -v.z);
			vec2 = _mm_set_ps(0.0f,  q.x, q.z,  q.z);
			vec1 = _mm_mul_ps(vec1, vec2);
			vv.simd = _mm_add_ps(vv.simd, vec1);
			vv.x = 1.0f + vv.x;
			m.v[0].simd(vv.simd);

			vec1 = _mm_set_ps(0.0f, v.y, -v.x, v.x);
			vec2 = _mm_set_ps(0.0f, q.z,  q.x, q.y);
			vv.simd = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, v.w, -v.z, -v.w);
			vec2 = _mm_set_ps(0.0f, q.x,  q.z,  q.z);
			vec1 = _mm_mul_ps(vec1, vec2);
			vv.simd = _mm_add_ps(vv.simd, vec1);
			vv.y = 1.0f + vv.y;
			m.v[1].simd(vv.simd);

			vec1 = _mm_set_ps(0.0f, -v.x, v.y, v.x);
			vec2 = _mm_set_ps(0.0f,  q.x, q.z, q.z);
			vv.simd = _mm_mul_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, -v.y, -v.w, v.w);
			vec2 = _mm_set_ps(0.0f,  q.y,  q.x, q.y);
			vec1 = _mm_mul_ps(vec1, vec2);
			vv.simd = _mm_add_ps(vv.simd, vec1);
			vv.z = 1.0f + vv.z;
			m.v[2].simd(vv.simd);
			m.v[3].simd(_mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f));
#else	// ~#if USE_SIMD
			register f32 ww, xx, yy, zz;
			ww = 2.0f * q.w;
			xx = 2.0f * q.x;
			yy = 2.0f * q.y;
			zz = 2.0f * q.z;

			m._11 = 1.0f - yy * q.y - zz * q.z;
			m._12 =        xx * q.y + ww * q.z;
			m._13 =        xx * q.z - ww * q.x;

			m._21 =        xx * q.y - ww * q.z;
			m._22 = 1.0f - xx * q.x - zz * q.z;
			m._23 =        yy * q.z + ww * q.x;

			m._31 =        xx * q.z + ww * q.y;
			m._32 =        yy * q.z - ww * q.x;
			m._33 = 1.0f - xx * q.x - yy * q.y;

			m._41 = m._42 = m._43 = 0.0f;
#endif	// ~#if USE_SIMD 
		}
		//----------------------------------------------------------------------
		//	ビュー行列作成
		//----------------------------------------------------------------------
		FORCEINLINE void MatrixLoockAt(Matrix4x3& m, const Vector4& eye, const Vector4& at, const Vector4& up)
		{
			register Vector4 x, y, z;
			Vec4Subtract(z, at, eye);
			Vec4Normalize(z, z);

			Vec4Cross(x, up, z);
			Vec4Normalize(x, x);

			Vec4Cross(y, z, x);

			m._11 = x.x; m._12 = y.x; m._13 = z.x;
			m._21 = x.y; m._22 = y.y; m._23 = z.y;
			m._31 = x.z; m._32 = y.z; m._33 = z.z;

			register Vector4 sub_eye = -eye;
			m._41 = Vec4Dot(sub_eye, x);
			m._42 = Vec4Dot(sub_eye, y);
			m._43 = Vec4Dot(sub_eye, z);
		}
	}
}

#endif	// __KGL_MATRIX4x3_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
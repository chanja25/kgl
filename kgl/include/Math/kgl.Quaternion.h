//----------------------------------------------------------------------
//!
//!	@file	kgl.Quaternion.h
//!	@brief	クオータニオン関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_QUATERNION_H__
#define	__KGL_QUATERNION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	//======================================================================
	//!	クオータニオン
	//======================================================================
	ALIGNED(16)
	class Quaternion
	{
	public:
		//!	コンストラクタ
		Quaternion(void);
		//!	コンストラクタ			
		Quaternion(const Quaternion& q);
		//!	コンストラクタ
		Quaternion(f32 x, f32 y, f32 z, f32 w);

		//!	キャスト
		operator f32* (void);
		//!	キャスト
		operator const f32* (void);
		//!	代入
		Quaternion& operator = (const Quaternion& q);
		//!	乗算
		Quaternion operator * (const Quaternion& q) const;
		//!	加算
		Quaternion operator + (const Quaternion& q) const;
		//!	減算
		Quaternion operator - (const Quaternion& q) const;
		//!	乗算
		Quaternion operator * (const f32 f) const;
		//!	除算
		Quaternion operator / (const f32 f) const;

		//!	乗算後代入
		Quaternion& operator *= (const Quaternion& q);
		//!	加算後代入
		Quaternion& operator += (const Quaternion& q);
		//!	減算後代入
		Quaternion& operator -= (const Quaternion& q);
		//!	乗算後代入
		Quaternion& operator *= (const f32 f);
		//!	除算後代入
		Quaternion& operator /= (const f32 f);

		//!	等号
		b8 operator == (const Quaternion& q) const;
		//!	不等号
		b8 operator != (const Quaternion& q) const;

		//!	Nanチェック
		b8 IsNan(void) const;

	public:
		union
		{
			f32	q[4];

			struct
			{
				f32	x, y, z, w;
			};

#if	USE_SIMD
			__m128	simd;
#endif	// ~#if USE_SIMD
		};
	};

	template<>
	struct HasTrivialConstructor<Quaternion> { enum { value = true }; };

	namespace Math
	{
		//!	クオータニオンの初期化
		//!	@param q	[out] 出力
		void QuaternionIdentity(Quaternion& q);

		//!	X軸回りの回転四元数作成
		//!	@param q	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void QuaternionRotationX(Quaternion& q, f32 rot);
		//!	Y軸回りの回転四元数作成
		//!	@param q	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void QuaternionRotationY(Quaternion& q, f32 rot);
		//!	Z軸回りの回転四元数作成
		//!	@param q	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void QuaternionRotationZ(Quaternion& q, f32 rot);
		//!	三軸角度で回転する四元数作成
		//!	@param q	[out] 出力
		//!	@param r	[in]  三軸角度
		void QuaternionRotationYawPitchRoll(Quaternion& q, const Rotation& r);
		//!	三軸角度で回転する四元数作成
		//!	@param q	[out] 出力
		//!	@param r	[in]  三軸角度
		void QuaternionRotationYawPitchRoll(Quaternion& q, const LightRotation& r);
		//!	三軸角度で回転する四元数作成
		//!	@param q		[out] 出力
		//!	@param yaw		[in]  ラジアン値(Yaw)
		//!	@param pitch	[in]  ラジアン値(Pitch)
		//!	@param roll		[in]  ラジアン値(Roll)
		void QuaternionRotationYawPitchRoll(Quaternion& q, f32 yaw, f32 pitch, f32 roll);
		//!	三軸角度で回転する四元数作成
		//!	@param q		[out] 出力
		//!	@param yaw		[in]  軽量角度(Yaw)
		//!	@param pitch	[in]  軽量角度(Pitch)
		//!	@param roll		[in]  軽量角度(Roll)
		void QuaternionRotationYawPitchRoll(Quaternion& q, s32 yaw, s32 pitch, s32 roll);
		//!	任意軸回転の四元数作成
		//!	@param q	[out] 出力
		//!	@param v	[in]  回転軸
		//!	@param rot	[in]  ラジアン値
		void QuaternionRotationAxis(Quaternion& q, const Vector3& v, f32 rot);
		//!	任意軸回転
		//!	@param q	[out] 出力
		//!	@param v	[in]  回転軸
		//!	@param rot	[in]  ラジアン値
		void QuaternionRotationAxis(Quaternion& q, const Vector4& v, f32 rot);
		//!	行列で四元数を回転させる
		//!	@param q	[out] 出力
		//!	@param m	[in]  回転行列
		void QuaternionRotationMatrix(Quaternion& q, const Matrix& m);
		//!	行列で四元数を回転させる
		//!	@param q	[out] 出力
		//!	@param m	[in]  回転行列
		void QuaternionRotationMatrix(Quaternion& q, const Matrix4x3& m);
		//!	行列で四元数を回転させる
		//!	@param q	[out] 出力
		//!	@param m	[in]  回転行列
		void QuaternionRotationMatrix(Quaternion& q, const RotationMatrix& m);

		//!	四元数の積の算出
		//!	@param q	[out] 出力
		//!	@param q1	[in]  クオータニオン1
		//!	@param q2	[in]  クオータニオン2
		void QuaternionMultiply(Quaternion& q, const Quaternion& q1, const Quaternion& q2);
		//!	球面線形補間
		//!	@param q	[out] 出力
		//!	@param q1	[in]  クオータニオン1
		//!	@param q2	[in]  クオータニオン2
		//!	@param t	[in]  補間値(0.0〜1.0)
		void QuaternionSlerp(Quaternion& q, const Quaternion& q1, const Quaternion& q2, f32 t);
	}
}

#endif	// __KGL_QUATERNION_H__
//=======================================================================
//	END OF FILE
//=======================================================================
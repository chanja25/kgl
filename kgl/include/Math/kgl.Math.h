//----------------------------------------------------------------------
//!
//!	@file	kgl.Math.h
//!	@brief	計算関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATH_H__
#define	__KGL_MATH_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include <DirectXMath.h>
#if	USE_SIMD
#include <smmintrin.h>
#endif	// ~#if USE_SIMD


namespace kgl
{
	namespace Math
	{
		const f32 PI				= 3.141592654f;		//!< π
		const f32 PI2				= 6.283185308f;		//!< 2π
		const f32 HalfPI			= 1.570796327f;		//!< π / 2
		const f32 InvPI				= 0.318309886f;		//!< 1.0f / π
		const f32 InvPI2			= 0.159154943f;		//!< 1.0f / 2π
		const f32 InvHalfPI			= 0.636619772f;		//!< 1.0f / 0.5π
		const f32 Radian			= 0.017453292f;		//!< Radian
		const f32 InvRadian			= 57.29577951f;		//!< 1.0f / Radian
		//	軽量角度(高速計算用)
		const s32 LightRotPI		= 32768;			//!< π = 32768
		const s32 LightRotPI2		= 65536;			//!< 2π = 65536
		const f32 LightRot			= 182.0444444f;		//!< 32768.0f / 180.0f
		const f32 InvLightRotPI		= 3.051757813e-5f;	//!< 1.0f / LightRotPI
		const f32 InvLightRotPI2	= 1.525878906e-5f;	//!< 1.0f / LightRotPI2
		const f32 InvLightRot		= 5.493164063e-3f;	//!< 1.0f / LightRot

		//!	角度をラジアン値に変換
		//!	@param degree [in] 角度
		//!	@return ラジアン値
		f32 DegreeToRadian(f32 degree);
		//!	角度を軽量角度に変換
		//!	@param degree [in] 角度
		//!	@return 軽量角度
		s32 DegreeToLightRot(f32 degree);

		//!	ラジアン値を角度に変換
		//!	@param radian [in] ラジアン値
		//!	@return 角度
		f32 RadianToDegree(f32 radian);
		//!	ラジアン値を軽量角度に変換
		//!	@param radian [in] ラジアン値
		//!	@return 軽量角度
		s32 RadianToLightRot(f32 radian);

		//!	軽量角度を角度に変換
		//!	@param lightrot [in] 軽量角度
		//!	@return 軽量角度
		f32 LightRotToDegree(s32 lightrot);
		//!	軽量角度をラジアン値に変換
		//!	@param lightrot [in] 軽量角度
		//!	@return ラジアン値
		f32 LightRotToRadian(s32 lightrot);

		//!	-πからπの範囲にラップする
		//!	@param radian [in] ラジアン値
		//!	@return -πからπにラップされた値
		f32 WrapPI(f32 radian);
		//!	0から2πの範囲にラップする
		//!	@param radian [in] ラジアン値
		//!	@return 0から2πにラップされた値
		f32 WrapPI2(f32 radian);

		//!	-πからπの範囲にラップする
		//!	@param lightrot [in] 軽量角度
		//!	@return -πからπにラップされた値
		s32 WrapLightRotPI(s32 lightrot);
		//!	0から2πの範囲にラップする
		//!	@param lightrot [in] 軽量角度
		//!	@return 0から2πにラップされた値
		s32 WrapLightRotPI2(s32 lightrot);

		//!	平方根の取得
		//!	@param f [in] 平方根を取得する値
		//!	@return 平方根
		f32 Sqrt(f32 f);
		//!	面積の取得
		//!	@param f [in] 面積を取得する値
		//!	@return 面積
		f32 Square(f32 f);
		//!	余りの取得
		//!	@param x [in] 余り算出する値
		//!	@param y [in] 割る値
		//!	@return x / yの余り
		f32 Mod(f32 x, f32 y);
		//!	べき乗の取得
		//!	@param x [in] べき乗を算出する値
		//!	@param y [in] 乗数
		//!	@return x^yの値
		f32 Pow(f32 x, f32 y);
		//!	exp
		//!	@param f [in] ラジアン値
		//!	@return exp(f)
		f32 Exp(f32 f);
		//!	log
		//!	@param f [in] ラジアン値
		//!	@return log(f)
		f32 Log(f32 f);
		//!	log10
		//!	@param f [in] ラジアン値
		//!	@return log10(f)
		f32 Log10(f32 f);

		//!	sinθの算出
		//!	@param radian [in] ラジアン値
		//!	@return sinθ
		f32 Sin(f32 radian);
		//!	cos値の算出
		//!	@param radian [in] ラジアン値
		//!	@return cosθ
		f32 Cos(f32 radian);
		//!	sinθとcosθの算出
		//!	@param sin   [out] sinθ
		//!	@param cos   [out] cosθ
		//!	@param radian [in] ラジアン値
		void SinCos(f32& sin, f32& cos, f32 radian);
		//!	tanθの算出
		//!	@param radian [in] ラジアン値
		//!	@return tanθ
		f32 Tan(f32 radian);

		//!	arcsinθの算出
		//!	@param sin [in] sinθ
		//!	@return ラジアン値
		f32 ASin(f32 sin);
		//!	arccosθの算出
		//!	@param cos [in] cosθ
		//!	@return ラジアン値
		f32 ACos(f32 cos);
		//!	arctanθの算出
		//!	@param tan [in] tanθ
		//!	@return ラジアン値
		f32 ATan(f32 tan);
		//!	x/yのarctanθの算出
		//!	@param x [in] X
		//!	@param y [in] Y
		//!	@return ラジアン値
		f32 ATan2(f32 x, f32 y);

		//!	線形補間
		//!	@param start	[in]  開始値
		//!	@param end		[in]  終了値
		//!	@param t		[in]  補間値(0.0〜1.0)
		//!	@return 補間結果
		f32 Linear(f32 start, f32 end, f32 t);
		//!	値を範囲内でリピートする
		//!	@param value [in] 値
		//!	@param min [in] 最小値
		//!	@param max [in] 最大値
		//!	@return 結果
		s32 Repeat(s32 value, s32 min, s32 max);
		//!	値を範囲内でリピートする
		//!	@param value [in] 値
		//!	@param min [in] 最小値
		//!	@param max [in] 最大値
		//!	@return 結果
		f32 Repeat(f32 value, f32 min, f32 max);

		//!	指定した値以上の最低の整数値を取得
		//!	@param f [in] 整数値を取得する値
		//!	@return f以上の最低の整数値
		f32 Ceil(f32 f);
		//!	指定した値以下の最大の整数値を取得
		//!	@param f [in] 整数値を取得する値
		//!	@return f以下の最大の整数値
		f32 Floor(f32 f);

		//!	ビットが立っている数の取得
		//!	@param n	[in] サイズ
		//!	@return	ビットが立っている数
		u32 CountBit(u32 n);
		//!	最上位ビットの取得
		//!	@param n	[in] サイズ
		//!	@return	最上位ビット
		u32 MSB(u32 n);
		//!	最下位ビットの取得
		//!	@param n	[in] サイズ
		//!	@return	最下位ビット
		u32 LSB(u32 n);

		//!	nanチェック
		//!	@param f [in] 値
		//!	@return	値がnanか？
		b8 IsNan(f32 f);
		//!	infiniteチェック
		//!	@param f [in] 値
		//!	@return	値がinfiniteか？
		b8 IsInfinite(f32 f);

		//!	フレーム数を秒に変換
		//!	@param fFrame [in] フレーム数
		//!	@param fFps   [in] FPS
		//!	@return 秒
		f32 FrameToSecond(f32 fFrame, f32 fFps = (f32)Config::TargetFPS);
		//!	秒をフレーム数に変換
		//!	@param fFrame [in] フレーム数
		//!	@param fFps   [in] FPS
		//!	@return フレーム数
		f32 SecondToFrame(f32 fSecond, f32 fFps = (f32)Config::TargetFPS);
	}

	class Vector2;
	class Vector3;
	class Vector4;
	class Rotation;
	class LightRotation;
	class Quaternion;
	class RotationMatrix;
	class Matrix3x3;
	class Matrix4x3;
	class Matrix;
}

//	Math include
#include "kgl.FastMath.h"
#include "kgl.Float16.h"
#include "kgl.Vector2.h"
#include "kgl.Vector3.h"
#include "kgl.Vector4.h"
#include "kgl.Rotation.h"
#include "kgl.LightRotation.h"
#include "kgl.Quaternion.h"
#include "kgl.RotationMatrix.h"
#include "kgl.Matrix3x3.h"
#include "kgl.Matrix4x3.h"
#include "kgl.Matrix.h"
#include "kgl.Math.declare.h"

//	inline include
#include "kgl.FastMath.inline.h"
#include "kgl.Math.inline.h"
#include "kgl.Float16.inline.h"
#include "kgl.Vector2.inline.h"
#include "kgl.Vector3.inline.h"
#include "kgl.Vector4.inline.h"
#include "kgl.Rotation.inline.h"
#include "kgl.LightRotation.inline.h"
#include "kgl.Quaternion.inline.h"
#include "kgl.RotationMatrix.inline.h"
#include "kgl.Matrix3x3.inline.h"
#include "kgl.Matrix4x3.inline.h"
#include "kgl.Matrix.inline.h"
#include "kgl.Math.Debug.h"

#endif	// __KGL_MATH_H__
//=======================================================================
//	END OF FILE
//=======================================================================
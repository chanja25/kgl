//----------------------------------------------------------------------
//!
//!	@file	kgl.Vector3.inline.h
//!	@brief	ベクター3関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_VECTOR3_INLINE_H__
#define	__KGL_VECTOR3_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Math/kgl.Vector3.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector3::Vector3(void)
		: x(0), y(0), z(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector3::Vector3(const Vector3& v)
		: x(v.x), y(v.y), z(v.z)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector3::Vector3(f32 xx, f32 yy, f32 zz)
		: x(xx), y(yy), z(zz)
	{}

	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Vector3::operator f32* (void)
	{
		return v;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Vector3::operator const f32* (void)
	{
		return v;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator = (const Vector3& v)
	{
		x = v.x; y = v.y; z = v.z;
		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator - (void) const
	{
		return Vector3(-x, -y, -z);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator + (const Vector3& v) const
	{
		register Vector3 vec;
		Math::Vec3Add(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator - (const Vector3& v) const
	{
		register Vector3 vec;
		Math::Vec3Subtract(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator * (const Vector3& v) const
	{
		register Vector3 vec;
		Math::Vec3Multiple(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator / (const Vector3& v) const
	{
		register Vector3 vec;
		Math::Vec3Division(vec, *this, v);
		return vec;
	}	
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator * (f32 s) const
	{
		register Vector3 vec;
		Math::Vec3Scale(vec, *this, s);
		return vec;
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::operator / (f32 s) const
	{
		register Vector3 vec;
		Math::Vec3Division(vec, *this, s);
		return vec;
	}	
	//----------------------------------------------------------------------
	//	加算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator += (const Vector3& v)
	{
		Math::Vec3Add(*this, *this, v);
		return *this;
	}	
	//----------------------------------------------------------------------
	//	減算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator -= (const Vector3& v)
	{
		Math::Vec3Subtract(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator *= (const Vector3& v)
	{
		Math::Vec3Multiple(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator /= (const Vector3& v)
	{
		Math::Vec3Division(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator *= (f32 s)
	{
		Math::Vec3Scale(*this, *this, s);
		return *this;
	}	
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector3& Vector3::operator /= (f32 s)
	{
		Math::Vec3Division(*this, *this, s);
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector3::operator == (const Vector3& v) const
	{
		return (x == v.x && y == v.y && z == v.z);
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector3::operator != (const Vector3& v) const
	{
		return (x != v.x || y != v.y || z != v.z);
	}
	//----------------------------------------------------------------------
	//	クリア
	//----------------------------------------------------------------------
	FORCEINLINE void Vector3::ZeroClear(void)
	{
		x = y = z = 0.0f;
	}
	//----------------------------------------------------------------------
	//	正規化
	//----------------------------------------------------------------------
	FORCEINLINE void Vector3::Normalize(void)
	{
		Math::Vec3Normalize(*this, *this);
	}
	//----------------------------------------------------------------------
	//	正規化(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::SafeNormalize(void) const
	{
		Vector3 vNormal;
		Math::Vec3Normalize(vNormal, *this);
		return vNormal;
	}
	//----------------------------------------------------------------------
	//	現在の値以上の最低の整数値に変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector3::Ceil(void)
	{
#if	USE_SIMD
		simd(_mm_ceil_ps(simd()));
#else	// ~#if USE_SIMD
		x = Math::Ceil(x);
		y = Math::Ceil(y);
		z = Math::Ceil(z);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	現在の値以上の最低の整数値に変換(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::SafeCeil(void) const
	{
		Vector3 vec(*this);
		vec.Ceil();
		return vec;
	}
	//----------------------------------------------------------------------
	//	現在の値以下の最大の整数値に変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector3::Floor(void)
	{
#if	USE_SIMD
		simd(_mm_floor_ps(simd()));
#else	// ~#if USE_SIMD
		x = Math::Floor(x);
		y = Math::Floor(y);
		z = Math::Floor(z);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	現在の値以下の最大の整数値に変換(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 Vector3::SafeFloor(void) const
	{
		Vector3 vec(*this);
		vec.Floor();
		return vec;
	}
	//----------------------------------------------------------------------
	//	長さの2乗取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector3::LengthSq(void) const
	{
		return Math::Vec3LengthSq(*this);
	}
	//----------------------------------------------------------------------
	//	長さ
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector3::Length(void) const
	{
		return Math::Vec3Length(*this);
	}
	//----------------------------------------------------------------------
	//	距離の2乗取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector3::DistanceSq(const Vector3& v) const
	{
		return Math::Vec3DistanceSq(*this, v);
	}
	//----------------------------------------------------------------------
	//	距離の取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector3::Distance(const Vector3& v) const
	{
		return Math::Vec3Distance(*this, v);
	}

	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector3::IsNan(void) const
	{
		return Math::IsNan(x) || Math::IsInfinite(x) || 
			   Math::IsNan(y) || Math::IsInfinite(y) || 
			   Math::IsNan(z) || Math::IsInfinite(z);
	}

#if	USE_SIMD
	//----------------------------------------------------------------------
	//	SIMDの値をVector3へ変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector3::simd(const __m128& simd)
	{
		kgMemcpy(v, &simd, sizeof(Vector3));
	}
	//----------------------------------------------------------------------
	//	SIMD用に値の変換
	//----------------------------------------------------------------------
	FORCEINLINE __m128 Vector3::simd(f32 w) const
	{
		return _mm_set_ps(w, z, y, x);
	}
#endif	// ~#if USE_SIMD
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector3 operator * (f32 s, const Vector3& v)
	{
		register Vector3 vec;
		Math::Vec3Scale(vec, v, s);
		return vec;
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	加算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Add(Vector3& vOut, const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_add_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x + v2.x;
			vOut.y = v1.y + v2.y;
			vOut.z = v1.z + v2.z;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	減算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Subtract(Vector3& vOut, const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_sub_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x - v2.x;
			vOut.y = v1.y - v2.y;
			vOut.z = v1.z - v2.z;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	乗算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Scale(Vector3& vOut, const Vector3& v, f32 s)
		{
#if	USE_SIMD
			vOut.simd(_mm_mul_ps(v.simd(), _mm_set_ps1(s)));
#else	// ~#if USE_SIMD
			vOut.x = v.x * s;
			vOut.y = v.y * s;
			vOut.z = v.z * s;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	除算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Division(Vector3& vOut, const Vector3& v, f32 s)
		{
#if	USE_SIMD
			vOut.simd(_mm_div_ps(v.simd(), _mm_set_ps1(s)));
#else	// ~#if USE_SIMD
			Vec3Scale(vOut, v, 1.0f / s);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	乗算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Multiple(Vector3& vOut, const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_mul_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x * v2.x;
			vOut.y = v1.y * v2.y;
			vOut.z = v1.z * v2.z;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	除算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Division(Vector3& vOut, const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_div_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x / v2.x;
			vOut.y = v1.y / v2.y;
			vOut.z = v1.z / v2.z;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	長さの2乗取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec3LengthSq(const Vector3& v)
		{
#if	USE_SIMD
			register Vector4 vv;
			vv.simd = v.simd();
			vv.simd = _mm_mul_ps(vv.simd, vv.simd);
			return (vv.x + vv.y + vv.z);
#else	// ~#if USE_SIMD
			return (v.x * v.x + v.y * v.y + v.z * v.z);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	長さ取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec3Length(const Vector3& v)
		{
			register f32 f = Vec3LengthSq(v);
			return Math::Sqrt(f);
		}
		//----------------------------------------------------------------------
		//	2点の距離の2乗取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec3DistanceSq(const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			register Vector4 v;
			v.simd = _mm_sub_ps(v1.simd(), v2.simd());
			v.simd = _mm_mul_ps(v.simd, v.simd);
			return v.x + v.y + v.z;
#else	// ~#if USE_SIMD
			register Vector3 v;
			Vec3Subtract(v, v1, v2);
			return Vec3LengthSq(v);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	2点の距離取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec3Distance(const Vector3& v1, const Vector3& v2)
		{
			register f32 f = Vec3DistanceSq(v1, v2);
			return Math::Sqrt(f);
		}
		//----------------------------------------------------------------------
		//	正規化
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Normalize(Vector3& vOut, const Vector3& v)
		{
			register f32 s = Vec3LengthSq(v);
			if( s > 0.0f )
			{
				Vec3Division(vOut, v, Math::Sqrt(s));
			}
		}
		//----------------------------------------------------------------------
		//	外積
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Cross(Vector3& vOut, const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			__m128 vec1	= _mm_set_ps(0.0f, v1.x, v1.z, v1.y);
			__m128 vec2	= _mm_set_ps(0.0f, v2.y, v2.x, v2.z);
			__m128 vec3	= _mm_set_ps(0.0f, v1.y, v1.x, v1.z);
			__m128 vec4	= _mm_set_ps(0.0f, v2.x, v2.z, v2.y);
			__m128 out1	= _mm_mul_ps(vec1, vec2);
			__m128 out2	= _mm_mul_ps(vec3, vec4);

			vOut.simd(_mm_sub_ps(out2, out1));
#else	// ~#if USE_SIMD
			register Vector3 v;
			v.x = v1.y * v2.z - v1.z * v2.y;
			v.y = v1.z * v2.x - v1.x * v2.z;
			v.z = v1.x * v2.y - v1.y * v2.x;
			kgMemcpy(&vOut, &v, sizeof(Vector3));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	内積
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec3Dot(const Vector3& v1, const Vector3& v2)
		{
#if	USE_SIMD
			register Vector4 v;
			v.simd = _mm_mul_ps(v1.simd(), v2.simd());
			return v.x + v.y + v.z;
#else	// ~#if USE_SIMD
			return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	線形補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Lerp(Vector3& vOut, const Vector3& v1, const Vector3& v2, f32 t)
		{
#if	USE_SIMD
			__m128 out, vec1 = v1.simd();
			out = _mm_sub_ps(v2.simd(), vec1);
			out = _mm_mul_ps(out, _mm_set_ps1(t));
			out = _mm_add_ps(out, vec1);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 v;
			Vec3Subtract(v, v2, v1);
			Vec3Scale(v, v, t);

			Vec3Add(vOut, v, v1);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	エルミート補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Hermite(Vector3& vOut, const Vector3& p1, const Vector3& t1, const Vector3& p2, const Vector3& t2, f32 s)
		{
			register f32 s2, s3, a1, a2, b1, b2;
			s2 = s * s;
			s3 = s2 * s;
			a1 =  2.0f * s3 - 3.0f * s2 + 1.0f;
			a2 = -2.0f * s3 + 3.0f * s2;
			b1 = s3 - 2.0f * s2 + s;
			b2 = s3 - s2;
#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(p1.simd(), _mm_set_ps1(a1));
			__m128 vec2 = _mm_mul_ps(p2.simd(), _mm_set_ps1(a2));
			__m128 vec3	= _mm_mul_ps(t1.simd(), _mm_set_ps1(b1));
			__m128 vec4	= _mm_mul_ps(t2.simd(), _mm_set_ps1(b2));

			__m128 out;
			out = _mm_add_ps(vec1, vec2);
			out	= _mm_add_ps(out, vec3);
			out	= _mm_add_ps(out, vec4);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 v, vv;
			Vec3Scale(v,  p1, a1);
			Vec3Scale(vv, p2, a2);
			Vec3Add(vOut, v, vv);

			Vec3Scale(v,  t1, b1);
			Vec3Scale(vv, t2, b2);
			Vec3Add(vOut, vOut, v);
			Vec3Add(vOut, vOut, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Bezier(Vector3& vOut, const Vector3& p1, const Vector3& p2, const Vector3& p3, f32 s)
		{
			register f32 ss, a1, a2, a3;
			ss = (1.0f-s);
			a1 = ss * ss;
			a2 = 2.0f * ss * s;
			a3 = s * s;
#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(p1.simd(), _mm_set_ps1(a1));
			__m128 vec2	= _mm_mul_ps(p2.simd(), _mm_set_ps1(a2));
			__m128 vec3	= _mm_mul_ps(p3.simd(), _mm_set_ps1(a3));

			__m128 out;
			out = _mm_add_ps(vec1, vec2);
			out = _mm_add_ps(out, vec3);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 v, vv;
			Vec3Scale(v,  p1, a1);
			Vec3Scale(vv, p2, a2);
			Vec3Add(v, v, vv);

			Vec3Scale(vv, p3, a3);
			Vec3Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Bezier(Vector3& vOut, const Vector3& p1, const Vector3& p2, const Vector3& p3, const Vector3& p4, f32 s)
		{
			register f32 ss, a1, a2, a3, a4;
			ss = (1.0f-s);
			a1 = ss * ss * ss;
			a2 = 3.0f * s * ss * ss;
			a3 = 3.0f * s * s * ss;
			a4 = s * s * s;
#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(p1.simd(), _mm_set_ps1(a1));
			__m128 vec2	= _mm_mul_ps(p2.simd(), _mm_set_ps1(a2));
			__m128 vec3	= _mm_mul_ps(p3.simd(), _mm_set_ps1(a3));
			__m128 vec4	= _mm_mul_ps(p4.simd(), _mm_set_ps1(a4));

			__m128 out;
			out = _mm_add_ps(vec1, vec2);
			out = _mm_add_ps(out, vec3);
			out = _mm_add_ps(out, vec4);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 v, vv;
			Vec3Scale(v,  p1, a1);
			Vec3Scale(vv, p2, a2);
			Vec3Add(v, v, vv);

			Vec3Scale(vv, p3, a3);
			Vec3Add(v, v, vv);
			Vec3Scale(vv, p4, a4);
			Vec3Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Bezier(Vector3& vOut, s32 iCount, const Vector3* p, f32 s)
		{
			register Vector3 v, vv;
			register s32 i;

			kgAssert(iCount >= 3, "ベジエ補間を行うための制御点が足りません(Vec3Bezier)");
			Vec3Bezier(v, p[0], p[1], p[2], s);
			for( i = 3; i < iCount; i ++ )
			{
				Vec3Bezier(vv, p[i-2], p[i-1], p[i], s);
				Vec3Lerp(v, v, vv, s);
			}
			kgMemcpy(&vOut, &v, sizeof(v));
		}

		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Transform(Vector3& vOut, const Vector3& v, const Matrix& m)
		{
#if	USE_SIMD
			__m128 vec1	= _mm_set_ps(0.0f, m.m[0][2], m.m[0][1], m.m[0][0]);
			__m128 vec2	= _mm_set_ps(0.0f, m.m[1][2], m.m[1][1], m.m[1][0]);
			__m128 vec3	= _mm_set_ps(0.0f, m.m[2][2], m.m[2][1], m.m[2][0]);
			__m128 vec4	= _mm_set_ps(1.0f, m.m[3][2], m.m[3][1], m.m[3][0]);

			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec2 = _mm_mul_ps(vec2, _mm_set_ps1(v.y));
			vec3 = _mm_mul_ps(vec3, _mm_set_ps1(v.z));

			__m128 out;
			out = _mm_add_ps(vec1, vec2);
			out	= _mm_add_ps(out, vec3);
			out	= _mm_add_ps(out, vec4);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0];
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1];
			vv.z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2];

			kgMemcpy(&vOut, &vv, sizeof(Vector3));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Transform(Vector3& vOut, const Vector3& v, const Matrix4x3& m)
		{
#if	USE_SIMD
			__m128 vec1	= _mm_set_ps(0.0f, m.m[0][2], m.m[0][1], m.m[0][0]);
			__m128 vec2	= _mm_set_ps(0.0f, m.m[1][2], m.m[1][1], m.m[1][0]);
			__m128 vec3	= _mm_set_ps(0.0f, m.m[2][2], m.m[2][1], m.m[2][0]);
			__m128 vec4	= _mm_set_ps(1.0f, m.m[3][2], m.m[3][1], m.m[3][0]);

			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec2 = _mm_mul_ps(vec2, _mm_set_ps1(v.y));
			vec3 = _mm_mul_ps(vec3, _mm_set_ps1(v.z));

			__m128 out;
			out = _mm_add_ps(vec1, vec2);
			out	= _mm_add_ps(out, vec3);
			out	= _mm_add_ps(out, vec4);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0];
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1];
			vv.z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2];

			kgMemcpy(&vOut, &vv, sizeof(Vector3));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3Transform(Vector3& vOut, const Vector3& v, const RotationMatrix& m)
		{
#if	USE_SIMD
			__m128 vec1	= _mm_set_ps(0.0f, m.m[0][2], m.m[0][1], m.m[0][0]);
			__m128 vec2	= _mm_set_ps(0.0f, m.m[1][2], m.m[1][1], m.m[1][0]);
			__m128 vec3	= _mm_set_ps(0.0f, m.m[2][2], m.m[2][1], m.m[2][0]);

			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec2 = _mm_mul_ps(vec2, _mm_set_ps1(v.y));
			vec3 = _mm_mul_ps(vec3, _mm_set_ps1(v.z));

			__m128 out;
			out = _mm_add_ps(vec1, vec2);
			out	= _mm_add_ps(out, vec3);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector3 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z;
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z;
			vv.z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z;

			kgMemcpy(&vOut, &vv, sizeof(Vector3));
#endif	// ~#if USE_SIMD
		}

		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3TransformCoord(Vector3& vOut, const Vector3& v, const Matrix& m)
		{
			register Vector4 vv(v.x, v.y, v.z, 1.0f);
			Vec4Transform(vv, vv, m);
#if	USE_SIMD
			vOut.simd(_mm_div_ps(vv.simd, _mm_set_ps1(vv.w)));
#else	// ~#if USE_SIMD
			register f32 inv = 1.0f / vv.w;
			vOut.x = vv.x * inv;
			vOut.y = vv.y * inv;
			vOut.z = vv.z * inv;
#endif	// ~#if USE_SIMD	
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec3TransformCoord(Vector3& vOut, const Vector3& v, const Matrix4x3& m)
		{
			register Vector4 vv(v.x, v.y, v.z, 1.0f);
			Vec4Transform(vv, vv, m);
#if	USE_SIMD
			vOut.simd(_mm_div_ps(vv.simd, _mm_set_ps1(vv.w)));
#else	// ~#if USE_SIMD
			register f32 inv = 1.0f / vv.w;
			vOut.x = vv.x * inv;
			vOut.y = vv.y * inv;
			vOut.z = vv.z * inv;
#endif	// ~#if USE_SIMD	
		}
	}
}

#endif	// __KGL_VECTOR3_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Float16.h
//!	@brief	半精度浮動小数点
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FLOAT16_H__
#define	__KGL_FLOAT16_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Math
	{
		//!	単精度浮動小数を半精度浮動小数に変換する
		//!	@param src [in] 単精度浮動小数
		//!	@return 半精度浮動小数点
		f16 Float32To16(f32 src);
		//!	半精度浮動小数を単精度浮動小数に変換する
		//!	@param src [in] 半精度浮動小数
		//!	@return 単精度浮動小数点
		f32 Float16To32(f16 src);
	}

	//======================================================================
	//!	半精度浮動少数
	//======================================================================
	class Float16
	{
	public:
		//!	コンストラクタ
		Float16(void);
		//!	コンストラクタ
		Float16(const Float16& f);
		//!	コンストラクタ
		Float16(f32 f);
		//!	コンストラクタ
		Float16(f16 f);

	public:
		//!	代入
		Float16& operator = (const Float16& f);
		//!	代入
		Float16& operator = (const f32& f);
		//!	代入
		Float16& operator = (const f16& f);

		//!	キャスト
		operator f16 (void) const;

		//!	等号
		b8 operator == (const Float16& f) const;
		//!	不等号
		b8 operator != (const Float16& f) const;

		//!	単精度浮動少数に変換
		f32 ToFloat32(void) const;

	public:
		f16	value;	//!< 値
	};

	template<>
	struct IsPODType<Float16> { enum { value = true }; };
}


#endif	// __KGL_FLOAT16_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.LightRotation.h
//!	@brief	軽量回転関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_LIGHT_ROTATION_H__
#define	__KGL_LIGHT_ROTATION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


namespace kgl
{
	//======================================================================
	//!	回転角
	//======================================================================
	class LightRotation
	{
	public:
		//!	コンストラクタ
		LightRotation(void);
		//!	コンストラクタ
		LightRotation(const LightRotation& r);
		//!	コンストラクタ
		LightRotation(const Rotation& r);
		//!	コンストラクタ
		LightRotation(s32 yaw, s32 pitch, s32 roll);
		//!	コンストラクタ
		LightRotation(f32 yaw, f32 pitch, f32 roll);

		//!	代入
		LightRotation& operator = (const LightRotation& r);
		//!	反転
		LightRotation operator - (void) const;
		//!	加算
		LightRotation operator + (const LightRotation& r) const;
		//!	減算
		LightRotation operator - (const LightRotation& r) const;
		//!	加算後代入
		LightRotation& operator += (const LightRotation& r);
		//!	減算後代入
		LightRotation& operator -= (const LightRotation& r);

		//!	等号
		b8 operator == (const LightRotation& r) const;
		//!	不等号
		b8 operator != (const LightRotation& r) const;

		//!	クリア
		void ZeroClear(void);
		//!	角度を-πからπの範囲にラップする
		void Wrap(void);

	public:
		s32	yaw;		//!< ヨー(Y軸回転)
		s32	pitch;		//!< ピッチ(X軸回転)
		s32	roll;		//!< ロール(Z軸回転)
	};

	template<>
	struct IsPODType<LightRotation> { enum { value = true }; };

	namespace Math
	{
		//!	回転角設定
		//!	@param r		[out] 出力
		//!	@param yaw		[in]  ラジアン値(Yaw)
		//!	@param pitch	[in]  ラジアン値(Pitch)
		//!	@param roll		[in]  ラジアン値(Roll)
		void RotationYawPitchRoll(LightRotation& r, f32 yaw, f32 pitch, f32 roll);
		//!	回転角設定
		//!	@param r		[out] 出力
		//!	@param yaw		[in]  軽量角度(Yaw)
		//!	@param pitch	[in]  軽量角度(Pitch)
		//!	@param roll		[in]  軽量角度(Roll)
		void RotationYawPitchRoll(LightRotation& r, s32 yaw, s32 pitch, s32 roll);
		//!	線形補間
		//!	@param r	[out] 出力
		//!	@param r1	[in]  回転角1
		//!	@param r2	[in]  回転角2
		//!	@param t	[in]  補間値(0.0〜1.0)
		void RotationLerp(LightRotation& r, const LightRotation& r1, const LightRotation& r2, f32 t);

		//!	クオータニオンから回転角作成
		//!	@param r	[out] 出力
		//!	@param q	[in]  クオータニオン
		void RotationFromQuaternion(LightRotation& r, const Quaternion& q);
		//!	行列から回転角を作成
		//!	@param r	[out] 出力
		//!	@param m	[in]  マトリックス
		void RotationFromMatrix(LightRotation& r, const Matrix& m);
		//!	行列から回転角を作成
		//!	@param r	[out] 出力
		//!	@param m	[in]  マトリックス
		void RotationFromMatrix(LightRotation& r, const Matrix4x3& m);
		//!	行列から回転角を作成
		//!	@param r	[out] 出力
		//!	@param m	[in]  マトリックス
		void RotationFromMatrix(LightRotation& r, const RotationMatrix& m);
	}
}

#endif	// __KGL_LIGHT_ROTATION_H__
//=======================================================================
//	END OF FILE
//=======================================================================
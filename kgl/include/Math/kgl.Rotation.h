//----------------------------------------------------------------------
//!
//!	@file	kgl.Rotation.h
//!	@brief	回転関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ROTATION_H__
#define	__KGL_ROTATION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


namespace kgl
{
	//======================================================================
	//!	回転角
	//======================================================================
	class Rotation
	{
	public:
		//!	コンストラクタ
		Rotation(void);
		//!	コンストラクタ			
		Rotation(const Rotation& r);
		//!	コンストラクタ			
		Rotation(const LightRotation& r);
		//!	コンストラクタ
		Rotation(f32 yaw, f32 pitch, f32 roll);

		//!	代入
		Rotation& operator = (const Rotation& r);
		//!	反転
		Rotation operator - (void) const;
		//!	加算
		Rotation operator + (const Rotation& r) const;
		//!	減算
		Rotation operator - (const Rotation& r) const;
		//!	加算後代入
		Rotation& operator += (const Rotation& r);
		//!	減算後代入
		Rotation& operator -= (const Rotation& r);

		//!	等号
		b8 operator == (const Rotation& r) const;
		//!	不等号
		b8 operator != (const Rotation& r) const;

		//!	クリア
		void ZeroClear(void);
		//!	角度を-πからπの範囲にラップする
		void Wrap(void);

		//!	Nanチェック
		b8 IsNan(void) const;

	public:
		f32	yaw;		//!< ヨー(Y軸回転)
		f32	pitch;		//!< ピッチ(X軸回転)
		f32	roll;		//!< ロール(Z軸回転)
	};

	template<>
	struct IsPODType<Rotation> { enum { value = true }; };

	namespace Math
	{
		//!	回転角設定
		//!	@param r		[out] 出力
		//!	@param yaw		[in]  ラジアン値(Yaw)
		//!	@param pitch	[in]  ラジアン値(Pitch)
		//!	@param roll		[in]  ラジアン値(Roll)
		void RotationYawPitchRoll(Rotation& r, f32 yaw, f32 pitch, f32 roll);
		//!	回転角設定
		//!	@param r		[out] 出力
		//!	@param yaw		[in]  軽量角度(Yaw)
		//!	@param pitch	[in]  軽量角度(Pitch)
		//!	@param roll		[in]  軽量角度(Roll)
		void RotationYawPitchRoll(Rotation& r, s32 yaw, s32 pitch, s32 roll);
		//!	線形補間
		//!	@param r	[out] 出力
		//!	@param r1	[in]  回転角1
		//!	@param r2	[in]  回転角2
		//!	@param t	[in]  補間値(0.0〜1.0)
		void RotationLerp(Rotation& r, const Rotation& r1, const Rotation& r2, f32 t);

		//!	クオータニオンから回転角作成
		//!	@param r	[out] 出力
		//!	@param q	[in]  クオータニオン
		void RotationFromQuaternion(Rotation& r, const Quaternion& q);
		//!	行列から回転角を作成
		//!	@param r	[out] 出力
		//!	@param m	[in]  マトリックス
		void RotationFromMatrix(Rotation& r, const Matrix& m);
		//!	行列から回転角を作成
		//!	@param r	[out] 出力
		//!	@param m	[in]  マトリックス
		void RotationFromMatrix(Rotation& r, const Matrix4x3& m);
		//!	行列から回転角を作成
		//!	@param r	[out] 出力
		//!	@param m	[in]  マトリックス
		void RotationFromMatrix(Rotation& r, const RotationMatrix& m);
	}
}

#endif	// __KGL_ROTATION_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Vector2.inline.h
//!	@brief	ベクター関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_VECTOR2_INLINE_H__
#define	__KGL_VECTOR2_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Math/kgl.Vector2.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector2::Vector2(void)
		: x(0), y(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector2::Vector2(const Vector2& v)
		: x(v.x), y(v.y)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector2::Vector2(f32 xx, f32 yy)
		: x(xx), y(yy)
	{}

	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Vector2::operator f32* (void)
	{
		return v;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Vector2::operator const f32* (void)
	{
		return v;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator = (const Vector2& v)
	{
		x = v.x; y = v.y;
		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator - (void) const
	{
		return Vector2(-x, -y);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator + (const Vector2& v) const
	{
		register Vector2 vec;
		Math::Vec2Add(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator - (const Vector2& v) const
	{
		register Vector2 vec;
		Math::Vec2Subtract(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator * (const Vector2& v) const
	{
		register Vector2 vec;
		Math::Vec2Multiple(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator / (const Vector2& v) const
	{
		register Vector2 vec;
		Math::Vec2Division(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator * (f32 s) const
	{
		register Vector2 vec;
		Math::Vec2Scale(vec, *this, s);
		return vec;
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::operator / (f32 s) const
	{
		register Vector2 vec;
		Math::Vec2Division(vec, *this, s);
		return vec;
	}
	//----------------------------------------------------------------------
	//	加算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator += (const Vector2& v)
	{
		Math::Vec2Add(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	減算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator -= (const Vector2& v)
	{
		Math::Vec2Subtract(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator *= (const Vector2& v)
	{
		Math::Vec2Multiple(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator /= (const Vector2& v)
	{
		Math::Vec2Division(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator *= (f32 s)
	{
		Math::Vec2Scale(*this, *this, s);
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector2& Vector2::operator /= (f32 s)
	{
		Math::Vec2Division(*this, *this, s);
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector2::operator == (const Vector2& v) const
	{
		return (x == v.x && y == v.y);
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector2::operator != (const Vector2& v) const
	{
		return (x != v.x || y != v.y);
	}
	//----------------------------------------------------------------------
	//	クリア
	//----------------------------------------------------------------------
	FORCEINLINE void Vector2::ZeroClear(void)
	{
		x = y = 0.0f;
	}
	//----------------------------------------------------------------------
	//	正規化
	//----------------------------------------------------------------------
	FORCEINLINE void Vector2::Normalize(void)
	{
		Math::Vec2Normalize(*this, *this);
	}
	//----------------------------------------------------------------------
	//	正規化(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::SafeNormalize(void) const
	{
		Vector2 vNormal;
		Math::Vec2Normalize(vNormal, *this);
		return vNormal;
	}
	//----------------------------------------------------------------------
	//	現在の値以上の最低の整数値に変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector2::Ceil(void)
	{
#if	USE_SIMD
		simd(_mm_ceil_ps(simd()));
#else	// ~#if USE_SIMD
		x = Math::Ceil(x);
		y = Math::Ceil(y);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	現在の値以上の最低の整数値に変換(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::SafeCeil(void) const
	{
		Vector2 vec(*this);
		vec.Ceil();
		return vec;
	}
	//----------------------------------------------------------------------
	//	現在の値以下の最大の整数値に変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector2::Floor(void)
	{
#if	USE_SIMD
		simd(_mm_floor_ps(simd()));
#else	// ~#if USE_SIMD
		x = Math::Floor(x);
		y = Math::Floor(y);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	現在の値以下の最大の整数値に変換(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 Vector2::SafeFloor(void) const
	{
		Vector2 vec(*this);
		vec.Floor();
		return vec;
	}
	//----------------------------------------------------------------------
	//	長さの2乗取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector2::LengthSq(void) const
	{
		return Math::Vec2LengthSq(*this);
	}
	//----------------------------------------------------------------------
	//	長さ
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector2::Length(void) const
	{
		return Math::Vec2Length(*this);
	}
	//----------------------------------------------------------------------
	//	距離の2乗取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector2::DistanceSq(const Vector2& v) const
	{
		return Math::Vec2DistanceSq(*this, v);
	}
	//----------------------------------------------------------------------
	//	距離
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector2::Distance(const Vector2& v) const
	{
		return Math::Vec2Distance(*this, v);
	}

	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector2::IsNan(void) const
	{
		return Math::IsNan(x) || Math::IsInfinite(x) || 
			   Math::IsNan(y) || Math::IsInfinite(y);
	}

#if	USE_SIMD
	//----------------------------------------------------------------------
	//	SIMDの値をVector3へ変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector2::simd(const __m128& simd)
	{
		kgMemcpy(v, &simd, sizeof(Vector2));
	}
	//----------------------------------------------------------------------
	//	SIMD用に値の変換
	//----------------------------------------------------------------------
	FORCEINLINE __m128 Vector2::simd(void) const
	{
		return _mm_set_ps(0.0f, 0.0f, y, x);
	}
#endif	// ~#if USE_SIMD
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector2 operator * (f32 s, const Vector2& v)
	{
		register Vector2 vec;
		Math::Vec2Scale(vec, v, s);
		return vec;
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	加算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Add(Vector2& vOut, const Vector2& v1, const Vector2& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_add_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x + v2.x;
			vOut.y = v1.y + v2.y;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	減算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Subtract(Vector2& vOut, const Vector2& v1, const Vector2& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_sub_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x - v2.x;
			vOut.y = v1.y - v2.y;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	乗算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Scale(Vector2& vOut, const Vector2& v, f32 s)
		{
#if	USE_SIMD
			vOut.simd(_mm_mul_ps(v.simd(), _mm_set_ps1(s)));
#else	// ~#if USE_SIMD
			vOut.x = v.x * s;
			vOut.y = v.y * s;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	除算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Division(Vector2& vOut, const Vector2& v, f32 s)
		{
#if	USE_SIMD
			vOut.simd(_mm_div_ps(v.simd(), _mm_set_ps1(s)));
#else	// ~#if USE_SIMD
			Vec2Scale(vOut, v, 1.0f / s);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	乗算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Multiple(Vector2& vOut, const Vector2& v1, const Vector2& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_mul_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x * v2.x;
			vOut.y = v1.y * v2.x;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	除算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Division(Vector2& vOut, const Vector2& v1, const Vector2& v2)
		{
#if	USE_SIMD
			vOut.simd(_mm_div_ps(v1.simd(), v2.simd()));
#else	// ~#if USE_SIMD
			vOut.x = v1.x / v2.x;
			vOut.y = v1.y / v2.x;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	長さの2乗取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec2LengthSq(const Vector2& v)
		{
#if	USE_SIMD
			register Vector4 vv;
			vv.simd = v.simd();
			vv.simd = _mm_mul_ps(vv.simd, vv.simd);
			return (vv.x + vv.y + vv.z);
#else	// ~#if USE_SIMD
			return (v.x * v.x + v.y * v.y);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	長さ取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec2Length(const Vector2& v)
		{
			register f32 f = Vec2LengthSq(v);
			return Math::Sqrt(f);
		}
		//----------------------------------------------------------------------
		//	2点の距離の2乗取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec2DistanceSq(const Vector2& v1, const Vector2& v2)
		{
#if	USE_SIMD
			register Vector4 v;
			v.simd = _mm_sub_ps(v1.simd(), v2.simd());
			v.simd = _mm_mul_ps(v.simd, v.simd);
			return v.x + v.y;
#else	// ~#if USE_SIDM
			register Vector2 v;
			Vec2Subtract(v, v1, v2);
			return Vec2LengthSq(v);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	2点の距離取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec2Distance(const Vector2& v1, const Vector2& v2)
		{
			register f32 f = Vec2DistanceSq(v1, v2);
			return Math::Sqrt(f);
		}
		//----------------------------------------------------------------------
		//	正規化
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Normalize(Vector2& vOut, const Vector2& v)
		{
			register f32 s = Vec2LengthSq(v);
			if( s > 0.0f )
			{
				Vec2Division(vOut, v, Math::Sqrt(s));
			}
		}
		//----------------------------------------------------------------------
		//	内積
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec2Dot(const Vector2& v1, const Vector2& v2)
		{
#if	USE_SIMD
			register Vector4 v;
			v.simd = _mm_mul_ps(v1.simd(), v2.simd());
			return v.x + v.y;
#else	// ~#if USE_SIMD
			return v1.x * v2.x + v1.y * v2.y;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	線形補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Lerp(Vector2& vOut, const Vector2& v1, const Vector2& v2, f32 t)
		{
#if	USE_SIMD
			__m128 out, vec1 = v1.simd();
			out = _mm_sub_ps(v2.simd(), vec1);
			out = _mm_mul_ps(out, _mm_set_ps1(t));
			out = _mm_add_ps(out, vec1);
			vOut.simd(out);
#else	// ~#if USE_SIMD
			register Vector2 v;
			Vec2Subtract(v, v2, v1);
			Vec2Scale(v, v, t);

			Vec2Add(vOut, v, v1);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	エルミート補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Hermite(Vector2& vOut, const Vector2& p1, const Vector2& t1, const Vector2& p2, const Vector2& t2, f32 s)
		{
			register f32 s2, s3, a1, a2, b1, b2;
			s2 = s * s;
			s3 = s2 * s;
			a1 =  2.0f * s3 - 3.0f * s2 + 1.0f;
			a2 = -2.0f * s3 + 3.0f * s2;
			b1 = s3 - 2.0f * s2 + s;
			b2 = s3 - s2;

#if	USE_SIMD
			__m128 vec1 = _mm_set_ps(p2.y, p2.x, p1.y, p1.x);
			__m128 vec2 = _mm_set_ps(t2.y, t2.x, t1.y, t1.x);
			__m128 scale1 = _mm_set_ps(a2, a2, a1, a1);
			__m128 scale2 = _mm_set_ps(b2, b2, b1, b1);

			vec1 = _mm_mul_ps(vec1, scale1);
			vec2 = _mm_mul_ps(vec2, scale2);
			register Vector4 v;
			v.simd = _mm_add_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, 0.0f, v.y, v.x);
			vec2 = _mm_set_ps(0.0f, 0.0f, v.w, v.z);
			vOut.simd(_mm_add_ps(vec1, vec2));
#else	// ~#if USE_SIMD
			register Vector2 v, vv;
			Vec2Scale(v,  p1, a1);
			Vec2Scale(vv, p2, a2);
			Vec2Add(v, v, vv);

			Vec2Scale(vv, t1, b1);
			Vec2Add(v, v, vv);
			Vec2Scale(vv, t2, b2);
			Vec2Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Bezier(Vector2& vOut, const Vector2& p1, const Vector2& p2, const Vector2& p3, f32 s)
		{
			register f32 ss, a1, a2, a3;
			ss = (1.0f-s);
			a1 = ss * ss;
			a2 = 2.0f * ss * s;
			a3 = s * s;

#if	USE_SIMD
			__m128 vec1 = _mm_set_ps(p2.y, p2.x, p1.y, p1.x);
			__m128 vec2 = _mm_set_ps(0.0f, 0.0f, p3.y, p3.x);
			__m128 scale1 = _mm_set_ps(a2, a2, a1, a1);
			__m128 scale2 = _mm_set_ps1(a3);

			vec1 = _mm_mul_ps(vec1, scale1);
			vec2 = _mm_mul_ps(vec2, scale2);
			register Vector4 v;
			v.simd = _mm_add_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, 0.0f, v.y, v.x);
			vec2 = _mm_set_ps(0.0f, 0.0f, v.w, v.z);
			vOut.simd(_mm_add_ps(vec1, vec2));
#else	// ~#if USE_SIMD
			register Vector2 v, vv;
			Vec2Scale(v,  p1, a1);
			Vec2Scale(vv, p2, a2);
			Vec2Add(v, v, vv);

			Vec2Scale(vv, p3, a3);
			Vec2Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Bezier(Vector2& vOut, const Vector2& p1, const Vector2& p2, const Vector2& p3, const Vector2& p4, f32 s)
		{
			register f32 ss, a1, a2, a3, a4;
			ss = (1.0f-s);
			a1 = ss * ss * ss;
			a2 = 3.0f * s * ss * ss;
			a3 = 3.0f * s * s * ss;
			a4 = s * s * s;

#if	USE_SIMD
			__m128 vec1 = _mm_set_ps(p2.y, p2.x, p1.y, p1.x);
			__m128 vec2 = _mm_set_ps(p4.y, p4.x, p3.y, p3.x);
			__m128 scale1 = _mm_set_ps(a2, a2, a1, a1);
			__m128 scale2 = _mm_set_ps(a4, a4, a3, a3);

			vec1 = _mm_mul_ps(vec1, scale1);
			vec2 = _mm_mul_ps(vec2, scale2);
			register Vector4 v;
			v.simd = _mm_add_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, 0.0f, v.y, v.x);
			vec2 = _mm_set_ps(0.0f, 0.0f, v.w, v.z);
			vOut.simd(_mm_add_ps(vec1, vec2));
#else	// ~#if USE_SIMD
			register Vector2 v, vv;
			Vec2Scale(v,  p1, a1);
			Vec2Scale(vv, p2, a2);
			Vec2Add(v, v, vv);

			Vec2Scale(vv, p3, a3);
			Vec2Add(v, v, vv);
			Vec2Scale(vv, p4, a4);
			Vec2Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Bezier(Vector2& vOut, s32 iCount, const Vector2* p, f32 s)
		{
			register Vector2 v, vv;
			register s32 i;

			kgAssert(iCount >= 3, "ベジエ補間を行うための制御点が足りません(Vec2Bezier)");
			Vec2Bezier(v, p[0], p[1], p[2], s);
			for( i = 3; i < iCount; i ++ )
			{
				Vec2Bezier(vv, p[i-2], p[i-1], p[i], s);
				Vec2Lerp(v, v, vv, s);
			}
			kgMemcpy(&vOut, &v, sizeof(v));
		}

		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec2Transform(Vector2& vOut, const Vector2& v, const Matrix3x3& m)
		{
#if	USE_SIMD
			__m128 vec1 = _mm_set_ps(m.m[1][1], m.m[1][0], m.m[0][1], m.m[0][0]);
			__m128 vec2 = _mm_set_ps(0.0f, 0.0f, m.m[2][1], m.m[2][0]);
			__m128 scale = _mm_set_ps(v.y, v.y, v.x, v.x);

			vec1 = _mm_mul_ps(vec1, scale);
			register Vector4 vv;
			vv.simd = _mm_add_ps(vec1, vec2);
			vec1 = _mm_set_ps(0.0f, 0.0f, vv.y, vv.x);
			vec2 = _mm_set_ps(0.0f, 0.0f, vv.w, vv.z);
			vOut.simd(_mm_add_ps(vec1, vec2));
#else	// ~#if USE_SIMD
			register Vector2 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0];
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1];

			kgMemcpy(&vOut, &vv, sizeof(Vector2));
#endif	// ~#if USE_SIMD
		}
	}
}

#endif	// __KGL_VECTOR2_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
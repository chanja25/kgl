//----------------------------------------------------------------------
//!
//!	@file	kgl.Vector4.inline.h
//!	@brief	ベクター関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_VECTOR4_INLINE_H__
#define	__KGL_VECTOR4_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Math/kgl.Vector4.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector4::Vector4(void)
		: x(0), y(0), z(0), w(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector4::Vector4(const Vector4& v)
#if	USE_SIMD
		: simd(v.simd)
#else	// ~#if USE_SIMD
		: x(v.x), y(v.y), z(v.z), w(v.w)
#endif	// ~#if USE_SIMD
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Vector4::Vector4(f32 xx, f32 yy, f32 zz, f32 ww)
#if	USE_SIMD
		: simd(_mm_set_ps(ww, zz, yy, xx))
#else	// ~#if USE_SIMD
		: x(xx), y(yy), z(zz), w(ww)
#endif	// ~#if USE_SIMD
	{}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Vector4::operator f32* (void)
	{
		return v;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Vector4::operator const f32* (void)
	{
		return v;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator = (const Vector4& v)
	{
#if	USE_SIMD
		simd = v.simd;
#else	// ~#if USE_SIMD
		x = v.x; y = v.y; z = v.z; w = v.w;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator - (void) const
	{
		return Vector4(-x, -y, -z, -w);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator + (const Vector4& v) const
	{
		register Vector4 vec;
		Math::Vec4Add(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator - (const Vector4& v) const
	{
		register Vector4 vec;
		Math::Vec4Subtract(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator * (const Vector4& v) const
	{
		register Vector4 vec;
		Math::Vec4Multiple(vec, *this, v);
		return vec;
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator / (const Vector4& v) const
	{
		register Vector4 vec;
		Math::Vec4Division(vec, *this, v);
		return vec;
	}	
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator * (f32 s) const
	{
		register Vector4 vec;
		Math::Vec4Scale(vec, *this, s);
		return vec;
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::operator / (f32 s) const
	{
		register Vector4 vec;
		Math::Vec4Division(vec, *this, s);
		return vec;
	}
	//----------------------------------------------------------------------
	//	加算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator += (const Vector4& v)
	{
		Math::Vec4Add(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	減算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator -= (const Vector4& v)
	{
		Math::Vec4Subtract(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator *= (const Vector4& v)
	{
		Math::Vec4Multiple(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator /= (const Vector4& v)
	{
		Math::Vec4Division(*this, *this, v);
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator *= (f32 s)
	{
		Math::Vec4Scale(*this, *this, s);
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Vector4& Vector4::operator /= (f32 s)
	{
		Math::Vec4Division(*this, *this, s);
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector4::operator == (const Vector4& v) const
	{
		return (x == v.x && y == v.y && z == v.z && w == v.w);
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector4::operator != (const Vector4& v) const
	{
		return (x != v.x || y != v.y || z != v.z || w != v.w);
	}
	//----------------------------------------------------------------------
	//	クリア
	//----------------------------------------------------------------------
	FORCEINLINE void Vector4::ZeroClear(void)
	{
		x = y = z = w = 0.0f;
	}
	//----------------------------------------------------------------------
	//	正規化
	//----------------------------------------------------------------------
	FORCEINLINE void Vector4::Normalize(void)
	{
		Math::Vec4Normalize(*this, *this);
	}
	//----------------------------------------------------------------------
	//	正規化(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::SafeNormalize(void) const
	{
		Vector4 vNormal;
		Math::Vec4Normalize(vNormal, *this);
		return vNormal;
	}
	//----------------------------------------------------------------------
	//	現在の値以上の最低の整数値に変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector4::Ceil(void)
	{
#if	USE_SIMD
		simd = _mm_ceil_ps(simd);
#else	// ~#if USE_SIMD
		x = Math::Ceil(x);
		y = Math::Ceil(y);
		z = Math::Ceil(z);
		w = Math::Ceil(w);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	現在の値以上の最低の整数値に変換(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::SafeCeil(void) const
	{
		Vector4 vec(*this);
		vec.Ceil();
		return vec;
	}
	//----------------------------------------------------------------------
	//	現在の値以下の最大の整数値に変換
	//----------------------------------------------------------------------
	FORCEINLINE void Vector4::Floor(void)
	{
#if	USE_SIMD
		simd = _mm_floor_ps(simd);
#else	// ~#if USE_SIMD
		x = Math::Floor(x);
		y = Math::Floor(y);
		z = Math::Floor(z);
		w = Math::Floor(w);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	現在の値以下の最大の整数値に変換(値を書き換えずに結果だけを返す)
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 Vector4::SafeFloor(void) const
	{
		Vector4 vec(*this);
		vec.Floor();
		return vec;
	}
	//----------------------------------------------------------------------
	//	長さの2乗取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector4::LengthSq(void) const
	{
		return Math::Vec4LengthSq(*this);
	}
	//----------------------------------------------------------------------
	//	長さ
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector4::Length(void) const
	{
		return Math::Vec4Length(*this);
	}
	//----------------------------------------------------------------------
	//	距離の2乗取得
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector4::DistanceSq(const Vector4& v) const
	{
		return Math::Vec4DistanceSq(*this, v);
	}
	//----------------------------------------------------------------------
	//	距離
	//----------------------------------------------------------------------
	FORCEINLINE f32 Vector4::Distance(const Vector4& v) const
	{
		return Math::Vec4Distance(*this, v);
	}
	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Vector4::IsNan(void) const
	{
		return Math::IsNan(x) || Math::IsInfinite(x) || 
			   Math::IsNan(y) || Math::IsInfinite(y) || 
			   Math::IsNan(z) || Math::IsInfinite(z) || 
			   Math::IsNan(w) || Math::IsInfinite(w);
	}

	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Vector4 operator * (f32 s, const Vector4& v)
	{
		register Vector4 vec;
		Math::Vec4Scale(vec, v, s);
		return vec;
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	加算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Add(Vector4& vOut, const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			vOut.simd = _mm_add_ps(v1.simd, v2.simd);
#else	// ~#if USE_SIMD
			vOut.x = v1.x + v2.x;
			vOut.y = v1.y + v2.y;
			vOut.z = v1.z + v2.z;
			vOut.w = v1.w + v2.w;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	減算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Subtract(Vector4& vOut, const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			vOut.simd = _mm_sub_ps(v1.simd, v2.simd);
#else	// ~#if USE_SIMD
			vOut.x = v1.x - v2.x;
			vOut.y = v1.y - v2.y;
			vOut.z = v1.z - v2.z;
			vOut.w = v1.w - v2.w;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	乗算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Scale(Vector4& vOut, const Vector4& v, f32 s)
		{
#if	USE_SIMD
			vOut.simd = _mm_mul_ps(v.simd, _mm_set_ps1(s));
#else	// ~#if USE_SIMD
			vOut.x = v.x * s;
			vOut.y = v.y * s;
			vOut.z = v.z * s;
			vOut.w = v.w * s;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	除算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Division(Vector4& vOut, const Vector4& v, f32 s)
		{
#if	USE_SIMD
			vOut.simd = _mm_div_ps(v.simd, _mm_set_ps1(s));
#else	// ~#if USE_SIMD
			Vec4Scale(vOut, v, 1.0f / s);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	乗算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Multiple(Vector4& vOut, const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			vOut.simd = _mm_mul_ps(v1.simd, v2.simd);
#else	// ~#if USE_SIMD
			vOut.x = v1.x * v2.x;
			vOut.y = v1.y * v2.y;
			vOut.z = v1.z * v2.z;
			vOut.w = v1.w * v2.w;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	除算
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Division(Vector4& vOut, const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			vOut.simd = _mm_div_ps(v1.simd, v2.simd);
#else	// ~#if USE_SIMD
			vOut.x = v1.x / v2.x;
			vOut.y = v1.y / v2.y;
			vOut.z = v1.z / v2.z;
			vOut.w = v1.w / v2.w;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	長さの2乗取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec4LengthSq(const Vector4& v)
		{
#if	USE_SIMD
			register Vector4 vv;
			vv.simd = _mm_mul_ps(v.simd, v.simd);
			return (vv.x + vv.y + vv.z + vv.w);
#else	// ~#if USE_SIMD
			return (v.x * v.x + v.y * v.y + v.z * v.z + v.w * v.w);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	長さ取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec4Length(const Vector4& v)
		{
			register f32 f = Vec4LengthSq(v);
			return Math::Sqrt(f);
		}
		//----------------------------------------------------------------------
		//	2点の距離の2乗取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec4DistanceSq(const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			register Vector4 v;
			v.simd = _mm_sub_ps(v1.simd, v2.simd);
			v.simd = _mm_mul_ps(v.simd, v.simd);
			return v.x + v.y + v.z + v.w;
#else	// ~#if USE_SIMD
			register Vector4 v;
			Vec4Subtract(v, v1, v2);
			return Vec4LengthSq(v);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	2点の距離取得
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec4Distance(const Vector4& v1, const Vector4& v2)
		{
			register f32 f = Vec4DistanceSq(v1, v2);
			return Math::Sqrt(f);
		}
		//----------------------------------------------------------------------
		//	正規化
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Normalize(Vector4& vOut, const Vector4& v)
		{
			register f32 s = Vec4LengthSq(v);
			if( s > 0.0f )
			{
				Vec4Division(vOut, v, Math::Sqrt(s));
			}
		}
		//----------------------------------------------------------------------
		//	外積
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Cross(Vector4& vOut, const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			__m128 vec1 = _mm_shuffle_ps(v1.simd, v1.simd, _MM_SHUFFLE(3, 0, 2, 1));
			__m128 vec2 = _mm_shuffle_ps(v2.simd, v2.simd, _MM_SHUFFLE(3, 1, 0, 2));
			__m128 vec3 = _mm_shuffle_ps(v1.simd, v1.simd, _MM_SHUFFLE(3, 1, 0, 2));
			__m128 vec4 = _mm_shuffle_ps(v2.simd, v2.simd, _MM_SHUFFLE(3, 0, 2, 1));
			__m128 out1 = _mm_mul_ps(vec1, vec2);
			__m128 out2 = _mm_mul_ps(vec3, vec4);

			vOut.simd = _mm_sub_ps(out2, out1);
			vOut.w = 0.0f;
#else	// ~#if USE_SIMD
			register Vector4 v;
			v.x = v1.y * v2.z - v1.z * v2.y;
			v.y = v1.z * v2.x - v1.x * v2.z;
			v.z = v1.x * v2.y - v1.y * v2.x;
			v.w = 0.0f;
			kgMemcpy(&vOut, &v, sizeof(Vector4));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	内積
		//----------------------------------------------------------------------
		FORCEINLINE f32 Vec4Dot(const Vector4& v1, const Vector4& v2)
		{
#if	USE_SIMD
			register Vector4 v;
			v.simd = _mm_mul_ps(v1.simd, v2.simd);
			return v.x + v.y + v.z + v.w;
#else	// ~#if USE_SIMD
			return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	線形補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Lerp(Vector4& vOut, const Vector4& v1, const Vector4& v2, f32 t)
		{
#if	USE_SIMD
			__m128 vec;
			vec = _mm_sub_ps(v2.simd, v1.simd);
			vec	= _mm_mul_ps(vec, _mm_set_ps1(t));
			vec = _mm_add_ps(vec, v1.simd);
			vOut.simd	= vec;
#else	// ~#if USE_SIMD
			register Vector4 v;
			Vec4Subtract(v, v2, v1);
			Vec4Scale(v, v, t);

			Vec4Add(vOut, v, v1);
#endif	// ~#if USE_SIMD
		}

		//----------------------------------------------------------------------
		//	Cutmull-Romスプライン補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4CatmullRom(Vector4& vOut, const Vector4& p1, const Vector4& p2, const Vector4& p3, const Vector4& p4, f32 s)
		{
			register f32 s2, s3;
			s2 = s * s;
			s3 = s2 * s;

#if	USE_SIMD
			__m128 v1 = _mm_sub_ps(p3.simd, p1.simd);
			__m128 v2 = _mm_sub_ps(p4.simd, p2.simd);
			__m128 half = _mm_set_ps1(0.5f);
			v1 = _mm_mul_ps(v1, half);
			v2 = _mm_mul_ps(v2, half);

			v2 = _mm_add_ps(v1, v2);

			__m128 m1 = _mm_sub_ps(p2.simd, p3.simd);
			m1 = _mm_mul_ps(m1, _mm_set_ps1(2.0f));
			m1 = _mm_add_ps(m1, v2);
			m1 = _mm_mul_ps(m1, _mm_set_ps1(s3));

			__m128 m2 = _mm_sub_ps(p3.simd, p2.simd);
			m2 = _mm_mul_ps(m2, _mm_set_ps1(3.0f));
			m2 = _mm_sub_ps(m2, v1);
			m2 = _mm_sub_ps(m2, v2);
			m2 = _mm_mul_ps(m2, _mm_set_ps1(s2));
			m2 = _mm_mul_ps(m2, _mm_set_ps1(s));
			m2 = _mm_add_ps(m2, v1);
			m2 = _mm_add_ps(m2, p2.simd);

			vOut.simd = _mm_add_ps(m1, m2);
#else	// ~#if USE_SIMD
			register Vector4 v1, v2;

			Vec4Subtract(v1, p3, p1);
			Vec4Subtract(v2, p4, p2);
			Vec4Scale(v1, v1, 0.5f);
			Vec4Scale(v2, v2, 0.5f);
			Vec4Add(v2, v1, v2);

			register Vector4 v, vv;
			Vec4Subtract(v, p2, p3);
			Vec4Scale(v, v, 2.0f);
			Vec4Add(v, v, v2);
			Vec4Scale(v, v, s3);

			Vec4Subtract(vv, p3, p2);
			Vec4Scale(vv, vv, 3.0f);
			Vec4Subtract(vv, vv, v1);
			Vec4Subtract(vv, vv, v2);
			Vec4Scale(vv, vv, s2);
			Vec4Scale(v1, v1, s);
			Vec4Add(vv, vv, v1);
			Vec4Add(vv, vv, p2);

			Vec4Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	エルミート補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Hermite(Vector4& vOut, const Vector4& p1, const Vector4& t1, const Vector4& p2, const Vector4& t2, f32 s)
		{
			register f32 s2, s3, a1, a2, b1, b2;
			s2 = s * s;
			s3 = s2 * s;
			a1 =  2.0f * s3 - 3.0f * s2 + 1.0f;
			a2 = -2.0f * s3 + 3.0f * s2;
			b1 = s3 - 2.0f * s2 + s;
			b2 = s3 - s2;

#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(p1.simd, _mm_set_ps1(a1));
			__m128 vec2 = _mm_mul_ps(p2.simd, _mm_set_ps1(a2));
			__m128 vec3	= _mm_mul_ps(t1.simd, _mm_set_ps1(b1));
			__m128 vec4	= _mm_mul_ps(t2.simd, _mm_set_ps1(b2));

			vOut.simd = _mm_add_ps(vec1, vec2);
			vOut.simd	= _mm_add_ps(vOut.simd, vec3);
			vOut.simd	= _mm_add_ps(vOut.simd, vec4);
#else	// ~#if USE_SIMD
			register Vector4 v, vv;
			Vec4Scale(v,  p1, a1);
			Vec4Scale(vv, p2, a2);
			Vec4Add(vOut, v, vv);

			Vec4Scale(v,  t1, b1);
			Vec4Scale(vv, t2, b2);
			Vec4Add(vOut, vOut, v);
			Vec4Add(vOut, vOut, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Bezier(Vector4& vOut, const Vector4& p1, const Vector4& p2, const Vector4& p3, f32 s)
		{
			register f32 ss, a1, a2, a3;
			ss = (1.0f-s);
			a1 = ss * ss;
			a2 = 2.0f * ss * s;
			a3 = s * s;

#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(p1.simd, _mm_set_ps1(a1));
			__m128 vec2	= _mm_mul_ps(p2.simd, _mm_set_ps1(a2));
			__m128 vec3	= _mm_mul_ps(p3.simd, _mm_set_ps1(a3));

			vOut.simd = _mm_add_ps(vec1, vec2);
			vOut.simd = _mm_add_ps(vOut.simd, vec3);
#else	// ~#if USE_SIMD
			register Vector4 v, vv;
			Vec4Scale(v,  p1, a1);
			Vec4Scale(vv, p2, a2);
			Vec4Add(v, v, vv);

			Vec4Scale(vv, p3, a3);
			Vec4Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Bezier(Vector4& vOut, const Vector4& p1, const Vector4& p2, const Vector4& p3, const Vector4& p4, f32 s)
		{
			register f32 ss, a1, a2, a3, a4;
			ss = (1.0f-s);
			a1 = ss * ss * ss;
			a2 = 3.0f * s * ss * ss;
			a3 = 3.0f * s * s * ss;
			a4 = s * s * s;

#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(p1.simd, _mm_set_ps1(a1));
			__m128 vec2	= _mm_mul_ps(p2.simd, _mm_set_ps1(a2));
			__m128 vec3	= _mm_mul_ps(p3.simd, _mm_set_ps1(a3));
			__m128 vec4	= _mm_mul_ps(p4.simd, _mm_set_ps1(a4));

			vOut.simd = _mm_add_ps(vec1, vec2);
			vOut.simd = _mm_add_ps(vOut.simd, vec3);
			vOut.simd = _mm_add_ps(vOut.simd, vec4);
#else	// ~#if USE_SIMD
			register Vector4 v, vv;
			Vec4Scale(v,  p1, a1);
			Vec4Scale(vv, p2, a2);
			Vec4Add(v, v, vv);

			Vec4Scale(vv, p3, a3);
			Vec4Add(v, v, vv);
			Vec4Scale(vv, p4, a4);
			Vec4Add(vOut, v, vv);
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	ベジエ補間
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Bezier(Vector4& vOut, s32 iCount, const Vector4* p, f32 s)
		{
			register Vector4 v, vv;
			register s32 i;

			kgAssert(iCount >= 3, "ベジエ補間を行うための制御点が足りません(Vec4Bezier)");
			Vec4Bezier(v, p[0], p[1], p[2], s);
			for( i = 3; i < iCount; i ++ )
			{
				Vec4Bezier(vv, p[i-2], p[i-1], p[i], s);
				Vec4Lerp(v, v, vv, s);
			}
			kgMemcpy(&vOut, &v, sizeof(v));
		}

		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Transform(Vector4& vOut, const Vector4& v, const Matrix& m)
		{
#if	USE_SIMD
			__m128 vec1	= _mm_mul_ps(m.v[0].simd, _mm_set_ps1(v.x));
			__m128 vec2	= _mm_mul_ps(m.v[1].simd, _mm_set_ps1(v.y));
			__m128 vec3	= _mm_mul_ps(m.v[2].simd, _mm_set_ps1(v.z));
			__m128 vec4	= _mm_mul_ps(m.v[3].simd, _mm_set_ps1(v.w));

			vOut.simd = _mm_add_ps(vec1, vec2);
			vOut.simd = _mm_add_ps(vOut.simd, vec3);
			vOut.simd = _mm_add_ps(vOut.simd, vec4);
#else	// ~#if USE_SIMD
			register Vector4 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0] * v.w;
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1] * v.w;
			vv.z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2] * v.w;
			vv.w = m.m[0][3] * v.x + m.m[1][3] * v.y + m.m[2][3] * v.z + m.m[3][3] * v.w;

			kgMemcpy(&vOut, &vv, sizeof(Vector4));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Transform(Vector4& vOut, const Vector4& v, const Matrix4x3& m)
		{
#if	USE_SIMD
			__m128 vec1	= _mm_set_ps(0.0f, m.m[0][2], m.m[0][1], m.m[0][0]);
			__m128 vec2	= _mm_set_ps(0.0f, m.m[1][2], m.m[1][1], m.m[1][0]);
			__m128 vec3	= _mm_set_ps(0.0f, m.m[2][2], m.m[2][1], m.m[2][0]);
			__m128 vec4	= _mm_set_ps(1.0f, m.m[3][2], m.m[3][1], m.m[3][0]);

			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec2 = _mm_mul_ps(vec2, _mm_set_ps1(v.y));
			vec3 = _mm_mul_ps(vec3, _mm_set_ps1(v.z));
			vec4 = _mm_mul_ps(vec4, _mm_set_ps1(v.w));

			vOut.simd = _mm_add_ps(vec1, vec2);
			vOut.simd = _mm_add_ps(vOut.simd, vec3);
			vOut.simd = _mm_add_ps(vOut.simd, vec4);
#else	// ~#if USE_SIMD
			register Vector4 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z + m.m[3][0] * v.w;
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z + m.m[3][1] * v.w;
			vv.z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z + m.m[3][2] * v.w;
			vv.w = v.w;

			kgMemcpy(&vOut, &vv, sizeof(Vector4));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Transform(Vector4& vOut, const Vector4& v, const RotationMatrix& m)
		{
#if	USE_SIMD
			f32 w = v.w;
			__m128 vec1	= _mm_set_ps(0.0f, m.m[0][2], m.m[0][1], m.m[0][0]);
			__m128 vec2	= _mm_set_ps(0.0f, m.m[1][2], m.m[1][1], m.m[1][0]);
			__m128 vec3	= _mm_set_ps(0.0f, m.m[2][2], m.m[2][1], m.m[2][0]);

			vec1 = _mm_mul_ps(vec1, _mm_set_ps1(v.x));
			vec2 = _mm_mul_ps(vec2, _mm_set_ps1(v.y));
			vec3 = _mm_mul_ps(vec3, _mm_set_ps1(v.z));

			vOut.simd = _mm_add_ps(vec1, vec2);
			vOut.simd = _mm_add_ps(vOut.simd, vec3);
			vOut.w = w;
#else	// ~#if USE_SIMD
			register Vector4 vv;
			vv.x = m.m[0][0] * v.x + m.m[1][0] * v.y + m.m[2][0] * v.z;
			vv.y = m.m[0][1] * v.x + m.m[1][1] * v.y + m.m[2][1] * v.z;
			vv.z = m.m[0][2] * v.x + m.m[1][2] * v.y + m.m[2][2] * v.z;
			vv.w = v.w;

			kgMemcpy(&vOut, &vv, sizeof(Vector4));
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Transform(Vector4& vOut, const Vector3& v, const Matrix& m)
		{
			Vec4Transform(vOut, Vector4(v.x, v.y, v.z, 1.0f), m);
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Transform(Vector4& vOut, const Vector3& v, const Matrix4x3& m)
		{
			Vec4Transform(vOut, Vector4(v.x, v.y, v.z, 1.0f), m);
		}
		//----------------------------------------------------------------------
		//	トランスフォーム
		//----------------------------------------------------------------------
		FORCEINLINE void Vec4Transform(Vector4& vOut, const Vector3& v, const RotationMatrix& m)
		{
			Vec4Transform(vOut, Vector4(v.x, v.y, v.z, 1.0f), m);
		}
	}
}

#endif	// __KGL_VECTOR4_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
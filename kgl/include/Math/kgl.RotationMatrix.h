//----------------------------------------------------------------------
//!
//!	@file	kgl.RotationMatrix.h
//!	@brief	行列関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ROTATION_MATRIX_H__
#define	__KGL_ROTATION_MATRIX_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


namespace kgl
{
	//======================================================================
	//!	行列
	//======================================================================
	class RotationMatrix
	{
	public:
		//!	コンストラクタ
		RotationMatrix(void);
		//!	コンストラクタ
		RotationMatrix(const RotationMatrix& m);
		//!	コンストラクタ
		RotationMatrix(const Rotation& r);
		//!	コンストラクタ
		RotationMatrix(const LightRotation& r);
		//!	コンストラクタ
		RotationMatrix(	f32 _11, f32 _12, f32 _13,
					f32 _21, f32 _22, f32 _23,
					f32 _31, f32 _32, f32 _33);

		//!	参照
		f32& operator() (u32 row, u32 col);
		//!	参照
		f32  operator() (u32 row, u32 col) const;
		//!	参照
		Vector3& operator() (u32 row);
		//!	参照
		const Vector3& operator() (u32 row) const;
		//!	キャスト
		operator f32* (void);
		//!	キャスト
		operator const f32* (void) const;

		//!	代入
		RotationMatrix& operator = (const RotationMatrix& m);
		//!	乗算
		RotationMatrix operator * (const RotationMatrix& m) const;

		//!	乗算後代入
		RotationMatrix& operator *= (const RotationMatrix& m);

		//!	等号
		b8 operator == (const RotationMatrix& m) const;
		//!	不等号
		b8 operator != (const RotationMatrix& m) const;

		//!	Nanチェック
		b8 IsNan(void) const;

	public:
		union
		{
			f32	m[3][3];

			struct
			{
				f32	_11, _12, _13;
				f32	_21, _22, _23;
				f32	_31, _32, _33;
			};

			struct
			{
				Vector3	v[3];
			};
		};
	};

	template<>
	struct HasTrivialConstructor<RotationMatrix> { enum { value = true }; };

	namespace Math
	{
		//!	単位行列作成
		//!	@param m	[out] 出力
		void MatrixIdentity(RotationMatrix& m);
		//!	逆行列作成
		//!	@param m	[out] 出力
		//!	@param mat	[in]  逆行列を算出する行列
		void MatrixInverse(RotationMatrix& m, const RotationMatrix& mat);
		//!	行列の積の算出(Out = M1 * M2)
		//!	@param m	[out] 出力
		//!	@param m1	[in]  行列1
		//!	@param m2	[in]  行列2
		void MatrixMultiply(RotationMatrix& m, const RotationMatrix& m1, const RotationMatrix& m2);

		//!	3x3部分の行列式の算出
		//!	@param m	[in]  行列
		//!	@return 行列式の値
		f32 MatrixDeterminant(const RotationMatrix& m);

		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  スケール値
		void MatrixScaling(RotationMatrix& m, f32 s);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  XYZのスケール値
		void MatrixScaling(RotationMatrix& m, const Vector3& v);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  XYZのスケール値
		void MatrixScaling(RotationMatrix& m, const Vector4& v);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param x	[in]  Xのスケール値
		//!	@param y	[in]  Yのスケール値
		//!	@param z	[in]  Zのスケール値
		void MatrixScaling(RotationMatrix& m, f32 x, f32 y, f32 z);

		//!	X軸回りの回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationX(RotationMatrix& m, f32 rot);
		//!	Y軸回りの回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationY(RotationMatrix& m, f32 rot);
		//!	Z軸回りの回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationZ(RotationMatrix& m, f32 rot);
		//!	三軸角度で回転する回転行列作成
		//!	@param m	[out] 出力
		//!	@param r	[in]  三軸角度
		void MatrixRotation(RotationMatrix& m, const Rotation& r);
		//!	三軸角度で回転する回転行列作成
		//!	@param m	[out] 出力
		//!	@param r	[in]  三軸角度
		void MatrixRotation(RotationMatrix& m, const LightRotation& r);
		//!	三軸角度で回転する回転行列作成
		//!	@param m		[out] 出力
		//!	@param yaw		[in]  ラジアン値(Yaw)
		//!	@param pitch	[in]  ラジアン値(Pitch)
		//!	@param roll		[in]  ラジアン値(Roll)
		void MatrixRotationYawPitchRoll(RotationMatrix& m, f32 yaw, f32 pitch, f32 roll);
		//!	三軸角度で回転する回転行列作成
		//!	@param m		[out] 出力
		//!	@param yaw		[in]  軽量角度(Yaw)
		//!	@param pitch	[in]  軽量角度(Pitch)
		//!	@param roll		[in]  軽量角度(Roll)
		void MatrixRotationYawPitchRoll(RotationMatrix& m, s32 yaw, s32 pitch, s32 roll);
		//!	任意軸回転行列作成
		//!	@param m	[out] 出力
		//!	@param v	[in]  回転軸
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationAxis(RotationMatrix& m, const Vector4& v, f32 rot);
		//!	1つ目のベクトルから2つ目のベクトルに変換する行列作成
		//!	@param m	[out] 出力
		//!	@param v1	[in]  回転開始軸
		//!	@param v2	[in]  回転後の軸
		void MatrixRotationVector(RotationMatrix& m, const Vector4& v1, const Vector4& v2);
		//!	クオータニオンから回転行列作成
		//!	@param m	[out] 出力
		//!	@param q	[in]  クオータニオン
		void MatrixRotationQuaternion(RotationMatrix& m, const Quaternion& q);
	}
}

#endif	// __KGL_ROTATION_MATRIX_H__
//=======================================================================
//	END OF FILE
//=======================================================================
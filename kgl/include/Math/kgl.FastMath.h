//----------------------------------------------------------------------
//!
//!	@file	kgl.FastMath.h
//!	@brief	�����v�Z�֘A
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FASTMATH_H__
#define	__KGL_FASTMATH_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace FastMath
	{
		//!	sin�l�̎Z�o
		//!	@param lightrot [in] �p�x
		//!	@return sin��
		f32 Sin(s32 lightrot);
		//!	cos�l�̎Z�o
		//!	@param lightrot [in] �p�x
		//!	@return cos��
		f32 Cos(s32 lightrot);
		//!	sin�Ƃ�cos�Ƃ̎Z�o
		//!	@param sin   [out] sin��
		//!	@param cos   [out] cos��
		//!	@param lightrot [in] �p�x
		void SinCos(f32& sin, f32& cos, s32 lightrot);
		//!	Tan�l�̎Z�o
		//!	@param lightrot [in] �p�x
		//!	@return Tan��
		f32 Tan(s32 lightrot);

		//!	sin�l�̎Z�o
		//!	@param radian [in] ���W�A���l
		//!	@return sin��
		f32 Sin(f32 radian);
		//!	cos�l�̎Z�o
		//!	@param radian [in] ���W�A���l
		//!	@return cos��
		f32 Cos(f32 radian);
		//!	sin�Ƃ�cos�Ƃ̎Z�o
		//!	@param sin   [out] sin��
		//!	@param cos   [out] cos��
		//!	@param radian [in] ���W�A���l
		void SinCos(f32& sin, f32& cos, f32 radian);
		//!	Tan�l�̎Z�o
		//!	@param radian [in] ���W�A���l
		//!	@return Tan��
		f32 Tan(f32 radian);
	}
}

#endif	// __KGL_FASTMATH_H__
//=======================================================================
//	END OF FILE
//=======================================================================
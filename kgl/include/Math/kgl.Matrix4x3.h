//----------------------------------------------------------------------
//!
//!	@file	kgl.Matrix4x3.h
//!	@brief	行列関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATRIX4X3_H__
#define	__KGL_MATRIX4X3_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


namespace kgl
{
	//======================================================================
	//!	行列
	//======================================================================
	ALIGNED(16)
	class Matrix4x3
	{
	public:
		//!	コンストラクタ
		Matrix4x3(void);
		//!	コンストラクタ
		Matrix4x3(const Matrix4x3& m);
		//!	コンストラクタ
		Matrix4x3(	f32 _11, f32 _12, f32 _13,
					f32 _21, f32 _22, f32 _23,
					f32 _31, f32 _32, f32 _33,
					f32 _41, f32 _42, f32 _43);

		//!	参照
		f32& operator() (u32 row, u32 col);
		//!	参照
		f32  operator() (u32 row, u32 col) const;
		//!	参照
		Vector3& operator() (u32 row);
		//!	参照
		const Vector3& operator() (u32 row) const;
		//!	キャスト
		operator f32* (void);
		//!	キャスト
		operator const f32* (void) const;

		//!	代入
		Matrix4x3& operator = (const Matrix4x3& m);
		//!	乗算
		Matrix4x3 operator * (const Matrix4x3& m) const;

		//!	乗算後代入
		Matrix4x3& operator *= (const Matrix4x3& m);

		//!	等号
		b8 operator == (const Matrix4x3& m) const;
		//!	不等号
		b8 operator != (const Matrix4x3& m) const;

		//!	Nanチェック
		b8 IsNan(void) const;

	public:
		union
		{
			f32	m[4][3];

			struct
			{
				f32	_11, _12, _13;
				f32	_21, _22, _23;
				f32	_31, _32, _33;
				f32	_41, _42, _43;
			};

			struct
			{
				Vector3	v[4];
			};
		};
	};

	template<>
	struct HasTrivialConstructor<Matrix4x3> { enum { value = true }; };

	namespace Math
	{
		//!	単位行列作成
		//!	@param m	[out] 出力
		void MatrixIdentity(Matrix4x3& m);
		//!	逆行列作成
		//!	@param m	[out] 出力
		//!	@param mat	[in]  逆行列を算出する行列
		void MatrixInverse(Matrix4x3& m, const Matrix4x3& mat);
		//!	行列の積の算出(Out = M1 * M2)
		//!	@param m	[out] 出力
		//!	@param m1	[in]  行列1
		//!	@param m2	[in]  行列2
		void MatrixMultiply(Matrix4x3& m, const Matrix4x3& m1, const Matrix4x3& m2);

		//!	3x3部分の行列式の算出
		//!	@param m	[in]  行列
		//!	@return 行列式の値
		f32 MatrixDeterminant(const Matrix4x3& m);

		//!	座標変換行列作成
		//!	@param m	[out] 出力
		//!	@param v	[in]  座標
		void MatrixTranslation(Matrix4x3& m, const Vector4& v);
		//!	座標変換行列作成
		//!	@param m	[out] 出力
		//!	@param v	[in]  座標
		void MatrixTranslation(Matrix4x3& m, const Vector3& v);
		//!	座標変換行列作成
		//!	@param m	[out] 出力
		//!	@param x	[in]  X座標
		//!	@param y	[in]  Y座標
		//!	@param z	[in]  Z座標
		void MatrixTranslation(Matrix4x3& m, f32 x, f32 y, f32 z);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  スケール値
		void MatrixScaling(Matrix4x3& m, f32 s);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  XYZのスケール値
		void MatrixScaling(Matrix4x3& m, const Vector3& v);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  XYZのスケール値
		void MatrixScaling(Matrix4x3& m, const Vector4& v);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param x	[in]  Xのスケール値
		//!	@param y	[in]  Yのスケール値
		//!	@param z	[in]  Zのスケール値
		void MatrixScaling(Matrix4x3& m, f32 x, f32 y, f32 z);

		//!	X軸回りの回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationX(Matrix4x3& m, f32 rot);
		//!	Y軸回りの回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationY(Matrix4x3& m, f32 rot);
		//!	Z軸回りの回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationZ(Matrix4x3& m, f32 rot);
		//!	三軸角度で回転する回転行列作成
		//!	@param m	[out] 出力
		//!	@param r	[in]  三軸角度
		void MatrixRotation(Matrix4x3& m, const Rotation& r);
		//!	三軸角度で回転する回転行列作成
		//!	@param m	[out] 出力
		//!	@param r	[in]  三軸角度
		void MatrixRotation(Matrix4x3& m, const LightRotation& r);
		//!	三軸角度で回転する回転行列作成
		//!	@param m		[out] 出力
		//!	@param yaw		[in]  ラジアン値(Yaw)
		//!	@param pitch	[in]  ラジアン値(Pitch)
		//!	@param roll		[in]  ラジアン値(Roll)
		void MatrixRotationYawPitchRoll(Matrix4x3& m, f32 yaw, f32 pitch, f32 roll);
		//!	三軸角度で回転する回転行列作成
		//!	@param m		[out] 出力
		//!	@param yaw		[in]  ラジアン値(Yaw)
		//!	@param pitch	[in]  ラジアン値(Pitch)
		//!	@param roll		[in]  ラジアン値(Roll)
		void MatrixRotationYawPitchRoll(Matrix4x3& m, s32 yaw, s32 pitch, s32 roll);
		//!	任意軸回転行列作成
		//!	@param m	[out] 出力
		//!	@param v	[in]  回転軸
		//!	@param rot	[in]  ラジアン値
		void MatrixRotationAxis(Matrix4x3& m, const Vector4& v, f32 rot);
		//!	1つ目のベクトルから2つ目のベクトルに変換する行列作成
		//!	@param m	[out] 出力
		//!	@param v1	[in]  回転開始軸
		//!	@param v2	[in]  回転後の軸
		void MatrixRotationVector(Matrix4x3& m, const Vector4& v1, const Vector4& v2);
		//!	クオータニオンから回転行列作成
		//!	@param m	[out] 出力
		//!	@param q	[in]  クオータニオン
		void MatrixRotationQuaternion(Matrix4x3& m, const Quaternion& q);

		//!	ビュー行列作成
		//!	@param m	[out] 出力
		//!	@param eye	[in]  視点
		//!	@param at	[in]  注視点
		//!	@param up	[in]  アップベクトル
		void MatrixLoockAtLH(Matrix4x3& m, const Vector4& eye, const Vector4& at, const Vector4& up);
	}
}

#endif	// __KGL_MATRIX4X3_H__
//=======================================================================
//	END OF FILE
//=======================================================================
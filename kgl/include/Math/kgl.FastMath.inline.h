//----------------------------------------------------------------------
//!
//!	@file	kgl.FastMath.inline.h
//!	@brief	高速計算関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FASTMATH_INLINE_H__
#define	__KGL_FASTMATH_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace FastMath
	{
		namespace Utility
		{
			//	360度内での分割数(分割数は2のべき乗にする事)
			const s32 SinTableCount = (Math::LightRotPI2 >> FASTMATH_CALC_LEVEL);	//((LightRot * 4) >> FASTMATH_CALC_LEVEL)byte
			//	90度内での分割数
			const s32 SinTableCountHaflPI = (SinTableCount >> 2);
			//	分割数の逆数
			const f32 SinTableCountInv = (1.0f / (f32)SinTableCount);
			//	サインテーブル用変数
			extern f32* pSinTable;

			//----------------------------------------------------------------------
			//	Sinテーブルのインデックス算出
			//----------------------------------------------------------------------
			FORCEINLINE s32 SinTblIndex(s32 lightrot)
			{
				//	符号部分を消す
				return (lightrot >> FASTMATH_CALC_LEVEL) & (SinTableCount-1);
			}
			//----------------------------------------------------------------------
			//	Cosテーブルのインデックス算出
			//----------------------------------------------------------------------
			FORCEINLINE s32 CosTblIndex(s32 lightrot)
			{
				//	Cos用に90度ずらしてから符号部分を消す
				return ((lightrot >> FASTMATH_CALC_LEVEL) + SinTableCountHaflPI) & (SinTableCount-1);
			}
		}

		//----------------------------------------------------------------------
		//	sinθの算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 Sin(s32 lightrot)
		{
			return Utility::pSinTable[Utility::SinTblIndex(lightrot)];
		}
		//----------------------------------------------------------------------
		//	cosθの算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 Cos(s32 lightrot)
		{
			return Utility::pSinTable[Utility::CosTblIndex(lightrot)];
		}
		//----------------------------------------------------------------------
		//	sinθとcosθの算出
		//----------------------------------------------------------------------
		FORCEINLINE void SinCos(f32& sin, f32& cos, s32 lightrot)
		{
			sin = Sin(lightrot);
			cos = Cos(lightrot);
		}
		//----------------------------------------------------------------------
		//	Tanθの算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 Tan(s32 lightrot)
		{
			register f32 s, c;
			SinCos(s, c, lightrot);

			return (c == 0)? 0: (s / c);
		}

		//----------------------------------------------------------------------
		//	sinθの算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 Sin(f32 radian)
		{
			return Sin(Math::RadianToLightRot(radian));
		}
		//----------------------------------------------------------------------
		//	cosθの算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 Cos(f32 radian)
		{
			return Cos(Math::RadianToLightRot(radian));
		}
		//----------------------------------------------------------------------
		//	sinθとcosθの算出
		//----------------------------------------------------------------------
		FORCEINLINE void SinCos(f32& sin, f32& cos, f32 radian)
		{
			SinCos(sin, cos, Math::RadianToLightRot(radian));
		}
		//----------------------------------------------------------------------
		//	Tanθの算出
		//----------------------------------------------------------------------
		FORCEINLINE f32 Tan(f32 radian)
		{
			return Tan(Math::RadianToLightRot(radian));
		}
	}
}

#endif	// __KGL_FASTMATH_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
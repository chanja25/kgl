//----------------------------------------------------------------------
//!
//!	@file	kgl.Matrix3x3.h
//!	@brief	行列関連(2D)
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATRIX3X3_H__
#define	__KGL_MATRIX3X3_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"


namespace kgl
{
	//======================================================================
	//!	行列(2D)
	//======================================================================
	class Matrix3x3
	{
	public:
		//!	コンストラクタ
		Matrix3x3(void);
		//!	コンストラクタ
		Matrix3x3(const Matrix3x3& m);
		//!	コンストラクタ
		Matrix3x3(	f32 _11, f32 _12, f32 _13,
					f32 _21, f32 _22, f32 _23,
					f32 _31, f32 _32, f32 _33);

		//!	参照
		f32& operator() (u32 row, u32 col);
		//!	参照
		f32  operator() (u32 row, u32 col) const;
		//!	参照
		Vector3& operator() (u32 row);
		//!	参照
		const Vector3& operator() (u32 row) const;
		//!	キャスト
		operator f32* (void);
		//!	キャスト
		operator const f32* (void) const;

		//!	代入
		Matrix3x3& operator = (const Matrix3x3& m);
		//!	乗算
		Matrix3x3 operator * (const Matrix3x3& m) const;

		//!	乗算後代入
		Matrix3x3& operator *= (const Matrix3x3& m);

		//!	等号
		b8 operator == (const Matrix3x3& m) const;
		//!	不等号
		b8 operator != (const Matrix3x3& m) const;

		//!	Nanチェック
		b8 IsNan(void) const;

	public:
		union
		{
			f32	m[3][3];

			struct
			{
				f32	_11, _12, _13;
				f32	_21, _22, _23;
				f32	_31, _32, _33;
			};

			struct
			{
				Vector3	v[3];
			};
		};
	};

	template<>
	struct HasTrivialConstructor<Matrix3x3> { enum { value = true }; };

	namespace Math
	{
		//!	単位行列作成
		//!	@param m	[out] 出力
		void MatrixIdentity(Matrix3x3& m);
		//!	逆行列作成
		//!	@param m	[out] 出力
		//!	@param mat	[in]  逆行列を算出する行列
		void MatrixInverse(Matrix3x3& m, const Matrix3x3& mat);
		//!	行列の積の算出(Out = M1 * M2)
		//!	@param m	[out] 出力
		//!	@param m1	[in]  行列1
		//!	@param m2	[in]  行列2
		void MatrixMultiply(Matrix3x3& m, const Matrix3x3& m1, const Matrix3x3& m2);

		//!	3x3部分の行列式の算出
		//!	@param m	[in]  行列
		//!	@return 行列式の値
		f32 MatrixDeterminant(const Matrix3x3& m);

		//!	座標変換行列作成
		//!	@param m	[out] 出力
		//!	@param v	[in]  座標
		void MatrixTranslation(Matrix3x3& m, const Vector2& v);
		//!	座標変換行列作成
		//!	@param m	[out] 出力
		//!	@param v	[in]  座標
		void MatrixTranslation(Matrix3x3& m, const Vector3& v);
		//!	座標変換行列作成
		//!	@param m	[out] 出力
		//!	@param x	[in]  X座標
		//!	@param y	[in]  Y座標
		void MatrixTranslation(Matrix3x3& m, f32 x, f32 y);

		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  スケール値
		void MatrixScaling(Matrix3x3& m, f32 s);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param s	[in]  XYZのスケール値
		void MatrixScaling(Matrix3x3& m, const Vector2& v);
		//!	スケール変換行列作成
		//!	@param m	[out] 出力
		//!	@param x	[in]  Xのスケール値
		//!	@param y	[in]  Yのスケール値
		void MatrixScaling(Matrix3x3& m, f32 x, f32 y);

		//!	回転行列作成
		//!	@param m	[out] 出力
		//!	@param rot	[in]  ラジアン値
		void MatrixRotation(Matrix3x3& m, f32 rot);
	}
}

#endif	// __KGL_MATRIX3X3_H__
//=======================================================================
//	END OF FILE
//=======================================================================
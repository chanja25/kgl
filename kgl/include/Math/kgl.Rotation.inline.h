//----------------------------------------------------------------------
//!
//!	@file	kgl.Rotation.inline.h
//!	@brief	回転関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ROTATION_INLINE_H__
#define	__KGL_ROTATION_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Math/kgl.Rotation.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Rotation::Rotation(void)
		: yaw(0), pitch(0), roll(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Rotation::Rotation(const Rotation& r)
		: yaw(r.yaw), pitch(r.pitch), roll(r.roll)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Rotation::Rotation(const LightRotation& r)
		: yaw(Math::LightRotToRadian(r.yaw)), pitch(Math::LightRotToRadian(r.pitch)), roll(Math::LightRotToRadian(r.roll))
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Rotation::Rotation(f32 _yaw, f32 _pitch, f32 _roll)
		: yaw(_yaw), pitch(_pitch), roll(_roll)
	{}

	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Rotation& Rotation::operator = (const Rotation& r)
	{
		yaw 	= r.yaw;
		pitch	= r.pitch;
		roll	= r.roll;

		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE Rotation Rotation::operator - (void) const
	{
		return Rotation(-yaw, -pitch, -roll);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Rotation Rotation::operator + (const Rotation& r) const
	{
		return Rotation(yaw + r.yaw, pitch + r.pitch, roll + r.roll);
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Rotation Rotation::operator - (const Rotation& r) const
	{
		return Rotation(yaw - r.yaw, pitch - r.pitch, roll - r.roll);
	}
	//----------------------------------------------------------------------
	//	加算後代入
	//----------------------------------------------------------------------
	FORCEINLINE Rotation& Rotation::operator += (const Rotation& r)
	{
		yaw		+= r.yaw;
		pitch	+= r.pitch;
		roll	+= r.roll;
		return *this;
	}
	//----------------------------------------------------------------------
	//	減算後代入
	//----------------------------------------------------------------------
	FORCEINLINE Rotation& Rotation::operator -= (const Rotation& r)
	{
		yaw		-= r.yaw;
		pitch	-= r.pitch;
		roll	-= r.roll;
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Rotation::operator == (const Rotation& r) const
	{
		return (yaw == r.yaw && pitch == r.pitch && roll == r.roll);
	}
	//----------------------------------------------------------------------
	//	不等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Rotation::operator != (const Rotation& r) const
	{
		return (yaw != r.yaw || pitch != r.pitch || roll != r.roll);
	}

	//----------------------------------------------------------------------
	//	クリア
	//----------------------------------------------------------------------
	FORCEINLINE void Rotation::ZeroClear(void)
	{
		yaw = pitch	= roll = 0.0f;
	}

	//----------------------------------------------------------------------
	//	角度を-πからπの範囲にラップする
	//----------------------------------------------------------------------
	FORCEINLINE void Rotation::Wrap(void)
	{
		pitch = Math::WrapPI(pitch);

		if( pitch < -Math::InvPI2 )
		{
			pitch	 =-Math::PI - pitch;
			yaw		+= Math::PI;
			roll	+= Math::PI;
		}
		else if( pitch > Math::InvPI2 )
		{
			pitch	 = Math::PI - pitch;
			yaw		+= Math::PI;
			roll	+= Math::PI;
		}

		if( Abs(pitch) > (Math::InvPI2-1e-4f) )
		{
			yaw += roll;
			roll = 0.0f;
		}
		else
		{
			roll = Math::WrapPI(roll);
		}

		yaw = Math::WrapPI(yaw);
	}

	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Rotation::IsNan(void) const
	{
		return Math::IsNan(yaw)   || Math::IsInfinite(yaw)   || 
			   Math::IsNan(pitch) || Math::IsInfinite(pitch) ||
			   Math::IsNan(roll)  || Math::IsInfinite(roll);
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	回転角設定
		//----------------------------------------------------------------------
		FORCEINLINE void RotationYawPitchRoll(Rotation& r, f32 yaw, f32 pitch, f32 roll)
		{
			r.yaw	= yaw;
			r.pitch	= pitch;
			r.roll	= roll;
		}
		//----------------------------------------------------------------------
		//	回転角設定
		//----------------------------------------------------------------------
		FORCEINLINE void RotationYawPitchRoll(Rotation& r, s32 yaw, s32 pitch, s32 roll)
		{
			r.yaw	= LightRotToRadian(yaw);
			r.pitch	= LightRotToRadian(pitch);
			r.roll	= LightRotToRadian(roll);
		}
		//----------------------------------------------------------------------
		//	線形補間
		//----------------------------------------------------------------------
		FORCEINLINE void RotationLerp(Rotation& r, const Rotation& r1, const Rotation& r2, f32 t)
		{
			r.yaw	= Linear(r1.yaw,	r2.yaw,		t);
			r.pitch	= Linear(r1.pitch,	r2.pitch,	t);
			r.roll	= Linear(r1.roll,	r2.roll,	t);
		}
		//----------------------------------------------------------------------
		//	クオータニオンから回転角作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromQuaternion(Rotation& r, const Quaternion& q)
		{
			//	sin(pitch)を取り出す
			register f32 sp = -2.0f * (q.y*q.z - q.w*q.x);

			//	ジンバルロックをチェックする
			//	数字の不正確さのためにわずかな許容範囲を与える
			if( fabs(sp) > 0.9999f )
			{
				//	真上か真下を向いている
				r.pitch	= Math::PI * sp;
				//	ヨーを計算し、ロールを0に設定する
				r.yaw	= Math::ATan2(-q.x*q.z + q.w*q.y, 0.5f - q.y*q.y - q.z*q.z);
				r.roll	= 0.0f;
			}
			else
			{
				//	角度を計算する
				r.yaw	= Math::ATan2(q.x*q.z + q.w*q.y, 0.5f - q.x*q.x - q.y*q.y);
				r.pitch	= Math::ASin(sp);
				r.roll	= Math::ATan2(q.x*q.y + q.w*q.z, 0.5f - q.x*q.x - q.z*q.z);
			}
		}
		namespace Utility
		{
			//	行列から回転角を作成
			template<typename T>
			FORCEINLINE void RotationFromMatrix(Rotation& r, const T& m)
			{
				//	m._23からsin(pitch)を取り出す
				register f32 sp = m._23;

				//	ジンバルロックをチェックする
				//	数字の不正確さのためにわずかな許容範囲を与える
				if( fabs(sp) > 0.9999f )
				{
				//	真上か真下を向いている
					r.pitch	= Math::PI * sp;
					//	ヨーを計算し、ロールを0に設定する
					r.yaw	= Math::ATan2(-m._31, m._11);
					r.roll	= 0.0f;
				}
				else
				{
					//	角度を計算する
					r.yaw	= Math::ATan2(m._13, m._33);
					r.pitch	= Math::ASin(sp);
					r.roll	= Math::ATan2(m._21, m._22);
				}
			}
		}
		//----------------------------------------------------------------------
		//	行列から回転角を作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromMatrix(Rotation& r, const Matrix& m)
		{
			Utility::RotationFromMatrix(r, m);
		}
		//----------------------------------------------------------------------
		//	行列から回転角を作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromMatrix(Rotation& r, const Matrix4x3& m)
		{
			Utility::RotationFromMatrix(r, m);
		}
		//----------------------------------------------------------------------
		//	行列から回転角を作成
		//----------------------------------------------------------------------
		FORCEINLINE void RotationFromMatrix(Rotation& r, const RotationMatrix& m)
		{
			Utility::RotationFromMatrix(r, m);
		}
	}
}

#endif	// __KGL_ROTATION_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
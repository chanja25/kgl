//----------------------------------------------------------------------
//!
//!	@file	kgl.Quaternion.inline.h
//!	@brief	ベクター関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_QUATERNION_INLINE_H__
#define	__KGL_QUATERNION_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Math/kgl.Quaternion.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion::Quaternion(void)
		: w(1.0f), x(0), y(0), z(0)
	{
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion::Quaternion(const Quaternion& q)
#if	USE_SIMD
		: simd(q.simd)
#else	// ~#if USE_SIMD
		: w(q.w), x(q.x), y(q.y), z(q.z)
#endif	// ~#if USE_SIMD
	{}	
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion::Quaternion(f32 xx, f32 yy, f32 zz, f32 ww)
		: w(ww), x(xx), y(yy), z(zz)
	{}

	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion::operator f32* (void)
	{
		return q;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion::operator const f32* (void)
	{
		return q;
	}	
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion& Quaternion::operator = (const Quaternion& q)
	{
#if	USE_SIMD
		simd = q.simd;
#else	// ~#if USE_SIMD
		w = q.w; x = q.x; y = q.y; z = q.z;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion Quaternion::operator * (const Quaternion& q) const
	{
		register Quaternion qq;
		Math::QuaternionMultiply(qq, *this, q);
		return qq;
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion Quaternion::operator + (const Quaternion& q) const
	{
#if	USE_SIMD
		register Quaternion quat;
		quat.simd = _mm_add_ps(simd, q.simd);
		return quat;
#else	// ~#if USE_SIMD
		return Quaternion(x+q.x, y+q.y, z+q.z, w+q.w);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion Quaternion::operator - (const Quaternion& q) const
	{
#if	USE_SIMD
		register Quaternion quat;
		quat.simd = _mm_sub_ps(simd, q.simd);
		return quat;
#else	// ~#if USE_SIMD
		return Quaternion(x-q.x, y-q.y, z-q.z, w-q.w);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion Quaternion::operator * (const f32 f) const
	{
#if	USE_SIMD
		register Quaternion quat;
		quat.simd = _mm_mul_ps(simd, _mm_set_ps1(f));
		return quat;
#else	// ~#if USE_SIMD
		return Quaternion(x*f, y*f, z*f, w*f);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion Quaternion::operator / (const f32 f) const
	{
#if	USE_SIMD
		register Quaternion quat;
		quat.simd = _mm_div_ps(simd, _mm_set_ps1(f));
		return quat;
#else	// ~#if USE_SIMD
		register f32 ff = 1.0f / f;
		return Quaternion(x*ff, y*ff, z*ff, w*ff);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion& Quaternion::operator *= (const Quaternion& q)
	{
		Math::QuaternionMultiply(*this, *this, q);
		return *this;
	}

	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion& Quaternion::operator += (const Quaternion& q)
	{
#if	USE_SIMD
		simd = _mm_add_ps(simd, q.simd);
#else	// ~#if USE_SIMD
		x += q.x; y += q.y; z += q.z; w += q.w;
#endif	// ~#if USE_SIMD
		return *this;
	}

	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion& Quaternion::operator -= (const Quaternion& q)
	{
#if	USE_SIMD
		simd = _mm_sub_ps(simd, q.simd);
#else	// ~#if USE_SIMD
		x -= q.x; y -= q.y; z -= q.z; w -= q.w;
#endif	// ~#if USE_SIMD
		return *this;
	}

	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion& Quaternion::operator *= (const f32 f)
	{
#if	USE_SIMD
		simd = _mm_mul_ps(simd, _mm_set_ps1(f));
#else	// ~#if USE_SIMD
		x *= f; y *= f; z *= f; w *= f;
#endif	// ~#if USE_SIMD
		return *this;
	}

	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Quaternion& Quaternion::operator /= (const f32 f)
	{
#if	USE_SIMD
		simd = _mm_div_ps(simd, _mm_set_ps1(f));
#else	// ~#if USE_SIMD
		register f32 ff = 1.0f / f;
		x *= ff; y *= ff; z *= ff; w *= ff;
#endif	// ~#if USE_SIMD
		return *this;
	}	
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Quaternion::operator == (const Quaternion& q) const
	{
		return (w == q.w && x == q.x && y == q.y && z == q.z);
	}	
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Quaternion::operator != (const Quaternion& q) const
	{
		return (w != q.w || x != q.x || y != q.y || z != q.z);
	}
	//----------------------------------------------------------------------
	//	Nanチェック
	//----------------------------------------------------------------------
	FORCEINLINE b8 Quaternion::IsNan(void) const
	{
		return Math::IsNan(x) || Math::IsInfinite(x) || 
			   Math::IsNan(y) || Math::IsInfinite(y) || 
			   Math::IsNan(z) || Math::IsInfinite(z) || 
			   Math::IsNan(w) || Math::IsInfinite(w);
	}

	namespace Math
	{
		//----------------------------------------------------------------------
		//	クオータニオンの初期化
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionIdentity(Quaternion& q)
		{
			q.x = q.y = q.z = 0.0f;
			q.w = 1.0f;
		}
		//----------------------------------------------------------------------
		//	X軸回りの回転四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationX(Quaternion& q, f32 rot)
		{
			register f32 thetaOver2 = rot * 0.5f;
			register f32 s, c;
			SinCos(s, c, thetaOver2);

			q.x = s;
			q.y = 0.0f;
			q.z = 0.0f;
			q.w = c;
		}
		//----------------------------------------------------------------------
		//	Y軸回りの回転四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationY(Quaternion& q, f32 rot)
		{
			register f32 thetaOver2 = rot * 0.5f;
			register f32 s, c;
			SinCos(s, c, thetaOver2);

			q.x = 0.0f;
			q.y = s;
			q.z = 0.0f;
			q.w = c;
		}
		//----------------------------------------------------------------------
		//	Z軸回りの回転四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationZ(Quaternion& q, f32 rot)
		{
			register f32 thetaOver2 = rot * 0.5f;
			register f32 s, c;
			SinCos(s, c, thetaOver2);

			q.x = 0.0f;
			q.y = 0.0f;
			q.z = s;
			q.w = c;
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationYawPitchRoll(Quaternion& q, const Rotation& r)
		{
			QuaternionRotationYawPitchRoll(q, r.yaw, r.pitch, r.roll);
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationYawPitchRoll(Quaternion& q, const LightRotation& r)
		{
			QuaternionRotationYawPitchRoll(q, r.yaw, r.pitch, r.roll);
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する四元数作成
		//----------------------------------------------------------------------
		INLINE void QuaternionRotationYawPitchRoll(Quaternion& q, f32 yaw, f32 pitch, f32 roll)
		{
			register f32 sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
#if	USE_SIMD
			Vector4 v;
			v.simd = _mm_set_ps(0.0f, roll, yaw, pitch);
			v.simd = _mm_mul_ps(v.simd, _mm_set_ps1(0.5f));
			SinCos(sinYaw,   cosYaw,   v.y);
			SinCos(sinPitch, cosPitch, v.x);
			SinCos(sinRoll,  cosRoll,  v.z);

			__m128 q1 = _mm_set_ps(cosRoll, cosRoll, sinRoll, cosRoll);
			__m128 q2 = _mm_set_ps(sinPitch, cosPitch, cosPitch, cosPitch);
			__m128 q3 = _mm_set_ps(cosYaw, cosYaw, cosYaw, sinYaw);
			__m128 out;
			out = _mm_mul_ps(q1, q2);
			out = _mm_mul_ps(out, q3);

			q1 = _mm_set_ps(-sinRoll, -sinRoll, cosRoll, sinRoll);
			q2 = _mm_set_ps(cosPitch, sinPitch, sinPitch, sinPitch);
			q3 = _mm_set_ps(sinYaw, sinYaw, sinYaw, cosYaw);
			q.simd = _mm_mul_ps(q1, q2);
			q.simd = _mm_mul_ps(q.simd, q3);

			q.simd = _mm_add_ps(q.simd, out);
#else	// ~#if USE_SIMD
			SinCos(sinYaw,   cosYaw,   yaw   * 0.5f);
			SinCos(sinPitch, cosPitch, pitch * 0.5f);
			SinCos(sinRoll,  cosRoll,  roll  * 0.5f);

			q.x = cosRoll * cosPitch * sinYaw + sinRoll * sinPitch * cosYaw;
			q.y = sinRoll * cosPitch * cosYaw + cosRoll * sinPitch * sinYaw;
			q.z = cosRoll * cosPitch * cosYaw - sinRoll * sinPitch * sinYaw;
			q.w = cosRoll * sinPitch * cosYaw - sinRoll * cosPitch * sinYaw;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	三軸角度で回転する四元数作成
		//----------------------------------------------------------------------
		INLINE void QuaternionRotationYawPitchRoll(Quaternion& q, s32 yaw, s32 pitch, s32 roll)
		{
			register f32 sinYaw, cosYaw, sinPitch, cosPitch, sinRoll, cosRoll;
			FastMath::SinCos(sinYaw,   cosYaw,   yaw   >> 1);
			FastMath::SinCos(sinPitch, cosPitch, pitch >> 1);
			FastMath::SinCos(sinRoll,  cosRoll,  roll  >> 1);
#if	USE_SIMD
			__m128 q1 = _mm_set_ps(cosRoll, cosRoll, sinRoll, cosRoll);
			__m128 q2 = _mm_set_ps(sinPitch, cosPitch, cosPitch, cosPitch);
			__m128 q3 = _mm_set_ps(cosYaw, cosYaw, cosYaw, sinYaw);
			__m128 out;
			out = _mm_mul_ps(q1, q2);
			out = _mm_mul_ps(out, q3);

			q1 = _mm_set_ps(-sinRoll, -sinRoll, cosRoll, sinRoll);
			q2 = _mm_set_ps(cosPitch, sinPitch, sinPitch, sinPitch);
			q3 = _mm_set_ps(sinYaw, sinYaw, sinYaw, cosYaw);
			q.simd = _mm_mul_ps(q1, q2);
			q.simd = _mm_mul_ps(q.simd, q3);

			q.simd = _mm_add_ps(q.simd, out);
#else	// ~#if USE_SIMD
			q.x = cosRoll * cosPitch * sinYaw + sinRoll * sinPitch * cosYaw;
			q.y = sinRoll * cosPitch * cosYaw + cosRoll * sinPitch * sinYaw;
			q.z = cosRoll * cosPitch * cosYaw - sinRoll * sinPitch * sinYaw;
			q.w = cosRoll * sinPitch * cosYaw - sinRoll * cosPitch * sinYaw;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	任意軸回転の四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationAxis(Quaternion& q, const Vector3& v, f32 rot)
		{
			register f32 thetaOver2 = rot * 0.5f;
			register f32 s, c;
			SinCos(s, c, thetaOver2);
#if	USE_SIMD
			__m128 scaler = _mm_set_ps(0.0f, s, s, s);
			q.simd = _mm_mul_ps(v.simd(), scaler);
			q.w = c;
#else	// ~#if USE_SIMD
			q.x = v.x * s;
			q.y = v.y * s;
			q.z = v.z * s;
			q.w = c;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	任意軸回転の四元数作成
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationAxis(Quaternion& q, const Vector4& v, f32 rot)
		{
			register f32 thetaOver2 = rot * 0.5f;
			register f32 s, c;
			SinCos(s, c, thetaOver2);
#if	USE_SIMD
			__m128 scaler = _mm_set_ps(0.0f, s, s, s);
			q.simd = _mm_mul_ps(v.simd, scaler);
			q.w = c;
#else	// ~#if USE_SIMD
			q.x = v.x * s;
			q.y = v.y * s;
			q.z = v.z * s;
			q.w = c;
#endif	// ~#if USE_SIMD
		}
		//----------------------------------------------------------------------
		//	四元数の積の算出
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionMultiply(Quaternion& q, const Quaternion& q1, const Quaternion& q2)
		{
#if	USE_SIMD
			__m128 m1 = _mm_set_ps1(q1.w);
			__m128 m2 = _mm_set_ps(q2.w, q2.z, q2.y, q2.x);
			__m128 out = _mm_mul_ps(m1, m2);

			m1 = _mm_set_ps(-q1.x, q1.z, q1.y, q1.x);
			m2 = _mm_set_ps(q2.x, q2.w, q2.w, q2.w);
			out = _mm_add_ps(out, _mm_mul_ps(m1, m2));

			m1 = _mm_set_ps(-q1.y, q1.y, q1.x, q1.z);
			m2 = _mm_set_ps(q2.y, q2.x, q2.z, q2.y);
			out = _mm_add_ps(out, _mm_mul_ps(m1, m2));

			m1 = _mm_set_ps(-q1.z, -q1.x, -q1.z, -q1.y);
			m2 = _mm_set_ps(q2.z, q2.y, q2.x, q2.z);
			q.simd = _mm_add_ps(out, _mm_mul_ps(m1, m2));
#else	// ~#if USE_SIMD
			register Quaternion qq;
			qq.x = q1.w * q2.x + q1.x * q2.w + q1.z * q2.y - q1.y * q2.z;
			qq.y = q1.w * q2.y + q1.y * q2.w + q1.x * q2.z - q1.z * q2.x;
			qq.z = q1.w * q2.z + q1.z * q2.w + q1.y * q2.x - q1.x * q2.y;
			qq.w = q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z;

			kgMemcpy(&q, &qq, sizeof(Quaternion));
#endif	// ~#if USE_SIMD
		}
		namespace Utility
		{
			//	行列で四元数を回転させる
			template<typename T>
			INLINE void QuaternionRotationMatrix(Quaternion& q, const T& m)
			{
				register s32 i = 0;
				register f32 tr, sq, mx, s;
#if	USE_SIMD
				__m128 quat1, quat2;
#endif	// ~#if USE_SIMD
				tr = m.m[0][0] + m.m[1][1] + m.m[2][2];
				if( tr > 0.0f )
				{
					sq = Math::Sqrt(tr + 1.0f);
					s = 0.5f / sq;
#if	USE_SIMD
					quat1 = _mm_set_ps(1.0f, m.m[0][1], m.m[2][0], m.m[0][1]);
					quat2 = _mm_set_ps(0.0f, m.m[2][1], m.m[0][2], m.m[1][0]);
					q.simd = _mm_sub_ps(quat1, quat2);
					q.simd = _mm_mul_ps(q.simd, _mm_set_ps(0.5f*sq, s, s, s));
#else	// ~#if USE_SIMD
					q.x = (m.m[1][2] - m.m[2][1]) * f;
					q.y = (m.m[2][0] - m.m[0][2]) * f;
					q.z = (m.m[0][1] - m.m[1][0]) * f;
					q.w = 0.5f * sq;
#endif	// ~#if USE_SIMD
				}
				else
				{
					mx = m.m[0][0];
					if( m.m[1][1] > mx ) { i = 1; mx = m.m[1][1]; }
					if( m.m[2][2] > mx ) { i = 2; }
					switch( i )
					{
					case 0:
						sq	= Math::Sqrt(m.m[0][0] - m.m[1][1] - m.m[2][2] + 1.0f);
						s = 0.5f / sq;
#if	USE_SIMD
						quat1 = _mm_set_ps( m.m[1][2], m.m[0][2], m.m[1][2], 1.0f);
						quat2 = _mm_set_ps(-m.m[2][1], m.m[2][0], m.m[2][1], 0.0f);
						q.simd = _mm_add_ps(quat1, quat2);
						q.simd = _mm_mul_ps(q.simd, _mm_set_ps(s, s, s, 0.5f*sq));
#else	// ~#if USE_SIMD
						q.x = 0.5f * sq;
						q.y = (m.m[1][0] + m.m[0][1]) * s;
						q.z = (m.m[0][2] + m.m[2][0]) * s;
						q.w = (m.m[1][2] - m.m[2][1]) * s;
						
						q.x = (m.m[1][0] + m.m[0][1]) * s;
						q.y = 0.5f * sq;
						q.z = (m.m[2][1] + m.m[1][2]) * s;
						q.w = (m.m[2][0] - m.m[0][2]) * s;

						q.x = (m.m[0][2] + m.m[2][0]) * s;
						q.y = (m.m[2][1] + m.m[1][2]) * s;
						q.z = 0.5f * sq;
						q.w = (m.m[0][1] - m.m[1][0]) * s;
#endif	// ~#if USE_SIMD
						break;
					case 1:
						sq	= Math::Sqrt(m.m[1][1] - m.m[0][0] - m.m[2][2] + 1.0f);
						s = 0.5f / sq;
#if	USE_SIMD
						quat1 = _mm_set_ps( m.m[2][0], m.m[2][1], 1.0f, m.m[1][0]);
						quat2 = _mm_set_ps(-m.m[0][2], m.m[1][2], 0.0f, m.m[0][1]);
						q.simd = _mm_add_ps(quat1, quat2);
						q.simd = _mm_mul_ps(q.simd, _mm_set_ps(s, s, 0.5f*sq, s));
#else	// ~#if USE_SIMD
						q.x = (m.m[1][0] + m.m[0][1]) * s;
						q.y = 0.5f * sq;
						q.z = (m.m[2][1] + m.m[1][2]) * s;
						q.w = (m.m[2][0] - m.m[0][2]) * s;
#endif	// ~#if USE_SIMD
						break;
					case 2:
						sq	= Math::Sqrt(m.m[2][2] - m.m[0][0] - m.m[1][1] + 1.0f);
						s = 0.5f / sq;
#if	USE_SIMD
						quat1 = _mm_set_ps( m.m[0][1], 1.0f, m.m[2][1], m.m[0][2]);
						quat2 = _mm_set_ps(-m.m[1][0], 0.0f, m.m[1][2], m.m[2][0]);
						q.simd = _mm_sub_ps(quat1, quat2);
						q.simd = _mm_mul_ps(q.simd, _mm_set_ps(s, 0.5f*sq, s, s));
#else	// ~#if USE_SIMD
						q.x = (m.m[0][2] + m.m[2][0]) * s;
						q.y = (m.m[2][1] + m.m[1][2]) * s;
						q.z = 0.5f * sq;
						q.w = (m.m[0][1] - m.m[1][0]) * s;
#endif	// ~#if USE_SIMD
						break;
					}
				}
			}
		}
		//----------------------------------------------------------------------
		//	行列で四元数を回転させる
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationMatrix(Quaternion& q, const Matrix& m)
		{
			Utility::QuaternionRotationMatrix(q, m);
		}
		//----------------------------------------------------------------------
		//	行列で四元数を回転させる
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationMatrix(Quaternion& q, const Matrix4x3& m)
		{
			Utility::QuaternionRotationMatrix(q, m);
		}
		//----------------------------------------------------------------------
		//	行列で四元数を回転させる
		//----------------------------------------------------------------------
		FORCEINLINE void QuaternionRotationMatrix(Quaternion& q, const RotationMatrix& m)
		{
			Utility::QuaternionRotationMatrix(q, m);
		}
		//----------------------------------------------------------------------
		//	球面線形補間
		//----------------------------------------------------------------------
		INLINE void QuaternionSlerp(Quaternion& q, const Quaternion& q1, const Quaternion& q2, f32 t)
		{
			if( t <= 0.0f )	{ q = q1; return; }
			if( t >= 1.0f )	{ q = q2; return; }

			register Quaternion qq;
#if	USE_SIMD
			qq.simd = _mm_mul_ps(q1.simd, q2.simd);
			register f32 cosOmega = qq.x + qq.y + qq.z + qq.w;
#else	// ~#if USE_SIMD
			register f32 cosOmega = q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w;
#endif	// ~#if USE_SIMD
			qq = q1;
			if( cosOmega < 0.0f )
			{
				qq.w = -q2.w;
				qq.x = -q2.x;
				qq.y = -q2.y;
				qq.z = -q2.z;
				cosOmega = -cosOmega;
			}
			register f32 k0, k1;
			if( cosOmega > 0.9999f )
			{
				k0 = 1.0f - t;
				k1 = t;
			}
			else
			{
				register f32 sinOmega, omega, oneOverSinOmega;
				sinOmega = Math::Sqrt(1.0f - cosOmega * cosOmega);

				omega = Math::ATan2(sinOmega, sinOmega);
				oneOverSinOmega = 1.0f / sinOmega;

				k0 = (Sin(1.0f - t) * omega) * oneOverSinOmega;
				k1 = (t * omega) * oneOverSinOmega;
			}
#if	USE_SIMD
			__m128 quat1 = _mm_mul_ps(q1.simd, _mm_set_ps1(k0));
			__m128 quat2 = _mm_mul_ps(qq.simd, _mm_set_ps1(k1));
			q.simd = _mm_add_ps(quat1, quat2);
#else	// ~#if USE_SIMD
			q.x = k0 * q1.x + k1 * qq.x;
			q.y = k0 * q1.y + k1 * qq.y;
			q.z = k0 * q1.z + k1 * qq.z;
			q.w = k0 * q1.w + k1 * qq.w;
#endif	// ~#if USE_SIMD
		}
	}
}

#endif	// __KGL_QUATERNION_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Math.Debug.h
//!	@brief	�v�Z�֘A
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATH_DEBUG_H__
#define	__KGL_MATH_DEBUG_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

#if	KGL_DEBUG_TRACE

#define	TraceVectro2(v)				Trace("Vector2(x:%f y:%f)", v.x, v.y);
#define	TraceVectro3(v)				Trace("Vector3(x:%f y:%f z:%f)", v.x, v.y, v.z);
#define	TraceVectro4(v)				Trace("Vector4(x:%f y:%f z:%f w:%f)", v.x, v.y, v.z, v.w);
#define	TraceRotation(r)			Trace("Rotation(yaw:%f pitch:%f roll:%f)", r.yaw, r.pitch, r.roll);
#define	TraceRotationToDeg(r)		Trace("Rotation(yaw:%f pitch:%f roll:%f)", kgl::Math::RadianToDeg(r.yaw), kgl::Math::RadianToDeg(r.pitch), kgl::Math::RadianToDeg(r.roll));
#define	TraceLightRotation(r)		Trace("LightRotation(yaw:%d pitch:%d roll:%d)", r.yaw, r.pitch, r.roll);
#define	TraceLightRotationToRad(r)	Trace("LightRotation(yaw:%f pitch:%f roll:%f)", kgl::Math::LightRotToRadian(r.yaw), kgl::Math::LightRotToRadian(r.pitch), kgl::Math::LightRotToRadian(r.roll));
#define	TraceLightRotationToDeg(r)	Trace("LightRotation(yaw:%f pitch:%f roll:%f)", kgl::Math::LightRotToDeg(r.yaw), kgl::Math::LightRotToDegr(r.pitch), kgl::Math::LightRotToDegr(r.roll));
#define	TraceQuaternion(q)			Trace("Quartanion(x:%f y:%f z:%f w:%f)", q.x, q.y, q.z, q.w);

#define	TraceRotationMatrix(m)		Trace("RotationMatrix");							\
									Trace("11:%f 12:%f 13:%f", m._11, m._12, m._13);	\
									Trace("21:%f 22:%f 23:%f", m._21, m._22, m._23);	\
									Trace("31:%f 32:%f 33:%f", m._31, m._32, m._33);
#define	TraceMatrix3x3(m)			Trace("Matrix3x3");									\
									Trace("11:%f 12:%f 13:%f", m._11, m._12, m._13);	\
									Trace("21:%f 22:%f 23:%f", m._21, m._22, m._23);	\
									Trace("31:%f 32:%f 33:%f", m._31, m._32, m._33);
#define	TraceMatrix4x3(m)			Trace("Matrix4x3");									\
									Trace("11:%f 12:%f 13:%f", m._11, m._12, m._13);	\
									Trace("21:%f 22:%f 23:%f", m._21, m._22, m._23);	\
									Trace("31:%f 32:%f 33:%f", m._31, m._32, m._33);	\
									Trace("41:%f 42:%f 43:%f", m._41, m._42, m._43);
#define	TraceMatrix(m)				Trace("Matrix");												\
									Trace("11:%f 12:%f 13:%f 14:%f", m._11, m._12, m._13, m._14);	\
									Trace("21:%f 22:%f 23:%f 24:%f", m._21, m._22, m._23, m._24);	\
									Trace("31:%f 32:%f 33:%f 34:%f", m._31, m._32, m._33, m._34);	\
									Trace("41:%f 42:%f 43:%f 44:%f", m._41, m._42, m._43, m._44);

#else	// ~#if KGL_DEBUG_TRACE

#define	TraceVectro2(v)
#define	TraceVectro3(v)
#define	TraceVectro4(v)
#define	TraceRotation(r)
#define	TraceRotationToDeg(r)
#define	TraceLightRotation(r)
#define	TraceLightRotationToRad(r)
#define	TraceLightRotationToDeg(r)
#define	TraceQuaternion(q)
#define	TraceMatrix3x3(m)
#define	TraceMatrix4x3(m)
#define	TraceMatrix(m)

#endif	// ~#if KGL_DEBUG_TRACE

#endif	// __KGL_MATH_DEBUG_H__
//=======================================================================
//	END OF FILE
//=======================================================================
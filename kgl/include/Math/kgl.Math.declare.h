//----------------------------------------------------------------------
//!
//!	@file	kgl.Math.declare.h
//!	@brief	�錾�֘A
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MATH_DECLARE_H__
#define	__KGL_MATH_DECLARE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	const Vector2		ZeroVector2(0.0f, 0.0f);
	const Vector2		OneVector2(1.0f, 1.0f);

	const Vector3		ZeroVector3(0.0f, 0.0f, 0.0f);
	const Vector3		OneVector3(1.0f, 1.0f, 1.0f);

	const Vector4		ZeroVector4(0.0f, 0.0f, 0.0f, 0.0f);
	const Vector4		OneVector4(1.0f, 1.0f, 1.0f, 1.0f);

	const Rotation		ZeroRotation(0.0f, 0.0f, 0.0f);
	const LightRotation	ZeroLightRotation(0, 0, 0);

	const Quaternion	ZeroQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
	const Quaternion	IdentityQuaternion(0.0f, 0.0f, 0.0f, 1.0f);
}


#endif	// __KGL_MATH_DECLARE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
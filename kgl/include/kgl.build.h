//----------------------------------------------------------------------
//!
//!	@file	kgl.build.h
//!	@brief	ビルド設定
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_BUILD_H__
#define	__KGL_BUILD_H__

//----------------------------------------------------------------------
//	ハード関連設定
//----------------------------------------------------------------------
#if	defined(_WIN64)
	#define	X86	0
	#define	X64	1
#else	// ~#if defined(_WIN64)
	#define	X86	1
	#define	X64	0
#endif	// ~#if defined(_WIN64)

#if	defined(_WIN32)
	#define	WINDOWS	1
#else	// ~#if defined(_WIN32)
	#define	WINDOWS	0
#endif	// ~#if defined(_WIN32)

#if	defined(_CONSOLE)
	#define	CONSOLE_TOOL	1
#else	// ~#if defined(_CONSOLE)
	#define	CONSOLE_TOOL	0
#endif	// ~#if defined(_CONSOLE)

//	Platform Dependent
#if	WINDOWS
	#define	PLATFORM	("PC")
#else
	#define	PLATFORM	("Unknown")
#endif

//----------------------------------------------------------------------
//	ビルド構成
//----------------------------------------------------------------------
#if	defined(_FINAL_RELEASE)
	#define	KGL_FINAL_RELEASE	1
#else	// ~#if defined(_FINAL_RELEASE)
	#define	KGL_FINAL_RELEASE	0
#endif	// ~#if defined(_FINAL_RELEASE)

#if	!KGL_FINAL_RELEASE
	#define KGL_DEBUG	1
#else	// ~#if KGL_FINAL_RELEASE
	#define KGL_DEBUG	0
#endif	// ~#if KGL_FINAL_RELEASE

#if	WINDOWS	&& !KGL_FINAL_RELEASE
	#define	KGL_USE_TOOLS	1
#else	// ~#if WINDOWS && !KGL_FINAL_RELEASE
	#define	KGL_USE_TOOLS	0
#endif	// ~#if WINDOWS && !KGL_FINAL_RELEASE


#if	KGL_DEBUG
	#define	KGL_DEBUG_ASSERT	1
	#define	KGL_DEBUG_TRACE		1
	#define	KGL_DEBUG_FONT		1
	#define	KGL_DEBUG_PROFILE	1
	#define	KGL_DEBUG_MEMORY	1
	#define	KGL_DEBUG_FILE		1
	#define	KGL_DEBUG_MENU		1
	#define	KGL_DEBUG_INLINE	1
#else	// ~#if KGL_DEBUG
	#define	KGL_DEBUG_ASSERT	0
	#define	KGL_DEBUG_TRACE		0
	#define	KGL_DEBUG_FONT		0
	#define	KGL_DEBUG_PROFILE	0
	#define	KGL_DEBUG_MEMORY	0
	#define	KGL_DEBUG_FILE		0
	#define	KGL_DEBUG_MENU		0
	#define	KGL_DEBUG_INLINE	0
#endif	// ~#if KGL_DEBUG

#if	CONSOLE_TOOL
	#define	KGL_USE_TLSF_ALLOCATOR	0
#else	// ~#if	CONSOLE_TOOL
	#define	KGL_USE_TLSF_ALLOCATOR	1
#endif	// ~#if	CONSOLE_TOOL

#if	KGL_USE_TOOLS
	#define	KGL_USE_WXWIDGETS	0
#else	// ~#if	KGL_USE_TOOLS
	#define	KGL_USE_WXWIDGETS	0
#endif	// ~#if	KGL_USE_TOOLS

//----------------------------------------------------------------------
//	プラットフォーム関連
//----------------------------------------------------------------------
//	Platform Dependent
#if	WINDOWS
	#define	USE_DIRECTX9	1
	#define	USE_DIRECTX11	1
	//#define	USE_OPENGL		1

	#define	USE_XAUDIO2		1
	//#define	USE_OPENAL		1

	#define	USE_SIMD		1
#else
#endif

//	ユーザーによって変更可能な項目
#include "kgl.build.user.h"

//----------------------------------------------------------------------
#if	!defined(USE_DIRECTX9)
	#define	USE_DIRECTX9	0
#endif	// ~#if !defined(USE_DIRECTX9)

#if	!defined(USE_DIRECTX11)
	#define	USE_DIRECTX11	0
#endif	// ~#if !defined(USE_DIRECTX11)

#if	!defined(USE_OPENGL)
	#define	USE_OPENGL	0
#endif	// ~#if !defined(USE_OPENGL)

#if	!defined(USE_XAUDIO2)
	#define	USE_XAUDIO2	0
#endif	// ~#if !defined(USE_XAUDIO2)

#if	!defined(USE_OPENAL)
	#define	USE_OPENAL	0
#endif	// ~#if !defined(USE_OPENAL)

#if	!defined(USE_SIMD)
	#define	USE_SIMD	0
#endif	// ~#if !defined(USE_SIMD)

//----------------------------------------------------------------------


#endif	// __KGL_BUILD_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Scene.h
//!	@brief	シーン管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_SCENE_H__
#define	__KGL_SCENE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "Render/kgl.Render.h"

namespace kgl
{
	namespace Scene
	{
		//	最大保持シーン数
		const s32 SCENE_CHAIN_MAX = 8;
		//	最大シーン設定数
		const s32 SCENE_MAX = 256;

		//======================================================================
		//!	シーンインターフェイス（継承してシーンを生成）
		//!	※各関数はシーンマネージャーで呼び出されます
		//======================================================================
		interface ISceneBase
			: public IKGLBase
		{
		public:
			//!	初期化
			//!	@return 結果
			virtual b8 Initialize(void) { return true; }
			//!	破棄
			virtual void Finalize(void) {}

			//!	更新
			//!	@param fElapsedTime [in] 経過時間
			virtual void Update(f32 fElapsedTime) = 0;
			//!	描画
			//!	@param pRenderingSystem	[in] 描画システム
			virtual void Render(Render::IRenderingSystem* pRenderingSystem) = 0;
		};

		//!	シーン生成関数定義
		typedef ISceneBase* (*FSceneCreate)(void);
		//!	シーンの生成テンプレート関数
		template<class T>
		ISceneBase* CreateScene(void) { return new T; }

		//======================================================================
		//!	シーンマネージャーインターフェイス
		//======================================================================
		interface ISceneManager
			: public IKGLBase
		{
		public:
			//!	更新
			//!	@param fElapsedTime		[in] 経過時間
			virtual void Update(f32 fElapsedTime) = 0;
			//!	描画
			//!	@param pRenderingSystem	[in] 描画システム
			virtual void Render(Render::IRenderingSystem* pRenderingSystem) = 0;

		public:
			//!	シーンのセット
			//!	@param iSceneNo	[in] シーン番号
			//!	@param Func			[in] シーン生成関数
			//!	@param pName		[in] シーン名(デバッグ用)
			virtual void SetScene(s32 iSceneNo, FSceneCreate Func, const c8* pName) = 0;
			//!	シーン変更
			//!	@param iSceneNo	[in] シーン番号
			//!	@param iParam		[in] 汎用パラメータ
			//!	@param bAllClose	[in] 保持しているシーンを全て破棄するか？
			virtual void JumpScene(s32 iSceneNo, b8 bAllClose = false) = 0;
			//!	現在のシーンを保存したままシーン変更する(SCENE_CHAIN_MAX個まで保存可能)
			//!	@param iSceneNo	[in] シーン番号
			//!	@param iParam		[in] 汎用パラメータ
			virtual void CallScene(s32 iSceneNo) = 0;

			//!	保存したシーンに戻る
			virtual void ReturnScene(void) = 0;

		public:
			//!	現在のシーンを取得
			//!	@return 現在のシーン
			virtual s32 NowScene(void) = 0;
			//!	前回のシーンを取得
			//!	@return 前回のシーン
			virtual s32 PrevScene(void) = 0;
			//!	現在のシーン名を取得
			//!	@return 現在のシーン名
			virtual const c8* NowSceneName(void) = 0;
			//!	前回のシーン名を取得
			//!	@return 前回のシーン名
			virtual const c8* PrevSceneName(void) = 0;

		public:
			//!	シーン名を取得
			//!	@return シーン名
			virtual const c8* SceneName(s32 iSceneNo) = 0;
		};

		//!	シーンマネージャーの生成
		//!	@param ppSceneManager [out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(ISceneManager** ppSceneManager);
	}
}

#endif	// __KGL_SCENE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
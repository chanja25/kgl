//----------------------------------------------------------------------
//!
//!	@file	kgl.Dll.h
//!	@brief	DLL管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DLL_H__
#define	__KGL_DLL_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Dll
	{
		//======================================================================
		//!	DLLマネージャーインターフェイス
		//======================================================================
		interface IDllManager
			: public IKGLBase
		{
		public:
			//!	DLLの読み込み
			//!	@param pPath	[in] パス
			//!	@return 管理ID(失敗時は-1)
			virtual s32 Load(const c8* pPath) = 0;
			//!	DLLの解放
			//!	@param iId	[in] 管理ID
			virtual void Unload(s32 iId) = 0;

			//!	関数取得
			//!	@param iId			[in] 管理ID
			//!	@param pFunction	[in] 関数名
			//!	@return 関数ポインタ(失敗時はnull)
			virtual FDllFunction GetFunction(s32 iId, const c8* pFunction) = 0;
		};

		//!	DLLマネージャーの生成
		//!	@param ppManager	[out] 実体格納用ポインタ
		//!	@param iMax			[in] 最大管理数
		//!	@return 結果
		b8 Create(IDllManager** ppManager, s32 iMax);
	}
}

#endif	// __KGL_DLL_H__
//=======================================================================
//	END OF FILE
//=======================================================================
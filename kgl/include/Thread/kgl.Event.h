//----------------------------------------------------------------------
//!
//!	@file	kgl.Event.h
//!	@brief	イベント
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_EVENT_H__
#define	__KGL_EVENT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Thread
	{
		//======================================================================
		//!	イベントインターフェイス
		//======================================================================
		interface IEvent
			: public IKGLBase
		{
		public:
			//!	シグナルがセットされるまでブロックする
			//!	@param uiTimeout [in] タイムアウト(ミリ秒単位)
			//!	@return false:タイムアウト
			virtual b8 Wait(u32 uiTimeout = InfiniteWait) = 0;
			//!	シグナルがセットされているかチェックする
			//!	@return シグナルの有無
			virtual b8 TryWait(void) = 0;
			//	シグナルをセットする
			virtual void SetSignal(void) = 0;
			//!	シグナルをクリアする(自動リセットを使用している場合は使用できません)
			virtual void ClearSignal(void) = 0;
		};

		//!	イベントの生成
		//!	@param ppEvent		[out] 実体格納用ポインタ
		//!	@param bManualReset	[in] Wait、TryWait時にシグナルを自動でリセットするか
		//!	@param bInitSignal	[in] 初期化時にシグナルをセットするか
		//!	@return 結果
		b8 Create(IEvent** ppEvent, b8 bManualReset = false, b8 bInitSignal = false);
	}
}

#endif	// __KGL_EVENT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Mutex.h
//!	@brief	ミューテックス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MUTEX_H__
#define	__KGL_MUTEX_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Thread
	{
		//======================================================================
		//!	ミューテックスインターフェイス
		//======================================================================
		interface IMutex
			: public IKGLBase
		{
		public:
			//!	ミューテックスをロック
			//!	@param uiTimeout [in] タイムアウト(ミリ秒単位)
			//!	@return false:タイムアウト
			virtual b8 Lock(u32 uiTimeout = InfiniteWait) = 0;
			//!	ミューテックスをロック
			//!	@return 結果
			virtual b8 TryLock(void) = 0;
			//!	ミューテックスをアンロック
			virtual void Unlock(void) = 0;
		};

		//!	ミューテックスの生成
		//!	@param ppMutex		[out] 実体格納用ポインタ
		//!	@param bInitLock	[in] 初期化時にLockするか
		//!	@return 結果
		b8 Create(IMutex** ppMutex, b8 bInitLock = false);

		//======================================================================
		//!	ロック用クラス
		//======================================================================
		class CLock
		{
		public:
			//!	コンストラクタ
			//!	@param pMutex [in] ミューテックス
			FORCEINLINE CLock(IMutex* pMutex)
				: m_pMutex(pMutex)
			{
				m_pMutex->Lock();
			}

			//!	デストラクタ
			FORCEINLINE ~CLock(void)
			{
				m_pMutex->Unlock();
			}

		private:
			IMutex*	m_pMutex;
		};
	}
}

#define	SCOPE_LOCK_MUTEX(x)	volatile kgl::Thread::CLock lock(x)

#endif	// __KGL_MUTEX_H__
//=======================================================================
//	END OF FILE
//=======================================================================
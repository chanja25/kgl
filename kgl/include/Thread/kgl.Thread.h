//----------------------------------------------------------------------
//!
//!	@file	kgl.Thread.h
//!	@brief	スレッド
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_THREAD_H__
#define	__KGL_THREAD_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Thread
	{
		namespace EPriority
		{
			const s32 TimeCritical	= 30;	//!< 最優先
			const s32 Highest		= 17;	//!< 優先度+2
			const s32 AboveNormal	= 16;	//!< 優先度+1
			const s32 Normal		= 15;	//!< 通常
			const s32 BelowNormal	= 14;	//!< 優先度-1
			const s32 Lowest		= 13;	//!< 優先度-2
			const s32 Idle			= 0;	//!< 優先度最低
		};

		//!	呼び出し元のスレッドIDの取得
		//!	@return スレッドID
		u32 GetThreadId(void);

		//======================================================================
		//!	スレッドインターフェイス
		//======================================================================
		interface IThread
			: public IKGLBase
		{
		public:
			//!	起動
			//!	@param Function		[in] スレッド起動する関数
			//!	@param pArg			[in] スレッドに渡す引数
			//!	@param uiStackSize	[in] スタックサイズ
			//!	@return 結果
			virtual b8 Start(FThreadFunction Function, void* pArg = null, u32 uiStackSize = KB(32), const c8* pName = null) = 0;
			//!	終了
			//!	@param bWait	[in] スレッドが終了するのを待つか
			virtual void Exit(b8 bWait = true) = 0;
			//!	終了確認
			//!	@return 終了の有無
			virtual b8 IsExit(void) = 0;
			//!	終了コード取得(IsExitがtrueの場合に有効)
			//!	@return 終了コード
			virtual u32 GetExitCode(void) = 0;
			//!	スレッドの優先設定
			//!	@param iPriority	[in] 優先度(EPriority参照)
			virtual void SetPriority(s32 iPriority) = 0;
			//!	スレッドを実行可能なプロセッサ設定
			//!	@param uiMask	[in] 実行可能なプロセッサ番号[ビットベクトル](例:0x03⇒コア0,1が有効)
			virtual void SetAffinityMask(s32 iMask) = 0;
		};

		//!	スレッドの生成
		//!	@param ppThread	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IThread** ppThread);
	}
}

#endif	// __KGL_THREAD_H__
//=======================================================================
//	END OF FILE
//=======================================================================
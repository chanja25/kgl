//----------------------------------------------------------------------
//!
//!	@file	kgl.CriticalSection.h
//!	@brief	クリティカルセクション
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_CRITICALSECTION_H__
#define	__KGL_CRITICALSECTION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Thread
	{
		//======================================================================
		//!	クリティカルセクションインターフェイス
		//======================================================================
		interface ICriticalSection
			: public IKGLBase
		{
		public:
			//!	クリティカルセクションオブジェクトの所有権取得
			virtual void Enter(void) = 0;
			//!	クリティカルセクションオブジェクトの所有権取得(ブロックしない)
			//!	@return 所有権を取得出来たか？
			virtual b8 TryEnter(void) = 0;
			//!	クリティカルセクションオブジェクトの所有権解放
			virtual void Leave(void) = 0;
		};

		//!	クリティカルセクションの生成
		//!	@param ppCriticalSection	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(ICriticalSection** ppCriticalSection);

		//======================================================================
		//!	ロック用クラス
		//======================================================================
		class CLockCS
		{
		public:
			//!	コンストラクタ
			//!	@param pCriticalSection [in] クリティカルセクション
			FORCEINLINE CLockCS(ICriticalSection* pCriticalSection)
				: m_pCriticalSection(pCriticalSection)
			{
				kgAssert(pCriticalSection != null, "インスタンスがnullです(CLockCS::CLockCS())");
				m_pCriticalSection->Enter();
			}
			//!	デストラクタ
			FORCEINLINE ~CLockCS(void)
			{
				m_pCriticalSection->Leave();
			}

		private:
			ICriticalSection*	m_pCriticalSection;
		};
	}
}

#define	SCOPE_LOCK_CRITICALSECTION(x)	volatile kgl::Thread::CLockCS lock(x)

#endif	// __KGL_CRITICALSECTION_H__
//=======================================================================
//	END OF FILE
//=======================================================================
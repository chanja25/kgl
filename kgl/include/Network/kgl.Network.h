//----------------------------------------------------------------------
//!
//!	@file	kgl.Network.h
//!	@brief	ネットワーク管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_NETWORK_H__
#define	__KGL_NETWORK_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Network
	{
		//!	デフォルトで送信使用するポート
		const s32 DefaultSendPort = 203071;
		//!	デフォルトで受信使用するポート
		const s32 DefaultRecvPort = 203072;

		//! 接続状態
		namespace EState
		{
			const s32 Unknown			= -1;	//!< 不明
			const s32 NotConnect		= 0;	//!< 未接続
			const s32 CreatingConnect	= 1;	//!< 接続試行中
			const s32 ClosingConnect	= 2;	//!< 接続解除中
			const s32 Connecting		= 3;	//!< 接続中

			const s32 ConnectFailed		= 10;	//!< 接続失敗
		}

		//======================================================================
		//!	ユーザーインターフェイス
		//======================================================================
		interface IUserBase
		{
		public:
			//!	インデックスの取得
			//!	@return インデックス
			virtual s32 GetIndex(void) { return m_iIndex; }
			//!	状態の取得
			//!	@return 状態
			virtual s32 GetState(void) { return m_iState; }
			//!	送信に使用するポートの取得
			//!	@return ポート番号
			virtual s32 GetSendPort(void) { return m_iSendPort; }
			//!	受信に使用するポートの取得
			//!	@return ポート番号
			virtual s32 GetRecvPort(void) { return m_iRecvPort; }
			//!	IPアドレスの取得
			//!	@return IPアドレス
			virtual const c8* GetIpAddrress(void) { return m_strIpAddr.c_str(); }

		protected:
			s32					m_iIndex;		//!< ユーザー番号
			s32					m_iState;		//!< ステータス
			s32					m_iSendPort;	//!< 送信に使用しているポート
			s32					m_iRecvPort;	//!< 受信に使用しているポート
			algorithm::string	m_strIpAddr;	//!< IPアドレス
		};

		//======================================================================
		//!	ネットワークインターフェイス
		//======================================================================
		interface INetworkBase
		{
		public:
			//!	データ送信
			//!	@param pUser	[in] 送信先のユーザー
			//!	@param pData	[in] 送信データ
			//!	@param uiSize	[in] 送信サイズ
			//!	@return 結果
			virtual b8 Send(IUserBase* pUser, const void* pData, u32 uiSize) = 0;
			//!	データ受信
			//!	@param ppUser		[in] 受信元のユーザー
			//!	@param pData		[in] 受信データの格納バッファ
			//!	@param uiSize		[in] バッファサイズ
			//!	@param pRecvSize	[in] 受信サイズ
			//!	@return 受信サイズ
			virtual b8 Recv(IUserBase** ppUser, void* pData, u32 uiSize, s32* pRecvSize) = 0;
		};

		//======================================================================
		//!	サーバーインターフェイス
		//======================================================================
		interface IServerBase
			: public INetworkBase
		{
		public:
			//!	初期化
			//!	@param iClientMax	[in] 最大接続数
			//!	@param iPort		[in] 使用するポート
			virtual b8 Initialize(s32 iClientMax, s32 iPort = DefaultSendPort) = 0;
			//!	終了処理
			virtual void Finalize(void);
		};
	}
}

#endif	// __KGL_NETWORK_H__
//=======================================================================
//	END OF FILE
//=======================================================================
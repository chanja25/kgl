//----------------------------------------------------------------------
//!
//!	@file	kgl.atom.h
//!	@brief	非同期関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ATOM_H__
#define	__KGL_ATOM_H__

#if	WINDOWS
//!	スレッドセーフな加算
FORCEINLINE s32 kgAtomicAdd(s32& i, s32 add)
{
	return InterlockedExchangeAdd((LONG*)&i, add) + add;
}
FORCEINLINE u32 kgAtomicAdd(u32& i, u32 add)
{
	return InterlockedExchangeAdd(&i, add) + add;
}
//!	スレッドセーフな減算
FORCEINLINE s32 kgAtomicSubtract(s32& i, s32 sub)
{
	return InterlockedExchangeAdd((LONG*)&i, -sub) - sub;
}
//!	スレッドセーフな減算
FORCEINLINE u32 kgAtomicSubtract(u32& i, u32 sub)
{
	return InterlockedExchangeSubtract(&i, sub) - sub;
}
//!	スレッドセーフなインクリメント
FORCEINLINE s32 kgAtomicIncrement(s32& i)
{
	return kgAtomicAdd(i, 1);
}
FORCEINLINE u32 kgAtomicIncrement(u32& i)
{
	return kgAtomicAdd(i, 1);
}
//!	スレッドセーフなデクリメント
FORCEINLINE s32 kgAtomicDecrement(s32& i)
{
	return kgAtomicSubtract(i, 1);
}
FORCEINLINE u32 kgAtomicDecrement(u32& i)
{
	return kgAtomicSubtract(i, 1);
}
#else
//!	スレッドセーフな加算
FORCEINLINE u32 kgAtomicAdd(volatile u32& i, u32 add)
{
	kgAssert(false, "スレッドセーフな関数が用意されていません(kgl.atom.h)");
	return (i += add);
}
//!	スレッドセーフな減算
FORCEINLINE u32 kgAtomicSubtract(volatile u32& i, u32 sub)
{
	kgAssert(false, "スレッドセーフな関数が用意されていません(kgl.atom.h)");
	return (i -= sub);
}
//!	スレッドセーフなインクリメント
FORCEINLINE u32 kgAtomicIncrement(volatile u32& i)
{
	kgAssert(false, "スレッドセーフな関数が用意されていません(kgl.atom.h)");
	return -- i;
}
//!	スレッドセーフなデクリメント
FORCEINLINE u32 kgAtomicDecrement(volatile u32& i)
{
	kgAssert(false, "スレッドセーフな関数が用意されていません(kgl.atom.h)");
	return ++ i;
}
#endif

#endif	// __KGL_ATOM_H__
//======================================================================
//	END OF FILE
//======================================================================
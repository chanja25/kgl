//----------------------------------------------------------------------
//!
//!	@file	kgl.win32.h
//!	@brief	WIN32
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_WIN32_H__
#define	__KGL_WIN32_H__

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
//#if	defined(_WIN32_WINNT)
//#undef  _WIN32_WINNT
//#define _WIN32_WINNT 0x0502
//#endif	// ~#if defined(_WIN32_WINNT)

#if	KGL_FINAL_RELEASE
#define WIN32_LEAN_AND_MEAN
#endif	// ~#if KGL_FINAL_RELEASE


//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <ObjBase.h>	//	interfaceマクロを使用する為
#include <new>			//	placement newを使用する為
#include <mmsystem.h>	//	WAVEFORMATEX
#include <atlbase.h>

//----------------------------------------------------------------------
// pragma
//----------------------------------------------------------------------
//	警告対策
#pragma	warning(disable:4996)	//	strcpy等の対応
#pragma	warning(disable:4201)	//	無名structの対応
#pragma	warning(disable:4127)	//	if(true)の対応
#pragma	warning(disable:4512)	//	class変数への参照渡しの対応
//#pragma	warning(disable:4100)	//	未使用のローカル変数を許容
#pragma warning(disable:4291)	//	placement newでの警告対策
#pragma warning(disable:4458)	//	変数と同名変数を許可
#pragma warning(disable:4456)	//	変数名のスコープ内の隠蔽対策
#pragma warning(disable:4457)	//	変数名のスコープ内の隠蔽対策
#pragma warning(disable:4297)	//	例外をスローしない場合の警告対策


//----------------------------------------------------------------------
// undef
//----------------------------------------------------------------------
#undef	DrawText
#undef	GetObject
#undef	CreateWindow
#undef	GetMessage

#endif	// __KGL_WIN32_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.DoubleBuffer.h
//!	@brief	ダブルバッファ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DOUBLEBUFFER_H__
#define	__KGL_DOUBLEBUFFER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	ダブルバッファ用テンプレート
		//======================================================================
		template<typename T>
		class TDoubleBuffer
		{
		public:
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			TDoubleBuffer(void)
				: m_pBuffer(null)
				, m_iSize(0)
			{}
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			TDoubleBuffer(T* pBuffer, s32 iSize)
			{
				Setup(pBuffer, iCount);
			}

		public:
			//----------------------------------------------------------------------
			//!	セットアップ
			//!	@param pBuffer	[in] 設定するバッファ
			//----------------------------------------------------------------------
			FORCEINLINE void Setup(T* pBuffer, s32 iSize)
			{
				m_pBuffer	= pBuffer;
				m_iSize		= iSize;
			}
			//----------------------------------------------------------------------
			//!	参照
			//----------------------------------------------------------------------
			FORCEINLINE T& operator [](s32 iNo)
			{
				register s32 iSize = m_iSize >> 1;
				register s32 iStart = m_bFlip? iSize: 0;
				kgAssert(iNo < iSize, "access error");
				return m_pBuffer[iNo + iStart];
			}
			//----------------------------------------------------------------------
			//!	参照
			//----------------------------------------------------------------------
			FORCEINLINE T* front(void)
			{
				register s32 iSize = m_iSize >> 1;
				register s32 iStart = m_bFlip? iSize: 0;
				kgAssert(iNo < iSize, "access error");
				return &m_pBuffer[iStart];
			}
			//----------------------------------------------------------------------
			//!	参照
			//----------------------------------------------------------------------
			FORCEINLINE T* back(void)
			{
				register s32 iSize = m_iSize >> 1;
				register s32 iStart = !m_bFlip? iSize: 0;
				kgAssert(iNo < iCount, "access error");
				return &m_pBuffer[iStart];
			}
			//----------------------------------------------------------------------
			//!	フロントバッファを参照
			//----------------------------------------------------------------------
			FORCEINLINE T& front_buffer(s32 iNo)
			{
				register s32 iSize = m_iSize >> 1;
				register s32 iStart = m_bFlip? iSize: 0;
				kgAssert(iNo < iSize, "access error");
				return m_pBuffer[iNo + iStart];
			}
			//----------------------------------------------------------------------
			//!	バックバッファを参照
			//----------------------------------------------------------------------
			FORCEINLINE T& back_buffer(s32 iNo)
			{
				register s32 iSize = m_iSize >> 1;
				register s32 iStart = m_bFlip? 0: iSize;
				kgAssert(iNo < iSize, "access error");
				return m_pBuffer[iNo + iStart];
			}

		public:
			//----------------------------------------------------------------------
			//!	バッファの入れ替え
			//----------------------------------------------------------------------
			FORCEINLINE void Flip(void) { m_bFlip = !m_bFlip; }

		private:
			T*	m_pBuffer;	//!< バッファ
			s32	m_iSize;	//!< バッファ数
			b8	m_bFlip;	//!< 前後の入れ替え
			s8	pad[3];
		};
	}
}

#endif	// __KGL_DOUBLEBUFFER_H__
//======================================================================
//	END OF FILE
//======================================================================
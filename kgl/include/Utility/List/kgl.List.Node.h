//----------------------------------------------------------------------
//!
//!	@file	kgl.List.Node.h
//!	@brief	リスト
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_LIST_NODE_H__
#define	__KGL_LIST_NODE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"


namespace kgl
{
	namespace Utility
	{
		// リストテンプレートクラス
		template<typename T>
		class TList;

		//======================================================================
		//!	ノードテンプレートクラス
		//======================================================================
		template<typename T>
		class TNode
		{
		public:
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			TNode(void)
				: m_pObject(null), m_pNext(null), m_pPrev(null)
				, m_fSort(0.0f)
			{
			}

		public:
			//----------------------------------------------------------------------
			//	オペレーター
			//----------------------------------------------------------------------
			INLINE b8 operator == (const TNode<T>& n) const
			{
				return m_fSort == n.m_fSort;
			}

			INLINE b8 operator > (const TNode<T>& n) const
			{
				return m_fSort > n.m_fSort;
			}

			INLINE b8 operator < (const TNode<T>& n) const
			{
				return m_fSort < n.m_fSort;
			}

			INLINE b8 operator >= (const TNode<T>& n) const
			{
				return m_fSort >= n.m_fSort;
			}

			INLINE b8 operator <= (const TNode<T>& n) const
			{
				return m_fSort <= n.m_fSort;
			}

		public:
			//----------------------------------------------------------------------
			//!	自分の次にノードを接続
			//!	@param pNode [in] ノード
			//----------------------------------------------------------------------
			void SetListNext(TNode<T>* pNode)
			{
				if( m_pNext )
				{
					//	次のノードの前に追加ノードをセット
					m_pNext->SetPrev(pNode);
					//	追加ノードの次に自分の次のノードセット
					pNode->SetNext(m_pNext);
				}

				//	次のノードに追加ノードをセット
				m_pNext = pNode;
				//	追加ノードの前に自分をセット
				pNode->SetPrev(this);
			}

			//----------------------------------------------------------------------
			//!	自分の前にノードを接続
			//!	@param pNode [in] ノード
			//----------------------------------------------------------------------
			void SetListPrev(TNode<T>* pNode)
			{
				if( m_pPrev )
				{
					//	次のノードの前に追加ノードをセット
					m_pPrev->SetNext(pNode);
					//	追加ノードの次に自分の次のノードセット
					pNode->SetPrev(m_pPrev);
				}

				//	次のノードに追加ノードをセット
				m_pPrev = pNode;
				//	追加ノードの前に自分をセット
				pNode->SetNext(this);
			}

			//----------------------------------------------------------------------
			//!	リストから削除
			//----------------------------------------------------------------------
			void Remove(void)
			{
				//	自分の前後を繋げる
				if( m_pNext )
					m_pNext->SetPrev(m_pPrev);
				if( m_pPrev )
					m_pPrev->SetNext(m_pNext);

				m_pNext = m_pPrev = null;
			}

			//----------------------------------------------------------------------
			//!	ノードの入れ替え
			//!	@param pNode [in] ノード
			//----------------------------------------------------------------------
			void Swap(TNode<T>* pNode)
			{
				TNode<T>* pTemp;

				if( m_pNext == pNode )
				{
					pTemp = pNode->GetNext();
					if( pTemp )
						pTemp->SetPrev(this);
					pNode->SetNext(this);
					m_pNext = pTemp;

					pNode->SetPrev(m_pPrev);
					if( m_pPrev )
						m_pPrev->SetNext(pNode);
					m_pPrev = pNode;
					return;
				}

				if( m_pPrev == pNode )
				{
					pNode->SetNext(m_pNext);
					if( m_pNext )
						m_pNext->SetPrev(pNode);
					m_pNext = pNode;

					pTemp = pNode->GetPrev();
					if( pTemp )
						pTemp->SetNext(this);
					pNode->SetPrev(this);
					m_pPrev = pTemp;
					return;
				}

				if( m_pNext )
					m_pNext->SetPrev(pNode);
				if( m_pPrev )
					m_pPrev->SetNext(pNode);

				pTemp = pNode->GetNext();
				if( pTemp )
					pTemp->SetPrev(this);
				pNode->SetNext(m_pNext);
				m_pNext = pTemp;

				pTemp = pNode->GetPrev();
				if( pTemp )
					pTemp->SetNext(this);
				pNode->SetPrev(m_pPrev);
				m_pPrev = pTemp;
			}

		public:
			//!	オブジェクトのセット
			//!	@param pObject [in] オブジェクト
			INLINE void SetObject(T* pObject) { m_pObject = pObject; }
			//!	オブジェクト取得
			//!	@return オブジェクト
			INLINE T* GetObject(void) { return m_pObject; }
			//!	次のノードをセット
			//!	@param pNode [in] ノード
			INLINE void SetNext(TNode<T>* pNext) { m_pNext = pNext; }
			//!	次のノードを取得
			//!	@return 次のノード
			INLINE TNode<T>* GetNext(void) { return m_pNext; }
			//!	前のノードをセット
			//!	@param pNode [in] ノード
			INLINE void SetPrev(TNode<T>* pPrev) { m_pPrev = pPrev; }
			//!	前のノードを取得
			//!	@return 前のノード
			INLINE TNode<T>* GetPrev(void) { return m_pPrev; }

			//!	ソートセット
			INLINE void SetSort(f32 fSort) { m_fSort = fSort; } 
			//!	ソート取得
			INLINE f32 GetSort(void) { return m_fSort; }

		private:
			T*			m_pObject;	//!< 管理しているオブジェクト
			TNode<T>*	m_pNext;	//!< 次のノード
			TNode<T>*	m_pPrev;	//!< 前のノード

			f32			m_fSort;	//!< ソート
		};
	}
}


#endif	// __KGL_LIST_NODE_H__
//======================================================================
//	END OF FILE
//======================================================================
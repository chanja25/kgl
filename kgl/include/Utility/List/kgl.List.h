//----------------------------------------------------------------------
//!
//!	@file	kgl.List.h
//!	@brief	リスト
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_LIST_H__
#define	__KGL_LIST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.List.Node.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	リストテンプレート
		//======================================================================
		template<typename T>
		class TList
		{
			typedef TList<T>	MyList;
			typedef TNode<T>	MyNode;
		public:
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			INLINE TList(void)
				: m_pFirst(null), m_pEnd(null), m_uiCount(0)
			{}

		public:
			//----------------------------------------------------------------------
			//!	ノードを末尾に追加
			//!	@param pNode [in] ノード
			//----------------------------------------------------------------------
			INLINE void Entry(MyNode* pNode)
			{
				if( pNode == null )
				{
					kgAssert(pNode, "中身のないノードが追加されようとしています");
					return;
				}

				//	ノード数をインクリメント
				m_uiCount ++;
				//	管理されているノードなければ、追加ノードを先端・末尾にセットする
				if( m_pFirst == null )
				{
					m_pFirst = m_pEnd = pNode;
					return;
				}

				//	末尾ノードの前にノードをセット
				m_pEnd->SetListNext(pNode);
				//	末尾ノードに追加ノードをセット
				m_pEnd = pNode;
			}

			//----------------------------------------------------------------------
			//!	対象ノードの削除
			//!	@param pNode [in] ノード
			//----------------------------------------------------------------------
			INLINE void Remove(MyNode* pNode)
			{
				if( pNode == null )
					return;

				//	ノードの数をデクリメント
				m_uiCount --;

				//	削除するノードが先頭なら、次のノードを先頭ノードにセットする
				if( m_pFirst == pNode )
					m_pFirst = pNode->GetNext();

				//	削除するノードが末尾なら、前のノードを末尾ノードにセットする
				if( m_pEnd == pNode )
					m_pEnd = pNode->GetPrev();

				//	リストからノードを外す
				pNode->Remove();
			}

			//----------------------------------------------------------------------
			//!	対象ノードの削除
			//!	@param pNode [in] ノード
			//----------------------------------------------------------------------
			INLINE MyNode* Search(T* pObject)
			{
				if( pObject == null )
					return null;

				MyNode* pNode = m_pFirst;
				while( pNode )
				{
					if( *pObject == *pNode->GetObject() )
						break;

					pNode = pNode->GetNext();
				}

				return pNode;
			}

			//----------------------------------------------------------------------
			//!	全ノードの削除
			//----------------------------------------------------------------------
			INLINE void Clear(void)
			{
				while( m_pFirst )
				{
					Remove(m_pFirst);
				}
			}

			//----------------------------------------------------------------------
			//!	ソート
			//----------------------------------------------------------------------
			INLINE void Sort(void)
			{
				MergeSort(&m_pFirst, m_uiCount);
			}

			//!	ノード数取得
			//!	@return ノード数
			INLINE u32 GetNum(void) { return m_uiCount; }
			//!	先頭ノード取得
			//!	@return 先頭ノード
			INLINE MyNode* GetFirst(void) { return m_pFirst; }
			//!	末尾ノード取得
			//!	@return 末尾ノード
			INLINE MyNode* GetEnd(void) { return m_pEnd; }

		private:
			//----------------------------------------------------------------------
			//!	マージソート(再起処理)
			//!	@param ppStart [in/out] ソートする先頭のノード
			//!	@param uiCount [in] ノード数
			//----------------------------------------------------------------------
			INLINE void MergeSort(MyNode** ppStart, const u32& uiCount)
			{
				u32 i;
				//	数を二等分する
				u32 uiDivision = (uiCount + 1) >> 1;
				while( uiDivision > 1 )
				{
					for( i = 0; i < uiDivision; i++ )
					{
						//	数が1になるまで繰り返す
						MergeSort(ppStart, uiCount1, ppLeft, ppRight);
						MergeSort(&pNode, uiCount2, ppLeft, ppRight);
					}
					uiDivision >>= 1;
				}

				//	マージ
				Merge(ppStart, uiCount1, pNode, uiCount2);
			}
			//----------------------------------------------------------------------
			//!	マージソート(再起処理)
			//!	@param ppStart [in/out] ソートする先頭のノード
			//!	@param uiCount [in] ノード数
			//----------------------------------------------------------------------
			INLINE void MergeSort(MyNode** ppStart, const u32& uiCount, MyNode** ppLeft, MyNode** ppRight)
			{
				//	数を二等分する
				u32 uiCount1 = uiCount >> 1;
				u32 uiCount2 = uiCount - uiCount1;

				//	中央のノードを取得
				MyNode* pNode = *ppStart;
				for( u32 i=0; i < uiCount1; i++ )
				{
					pNode = pNode->GetNext();
				}

				//	数が1になるまで繰り返す
				if( uiCount1 > 1 )
					MergeSort(ppStart, uiCount1, ppLeft, ppRight);
				if( uiCount2 > 1 )
					MergeSort(&pNode, uiCount2, ppLeft, ppRight);

				//	マージ
				Merge(ppStart, uiCount1, pNode, uiCount2);
			}

			//----------------------------------------------------------------------
			//!	マージ
			//!	@param ppNode1 [in/out] ノード1
			//!	@param uiCount1 [in] ノード1の数
			//!	@param ppNode2 [in/out] ノード2
			//!	@param uiCount2 [in] ノード2の数
			//----------------------------------------------------------------------
			INLINE void Merge(MyNode** ppNode1, const u32& uiCount1, MyNode* pNode2, const u32& uiCount2, MyNode** ppLeft, MyNode** ppRight)
			{
				u32 i, j;
				i = j = 0;

				b8 bEnd1 = i < uiCount1;
				b8 bEnd2 = j < uiCount2;
				MyList List;
				MyNode* pNode;
				MyNode* pTemp;
				MyNode* pNode1 = *ppNode1;
				//	ノード1,2から昇順でListに登録していく
				while( bEnd1 || bEnd2 )
				{
					if( !bEnd2 || (bEnd1 && (*pNode1 < *pNode2)) )
					{
						pNode = pNode1->GetNext();
						pTemp = pNode1;
						pNode1->Remove();
						List.Entry(pNode1);
						pNode1 = pNode;
						i++;
					}
					else
					{
						pNode = pNode2->GetNext();
						pNode2->Remove();
						List.Entry(pNode2);
						pNode2 = pNode;
						j++;
					}

					bEnd1 = i < uiCount1;
					bEnd2 = j < uiCount2;
				}

				*ppNode1 = List.m_pFirst;
				m_pEnd = List.m_pEnd;
			}

		private:
			u32		m_uiCount;	//!< ノードの数
			MyNode*	m_pFirst;	//!< 先頭ノード
			MyNode*	m_pEnd;		//!< 末尾ノード
		};
	}
}

#endif	// __KGL_LIST_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Process.h
//!	@brief	プロセス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_PROCESS_H__
#define	__KGL_PROCESS_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Process
	{
		//!	プロセス情報
		struct ProcessInfo
		{
			c8	cExeName[256];		//!< 実行ファイル名(パス含む)
			c8	cCommandLine[256];	//!< コマンドライン
			c8	cCurrentDir[256];	//!< カレントディレクトリ
		};

		//======================================================================
		//!	プロセス制御インターフェイス
		//======================================================================
		interface IProcess
			: public IKGLBase
		{
		public:
			//!	別プロセスの起動
			//!	@param Info [in] プロセス情報
			virtual b8 Create(ProcessInfo& Info) = 0;
			//!	プロセスの終了待ち
			//!	@param uiTimeout [in] タイムアウト(ミリ秒単位)
			//!	@return false:タイムアウト
			virtual b8 WaitProcess(u32 uiTimeout = InfiniteWait) = 0;
		};

		//!	プロセス制御の生成
		//!	@param ppProcess	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IProcess** ppProcess);
	}
}

#endif	// __KGL_PROCESS_H__
//=======================================================================
//	END OF FILE
//=======================================================================
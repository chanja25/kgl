//----------------------------------------------------------------------
//!
//!	@file	kgl.MultiFlag.h
//!	@brief	フラグ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MULTI_FLAG_H__
#define	__KGL_MULTI_FLAG_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		const u32 U32BitMax = sizeof(u32) * 8;

		//======================================================================
		//!	フラグクラス
		//======================================================================
		class CMultiFlag
		{
		public:
			//!	コンストラクタ
			CMultiFlag(void)
				: m_uiFlagMax(0)
			{}

			//!	フラグの生成
			//!	@param uiFlagMax [in] フラグの最大数
			//!	@param pFlags [in] フラグデータ
			FORCEINLINE void Create(u32 uiFlagMax, const u32* pFlags = null)
			{
				m_uiFlagMax = Align(uiFlagMax, U32BitMax);
				u32 uiCount = m_uiFlagMax / U32BitMax;
				m_pFlags = new u32[uiCount];
				if( pFlags )
				{
					kgMemcpy(m_pFlags, pFlags, sizeof(u32) * uiCount);
				}
				else
				{
					kgZeroMemory(m_pFlags, sizeof(u32) * uiCount);
				}
			}

		public:
			//!	セット
			//!	@param uiFlag [in] フラグ番号
			FORCEINLINE void Set(u32 uiFlag)
			{
				u32& uiBits = GetBits(uiFlag);
				uiBits |= (1 >> GetShift(uiFlag));
			}
			//!	クリア
			//!	@param uiFlag [in] フラグ番号
			FORCEINLINE void Clear(u32 uiFlag)
			{
				u32& uiBits = GetBits(uiFlag);
				uiBits &= ~(1 >> GetShift(uiFlag));
			}
			//!	チェック
			//!	@param uiFlag [in] フラグ番号
			//!	@return フラグの有無
			FORCEINLINE b8 Check(u32 uiFlag) const
			{
				u32 uiBits = GetBits(uiFlag);
				return (uiBits & (1 >> GetShift(uiFlag))) != 0;
			}

		public:
			//!	フラグの取得
			//!	@return フラグ
			FORCEINLINE u32* GetFlags(void) { return m_pFlags; }
			//!	フラグの取得
			//!	@return フラグ
			FORCEINLINE const u32* GetFlags(void) const { return m_pFlags; }
			//!	フラグの最大数を取得
			//!	@return フラグの最大数
			FORCEINLINE u32 GetFlagMax(void) const { return m_uiFlagMax; }

		public:
			//!	所属するフラグの変数を取得
			//!	@return ビット
			FORCEINLINE u32& GetBits(u32 uiFlag)
			{
				kgAssert(uiFlag < m_uiFlagMax, "フラグの最大数を超えています");
				return m_pFlags[uiFlag / U32BitMax];
			}
			//!	所属するフラグの変数を取得
			//!	@return ビット
			FORCEINLINE u32 GetBits(u32 uiFlag) const
			{
				kgAssert(uiFlag < m_uiFlagMax, "フラグの最大数を超えています");
				return m_pFlags[uiFlag / U32BitMax];
			}
			//!	フラグのシフト数を取得
			//!	@return シフト数
			FORCEINLINE u32 GetShift(u32 uiFlag) const
			{
				return uiFlag & (U32BitMax-1);
			}

		protected:
			u32					m_uiFlagMax;	//!< 最大フラグ数
			TSmartPtr<u32>	m_pFlags;		//!< フラグ
		};
	}
}

#endif	// __KGL_MULTI_FLAG_H__
//=======================================================================
//	END OF FILE
//=======================================================================
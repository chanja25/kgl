//----------------------------------------------------------------------
//!
//!	@file	kgl.endian.h
//!	@brief	エンディアン関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ENDIAN_H__
#define	__KGL_ENDIAN_H__

//----------------------------------------------------------------------
//	define
//----------------------------------------------------------------------
#define	DECL_BSWAP(_type_, _func_)		FORCEINLINE _type_ bswap(_type_* p) { return _func_(p); }
#define	DECL_BSWAP_ORDER(_type_, ...)	FORCEINLINE void* bswap(_type_* p) { return bswap(p, __VA_ARGS__); }

namespace kgl
{
	//----------------------------------------------------------------------
	//!	リトルエンディアンかチェック
	//!	@return リトルエンディアンか？
	//----------------------------------------------------------------------
	FORCEINLINE b8 IsLittleEndian(void)
	{
		s16 sTemp = 1;
		return !!*(s8*)&sTemp;
	}

	//----------------------------------------------------------------------
	//!	２バイト列のエンディアン変換
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T bswap16(T* p)
	{
		register u8* ptr = (u8*)p;
		register u8 c0 = ptr[0];
		register u8 c1 = ptr[1];

		ptr[0] = c1;
		ptr[1] = c0;
		return *ptr;
	}
	//----------------------------------------------------------------------
	//!	４バイト列のエンディアン変換
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T bswap32(T* p)
	{
		register u8* ptr = (u8*)p;
		register u8 c0 = ptr[0];
		register u8 c1 = ptr[1];
		register u8 c2 = ptr[2];
		register u8 c3 = ptr[3];

		ptr[0] = c3;
		ptr[1] = c2;
		ptr[2] = c1;
		ptr[3] = c0;
		return *ptr;
	}
	//----------------------------------------------------------------------
	//!	８バイト列のエンディアン変換
	//----------------------------------------------------------------------
	template<typename T>
	FORCEINLINE T bswap64(T* p)
	{
		register u8* ptr = (u8*)p;
		register u8 c0 = ptr[0];
		register u8 c1 = ptr[1];
		register u8 c2 = ptr[2];
		register u8 c3 = ptr[3];
		register u8 c4 = ptr[4];
		register u8 c5 = ptr[5];
		register u8 c6 = ptr[6];
		register u8 c7 = ptr[7];

		ptr[0] = c7;
		ptr[1] = c6;
		ptr[2] = c5;
		ptr[3] = c4;
		ptr[4] = c3;
		ptr[5] = c2;
		ptr[6] = c1;
		ptr[7] = c0;
		return *ptr;
	}

	//----------------------------------------------------------------------
	//!	変換オーダー指定のエンディアン変換
	//----------------------------------------------------------------------
	INLINE s8* bswap(void* ptr, const c8* order, u32 loop = 1)
	{
		if( ptr == null ) return null;

		register u32 i, j;
		register s8* pData = (s8*)ptr;
		for( i = 0; i < loop; i++ )
		{
			for( j = 0; order[j] != '\0'; j++ )
			{
				switch( order[j] )
				{
					case '1':					pData += 1; break;
					case '2': bswap16(pData);	pData += 2; break;
					case '4': bswap32(pData);	pData += 4; break;
					case '8': bswap64(pData);	pData += 8; break;
				}
			}
		}
		return pData;
	}

	DECL_BSWAP(c8, *);
	DECL_BSWAP(s8, *);
	DECL_BSWAP(u8, *);
	DECL_BSWAP(c16, bswap16);
	DECL_BSWAP(s16, bswap16);
	DECL_BSWAP(u16, bswap16);
	DECL_BSWAP(s32, bswap32);
	DECL_BSWAP(u32, bswap32);
	DECL_BSWAP(f32, bswap32);
	DECL_BSWAP(s64, bswap64);
	DECL_BSWAP(u64, bswap64);
	DECL_BSWAP(f64, bswap64);
	DECL_BSWAP(dword, bswap32);
	DECL_BSWAP_ORDER(Color, "");
	DECL_BSWAP_ORDER(FColor, "4444");
	DECL_BSWAP_ORDER(Vector2, "44");
	DECL_BSWAP_ORDER(Vector3, "444");
	DECL_BSWAP_ORDER(Vector4, "4444");
	DECL_BSWAP_ORDER(Rotation, "444");
	DECL_BSWAP_ORDER(LightRotation, "444");
	DECL_BSWAP_ORDER(Quaternion, "4444");
	DECL_BSWAP_ORDER(RotationMatrix, "444", 3);
	DECL_BSWAP_ORDER(Matrix3x3, "444", 3);
	DECL_BSWAP_ORDER(Matrix4x3, "4444", 3);
	DECL_BSWAP_ORDER(Matrix, "4444", 4);
}

#endif	// __KGL_ENDIAN_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.NArray.h
//!	@brief	配列
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_NARRAY_H__
#define	__KGL_NARRAY_H__
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	配列テンプレート
		//	@param T 型
		//======================================================================
		template<typename T>
		class TNArray
		{
		public:
			//!	コンストラクタ
			TNArray(void)
				: m_pArray(null), m_uiNum(0), m_uiMax(0)
			{}
			//!	コンストラクタ
			//!	@param uiNum [in] 配列数
			TNArray(u32 uiNum)
				: m_uiNum(0)
			{
				m_pArray = new T[uiNum];
				m_uiMax = uiNum;
			}
			//!	デストラクタ
			~TNArray(void)
			{
				release();
			}

		public:
			//!	配列参照
			//!	@param n [in] 配列番号
			//!	@return 値
			FORCEINLINE T& operator [] (const u32& n)
			{
				kgAssert(n >= 0 || n < m_uiNum, CharSet::Format("配列外参照です(TArray[%d])", n));
				return m_pArray[n];
			}
			//!	配列数確保
			//!	@param uiNum [in] 配列数
			FORCEINLINE void reserve(u32 uiNum)
			{
				if( uiNum <= m_uiMax )	return;
				T* pArray = new T[uiNum];
				kgMemcpy(pArray, m_pArray, sizeof(T) * m_uiNum);

				SafeDeleteArray(m_pArray);
				m_pArray = pArray;
				m_uiMax  = uiNum;
			}
			//!	配列解放
			FORCEINLINE void release(void)
			{
				SafeDeleteArray(m_pArray);
				m_uiNum = m_uiMax = 0;
			}
			//!	指定要素の削除
			//!	@param uiNo [in] 要素番号
			FORCEINLINE void erase(u32 uiNo)
			{
				kgMemmove(&m_pArray[uiNo], &m_pArray[uiNo + 1], sizeof(T) * (m_uiNum - uiNo - 1));
				m_uiNum --;
			}
			//!	指定要素の要素追加
			//!	@param uiNo [in] 要素番号
			FORCEINLINE void insert(u32 uiNo, T Value)
			{
				kgMemmove(&m_pArray[uiNo + 1], &m_pArray[uiNo], sizeof(T) * (m_uiNum - uiNo - 1));
				m_uiNum ++;
			}			
			//!	末尾に要素追加
			//!	@param Value [in] 値
			FORCEINLINE void push_back(T Value)
			{
				if( m_uiNum == m_uiMax )
				{
					return;
				}
				kgAssert(m_uiNum < m_uiMax, "最大要素数を超えました");

				m_pArray[m_uiNum] = Value;
				m_uiNum ++;
			}			
			//!	末尾に要素追加
			//!	@param Value [in] 値
			FORCEINLINE void push_back(void)
			{
				kgAssert(m_uiNum < m_uiMax, "最大要素数を超えました");
				m_uiNum ++;
			}			
			//!	末尾の要素削除
			FORCEINLINE void pop_back(void)
			{
				kgAssert(m_uiNum != 0, "要素がありません");
				m_uiNum --;
			}			
			//!	先頭要素取得
			//!	@return 先頭要素
			FORCEINLINE T& front(void)
			{
				kgAssert(m_uiNum != 0, "要素がありません");
				return m_pArray[0];
			}			
			//!	末尾要素取得
			//!	@return 末尾要素
			FORCEINLINE T& back(void)
			{
				kgAssert(m_uiNum != 0, "要素がありません");
				return m_pArray[m_uiNum - 1];
			}
			//!	配列要素の削除
			FORCEINLINE void clear(void) { m_uiNum = 0; }
			//!	要素が空なら真を返す
			//!	@return 要素が空か
			FORCEINLINE b8 empty(void) { return m_uiNum == 0; }
			//!	配列数取得
			//!	@return 配列数
			FORCEINLINE u32 size(void) { return m_uiNum; }
			//!	保持可能な最大配列数取得
			//!	@return 最大配列数
			FORCEINLINE s32 max_size(void) { return m_uiMax; }

		private:
			T*	m_pArray;	//!< 配列データ
			u32	m_uiNum;	//!< 現在の配列数
			u32	m_uiMax;	//!< 最大配列数
		};
	}
}

#endif	// __KGL_NARRAY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
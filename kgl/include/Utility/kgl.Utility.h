//----------------------------------------------------------------------
//!
//!	@file	kgl.Utility.h
//!	@brief	ユーティリティ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_UTILITY_H__
#define	__KGL_UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.Array.h"
#include "kgl.NArray.h"
#include "kgl.Flag.h"
#include "kgl.DoubleBuffer.h"
#include "kgl.Ring.h"
#include "kgl.SmartPointer.h"

namespace kgl
{
	namespace Utility
	{
		//!	PC名の取得
		//!	@return PC名
		const c8* ComputerName(void);
		//!	ユーザー名の取得
		//!	@return ユーザー名
		const c8* UserName(void);
	}
}

#endif	// __KGL_UTILITY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
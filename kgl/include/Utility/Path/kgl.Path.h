//----------------------------------------------------------------------
//!
//!	@file	kgl.Path.h
//!	@brief	パス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_PATH_H__
#define	__KGL_PATH_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Path
	{
		//!	拡張子取得
		//!	@param pPath	[in] パス
		//!	@return 拡張子
		const c8* GetExtension(const c8* pPath);
		//!	ファイル名取得
		//!	@param pPath	[in] パス
		//!	@return ファイル名
		const c8* GetFileName(const c8* pPath);
		//!	ファイル名(拡張子除く)取得
		//!	@param pOut		[out] ファイル名
		//!	@param pPath	[in] パス
		const c8* GetFileNameWithoutExtension(c8* pOut, const c8* pPath);
		//!	ディレクトリ取得
		//!	@param pOut		[out] ディレクトリ
		//!	@param pPath	[in]  パス
		const c8* GetDirectory(c8* pOut, const c8* pPath);

		//!	ファイル名(拡張子除く)取得
		//!	@param pOut		[out] ファイル名
		algorithm::string GetFileNameWithoutExtension(const c8* pPath);
		//!	ディレクトリ取得
		//!	@param pOut		[out] ディレクトリ
		algorithm::string GetDirectory(const c8* pPath);
	}
}

#endif	// __KGL_PATH_H__
//=======================================================================
//	END OF FILE
//=======================================================================
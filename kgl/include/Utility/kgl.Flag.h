//----------------------------------------------------------------------
//!
//!	@file	kgl.Flag.h
//!	@brief	フラグ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FLAG_H__
#define	__KGL_FLAG_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	フラグクラス
		//======================================================================
		template<typename T>
		class TFlag
		{
		public:
			//!	コンストラクタ
			FORCEINLINE TFlag(void)
				: m_Flag(0)
			{}
			//!	コンストラクタ
			FORCEINLINE TFlag(T Flag)
				: m_Flag(Flag)
			{}

		public:
			//!	セット
			//!	@param Flag [in] フラグ
			FORCEINLINE void Set(T Flag) { m_Flag |= Flag; }
			//!	クリア
			//!	@param Flag [in] フラグ
			FORCEINLINE void Clear(T Flag) { m_Flag &= ~Flag; }
			//!	クリア
			FORCEINLINE void Clear(void) { m_Flag = 0; }
			//!	チェック
			//!	@param Flag [in] フラグ
			//!	@return フラグの有無
			FORCEINLINE b8 Check(T Flag) const { return ( m_Flag & Flag ) != 0; }
			//!	フラグ取得
			//!	@return フラグ
			FORCEINLINE const T& Get(void) const { return m_Flag; }

		private:
			T	m_Flag;	//!< フラグ
		};

		//	フラグクラス定義
		typedef TFlag<u32>	CFlag;
		typedef TFlag<u16>	CFlag16;
		typedef TFlag<u8>	CFlag8;
	}
}

#endif	// __KGL_FLAG_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Ring.h
//!	@brief	リングバッファ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_RING_H__
#define	__KGL_RING_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	リングバッファ用テンプレート
		//======================================================================
		template<typename T, s32 N>
		class TRing
		{
		public:
			//!	コンストラクタ
			TRing(void)
				: m_iCount(0)
				, m_iNow(0)
			{}

		public:
			//!	参照
			//!	@param iNo [in] 配列番号
			//!	@return 値
			FORCEINLINE T& operator [] (s32 iNo)
			{
				kgAssert(iNo < m_iCount, "access error");

				register s32 iIndex = m_iNow - m_iCount + iNo;
				if( iIndex >= m_iCount )	iIndex -= m_iCount;
				if( iIndex < 0 )			iIndex += m_iCount;
				return m_Array[iIndex];
			}
			//!	参照
			//!	@param iNo [in] 配列番号
			//!	@return 値
			FORCEINLINE void push_back(const T& value)
			{
				m_Array[m_iNow] = value;
				m_iNow ++;
				m_iNow   = (m_iNow == N)? 0: m_iNow;
				m_iCount = Max(m_iCount+1, N);
			}
			//!	先頭要素の取得
			//!	@return 値
			FORCEINLINE T& front(void)
			{
				register s32 iIndex = m_iNow - m_iCount;
				if( iIndex < 0 ) iIndex += m_iCount;
				return m_Array[iIndex];
			}
			//!	末尾要素の取得
			//!	@return 値
			FORCEINLINE T& back(void)
			{
				register s32 iIndex = m_iNow - 1;
				if( iIndex < 0 ) iIndex += m_iCount;
				return m_Array[iIndex];
			}

			//!	クリア
			FORCEINLINE void clear(void) { m_iNow = m_iCount = 0; }
			//!	長さ取得
			FORCEINLINE s32 length(void)	{ return m_iCount; }

		private:
			s32	m_iCount;	//!< バッファ数
			s32	m_iNow;		//!< 現在の位置
			T	m_Array[N];	//!< バッファ
		};
	}
}

#endif	// __KGL_RING_H__
//======================================================================
//	END OF FILE
//======================================================================
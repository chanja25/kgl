//----------------------------------------------------------------------
//!
//!	@file	kgl.HashList.h
//!	@brief	ハッシュリスト
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_HASH_LIST_H__
#define	__KGL_HASH_LIST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "Kgl.Hash.h"

namespace kgl
{
	namespace Hash
	{
		//======================================================================
		//!	ハッシュリスト
		//======================================================================
		template<typename T1, typename T2 = T1>
		class THashList
		{
		public:
			//!	コンストラクタ
			THashList(void)
			{
				CreateCRC32(m_pHash.GetReference());
			}

			//!	インデックスの取得
			//!	@param hash	[in] ハッシュ算出用データ
			//!	@return インデックス
			s32 GetIndex(T2& hash)
			{
				s32 iHash = m_pHash->Get(&hash, sizeof(T2));
				for( u32 i=0; i < m_Values.size(); i++ )
				{
					if( iHash == m_Values[i].iHash )
					{
						return i;
					}
				}
				return algorithm::NotFind;
			}
			//!	データの取得
			//!	@param index	[in] インデックス
			//!	@return データ
			T1& GetValue(s32 iIndex)
			{
				return m_Values[iIndex].Value;
			}
			//!	データの追加
			//!	@param value	[in] 値
			//!	@param hash		[in] ハッシュ算出用データ
			void AddValue(T1& value, T2& hash)
			{
				s32 iHash = m_pHash->Get(&hash, sizeof(T2));

				HashValue NewValue;
				NewValue.Value	= value;
				NewValue.iHash	= iHash;
				m_Values.push_back(NewValue);
			}

		private:
			//!	ハッシュ値
			struct HashValue
			{
				T1	Value;	//!< 値
				s32	iHash;	//!< ハッシュ値
			};

			TKGLPtr<IHash>					m_pHash;	//!< ハッシュ算出用
			algorithm::vector<HashValue>	m_Values;	//!< データリスト
		};
	}
}

#endif	// __KGL_HASH_LIST_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.CommandLine.h
//!	@brief	コマンドライン管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_COMMANDLINE_H__
#define	__KGL_COMMANDLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		//!	最大コマンド管理数
		const u32 COMMAND_COUNT_MAX	= 64;
		//!	最大コマンド文字数
		const u32 COMMAND_MAX		= 256;

		//======================================================================
		//!	コマンドライン管理インターフェイス
		//======================================================================
		interface ICommandLine
			: public IKGLBase
		{
		public:
			//!	コマンド数の取得
			//!	@return コマンド数
			virtual u32 GetCount(void) = 0;
			//!	コマンドの取得
			//!	@param uiNo	[in] コマンド番号
			//!	@return コマンド
			virtual const c8* GetCommand(u32 uiNo) = 0;
			//!	コマンドの追加
			//!	@param pCommand	[in] コマンド
			//!	@return 結果
			virtual b8 AddCommand(const c8* pCommand) = 0;
			//!	コマンドの削除
			//!	@param uiNo	[in] コマンド番号
			//!	@return 結果
			virtual b8 EraseCommand(u32 uiNo) = 0;
		};

		//!	コマンドライン管理の生成
		//!	@param ppCommandLine [out] 実体格納用ポインタ
		//!	@param pCommandLine [in] コマンドライン引数(nullの場合はGetCommandLine()で取得)
		//!	@return 結果
		b8 Create(ICommandLine** ppCommandLine, const c8* pCommandLine = null);
	}
}

#endif	// __KGL_COMMANDLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
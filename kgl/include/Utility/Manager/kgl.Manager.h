//----------------------------------------------------------------------
//!
//!	@file	kgl.Manager.h
//!	@brief	管理クラス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_MANAGER_H__
#define	__KGL_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "../List/kgl.List.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	マネージャテンプレートクラス
		//======================================================================
		template<typename T>
		class TManager
		{
		public:
			//======================================================================
			//!	マネージャ用アイテムテンプレートクラス
			//======================================================================
			class CItem
			{
				friend class TManager<T>;
			public:
				//!	オブジェクトの取得
				INLINE T& GetObject(void)	{ return m_Object; }
				//!	次のアイテム取得
				INLINE CItem* GetNext(void)
				{
					TNode<CItem>* pNode = m_Node.GetNext();
					return pNode? pNode->GetObject(): null;
				}

				//!	前のアイテム取得
				INLINE CItem* GetPrev(void)
				{
					TNode<CItem>* pNode = m_Node.GetPrev();
					return pNode? pNode->GetObject(): null;
				}

				//!	前のアイテム取得
				INLINE void SetSort(f32 fSort)
				{
					m_Node.SetSort(fSort);
				}

			private:
				TNode<CItem>	m_Node;		//!< 管理用ノード
				T				m_Object;	//!< 管理オブジェクト
			};

		public:
			//----------------------------------------------------------------------
			//!	コンストラクタ
			//----------------------------------------------------------------------
			INLINE TManager(void)
				: m_pItem(null), m_uiMax(0)
			{}
			//----------------------------------------------------------------------
			//!	デストラクタ
			//----------------------------------------------------------------------
			INLINE ~TManager(void)
			{
				Finalize();
			}

		public:
			//----------------------------------------------------------------------
			//!	初期化
			//!	@param uiMax	[in] 最大管理数
			//!	@return 結果
			//----------------------------------------------------------------------
			INLINE b8 Initialize(u32 uiMax)
			{
				Finalize();

				m_uiMax	= uiMax;
				m_pItem	= new CItem[uiMax];
				for( u32 i=0; i < uiMax; i++ )
				{
					m_pItem[i].m_Node.SetObject(&m_pItem[i]);
					m_Free.Entry(&m_pItem[i].m_Node);
				}

				return true;
			}

			//----------------------------------------------------------------------
			//!	終了処理
			//----------------------------------------------------------------------
			INLINE void Finalize(void)
			{
				m_uiMax = 0;
				
				m_Free.Clear();
				m_Use.Clear();

				SafeDeleteArray(m_pItem);
			}

			//----------------------------------------------------------------------
			//!	管理アイテム生成
			//!	@return アイテム(使用可能なものが無ければnull)
			//----------------------------------------------------------------------
			INLINE CItem* CreateItem(void)
			{
				TNode<CItem>* pNode = m_Free.GetFirst();
				//	アイテムが無ければnullを返す
				if( pNode == null )	return null;

				m_Free.Remove(pNode);
				m_Use.Entry(pNode);

				return pNode->GetObject();
			}

			//----------------------------------------------------------------------
			//!	管理アイテム解放
			//!	@param pNode アイテム(使用可能なものが無ければnull)
			//----------------------------------------------------------------------
			INLINE void ReleaseItem(CItem* pItem)
			{
				if( pItem == null )	return;
				TNode<CItem>* pNode = &pItem->m_Node;

				m_Use.Remove(pNode);
				m_Free.Entry(pNode);
			}

			//----------------------------------------------------------------------
			//!	管理アイテムの先頭を取得
			//!	@param pNode アイテム(使用可能なものが無ければnull)
			//----------------------------------------------------------------------
			INLINE CItem* GetFirst(void) { return m_Use.GetFirst()? m_Use.GetFirst()->GetObject(): null; }
			//----------------------------------------------------------------------
			//!	ソート
			//----------------------------------------------------------------------
			INLINE void Sort(void) { m_Use.Sort(); }

		public:
			//!	最大管理数取得
			INLINE u32 GetMax(void) { return m_uiMax; }
			//!	使用可能数取得
			INLINE u32 FreeCount(void) { return m_Free.GetNum(); }
			//!	使用数取得
			INLINE u32 UseCount(void) { return m_Use.GetNum(); }

		private:
			CItem*			m_pItem;	//!< 管理アイテム
			TList<CItem>	m_Free;		//!< 未使用のアイテムリスト
			TList<CItem>	m_Use;		//!< 使用中のアイテムリスト
			u32				m_uiMax;	//!< 最大管理数
		};
	}
}

#endif	// __KGL_MANAGER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
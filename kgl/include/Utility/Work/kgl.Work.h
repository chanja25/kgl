//----------------------------------------------------------------------
//!
//! @file	kgl.Work.h
//! @brief	処理管理
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_WORK_H__
#define	__KGL_WORK_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Work
	{
		//!	作業情報
		struct WorkInfo
		{
			FThreadFunction	Func;		//!< 作業用コールバック
			void*			pArg;		//!< 引数
		};

		//!	初期化
		//!	@param iMaxThread [in] 最大スレッド数
		//!	@return 結果
		b8 Initialize(s32 iMaxThread = 4);
		//!	終了処理
		void Finalize(void);

		//!	作業リクエスト
		//!	@param Info [in] 作業情報
		//!	@return 作業に割り当てられるID(-1で失敗)
		s32	WorkRequest(s32 iThread, WorkInfo& Info);
		//!	作業リクエスト
		//!	@return 作業に割り当てられるID(-1で失敗)
		b8 IsFinish(WorkInfo& Info);
	}
}

#endif	// __KGL_WORK_H__
//=======================================================================
//	END OF FILE
//=======================================================================
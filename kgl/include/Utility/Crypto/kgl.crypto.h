//----------------------------------------------------------------------
//!
//!	@file	kgl.crypto.h
//!	@brief	暗号化
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_CRYPTO_H__
#define	__KGL_CRYPTO_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace crypto
	{
		//======================================================================
		//!	暗号化インターフェイス
		//======================================================================
		interface ICrypto
			: public IKGLBase
		{
		public:
			//!	キーの設定
			//!	@param pKey	[in] 暗号化キー
			//!	@param pIV	[in] 初期ベクトル
			virtual void SetKey(const c16* pKey, const c16* pIV = L"KGL Library") = 0;

		public:
			//!	暗号化
			//!	@param pBuffer	[in] バッファ
			//!	@param uiSize	[in] サイズ
			//!	@return 結果
			virtual b8 Encode(const void* pBuffer, u32 uiSize) = 0;
			//!	復号化
			//!	@param pBuffer	[in] バッファ
			//!	@param uiSize	[in] サイズ
			//!	@return 結果
			virtual b8 Decode(const void* pBuffer, u32 uiSize) = 0;

		public:
			//!	暗号(複合)化済みバッファの取得
			//!	@return バッファ
			virtual const void* GetBuffer(void) = 0;
			//!	暗号(複合)化済みバッファの取得
			//!	@return サイズ
			virtual u32 GetSize(void) = 0;
		};

		//!	暗号化クラスの生成
		//!	@param ppCrypto [out] 実体格納用ポインタ
		//!	@return 結果
		b8 CreateAES(ICrypto** ppCrypto);
		//!	暗号化クラスの生成
		//!	@param ppCrypto [out] 実体格納用ポインタ
		//!	@return 結果
		b8 CreateDES(ICrypto** ppCrypto);
	}
}

#endif	// __KGL_HASH_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.crypto.cpp
//!	@brief	暗号化
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "Utility/Crypto/kgl.crypto.h"
#include <string>

#pragma warning(push)
#pragma warning(disable: 4003)
#pragma warning(disable: 4100)
#pragma warning(disable: 4189)
#pragma warning(disable: 4244)
#pragma warning(disable: 4616)
#define	CRYPTOPP_ENABLE_NAMESPACE_WEAK	1
#include <type_traits>
#include <modes.h>
#include <filters.h>
#include <aes.h>
#include <des.h>
#include <md5.h>
#include <hmac.h>
#include <hex.h>
#pragma warning(pop)

namespace kgl
{
	namespace crypto
	{
		using namespace CryptoPP;
		//======================================================================
		//	暗号化クラステンプレート
		//======================================================================
		template<typename Type = AES, typename Mode = CTR_Mode<Type>>
		class CCrypto
			: public ICrypto
		{
		private:
			typedef	Type	CryptoType;
			typedef	Mode	CryptoMode;

			typedef	decltype(CryptoMode::Encryption())	Encryption;
			typedef	decltype(CryptoMode::Decryption())	Decryption;

			static const u32 PasswordLength = CryptoType::DEFAULT_KEYLENGTH;

		public:
			//----------------------------------------------------------------------
			//	コンストラクタ
			//----------------------------------------------------------------------
			CCrypto(void)
				: m_bKey(false)
			{}
			//----------------------------------------------------------------------
			//	キーの設定
			//----------------------------------------------------------------------
			void SetKey(const c16* pKey, const c16* pIV)
			{
				auto password = MakeHash<Weak1::MD5>(pKey);
				auto iv = MakeHash<Weak1::MD5>(pIV);

				m_encode.SetKeyWithIV((byte*)password.data(), PasswordLength, (byte*)iv.data());
				m_decode.SetKeyWithIV((byte*)password.data(), PasswordLength, (byte*)iv.data());

				m_bKey = true;
			}

			//----------------------------------------------------------------------
			//	暗号化
			//----------------------------------------------------------------------
			b8 Encode(const void* pBuffer, u32 uiSize)
			{
				if( !m_bKey ) return false;

				try
				{
					std::string encoded;
					StringSource((byte*)pBuffer, uiSize, true, new StreamTransformationFilter(m_encode, new StringSink(encoded)));
					m_buffer.swap(encoded);
				}
				catch (...)
				{
					return false;
				}
				return true;
			}
			//----------------------------------------------------------------------
			//	復号化
			//----------------------------------------------------------------------
			b8 Decode(const void* pBuffer, u32 uiSize)
			{
				if( !m_bKey ) return false;

				try
				{
					std::string decoded;
					StringSource((byte*)pBuffer, uiSize, true, new StreamTransformationFilter(m_decode, new StringSink(decoded)));
					m_buffer.swap(decoded);
				}
				catch (...)
				{
					return false;
				}
				return true;
			}

		public:
			//----------------------------------------------------------------------
			//	暗号(複合)化済みバッファの取得
			//----------------------------------------------------------------------
			const void* GetBuffer(void)
			{
				return &m_buffer[0];
			}
			//----------------------------------------------------------------------
			//	暗号(複合)化済みバッファの取得
			//----------------------------------------------------------------------
			u32 GetSize(void)
			{
				return (u32)m_buffer.size();
			}

		private:
			template <typename Hash>
			std::string MakeHash(const std::wstring& buffer)
			{
				SecByteBlock passtext(Hash::DIGESTSIZE);
				Hash hash;
				hash.Update((byte*)buffer.c_str(), buffer.size());
				hash.Final(passtext);

				std::string out;
				out.resize(Hash::DIGESTSIZE);
				hash.CalculateDigest((byte*)&out[0], passtext.begin(), passtext.size());

				return out;
			}

		private:
			std::string	m_buffer;	//!< バッファ
			bool		m_bKey;		//!< キーが設定されているか？

			Encryption	m_encode;	//!< エンコード用
			Decryption	m_decode;	//!< デコード用
		};

		//----------------------------------------------------------------------
		//	暗号化クラスの生成
		//----------------------------------------------------------------------
		b8 CreateAES(ICrypto** ppCrypto)
		{
			*ppCrypto = new crypto::CCrypto<>();
			(*ppCrypto)->AddRef();

			return true;
		}
		//----------------------------------------------------------------------
		//	暗号化クラスの生成
		//----------------------------------------------------------------------
		b8 CreateDES(ICrypto** ppCrypto)
		{
			*ppCrypto = new crypto::CCrypto<DES>();
			(*ppCrypto)->AddRef();

			return true;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
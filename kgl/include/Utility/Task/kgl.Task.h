//----------------------------------------------------------------------
//!
//!	@file	kgl.Task.h
//!	@brief	非同期タスク管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TASK_H__
#define	__KGL_TASK_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Task
	{
		//======================================================================
		//!	タスクインターフェイス
		//======================================================================
		interface ITaskBase
			: public IKGLBase
		{
		public:
			//!	タスク処理
			//!	@return trueを返している間は繰り返し実行する
			virtual b8 OnWork(void) = 0;
		};
		//======================================================================
		//!	タスク監視インターフェイス
		//======================================================================
		interface ITaskObserverBase
			: public IKGLBase
		{
		public:
			//!	タスクの実行待ち
			virtual void Wait(void) = 0;

		public:
			//!	生存中か？
			//!	@return 生存の有無
			virtual b8 IsAlive(void) = 0;
			//!	タスクの実行待ち中か？
			//!	@return 実行待ちの有無
			virtual b8 IsWaiting(void) = 0;
		};

		//======================================================================
		//!	タスク管理インターフェイス
		//======================================================================
		interface ITaskManager
			: public IKGLBase
		{
		public:
			//!	タスクに追加する
			//!	@param pTask		[in] 起動するタスク
			//!	@param bExclusive	[in] 専用のスレッドで実行するか？
			//!	@return タスクの
			virtual TKGLPtr<ITaskObserverBase> Join(ITaskBase* pTask, b8 bExclusive = false) = 0;

		public:
			//!	タスク実行用の最大スレッド数
			//!	@return 結果
			virtual u32 ThreadMax(void) = 0;
		};


		const u32 AsyncTaskMax	= 256;

		//!	作業情報
		struct TaskInfo
		{
			FTaskFunction	Func;		//!< 作業用コールバック(引数はpArgとスレッド番号)
			void*			pArg;		//!< コールバックの引数
			s32				iThreadNo;	//!< 使用するスレッド番号(0〜iMaxThread-1, -1で自動割り当て)

			//!	コンストラクタ
			FORCEINLINE TaskInfo(void)
				: Func(null), pArg(null)
				, iThreadNo(-1)
			{}
		};
		//!	作業状態
		struct TaskState
		{
			s32	iThreadNo;	//!< 使用するスレッド番号
			s32	iTaskId;	//!< タスクに割り当てられたID
		};

		//!	タスクリクエスト
		//!	@param pInfo	[in]  タスク情報
		//!	@param pState	[out] タスクの状態
		//!	@return リクエスト結果
		b8 RequestTask(TaskInfo* pInfo, TaskState* pState);
		//!	タスク終了しているか？
		//!	@param iTaskId [in] タスクID
		//!	@return trueでタスク終了
		b8 IsFinish(s32 iTaskId);
		//!	スレッドが作業中か？
		//!	@param iThreadNo [in] スレッド番号
		//!	@return 作業の有無
		b8 IsWorking(s32 iThreadNo);

		//!	指定タスクが終了するまで待つ
		//!	@param iTaskId [in] タスクID
		void WaitTask(s32 iTaskId);
		//!	指定スレッドのタスクが全て終了するまで待つ
		//!	@param iTaskId [in] タスクID
		void WaitThreadTask(s32 iThreadNo);
		//!	スレッドのタスクが全て終了するまで待つ
		void WaitThreadAllTask(void);

		//!	スレッドの自動割り当て対象にする(デフォルトは有効)
		//!	@param iThreadNo [in] スレッド番号
		void EnableAutoSelect(s32 iThreadNo);
		//!	スレッドの自動割り当て対象から外す
		//!	@param iThreadNo [in] スレッド番号
		void DisableAutoSelect(s32 iThreadNo);
		//!	スレッドの自動割り当ての有無を取得
		//!	@param iThreadNo [in] スレッド番号
		//!	@return 自動割り当ての有無
		b8 IsAutoSelect(s32 iThreadNo);
	}
}

#define	START_ASYNCTASK(Name, FuncName, Arg)						\
	s32	Name##TaskId;												\
	{																\
		kgl::Task::TaskInfo		Name##TaskInfo;						\
		kgl::Task::TaskState	Name##TaskState;					\
		Name##TaskInfo.Func	= FuncName;								\
		Name##TaskInfo.pArg	= Arg;									\
		kgl::Task::RequestTask(&Name##TaskInfo, &Name##TaskState);	\
		Name##TaskId = Name##TaskState.iTaskId;						\
	}
#define	WAIT_ASYNCTASK(Name)	kgl::Task::WaitTask(Name##TaskId)
#define	CHECK_ASYNCTASK(Name)	kgl::Task::IsFinish(Name##TaskId)

#endif	// __KGL_TASK_H__
//=======================================================================
//	END OF FILE
//=======================================================================
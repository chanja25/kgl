//----------------------------------------------------------------------
//!
//!	@file	kgl.Sort.h
//!	@brief	ソートシステム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_SORT_H__
#define	__KGL_SORT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "Thread/kgl.Thread.h"
#include "Thread/kgl.Event.h"

namespace kgl
{
	namespace Sort
	{
		//	スレッドの最大数
		const s32 ThreadMax = 4;

		//	ソートスレッドの制限数の最大値
		const s32 LimitMax = 0xffffffff;

		//	ソートスレッドの制限数のデフォルト値
		const s32 LimitSimgleDefault	= 1024;
		const s32 LimitDualDefault		= 4096;
		const s32 LimitTripleDefault	= 8192;

		//======================================================================
		//!	ソートシステム(スレッドは最大でクアッドコアまで対応)
		//======================================================================
		template<typename T>
		class TSortSystem
		{
			typedef b8 (*compare)(const T&, const T&);
			//!	ソート情報
			struct SortInfo
			{
				T*	pBegin;
				T*	pWork;
				u32	uiCount;
			};

		public:
			//!	コンストラクタ
			//!	@param bStable	[in] 安定ソートを使用するか？
			//!	@param bThread	[in] スレッドを使用するか？
			TSortSystem(b8 bStable = false, b8 bThread = false);
			//!	デストラクタ
			~TSortSystem(void);

		public:
			//!	ソート
			//!	@param pBegin	[in] 先頭アドレス
			//!	@param uiCount	[in] 要素数
			//!	@param Func		[in] 関数
			void SortReqest(T* pBegin, u32 uiCount, compare Func);

		public:
			//!	安定ソートの有効設定
			//!	@param bThread	[in] 安定ソートを使用するか？
			void SetEnableStable(b8 bStable);

			//!	スレッドの有効設定
			//!	@param bThread	[in] スレッドを使用するか？
			void SetEnableThread(b8 bEnable);
			//!	スレッドの有効設定
			//!	@param bThread	[in] スレッドを使用するか？
			void SetThreadLimit(u32 uiSingle = LimitTripleDefault, u32 uiDual = LimitDualDefault, u32 ucTriple = LimitTripleDefault);

		private:
			//!	ソート
			//!	@param pBegin	[in] 先頭アドレス
			//!	@param uiCount	[in] 要素数
			//!	@param Func		[in] 関数
			void Sort(T* pBegin, u32 uiCount, compare Func);
			//!	ソート
			//!	@param pBegin	[in] 先頭アドレス
			//!	@param pWork	[in] 作業バッファ
			//!	@param uiCount	[in] 要素数
			//!	@param Func		[in] 関数
			void StableSort(T* pBegin, T* pWork, u32 uiCount, compare Func);

		private:
			//!	ソートスレッド
			static u32 SortThreadMain(void* pArg);
			//!	ソートスレッド
			void SortThread(void);

		private:
			T*							m_pBegin;				//!< 先頭アドレス
			T*							m_pWork;				//!< 作業バッファ
			u32							m_uiCount;				//!< 要素数
			bool						m_bInitThread;			//!< スレッドが初期化されているか？
			bool						m_bExit;				//!< 終了するか？
			bool						m_bStable;				//!< 安定ソートを使用するか？
			bool						m_bThread;				//!< スレッドを使用するか？
			u32							m_uiLimitSingle;		//!< シングルコアで処理を行う最大要素数
			u32							m_uiLimitDual;			//!< デュアルコアで処理を行う最大要素数
			u32							m_uiLimitTriple;		//!< トリプルコアで処理を行う最大要素数
			u32							m_uiRunThreadNum;		//!< 起動しているスレッドの数
			u32							m_uiUseCoreNum;			//!< ソートに使用するコアの数
			compare						m_compare;				//!< 比較関数
			TKGLPtr<Thread::IThread>	m_pThread[ThreadMax];	//!< ソートスレッド用
			TKGLPtr<Thread::IEvent>		m_pEvent[ThreadMax-1];	//!< イベント
			SortInfo					m_SortInfo[ThreadMax-1];//!< ソート情報
		};
	}
}

#endif	// __KGL_SORT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Array.h
//!	@brief	配列
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ARRAY_H__
#define	__KGL_ARRAY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Utility
	{
		//======================================================================
		//!	配列テンプレート
		//	@param T 型
		//	@param N サイズ
		//======================================================================
		template<typename T, s32 N>
		class TArray
		{
		public:
			//!	配列参照
			//!	@param n [in] 配列番号
			T& operator [] (const s32& n)
			{
				kgAssert(n >= 0 || n < N, CharSet::Format("配列外参照です(TArray[%d])", n));

				return m_Array[n];
			}

			//!	データクリア
			FORCEINLINE void ZeroClear(void) { kgZeroMemory(m_Array, sizeof(T) * N); }
			//!	配列数取得
			//!	@return 配列数
			FORCEINLINE s32 size(void) { return N; }

		private:
			T	m_Array[N];	//!< 配列データ
		};
	}
}

#endif	// __KGL_ARRAY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
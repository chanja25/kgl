//----------------------------------------------------------------------
//!
//!	@file	kgl.allocator.h
//!	@brief	allocator
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_ALLOCATOR_H__
#define	__KGL_ALGLITHM_ALLOCATOR_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace algorithm
	{
		//======================================================================
		//!	alocator
		//======================================================================
		class allocator
		{
		public:
			//!	メモリ確保
			//!	@param size	[in] 確保サイズ
			//!	@return 確保メモリの先頭アドレス
			static INLINE void* Allocate(u32 size) { return kgl::Memory::Allocate(size); }
			//!	メモリ再確保
			//!	@param memory	[in] 現在のメモリ
			//!	@param size		[in] 確保サイズ
			//!	@return 確保メモリの先頭アドレス
			static INLINE void* Reallocate(void* memory, u32 size) { return kgl::Memory::Reallocate(memory, size); }
			//!	メモリ解放
			//!	@param ptr	[in] 解放するメモリの先頭アドレス
			static INLINE void Free(void* ptr) { kgl::Memory::Free(ptr); }
		};
	}
}

#endif	// __KGL_ALGLITHM_ALLOCATOR_H__
//======================================================================
//	END OF FILE
//======================================================================
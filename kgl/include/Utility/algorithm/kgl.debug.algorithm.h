//----------------------------------------------------------------------
//!
//!	@file	kgl.debug.allocator.h
//!	@brief	debug allocator
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DEBUG_ALGLITHM_ALLOCATOR_H__
#define	__KGL_DEBUG_ALGLITHM_ALLOCATOR_H__

#if	KGL_DEBUG_MEMORY
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace algorithm
	{
		//======================================================================
		//!	debug_alocator
		//======================================================================
		class debug_allocator
		{
		public:
			//!	メモリ確保
			//!	@param size [in] 確保サイズ
			//!	@return 確保メモリの先頭アドレス
			static INLINE void* Allocate(u32 size)	{ return Memory::Debug::Allocate(size); }
			//!	メモリ再確保
			//!	@param memory	[in] 現在のメモリ
			//!	@param size		[in] 確保サイズ
			//!	@return 確保メモリの先頭アドレス
			static INLINE void* Reallocate(void* memory, u32 size) { return kgl::Memory::Reallocate(memory, size); }
			//!	メモリ解放
			//!	@param ptr [in] 解放するメモリの先頭アドレス
			static INLINE void Free(void* ptr) { Memory::Debug::Free(ptr); }
		};

		//!	debug_vector
		template <typename Type>
		class debug_vector
			: public base_vector<Type, debug_allocator>
		{};
		//!	debug_string
		typedef base_string<c8, debug_allocator>	debug_string;
		//!	debug_wstring
		typedef base_string<c16, debug_allocator>	debug_wstring;
	}
}
#endif	// ~#if KGL_DEBUG_MEMORY

#endif	// __KGL_DEBUG_ALGLITHM_ALLOCATOR_H__
//======================================================================
//	END OF FILE
//======================================================================
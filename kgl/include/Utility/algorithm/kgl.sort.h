//----------------------------------------------------------------------
//!
//!	@file	kgl.sort.h
//!	@brief	sort
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_SORT_H__
#define	__KGL_ALGLITHM_SORT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.utility.h"

namespace kgl
{
	namespace algorithm
	{
		//----------------------------------------------------------------------
		//!	挿入ソート
		//----------------------------------------------------------------------
		template<typename T>
		INLINE void isort(T* begin, u32 size, b8 (*compare)(const T&, const T&) = utility::lessequal)
		{
			if( begin == null || size == 0 || compare == null )return;
			sort::insert_sort(begin, size, compare);
		}
		//----------------------------------------------------------------------
		//!	クイックソート
		//----------------------------------------------------------------------
		template<typename T>
		INLINE void qsort(T* begin, u32 size, b8 (*compare)(const T&, const T&) = utility::less)
		{
			if( begin == null || size == 0 || compare == null )return;
			sort::quick_sort(begin, 0, size - 1, compare);
		}
		//----------------------------------------------------------------------
		//!	マージソート
		//----------------------------------------------------------------------
		template<typename T>
		INLINE void msort(T* begin, u32 size, T* work, b8 (*compare)(const T&, const T&) = utility::lessequal)
		{
			if( begin == null || size == 0 || compare == null )return;

			//	要素数が少ない場合は挿入ソートを使用する
			sort::marge_sort(begin, work, 0, size - 1, compare);
		}
		//----------------------------------------------------------------------
		//!	マージソート
		//----------------------------------------------------------------------
		template<typename T>
		INLINE void msort(T* begin, u32 size, b8 (*compare)(const T&, const T&) = utility::lessequal)
		{
			if( begin == null || size == 0 || compare == null )return;

			//	要素数が少ない場合は挿入ソートを使用する
			if( size < utility::Threashhold )
			{
				sort::insert_sort(begin, size, compare);
				return;
			}
			T* work = (T*)Memory::Allocate(sizeof(T) * size);
			sort::marge_sort(begin, work, 0, size - 1, compare);
			Memory::Free(work);
		}
	}
}


#endif	// __KGL_ALGLITHM_SORT_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.find.h
//!	@brief	find
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_FIND_H__
#define	__KGL_ALGLITHM_FIND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.utility.h"

namespace kgl
{
	namespace algorithm
	{
		//----------------------------------------------------------------------
		//!	���`�T��
		//----------------------------------------------------------------------
		template<typename T1, typename T2>
		INLINE u32 linear_search(const T1& key, const T2* begin, u32 size, b8 (*compare)(const T1&, const T2&) = utility::equal)
		{
			for( u32 i = 0; i < size; i++ )
			{
				if( compare(key, begin[i]) ) return i;
			}
			return NotFind;
		}
		//----------------------------------------------------------------------
		//!	�񕪌���
		//----------------------------------------------------------------------
		template<typename T1, typename T2>
		INLINE u32 binary_search(const T1& key, const T2* begin, s32 size, s32 (*compare)(const T1&, const T2&) = utility::compare)
		{
			register s32 low, high, mid;

			low  = 0;
			high = size - 1;
			while( low <= high )
			{
				mid = (low + high) >> 1;
				s32 ret = compare(key, begin[mid]);
				if( ret == 0 ) return mid;

				(ret < 0)? (high = mid - 1): (low = mid + 1);
			}
			return NotFind;
		}
	}
}

#endif	// __KGL_ALGLITHM_FIND_H__
//======================================================================
//	END OF FILE
//======================================================================
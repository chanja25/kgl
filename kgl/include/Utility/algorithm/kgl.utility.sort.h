//----------------------------------------------------------------------
//!
//!	@file	kgl.utility.sort.h
//!	@brief	utility
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_UTILITY_SORT_H__
#define	__KGL_ALGLITHM_UTILITY_SORT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "../../Thread/kgl.Thread.h"
#include <algorithm>

namespace kgl
{
	namespace algorithm
	{
		namespace sort
		{
			//!	挿入ソート
			template<typename T>
			FORCEINLINE void insert_sort(T* begin, u32 size, b8 (*compare)(const T&, const T&))
			{
				for( u32 i=1; i < size; ++i )
				{
					for( u32 j=i; j >= 1 && compare(begin[j], begin[j-1]); --j )
					{
						utility::swap(begin[j], begin[j-1]);
					}
				}
			}

			//!	クイックソート
			template<typename T>
			FORCEINLINE void quick_sort(T* begin, u32 left, u32 right, b8 (*compare)(const T&, const T&))
			{
				//	要素数が少ない場合は挿入ソートに切り替える
				if( right - left < utility::Threashhold )
				{
					insert_sort(&begin[left], right - left + 1, compare);
					return;
				}

				u32 i, j;
				i = left;
				j = right;
				//	中心(評価基準)の選定
				const T pivot = utility::median(begin[left], begin[(left + right) / 2], begin[right], compare);
				while( true )
				{
					while( compare(begin[i], pivot) ) ++ i;
					while( compare(pivot, begin[j]) ) -- j;
					if( i >= j ) break;

					utility::swap(begin[i], begin[j]);
					++ i; --j;
				}

				if( left < i - 1 )	quick_sort(begin, left, i - 1, compare);
				if( j + 1 < right )	quick_sort(begin, j + 1, right, compare);
			}

			//!	マージソート
			template<typename T>
			FORCEINLINE void marge_sort(T* begin, T* work, u32 left, u32 right, b8 (*compare)(const T&, const T&))
			{
				//	要素数が少ない場合は挿入ソートに切り替える
				if( right - left < utility::Threashhold )
				{
					insert_sort(&begin[left], right - left + 1, compare);
					return;
				}

				u32 mid;
				if( left >= right )return;

				mid = (left + right) / 2;
				marge_sort(begin, work, left, mid, compare);
				marge_sort(begin, work, mid + 1, right, compare);

				u32 i, j, k;
				for( i = left; i <= mid; ++i )
				{
					utility::copy(work[i], begin[i]);
				}
				for( j = right; i <= right; ++i, --j )
				{
					utility::copy(work[i], begin[j]);
				}

				i = left;
				j = right;

				for( k = left; k <= right; ++ k )
				{
					if( compare(work[i], work[j]) )
					{
						utility::copy(begin[k], work[i++]);
					}
					else
					{
						utility::copy(begin[k], work[j--]);
					}
				}
			}

			template<typename T>
			struct MargeWorkInfo
			{
				T* begin;
				T* work;
				u32 left;
				u32 right;
				b8 (*compare)(const T&, const T&);
			};

			//!	マージソート
			template<typename T>
			static u32 THREADFUNC marge_sort_thread(void* pArg)
			{
				MargeWorkInfo<T>* pInfo = (MargeWorkInfo<T>*)pArg;
				marge_sort(pInfo->begin, pInfo->work, pInfo->left, pInfo->right, pInfo->compare);

				u32 i;
				for( i = pInfo->left; i <= pInfo->right; ++i )
				{
					utility::copy(pInfo->work[i], pInfo->begin[i]);
				}
				return 0;
			}
			
			//!	マージソート
			template<typename T>
			FORCEINLINE void marge_sort_dualcore(T* begin, T* work, u32 left, u32 right, b8 (*compare)(const T&, const T&))
			{
				u32 mid;
				mid = (left + right) / 2;

				TKGLPtr<Thread::IThread> pThread;
				Thread::Create(pThread.GetReference());

				MargeWorkInfo<T> Info;
				Info.begin = begin;
				Info.work = work;
				Info.left = left;
				Info.right = mid;
				Info.compare = compare;

				pThread->SetPriority(Thread::EPriority::Highest);
				pThread->Start(marge_sort_thread<T>, &Info, 0, "marge_thread");
				marge_sort(begin, work, mid + 1, right, compare);

				u32 i, j, k;
				i = mid + 1;
				for( j = right; i <= right; ++i, --j )
				{
					utility::copy(work[i], begin[j]);
				}
				pThread->Exit();

				i = left;
				j = right;

				for( k = left; k <= right; ++ k )
				{
					if( compare(work[i], work[j]) )
					{
						utility::copy(begin[k], work[i++]);
					}
					else
					{
						utility::copy(begin[k], work[j--]);
					}
				}
			}
			//!	マージソート
			template<typename T>
			FORCEINLINE void marge_sort_trycore(T* begin, T* work, u32 left, u32 right, b8 (*compare)(const T&, const T&))
			{
				u32 mid1, mid2;
				mid1 = (left + right) / 3;
				mid2 = mid1 * 2;

				TKGLPtr<Thread::IThread> pThread1;
				Thread::Create(pThread1.GetReference());
				TKGLPtr<Thread::IThread> pThread2;
				Thread::Create(pThread2.GetReference());

				MargeWorkInfo<T> Info1;
				Info1.begin = begin;
				Info1.work = work;
				Info1.left = left;
				Info1.right = mid1;
				Info1.compare = compare;

				MargeWorkInfo<T> Info2;
				Info2.begin = begin;
				Info2.work = work;
				Info2.left = mid1 + 1;
				Info2.right = mid2;
				Info2.compare = compare;

				pThread1->SetPriority(Thread::EPriority::Highest);
				pThread2->SetPriority(Thread::EPriority::Highest);
				pThread1->Start(marge_sort_thread<T>, &Info1, 0, "marge_thread");
				pThread2->Start(marge_sort_thread<T>, &Info2, 0, "marge_thread");
				marge_sort(begin, work, mid2 + 1, right, compare);

				u32 i, j, k, l;
				i = mid2 + 1;
				for( j = right; i <= right; ++i, --j )
				{
					utility::copy(work[i], begin[j]);
				}
				pThread1->Exit();
				pThread2->Exit();

				i = left;
				j = right;
				l = mid1 + 1;

				for( k = left; k <= right; ++ k )
				{
					if( i <= mid1 && compare(work[i], work[j]) )
					{
						if( l > mid2 || compare(work[i], work[l]) )
						{
							utility::copy(begin[k], work[i++]);
						}
						else
						{
							utility::copy(begin[k], work[l++]);
						}
					}
					else if( j > mid2 )
					{
						if( l > mid2 || compare(work[j], work[l]) )
						{
							utility::copy(begin[k], work[j--]);
						}
						else
						{
							utility::copy(begin[k], work[l++]);
						}
					}
					else
					{
						utility::copy(begin[k], work[l++]);
					}
				}
			}
			//!	マージソート
			template<typename T>
			static u32 THREADFUNC marge_sort_thread_dualcore(void* pArg)
			{
				MargeWorkInfo<T>* pInfo = (MargeWorkInfo<T>*)pArg;
				marge_sort_dualcore(pInfo->begin, pInfo->work, pInfo->left, pInfo->right, pInfo->compare);

				u32 i;
				for( i = pInfo->left; i <= pInfo->right; ++i )
				{
					utility::copy(pInfo->work[i], pInfo->begin[i]);
				}
				return 0;
			}
			//!	マージソート
			template<typename T>
			FORCEINLINE void marge_sort_quadcore(T* begin, T* work, u32 left, u32 right, b8 (*compare)(const T&, const T&))
			{
				u32 mid;
				mid = (left + right) / 2;

				TKGLPtr<Thread::IThread> pThread;
				Thread::Create(pThread.GetReference());

				MargeWorkInfo<T> Info;
				Info.begin = begin;
				Info.work = work;
				Info.left = left;
				Info.right = mid;
				Info.compare = compare;

				pThread->SetPriority(Thread::EPriority::Highest);
				pThread->Start(marge_sort_thread_dualcore<T>, &Info, 0, "marge_thread");
				marge_sort_dualcore(begin, work, mid + 1, right, compare);

				u32 i, j, k;
				i = mid + 1;
				for( j = right; i <= right; ++i, --j )
				{
					utility::copy(work[i], begin[j]);
				}
				pThread->Exit();

				i = left;
				j = right;

				for( k = left; k <= right; ++ k )
				{
					if( compare(work[i], work[j]) )
					{
						utility::copy(begin[k], work[i++]);
					}
					else
					{
						utility::copy(begin[k], work[j--]);
					}
				}
			}
		}
	}
}

#endif	// __KGL_ALGLITHM_UTILITY_SORT_H__
//======================================================================
//	END OF FILE
//======================================================================
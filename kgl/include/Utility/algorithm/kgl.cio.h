//----------------------------------------------------------------------
//!
//!	@file	kgl.cio.h
//!	@brief	文字列入出力
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGORITHM_CIO_H__
#define	__KGL_ALGORITHM_CIO_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "File/kgl.File.h"

namespace kgl
{
	namespace algorithm
	{
		//======================================================================
		//!	basic_cout
		//======================================================================
		template<typename Char, typename Allocator>
		class basic_cout
		{
		public:
			typedef base_string<c8, Allocator>		path_string;
			typedef base_string<Char, Allocator>	cout_string;

		public:
			//!	コンストラクタ
			basic_cout(void){}
			//!	コンストラクタ
			basic_cout(const path_string& path, b8 add = false)
			{
				write(path, add);
			}
			//!	コンストラクタ
			~basic_cout(void)
			{
				if (_file.IsValid()) _file->Close();
			}
			//!	コンストラクタ
			void write(const path_string& path, b8 add = false)
			{
				if (_file.IsValid()) _file->Close();
				TKGLPtr<File::IFile> file;
				File::Create(file.GetReference());
				if (file->Open(
					path.c_str(),
					File::EOpen::Write,
					File::EShare::Read,
					add ? File::ECreate::OpenAlways : File::ECreate::CreateAlways))
				{
					if (add) file->Seek(0, File::EFilePointer::End);
					_file = file;
				}
			}
			//!	ライン書き込み
			INLINE basic_cout& operator << (const cout_string& str)
			{
				if (_file.IsValid())
				{
					_file->Write(str.c_str(), str.length());
				}
				return *this;
			}

		private:
			cout_string				_str;	//!< 文字列
			TKGLPtr<File::IFile>	_file;	//!< ファイル管理
		};

		//======================================================================
		//!	basic_cin
		//======================================================================
		template<typename Char, typename Allocator>
		class basic_cin
		{
		public:
			typedef base_string<Char, Allocator>	cin_string;

		public:
			//!	コンストラクタ
			basic_cin(void){}
			//!	コンストラクタ
			basic_cin(const cin_string& path)
			{
				read(path);
			}
			//!	コンストラクタ
			~basic_cin(void)
			{
				if (_file.IsValid())	_file->Close();
			}
			//!	コンストラクタ
			void read(const cin_string& path)
			{
				if (_file.IsValid()) _file->Close();
				TKGLPtr<File::IFile> file;
				File::Create(file.GetReference());
				if (file->Open(
					path.c_str(),
					File::EOpen::Read,
					File::EShare::Read,
					File::ECreate::OpenExisting))
				{
					_file = file;
				}
			}

			//!	ライン読み込み
			INLINE basic_cin& operator >> (cin_string& str)
			{
				str.clear();
				if (_file.IsValid())
				{
					c8 temp;
					c8 buffer[512];
					u32 count;
					while (true)
					{
						if (_file->Read(&temp, 1) == 0)	break;
						if (temp == '\n' || temp == '\0')	break;
						buffer[count ++] = temp;
						if (count == ArrayCount(buffer) - 1)
						{
							buffer[count] = '\0';
							count = 0;
							str += buffer;
						}
					}
					if (count > 0)
					{
						buffer[count] = '\0';
						str += buffer;
					}
				}
				return *this;
			}

		private:
			TKGLPtr<File::IFile>	_file;	//!< ファイル管理
		};

		//!	cout
		typedef	basic_cout<c8, allocator>	cout;
		typedef	basic_cout<c16, allocator>	wcout;
		//!	cin
		typedef	basic_cin<c8, allocator>	cin;
		typedef	basic_cin<c16, allocator>	wcin;
	}
}


#endif	// __KGL_ALGORITHM_CIO_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.list.h
//!	@brief	list
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_LIST_H__
#define	__KGL_ALGLITHM_LIST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "Utility/algorithm/kgl.allocator.h"

namespace kgl
{
	namespace algorithm
	{
		//======================================================================
		//!	list
		//======================================================================
		template<typename TYPE, typename Allocator=allocator>
		class list
		{
		public:
			template<typename TYPE>
			class list_iterator;

			//======================================================================
			//!	値
			//======================================================================
			template<typename TYPE>
			class list_value
			{
			public:
				//!	キャスト
				INLINE operator TYPE&(void){ return _value; }

			public:
				TYPE				_value;
				list_iterator<TYPE>	_iterator;
			};

			//======================================================================
			//!	イテレータ
			//======================================================================
			template<typename TYPE>
			class list_iterator
			{
			public:
				//!	コンストラクタ
				INLINE list_iterator(void)
					: _ptr(null), _next(null), _prev(null)
				{
					_this = this;
				}
				//!	コンストラクタ
				INLINE list_iterator(list_value<TYPE>* ptr)
					: _ptr(ptr), _next(null), _prev(null)
				{
					_this = this;
				}
				//!	デストラクタ
				INLINE ~list_iterator(void)
				{}

			public:
				//!	キャスト
				INLINE operator TYPE*(void) { return *_this->_ptr; }
				//!	キャスト
				INLINE operator const TYPE*(void) const { return *_this->_ptr; }
				//!	->
				INLINE TYPE* operator ->(void) { return &_this->_ptr->value; }
				//!	->
				INLINE const TYPE* operator ->(void) const { return &_this->_ptr->value; }
				//!	インクリメント
				INLINE list_iterator& operator ++(void)
				{
					list_iterator old = *this;
					_this = _next;
					return old;
				}
				//!	デクリメント
				INLINE list_iterator operator --(void)
				{
					list_iterator old = *this;
					_this = _next;
					return old;
				}
				//!	インクリメント
				INLINE list_iterator& operator ++(s32)
				{
					_this = _next;
					return *this;
				}
				//!	デクリメント
				INLINE list_iterator& operator --(s32)
				{
					_this = _prev;
					return *this;
				}
				//!	等号
				INLINE b8 operator ==(const list_iterator& list) { return _this == list._this; }
				//!	不等号
				INLINE b8 operator !=(const list_iterator& list) { return _this != list._this; }

			public:
				//!	リストを繋げる
				//!	@param list [in/out] 繋げるリスト
				INLINE void chain(list_iterator& list)
				{
					if( _this->_next )		_this->_next->_prev			= list._this;
					if( list._this->_prev )	list._this->_prev->_next	= _this;
					list._this->_next	= _this->_next;
					_this->_prev		= list._this->_prev;

					_this->_next		= list._this;
					list._this->_prev	= _this;
				}
				//!	リストを外す
				INLINE void remove(void)
				{
					if( _this->_next )	_this->_next->_prev = _this->_prev;
					if( _this->_prev )	_this->_prev->_next = _this->_next;
					_this->_next = _this->_prev = null;
				}

			public:
				list_value<TYPE>*	_ptr;	//!< 値
				list_iterator*		_this;	//!< 自分自身
				list_iterator*		_next;	//!< 次の値
				list_iterator*		_prev;	//!< 前の値
			};

			typedef TYPE						reference;
			typedef	list_value<reference>		value_iterator;
			typedef	list_iterator<reference>	iterator;

		private:
			iterator*	_begin_ptr;	//!< 先頭要素のポインタ
			iterator	_end;		//!< 末尾要素
			u32			_count;		//!< 管理数

		public:
			//!	コンストラクタ
			INLINE list(void)
				: _begin_ptr(&_end), _count(0)
			{}
			//!	コンストラクタ
			//!	@param size  [in] 最大格納数
			//!	@param value [in] 要素の初期設定
			INLINE list(u32 size, const reference& value)
				: _begin_ptr(&_end), _count(0)
			{
				for( u32 i=0; i < size; i++ )
				{
					push_back(value);
				}
			}
			//!	コピーコンストラクタ
			//!	@param from [in] コピーするベクタ
			INLINE list(const list& from)
				: _begin_ptr(&_end), _count(0)
			{
				iterator it;
				for( it = from.begin(); it != from.end(); ++it )
				{
					push_back(*it);
				}
			}
			//!	デストラクタ
			INLINE ~list(void) { clear(); }

		public:
			//!	先頭要素のイテレータ取得
			//!	@return 先頭要素
			INLINE iterator begin(void) { return *_begin_ptr; }
			//!	末尾要素のイテレータ取得
			//!	@return 末尾要素
			INLINE iterator end(void) { return _end; }
			//!	先頭要素の取得
			INLINE reference& front(void) { return *_begin_ptr->_ptr; }
			//!	先頭要素の取得
			INLINE const reference& front(void) const { return *_begin_ptr->_ptr; }
			//!	末尾要素の取得
			INLINE reference& back(void) { return *_end._prev->_ptr; }
			//!	末尾要素の取得
			INLINE const reference& back(void) const { return *_end._prev->_ptr; }

			//!	要素の有無を取得
			//!	@return 要素が無い場合true
			INLINE b8 empty(void) const { return _count == 0; }
			//!	要素数の取得
			//!	@return 要素数
			INLINE u32 size(void) const { return _count; }

			//!	末尾に要素追加
			//!	@param value [in] 追加する値
			void push_back(const reference& value)
			{
				value_iterator* ptr	= (value_iterator*)allocator::Allocate(sizeof(value_iterator));
				iterator* it = &ptr->_iterator;
				new(it) iterator(ptr);
				ptr->_value = value;

				if( *_begin_ptr == _end )
				{
					_begin_ptr = it;
				}
				it->chain(_end);

				++ _count;
			}
			//!	先頭に要素追加
			//!	@param value [in] 追加する値
			void push_front(const reference& value)
			{
				list_value* ptr	= (list_value*)allocator::Allocate(sizeof(list_value));
				iterator* it = &ptr->_iterator;
				new(it) iterator(ptr);
				ptr->_value = value;

				it->chain(_begin_ptr);
				_begin_ptr = it;
				++ _count;
			}
			//!	末尾要素の削除
			void pop_back(void)
			{
				kgAssert(_count > 0, "要素がありません(list::pop_pack)");
				iterator* ptr = _end._prev;

				if( _begin_ptr == ptr )
				{
					_begin_ptr = &_end;
				}
				ptr->remove();
				ptr->~list_iterator();
				allocator::Free(ptr->_ptr);
				-- _count;
			}
			//!	先頭要素の削除
			void pop_front(void)
			{
				kgAssert(_count > 0, "要素がありません(list::pop_front)");
				iterator* ptr = _begin_ptr;

				_begin_ptr = ptr->_next;
				ptr->remove();
				ptr->~list_iterator();
				allocator::Free(ptr->_ptr);
				-- _count;
			}
			//!	先頭要素の削除
			//!	@param it [in] 削除する要素(引数の状態は保障されません)
			//!	@return 次の要素
			iterator erase(iterator& it)
			{
				kgAssert(_count > 0, "要素がありません(list::erase)");
				if( it == *_begin_ptr )
				{
					pop_front();
					return *_begin_ptr;
				}

				iterator* ptr = it._this;
				ptr->remove();
				ptr->~list_iterator();
				allocator::Free(ptr->_ptr);
				-- _count;

				return *ptr;
			}
			//!	指定要素間の要素を削除
			//!	@param start [in] 削除する要素の先頭(引数の状態は保障されません)
			//!	@param end   [in] 削除する要素の末尾(引数の状態は保障されません)
			//!	@return 次の要素
			INLINE iterator erase(iterator& start, iterator& end)
			{
				kgAssert(_count > 0, "要素がありません(list::erase)");
				iterator it = start;
				while( it != end )
				{
					it = erase(it);
				}
				return erase(it);
			}
			//!	全要素の削除
			INLINE void clear(void)
			{
				while( _count != 0 )
				{
					pop_back();
				}
			}
		};
	}
}

#endif	// __KGL_ALGLITHM_LIST_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.string.h
//!	@brief	string
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_STRING_UTILITY_H__
#define	__KGL_ALGLITHM_STRING_UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace algorithm
	{
		//!	文字列を指定した文字で分割
		//!	@param Out		[out] 分割した文字列
		//!	@param pString	[in]  文字列
		//!	@param cDelim	[in]  分割する文字
		template<typename Char, typename Allocator>
		INLINE void Split(vector<base_string<Char, Allocator>>& Out, const Char* pString, Char cDelim, b8 bCullEmpty = true)
		{
			Out.clear();
			u32 uiLen = kgStrlen(pString);

			string Work;
			Work.reserve(uiLen);
			for( u32 i=0; i < uiLen; i++ )
			{
				if( CharSet::IsMultiByteChar(pString[i]) )
				{
					Work += pString[i];
					Work += pString[++ i];
				}
				else if( pString[i] == cDelim )
				{
					if( !bCullEmpty || Work.length() != 0 )
					{
						Out.push_back(Work);
						Work.clear();
					}
				}
				else
				{
					Work += pString[i];
				}
			}
			if( !bCullEmpty || Work.length() != 0 )
			{
				Out.push_back(Work);
				Work.clear();
			}
		}
		//!	文字列を指定した文字で分割
		//!	@param Out		[out] 分割した文字列
		//!	@param pString	[in]  文字列
		//!	@param pDelim	[in]  分割する文字列
		template<typename Char, typename Allocator>
		INLINE void Split(vector<base_string<Char, Allocator>>& Out, const Char* pString, const Char* pDelim, b8 bCullEmpty = true)
		{
			Out.clear();
			u32 uiDelimLen = kgStrlen(pDelim);

			auto pStart = pString;
			if( pStart && uiDelimLen > 0 )
			{
				while( auto pAt = kgStrstr(pStart, pDelim) )
				{
					u32 uiSize = (u32)(pAt - pStart);
					if( !bCullEmpty || uiSize > 0 )
					{
						Out.push_back(string(pStart, uiSize));
					}
					pStart += uiDelimLen + uiSize;
				}
				if( !bCullEmpty || *pStart )
				{
					Out.push_back(pStart);
				}
			}
		}
		//!	指定した文字列を文字列から削除
		//!	@param strInput	[in] 文字列
		//!	@param pDelim	[in] 削除する文字列
		template<typename Char, typename Allocator>
		INLINE base_string<Char, Allocator> Trim(const base_string<Char, Allocator>& strInput, const Char* pDelim)
		{
			u32 uiDelimLen = kgStrlen(pDelim);

			string Work;
			Work.reserve(uiDelimLen);
			auto pStart = strInput.c_str();
			if( pStart && uiDelimLen > 0 )
			{
				while( auto pAt = kgStrstr(pStart, pDelim) )
				{
					u32 uiSize = (u32)(pAt - pStart);
					if( uiSize > 0 )
					{
						Work += string(pStart, uiSize);
					}
					pStart += uiDelimLen + uiSize;
				}
				if( *pStart )
				{
					Work += pStart;
				}
			}
			return Work;
		}
		//!	文字を囲んでいるダブルクオーテーションの削除
		//!	@param strInput			[in]  文字列
		//!	@param pQuotesRemoved	[out] ダブルクオーテーションを削除したか？
		//!	@return	文字列
		template<typename Char, typename Allocator>
		INLINE base_string<Char, Allocator> TrimQuotes(const base_string<Char, Allocator>& strInput, b8* pQuotesRemoved = null)
		{
			b8 bQuotesWereRemoved = false;
			u32 uiStart = 0, uiCount = strInput.length();
			if( uiCount > 0 )
			{
				if( strInput.front() == '"' )
				{
					++ uiStart;
					-- uiCount;
					bQuotesWereRemoved = true;
				}
				if( strInput.length() > 1 && strInput.back() == '"' )
				{
					-- uiCount;
					bQuotesWereRemoved = false;
				}
			}
			if( pQuotesRemoved )
			{
				*pQuotesRemoved = bQuotesWereRemoved;
			}
			return strInput.substr(uiStart, uiCount);
		}
	}
}

#endif	// __KGL_ALGLITHM_STRING_UTILITY_H__
//======================================================================
//	END OF FILE
//======================================================================
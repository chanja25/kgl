//----------------------------------------------------------------------
//!
//!	@file	kgl.string.h
//!	@brief	string
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_STRING_H__
#define	__KGL_ALGLITHM_STRING_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.vector.h"

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define	STR_CONSTRUCTOR(x)				\
			INLINE base_string(x ref)	\
			{							\
				clear();				\
				add(ref);				\
			}

#define	STR_OP_COPY(x)								\
			INLINE base_string& operator = (x ref)	\
			{										\
				clear();							\
				return add(ref);					\
			}

#define	STR_OP_ADD(x)								\
			INLINE base_string& operator += (x ref)	\
			{										\
				return add(ref);					\
			}										\
			INLINE base_string operator + (x ref)	\
			{										\
				return base_string(*this).add(ref);	\
			}

#define	STR_ADD(x)									\
			INLINE base_string& add(x ref)			\
			{										\
				return FormatAdd<chartype>(ref);	\
			}

#define	STR_FORMAT(x, format, ...)								\
			template<typename T>								\
			base_string& FormatAdd(x ref);						\
			template<>											\
			INLINE base_string& FormatAdd<c8>(x ref)			\
			{													\
				chartype temp[MAX_PATH];						\
				CharSet::SFormat(temp, format, __VA_ARGS__);	\
				return add(temp);								\
			}													\
			template<>											\
			INLINE base_string& FormatAdd<c16>(x ref)			\
			{													\
				chartype temp[MAX_PATH];						\
				CharSet::SFormat(temp, L##format, __VA_ARGS__);	\
				return add(temp);								\
			}

#define	STR_FUNC_LIST(x)		\
			STR_CONSTRUCTOR(x)	\
			STR_OP_COPY(x)		\
			STR_OP_ADD(x)

#define	STR_FUNC_LIST_SELF_COPY(x)	\
			STR_CONSTRUCTOR(x)		\
			STR_OP_ADD(x)

#define	STR_FUNC_LIST_ALL(x, format, ...)		\
			STR_CONSTRUCTOR(x)					\
			STR_OP_COPY(x)						\
			STR_OP_ADD(x)						\
			STR_ADD(x)							\
			STR_FORMAT(x, format, __VA_ARGS__)

#if X64
#define	STR_ADDR_FORMAT	"0x%x"
#else
#define	STR_ADDR_FORMAT	"0x%llx"
#endif

namespace kgl
{
	namespace algorithm
	{
		//======================================================================
		//!	basic_string
		//======================================================================
		template<typename Char, typename Allocator>
		class base_string
			: public base_vector<Char, Allocator>
		{
		private:
			typedef base_vector<Char, Allocator>	string_vector;
			typedef	Char							chartype;

		public:
			//!	コンストラクタ
			INLINE base_string(void) { clear(); }
			//!	コンストラクタ
			INLINE base_string(const chartype* str, u32 size)
			{
				clear();
				add(str, Min(kgStrlen(str), size));
			}
			//!	コンストラクタ
			INLINE base_string(const base_string& str, u32 size)
			{
				clear();
				add(str, Min(str.length(), size));
			}

		public:
			//!	代入
			INLINE base_string& operator = (const chartype* str)
			{
				if( str == c_str() ) return *this;
				clear();
				return add(str);
			}
			//!	代入
			INLINE base_string& operator = (const base_string& str)
			{
				if( &str == this ) return *this;
				clear();
				return add(str);
			}

		public:
			//!	文字列比較
			//!	@param str	[in] 文字列
			//!	@return 結果
			INLINE b8 operator == (const base_string& str) const { return equal(str); }
			//!	文字列比較
			//!	@param str	[in] 文字列
			//!	@return 結果
			INLINE b8 operator == (const chartype* str) const { return equal(str); }
			//!	文字列比較
			//!	@param str	[in] 文字列
			//!	@return 結果
			INLINE b8 operator != (const base_string& str) const { return !equal(str); }
			//!	文字列比較
			//!	@param str	[in] 文字列
			//!	@return 結果
			INLINE b8 operator != (const chartype* str) const { return !equal(str); }
			//!	追記
			//!	@param pos [in] 要素番号
			//!	@return 要素
			INLINE base_string& operator << (const base_string& ref) { return add(ref); }
			//!	文字列取得
			//!	@return 文字列
			INLINE const chartype* c_str() const { return data(); }
			//!	要素数の取得
			//!	@return 要素数
			INLINE u32 size(void) const { return string_vector::size() - 1; }
			//!	文字列取得
			//!	@return 長さ
			INLINE u32 length(void) const { return size(); }
			//!	要素の有無を確認
			//!	@return 要素が無い場合true
			INLINE b8 empty(void) const { return length() == 0; }
			//!	クリア
			INLINE void clear(void) { string_vector::resize(1); at(0) = 0; }

			//!	最大管理要素数の変更
			//!	@param size [in] 最大管理数
			INLINE void resize(u32 size) { string_vector::resize(size+1); -- _end; }

		public:
			//!	文字列比較
			//!	@param str [in] 比較する文字列
			//!	@return 比較結果
			INLINE s32 compare(const base_string& str) const { return compare(str.c_str()); }
			//!	文字列比較
			//!	@param str [in] 比較する文字列
			//!	@return 比較結果
			INLINE s32 compare(const chartype* str) const { return kgStrcmp(c_str(), str); }
			//!	文字列比較(大文字小文字を区別しない)
			//!	@param str [in] 比較する文字列
			//!	@return 比較結果
			INLINE s32 comparei(const base_string& str) const { return comparei(str); }
			//!	文字列比較(大文字小文字を区別しない)
			//!	@param str [in] 比較する文字列
			//!	@return 比較結果
			INLINE s32 comparei(const chartype* str) const { return kgStricmp(c_str(), str); }

			//!	指定文字数までの文字列比較(大文字小文字を区別しない)
			//!	@param str		[in] 比較する文字列
			//!	@param uiSize	[in] 文字数
			//!	@return 比較結果
			INLINE s32 compare(const base_string& str, u32 uiSize) const { return compare(str.c_str(), uiSize); }
			//!	指定文字数までの文字列比較(大文字小文字を区別しない)
			//!	@param str		[in] 比較する文字列
			//!	@param uiSize	[in] 文字数
			//!	@return 比較結果
			INLINE s32 compare(const chartype* str, u32 uiSize) const { return kgStrncmp(c_str(), str, uiSize); }
			//!	指定文字数までの文字列比較(大文字小文字を区別しない)
			//!	@param str		[in] 比較する文字列
			//!	@param uiSize	[in] 文字数
			//!	@return 比較結果
			INLINE s32 comparei(const base_string& str, u32 uiSize) const { return comparei(str.c_str(), uiSize); }
			//!	指定文字数までの文字列比較(大文字小文字を区別しない)
			//!	@param str		[in] 比較する文字列
			//!	@param uiSize	[in] 文字数
			//!	@return 比較結果
			INLINE s32 comparei(const chartype* str, u32 uiSize) const { return kgStrnicmp(c_str(), str, uiSize); }
			//!	一致する文字列の開始位置を検索
			//!	@param str	[in] 検索する文字列
			//!	@return 一致文字列
			INLINE const chartype* strstr(const chartype* str) const { return kgStrstr(c_str(), str); }
			//!	一致する文字列の開始位置を検索
			//!	@param str	[in] 検索する文字列
			//!	@return 一致文字列
			INLINE const chartype* strstr(const base_string& str) const { return strstr(c_str(), str.c_str()); }

			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@return インデックス
			INLINE b8 contains(const chartype* str) const { return strstr(str) != null; }
			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@return インデックス
			INLINE b8 contains(const base_string& str) const { return strstr(str.c_str()) != null; }

			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@return インデックス
			INLINE b8 equal(const base_string& str) const
			{
				if( at(0) != str[0] )				return false;

				u32 uiLen = str.length();
				if( length() != uiLen )				return false;
				if( uiLen <= 1 )					return true;
				if( at(uiLen-1) != str[uiLen-1] )	return false;

				u32 uiMid = uiLen / 2;
				if( at(uiMid) != str[uiMid] )		return false;
				if( uiLen <= 3 )					return true;

				return compare(str) == 0;
			}
			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@return インデックス
			INLINE b8 equal(const chartype* str) const
			{
				if( str && at(0) != str[0] )		return false;

				u32 uiLen = kgStrlen(str);
				if( length() != uiLen )				return false;
				if( uiLen <= 1 )					return true;
				if( at(uiLen-1) != str[uiLen-1] )	return false;

				u32 uiMid = uiLen / 2;
				if( at(uiMid) != str[uiMid] )		return false;
				if( uiLen <= 3 )					return true;

				return compare(str) == 0;
			}
			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@param uiSize	[in] 文字数
			//!	@return インデックス
			INLINE b8 equal(const base_string& str, u32 uiSize) const
			{
				if( at(0) != str[0] )				return false;
				if( length() < uiSize )				return false;

				u32 uiLen = uiSize;
				if( uiLen <= 1 )					return true;
				if( at(uiLen-1) != str[uiLen-1] )	return false;

				u32 uiMid = uiLen / 2;
				if( at(uiMid) != str[uiMid] )		return false;
				if( uiLen <= 3 )					return true;

				return compare(str, uiSize) == 0;
			}
			//!	要素の比較
			//!	@param str [in] 比較する値
			//!	@param uiSize	[in] 文字数
			//!	@return インデックス
			INLINE b8 equal(const chartype* str, u32 uiSize) const
			{
				if( str && at(0) != str[0] )		return false;
				if( length() < uiSize )				return false;

				u32 uiLen = uiSize;
				if( uiLen <= 1 )					return true;
				if( at(uiLen-1) != str[uiLen-1] )	return false;

				u32 uiMid = uiLen / 2;
				if( at(uiMid) != str[uiMid] )		return false;
				if( uiLen <= 3 )					return true;

				return compare(str, uiSize) == 0;
			}

			//!	先頭文字列が一致しているか？
			//!	@param str	[in] 比較する文字列
			//!	@return 比較結果
			INLINE b8 startwith(const base_string& str) const { return equal(str, str.length()); }
			//!	文字列比較(大文字小文字を区別しない)
			//!	@param str	[in] 比較する文字列
			//!	@return 比較結果
			INLINE b8 startwith(const chartype* str) const { return equal(str, kgStrlen(str)); }

		public:
			//!	先頭から指定箇所までの文字列を取得
			//!	@param left	[in] 終端文字位置
			//!	@return 分割した文字列
			INLINE base_string left(u32 left) const
			{
				return base_string(c_str(), left < length()? left: length());
			}
			//!	末尾から指定箇所までの文字列を取得
			//!	@param right	[in] 開始文字位置
			//!	@return 分割した文字列
			INLINE base_string right(u32 right) const
			{
				u32 left = right < length(): length() - right: length();
				return base_string(&at(left), length() - left);
			}
			//!	指定範囲の文字列を取得
			//!	@param left		[in] 開始文字位置
			//!	@param right	[in] 終端文字位置
			//!	@return 分割した文字列
			INLINE base_string substr(u32 left, u32 count = 0xffffffff) const
			{
				count = Min(count, length()-left);
				if( count == 0 ) return base_string();

				return base_string(&at(left), count);
			}

		public:
			STR_FUNC_LIST_SELF_COPY(const base_string&);
			STR_FUNC_LIST_SELF_COPY(const chartype*);

			STR_FUNC_LIST(bool);
			STR_FUNC_LIST_ALL(const void*, STR_ADDR_FORMAT, ref);
			STR_FUNC_LIST_ALL(s64, "%lld", ref);
			STR_FUNC_LIST_ALL(s32, "%d", ref);
			STR_FUNC_LIST_ALL(s16, "%hd", ref);
			STR_FUNC_LIST_ALL(s8, "%hhd", ref);
			STR_FUNC_LIST_ALL(u64, "%llu", ref);
			STR_FUNC_LIST_ALL(u32, "%u", ref);
			STR_FUNC_LIST_ALL(u16, "%hu", ref);
			STR_FUNC_LIST_ALL(u8, "%hhu", ref);
			STR_FUNC_LIST_ALL(f32, "%f", ref);
			STR_FUNC_LIST_ALL(f64, "%f", ref);
			STR_FUNC_LIST_ALL(chartype, "%c", ref);

			STR_FUNC_LIST_ALL(const Vector2&, "%f %f", ref.x, ref.y);
			STR_FUNC_LIST_ALL(const Vector3&, "%f %f %f", ref.x, ref.y, ref.z);
			STR_FUNC_LIST_ALL(const Vector4&, "%f %f %f %f", ref.x, ref.y, ref.z, ref.w);
			STR_FUNC_LIST_ALL(const Color&, "%hhu %hhu %hhu %hhu", ref.r, ref.g, ref.b, ref.a);
			STR_FUNC_LIST_ALL(const FColor&, "%f %f %f %f", ref.r, ref.g, ref.b, ref.a);
			STR_FUNC_LIST_ALL(const Rotation&, "%f %f %f", ref.yaw, ref.pitch, ref.roll);
			STR_FUNC_LIST_ALL(const LightRotation&, "%d %d %d", ref.yaw, ref.pitch, ref.roll);
			STR_FUNC_LIST_ALL(const Quaternion&, "%f %f %f %f", ref.x, ref.y, ref.z, ref.w);

		public:
			//	追加関数
			INLINE base_string& add(const chartype* pString, u32 uiLen)
			{
				resize(length() + uiLen);
				if( 0 < uiLen )
				{
					kgMemcpy(&at(length()-uiLen), pString, uiLen * sizeof(chartype));
				}
				return *this;
			}
			INLINE base_string& add(const base_string& str) { return add(str.c_str(), str.length()); }
			INLINE base_string& add(const chartype* str) { return add(str, kgStrlen(str)); }

			template<typename T>base_string& BoolStringAdd(bool b);
			template<> INLINE base_string& BoolStringAdd<c8>(bool b) { return add(b? "true": "false"); }
			template<> INLINE base_string& BoolStringAdd<c16>(bool b) { return add(b? L"true": L"false"); }

			INLINE base_string& add(bool b) { return BoolStringAdd<chartype>(b); }
		};

		//!	文字列比較
		//!	@param str1	[in] 文字列1
		//!	@param str2	[in] 文字列2
		//!	@return 結果
		template<typename Char, typename Allocator>
		INLINE b8 operator == (const Char* str1, const base_string<Char, Allocator>& str2)
		{
			return str2.equal(str1);
		}
		//!	文字列の結合
		//!	@param str1	[in] 文字列1
		//!	@param str2	[in] 文字列2
		//!	@return 要素
		template<typename Char, typename Allocator>
		INLINE base_string<Char, Allocator> operator + (const Char* str1, const base_string<Char, Allocator>& str2)
		{
			return base_string<Char, Allocator>(str1) + str2;
		}

		//!	string
		typedef	base_string<c8, allocator>	string;
		//!	wstring
		typedef	base_string<c16, allocator>	wstring;
	}
}

#include "kgl.string.utility.h"

#endif	// __KGL_ALGLITHM_STRING_H__
//======================================================================
//	END OF FILE
//======================================================================
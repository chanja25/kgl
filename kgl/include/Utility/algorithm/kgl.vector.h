//----------------------------------------------------------------------
//!
//!	@file	kgl.vector.h
//!	@brief	vector
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_VECTOR_H__
#define	__KGL_ALGLITHM_VECTOR_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.allocator.h"
#include "kgl.find.h"

namespace kgl
{
	namespace algorithm
	{
		//======================================================================
		//!	base_vector
		//======================================================================
		template<typename Type, typename Allocator=allocator>
		class base_vector
		{
		private:
			typedef Type				reference;
			typedef	TypeInfo<reference>	TypeInfo;

			const u32 TypeSize = sizeof(Type);
			const u32 AllocateNum = Max<u32>(Align<u32>(TypeSize, 16) / 16, 4);

		public:
			//======================================================================
			//!	値
			//======================================================================
			class vec_value
			{
			public:
				//!	コンストラクタ
				vec_value(void){}
				//!	デストラクタ
				INLINE ~vec_value(void){}
				//!	キャスト
				INLINE operator reference&(void){ return _value; }
				//!	キャスト
				INLINE operator const reference&(void) const{ return _value; }
				//!	new
				INLINE void* operator new(size_t size, void* ptr){ (void)size; return ptr; }
				//!	delete
				INLINE void operator delete(void* ptr){ (void)ptr; }

			public:
				reference	_value;
			};
			//======================================================================
			//!	イテレータ
			//======================================================================
			class vec_iterator
			{
				friend class base_vector;
			public:
				//!	コンストラクタ
				vec_iterator(void) : _ptr(null) {}
			private:
				//!	コンストラクタ
				vec_iterator(vec_value* ptr) : _ptr(ptr) {}

			public:
				//!	キャスト
				INLINE operator reference*(void){ return &_ptr->_value; }
				//!	キャスト
				INLINE operator const reference*(void) const{ return &_ptr->_value; }
				//!	->
				INLINE reference* operator ->(void){ return &_ptr->_value; }
				//!	->
				INLINE const reference* operator ->(void) const{ return &_ptr->_value; }

				//!	インクリメント
				INLINE vec_iterator operator ++(void) { vec_iterator old = *this; _ptr ++; return old; }
				//!	デクリメント
				INLINE vec_iterator operator --(void) { vec_iterator old = *this; _ptr --; return old; }
				//!	インクリメント
				INLINE vec_iterator& operator ++(s32) { ++ _ptr; return *this; }
				//!	デクリメント
				INLINE vec_iterator& operator --(s32) { -- _ptr; return *this; }
				//!	等号
				INLINE b8 operator ==(const vec_iterator& vec) { return _ptr == vec._ptr; }
				//!	不等号
				INLINE b8 operator !=(const vec_iterator& vec) { return _ptr != vec._ptr; }

			private:
				vec_value*	_ptr;	//!< 値
			};
			typedef	vec_value		value_iterator;
			typedef	vec_iterator	iterator;

		public:
			//!	コンストラクタ
			INLINE base_vector(void)
				: _count(0), _max(0)
				, _begin(null), _end(null)
			{}
			//!	コンストラクタ
			//!	@param size [in] 最大格納数
			INLINE base_vector(u32 size)
				: _count(0), _max(0)
				, _begin(null), _end(null)
			{
				reserve(size);
			}
			//!	コンストラクタ
			//!	@param size  [in] 最大格納数
			//!	@param value [in] 要素の初期設定
			INLINE base_vector(u32 size, const reference& value)
				: _count(0), _max(0)
				, _begin(null), _end(null)
			{
				reserve(size);
				for( u32 i=0; i < size; i++ )
				{
					push_back(value);
				}
			}
			//!	コピーコンストラクタ
			//!	@param from [in] コピーするベクタ
			INLINE base_vector(const base_vector& from)
				: _count(0), _max(0)
				, _begin(null), _end(null)
			{
				reserve(from._count);
				_end._ptr = _begin._ptr + from._count;
				_count = from._count;

				if( _count > 0 )
				{
					Copy(0, &from[0], _count);
				}
			}
			//!	デストラクタ
			INLINE ~base_vector(void)
			{
				clear();
				Allocator::Free(_begin._ptr);
			}

		public:
			//!	要素にアクセス
			//!	@param pos [in] 要素番号
			//!	@return 要素
			INLINE reference& operator[] (u32 pos)
			{
				kgAssert(pos < _count, "access error");
				return _begin._ptr[pos];
			}
			//!	要素にアクセス
			//!	@param pos [in] 要素番号
			//!	@return 要素
			INLINE const reference& operator[] (u32 pos) const
			{
				kgAssert(pos < _count, "access error");
				return _begin._ptr[pos];
			}
			//!	要素にアクセス
			//!	@param pos [in] 要素番号
			//!	@return 要素
			INLINE reference& at(u32 pos)
			{
				kgAssert(pos < _count, "access error");
				return _begin._ptr[pos];
			}
			//!	要素にアクセス
			//!	@param pos [in] 要素番号
			//!	@return 要素
			INLINE const reference& at(u32 pos) const
			{
				kgAssert(pos < _count, "access error");
				return _begin._ptr[pos];
			}
			//!	データにアクセス
			//!	@return バッファの先頭ポインタ
			INLINE reference* data(void)
			{
				return (reference*)_begin._ptr;
			}
			//!	データにアクセス
			//!	@return バッファの先頭ポインタ
			INLINE const reference* data(void) const
			{
				return (reference*)_begin._ptr;
			}
			//!	代入
			//!	@param from [in] 代入するベクタ
			INLINE base_vector& operator = (const base_vector& from)
			{
				if( _begin._ptr == from._begin._ptr )	return *this;
				if( _begin._ptr )
				{
					clear();
				}
				if( from._count > 0 )
				{
					u32 size = from._count;
					if( size > capacity() ) reserve(size);

					_end._ptr = _begin._ptr + size;
					_count = size;
					Copy(0, &from[0], size);
				}
				return *this;
			}
			//!	加算後代入
			//!	@param from [in] 代入するベクタ
			INLINE base_vector& operator += (const base_vector& from)
			{
				if( from._count > 0 )
				{
					u32 size	= _count + from._count;
					if( size > capacity() ) reserve(size);

					_end._ptr += from._count;
					Copy(_count, &from[0], from._count);
					_count = size;
				}
				return *this;
			}
		public:
			//!	先頭要素のイテレータ取得
			//!	@return 先頭要素
			INLINE iterator begin(void) { return _begin; }
			//!	先頭要素のイテレータ取得
			//!	@return 先頭要素
			INLINE const iterator& begin(void) const { return _begin; }
			//!	末尾要素のイテレータ取得
			//!	@return 末尾要素
			INLINE iterator end(void) { return _end; }
			//!	末尾要素のイテレータ取得
			//!	@return 末尾要素
			INLINE const iterator& end(void) const { return _end; }
			
			//!	先頭要素の取得
			INLINE reference& front(void) { return _begin._ptr[0]; }
			//!	先頭要素の取得
			INLINE const reference& front(void) const { return _begin._ptr[0]; }
			//!	末尾要素の取得
			INLINE reference& back(void) { return _begin._ptr[_count - 1]; }
			//!	末尾要素の取得
			INLINE const reference& back(void) const { return _begin._ptr[_count - 1]; }

		public:
			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@return インデックス
			INLINE u32 find(reference value) const
			{
				return linear_search(value, &front(), size());
			}
			//!	要素の検索
			//!	@param value [in] 検索する値
			//!	@return インデックス
			INLINE b8 contains(reference value) const
			{
				return find(value) != NotFind;
			}

		public:
			//!	要素の有無を確認
			//!	@return 要素が無い場合true
			INLINE b8 empty(void) const { return _count == 0; }
			//!	要素数の取得
			//!	@return 要素数
			INLINE u32 size(void) const { return _count; }
			//!	最大格納要素数の取得
			//!	@return 最大格納要素数
			INLINE u32 capacity(void) const { return _max; }

		public:
			//!	最大管理要素数の変更
			//!	@param size [in] 最大管理数(現在の管理数より少ない場合は変更を行わない)
			INLINE void reserve(u32 size)
			{
				if( size <= _max )	return;
				u32 alloc_size = sizeof(value_iterator) * size;
				value_iterator* ptr = (value_iterator*)Allocator::Reallocate(_begin._ptr, alloc_size);

				_begin._ptr	= ptr;
				_end._ptr	= ptr + _count;
				_max		= size;
			}
			//!	最大管理要素数の変更
			//!	@param size [in] 最大管理数
			INLINE void resize(u32 size)
			{
				reserve(size);
				if( size > _count )
				{
					Constructor(_count, size - _count);
				}
				else if( size < _count )
				{
					erase(size, _count - size);
				}
				_end._ptr = _begin._ptr + size;
				_count = size;
			}
			//!	先頭に要素追加
			//!	@param value [in] 追加する値
			INLINE void push_back(const reference& value)
			{
				if( _count == _max )
				{
					reserve(_max + AllocateNum);
				}
				Copy(_count, &value, 1);
				++ _end._ptr;
				++ _count;
			}
			//!	末尾要素の削除
			INLINE void pop_back(void)
			{
				if( _count <= 0 )	return;
				Destructor(_count-1, 1);
				-- _end._ptr;
				-- _count;
			}
			//!	指定要素の直前に要素を挿入
			//!	@param it		[in] 要素(引数の状態は保障されません)
			//!	@param value	[in] 挿入する値
			//!	@return 追加した要素
			INLINE iterator insert(iterator& it, const reference& value)
			{
				u32 pos = (u32)(it._ptr - _begin._ptr);
				insert(pos, value);
				return _begin._ptr + pos;
			}
			//!	指定要素の直前に要素を挿入
			//!	@param it		[in] 要素(引数の状態は保障されません)
			//!	@param count	[in] 挿入する数
			//!	@param value	[in] 挿入する値
			//!	@return 追加した要素の先頭
			INLINE iterator insert(iterator& it, u32 count, const reference& value)
			{
				u32 pos = (u32)(it._ptr - _begin._ptr);
				insert(pos, count, value);
				return _begin._ptr + pos;
			}
			//!	指定要素の直前に要素を挿入
			//!	@param pos		[in] 要素番号
			//!	@param value	[in] 挿入する値
			INLINE void insert(u32 pos, const reference& value)
			{
				kgAssert(pos <= _count, "insert error");
				if( _count == _max )
				{
					reserve(_max + AllocateNum);
				}
				value_iterator* ptr = &_begin._ptr[pos];
				u32	size = _count - pos;
				if( size > 0 )
				{
					kgMemmove(ptr + 1, ptr, sizeof(reference) * size);
				}
				Copy(pos, &value, 1);

				++ _end._ptr;
				++ _count;
			}
			//!	指定要素の直前に要素を挿入
			//!	@param pos		[in] 要素番号
			//!	@param count	[in] 挿入する数
			//!	@param value	[in] 挿入する値
			//!	@return 追加した要素の先頭
			INLINE void insert(u32 pos, u32 count, const reference& value)
			{
				kgAssert(pos <= _count, "insert error");
				if( _count + count > _max )
				{
					reserve(_count + count + AllocateNum);
				}
				value_iterator* ptr = &_begin._ptr[pos];
				u32	size = _count - pos;
				if( size > 0 )
				{
					kgMemmove(ptr + count, ptr, sizeof(reference) * size);
				}
				for( u32 i=0; i < count; i++ )
				{
					Copy(pos + i, &value, 1);
				}
				++ _end._ptr;
				_count += count;
			}
			//!	要素の削除
			//!	@param it [in] 削除する要素(引数の状態は保障されません)
			//!	@return 次の要素
			iterator erase(iterator& it)
			{
				u32 pos = (u32)(it._ptr - _begin._ptr);
				erase(pos);
				return _begin._ptr + pos - 1;
			}
			//!	指定要素間の要素を削除
			//!	@param start [in] 削除する先頭の要素(引数の状態は保障されません)
			//!	@param end   [in] 削除する末尾の要素(引数の状態は保障されません)
			//!	@return 次の要素
			iterator erase(iterator& start, iterator& end)
			{
				u32 pos = (u32)(start._ptr - _begin._ptr);
				u32 count = (u32)(end._ptr - start._ptr);
				erase(pos, count);
				return _begin._ptr + pos - 1;
			}
			//!	要素の削除
			//!	@param pos [in] 削除する要素番号
			void erase(u32 pos)
			{
				kgAssert(pos < _count, "erase error");
				if( pos < _count )
				{
					value_iterator* ptr = &_begin._ptr[pos];
					Destructor(pos, 1);
					if( ptr != _end._ptr )
					{
						kgMemmove(ptr, ptr + 1, sizeof(value_iterator) * (_count - pos - 1));
					}
					-- _end._ptr;
					-- _count;
				}
			}
			//!	指定要素間の要素を削除
			//!	@param pos		[in] 削除する先頭の要素番号
			//!	@param count	[in] 削除する数
			void erase(u32 pos, u32 count)
			{
				kgAssert(pos+count <= _count, "erase error");
				if( _count <= pos )return;
				if( count == 0 )return;

				u32 size = _count-pos;
				count = Min(count, size);
				Destructor(pos, count);

				value_iterator* ptr = &_begin._ptr[pos];
				if( count < size )
				{
					kgMemmove(ptr, ptr+count, sizeof(value_iterator) * (size-count));
				}
				_end._ptr -= count;
				_count -= count;
			}
			//!	全要素の削除
			INLINE void clear(void)
			{
				Destructor(0, _count);
				_end._ptr = _begin._ptr;
				_count = 0;
			}
			//!	要素の入れ替え
			//!	@param vec [in/out] 要素の入れ替えを行うベクター
			INLINE void swap(base_vector& from)
			{
				if( this == &from )return;

				iterator begin, end;
				u32 count, max;
				begin		= from._begin;
				end			= from._end;
				count		= from._count;
				max			= from._max;

				from._begin	= _begin;
				from._end	= _end;
				from._count	= _count;
				from._max	= _max;

				_begin		= begin;
				_end		= end;
				_count		= count;
				_max		= max;
			}

		private:
			//!	コピー
			//!	@param pos		[in] 要素番号
			//!	@param value	[in] コピーする値
			//!	@param count	[in] コピーする数
			INLINE void Copy(u32 pos, const reference* value, u32 count)
			{
				value_iterator* ptr = &_begin._ptr[pos];
				if (TypeInfo::NeedsConstructor)
				{
					for (u32 i = 0; i < count; i++)
					{
						new(&ptr[i]._value) reference(value[i]);
					}
				}
				else
				{
					kgMemcpy(ptr, value, sizeof(reference) * count);
				}
			}
			//!	コンストラクタの呼び出し
			//!	@param pos		[in] 要素番号
			//!	@param count	[in] 呼び出す数
			INLINE void Constructor(u32 pos, u32 count)
			{
				value_iterator* ptr = &_begin._ptr[pos];
				if( TypeInfo::NeedsConstructor )
				{
					for( u32 i = 0; i < count; i++ )
					{
						new(&ptr[i]._value) reference;
					}
				}
				else
				{
					kgMemset(ptr, 0, sizeof(reference) * count);
				}
			}
			//!	デストラクタの呼び出し
			//!	@param pos		[in] 要素番号
			//!	@param count	[in] 呼び出す数
			INLINE void Destructor(u32 pos, u32 count)
			{
				if (TypeInfo::NeedsDestructor)
				{
					value_iterator* ptr = &_begin._ptr[pos];
					for( u32 i=0; i < count; i++ )
					{
						(*(ptr + i)).~value_iterator();
					}
				}
			}

		protected:
			iterator	_begin;		//!< 先頭要素
			iterator	_end;		//!< 末尾要素
			u32			_count;		//!< 管理数
			u32			_max;		//!< 最大格納数
		};

		//!	vector
		template <typename Type>
		class vector
			: public base_vector<Type, allocator>
		{};
	}
}

#endif	// __KGL_ALGLITHM_VECTOR_H__
//======================================================================
//	END OF FILE
//======================================================================
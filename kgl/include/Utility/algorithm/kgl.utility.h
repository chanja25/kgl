//----------------------------------------------------------------------
//!
//!	@file	kgl.utility.h
//!	@brief	utility
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ALGLITHM_UTILITY_H__
#define	__KGL_ALGLITHM_UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace algorithm
	{
		//!	�s��v�̏ꍇ�̖߂�l
		const u32 NotFind = 0xffffffff;

		namespace utility
		{
			// �N�C�b�N�\�[�g����}���\�[�g�ɐ؂�ւ���v�f��
			const u32 Threashhold = 32;

			//!	�R�s�[
			template<typename T>
			FORCEINLINE void copy(T& param1, const T& param2)
			{
				kgMemcpy(&param1, &param2, sizeof(T));
			}
			//!	�X���b�v
			template<typename T>
			FORCEINLINE void swap(T& param1, T& param2)
			{
				T temp = param1;
				param1 = param2;
				param2 = temp;
			}
			//!	���S�̑I��
			template<typename T>
			FORCEINLINE const T& median(T& param1, T& param2, T& param3, b8 (*compare)(const T&, const T&))
			{
			  if( compare(param2, param1) )	swap(param1, param2);
			  if( compare(param3, param1) )	swap(param1, param3);
			  if( compare(param3, param2) )	swap(param2, param3);
			  return param2;
			}
			//!	��r(==)
			template<typename T1, typename T2>
			FORCEINLINE b8 equal(const T1& p1, const T2& p2)
			{
				return p1 == p2;
			}
			//!	��r(==)
			template<typename T1, typename T2>
			FORCEINLINE b8 notequal(const T1& p1, const T2& p2)
			{
				return p1 != p2;
			}
			//!	��r
			template<typename T1, typename T2>
			FORCEINLINE int compare(const T1& p1, const T2& p2)
			{
				if( p1 < p2 )return -1;
				if( p1 > p2 )return 1;

				return 0;
			}
			//!	��r(<)
			template<typename T1, typename T2>
			FORCEINLINE b8 less(const T1& p1, const T2& p2)
			{
				return p1 < p2;
			}
			//!	��r(<=)
			template<typename T1, typename T2>
			FORCEINLINE b8 lessequal(const T1& p1, const T2& p2)
			{
				return p1 <= p2;
			}
			//!	��r(>)
			template<typename T1, typename T2>
			FORCEINLINE b8 greater(const T1& p1, const T2& p2)
			{
				return p1 > p2;
			}
			//!	��r(>=)
			template<typename T1, typename T2>
			FORCEINLINE b8 greaterequal(const T1& p1, const T2& p2)
			{
				return p1 >= p2;
			}
		}
	}
}

#include "kgl.utility.sort.h"

#endif	// __KGL_ALGLITHM_UTILITY_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Window.h
//!	@brief	ウィンドウ関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_WINDOW_H__
#define	__KGL_WINDOW_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.WindowEvent.h"

namespace kgl
{
	namespace Window
	{
		namespace EStyle
		{
			const u32 Overlapped		= (1 <<  0);	//!< タイトルバー，境界線つきオーバーラップウインドウ
			const u32 OverlappedWindow	= (1 <<  1);	//!< Overlapped | Caption | SystemMenu | ThickFrame | MinimizeBox | MaximizeBox
			const u32 Child				= (1 <<  2);	//!< 子ウインドウ。Popupとは併用できない
			const u32 Popup				= (1 <<  3);	//!< ポップアップウインドウ。Childとは併用できない
			const u32 PopupWindow		= (1 <<  4);	//!< Border | Popup | SystemMenu
			const u32 ClipChildren		= (1 <<  5);	//!< 親の描画で子ウインドウの部分をクリッピング
			const u32 ClipSiblings		= (1 <<  6);	//!< 子の描画で兄弟ウインドウの部分をクリッピング
			const u32 Caption			= (1 <<  7);	//!< タイトルバー(WS_DLGFRAMEも一緒についてくる)
			const u32 SystemMenu		= (1 <<  8);	//!< タイトルバーにシステムメニュー(閉じるボタンとアイコン)つける
			const u32 MinimizeBox		= (1 <<  9);	//!< システムメニューに最小化ボタン加える
			const u32 MaximizeBox		= (1 << 10);	//!< システムメニューに最大化ボタン加える
			const u32 Border			= (1 << 11);	//!< 細い境界線
			const u32 DlgFrame			= (1 << 12);	//!< サイズ変更できない境界線
			const u32 ThickFrame		= (1 << 13);	//!< サイズ変更できる境界線
			const u32 HScroll			= (1 << 14);	//!< horizon scroll，つまり水平方向スクロール
			const u32 VScroll			= (1 << 15);	//!< vertical scroll，つまり垂直方向スクロール
			const u32 Visible			= (1 << 16);	//!< 初期状態で表示される
			const u32 Disable			= (1 << 17);	//!< 初期状態が使用不可
			const u32 Maximize			= (1 << 18);	//!< 初期状態で最大化
			const u32 Minimize			= (1 << 19);	//!< 初期状態で最小化
			const u32 TabStop			= (1 << 20);	//!< ダイアログのコントロールにつかう．Tabでフォーカスを当てられるようにする

			const u32 AbsPosition		= (1 << 21);	//!< ウインドウが絶対位置を持つ
			const u32 AcceptFiles		= (1 << 22);	//!< ドラッグアンドドロップでファイルを受け付ける
			const u32 TopMost			= (1 << 23);	//!< どのウインドウよりも手前に表示
			const u32 ToolWindow		= (1 << 24);	//!< ツールウインドウ(小さいタイトルバー)
			const u32 Transparent		= (1 << 25);	//!< 透明ウインドウWS_EX_ABSPOTION
			const u32 Layred			= (1 << 26);	//!< レイヤーウィンドウ

			const u32 DefaultWindow		= Overlapped | Caption | SystemMenu | DlgFrame | MinimizeBox;
			const u32 FullScreen		= Visible | Popup;// | Maximize;
		}

		//======================================================================
		//!	ウィンドウインターフェイス
		//======================================================================
		interface IWindow
			: public IKGLBase
		{
		public:
			//!	ウィンドウ生成
			//!	@param pName		[in] ウィンドウ名
			//!	@param bFullScreen	[in] フルスクリーンにするか
			//!	@return 結果
			virtual b8 Create(const c16* pName, b8 bShow = true, b8 bFullScreen = false) = 0;
			//!	ウィンドウ破棄
			virtual void Close(void) = 0;

			//!	位置設定
			//!	@param iX	[in] X座標
			//!	@param iY	[in] Y座標
			virtual void SetPosition(s32 iX, s32 iY) = 0;
			//!	位置取得
			//!	@param iX	[out] X座標
			//!	@param iY	[out] Y座標
			virtual void GetPosition(s32& iX, s32& iY) = 0;
			//!	サイズ設定
			//!	@param iWidth	[in] 横幅
			//!	@param iHeight	[in] 縦幅
			virtual void SetSize(s32 iWidth, s32 iHeight) = 0;
			//!	サイズ取得
			//!	@param iWidth	[out] 横幅
			//!	@param iHeight	[out] 縦幅
			virtual void GetSize(s32& iWidth, s32& iHeight) = 0;
			//!	サイズ取得
			//!	@return サイズ
			virtual Vector2 GetSize(void) = 0;
			//!	画面サイズ取得
			//!	@param iWidth	[out] 横幅
			//!	@param iHeight	[out] 縦幅
			virtual void GetDisplaySize(s32& iWidth, s32& iHeight) = 0;
			//!	ウィンドウ名設定
			//!	@param ウィンドウ名
			virtual void SetName(const c16* pName) = 0;
			//!	IMEを有効にするか？
			//!	@param bEnable [in] 有効フラグ
			virtual void SetIME(b8 bEnable) = 0;
			//!	Espaceキーによる終了を有効にするか？
			//!	@param bEnable [in] 有効フラグ
			virtual void SetEscapeExit(b8 bEnable) = 0;

		public:
			//!	ウィンドウスタイルの設定
			//!	@param uiStyle [in] ウィンドウスタイル(EStyle参照)
			virtual void SetStyle(u32 uiStyle) = 0;
			//!	ウィンドウスタイルの取得
			//!	@return ウィンドウスタイル(EStyle参照)
			virtual u32 GetStyle(void) = 0;
			//!	アイコンの設定
			//!	@param pHandle [in] アイコンハンドル
			virtual void SetIcon(void* pHandle) = 0;
			//!	メニューの設定
			//!	@param pHandle [in] メニューハンドル
			virtual void SetMenu(void* pHandle) = 0;
			//!	アイコンハンドルの取得
			//!	@return アイコンハンドル
			virtual void* GetIconHandle(void) = 0;
			//!	メニューハンドルの取得
			//!	@return メニューハンドル
			virtual void* GetMenuHandle(void) = 0;
			//!	ウィンドウハンドルの取得
			//!	@return ウィンドウハンドル
			virtual void* GetHandle(void) = 0;

		public:
			//!	カーソルを消すか？
			//!	@param bShow [in] 表示の有無
			//!	@param bMove [in] 表示を消した際に移動を許可するか？
			virtual void ShowCursor(b8 bShow, b8 bMove = true) = 0;

		public:
			//!	ウィンドウにフォーカスを当てる
			virtual void Forcus(void) = 0;
			//!	ウィンドウの表示制御
			//!	@param bShow [in] 表示フラグ
			virtual void Show(b8 bShow) = 0;
			//!	ウィンドウモード変更(フルスクリーン⇔ウインドウ)
			virtual void ChangeWindowMode(void) = 0;
			//!	フルスクリーンフラグの取得
			//!	@return フルスクリーンフラグ
			virtual b8 IsFullScreen(void) = 0;
			//!	フォーカスフラグの取得
			//!	@return フォーカスフラグ
			virtual b8 IsForcus(void) = 0;
			//!	ウィンドウが生成さているか？
			//!	@return ウィンドウの有無
			virtual b8 IsActive(void) = 0;
			//!	終了コードの取得
			//!	@return 終了コード
			virtual u32 ExitCode(void) = 0;

		public:
			//!	オーナーウィンドウの設定(自由に移動できるがオーナーの裏に回る事が不可)
			//!	@param pOwner	[in] オーナーに設定するウィンドウ
			virtual void SetOwner(IWindow* pOwner) = 0;
			//!	親ウィンドウの設定(子は親のウィンドウ外へ移動不可)
			//!	@param pParent	[in] 親に設定するウィンドウ
			virtual void SetParent(IWindow* pParent) = 0;
			//!	ウィンドウイベントの設定
			//!	@param pEvent	[in] イベント
			virtual void SetWindowEvent(IWindowEvent* pEvent) = 0;
			//!	ウィンドウイベントのクリア
			//!	@param pEvent	[in] クリアするイベント
			virtual void ClearWindowEvent(IWindowEvent* pEvent = null) = 0;
		};

		//!	ウィンドウの生成
		//!	@param ppWindow	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IWindow** ppWindow);
	}
}

#endif	// __KGL_WINDOW_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.System.h
//!	@brief	システム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_SYSTEM_H__
#define	__KGL_SYSTEM_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.Window.h"
#include "../Time/kgl.Frame.h"
#include "../File/kgl.FileReader.h"
#include "../../../Library/kgGraphics/include/kgGraphics.h"
#include "../../../Library/kgSound/include/kgSound.h"
#include "../Render/kgl.Render.h"
#include "Manager/kgl.GameManager.h"

namespace kgl
{
	namespace System
	{
		//======================================================================
		//!	システムインターフェイス(アプリケーション側で継承して使用)
		//======================================================================
		interface ISystem
		{
		public:
			//!	コンストラクタ
			ISystem(void);
			//!	初期化
			//!	@param pWindow		[in] システムに設定するウィンドウ
			//!	@param pPackagePath	[in] パッケージデータのパス
			//!	@param pRootDir		[in] ルートディレクトリ
			//!	@return 結果
			virtual b8 InitializeKGL(Window::IWindow* pWindow, const c8* pPackagePath = "./Data.kga", const c8* pRootDir = null);
			//!	破棄
			virtual void FinalizeKGL(void);
			//!	実行
			virtual void Run(void);

			//!	終了コードの取得
			//!	@return 終了コード
			virtual s32 GetExitCode(void);

		protected:
			//!	初期化
			//!	@return 結果
			virtual b8 Initialize(void) = 0;
			//!	破棄
			virtual void Finalize(void) = 0;

			//!	更新
			//!	@param fElapsedTime		[in] 経過時間
			virtual void Update(f32 fElapsedTime) = 0;
			//!	描画
			//!	@param pRenderingSystem	[in] 描画システム
			virtual void Render(Render::IRenderingSystem* pRenderingSystem) = 0;
#if	KGL_DEBUG
			//!	デバッグ描画
			//!	@param pRenderingSystem	[in] 描画システム
			virtual void DebugRender(Render::IRenderingSystem* pRenderingSystem);
#endif	// ~#if	KGL_DEBUG

		public:
			//!	フレームコントロールの取得
			//!	@return フレームコントロール
			FORCEINLINE Frame::IFrameControl* FrameControl(void){ return m_pFrameControl; }
			//!	描画システムの取得
			//!	@return 描画システム
			FORCEINLINE Render::IRenderingSystem* RenderingSystem(void){ return m_pRenderingSystem; }

		protected:
			TKGLPtr<Window::IWindow>			m_pWindow;			//!< ウィンドウ
			TKGLPtr<Frame::IFrameControl>		m_pFrameControl;	//!< フレーム制御(アプリケーション側で設定の変更が可能)
			TKGLPtr<Render::IRenderingSystem>	m_pRenderingSystem;	//!< 描画システム
			TKGLPtr<Render::ITexture>			m_pFrameBuffer;		//!< フレームバッファ
			TKGLPtr<Render::ITexture>			m_pDepthBuffer;		//!< フレームデプスバッファ
			s32									m_iState;			//!< 状態
			s32									m_iExitCode;		//!< 終了コード
			BITFIELD							m_bComInit;			//!< COMコンポーネントの初期化
		};
	}
}

#endif	// __KGL_SYSTEM_H__
//======================================================================
//	END OF FILE
//======================================================================
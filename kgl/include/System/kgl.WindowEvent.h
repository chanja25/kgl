//----------------------------------------------------------------------
//!
//!	@file	kgl.WindowEvent.h
//!	@brief	ウィンドウイベント関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_WINDOW_EVENT_H__
#define	__KGL_WINDOW_EVENT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Window
	{
		namespace EEvent
		{
			const u32 Create	= 0;
			const u32 Quit		= 1;
			const u32 EventMax	= 32;
		}

		//======================================================================
		//!	ウィンドウイベントインターフェイス
		//======================================================================
		interface IWindowEvent
			: public IKGLBase
		{
		public:
			//!	データの登録
			virtual void SetData(void* pData) = 0;
			//!	データの取得
			//!	@return データ
			virtual void* GetData(void) = 0;

			//!	イベント時のコールバック関数の設定
			//!	@param iEvent	[in] イベント
			//!	@param Func		[in] コールバック関数
			virtual void SetEventCallBack(s32 iEvent, FWindowEvent Func) = 0;
			//!	コールバック関数の取得
			//!	@param iEvent	[in] イベント
			//!	@return	コールバック関数
			virtual FWindowEvent GetEventCallBack(s32 iEvent) = 0;
			//!	イベント時のコールバック関数の設定
			//!	@param Func		[in] コールバック関数
			virtual void SetCommonEventCallBack(FWindowCommonEvent Func) = 0;
			//!	コールバック関数の取得
			//!	@return	コールバック関数
			virtual FWindowCommonEvent GetCommonEventCallBack(void) = 0;
		};

		//!	ウィンドウイベントの生成
		//!	@param ppWindowEvent [out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IWindowEvent** ppWindowEvent);
	}
}

#endif	// __KGL_WINDOW_EVENT_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Random.h
//!	@brief	乱数関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_RANDOM_H__
#define	__KGL_RANDOM_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace Random
	{
		//======================================================================
		//!	ランダムベースインターフェイス
		//======================================================================
		interface IRandomBase
		{
		public:
			//!	初期シードの設定
			//!	@param iSeed [in] シード
			virtual void Seed(s32 iSeed) = 0;
			//!	乱数取得
			//!	@retrun 乱数
			virtual s32 Rand(void) = 0;
			//!	乱数取得
			//!	@param iMin [in] 最小値
			//!	@param iMax [in] 最大値
			//!	@retrun 乱数
			virtual s32 RandRange(s32 iMin, s32 iMax) = 0;
			//!	乱数取得
			//!	@param dMin [in] 最小値
			//!	@param dMax [in] 最大値
			//!	@retrun 乱数
			virtual f32 RandRange(f32 fMin, f32 fMax) = 0;
			//!	乱数取得
			//!	@param dMin [in] 最小値
			//!	@param dMax [in] 最大値
			//!	@retrun 乱数
			virtual f64 RandRange(f64 dMin, f64 dMax) = 0;
		};

		//======================================================================
		//!	ランダム
		//======================================================================
		class CRandom
			: public IRandomBase
		{
		public:
			//!	コンストラクタ
			CRandom(void);

			//!	初期シードの設定
			//!	@param iSeed [in] シード
			void Seed(s32 iSeed);
			//!	乱数取得
			//!	@retrun 乱数
			s32 Rand(void);
			//!	乱数取得
			//!	@param iMin [in] 最小値
			//!	@param iMax [in] 最大値
			//!	@retrun 乱数
			s32 RandRange(s32 iMin, s32 iMax);
			//!	乱数取得
			//!	@param dMin [in] 最小値
			//!	@param dMax [in] 最大値
			//!	@retrun 乱数
			f32 RandRange(f32 fMin, f32 fMax);
			//!	乱数取得
			//!	@param dMin [in] 最小値
			//!	@param dMax [in] 最大値
			//!	@retrun 乱数
			f64 RandRange(f64 dMin, f64 dMax);

		private:
			s32	seed;
		};
		//======================================================================
		//!	ランダム(XorShift)
		//======================================================================
		class CRandomXORShift
			: public IRandomBase
		{
		public:
			//!	コンストラクタ
			CRandomXORShift(void);

			//!	初期シードの設定
			//!	@param iSeed [in] シード
			void Seed(s32 iSeed);
			//!	乱数取得
			//!	@retrun 乱数
			s32 Rand(void);
			//!	乱数取得
			//!	@param iMin [in] 最小値
			//!	@param iMax [in] 最大値
			//!	@retrun 乱数
			s32 RandRange(s32 iMin, s32 iMax);
			//!	乱数取得
			//!	@param dMin [in] 最小値
			//!	@param dMax [in] 最大値
			//!	@retrun 乱数
			f32 RandRange(f32 fMin, f32 fMax);
			//!	乱数取得
			//!	@param dMin [in] 最小値
			//!	@param dMax [in] 最大値
			//!	@retrun 乱数
			f64 RandRange(f64 dMin, f64 dMax);

		private:
			s32	x, y, z, w;
		};
	}
}

#include "kgl.Random.inline.h"

#endif	// __KGL_RANDOM_H__
//=======================================================================
//	END OF FILE
//=======================================================================
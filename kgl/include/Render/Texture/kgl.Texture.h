//----------------------------------------------------------------------
//!
//!	@file	kgl.Texture.h
//!	@brief	テクスチャー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_TEXTURE_H__
#define	__KGL_TEXTURE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "../../../Library/kgGraphics/include/kgGraphics.h"
#include "File/kgl.FileReader.h"

namespace kgl
{
	namespace Render
	{
		//!	フィルター情報
		struct FilterInfo
		{
			s32	iFilterMin;	//!< Minフィルター
			s32	iFilterMag;	//!< Magフィルター
			s32	iFilterMip;	//!< Mipフィルター
			s32	iWrapU;		//!< U方向のラップモード
			s32	iWrapV;		//!< V方向のラップモード
		};

		//======================================================================
		//!	テクスチャーインターフェイス
		//======================================================================
		interface ITexture
			: public File::IExtraObject
		{
		public:
			//!	フィルター情報の設定
			//!	@param Info [in] フィルター情報
			virtual void SetFilterInfo(const FilterInfo& Info) = 0;
			//!	フィルターの設定
			//!	@param iMin [in] Minフィルター
			//!	@param iMag [in] Magフィルター
			//!	@param iMip [in] Mipフィルター
			virtual void SetFilter(s32 iMin, s32 iMag, s32 iMip) = 0;
			//!	ラップモードの設定
			//!	@param iU [in] U方向のラップモード
			//!	@param iV [in] V方向のラップモード
			virtual void SetWrapMode(s32 iU, s32 iV) = 0;

		public:
			//!	リソースの取得
			//!	@return リソース情報
			virtual kgGraphics::ITextureBase* GetResource(void) = 0;
			//!	フィルター情報の取得
			//!	@return フィルター情報
			virtual const FilterInfo& GetFilterInfo(void) = 0;
			//!	テクスチャー情報の取得
			//!	@return テクスチャー情報
			virtual const kgGraphics::TextureInfo& GetInfo(void) = 0;

			//!	テクスチャー情報の取得
			//!	@return テクスチャー情報
			Vector2 GetSize(void) { return GetResource()->GetSize(); }
		};

		//!	テクスチャーの生成
		//!	@param ppTexture [out] 実体格納用ポインタ
		//!	@param pData [in] データ
		//!	@return 結果
		b8 Create(ITexture** ppTexture, const void* pData);
		//!	テクスチャーの生成
		//!	@param ppTexture [out] 実体格納用ポインタ
		//!	@param pResource [in] リソース(テクスチャー)
		//!	@return 結果
		b8 Create(ITexture** ppTexture, kgGraphics::ITextureBase* pResource);
	}
}

#endif	// __KGL_TEXTURE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
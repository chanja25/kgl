//----------------------------------------------------------------------
//!
//!	@file	kgl.Font.h
//!	@brief	文字関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_FONT_H__
#define	__KGL_FONT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "File/kgl.FileReader.h"

namespace kgl
{
	namespace Render
	{
		interface IRenderingSystem;
	}

	namespace Font
	{
		struct FontInfo
		{
			FontInfo(void) { kgZeroMemory(this, sizeof(*this)); }

			const void*	pTTF;			//!< TTFデータ(不要な場合はnull)
			u32			uiTTFSIze;		//!< TTFデータサイズ
			c8			cFont[64];		//!< フォント名
			s32			iSize;			//!< フォントサイズ
			s32			iCharPaddingX;	//!< X軸の余白
			s32			iCharPaddingY;	//!< Y軸の余白
			BITFIELD	bBold: 1;		//!< 太字にするか？
			BITFIELD	bItalic: 1;		//!< 斜体にするか？
			BITFIELD	bOutline: 1;	//!< 外枠を付けるか？
		};

		namespace EAlign
		{
			const s32 LeftTop		= 0;
			const s32 LeftBottom	= 1;
			const s32 CenterTop		= 2;
			const s32 Center		= 3;
			const s32 CenterBottom	= 4;
			const s32 RightTop		= 5;
			const s32 RightBottom	= 6;
		}

		//======================================================================
		//!	文字描画管理インターフェイス
		//======================================================================
		interface IFont
			: public File::IExtraObject
		{
		public:
			//!	余白の設定
			//!	@param iX [in] X軸の余白
			//!	@param iY [in] X軸の余白
			virtual void SetPadding(s32 iX, s32 iY) = 0;
			//!	余白の取得
			//!	@param iX [out] X軸の余白
			//!	@param iY [out] X軸の余白
			virtual void GetPadding(s32& iX, s32& iY) = 0;
			//!	余白の取得
			//!	@return 余白
			virtual Vector2 GetPadding(const Vector2& vScale = OneVector2) = 0;

			//!	文字列描画
			//!	@param pRenderingSystem	[in] 描画システム
			//!	@param pText			[in] 文字列
			//!	@param vPos				[in] 位置
			//!	@param vScale			[in] スケール
			//!	@param DrawColor		[in] カラー
			//!	@param DrawAlign		[in] 描画の基準位置
			virtual void Draw(Render::IRenderingSystem* pRenderingSystem, const c8* pText, const Vector2& vPos, const Vector2& vScale = OneVector2, const FColor& DrawColor = FColorWhite, s32 DrawAlign = EAlign::LeftTop) = 0;
			//!	文字列を描画した際の描画サイズ取得
			//!	@param pText		[in]  文字列
			//!	@param vScale		[in]  スケール
			//!	@param pNextDrawPos	[out] 次の文字描画位置
			//!	@return 描画サイズ
			virtual Vector2 GetDrawTextSize(const c8* pText, const Vector2& vScale = OneVector2, Vector2* pNextDrawPos = null) = 0;
			//!	文字列描画
			//!	@param pRenderingSystem	[in] 描画システム
			//!	@param pText			[in] 文字列
			//!	@param vPos				[in] 位置
			//!	@param vScale			[in] スケール
			//!	@param DrawColor		[in] カラー
			//!	@param DrawAlign		[in] 描画の基準位置
			virtual void Draw(Render::IRenderingSystem* pRenderingSystem, const c16* pText, const Vector2& vPos, const Vector2& vScale = OneVector2, const FColor& DrawColor = FColorWhite, s32 DrawAlign = EAlign::LeftTop) = 0;
			//!	文字列を描画した際の描画サイズ取得
			//!	@param pText		[in] 文字列
			//!	@param vScale		[in] スケール
			//!	@param pNextDrawPos	[out] 次の文字描画位置
			//!	@return 描画サイズ
			virtual Vector2 GetDrawTextSize(const c16* pText, const Vector2& vScale = OneVector2, Vector2* pNextDrawPos = null) = 0;
		};

		//!	フォントの生成
		//!	@param ppFont	[out] 実体格納用ポインタ
		//!	@param pData	[in] フォントデータ
		//!	@return 結果
		b8 Create(IFont** ppFont, const void* pData);
		//!	フォントの生成
		//!	@param ppFont	[out] 実体格納用ポインタ
		//!	@param Info		[in] フォント情報
		//!	@return 結果
		b8 Create(IFont** ppFont, const FontInfo& Info);
	}
}

#endif	// __KGL_FONT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
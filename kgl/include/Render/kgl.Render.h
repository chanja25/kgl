//----------------------------------------------------------------------
//!
//!	@file	kgl.Render.h
//!	@brief	描画システム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_RENDER_H__
#define	__KGL_RENDER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "../../Library/kgGraphics/include/kgGraphics.h"
#include "System/kgl.Window.h"
#include "kgl.Drawer.h"
#include "Font/kgl.Font.h"

namespace kgl
{
	namespace Render
	{
		//======================================================================
		//!	描画システムインターフェイス
		//======================================================================
		interface IRenderingSystem
			: public IKGLBase
		{
		public:
			//!	グラフィックドライバーの取得
			//!	@return グラフィックドライバー
			FORCEINLINE IGraphicDriver* GraphicDriver(void) { return m_pDriver; }
			//!	コマンドクリエイターの取得
			//!	@return グラフィックドライバー
			FORCEINLINE ICommandCreator* CommandCreator(void) { return m_pCommand; }
			//!	描画管理の取得
			//!	@return 描画管理
			FORCEINLINE IDrawer* Drawer(void) { return m_pDrawer; }
#if	KGL_DEBUG_FONT
			//!	デバッグフォントの取得
			//!	@return デバッグフォント
			FORCEINLINE Font::IFont* DebugFont(void) { return m_pDebugFont; };
#endif	// ~#if	KGL_DEBUG_FONT

		protected:
			TKGLPtr<IGraphicDriver>		m_pDriver;		//!< グラフィックドライバー
			TKGLPtr<ICommandCreator>	m_pCommand;		//!< グラフィックコマンド制御
			TKGLPtr<IDrawer>			m_pDrawer;		//!< 描画管理
#if	KGL_DEBUG_FONT
			TKGLPtr<Font::IFont>		m_pDebugFont;	//!< デバッグフォント
#endif	// ~#if	KGL_DEBUG_FONT
		};

		//!	描画システムの生成
		//!	@param ppSystem	[out] 実体格納用ポインタ
		//!	@param pWindow	[in] ウィンドウ
		//!	@return 結果
		b8 Create(IRenderingSystem** ppSystem, IGraphicDriver* pDriver);
	}
}

#endif	// __KGL_RENDER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
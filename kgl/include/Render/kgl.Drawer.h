//----------------------------------------------------------------------
//!
//!	@file	kgl.Drawer.h
//!	@brief	描画管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DRAWER_H__
#define	__KGL_DRAWER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "../../Library/kgGraphics/include/kgGraphics.h"
#include "Texture/kgl.Texture.h"
#include "Font/kgl.Font.h"

namespace kgl
{
	using namespace kgGraphics;

	namespace Render
	{
		//!	描画モード
		namespace EDrawMode
		{
			const s32 Clear		= 0;	//!	書き込み
			const s32 Add		= 1;	//!	加算
			const s32 Sub		= 2;	//!	減算
			const s32 Alpha		= 3;	//!	透過

			const s32 Font		= 100;	//!	透過(フォント)
			const s32 FontAdd	= 101;	//!	加算(フォント)
		};

		//!	頂点フォーマット
		struct SimpleVertex
		{
			Vector4	Position;	//!< 座標
			FColor	Color;		//!< カラー
			Vector2	UV;			//!< UV
		};

		//======================================================================
		//!	描画管理インターフェイス
		//======================================================================
		interface IDrawer
			: public IKGLBase
		{
		public:
			//!	四角形の頂点を設定
			//!	@param pVertex		[out] 頂点バッファ
			//!	@param vPos			[in] 座標
			//!	@param vSize		[in] サイズ
			//!	@param vRectUV		[in] UV値
			//!	@param DrawColor	[in] カラー
			virtual void SetupRectVertex(SimpleVertex* pVertex, const Vector2& vPos, const Vector2& vSize, const Vector4& vRectUV, const FColor& DrawColor) = 0;
			//!	四角形の頂点を設定(回転付き)
			//!	@param pVertex		[out] 頂点バッファ
			//!	@param vPos			[in] 座標
			//!	@param vSize		[in] サイズ
			//!	@param vRectUV		[in] UV値
			//!	@param fAngle		[in] 角度
			//!	@param vOrigin		[in] 原点
			//!	@param DrawColor	[in] カラー
			virtual void SetupRotatedRectVertex(SimpleVertex* pVertex, const Vector2& vPos, const Vector2& vSize, const Vector4& vRectUV, f32 fAngle, const Vector2 vOrigin, const FColor& DrawColor) = 0;

			//!	シザリング設定
			//!	@param iX		[in] X座標
			//!	@param iY		[in] Y座標
			//!	@param iWidth	[in] 横幅
			//!	@param iHeight	[in] 高さ
			virtual void ScissorRect(s32 iX, s32 iY, s32 iWidth, s32 iHeight) = 0;

		public:
			//!	ラインの描画
			//!	@param vStart		[in] 位置
			//!	@param vEnd			[in] サイズ
			//!	@param DrawColor	[in] カラー
			//!	@param iDrawMode	[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawLine(const Vector2& vStart, const Vector2& vEnd, const FColor& DrawColor = FColorWhite, s32 iDrawMode = EDrawMode::Alpha) = 0;

			//!	レクトの描画
			//!	@param vPos			[in] 位置
			//!	@param vSize		[in] サイズ
			//!	@param DrawColor	[in] カラー
			//!	@param iDrawMode	[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawRect(const Vector2& vPos, const Vector2& vSize, const FColor& DrawColor = FColorWhite, s32 iDrawMode = EDrawMode::Alpha) = 0;
			//!	レクトの描画(回転有り)
			//!	@param vPos			[in] 位置
			//!	@param vSize		[in] サイズ
			//!	@param fAngle		[in] 角度
			//!	@param vOrigin		[in] 原点
			//!	@param DrawColor	[in] カラー
			//!	@param iDrawMode	[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawRotatedRect(const Vector2& vPos, const Vector2& vSize, f32 fAngle, const Vector2& vOrigin = ZeroVector2, const FColor& DrawColor = FColorWhite, s32 iDrawMode = EDrawMode::Alpha) = 0;

			//!	テクスチャー描画
			//!	@param pTexture		[in] テクスチャー
			//!	@param vPos			[in] 位置
			//!	@param vSize		[in] サイズ
			//!	@param vTexPos		[in] テクスチャーの切り取り位置
			//!	@param vTexSize		[in] テクスチャーの切り取りサイズ
			//!	@param DrawColor	[in] カラー
			//!	@param iDrawMode	[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawTexture(ITexture* pTexture, const Vector2& vPos, const Vector2& vSize, const Vector2& vTexPos, const Vector2& vTexSize, const FColor& DrawColor = FColorWhite, s32 iDrawMode = EDrawMode::Alpha) = 0;
			//!	テクスチャー描画(回転有り)
			//!	@param pTexture		[in] テクスチャー
			//!	@param vPos			[in] 位置
			//!	@param vSize		[in] サイズ
			//!	@param vTexPos		[in] テクスチャーの切り取り位置
			//!	@param vTexSize		[in] テクスチャーの切り取りサイズ
			//!	@param fAngle		[in] 角度
			//!	@param vOrigin		[in] 原点
			//!	@param DrawColor	[in] カラー
			//!	@param iDrawMode	[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawRotatedTexture(ITexture* pTexture, const Vector2& vPos, const Vector2& vSize, const Vector2& vTexPos, const Vector2& vTexSize, f32 fAngle, const Vector2& vOrigin = ZeroVector2, const FColor& DrawColor = FColorWhite, s32 iDrawMode = EDrawMode::Alpha) = 0;

		public:
			//!	プリミティブ描画(TriangleStrip)
			//!	@param pVertex		[in] 頂点バッファ
			//!	@param uiCount		[in] 描画数
			//!	@param pTexture		[in] テクスチャー
			//!	@param iDrawMode	[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawStrip(const SimpleVertex* pVertex, u32 uiCount, ITexture* pTexture = null, s32 iDrawMode = EDrawMode::Alpha) = 0;
			//!	プリミティブ描画(TriangleList)
			//!	@param pIndex			[in] インデックスバッファ
			//!	@param uiCount			[in] 描画数
			//!	@param pVertex			[in] 頂点バッファ
			//!	@param uiVertexCount	[in] 頂点数
			//!	@param pTexture			[in] テクスチャー
			//!	@param iDrawMode		[in] 描画モード(上記のEDrawModeを参照)
			virtual void DrawList(const u16* pIndex, u32 uiCount, const SimpleVertex* pVertex, u32 uiVertexCount, ITexture* pTexture = null, s32 iDrawMode = EDrawMode::Alpha) = 0;

		public:
#if WINDOWS
			//!	指定したウィンドウにテクスチャー描画(R8G8B8, A8R8G8B8のみ対応):
			//!	@param pTarget	[in/out] ターゲットウィンドウ
			//!	@param pTexture	[in] テクスチャー
			//!	@param 結果
			virtual b8 DrawToWindow(Window::IWindow* pTarget, ITexture* pTexture) = 0;
#endif // ~#if WINDOWS
		};

		//	描画制御の生成
		b8 Create(IDrawer** ppDrawer, IGraphicDriver* pDriver);
	}
}

#endif	// __KGL_DRAWER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
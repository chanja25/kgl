//----------------------------------------------------------------------
//!
//!	@file	kgl.Image.h
//!	@brief	画像関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_IMAGE_H__
#define	__KGL_IMAGE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"

namespace kgl
{
	namespace Image
	{
		namespace EColorType
		{
			const s32 RGB	= 0;	//!< RGB
			const s32 RGBA	= 1;	//!< RGBA
			const s32 ARGB	= 3;	//!< ARGB
		}

		//----------------------------------------------------------------------
		//!	画像出力(bmp)
		//!	@param pPath     [in] パス
		//!	@param pImage    [in] イメージデータ
		//!	@param iWidth    [in] 横幅
		//!	@param iHeight   [in] 縦幅
		//!	@param iType     [in] イメージデータのカラータイプ
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 OutputImageToBitmap(const c8* pPath, const void* pImage, s32 iWidth, s32 iHeight, s32 iType = EColorType::RGBA);

		//----------------------------------------------------------------------
		//!	画像出力(png)
		//!	@param pPath     [in] パス
		//!	@param pImage    [in] イメージデータ
		//!	@param iWidth    [in] 横幅
		//!	@param iHeight   [in] 縦幅
		//!	@param iType     [in] イメージデータのカラータイプ
		//!	@return 結果
		//----------------------------------------------------------------------
		b8 OutputImageToPNG(const c8* pPath, const void* pImage, s32 iWidth, s32 iHeight, s32 iType = EColorType::RGBA);
	}
}

#endif	// __KGL_IMAGE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
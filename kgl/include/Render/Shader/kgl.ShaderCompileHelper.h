//----------------------------------------------------------------------
//!
//!	@file	kgl.ShaderCompileHelper.h
//!	@brief	シェーダーコンパイル関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_SHADER_COMPILE_HELPER_H__
#define	__KGL_SHADER_COMPILE_HELPER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "../../../Library/kgGraphics/include/kgGraphics.h"

namespace kgl
{
	namespace Shader
	{
#if	!KGL_FINAL_RELEASE
		//!	シェーダーのコンパイル
		//!	@param bRebuild	[in] 全てをコンパイルし直すか？
		//!	@return 結果
		b8 ShaderCompile(b8 bRebuild = false);
		//!	シェーダーのコンパイル(ゲーム固有のシェーダー)
		//!	@param bRebuild	[in] 全てをコンパイルし直すか？
		//!	@return 結果
		extern b8 ShaderCompileFromGame(b8 bRebuild = false);
#endif	// ~#if	!KGL_FINAL_RELEASE
	}
}

#endif	// __KGL_SHADER_COMPILE_HELPER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
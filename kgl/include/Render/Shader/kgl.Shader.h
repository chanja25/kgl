//----------------------------------------------------------------------
//!
//!	@file	kgl.Shader.h
//!	@brief	シェーダー関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_SHADER_H__
#define	__KGL_SHADER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "../../../Library/kgGraphics/include/kgGraphics.h"
#include "File/kgl.FileReader.h"

namespace kgl
{
	namespace Render
	{
		//======================================================================
		//!	頂点シェーダーインターフェイス
		//======================================================================
		interface IVertexShader
			: public File::IExtraObject
		{
		public:
			//!	リソースの取得
			//!	@return リソース情報
			virtual kgGraphics::IVertexShader* GetResource(void) = 0;
		};
		//======================================================================
		//!	ピクセルシェーダーインターフェイス
		//======================================================================
		interface IPixelShader
			: public File::IExtraObject
		{
		public:
			//!	リソースの取得
			//!	@return リソース情報
			virtual kgGraphics::IPixelShader* GetResource(void) = 0;
		};

		//!	頂点シェーダーの生成
		//!	@param ppTexture [out] 実体格納用ポインタ
		//!	@param pData [in] データ
		//!	@return 結果
		b8 Create(IVertexShader** ppShader, const void* pData);
		//!	ピクセルシェーダーの生成
		//!	@param ppTexture [out] 実体格納用ポインタ
		//!	@param pData [in] データ
		//!	@return 結果
		b8 Create(IPixelShader** ppShader, const void* pData);
	}
}

#endif	// __KGL_SHADER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
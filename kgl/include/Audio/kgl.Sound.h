//----------------------------------------------------------------------
//!
//!	@file	kgl.Sound.h
//!	@brief	サウンド関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_SOUND_H__
#define	__KGL_SOUND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "../File/kgl.FileReader.h"
#include "../../Library/kgSound/include/kgSound.h"

namespace kgl
{
	namespace Audio
	{
		//======================================================================
		//!	サウンド管理インターフェイス
		//======================================================================
		interface ISound
			: public File::IExtraObject
		{
		public:
			//!	再生
			//!	@return 結果
			virtual b8 Play(b8 bLoop = false, f32 fOffset = 0.0f) = 0;
			//!	停止
			virtual void Stop(void) = 0;
			//!	一時停止
			virtual void Pause(void) = 0;
			//!	一時停止の解除
			virtual void Resume(void) = 0;

			//!	再生中か？
			//!	@return 再生フラグ
			virtual b8 IsPlaying(void) = 0;
			//!	ループフラグ取得
			//!	@return ループフラグ
			virtual b8 IsLoop(void) = 0;

		public:
			//!	リソースの取得
			//!	@return リソース情報
			virtual kgSound::ISoundBuffer* GetResource(void) = 0;
			//!	Wave情報の取得
			//!	@return Wave情報
			virtual const kgSound::WaveInfo& GetWaveInfo(void) = 0;
		};

		//!	サウンドの生成
		//!	@param ppSound	[out] 実体格納用ポインタ
		//!	@param Info		[in] サウンドバッファ情報
		//!	@param pData	[in] データ
		//!	@return 結果
		b8 Create(ISound** ppSound, const kgSound::WaveInfo& Info, const void* pData = null);
		//!	サウンドの生成
		//!	@param ppSound	[out] 実体格納用ポインタ
		//!	@param pDecoder	[in]  デコーダー
		//!	@return 結果
		b8 Create(ISound** ppSound, kgSound::IDecoder* pDecoder);
	}
}

#endif	// __KGL_SOUND_H__
//=======================================================================
//	END OF FILE
//=======================================================================
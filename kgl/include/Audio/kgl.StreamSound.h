//----------------------------------------------------------------------
//!
//!	@file	kgl.StreamSound.h
//!	@brief	ストリームサウンド関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_STREAMSOUND_H__
#define	__KGL_STREAMSOUND_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "kgl.Sound.h"

namespace kgl
{
	namespace Audio
	{
		//======================================================================
		//!	ストリームサウンドインターフェイス
		//======================================================================
		interface IStreamSound
			: public IKGLBase
		{
		public:
			//!	再生
			//!	@return 結果
			virtual b8 Play(b8 bLoop = false, f32 fOffset = 0.0f) = 0;
			//!	停止
			virtual void Stop(void) = 0;
			//!	一時停止
			virtual void Pause(void) = 0;
			//!	一時停止の解除
			virtual void Resume(void) = 0;

			//!	再生中か？
			//!	@return 再生フラグ
			virtual b8 IsPlaying(void) = 0;
			//!	ループフラグ取得
			//!	@return ループフラグ
			virtual b8 IsLoop(void) = 0;

			//!	現在の再生時間を取得
			//!	@return 現在の再生時間
			virtual f32 CurrentTime(void) = 0;

		public:
			//!	リソースの取得
			//!	@return リソース情報
			virtual kgSound::ISoundBuffer* GetResource(void) = 0;
			//!	Wave情報の取得
			//!	@return Wave情報
			virtual const kgSound::WaveInfo& GetWaveInfo(void) = 0;
		};

		//======================================================================
		//!	ストリーム管理インターフェイス
		//======================================================================
		interface IStreamManager
			: public IKGLBase
		{
		public:
			//!	サウンドの生成
			//!	@param ppSound	[out] 実体格納用ポインタ
			//!	@param pDecoder	[in]  デコーダー
			//!	@return 結果
			virtual b8 Create(IStreamSound** ppStream, kgSound::IDecoder* pDecoder) = 0;

		public:
			//!	ストリームバッファの更新
			virtual void Update(void) = 0;
		};
	}
}

#endif	// __KGL_STREAMSOUND_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Audio.h
//!	@brief	オーディオシステム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_AUDIO_H__
#define	__KGL_AUDIO_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "../../Library/kgSound/include/kgSound.h"
#include "kgl.Sound.h"
#include "kgl.StreamSound.h"

namespace kgl
{
	using namespace kgSound;

	namespace Audio
	{
		//======================================================================
		//!	オーディオ管理インターフェイス
		//======================================================================
		interface IAudioManager
			: public IKGLBase
		{
		public:
			//!	サウンドドライバーの取得
			//!	@return サウンドドライバー
			FORCEINLINE ISoundDriver* SoundDriver(void) { return m_pDriver; }
			//!	ストリーム管理の取得
			//!	@return ストリーム管理
			FORCEINLINE IStreamManager* StreamManager(void) { return m_pStreamManager; }

		protected:
			TKGLPtr<ISoundDriver>	m_pDriver;			//!< サウンドドライバー
			TKGLPtr<IStreamManager>	m_pStreamManager;	//!< ストリーム管理
		};

		//!	オーディオ管理の生成
		//!	@param ppAudio	[out] 実体格納用ポインタ
		//!	@return 結果
		b8 Create(IAudioManager** ppAudio);
	}
}

#endif	// __KGL_AUDIO_H__
//=======================================================================
//	END OF FILE
//=======================================================================
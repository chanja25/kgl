//----------------------------------------------------------------------
//!
//!	@file	kgl.zlib.h
//!	@brief	zlib圧縮
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_ZLIB_H__
#define	__KGL_ZLIB_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	namespace zlib
	{
		//!	戻り値
		namespace EResult
		{
			const s32 OK		= 0;	//!< 成功
			const s32 MemError	= 1;	//!< メモリ不足
			const s32 BufError	= 2;	//!< 出力バッファサイズ不足
			const s32 Unknown	= -1;	//!< 不明なエラー
		}

		//!	圧縮
		//!	@param pBuffer			[out]	 圧縮後のデータ
		//!	@param pCompressSize	[in/out] pBufferのサイズ/圧縮後のサイズ
		//!	@param pSource			[in]	 圧縮するデータ
		//!	@param uiSourceSize		[in]	 圧縮するデータのサイズ
		//!	@param iLevel			[in]	 圧縮レベル(1〜9)
		//!	@return 結果(EResult参照)
		s32 Compress(void* pBuffer, u32* pCompressSize, const void* pSource, u32 uiSourceSize, s32 iLevel = 9);

		//!	解凍
		//!	@param pBuffer		[out]	 解凍後のデータ
		//!	@param pSize		[in/out] pBufferのサイズ/解凍後のサイズ
		//!	@param pSource		[in]	 解凍するデータ
		//!	@param uiSourceSize	[in]	 解凍するデータのサイズ
		//!	@return 結果(EResult参照)
		s32 Decompress(void* pBuffer, u32* pSize, const void* pSource, u32 uiSourceSize);
	}
}

#endif	// __KGL_ZLIB_H__
//=======================================================================
//	END OF FILE
//=======================================================================
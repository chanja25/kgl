//----------------------------------------------------------------------
//!
//!	@file	kgl.define.h
//!	@brief	define��`
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_DEFINE_H__
#define	__KGL_DEFINE_H__


//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#ifndef	null
	#define null		nullptr
#endif	// ~#if null

#ifndef	interface
	#define interface	struct
#endif	// ~#if interface

#if	KGL_DEBUG_INLINE
	#ifdef	FORCEINLINE
		#undef	FORCEINLINE
	#endif	// ~#if FORCEINLUNE

	#define	FORCEINLINE	inline
#endif	// ~#if	KGL_DEBUG_INLINE

#ifndef	INLINE
	#define	INLINE		inline
#endif	// ~#if INLINE

#ifndef	FORCEINLINE
	#define	FORCEINLINE	__forceinline
#endif	// ~#if FORCEINLUNE

#ifndef	EXPORTFUNC
	#define	EXPORTFUNC	extern "C" __declspec(dllexport)
#endif	// ~#if EXPORTFUNC

#ifndef	THREADFUNC
	#define	THREADFUNC	__stdcall
#endif	// ~#if THREADFUNC

#ifndef	ALIGNED
	#define	ALIGNED(x)	__declspec(align(x))
#endif	// ~#if ALIGNED

#ifndef	FOURCC
	#define FOURCC(a, b, c, d)	((a) | (b << 8) | (c << 16) | (d << 24))
#endif	// ~#if FOURCC

#ifndef	STRUCT_OFFSET
	#define STRUCT_OFFSET(_class_, _member_) ((s32)&((_class_*)0)->_member_)
#endif	// ~#if STRUCT_OFFSET

#ifndef	KB
	#define	KB(_size_)	((_size_) * 1024LL)
#endif	// ~#if KB

#ifndef	MB
	#define	MB(_size_)	((_size_) * 1024LL * 1024LL)
#endif	// ~#if MB

#ifndef	GB
	#define	GB(_size_)	((_size_) * 1024LL * 1024LL * 1024LL)
#endif	// ~#if GB

#ifndef	Is64bit
	#define	Is64bit()	(sizeof(void*) == 8)
#endif	// ~#if Is64bit

#ifndef	InfiniteWait
	#define	InfiniteWait	((u32)0xffffffff)
#endif	// ~#if InfiniteWait

#ifndef	EOL
	#define	EOL	"\r\n"
#endif	// ~#if EOL

#define	kgVersion(_major_ ,_minor_)	((((_major_) << 16) & 0xffff0000) & ((_minor_) & 0x0000ffff))

#define	kgSleep(_second_)			Sleep((u32)((_second_) * 1000.0f))

#define	kgZeroMemory(_ptr_, _size_)	memset(_ptr_, 0, _size_)
#define	kgMemset					memset
#define	kgMemcpy					memcpy
#define	kgMemmove					memmove
#define	kgMemcmp					memcmp

/*
#define HasTrivialConstructor(_t_)	__has_trivial_constructor(_t_)
#define HasTrivialDestructor(_t_)	__has_trivial_destructor(_t_)
#define HasTrivialAssign(_t_)		__has_trivial_assign(_t_)
#define HasTrivialCopy(_t_)			__has_trivial_copy(_t_)
#define IsPODType(_t_)				__is_pod(_t_)
#define IsEnumType(_t_)				__is_enum(_t_)
#define	IsEmptyType(_t_)			__is_empty(_t_)
#define	IsAbstractType(_t_)			__is_abstract(_t_)
#define	IsBaseOf(_base_, _t_)		__is_base_of(_base_, _t_)
*/
#endif	// __KGL_DEFINE_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Collision.inline.h
//!	@brief	当たり関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_COLLISION_INLINE_H__
#define	__KGL_COLLISION_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.Collision.struct.h"

namespace kgl
{
	namespace Collision
	{
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Point& point, Shere& shere)
		{
			register f32 radius, dist;
			dist = Math::Vec3DistanceSq(point.vLocation, shere.vLocation);
			radius = shere.fRadius * shere.fRadius;

			return (dist < radius);
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Point& point, Cube& cube)
		{
			register RotationMatrix mInvRot;
			Math::MatrixRotation(mInvRot, cube.rRotaion);
			Math::MatrixInverse(mInvRot, mInvRot);

			register Vector3 v;
			Math::Vec3Subtract(v, point.vLocation, cube.vLocation);
			Math::Vec3Transform(v, v, mInvRot);

			if( v.x < -cube.vSize.x || cube.vSize.x < v.x )return false;
			if( v.y < -cube.vSize.y || cube.vSize.y < v.y )return false;
			if( v.z < -cube.vSize.z || cube.vSize.z < v.z )return false;

			return true;
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Line& line, Triangle& triangle)
		{
			register f32 dist;
			dist = Math::Vec3DistanceSq(line.vStart, line.vEnd);
			dist *= dist;

			register Vector3 center;
			Math::Vec3Add(center, triangle.vLocation[0], triangle.vLocation[1]);
			Math::Vec3Add(center, center, triangle.vLocation[2]);
			Math::Vec3Division(center, center, 3.0f);

			if( dist < Math::Vec3DistanceSq(center, line.vStart) )
			{
				return false;
			}

			register Vector3 vec, normal, v, v1, v2;
			Math::Vec3Subtract(vec, line.vEnd, line.vStart);
			Math::Vec3Normalize(vec, vec);

			Math::Vec3Subtract(v1, triangle.vLocation[1], triangle.vLocation[0]);
			Math::Vec3Subtract(v2, triangle.vLocation[2], triangle.vLocation[0]);
			//	外積で法線を算出
			Math::Vec3Cross(normal, v1, v2);
			//	内積により面の向きを確認
			if( Math::Vec3Dot(normal, vec) > 0.0f )
			{
				return false;
			}

			register f32 t;
			//	面との交点算出
			//	交点p = pos + t * vec・・・�@
			//	(p1 - p)・n = 0・・・�A
			//	p・n = pos・n + t * vec・n・・・�B = �@ ・ n
			//	p1・n = pos・n + t * vec・n・・・�C = �B - �A
			//	t = (p1 - pos)・n / (vec・n)・・・�Cをtについて解く
			Math::Vec3Subtract(v, triangle.vLocation[0], line.vStart);
			t = Math::Vec3Dot(v, normal) / Math::Vec3Dot(vec, normal);
			if( t < 0.0f )
			{
				return false;
			}
			Math::Vec3Scale(v, vec, t);
			Math::Vec3Add(v, v, line.vStart);

			//	交点が面の内側にあるか確認
			Math::Vec3Subtract(v1, triangle.vLocation[0], v);
			Math::Vec3Subtract(v2, triangle.vLocation[1], triangle.vLocation[0]);
			Math::Vec3Cross(v1, v1, v2);
			if( Math::Vec3Dot(v1, normal) < 0 )
			{
				return false;
			}
			Math::Vec3Subtract(v1, triangle.vLocation[1], v);
			Math::Vec3Subtract(v2, triangle.vLocation[2], triangle.vLocation[1]);
			Math::Vec3Cross(v1, v1, v2);
			if( Math::Vec3Dot(v1, normal) < 0 )
			{
				return false;
			}
			Math::Vec3Subtract(v1, triangle.vLocation[2], v);
			Math::Vec3Subtract(v2, triangle.vLocation[0], triangle.vLocation[2]);
			Math::Vec3Cross(v1, v1, v2);
			if( Math::Vec3Dot(v1, normal) < 0 )
			{
				return false;
			}

			return true;
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Line& line, Shere& shere)
		{
			register Vector3 v1, v2;
			Math::Vec3Subtract(v1, line.vEnd, line.vStart);
			Math::Vec3Subtract(v2, shere.vLocation, line.vStart);

			register f32 t, dist;
			dist = Math::Vec3LengthSq(v2);
			if( dist < shere.fRadius * shere.fRadius )
			{
				return true;
			}

			register f32 distmax;
			distmax = Math::Vec3Length(v1);
			dist = Math::Sqrt(dist);
			Math::Vec3Division(v1, v1, distmax);
			Math::Vec3Division(v2, v2, dist);

			t = Math::Vec3Dot(v1, v2);
			if( t < 0.0f )
			{
				return false;
			}

			dist = Min(dist, distmax);
			Math::Vec3Scale(v1, v1, t * dist);
			Math::Vec3Add(v1, line.vStart, v1);

			register f32 radius;
			dist = Math::Vec3DistanceSq(v1, shere.vLocation);
			radius = shere.fRadius * shere.fRadius;
			return (dist < radius);
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Line& line, Cube& cube)
		{
			(void)line;
			(void)cube;
			return false;
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Line& line, Capsule& capsule)
		{
			(void)line;
			(void)capsule;
			return false;
		}

		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Cube& cube1, Cube& cube2)
		{
			(void)cube1;
			(void)cube2;
			return false;
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Cube& cube, Shere& shere)
		{
			register RotationMatrix mInvRot;
			Math::MatrixRotation(mInvRot, cube.rRotaion);
			Math::MatrixInverse(mInvRot, mInvRot);

			register Vector3 v, size;
			Math::Vec3Subtract(v, shere.vLocation, cube.vLocation);
			Math::Vec3Transform(v, v, mInvRot);

			Math::Vec3Division(size, cube.vSize, shere.fRadius);

			if( v.x < -size.x || size.x < v.x )return false;
			if( v.y < -size.y || size.y < v.y )return false;
			if( v.z < -size.z || size.z < v.z )return false;

			return true;
		}

		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Shere& shere1, Shere& shere2)
		{
			register f32 dist1, dist2;
			dist1 = Math::Vec3DistanceSq(shere1.vLocation, shere2.vLocation);
			dist2 = shere1.fRadius * shere1.fRadius + shere2.fRadius * shere2.fRadius;
			return (dist1 < dist2);
		}
		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Shere& shere, Capsule& capsule)
		{
			register Vector3 v1, v2;

			register f32 t, radius, dist;
			radius = shere.fRadius * shere.fRadius + capsule.fRadius * capsule.fRadius;

			Math::Vec3Subtract(v1, shere.vLocation, capsule.vStart);
			dist = Math::Vec3LengthSq(v1);
			if( dist < radius )return true;

			Math::Vec3Subtract(v1, shere.vLocation, capsule.vEnd);
			dist = Math::Vec3LengthSq(v1);
			if( dist < radius )return true;

			Math::Vec3Subtract(v2, capsule.vEnd, capsule.vStart);

			register f32 distmax;
			distmax = Math::Vec3Length(v1);
			dist = Math::Sqrt(dist);
			Math::Vec3Division(v1, v1, distmax);
			Math::Vec3Division(v2, v2, dist);

			t = Math::Vec3Dot(v1, v2);
			if( t < 0.0f )
			{
				return false;
			}

			dist = Min(dist, distmax);
			Math::Vec3Scale(v1, v1, t * dist);
			Math::Vec3Add(v1, capsule.vStart, v1);

			dist = Math::Vec3DistanceSq(v1, shere.vLocation);
			radius = shere.fRadius * shere.fRadius / capsule.fRadius;
			return (dist < radius);
		}

		//----------------------------------------------------------------------
		//	当たり判定
		//----------------------------------------------------------------------
		FORCEINLINE b8 Collision(Capsule& capsule1, Capsule& capsule2)
		{
			(void)capsule1;
			(void)capsule2;
			return false;
		}
	}
}


#endif	// __KGL_COLLISION_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Collision.h
//!	@brief	当たり関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_COLLISION_H__
#define	__KGL_COLLISION_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../../kgl.h"
#include "kgl.Collision.struct.h"

namespace kgl
{
	namespace Collision
	{
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Point& point, Shere& shere);
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Point& point, Cube& cube);

		//!	当たり判定
		//!	@return 結果
		b8 Collision(Line& line, Triangle& triangle);
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Line& line, Cube& cube);
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Line& line, Shere& shere);
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Line& line, Capsule& capsule);

		//!	当たり判定
		//!	@return 結果
		b8 Collision(Cube& cube1, Cube& cube2);
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Cube& cube, Shere& shere);

		//!	当たり判定
		//!	@return 結果
		b8 Collision(Shere& shere1, Shere& shere2);
		//!	当たり判定
		//!	@return 結果
		b8 Collision(Shere& shere, Capsule& capsule);

		//!	当たり判定
		//!	@return 結果
		b8 Collision(Capsule& capsule1, Capsule& capsule2);
	}
}

#include "kgl.Collision.inline.h"

#endif	// __KGL_COLLISION_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Color.inline.h
//!	@brief	カラー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_COLOR_INLINE_H__
#define	__KGL_COLOR_INLINE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Color::Color(void)
		: r(0), g(0), b(0), a(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Color::Color(const Color& c)
		: r(c.r), g(c.g), b(c.b), a(c.a)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE Color::Color(u8 rr, u8 gg, u8 bb, u8 aa)
		: r(rr), g(gg), b(bb), a(aa)
	{}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Color::operator u8* (void)
	{
		return c;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE Color::operator const u8* (void)
	{
		return c;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE Color& Color::operator = (const Color& c)
	{
		r = c.r; g = c.g; b = c.b; a = c.a;
		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE Color Color::operator - (void) const
	{
		return Color(-r, -g, -b, -a);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE Color Color::operator + (const Color& c) const
	{
		return Color(r + c.r, g + c.g, b + c.b, a + c.a);
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE Color Color::operator - (const Color& c) const
	{
		return Color(r - c.r, g - c.g, b - c.b, a - c.a);
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Color Color::operator * (u8 s) const
	{
		return Color(r * s, g * s, b * s, a * s);
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE Color Color::operator / (u8 s) const
	{
		return Color(r / s, g / s, b / s, a / s);
	}
	//----------------------------------------------------------------------
	//	加算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Color& Color::operator += (const Color& c)
	{
		r += c.r; g += c.g; b += c.b; a += c.a;
		return *this;
	}
	//----------------------------------------------------------------------
	//	減算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Color& Color::operator -= (const Color& c)
	{
		r -= c.r; g -= c.g; b -= c.b; a -= c.a;
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Color& Color::operator *= (u8 s)
	{
		r *= s; g *= s; b *= s; a *= s;
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE Color& Color::operator /= (u8 s)
	{
		r /= s; g /= s; b /= s; a /= s;
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE Color Color::operator * (const Color& c) const
	{
		return Color(r * c.r, g * c.g, b * c.b, a * c.a);
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Color::operator == (const Color& c) const
	{
		return (r == c.r && g == c.g && b == c.b && a == c.a);
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 Color::operator != (const Color& c) const
	{
		return (r != c.r || g != c.g || b != c.b || a != c.a);
	}
	//----------------------------------------------------------------------
	//	値の設定
	//----------------------------------------------------------------------
	FORCEINLINE void Color::Set(u8 r, u8 g, u8 b, u8 a)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE Color operator * (u8 s, const Color& c)
	{
		return Color(c.r * s, c.g * s, c.b * s, c.a * s);
	}

	
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE FColor::FColor(void)
		: r(0), g(0), b(0), a(0)
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE FColor::FColor(const FColor& c)
#if	USE_SIMD
		: simd(c.simd)
#else	// ~#if USE_SIMD
		: r(c.r), g(c.g), b(c.b), a(c.a)
#endif	// ~#if USE_SIMD
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE FColor::FColor(f32 rr, f32 gg, f32 bb, f32 aa)
#if	USE_SIMD
		: simd(_mm_set_ps(aa, bb, gg, rr))
#else	// ~#if USE_SIMD
		: r(rr), g(gg), b(bb), a(aa)
#endif	// ~#if USE_SIMD
	{}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	FORCEINLINE FColor::FColor(u32 uiColor)
	{
		const f32 fInv = 1.0f / 255;

		const u8* pColor = (u8*)&uiColor;
#if	USE_SIMD
		__m128 vec;
		vec	= _mm_set_ps((f32)pColor[3], (f32)pColor[2], (f32)pColor[1], (f32)pColor[0]);
		simd	= _mm_mul_ps(vec, _mm_set_ps1(fInv));
#else	// ~#if USE_SIMD
		r = (f32)pColor[0] * fInv;
		g = (f32)pColor[1] * fInv;
		b = (f32)pColor[2] * fInv;
		a = (f32)pColor[3] * fInv;
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE FColor::operator f32* (void)
	{
		return c;
	}
	//----------------------------------------------------------------------
	//	キャスト
	//----------------------------------------------------------------------
	FORCEINLINE FColor::operator const f32* (void)
	{
		return c;
	}
	//----------------------------------------------------------------------
	//	代入
	//----------------------------------------------------------------------
	FORCEINLINE FColor& FColor::operator = (const FColor& c)
	{
#if	USE_SIMD
		simd	= _mm_set_ps(c.a, c.b, c.g, c.r);
#else	// ~#if USE_SIMD
		r = c.r; g = c.g; b = c.b; a = c.a;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	反転
	//----------------------------------------------------------------------
	FORCEINLINE FColor FColor::operator - (void) const
	{
		return FColor(-r, -g, -b, -a);
	}
	//----------------------------------------------------------------------
	//	加算
	//----------------------------------------------------------------------
	FORCEINLINE FColor FColor::operator + (const FColor& c) const
	{
#if	USE_SIMD
		FColor out;
		out.simd = _mm_add_ps(simd, c.simd);
		return out;
#else	// ~#if USE_SIMD
		return FColor(r + c.r, g + c.g, b + c.b, a + c.a);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	減算
	//----------------------------------------------------------------------
	FORCEINLINE FColor FColor::operator - (const FColor& c) const
	{
#if	USE_SIMD
		FColor out;
		out.simd = _mm_sub_ps(simd, c.simd);
		return out;
#else	// ~#if USE_SIMD
		return FColor(r - c.r, g - c.g, b - c.b, a - c.a);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE FColor FColor::operator * (f32 s) const
	{
#if	USE_SIMD
		FColor out;
		out.simd = _mm_mul_ps(simd, _mm_set_ps1(s));
		return out;
#else	// ~#if USE_SIMD
		return FColor(r * s, g * s, b * s, a * s);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	除算
	//----------------------------------------------------------------------
	FORCEINLINE FColor FColor::operator / (f32 s) const
	{
#if	USE_SIMD
		FColor out;
		out.simd = _mm_div_ps(simd, _mm_set_ps1(s));
		return out;
#else	// ~#if USE_SIMD
		s = 1.0f / s;
		return FColor(r * s, g * s, b * s, a * s);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	加算・代入
	//----------------------------------------------------------------------
	FORCEINLINE FColor& FColor::operator += (const FColor& c)
	{
#if	USE_SIMD
		simd = _mm_add_ps(simd, c.simd);
#else	// ~#if USE_SIMD
		r += c.r; g += c.g; b += c.b; a += c.a;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	減算・代入
	//----------------------------------------------------------------------
	FORCEINLINE FColor& FColor::operator -= (const FColor& c)
	{
#if	USE_SIMD
		simd = _mm_sub_ps(simd, c.simd);
#else	// ~#if USE_SIMD
		r -= c.r; g -= c.g; b -= c.b; a -= c.a;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	乗算・代入
	//----------------------------------------------------------------------
	FORCEINLINE FColor& FColor::operator *= (f32 s)
	{
#if	USE_SIMD
		simd = _mm_mul_ps(simd, _mm_set_ps1(s));
#else	// ~#if USE_SIMD
		r *= s; g *= s; b *= s; a *= s;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	除算・代入
	//----------------------------------------------------------------------
	FORCEINLINE FColor& FColor::operator /= (f32 s)
	{
#if	USE_SIMD
		simd = _mm_div_ps(simd, _mm_set_ps1(s));
#else	// ~#if USE_SIMD
		s = 1.0f / s;
		r *= s; g *= s; b *= s; a *= s;
#endif	// ~#if USE_SIMD
		return *this;
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE FColor FColor::operator * (const FColor& c) const
	{
#if	USE_SIMD
		FColor out;
		out.simd = _mm_mul_ps(simd, c.simd);
		return out;
#else	// ~#if USE_SIMD
		return FColor(r * c.r, g * c.g, b * c.b, a * c.a);
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 FColor::operator == (const FColor& c) const
	{
		return (r == c.r && g == c.g && b == c.b && a == c.a);
	}
	//----------------------------------------------------------------------
	//	等号
	//----------------------------------------------------------------------
	FORCEINLINE b8 FColor::operator != (const FColor& c) const
	{
		return (r != c.r || g != c.g || b != c.b || a != c.a);
	}
	//----------------------------------------------------------------------
	//	値の設定
	//----------------------------------------------------------------------
	FORCEINLINE void FColor::Set(f32 r, f32 g, f32 b, f32 a)
	{
#if	USE_SIMD
		simd = _mm_set_ps(a, b, g, r);
#else	// ~#if USE_SIMD
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
#endif	// ~#if USE_SIMD
	}
	//----------------------------------------------------------------------
	//	乗算
	//----------------------------------------------------------------------
	FORCEINLINE FColor operator * (f32 s, const FColor& c)
	{
#if	USE_SIMD
		FColor out;
		out.simd = _mm_mul_ps(c.simd, _mm_set_ps1(s));
		return out;
#else	// ~#if USE_SIMD
		return FColor(c.r * s, c.g * s, c.b * s, c.a * s);
#endif	// ~#if USE_SIMD
	}
}

#endif	// __KGL_COLOR_INLINE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
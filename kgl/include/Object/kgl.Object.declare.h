//----------------------------------------------------------------------
//!
//!	@file	kgl.Object.declare.h
//!	@brief	オブジェクト
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_OBJECT_DECLARE_H__
#define	__KGL_OBJECT_DECLARE_H__


//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	const Color		ColorBlack	(  0,   0,   0);
	const Color		ColorWhite	(255, 255, 255);
	const Color		ColorGray	(128, 128, 128);
	const Color		ColorRed	(255,   0,   0);
	const Color		ColorGreen	(  0, 255,   0);
	const Color		ColorBlue	(  0,   0, 255);
	const Color		ColorYellow	(255, 255,   0);
	const Color		ColorPurple	(128,   0, 128);
	const Color		ColorOrange	(255, 165,   0);
	const Color		ColorPink	(255, 192, 202);

	const FColor	FColorBlack	(0.0f, 0.0f,  0.0f);
	const FColor	FColorWhite	(1.0f, 1.0f,  1.0f);
	const FColor	FColorGray	(0.5f, 0.5f,  0.5f);
	const FColor	FColorRed	(1.0f, 0.0f,  0.0f);
	const FColor	FColorGreen	(0.0f, 1.0f,  0.0f);
	const FColor	FColorBlue	(0.0f, 0.0f,  1.0f);
	const FColor	FColorYellow(1.0f, 1.0f,  0.0f);
	const FColor	FColorPurple(0.5f, 0.0f,  0.5f);
	const FColor	FColorOrange(1.0f, 0.64f, 0.0f);
	const FColor	FColorPink	(1.0f, 0.75f, 0.79f);
}


#endif	// __KGL_OBJECT_DECLARE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
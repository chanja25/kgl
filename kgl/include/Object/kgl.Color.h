//----------------------------------------------------------------------
//!
//!	@file	kgl.Color.h
//!	@brief	カラー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_COLOR_H__
#define	__KGL_COLOR_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"

namespace kgl
{
	//======================================================================
	//!	カラー
	//======================================================================
	class Color
	{
	public:
		//!	コンストラクタ
		Color(void);
		//!	コンストラクタ
		Color(const Color& c);
		//!	コンストラクタ
		Color(u8 r, u8 g, u8 b, u8 a = 255);

		//!	キャスト
		operator u8* (void);
		//!	キャスト
		operator const u8* (void);		
		//!	代入
		Color& operator = (const Color& c);
		//!	反転
		Color operator -(void) const;
		//!	加算
		Color operator + (const Color& c) const;
		//!	減算
		Color operator - (const Color& c) const;
		//!	スカラー乗算
		Color operator * (u8 s) const;
		//!	スカラー除算
		Color operator / (u8 s) const;
		//!	加算後代入
		Color& operator += (const Color& c);
		//!	減算後代入
		Color& operator -= (const Color& c);
		//!	スカラー乗算後代入
		Color& operator *= (u8 s);
		//!	スカラー除算後代入
		Color& operator /= (u8 s);
		//!	内積
		Color operator * (const Color& c) const;

		//!	等号
		b8 operator == (const Color& c) const;
		//!	不等号
		b8 operator != (const Color& c) const;

		//!	値の設定
		void Set(u8 r, u8 g, u8 b, u8 a = 255);

	public:
		union
		{
			u8	c[4];

			struct
			{
				u8	r, g, b, a;
			};
		};
	};
	//!	乗算
	Color operator * (u8 s, const Color& c);

	template<>
	struct IsPODType<Color> { enum { value = true }; };

	//======================================================================
	//!	カラー
	//======================================================================
	class FColor
	{
	public:
		//!	コンストラクタ
		FColor(void);
		//!	コンストラクタ
		FColor(const FColor& c);
		//!	コンストラクタ
		FColor(f32 r, f32 g, f32 b, f32 a = 1.0f);
		//!	コンストラクタ
		FColor(u32 uiColor);

		//!	キャスト
		operator f32* (void);
		//!	キャスト
		operator const f32* (void);		
		//!	代入
		FColor& operator = (const FColor& c);
		//!	反転
		FColor operator -(void) const;
		//!	加算
		FColor operator + (const FColor& c) const;
		//!	減算
		FColor operator - (const FColor& c) const;
		//!	スカラー乗算
		FColor operator * (f32 s) const;
		//!	スカラー除算
		FColor operator / (f32 s) const;
		//!	加算後代入
		FColor& operator += (const FColor& c);
		//!	減算後代入
		FColor& operator -= (const FColor& c);
		//!	スカラー乗算後代入
		FColor& operator *= (f32 s);
		//!	スカラー除算後代入
		FColor& operator /= (f32 s);
		//!	乗算
		FColor operator * (const FColor& c) const;

		//!	等号
		b8 operator == (const FColor& c) const;
		//!	不等号
		b8 operator != (const FColor& c) const;

		//!	値の設定
		void Set(f32 r, f32 g, f32 b, f32 a = 1.0f);

	public:
		union
		{
			f32	c[4];

			struct
			{
				f32	r, g, b, a;
			};
#if	USE_SIMD
			__m128	simd;
#endif	// ~#if USE_SIMD
		};
	};
	//!	乗算
	FColor operator * (f32 s, const FColor& c);

	template<>
	struct IsPODType<FColor> { enum { value = true }; };
}

#endif	// __KGL_COLOR_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.GameManager.h
//!	@brief	ゲーム管理関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_GAME_MANAGER_H__
#define	__KGL_GAME_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "../Scene/kgl.Scene.h"
#include "../File/kgl.FileReader.h"
#include "../Time/kgl.Frame.h"
#include "kgl.ResourceManager.h"
#include "Audio/kgl.Audio.h"

namespace kgl
{
	namespace Manager
	{
		//======================================================================
		//!	ゲーム情報管理インターフェイス(ゲーム側で実装して使用する)
		//======================================================================
		interface IGameInfo
			: public IKGLBase
		{};

		//======================================================================
		//!	ゲーム管理インターフェイス
		//======================================================================
		interface IGameManager
			: public IKGLBase
		{
		public:
			//!	汎用データの設定
			//!	@param pParam	[in] 汎用データ
			FORCEINLINE void SetGameInfo(IGameInfo* pGameInfo){ m_pGameInfo = pGameInfo; }
			//!	汎用データの取得
			//!	@return 汎用データ
			template<typename T>
			FORCEINLINE T* GameInfo(void) { return Cast<T, IGameInfo>(m_pGameInfo); }

		public:
			//!	ファイル管理の取得
			//!	@return ファイル管理
			FORCEINLINE Window::IWindow* MainWindow(void) { return m_pWindow; }
			//!	ファイル管理の取得
			//!	@return ファイル管理
			FORCEINLINE File::IFileReader* FileReader(void) { return m_pFileReader; }
			//!	ファイル管理の取得
			//!	@return ファイル管理
			FORCEINLINE IResourceManager* ResourceManager(void) { return m_pResourceManager; }
			//!	シーン管理の取得
			//!	@return シーン管理
			FORCEINLINE Scene::ISceneManager* SceneManager(void) { return m_pSceneManager; }
			//!	オーディオ管理の取得
			//!	@return オーディオ管理
			FORCEINLINE Audio::IAudioManager* AudioManager(void) { return m_pAudioManager; }
			//!	グラフィックドライバーの取得
			//!	@return グラフィックドライバー
			FORCEINLINE IGraphicDriver* GraphicDriver(void) { return m_pGraphicDriver; }
			//!	フレーム制御の取得
			//!	@return フレーム制御の取得
			FORCEINLINE const Frame::IFrameControl* FrameControl(void) { return m_pFrameControl; }
#if	KGL_DEBUG_MENU
			//!	デバッグメニューの取得
			//!	@return デバッグメニュー
			FORCEINLINE Debug::CDebugMenu* DebugMenu(void) { return m_pDebugMenu; }
#endif	// ~#if	KGL_DEBUG_MENU
#if	KGL_DEBUG_PROFILE
			//!	FPSカウンタの取得(Update)
			//!	@return FPSカウンタ
			FORCEINLINE Frame::IFPSCounter* FPSCounter(void) { return m_pFPSCounter; }
			//!	FPSカウンタの取得(Draw)
			//!	@return FPSカウンタ
			FORCEINLINE Frame::IFPSCounter* RenderFPSCounter(void) { return m_pRenderFPSCounter; }
#endif	// ~#if	KGL_DEBUG_PROFILE

		protected:
			TKGLPtr<IGameInfo>				m_pGameInfo;			//!< ゲーム情報管理用(ゲーム側で実装)
			TKGLPtr<Window::IWindow>		m_pWindow;				//!< ウィンドウ管理
			TKGLPtr<File::IFileReader>		m_pFileReader;			//!< ファイル管理
			TKGLPtr<IResourceManager>		m_pResourceManager;		//!< リソース管理
			TKGLPtr<Scene::ISceneManager>	m_pSceneManager;		//!< シーン管理
			TKGLPtr<IGraphicDriver>			m_pGraphicDriver;		//!< グラフィックドライバー
			TKGLPtr<Audio::IAudioManager>	m_pAudioManager;		//!< オーディオ管理
			TKGLPtr<Frame::IFrameControl>	m_pFrameControl;		//!< フレーム制御
#if	KGL_DEBUG_MENU
			Debug::CDebugMenu*				m_pDebugMenu;			//!< デバッグメニュー
#endif	// ~#if	KGL_DEBUG_MENU
#if	KGL_DEBUG_PROFILE
			TKGLPtr<Frame::IFPSCounter>		m_pFPSCounter;			//!< FPSカウンタ(Update)
			TKGLPtr<Frame::IFPSCounter>		m_pRenderFPSCounter;	//!< FPSカウンタ(Draw)
#endif	// ~#if	KGL_DEBUG_PROFILE
		};

		//!	ゲーム管理の取得
		//!	@return ゲーム管理
		IGameManager* GameManager(void);
	}
}

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define	KGLGameManager()		kgl::Manager::GameManager()
#define	KGLSceneManager()		KGLGameManager()->SceneManager()
#define	KGLFileReader()			KGLGameManager()->FileReader()
#define	KGLResourceManager()	KGLGameManager()->ResourceManager()
#define	KGLAudioManager()		KGLGameManager()->AudioManager()

#if	KGL_DEBUG_MENU
#define	KGLDebugMenu()			KGLGameManager()->DebugMenu()
#endif	// ~#if	KGL_DEBUG_MENU

#endif	// __KGL_GAME_MANAGER_H__
//======================================================================
//	END OF FILE
//======================================================================
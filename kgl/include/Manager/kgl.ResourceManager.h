//----------------------------------------------------------------------
//!
//!	@file	kgl.ResourceManager.h
//!	@brief	リソース管理関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_RESOURCE_MANAGER_H__
#define	__KGL_RESOURCE_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../kgl.h"
#include "Render/Shader/kgl.Shader.h"
#include "Render/Texture/kgl.Texture.h"
#include "Render/Font/kgl.Font.h"
#include "Audio/kgl.Sound.h"
#include "Data/xml/kgl.xml.h"

//	リソースの取得用マクロ
#define	DECLARE_GETLOADED_FUNCTION(_class_, _function_)		\
	virtual _class_* _function_(const c8* pPath) = 0;		\
	virtual _class_* _function_(s32 iIndex) = 0;

namespace kgl
{
	namespace Manager
	{
		namespace EResource
		{
			enum
			{
				Unknown = -1,		//!< 不明なデータ(指定された場合は拡張子で判定を行う)

				VertexShader = 0,	//!< 頂点シェーダー(*.pvs)
				PixelShader,		//!< ピクセルシェーダー(*.pps)
				Texture,			//!< テクスチャー(*.dds)
				Font,				//!< フォント(*.ktf)
				Sound,				//!< フォント(*.wav)

				Xml,				//!< XML(*.xml)

				Custom = 256,		// Custom以降はゲーム側で使用可能
			};
		}

		//!	リソース生成用コールバック
		typedef File::IExtraObject* (*FCreateResource)(const void* pData, u32 uiSize, b8& bDeleteData);

		//======================================================================
		//!	リソース管理インターフェイス
		//======================================================================
		interface IResourceManager
			: public IKGLBase
		{
		public:
			//!	読み込み
			//!	@param pPath		[in] パス
			//!	@param iResource	[in] リソースの種類(EResource参照)
			//!	@return 結果
			virtual b8 Load(const c8* pPath, s32 iResource = EResource::Unknown) = 0;
			//!	読み込み
			//!	@param iIndex		[in] ファイル番号
			//!	@param iResource	[in] リソースの種類(EResource参照)
			//!	@return 結果
			virtual b8 Load(s32 iIndex, s32 iResource = EResource::Unknown) = 0;

			//!	読み込み(同期読み)
			//!	@param pPath		[in] パス
			//!	@param iResource	[in] リソースの種類(EResource参照)
			//!	@return 結果
			virtual b8 LoadBlock(const c8* pPath, s32 iResource = EResource::Unknown) = 0;
			//!	読み込み
			//!	@param iIndex		[in] ファイル番号
			//!	@param iResource	[in] リソースの種類(EResource参照)
			//!	@return 結果
			virtual b8 LoadBlock(s32 iIndex, s32 iResource = EResource::Unknown) = 0;

			//!	解放
			//!	@param pPath	[in] パス
			virtual void Unload(const c8* pPath) = 0;
			//!	解放
			//!	@param iIndex	[in] ファイル番号
			virtual void Unload(s32 iIndex) = 0;

		public:
			//!	読み込み中か？
			//!	@return 結果
			virtual b8 IsLoading(void) = 0;

		public:
			//!	リソースのバインド
			//!	@param pExtension	[in] 拡張子
			//!	@param iResource	[in] リソースID
			//!	@param CreateFunc	[in] リソース生成関数
			virtual void BindResource(const c8* pExtension, s32 iResource, FCreateResource CreateFunc) = 0;

		public:
			//!	ロード済みのオブジェクトを取得
			DECLARE_GETLOADED_FUNCTION(File::IExtraObject, GetLoadedObject);

		public:
			//!	頂点シェーダーの取得
			DECLARE_GETLOADED_FUNCTION(Render::IVertexShader, GetVertexShader);
			//!	ピクセルシェーダーの取得
			DECLARE_GETLOADED_FUNCTION(Render::IPixelShader, GetPixelShader);
			//!	テクスチャーの取得
			DECLARE_GETLOADED_FUNCTION(Render::ITexture, GetTexture);
			//!	フォントの取得
			DECLARE_GETLOADED_FUNCTION(Font::IFont, GetFont);
			//!	サウンドの取得
			DECLARE_GETLOADED_FUNCTION(Audio::ISound, GetSound);
			//!	XMLの取得
			DECLARE_GETLOADED_FUNCTION(Xml::IXml, GetXml);
		};
	}
}

#endif	// __KGL_RESOURCE_MANAGER_H__
//======================================================================
//	END OF FILE
//======================================================================
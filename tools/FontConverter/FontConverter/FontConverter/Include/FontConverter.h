//----------------------------------------------------------------------
//!
//!	@file	FontConverter.h
//!	@brief	文字コンバーター
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__FONT_CONVERTER_H__
#define	__FONT_CONVERTER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Data/Render/kgl.FontData.h"
#include "Data/Render/kgl.TextureData.h"


namespace FontConverter
{
	//	バージョン情報
	const s16 MajorVersion	= 1;
	const s16 MinorVersion	= 0;

	//	コンバート用の作業バッファサイズ
	const s32 DefaultWorkBuffer = 1024;
	//	フォントが見つからなかった際のダミー用文字
	const c16 UnknownChar = 0x7F;

	//======================================================================
	//!	フォントのコンバート情報
	//======================================================================
	struct FontConvertInfo
	{
		c8			cFont[64];				//!< フォント
		s32			iTextureWidth;			//!< テクスチャーの横幅
		s32			iTextureHeight;			//!< テクスチャーの高さ
		s32			iCharMarginX;			//!< 幅の余白(テクスチャーの文字間)
		s32			iCharMarginY;			//!< 高さの余白(テクスチャーの文字間)
		s32			iCharPaddingX;			//!< 幅の余白(描画時の文字間)
		s32			iCharPaddingY;			//!< 高さの余白(描画時の文字間)
		s32			iCharSet;				//!< 文字コードの種類
		s32			iHeight;				//!< 高さ
		BITFIELD	bDefaultChars:1;		//!< 基本的な英数字や記号を含めるか？
		BITFIELD	bBold:1;				//!< 太字か？
		BITFIELD	bItalic:1;				//!< 斜体か？
		BITFIELD	bLightShadow:1;			//!< 軽量の影にするか？
		BITFIELD	bShadow:1;				//!< 影を落とすか？
		BITFIELD	bAlphaOutline:1;		//!< 外枠を付けるか？
		BITFIELD	bDistanceField:1;		//!< ディスタンスフィールドか？
		c8			cCharacterText[256];	//!< 生成する文字のリストが記述されたテキストへのパス
		c8			cOutputPath[256];		//!< 出力パス
	};
	//======================================================================
	//!	フォントのプロジェクト情報
	//======================================================================
	struct FontProjectInfo
	{
		s32				iVersion;		//!< バージョン
		c8				cUserName[64];	//!< コンバートユーザー名
		FontConvertInfo	ConvertInfo;	//!< コンバート情報
	};

	//======================================================================
	//!	テクスチャー情報
	//======================================================================
	struct FontTexture
	{
		TSmartPtr<u8>	pBuffer;
		s32					iBufferSize;
		s32					iFormat;
		s32					iStride;
		s32					iWidth;
		s32					iHeight;
	};
	//======================================================================
	//!	文字情報
	//======================================================================
	struct FontCharacter
	{
		c16	sChar;			//!< 文字コード
		s16	sTextureIndex;	//!< テクスチャーのインデックス
		s32	iX;				//!< X座標
		s32	iY;				//!< Y座標
		s32	iWidth;			//!< 横幅
		s32	iHeight;		//!< 縦幅
	};

	//======================================================================
	//!	フォントのコンバート情報
	//======================================================================
	class CFontConverter
	{
	public:
		//!	コンストラクタ
		CFontConverter(void);
		//!	初期化
		b8 Initialize(const c8* pPath);
		//!	解放
		void Finalize(void);

	public:
		//!	コンバート
		b8 Convert(void);

	private:
		//!	コンバート用のバッファ等を初期化
		b8 InitializeConvert(void);
		//!	テクスチャー生成
		void CreateTexture(s32 iWidth, s32 iHeight);
		//!	テクスチャーの構築
		void BuildTexture(FontTexture& Texture);

		//!	文字の書き込み
		b8 WriteCharacter(s16 sIndex, c16 sChar, s32& iX, s32& iY);
		//!	文字のサイズ取得
		void GetCharacterSize(const c16* pChar, s32& iOffsetX, s32& iOffsetY, s32& iWidth, s32& iHeight);
		//!	文字の書き込み
		void WriteCharacter(const c16* pChar, s32 iX, s32 iY, s32 iPadX, s32 iPadY, s32 iWidth, s32 iHeight);
		//!	文字の取得
		const c16* GetChar(c16 sChar);

		//!	フォントデータの出力
		void WriteFontData(void);

	private:
		HDC								m_hDC;
		HBITMAP							m_hBitmap;
		TEXTMETRIC						m_tm;
		HFONT							m_hFont;
		FontConvertInfo					m_FontConvertInfo;
		s32								m_iFormat;
		vector<c16>						m_cCharacters;
		vector<FontTexture>				m_FontTextures;
		vector<FontCharacter>			m_FontCharacters;
	};
}

#endif	// __FONT_CONVERTER_H__
//======================================================================
//	END OF FILE
//======================================================================
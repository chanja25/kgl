//----------------------------------------------------------------------
//!
//!	@file	main.cpp
//!	@brief	メイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "FontConverter.h"

//----------------------------------------------------------------------
// library
//----------------------------------------------------------------------
#if	defined(_DEBUG)
	#pragma comment(lib, "kgld.lib")
#else	// ~#if defined(_DEBUG)
	#pragma comment(lib, "kgl.lib")
#endif	// ~#if defined(_DEBUG)

//----------------------------------------------------------------------
//!	アプリケーションの実行
//!	@return 実行結果
//----------------------------------------------------------------------
s32 AppRun(Utility::ICommandLine* pCommandLine)
{
	u32 uiCount = pCommandLine->GetCount();
	if( uiCount < 2 )
	{
#if	CONVERT_TEST
		FontConverter::CFontConverter FontConverter;
		if( FontConverter.Initialize(null) )
		{
			FontConverter.Convert();
		}
		FontConverter.Finalize();
#endif	// ~#if	CONVERT_TEST
		return 1;
	}

	for( u32 i=1; i < uiCount; i++)
	{
		const c8* pStr = pCommandLine->GetCommand(i);
		FontConverter::CFontConverter FontConverter;

		if( FontConverter.Initialize(pStr) )
		{
			if( FontConverter.Convert() )
			{
				printf("[%s]のコンバートが完了しました\n", pStr);
			}
			else
			{
				printf("[%s]のコンバートに失敗しました\n", pStr);
			}
		}
		FontConverter.Finalize();
	}

	return 0;
}

IMPLEMENT_ENTRYPOINT;

//======================================================================
//	END OF FILE
//======================================================================

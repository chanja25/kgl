//----------------------------------------------------------------------
//!
//!	@file	FontConverter.cpp
//!	@brief	文字コンバーター
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "FontConverter.h"
#include "File/kgl.File.h"
#include "File/kgl.Directory.h"
#include "Render/Utility/kgl.Image.h"

namespace FontConverter
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CFontConverter::CFontConverter(void)
		: m_hDC(null)
		, m_hBitmap(null)
		, m_hFont(null)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CFontConverter::Initialize(const c8* pPath)
	{
		b8 bSuccess = false;
		TKGLPtr<File::IFile> pFile;
		File::Create(pFile.GetReference());

		if( pPath )
		{
			if( pFile->Open(pPath) )
			{
				FontProjectInfo ProjectInfo;
				pFile->Read(&ProjectInfo, sizeof(ProjectInfo));
				pFile->Close();
				bSuccess = true;

				m_FontConvertInfo = ProjectInfo.ConvertInfo;
			}
		}
#if	CONVERT_TEST
		else
		{
			//	テスト処理
			//kgStrcpy(m_FontConvertInfo.cFont, "IWA新隷書Plus");
			//kgStrcpy(m_FontConvertInfo.cFont, "Meiryo UI");
			//kgStrcpy(m_FontConvertInfo.cFont, "HG行書体");
			kgStrcpy(m_FontConvertInfo.cFont, "ＭＳ ゴシック");
			m_FontConvertInfo.iTextureWidth  = 512;
			m_FontConvertInfo.iTextureHeight = 512;
			m_FontConvertInfo.iCharMarginX = 0;
			m_FontConvertInfo.iCharMarginY = 0;
			m_FontConvertInfo.iCharPaddingX = 0;
			m_FontConvertInfo.iCharPaddingY = 0;
			m_FontConvertInfo.iCharSet = Font::ECharSet::ShiftJIS;
			//m_FontConvertInfo.iCharSet = Font::ECharSet::UTF16;
			m_FontConvertInfo.iHeight = 32;
			m_FontConvertInfo.bDefaultChars = true;
			m_FontConvertInfo.bBold = false;
			m_FontConvertInfo.bItalic = false;
			m_FontConvertInfo.bLightShadow = false;
			m_FontConvertInfo.bShadow = false;
			m_FontConvertInfo.bAlphaOutline = false;
			m_FontConvertInfo.bDistanceField = false;
			kgStrcpy(m_FontConvertInfo.cCharacterText, "Characters.txt");
			kgStrcpy(m_FontConvertInfo.cOutputPath, "test/test.ktf");
			bSuccess = true;
		}
#endif	// ~#if	CONVERT_TEST

		//	適当なサイズのバッファを作成しておく
		m_cCharacters.reserve(DefaultWorkBuffer);
		m_cCharacters.clear();
		if( bSuccess )
		{
			if( m_FontConvertInfo.bDefaultChars )
			{
				//	基本的な英数字等を追加
				for( c16 c=0x20; c < 0x80; c ++ )
				{
					m_cCharacters.push_back(c);
				}
			}
			//	未作成文字が描画された時のダミーとしていれておく
			m_cCharacters.push_back(UnknownChar);

			vector<c8> cCharacters;
			cCharacters.reserve(m_cCharacters.capacity());

			//	外部ファイルから使用する文字を参照
			if( m_FontConvertInfo.cCharacterText[0] != '\0' )
			{
				if( pFile->Open(m_FontConvertInfo.cCharacterText) )
				{
					c8 cChar;
					s32 iCharSet = Font::ECharSet::UTF16;
					if( pFile->Read(&cChar, sizeof(c8)) > 0 )
					{
						if( cChar == (c8)0xFF )
						{
							pFile->Read(&cChar, sizeof(c8));
							if( cChar == (c8)0xFE )
							{
								iCharSet = Font::ECharSet::UTF16;
							}
						}
						else
						{
							iCharSet = Font::ECharSet::ShiftJIS;
							cCharacters.push_back(cChar);
						}
					}

					while( pFile->Read(&cChar, sizeof(c8)) > 0 )
					{
						cCharacters.push_back(cChar);
					}
					pFile->Close();

					if( iCharSet == Font::ECharSet::ShiftJIS )
					{
						u32 uiSize = sizeof(c8) * cCharacters.size() * 2;
						TSmartPtr<c8> pBuffer = new c8[uiSize];

						CharSet::ConvertShiftJISToUTF16(pBuffer, &cCharacters[0], &uiSize);
						cCharacters.clear();
						cCharacters.reserve(uiSize);
						for( u32 i=0; i < uiSize; i++ )
						{
							cCharacters.push_back(pBuffer[i]);
						}
					}
				}

				for( u32 i = 0; i < cCharacters.size(); i++ )
				{
					c16 sChar;
					sChar = (c16)cCharacters[i] & 0x00FF;
					sChar |= ((c16)cCharacters[++ i] << 8) & 0xFF00;

					//	有効な値でかつ既に登録済みでなければ追加する
					if( sChar >= 0x0080 &&
						linear_search(sChar, &m_cCharacters[0], m_cCharacters.size()) == NotFind )
					{
						m_cCharacters.push_back(sChar);
					}
				}
			}

			//	文字コード順にならべる
			qsort(&m_cCharacters[0], m_cCharacters.size());

			bSuccess = InitializeConvert();
		}

		return bSuccess;
	}
	//----------------------------------------------------------------------
	//	解放
	//----------------------------------------------------------------------
	void CFontConverter::Finalize(void)
	{
		if( m_hFont != null )
		{
			DeleteObject(m_hFont);
			m_hFont = null;
		}
		if( m_hBitmap != null )
		{
			DeleteObject(m_hBitmap);
			m_hBitmap = null;
		}
		if( m_hDC != null )
		{
			DeleteDC(m_hDC);
			m_hDC = null;
		}
	}
	//----------------------------------------------------------------------
	//	コンバート
	//----------------------------------------------------------------------
	b8 CFontConverter::Convert(void)
	{
		s16 sPage;
		s32 iX, iY, iWidth, iHeight;

		sPage = 0;
		iX = iY = iWidth = iHeight = 0;
		//	バッファをクリアする
		HBRUSH Black = CreateSolidBrush(0x00000000);
		RECT rect = {0, 0, m_FontConvertInfo.iTextureWidth, m_FontConvertInfo.iTextureHeight};
		FillRect(m_hDC, &rect, Black);

		b8 bCreateTexture = false;
		//	文字の書き込みとテクスチャーの生成
		for( u32 i=0; i < m_cCharacters.size(); i++ )
		{
			const c16& sChar = m_cCharacters[i];

			if( !WriteCharacter(sPage, sChar, iX, iY) )
			{
				bCreateTexture = false;
				CreateTexture(iWidth, iHeight);
				sPage ++;
				i --;
				iX = iY = 0;
				iWidth = iHeight = 0;

				//	バッファをクリアする
				FillRect(m_hDC, &rect, Black);
			}
			else
			{
				iWidth  = Max<s32>(iWidth,  iX);
				iHeight = Max<s32>(iHeight, iY + m_tm.tmHeight);
				bCreateTexture = true;
			}
		}
		//	残りの文字のテクスチャー生成
		if( bCreateTexture )
		{
			CreateTexture(iWidth, iHeight);
		}

		//	フォントデータの出力
		WriteFontData();

		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	b8 CFontConverter::InitializeConvert(void)
	{
		HDC hDC = GetDC(null);

		if( hDC == null )
		{
			return false;
		}

		m_hDC = CreateCompatibleDC(hDC);
		if( m_hDC == null )
		{
			return false;
		}
		BITMAPINFO bi;
		bi.bmiHeader.biBitCount = 32;
		bi.bmiHeader.biCompression = BI_RGB;
		bi.bmiHeader.biPlanes = 1;
		bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
		bi.bmiHeader.biWidth = m_FontConvertInfo.iTextureWidth;
		bi.bmiHeader.biHeight = -m_FontConvertInfo.iTextureHeight;

		m_hBitmap = CreateDIBSection(m_hDC, &bi, DIB_RGB_COLORS, 0, 0, 0);
		if( m_hBitmap == null )
		{
			return false;
		}

		m_hFont = CreateFont(
					m_FontConvertInfo.iHeight,
					0,
					0,
					0,
					m_FontConvertInfo.bBold? FW_BOLD: FW_NORMAL,
					m_FontConvertInfo.bItalic,
					false,
					false,
					DEFAULT_CHARSET,
					OUT_DEFAULT_PRECIS,
					CLIP_DEFAULT_PRECIS,
					ANTIALIASED_QUALITY,
					VARIABLE_PITCH,
					m_FontConvertInfo.cFont
					);

		if( m_hFont == NULL ) 
		{
			return false;
		}
		//	書き込み用のバッファを指定しておく
		SelectObject(m_hDC, m_hBitmap);
		SelectObject(m_hDC, m_hFont);

		GetTextMetrics(m_hDC, &m_tm);

		m_FontTextures.clear();

		m_FontCharacters.reserve(m_cCharacters.size());
		m_FontCharacters.clear();

		//	テクスチャーのフォーマットを取得
		s32 iFormat = Font::EFormat::Alpha;
		if( m_FontConvertInfo.bDistanceField )
		{
			iFormat = Font::EFormat::DistanceField;
		}
		else if( m_FontConvertInfo.bAlphaOutline )
		{
			iFormat = Font::EFormat::AlphaOutline;
		}
		else if( m_FontConvertInfo.bShadow )
		{
			iFormat = Font::EFormat::AlphaShadow;
		}
		m_iFormat = iFormat;
		return true;
	}

	//----------------------------------------------------------------------
	//	テクスチャー生成
	//----------------------------------------------------------------------
	void CFontConverter::CreateTexture(s32 iWidth, s32 iHeight)
	{
		s32 iStride = 1;
		switch( m_iFormat )
		{
		case Font::EFormat::Alpha:			iStride = 1;	break;
		case Font::EFormat::AlphaOutline:	iStride = 2;	break;
		case Font::EFormat::AlphaShadow:	iStride = 2;	break;
		case Font::EFormat::DistanceField:	iStride = 1;	break;
		}

		//	テクスチャーサイズを2のべき乗にする
		if( Math::CountBit((u32)iWidth) > 1 )
		{
			iWidth  = (s32)(1 << (Math::MSB((u32)iWidth) + 1));
		}
		if( Math::CountBit((u32)iHeight) > 1 )
		{
			iHeight = (s32)(1 << (Math::MSB((u32)iHeight) + 1));
		}

		FontTexture Texture;
		//	テクスチャーのフォーマット設定とバッファの確保
		Texture.pBuffer		= new u8[iWidth * iHeight * iStride];
		Texture.iBufferSize	= iWidth * iHeight * iStride;
		Texture.iFormat		= m_iFormat;
		Texture.iStride		= iStride;
		Texture.iWidth		= iWidth;
		Texture.iHeight		= iHeight;
		//	テクスチャーの構築
		BuildTexture(Texture);

		m_FontTextures.push_back(Texture);
	}

	//----------------------------------------------------------------------
	//	テクスチャーの構築
	//----------------------------------------------------------------------
	void CFontConverter::BuildTexture(FontTexture& Texture)
	{
		BITMAPINFO bi;
		bi.bmiHeader.biBitCount = 32;
		bi.bmiHeader.biCompression = BI_RGB;
		bi.bmiHeader.biPlanes = 1;
		bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
		bi.bmiHeader.biWidth = Texture.iWidth;
		bi.bmiHeader.biHeight = -Texture.iHeight;

		TSmartPtr<s32> pBits = new s32[Texture.iWidth * Texture.iHeight];
		//	書き込んだ文字のビット情報を取得
		GetDIBits(m_hDC, m_hBitmap, 0, Texture.iHeight, pBits, &bi, DIB_RGB_COLORS);

		for( s32 x=0; x < Texture.iWidth; x++ )
		{
			for( s32 y=0; y < Texture.iHeight; y++ )
			{
				f32 fAlpha;
				//	(x,y)座標のビット情報を取得
				u8 ucAlpha = (u8)(pBits[x + y * Texture.iWidth] & 0xFF);

				//	出力用のバッファを取得
				u8* pBuffer = &Texture.pBuffer[Texture.iStride * (x + y * Texture.iWidth)];
				switch( Texture.iFormat )
				{
				case Font::EFormat::Alpha:
					pBuffer[0] = ucAlpha;
					break;

				case Font::EFormat::AlphaOutline:
				case Font::EFormat::AlphaShadow:
					pBuffer[0] = ucAlpha;
					pBuffer[1] = ucAlpha;
					break;
					fAlpha = 0.0f;
					//	アウトライン(影)の算出
					if( ucAlpha < 0xFF )
					{
						s32 iOutlineRadius2 = Max(Align(m_FontConvertInfo.iHeight, 8) / 16, 2);
						s32 iOutlineRadius = iOutlineRadius2 / 2;
						s32 iStartX, iStartY, iEndX, iEndY;

						if( Texture.iFormat == Font::EFormat::AlphaOutline )
						{
							iStartX = iStartY = -iOutlineRadius2;
							iEndX = iEndY = iOutlineRadius2;
						}
						else
						{
							iStartX = iStartY = -iOutlineRadius2;
							iEndX = iEndY = 0;
						}

						s32 iDist = iOutlineRadius2 + 1;
						for( s32 xx=iStartX; xx <= iEndX; xx++ )
						{
							for( s32 yy=iStartY; yy <= iEndY; yy++ )
							{
								if( x + xx < 0 || x + xx >= Texture.iWidth )continue;
								if( y + yy < 0 || y + yy >= Texture.iHeight )continue;

								if( (pBits[(x + xx) + (y + yy) * Texture.iWidth] & 0xFF) >= 0x8F )
								{
									iDist = Min(Abs(xx) + Abs(yy), iDist);
								}
							}
						}
						if( iDist <= iOutlineRadius2 )
						{
							if( iDist >= iOutlineRadius )
							{
								//	文字から遠ければ
								fAlpha = (f32)(iOutlineRadius2-iDist+1) / (f32)(iOutlineRadius2);
								ucAlpha = 0;
							}
						}
					}
					pBuffer[0] = ucAlpha;
					pBuffer[1] = (u8)(fAlpha * 255.0f);

					pBits[x + y * Texture.iWidth] = (ucAlpha & 0xFF) | (((u8)(fAlpha * 255.0f) & 0xFF) << 8);
					break;

				case Font::EFormat::DistanceField:
					fAlpha = 0.0f;
					//	アウトライン(影)の算出
					if( ucAlpha < 0xFF )
					{
						s32 iOutlineRadius2 = 12;
						s32 iOutlineRadius = iOutlineRadius2 / 2;
						s32 iStartX, iStartY, iEndX, iEndY;

						iStartX = iStartY = -iOutlineRadius2;
						iEndX = iEndY = iOutlineRadius2;

						s32 iDist = iOutlineRadius2 + 1;
						for( s32 xx=iStartX; xx <= iEndX; xx++ )
						{
							for( s32 yy=iStartY; yy <= iEndY; yy++ )
							{
								if( x + xx < 0 || x + xx >= Texture.iWidth )continue;
								if( y + yy < 0 || y + yy >= Texture.iHeight )continue;

								if( (pBits[(x + xx) + (y + yy) * Texture.iWidth] & 0xFF) >= 0x8F )
								{
									iDist = Min(Abs(xx) + Abs(yy), iDist);
								}
							}
						}
						if( iDist <= iOutlineRadius2 )
						{
							if( iDist > iOutlineRadius )
							{
								//	文字から遠ければ
								fAlpha = (f32)(iOutlineRadius2-iDist+1) / (f32)(iOutlineRadius2);
							}
							else
							{
								fAlpha = 1.0f;
							}
							fAlpha *= 1.0f - ((f32)ucAlpha / 255.0f);
						}
					}
					pBuffer[0] = ucAlpha;
					pBuffer[1] = (u8)(fAlpha * 255.0f);
					break;
				}
			}
		}
#if	CONVERT_TEST
		//	BMPの出力
		{
			for( s32 x=0; x < Texture.iWidth; x++ )
			{
				for( s32 y=0; y < Texture.iHeight; y++ )
				{
					//	(x,y)座標のビット情報を取得
					s32* pBmpBits = &pBits[x + y * Texture.iWidth];
					//	出力用のバッファを取得
					u8* pBuffer = &Texture.pBuffer[Texture.iStride * (x + y * Texture.iWidth)];
					switch( Texture.iFormat )
					{
					case Font::EFormat::Alpha:
					case Font::EFormat::DistanceField:
						pBmpBits[0] = pBuffer[0];
						break;
					case Font::EFormat::AlphaOutline:
					case Font::EFormat::AlphaShadow:
						pBmpBits[0] = pBuffer[0] | (pBuffer[1] << 8);
						break;
					}
				}
			}
			Directory::Create("test");
			c8 cPath[32];
			CharSet::Format2(cPath, "test/test%02d.bmp", m_FontTextures.size());
			Image::OutputImageToBitmap(cPath, pBits, Texture.iWidth, Texture.iHeight, Image::EColorType::ARGB);
		}
#endif	// ~#if	CONVERT_TEST
	}

	//----------------------------------------------------------------------
	//	文字の書き込み
	//----------------------------------------------------------------------
	b8 CFontConverter::WriteCharacter(s16 sIndex, c16 sChar, s32& iX, s32& iY)
	{
		const c16* pChar = GetChar(sChar);

		s32 iWidth, iHeight;
		iWidth  = m_FontConvertInfo.iTextureWidth;
		iHeight = m_FontConvertInfo.iTextureHeight;

		//	出力するフォントの設定
		SelectObject(m_hDC, m_hFont);
		SetBkColor(m_hDC, 0x00000000);
		SetTextColor(m_hDC, 0x00ff0000);

		s32 iCharOffsetX, iCharOffsetY, iCharWidth, iCharHeight;
		//	文字サイズの取得
		GetCharacterSize(pChar, iCharOffsetX, iCharOffsetY, iCharWidth, iCharHeight);
		if( iCharWidth <= 0 || iCharHeight <= 0 )
		{
			return true;
		}

		s32 iPaddingX = m_FontConvertInfo.iCharMarginX;
		s32 iPaddingY = m_FontConvertInfo.iCharMarginY;
		//	フォーマットによっては余白を広げておく
		switch( m_iFormat )
		{
		case Font::EFormat::AlphaOutline:
			iPaddingX = Max(iPaddingX, Max(2, Align(m_FontConvertInfo.iHeight, 8) / 16));
			iPaddingY = Max(iPaddingY, Max(2, Align(m_FontConvertInfo.iHeight, 8) / 16));
			break;
		case Font::EFormat::AlphaShadow:
			iPaddingX = Max(iPaddingX, Max(1, (Align(m_FontConvertInfo.iHeight, 8) / 16) / 2));
			iPaddingY = Max(iPaddingY, Max(1, (Align(m_FontConvertInfo.iHeight, 8) / 16) / 2));
			break;
		case Font::EFormat::DistanceField:
			iPaddingX = Max(iPaddingX, 16);
			iPaddingY = Max(iPaddingY, 16);
			break;
		}

		s32 iDrawX = iX;
		s32 iDrawY = iY;
		s32 iDrawEndX = iDrawX + iCharWidth  + iPaddingX * 2;
		s32 iDrawEndY = iDrawY + iCharHeight + iPaddingY * 2;

		//	テクスチャーの端まで来ていれば改行する
		if( iWidth < iDrawEndX )
		{
			iDrawX = iX = 0;
			iDrawY = iY = iDrawEndY;
			iDrawEndX = iDrawX + iCharWidth  + iPaddingX * 2;
			iDrawEndY = iDrawY + iCharHeight + iPaddingY * 2;
			//	書き込む領域が無ければ次のテクスチャーを使用する
			if( iHeight < iDrawEndY )
			{
				return false;
			}
		}

		//	文字の書き込み
		WriteCharacter(pChar, iDrawX, iDrawY, iPaddingX + iCharOffsetX, iPaddingY + iCharOffsetY, iDrawEndX - iDrawX, iDrawEndY - iDrawY);

		//	文字情報の追加
		FontCharacter FontChar;
		FontChar.sChar			= sChar;
		FontChar.sTextureIndex	= sIndex;
		FontChar.iX				= iX;
		FontChar.iY				= iY;
		FontChar.iWidth			= iDrawEndX - iX;
		FontChar.iHeight		= iDrawEndY - iY;

		m_FontCharacters.push_back(FontChar);

		iX = iDrawEndX;
		iY = iDrawY;

		return true;
	}
	//----------------------------------------------------------------------
	//	文字のサイズ取得
	//----------------------------------------------------------------------
	void CFontConverter::GetCharacterSize(const c16* pChar, s32& iOffsetX, s32& iOffsetY, s32& iWidth, s32& iHeight)
	{
		SIZE Size = {0, 0};
		//	文字のサイズの取得
		GetTextExtentPoint32W(m_hDC, pChar, 1, &Size);

		iOffsetX = 0;
		iOffsetY = 0;
		iWidth	= Size.cx;
		iHeight	= Size.cy;
	}
	//----------------------------------------------------------------------
	//	文字の書き込み
	//----------------------------------------------------------------------
	void CFontConverter::WriteCharacter(const c16* pChar, s32 iX, s32 iY, s32 iPadX, s32 iPadY, s32 iWidth, s32 iHeight)
	{
		RECT rect =
		{
			iX + iPadX,
			iY + iPadY,
			iX + iWidth,
			iY + iHeight,
		};
		//	バッファに対して文字を書き込む
		DrawTextW(m_hDC, pChar, 1, &rect, DT_NOPREFIX);
	}
	//----------------------------------------------------------------------
	//	文字の取得
	//----------------------------------------------------------------------
	const c16* CFontConverter::GetChar(c16 sChar)
	{
		static c16 cTemp[2];
		cTemp[0] = sChar;
		cTemp[1] = 0;
		return cTemp;
	}

	//----------------------------------------------------------------------
	//	コンバート情報の比較(<)
	//----------------------------------------------------------------------
	b8 LessFontConvert(const Font::kgFontConvert& p1, const Font::kgFontConvert& p2)
	{
		return p1.sChar < p2.sChar;
	}
	//----------------------------------------------------------------------
	//	フォントデータの出力
	//----------------------------------------------------------------------
	void CFontConverter::WriteFontData(void)
	{
		//	文字が見つからない際のダミーを末尾に設定(0x79は必ず作成される)
		u32 uiIndex = binary_search(UnknownChar, &m_cCharacters[0], m_cCharacters.size());
		m_FontCharacters.push_back(m_FontCharacters[uiIndex]);

		vector<Font::kgFontCharacter> FontCharacters;
		FontCharacters.reserve(m_FontCharacters.size());
		for( u32 i=0; i < m_FontCharacters.size(); i++ )
		{
			const FontCharacter& CharInfo = m_FontCharacters[i];
			const FontTexture& TextureInfo = m_FontTextures[CharInfo.sTextureIndex];

			//	文字情報の追加
			Font::kgFontCharacter FontChar;
			FontChar.sChar = CharInfo.sChar;
			FontChar.sTextureIndex	= CharInfo.sTextureIndex;
			FontChar.usU = (u16)CharInfo.iX;
			FontChar.usV = (u16)CharInfo.iY;
			FontChar.usSizeU = (u16)CharInfo.iWidth;
			FontChar.usSizeV = (u16)CharInfo.iHeight;

			FontCharacters.push_back(FontChar);
		}

		TKGLPtr<File::IFile> pFile;
		File::Create(pFile.GetReference());

		if( pFile->Open(m_FontConvertInfo.cOutputPath, File::EOpen::Write, File::EShare::Read, File::ECreate::CreateAlways) )
		{
			Font::kgFontHeader FontHeader;

			s32 iCharOffset, iTextureOffset, iConvertOffset;

			iCharOffset = sizeof(Font::kgFontHeader);

			vector<Font::kgFontConvert> ConvertList;
			if( m_FontConvertInfo.iCharSet == Font::ECharSet::ShiftJIS )
			{
				u32 uiSize = sizeof(c8) * m_cCharacters.size() * 2;
				TSmartPtr<c8> pBuffer = new c8[uiSize];

				CharSet::ConvertUTF16ToShiftJIS(&pBuffer[0], &m_cCharacters[0], &uiSize);

				ConvertList.reserve(m_cCharacters.size());

				s16 sIndex = 0;
				for( u32 i=0; i < uiSize; i++ )
				{
					Font::kgFontConvert FontConvert;

					c16 sChar;
					sChar = (c16)pBuffer[i] & 0x00FF;
					if( CharSet::IsMultiByteChar(pBuffer[i]) )
					{
						sChar |= ((c16)pBuffer[++ i] << 8) & 0xFF00;
					}
					FontConvert.sChar	= sChar;
					FontConvert.sIndex	= sIndex ++;

					ConvertList.push_back(FontConvert);
				}
				qsort(&ConvertList[0], ConvertList.size(), LessFontConvert);

				iConvertOffset = iCharOffset + sizeof(Font::kgFontCharacter) * FontCharacters.size();
				iTextureOffset = iConvertOffset + sizeof(Font::kgFontConvert) * FontCharacters.size();
			}
			else
			{
				iConvertOffset = 0;
				iTextureOffset = iCharOffset + sizeof(Font::kgFontCharacter) * FontCharacters.size();
			}
			FontHeader.iID				= FOURCC('k', 't', 'f', ' ');
			FontHeader.iVersion			= kgVersion(MajorVersion, MinorVersion);
			FontHeader.iCharSet			= m_FontConvertInfo.iCharSet;
			FontHeader.iCharCount		= FontCharacters.size();
			FontHeader.iCharOffset		= iCharOffset;
			FontHeader.iConvertOffset	= iConvertOffset;
			FontHeader.iCharPaddingX	= m_FontConvertInfo.iCharPaddingX;
			FontHeader.iCharPaddingY	= m_FontConvertInfo.iCharPaddingY;
			FontHeader.iFormat			= m_iFormat;
			FontHeader.iTextureCount	= m_FontTextures.size();
			FontHeader.iTextureOffset	= iTextureOffset;

			//	ヘッダーの書き込み
			pFile->Write(&FontHeader, sizeof(Font::kgFontHeader));

			pFile->Seek(FontHeader.iCharOffset);
			//	文字情報の書き込み
			pFile->Write(&FontCharacters[0], sizeof(Font::kgFontCharacter) * FontCharacters.size());

			if( ConvertList.size() > 0 )
			{
				pFile->Seek(FontHeader.iConvertOffset);
				//	コンバート情報の書き込み
				pFile->Write(&ConvertList[0], sizeof(Font::kgFontConvert) * ConvertList.size());
			}

			pFile->Seek(FontHeader.iTextureOffset);

			vector<Font::kgFontTexture> Textures;
			Textures.reserve(FontHeader.iTextureCount);

			s32 iOffset = Align<s32>(FontHeader.iTextureOffset + sizeof(Font::kgFontTexture) * FontHeader.iTextureCount, 128);
			//	テクスチャー情報の書き込み
			for( s32 i=0; i < FontHeader.iTextureCount; i++ )
			{
				FontTexture& Texture = m_FontTextures[i];

				Font::kgFontTexture FontTexture;
				FontTexture.iWidth	= Texture.iWidth;
				FontTexture.iHeight	= Texture.iHeight;
				FontTexture.iSize	= Texture.iBufferSize;
				FontTexture.iOffset	= iOffset;

				pFile->Write(&FontTexture, sizeof(Font::kgFontTexture));

				Textures.push_back(FontTexture);

				iOffset = Align(iOffset + FontTexture.iSize, 128);
			}

			//	テクスチャーバッファの書き込み
			for( s32 i=0; i < FontHeader.iTextureCount; i++ )
			{
				FontTexture& Texture = m_FontTextures[i];

				pFile->Seek(Textures[i].iOffset);
				pFile->Write(Texture.pBuffer, Texture.iBufferSize);
			}

			pFile->Close();
		}

		m_FontTextures.clear();
	}
}


//======================================================================
//	END OF FILE
//======================================================================
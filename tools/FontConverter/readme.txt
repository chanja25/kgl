■FontConverter

〇実行ファイル
FontConverter.exe


○使用方法
FontConverter.exeを起動して使用するプロジェクトを指定します。

次にフォントの情報を設定する画面が表示されるので使用するフォントを決定します。

最後にコンバートボタンを押すとフォントの作成が完了します。


〇出力データ
FontConverterではコンバートしたフォント設定や出力パス等の情報を保持するプロジェクトデータ(fntproj)と
実際にゲームで表示するために使用するフォントデータ(ktf)の２つが出力されます。


〇コマンドラインでの制御について
FontConverterはコマンドラインでのコンバートに対応しています。

コンバート方法としてはFontConverter.exeと同階層にbinフォルダが存在するので
そちらの中にあるFontConverter.exeをコマンドラインで起動してください。

引数にフォントプロジェクトのパスを指定する事でコンバートを行います。
引数にパスを複数指定すれば全てを一度にコンバート可能です。


〇フォントデータ(ktf)の構造
・ヘッダーデータ(データの先頭)
struct kgFontHeader
{
	s32	iID;			//!< FOURCC('k', 't', 'f', ' ')
	s32 iVersion;		//!< バージョン
	s32	iCharSet;		//!< 文字コードの種類
	s32	iCharCount;		//!< フォント数
	s32	iCharOffset;	//!< 文字情報へのオフセット
	s32	iConvertOffset;	//!< コンバート情報へのオフセット(0の場合は無し)
	s32 iCharPaddingX;	//!< X軸の余白
	s32 iCharPaddingY;	//!< Y軸の余白
	s32	iFormat;		//!< フォーマット(EFormat参照)
	s32	iTextureCount;	//!< テクスチャー数
	s32	iTextureOffset;	//!< テクスチャーへのオフセット
};

・文字情報(ヘッダーのiCharOffsetの位置からiCharCount個)
struct kgFontCharacter
{
	c16	sChar;			//!< 文字コード
	s16	sTextureIndex;	//!< テクスチャーのインデックス
	u16	usU;			//!< U値
	u16	usV;			//!< V値
	u16	usSizeU;		//!< 横幅
	u16	usSizeV;		//!< 縦幅
};

・文字コードのコンバート情報(ヘッダーのiConvertOffsetの位置からiCharCount個)
※UTF-16の場合でコンバートした場合はありません(iConvertOffsetは-1)
struct kgFontConvert
{
	c16	sChar;	//!< 文字コード(Shift-JIS)
	s16	sIndex;	//!< 文字情報のインデックス
};

・テクスチャー情報(ヘッダーのiTextureOffsetの位置からiTextureCount個)
struct kgFontTexture
{
	s32	iWidth;		//!< 横幅
	s32	iHeight;	//!< 縦幅
	s32	iSize;		//!< データサイズ
	s32	iOffset;	//!< データへのオフセット(データの先頭アドレスを基準とする)
};

・テクスチャーデータ(テクスチャーデータのiOffsetからiSize)
αのみ(8bit)のテクスチャーデータ


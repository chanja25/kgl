//----------------------------------------------------------------------
//!
//!	@file	directory.h
//!	@brief	ディレクトリ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DIRECTORY_H__
#define	__DIRECTORY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <windows.h>
#include "define.h"
#include "typedef.h"

namespace Directory
{
	//!	フォルダー作成
	//!	@param sPath [in] パス
	//!	@return 結果
	b8 Create(const c8* pPath);
	//!	フォルダー削除(ファイルが存在する場合は失敗)
	//!	@param sPath [in] パス
	//!	@return 結果
	b8 Remove(const c8* pPath);

	//!	属性
	namespace EAttribute
	{
		const u32 All		= 0xffffffff;
		const u32 File		= (1 << 0);	//!< ファイル
		const u32 Directory	= (1 << 1);	//!< ディレクトリ
		const u32 Hidden	= (1 << 2);	//!< 隠しファイル
		const u32 ReadOnly	= (1 << 3);	//!< 読み取り専用
		const u32 System	= (1 << 4);	//!< システムファイル
	}

	//======================================================================
	//!	ファイル情報
	//======================================================================
	struct Info
	{
		c8		cName[256];			//!< ファイル名
		u32		uiSize;				//!< ファイルサイズ
		u32		uiAttribute;		//!< 属性
		Date	Create;				//!< 作成日時
		Date	Access;				//!< アクセス日時
		Date	Update;				//!< 更新日時
	};

	//======================================================================
	//!	ディレクトリマネージャーインターフェイス
	//======================================================================
	class CDirectory
	{
	public:
		//!	コンストラクタ
		CDirectory(void);
		//!	デストラクタ
		~CDirectory(void);

		//!	ファイル検索
		//!	@param sPath [in] パス
		//!	@param pInfo [in] ファイル情報
		//!	@param uiAttribute [in] 属性
		//!	@return ファイル名(失敗時はnull)
		const c8* FindFile(const c8* pPath, Info* pInfo = null, u32 uiAttribute = EAttribute::All);
		//!	次のファイル取得
		//!	@param sPath [in] パス
		//!	@param pInfo [out] ファイル情報
		//!	@return ファイル名(失敗時はnull)
		const c8* NextFile(Info* pInfo = null);

	private:
		//!	ファイル情報の設定
		void SetupInfo(const WIN32_FIND_DATA& fd, Info* pInfo, u32 uiAttribute);
		//!	ハンドルを閉じる
		void Close(void);

	private:
		HANDLE	m_hFind;			//!< ファイル検索用ハンドル
		c8		m_cFileName[256];	//!< ファイル名
		u32		m_uiAttribute;		//!< 検索する属性
	};
}

#endif	// __DIRECTORY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
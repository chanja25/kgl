//----------------------------------------------------------------------
//!
//!	@file	typedef.h
//!	@brief	型定義
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__TYPEDEF_H__
#define	__TYPEDEF_H__

//----------------------------------------------------------------------
// typedef
//----------------------------------------------------------------------
typedef unsigned	long	dword;
typedef unsigned	short	word;

typedef signed		char		s8;		//!< 符号あり  8bit整数
typedef unsigned	char		u8;		//!< 符号なし  8bit整数
typedef signed		short		s16;	//!< 符号あり 16bit整数
typedef unsigned	short		u16;	//!< 符号なし 16bit整数
typedef signed		int			s32;	//!< 符号あり 32bit整数
typedef unsigned	int			u32;	//!< 符号なし 32bit整数
typedef signed		__int64		s64;	//!< 符号あり 64bit整数
typedef unsigned	__int64		u64;	//!< 符号なし 64bit整数

typedef u8		b8;			//!< 8bitフラグ
typedef char	c8;			//!< 8bit文字
typedef wchar_t c16;		//!< 16bitワイド文字
typedef u16		f16;		//!< 半精度浮動少数点数
typedef float	f32;		//!< 単精度浮動少数点数
typedef double	f64;		//!< 倍精度浮動少数点数

typedef s32		BITFIELD;	//!< ビットフィールド

#endif	// __TYPEDEF_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	template.h
//!	@brief	テンプレート関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__TEMPLATE_H__
#define	__TEMPLATE_H__


//----------------------------------------------------------------------
//	解放用テンプレート
//----------------------------------------------------------------------
template<typename T>
INLINE void SafeRelease(T& Pointer)
{
	if( Pointer != null )
	{
		Pointer->Release();
		Pointer = null;
	}
}
template<typename T>
INLINE void SafeDelete(T& Pointer)
{
	if( Pointer != null )
	{
		delete Pointer;
		Pointer = null;
	}
}
template<typename T>
INLINE void SafeDeleteArray(T& Pointer)
{
	if( Pointer != null )
	{
		delete [] Pointer;
		Pointer = null;
	}
}

//----------------------------------------------------------------------
//!	絶対値を返す
//!	@param value [in] 値
//!	@return 結果
//----------------------------------------------------------------------
template<typename T>
INLINE T Abs(const T& value)
{
	return (value < 0)? -value: value;
}
//----------------------------------------------------------------------
//!	低い方の値を返す
//!	@param value [in] 値
//!	@param min   [in] 最小値
//!	@return 結果
//----------------------------------------------------------------------
template<typename T>
INLINE T Min(const T& value, const T& min)
{
	return ( value < min )? value: min;
}
//----------------------------------------------------------------------
//!	高い方の値を返す
//!	@param value [in] 値
//!	@param max   [in] 最大値
//!	@return 結果
//----------------------------------------------------------------------
template<typename T>
INLINE T Max(const T& value, const T& max)
{
	return ( value > max )? value: max;
}
//----------------------------------------------------------------------
//!	値を範囲内に収める
//!	@param value [in] 値
//!	@param min   [in] 最小値
//!	@param max   [in] 最大値
//!	@return 結果
//----------------------------------------------------------------------
template<typename T>
INLINE T Clamp(const T& value, const T& min, const T& max)
{
	return ( value < min )? min: ( value > max )? max: value;
}
//----------------------------------------------------------------------
//!	値が範囲内にあるか確認
//!	@param value [in] 値
//!	@param min   [in] 最小値
//!	@param max   [in] 最大値
//!	@return 結果
//----------------------------------------------------------------------
template<typename T>
INLINE bool IsRange(const T& value, const T& min, const T& max)
{
	return ( min <= value && value <= max )? true: false;
}
//----------------------------------------------------------------------
//!	配列の要素数を取得する
//!	@return 要素数
//----------------------------------------------------------------------
template<typename T, u32 N>
INLINE u32 ArrayCount(const T (&)[N])
{
	return N;
}

#endif	// __KGL_TEMPLATE_H__
//======================================================================
//	END OF FILE
//======================================================================
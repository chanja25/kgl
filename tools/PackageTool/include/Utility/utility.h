//----------------------------------------------------------------------
//!
//!	@file	Time.h
//!	@brief	時間関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__UTILITY_H__
#define	__UTILITY_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <windows.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <ObjBase.h>	//	interfaceマクロを使用する為
#include <new>			//	placement newを使用する為
#include <vector>
#include <map>
#include <string>

//	utility
#include "define.h"
#include "typedef.h"
#include "template.h"
#include "struct.h"
#include "time.h"
#include "crc32.h"
#include "file.h"
#include "directory.h"


//	警告対策
#pragma	warning(disable:4996)	//	kgStrcpy等の対応
#pragma	warning(disable:4201)	//	無名structの対応
#pragma	warning(disable:4127)	//	if(true)の対応

#endif	// __UTILITY_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	define.h
//!	@brief	define定義
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__DEFINE_H__
#define	__DEFINE_H__

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#ifndef	null
#define null	nullptr
#endif	// ~#if null

#ifndef	interface
#define interface	struct
#endif	// ~#if interface

#ifndef	INLINE
#define	INLINE	inline
#endif	// ~#if INLINE

#ifndef	FORCEINLUNE
#define	FORCEINLUNE	__forceinline
#endif	// ~#if FORCEINLUNE

#ifndef	EXPORTFUNC
#define	EXPORTFUNC	extern "C" __declspec(dllexport)
#endif	// ~#if EXPORTFUNC

#ifndef	ALIGNED
#define	ALIGNED(x)	__declspec(align(x))
#endif	// ~#if ALIGNED

#ifndef	FOURCC
#define FOURCC(a, b, c, d)	((a) | (b << 8) | (c << 16) | (d << 24))
#endif	// ~#if FOURCC

#ifndef	STRUCT_OFFSET
#define STRUCT_OFFSET(_class_, _member_) ((s32)&((_class_*)0)->_member_)
#endif	// ~#if STRUCT_OFFSET

#ifndef	ALIGN
#define ALIGN(_size_, _align_)	((_size_ + _align_ - 1) & ~(_align_ - 1))
#endif	// ~#if ALIGN

#ifndef	KB
#define	KB(_size_)	((_size_) * 1024)
#endif	// ~#if KB

#ifndef	MB
#define	MB(_size_)	((_size_) * 1024 * 1024)
#endif	// ~#if MB

#ifndef	GB
#define	GB(_size_)	((_size_) * 1024 * 1024 * 1024)
#endif	// ~#if GB

#ifndef	Is64bit
#define	Is64bit()	(sizeof(void*) == 8)
#endif	// ~#if Is64bit

//	強制ブレーク
#define	kgDebugBreak()					DebugBreak()
//	アサート
#define	kgAssert(_condition_, _string_)	if( !(_condition_) ) { kgErrorLog(_string_); kgDebugBreak(); }
#define	kgAssertionFailed(_condition_)	kgAssert(_condition_, #_condition_)
//	エラーログ出力
#define	kgErrorLog(_string_)			printf("======================= ERROR LOG =======================");	\
										printf("File : %s", __FILE__);											\
										printf("Line : %d", __LINE__);											\
										printf(_string_);														\
										printf("=========================================================");

#define	kgTrace							printf

#endif	// __DEFINE_H__
//======================================================================
//	END OF FILE
//======================================================================
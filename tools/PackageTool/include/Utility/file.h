//----------------------------------------------------------------------
//!
//!	@file	file.h
//!	@brief	ファイル管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__FILE_H__
#define	__FILE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <windows.h>
#include "define.h"
#include "typedef.h"

namespace File
{
	//!	オープンフラグ
	namespace EOpen
	{
		const s32 Read			= (1 << 0);	//!< 読み込み
		const s32 Write			= (1 << 1);	//!< 書き込み
	}

	//!	共有フラグ
	namespace EShare
	{
		const s32 Read			= (1 << 0);	//!< 読み込み許可
		const s32 Write			= (1 << 1);	//!< 書き込み許可
		const s32 Delete		= (1 << 2);	//!< 削除許可
	}

	//!	作成モード
	namespace ECreate
	{
		const s32 CreateNew		= 0;	//!< 新しいファイル生成(既に存在している場合は失敗)
		const s32 CreateAlways	= 1;	//!< 新しいファイル生成(既に存在している場合は上書き)
		const s32 OpenExisting	= 2;	//!< ファイルをオープン(存在しない場合は失敗)
		const s32 OpenAlways	= 3;	//!< ファイルをオープン(存在しない場合は新しく作成)
	}

	//!	属性フラグ
	namespace EAttribute
	{
		const s32 Normal		= (1 << 0);	//!< 属性無し(単独で指定します)
		const s32 Hidden		= (1 << 1);	//!< 隠しファイル
		const s32 ReadOnly		= (1 << 2);	//!< 読み取り専用
		const s32 System		= (1 << 3);	//!< システムファイル
		const s32 Temporary		= (1 << 4);	//!< テンポラリファイル
		const s32 DeleteOnClose	= (1 << 5);	//!< ファイルのクローズ時に削除
	}

	//!	ファイル位置指定
	namespace EFilePointer
	{
		const s32 BEGIN   = 0;	//!< 先頭
		const s32 CURRENT = 1;	//!< 現在位置
		const s32 END     = 2;	//!< 終端
	}

	//======================================================================
	//!	ファイルマネージャーインターフェイス
	//======================================================================
	class CFile
	{
	public:
		//!	コンストラクタ
		CFile(void);

		//!	ファイルを開く
		//!	@param sFileName  [in] ファイル名
		//!	@param iOpen	  [in] オープンフラグ(上記のEOpenを参照)
		//!	@param iShare	  [in] 共有フラグ(上記のEShareを参照)
		//!	@param iCreate	  [in] 作成モード(上記のECreateを参照)
		//!	@param iAttribute [in] 属性フラグ(上記のEAttributeを参照)
		//!	@return 結果
		b8 Open(const c8* pFileName, s32 iOpen = EOpen::Read, s32 iShare = EShare::Read, s32 iCreate = ECreate::OpenExisting, s32 iAttribute = EAttribute::Normal);
		//!	ファイルを閉じる
		void Close(void);
		//!	読み込み
		//!	@param pBuffer	[out] 読み込んだデータ
		//!	@param uiSize	[in] 読み込むサイズ
		//!	@return 読み込んだサイズ
		u32 Reading(void* pBuffer, u32 uiSize);
		//!	書き込み
		//!	@param pBuffer	[in] データ
		//!	@param uiSize	[in] サイズ
		//!	@return 書き込んだサイズ
		u32 Writting(const void* pBuffer, u32 uiSize);

		//!	ファイルサイズ取得
		//!	@return ファイルサイズ
		u32 GetSize(void);
		//!	ファイル位置のセット
		//!	@param iMove		[in] 移動量(pMoveHighがnull以外の場合、iMoveと*pHighSize<<32で64bit値(s64)を生成して使用)
		//!	@param iPosition	[in] 移動開始位置
		//!	@return 現在の位置
		u32 Seek(s32 iMove, s32 iPosition = EFilePointer::BEGIN);
		//!	現在のファイル位置の取得
		//!	@return 現在の位置
		u32 Tell(void);

		//!	ファイルハンドルの取得
		INLINE HANDLE CFile::GetHandle(void) { return m_hFile; }

	private:
		HANDLE	m_hFile;	//!< ファイルハンドル
	};
}

#endif	// __FILE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
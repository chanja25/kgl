//----------------------------------------------------------------------
//!
//!	@file	PackageData.h
//!	@brief	パッケージツール
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_PACKAGEDATA_H__
#define	__KGL_PACKAGEDATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "PackageTool.h"

namespace Package
{
	namespace Project
	{
		const u32 PATH_MAX = 256;

		//!	プロジェクト情報
		struct ProjectInfo
		{
			Date		PackageDate;		//!< パッケージされた時間
			u64			u64Size;			//!< データの総サイズ
			u32			uiFileNum;			//!< ファイルの総数
			u32			uiDirectoryNum;		//!< ディレクトリの総数
			u32			uiOffset;			//!< ファイル情報へのオフセット
			BITFIELD	bAllPack: 1;		//!< 全てのデータを一つにパックするか？
			BITFIELD	bOptimizeDisk: 1;	//!< DVD、BD用に最適化を行うか？(bAllPackが立っている場合のみ有効)
		};
		//!	ディレクトリ情報
		struct DirectoryInfo
		{
			c8			cName[PATH_MAX];	//!< ファイル名
			s16			sHead;				//!< ファイル情報の先頭位置
			s16			sFileNum;			//!< ファイル数
			s16			sDirectoryNum;		//!< フォルダ数
			s16			sParent;			//!< 親のフォルダ
			s16			sChild;				//!< 子のフォルダ
			s16			sBrother;			//!< 同階層のフォルダ
			BITFIELD	bHighPriority: 1;	//!< 優先度高(bOptimizeDiskが有効の場合はデータの後方にパッケージされる)
			BITFIELD	bExclude: 1;		//!< 除外するか？
			BITFIELD	bLengthOver: 1;		//!< ファイル名の上限を超えているか？
		};
		//!	ファイル情報
		struct FileInfo
		{
			c8			cName[PATH_MAX];	//!< ファイル名
			Date		PackageDate;		//!< パッケージされた時間
			u32			uiSize;				//!< サイズ
			BITFIELD	bExclude: 1;		//!< 除外するか？
			BITFIELD	bCompress: 1;		//!< 圧縮するか？
		};
	}

	namespace Data
	{
		//!	プロジェクト情報
		struct kgPackageHeader
		{
			s32		iID;			//!< FOURCC('k', 'p', 'k', 'g')
			s32		iVersion;		//!< バージョン
			u32		uiDirectoryNum;	//!< ディレクトリの総数
			u32		uiFileNum;		//!< ファイルの総数
			u32		uiOffset;		//!< データへのオフセット
			s32		iReserved;
			u64		u64Size;		//!< データサイズ
		};

		//!	ディレクトリ情報
		struct kgDirectoryInfo
		{
			c8		cName[60];		//!< ファイル名
			u32		uiHash;			//!< ハッシュ値
			s16		sHead;			//!< ファイル情報の先頭位置
			s16		sFileNum;		//!< ファイル数
			s16		sChild;			//!< 子のフォルダー
			s16		sBrother;		//!< 次のフォルダー
		};

		//!	ファイル情報
		struct kgFileInfo
		{
			c8		cName[64];		//!< ファイル名
			u32		uiHash;			//!< ハッシュ値
			u32		uiSize;			//!< サイズ
			u32		uiArcSize;		//!< 圧縮ファイルサイズ
			u32		uiOffset;		//!< ファイル情報へのオフセット
		};
	}
}

#endif	// __KGL_PACKAGEDATA_H__
//======================================================================
//	END OF FILE
//======================================================================
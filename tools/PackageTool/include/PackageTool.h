//----------------------------------------------------------------------
//!
//!	@file	PackageTool.h
//!	@brief	パッケージツール
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KGL_PACKAGETOOL_H__
#define	__KGL_PACKAGETOOL_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include <Windows.h>
#include <string>
#include <vector>
#include "Utility/utility.h"
#include "PackageData.h"

using namespace std;

namespace Tool
{
	const s32 Version = 0;

	//!	ファイル情報
	struct FileInfo
	{
		Date		PackageDate;		//!< パッケージされた時間
		u32			uiSize;				//!< サイズ
		BITFIELD	bExclude: 1;		//!< 除外するか？
		BITFIELD	bCompress: 1;		//!< 圧縮するか？
		BITFIELD	bCreate: 1;			//!< 新規で作成されたか？
		BITFIELD	bUpdate: 1;			//!< 更新されたか？
		BITFIELD	bDelete: 1;			//!< 削除されてか？
	};

	//!	ディレクトリ情報
	struct DirectoryInfo
	{
		map<string, DirectoryInfo>	DirectoryList;		//!< ディレクトリ情報
		map<string, FileInfo>		FileList;			//!< ファイル情報
		BITFIELD					bHighPriority: 1;	//!< 優先度高(bOptimizeDiskが有効の場合はデータの後方にパッケージされる)
		BITFIELD					bExclude: 1;		//!< 除外するか？
		BITFIELD					bCreate: 1;			//!< 新規で作成されたか？
		BITFIELD					bUpdate: 1;			//!< 更新されたか？
		BITFIELD					bDelete: 1;			//!< 削除されてか？
	};

	//!	プロジェクト情報
	struct ProjectInfo
	{
		string			strPath;			//!< プロジェクト名
		Date			PackageDate;		//!< パッケージされた時間
		BITFIELD		bAllPack: 1;		//!< 全てのデータを一つにパックするか？
		BITFIELD		bOptimizeDisk: 1;	//!< DVD、BD用に最適化を行うか？(bAllPackが立っている場合のみ有効)
		BITFIELD		bUpdate: 1;			//!< 更新されたか？
		string			strRoot;			//!< ルートディレクトリ名
		DirectoryInfo	RootInfo;			//!< ルートディレクトリの情報
	};

	//======================================================================
	//!	パッケージツール
	//======================================================================
	class CPackageTool
	{
	public:
		//!	パッケージデータの作成
		//!	@param pProjectPath [in] プロジェクトのパス
		//!	@param pDirectory [in] ディレクトリ名
		//!	@param pRootPath [in] ルートディレクトリのパス
		//!	@return 結果
		bool CreatePackage(const c8* pProjectPath, const c8* pDirectory, const c8* pRootPath = null, bool bPackage = true, bool bUpdate = false);

	private:
		//!	プロジェクトの作成
		//!	@param pProjectPath [in] プロジェクトのパス
		//!	@param pDirectory [in] ディレクトリ名
		//!	@return 結果
		bool CreateProject(const c8* pProjectPath, const c8* pDirectory, bool bPackage);
		//!	プロジェクトの更新
		//!	@param pData [in] プロジェクトデータ
		//!	@param pDirectory [in] ディレクトリ名
		//!	@return 結果
		bool UpdateProject(const c8* pProjectPath, void* pData, const c8* pDirectory);

	private:
		//!	ディレクトリ情報の更新
		//!	@param Info [in] ディレクトリ情報
		//!	@param pPath [in] パス
		//!	@return 結果
		bool SetupDirectory(DirectoryInfo& Info, const c8* pPath);
		//!	ファイル情報の更新
		//!	@param Info [in] ファイル情報
		//!	@param pPath [in] パス
		//!	@return 結果
		bool SetupFile(FileInfo& Info, const Directory::Info& FindInfo);

	private:
		//!	プロジェクトデータの組み立て
		//!	@return 結果
		bool BuildProject(void);
		//!	プロジェクトデータの組み立て
		//!	@return 結果
		u64 BuildProjectDirectoryInfo(string strName, DirectoryInfo& Info, s16 sParent = -1);
		//!	プロジェクトデータの組み立て
		//!	@return 結果
		u64 BuildProjectFileInfo(string strName, FileInfo& Info);

	private:
		//!	プロジェクトの読み込み
		//!	@return 結果
		bool ReadProject(ProjectInfo& Info);

		//!	プロジェクトの出力
		//!	@return 結果
		bool WriteProject();

	private:
		string		m_strRoot;	//!< ルートディレクトリ
		ProjectInfo	m_Project;	//!< プロジェクト情報

		Package::Project::ProjectInfo			m_ProjectInfo;
		vector<Package::Project::DirectoryInfo> m_DirInfoList;
		vector<Package::Project::FileInfo>		m_FileInfoList;
	};
}

#endif	// __KGL_PACKAGETOOL_H__
//======================================================================
//	END OF FILE
//======================================================================
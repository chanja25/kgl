//----------------------------------------------------------------------
//!
//!	@file	PackageTool.cpp
//!	@brief	パッケージツール
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "PackageTool.h"
#include "PackageData.h"

namespace Tool
{
	//----------------------------------------------------------------------
	//	パッケージデータの作成
	//----------------------------------------------------------------------
	bool CPackageTool::CreatePackage(const c8* pProjectPath, const c8* pDirectory, const c8* pRootPath, bool bPackage, bool bUpdate)
	{
		if( pProjectPath == null )return false;
		if( pDirectory == null )return false;
		if( pRootPath == null )
		{
			pRootPath = ".";
		}
		m_strRoot = pRootPath;

		shared_ptr<File::CFile> pFile(new File::CFile);
		bool bCreate = false;
		if( !bUpdate )
		{
			bCreate = !pFile->Open((m_strRoot + "/" + pProjectPath).c_str());
		}
		if( bCreate )
		{
			kgTrace("新規でファイルを作成します(%s)\n", (m_strRoot + "/" + pProjectPath).c_str());
			if( !CreateProject(pProjectPath, pDirectory, bPackage) )
			{
				kgTrace("プロジェクトの作成に失敗しました\n");
				return false;
			}
		}
		else
		{
			u32 uiSize = pFile->GetSize();
			shared_ptr<c8> pProject(new c8[uiSize]);
			pFile->Reading(pProject.get(), uiSize);
			pFile->Close();

			kgTrace("プロジェクトの更新を行います(%s)\n", (m_strRoot + "/" + pProjectPath).c_str());
			if( !UpdateProject(pProjectPath, pProject.get(), pDirectory) )
			{
				kgTrace("プロジェクトの更新に失敗しました\n");
				return false;
			}
		}
		return true;
	}
	//----------------------------------------------------------------------
	//	プロジェクトの作成
	//----------------------------------------------------------------------
	bool CPackageTool::CreateProject(const c8* pProjectPath, const c8* pDirectory, bool bPackage)
	{
		Date PackageDate;
		DateTime(&PackageDate);

		m_Project.strPath		= pProjectPath;
		m_Project.PackageDate	= PackageDate;
		m_Project.bAllPack		= bPackage;
		m_Project.bOptimizeDisk = false;
		m_Project.bUpdate		= true;
		m_Project.strRoot		= pDirectory;

		SetupDirectory(m_Project.RootInfo, (m_strRoot + "/" + pDirectory).c_str());

		//	プロジェクトの出力
		WriteProject();

		return true;
	}
	//----------------------------------------------------------------------
	//	プロジェクトのセットアップ
	//----------------------------------------------------------------------
	bool CPackageTool::UpdateProject(const c8* pProjectPath, void* pData, const c8* pDirectory)
	{
		Date PackageDate;
		DateTime(&PackageDate);

		m_Project.strPath		= pProjectPath;
		m_Project.PackageDate	= PackageDate;
		m_Project.bAllPack		= false;
		m_Project.bOptimizeDisk = false;
		m_Project.bUpdate		= false;
		m_Project.strRoot		= pDirectory;

		SetupDirectory(m_Project.RootInfo, (m_strRoot + "/" + pDirectory).c_str());

		ProjectInfo	PrevProject;
		//!	プロジェクトの出力
		ReadProject(PrevProject);

		//	プロジェクトの出力
		WriteProject();

		return true;
	}

	//----------------------------------------------------------------------
	//	ディレクトリ情報の更新
	//----------------------------------------------------------------------
	bool CPackageTool::SetupDirectory(DirectoryInfo& Info, const c8* pPath)
	{
		Info.bCreate		= true;
		Info.bUpdate		= false;
		Info.bDelete		= false;

		shared_ptr<Directory::CDirectory> pDirectory(new Directory::CDirectory);

		Directory::Info FindInfo;
		bool bFind;
		{
			string strPath = pPath;
			bFind = pDirectory->FindFile((strPath + "/*").c_str(), &FindInfo) != null;
		}
		while( bFind )
		{
			if( FindInfo.uiAttribute & Directory::EAttribute::Directory )
			{
				string strFile = pPath;
				strFile += "/";
				strFile += FindInfo.cName;

				Info.DirectoryList[FindInfo.cName] = DirectoryInfo();
				DirectoryInfo& DInfo = Info.DirectoryList[FindInfo.cName];
				SetupDirectory(DInfo, strFile.c_str());
			}
			if( FindInfo.uiAttribute & Directory::EAttribute::File )
			{
				string strFile = pPath;
				strFile += "/";
				strFile += FindInfo.cName;

				Info.FileList[FindInfo.cName] = FileInfo();
				FileInfo& FInfo = Info.FileList[FindInfo.cName];
				SetupFile(FInfo, FindInfo);
			}
			bFind = pDirectory->NextFile(&FindInfo) != null;
		}
		return true;
	}
	//----------------------------------------------------------------------
	//	ファイル情報の更新
	//----------------------------------------------------------------------
	bool CPackageTool::SetupFile(FileInfo& Info, const Directory::Info& FindInfo)
	{
		Info.PackageDate	= FindInfo.Update;
		Info.uiSize			= FindInfo.uiSize;
		Info.bCreate		= true;
		Info.bUpdate		= false;
		Info.bDelete		= false;

		return true;
	}

	//----------------------------------------------------------------------
	//	プロジェクトデータの組み立て
	//----------------------------------------------------------------------
	bool CPackageTool::BuildProject(void)
	{
		m_FileInfoList.clear();
		m_DirInfoList.clear();

		u64 u64Size;
		u64Size = BuildProjectDirectoryInfo(m_Project.strRoot, m_Project.RootInfo);

		m_ProjectInfo.PackageDate		= m_Project.PackageDate;
		m_ProjectInfo.u64Size			= u64Size;
		m_ProjectInfo.uiFileNum			= (u32)m_FileInfoList.size();
		m_ProjectInfo.uiDirectoryNum	= (u32)m_DirInfoList.size();
		m_ProjectInfo.uiOffset			= sizeof(ProjectInfo);
		m_ProjectInfo.bAllPack			= m_Project.bAllPack;
		m_ProjectInfo.bOptimizeDisk		= m_Project.bOptimizeDisk;

		u32 i, j, uiCount;
		for( i = 0; i < m_ProjectInfo.uiDirectoryNum; i++ )
		{
			vector<s16> sChildList;
			for( j = i + 1; j < m_ProjectInfo.uiDirectoryNum; j++ )
			{
				if( i == m_DirInfoList[j].sParent )
				{
					sChildList.push_back(j);
				}
			}
			uiCount = (u32)sChildList.size();
			if( uiCount > 0 )
			{
				m_DirInfoList[i].sChild = sChildList[0];

				for( j = 0; j < uiCount - 1; j++ )
				{
					m_DirInfoList[sChildList[j]].sBrother = sChildList[j + 1];
				}
			}
		}

		return true;
	}

	//----------------------------------------------------------------------
	//	プロジェクトデータの組み立て
	//----------------------------------------------------------------------
	u64 CPackageTool::BuildProjectDirectoryInfo(string strName, DirectoryInfo& Info, s16 sParent)
	{
		s16 sIndex = (s16)m_DirInfoList.size();

		Package::Project::DirectoryInfo DirInfo;
		strcpy(DirInfo.cName, strName.c_str());
		DirInfo.sHead			= (s16)m_FileInfoList.size();
		DirInfo.sFileNum		= (s16)Info.FileList.size();
		DirInfo.sDirectoryNum	= (s16)Info.DirectoryList.size();
		DirInfo.sParent			= sParent;
		DirInfo.sChild			= -1;
		DirInfo.sBrother		= -1;
		DirInfo.bHighPriority	= Info.bHighPriority;
		DirInfo.bExclude		= Info.bExclude;
		m_DirInfoList.push_back(DirInfo);

		u64 u64Size = 0;
		if( Info.FileList.size() > 0 )
		{
			map<string, Tool::FileInfo>::iterator it;
			it = Info.FileList.begin();
			while( it != Info.FileList.end() )
			{
				u64Size += BuildProjectFileInfo((*it).first, (*it).second);
				it ++;
			}
		}

		if( Info.DirectoryList.size() > 0 )
		{
			map<string, Tool::DirectoryInfo>::iterator it;
			it = Info.DirectoryList.begin();
			while( it != Info.DirectoryList.end() )
			{
				u64Size += BuildProjectDirectoryInfo((*it).first, (*it).second, sIndex);
				it ++;
			}
		}
		return u64Size;
	}

	//----------------------------------------------------------------------
	//	プロジェクトデータの組み立て
	//----------------------------------------------------------------------
	u64 CPackageTool::BuildProjectFileInfo(string strName, FileInfo& Info)
	{
		Package::Project::FileInfo FileInfo;
		strcpy(FileInfo.cName, strName.c_str());
		FileInfo.PackageDate	= Info.PackageDate;
		FileInfo.uiSize			= Info.uiSize;
		FileInfo.bExclude		= Info.bExclude;
		FileInfo.bCompress		= Info.bCompress;

		m_FileInfoList.push_back(FileInfo);
		return Info.uiSize;
	}

	//----------------------------------------------------------------------
	//	プロジェクトの読み込み
	//----------------------------------------------------------------------
	bool CPackageTool::ReadProject(ProjectInfo& Info)
	{
		return true;
	}

	//----------------------------------------------------------------------
	//	プロジェクトの出力
	//----------------------------------------------------------------------
	bool CPackageTool::WriteProject()
	{
		if( m_Project.bUpdate == false )return true;

		if( !BuildProject() )
		{
			return false;
		}

		//	パッケージ内容を出力
		{
			kgTrace("FilePath:%s/%s\n", m_Project.strRoot.c_str(), m_Project.strPath.c_str());
			for( u32 i=0; i < m_DirInfoList.size(); i++ )
			{
				kgTrace("--Dir:%s", m_DirInfoList[i].cName);
				if( m_DirInfoList[i].sParent != -1 )
				{
					kgTrace(" --Parent:%s", m_DirInfoList[m_DirInfoList[i].sParent].cName);
				}
				kgTrace("\n");
				for( s16 j=0; j < m_DirInfoList[i].sFileNum; j++ )
				{
					kgTrace(" --File:%s\n", m_FileInfoList[m_DirInfoList[i].sHead + j].cName);
				}
			}
		}

		shared_ptr<File::CFile> pFile(new File::CFile);
		if( !pFile->Open((m_strRoot + "/" + m_Project.strPath + ".kpp").c_str(), File::EOpen::Write, File::EShare::Read, File::ECreate::CreateAlways) )
		{
			return false;
		}
		pFile->Writting(&m_ProjectInfo, sizeof(Package::Project::ProjectInfo));

		u32 uiSize = sizeof(Package::Project::DirectoryInfo) * (u32)m_DirInfoList.size();
		pFile->Writting(&m_DirInfoList[0], uiSize);

		uiSize = sizeof(Package::Project::FileInfo) * (u32)m_FileInfoList.size();
		pFile->Writting(&m_FileInfoList[0], uiSize);

		pFile->Close();
		return true;
	}
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	kgl.Directory.cpp
//!	@brief	ディレクトリ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "Utility/utility.h"
#include "Utility/directory.h"
#include <imagehlp.h>

//----------------------------------------------------------------------
// library
//----------------------------------------------------------------------
#pragma comment(lib, "imagehlp.lib")

using namespace std;

namespace Directory
{
	//----------------------------------------------------------------------
	//	ディレクトリ作成
	//----------------------------------------------------------------------
	b8 Create(const c8* pPath)
	{
		string sPath = pPath;
		size_t iLength = sPath.length();
		for( size_t i=0; i < iLength; i++ )
		{
			if( sPath[i] == '/' )	sPath[i] = '\\';
		}
		if( sPath[iLength - 1] != '\\' ) sPath += '\\';

		return MakeSureDirectoryPathExists(sPath.c_str()) != false;
	}

	//----------------------------------------------------------------------
	//	ディレクトリ削除(ファイルが存在する場合は失敗)
	//----------------------------------------------------------------------
	b8 Remove(const c8* pPath)
	{
		string sPath = pPath;
		size_t iLength = sPath.length();
		for( size_t i=0; i < iLength; i++ )
		{
			if( sPath[i] == '/' )	sPath[i] = '\\';
		}
		if( sPath[iLength - 1] != '\\' ) sPath += '\\';

		return RemoveDirectory(sPath.c_str()) != false;
	}

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CDirectory::CDirectory(void)
		: m_hFind(null), m_uiAttribute(EAttribute::All)
	{}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CDirectory::~CDirectory(void)
	{
		//Close();
	}

	//----------------------------------------------------------------------
	//	ファイル検索
	//----------------------------------------------------------------------
	const c8* CDirectory::FindFile(const c8* pPath, Info* pInfo, u32 uiAttribute)
	{
		Close();

		WIN32_FIND_DATA fd;
		//	ファイル検索
		m_hFind = FindFirstFile(pPath, &fd);
		//	ファイルの有無確認
		b8 bResult = (m_hFind != INVALID_HANDLE_VALUE);

		m_uiAttribute = uiAttribute;
		//	ファイルを検索していく
		while( bResult )
		{
			if( strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )
			{
				dword dwAttribute = fd.dwFileAttributes;
				uiAttribute = 0;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_DIRECTORY)?	EAttribute::Directory:		EAttribute::File;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_HIDDEN)?		EAttribute::Hidden:			0;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_READONLY)?		EAttribute::ReadOnly:		0;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_SYSTEM)?		EAttribute::System:			0;

				if( m_uiAttribute & uiAttribute )
				{
					strcpy(m_cFileName, fd.cFileName);

					SetupInfo(fd, pInfo, uiAttribute);

					return m_cFileName;
				}
			}
			//	次のファイルを取得
			bResult = !!FindNextFile(m_hFind, &fd);
		}
		//	閉じる
		Close();

		return null;
	}

	//----------------------------------------------------------------------
	//	ファイル検索
	//----------------------------------------------------------------------
	const c8* CDirectory::NextFile(Info* pInfo)
	{
		WIN32_FIND_DATA fd;
		//	次のファイルを取得
		b8 bResult = FindNextFile(m_hFind, &fd) != false;

		u32 uiAttribute = 0;
		//	ファイルを検索していく
		while( bResult )
		{
			if( strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, "..") )
			{
				dword dwAttribute = fd.dwFileAttributes;
				uiAttribute = 0;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_DIRECTORY)?	EAttribute::Directory:		EAttribute::File;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_HIDDEN)?		EAttribute::Hidden:			0;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_READONLY)?		EAttribute::ReadOnly:		0;
				uiAttribute |= (dwAttribute & FILE_ATTRIBUTE_SYSTEM)?		EAttribute::System:			0;

				if( m_uiAttribute & uiAttribute )
				{
					strcpy(m_cFileName, fd.cFileName);

					SetupInfo(fd, pInfo, uiAttribute);

					return m_cFileName;
				}
			}
			//	次のファイルを取得
			bResult = !!FindNextFile(m_hFind, &fd);
		}

		return null;
	}

	//----------------------------------------------------------------------
	//	ファイル情報設定
	//----------------------------------------------------------------------
	void CDirectory::SetupInfo(const WIN32_FIND_DATA& fd, Info* pInfo, u32 uiAttribute)
	{
		if( pInfo == null )	return;
		//	情報設定
		memset(pInfo, 0, sizeof(Info));

		strcpy(pInfo->cName, fd.cFileName);
		pInfo->uiSize		= fd.nFileSizeLow;
		pInfo->uiAttribute	= uiAttribute;

		//	時刻取得
		FileTimeToDate(&pInfo->Create, &fd.ftCreationTime);
		FileTimeToDate(&pInfo->Access, &fd.ftLastAccessTime);
		FileTimeToDate(&pInfo->Update, &fd.ftLastWriteTime);
	}

	//----------------------------------------------------------------------
	//	検索用ハンドルを閉じる
	//----------------------------------------------------------------------
	void CDirectory::Close(void)
	{
		if( m_hFind )
		{
			FindClose(m_hFind);
			m_hFind = null;
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	crc32.cpp
//!	@brief	CRC32
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
//	include
//----------------------------------------------------------------------
#include "Utility/utility.h"
#include "Utility/crc32.h"


namespace crc32
{
	static u32* s_pCrc32Table = null;
	static s32	s_iCrc32TableRefCount = 0;

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	bool Initialize(void)
	{
		if( s_iCrc32TableRefCount == 0 )
		{
			s_pCrc32Table = (u32*)malloc(sizeof(u32) * 256);

			register u32 uiPoly = 0xEDB88320;
			register u32 i, j, uiTemp;
	
			for( i = 0; i < 256; i++ )
			{
				uiTemp  = i;
		
				for( j = 0; j < 8; j++ )
				{
					if( uiTemp & 0x01 )	uiTemp = (uiTemp >> 1) ^ uiPoly;
					else				uiTemp = uiTemp >> 1;
				}
		
				s_pCrc32Table[i] = uiTemp;
			}
		}

		s_iCrc32TableRefCount ++;
		return true;
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void Finalize(void)
	{				
		s_iCrc32TableRefCount --;
		if( s_iCrc32TableRefCount <= 0 )
		{
			free(s_pCrc32Table);
			s_pCrc32Table = null;
		}
	}

	//----------------------------------------------------------------------
	//	ハッシュ値取得
	//----------------------------------------------------------------------
	u32 Get(const c8* pString)
	{
		if( !pString || !s_pCrc32Table )	return 0;

		register u32 uiHash	= 0xffffffff;
		register u32 i;

		for( i = 0; pString[i] != '\0'; i++ )
		{
			uiHash	= ((uiHash >> 8) & 0x00FFFFFF) ^ s_pCrc32Table[(uiHash ^ pString[i]) & 0xFF];
		}

		return uiHash ^ 0xffffffff;
	}

	//----------------------------------------------------------------------
	//	ハッシュ値取得
	//----------------------------------------------------------------------
	u32 Get(const s8* pPtr, u32 uiSize)
	{
		if( !pPtr || !s_pCrc32Table )	return 0;

		register u32 uiHash	= 0xffffffff;
		register u32 i;

		for( i = 0; i < uiSize; i++ )
		{
			uiHash	= ((uiHash >> 8) & 0x00FFFFFF) ^ s_pCrc32Table[(uiHash ^ pPtr[i]) & 0xFF];
		}

		return uiHash ^ 0xffffffff;
	}
}

//======================================================================
//	END OF FILE
//======================================================================
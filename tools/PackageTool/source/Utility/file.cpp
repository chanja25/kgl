//----------------------------------------------------------------------
//!
//!	@file	kgl.File.cpp
//!	@brief	ファイル管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "Utility/utility.h"
#include "Utility/file.h"

namespace File
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CFile::CFile(void)
		: m_hFile(null)
	{}
	//----------------------------------------------------------------------
	//	ファイルを開く
	//----------------------------------------------------------------------
	b8 CFile::Open(const c8* pFileName, s32 iOpen, s32 iShare, s32 iCreate, s32 iAttribute)
	{
		if( m_hFile )
		{
			Close();
			kgAssert(false, "ファイルが閉じられていません(CFile::Open)");
		}

		u32 uiAccess = 0;
		uiAccess |= (iOpen & EOpen::Read)?	GENERIC_READ:	0;
		uiAccess |= (iOpen & EOpen::Write)? GENERIC_WRITE:	0;

		u32 uiShare = 0;
		uiShare |= (iShare & EShare::Read)?		FILE_SHARE_READ:	0;
		uiShare |= (iShare & EShare::Write)?	FILE_SHARE_WRITE:	0;
		uiShare |= (iShare & EShare::Delete)?	FILE_SHARE_DELETE:	0;

		u32 uiCreate = 0;
		switch( iCreate )
		{
		case ECreate::CreateNew:	uiCreate = CREATE_NEW;		break;
		case ECreate::CreateAlways:	uiCreate = CREATE_ALWAYS;	break;
		case ECreate::OpenExisting:	uiCreate = OPEN_EXISTING;	break;
		case ECreate::OpenAlways:	uiCreate = OPEN_ALWAYS;		break;
		default:
			kgAssert(false, "Createモードに対応していない値が指定されました(CFile::Open)");
			uiCreate = OPEN_EXISTING;
			break;
		}

		u32 uiAttribute = 0;
		uiAttribute |= (iAttribute & EAttribute::Normal)?			FILE_ATTRIBUTE_NORMAL:		0;
		uiAttribute |= (iAttribute & EAttribute::Hidden)?			FILE_ATTRIBUTE_HIDDEN:		0;
		uiAttribute |= (iAttribute & EAttribute::ReadOnly)?			FILE_ATTRIBUTE_READONLY:	0;
		uiAttribute |= (iAttribute & EAttribute::System)?			FILE_ATTRIBUTE_SYSTEM:		0;
		uiAttribute |= (iAttribute & EAttribute::Temporary)?		FILE_ATTRIBUTE_TEMPORARY:	0;
		uiAttribute |= (iAttribute & EAttribute::DeleteOnClose)?	FILE_FLAG_DELETE_ON_CLOSE:	0;

		//	ファイルを開く
		m_hFile = CreateFile(pFileName,
								uiAccess,
								uiShare,
								null,
								uiCreate,
								uiAttribute,
								null);

		//	開けているか確認
		if( m_hFile == INVALID_HANDLE_VALUE )
		{
			kgTrace("ファイルのオープンに失敗しました(CFile::Open)");
			kgTrace("[%s]", pFileName);
			m_hFile = null;
			return false;
		}

		return true;
	}

	//----------------------------------------------------------------------
	//	ファイルを閉じる
	//----------------------------------------------------------------------
	void CFile::Close(void)
	{
		if( m_hFile == null )
			return;

		CloseHandle(m_hFile);
		m_hFile = null;
	}

	//----------------------------------------------------------------------
	//	読み込み
	//----------------------------------------------------------------------
	u32 CFile::Reading(void* pBuffer, u32 uiSize)
	{
		dword dwReadSize;
		//	データの読み込み
		ReadFile(m_hFile, pBuffer, uiSize, &dwReadSize, null);

		return (u32)dwReadSize;
	}

	//----------------------------------------------------------------------
	//	書き込み
	//----------------------------------------------------------------------
	u32 CFile::Writting(const void* pBuffer, u32 uiSize)
	{
		dword dwWriteSize;
		//	データの書き出し
		WriteFile(m_hFile, pBuffer, uiSize, &dwWriteSize, null);

		return (u32)dwWriteSize;
	}
			
	//----------------------------------------------------------------------
	//	ファイルサイズ取得
	//----------------------------------------------------------------------
	u32 CFile::GetSize(void)
	{
		return ::GetFileSize(m_hFile, null);
	}

	//----------------------------------------------------------------------
	//	ファイル位置のセット
	//----------------------------------------------------------------------
	u32 CFile::Seek(s32 iMove, s32 iPosition)
	{
		const s32 iPoint[] =
		{
			FILE_BEGIN,
			FILE_CURRENT,
			FILE_END,
		};

		return ::SetFilePointer(m_hFile, iMove, null, iPoint[iPosition]);
	}

	//----------------------------------------------------------------------
	//	ファイル位置の取得
	//----------------------------------------------------------------------
	u32 CFile::Tell(void)
	{
		return ::SetFilePointer(m_hFile, 0, null, FILE_CURRENT);
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Time.cpp
//!	@brief	時間関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "Utility/utility.h"
#include "Utility/time.h"
#include <time.h>

//----------------------------------------------------------------------
//	日時取得
//----------------------------------------------------------------------
void DateTime(Date* pDate)
{
	time_t timer;
	tm* pTime;

	timer = time(null);
	pTime = localtime(&timer);

	pDate->sYeah		= (s16)pTime->tm_year + 1900;
	pDate->cMonth		= (s8)pTime->tm_mon + 1;
	pDate->cDay			= (s8)pTime->tm_mday;
	pDate->cWeek		= (s8)pTime->tm_wday;
	pDate->cHour		= (s8)pTime->tm_hour;
	pDate->cMinute		= (s8)pTime->tm_min;
	pDate->cSecond		= (s8)pTime->tm_sec;
	pDate->cMilliSecond	= 0;
}

//----------------------------------------------------------------------
//	ファイルタイムから日時取得
//----------------------------------------------------------------------
void FileTimeToDate(Date* pDate, const FILETIME* pTime)
{
	SYSTEMTIME stTime;
					
	FileTimeToSystemTime(pTime, &stTime);
	pDate->sYeah		= (s16)stTime.wYear;
	pDate->cMonth		= (s8)stTime.wMonth;
	pDate->cDay			= (s8)stTime.wDay;
	pDate->cWeek		= (s8)stTime.wDayOfWeek;
	pDate->cHour		= (s8)stTime.wHour;
	pDate->cMinute		= (s8)stTime.wMinute;
	pDate->cSecond		= (s8)stTime.wSecond;
	pDate->cMilliSecond	= (s8)stTime.wMilliseconds;
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//! @file	main.cpp
//! @brief	main
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "ShaderCompiler.h"
#include <conio.h>

// library
#if	defined(_DEBUG)
#pragma comment(lib, "../../../kgl/Win32/kgld.lib")
#else	// ~#if defined(_DEBUG)
#pragma comment(lib, "../../../kgl/Win32/kgl.lib")
#endif	// ~#if defined(_DEBUG)
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "d3dx11.lib")


namespace CompileInfo
{
	EShaderPlatform		ShaderPlatform = ShaderPlatformUnknown;
	bool				SubDirctory = false;
	bool				FileOnly = true;
	Strings				Defines;
	const c8*			EntryPoint = "main";
	const c8*			OutputDir = "./bin";
	D3DXMACRO*			Define = null;
}

extern s32 EntryPoint(void);
//----------------------------------------------------------------------
//	main
//----------------------------------------------------------------------
int main(int, const c8**)
{
	return EntryPoint();
}

//	コンパイルスレッド
u32 CompileThread(void* pArg, s32 iId)
{
	TSmartPtr<c8> pPath = (c8*)pArg;
	const c8* pExtension = Path::GetExtension(pPath);

	TKGLPtr<IShaderCompiler> pShaderCompiler; 
	if( kgStrcmp(pExtension, "vsf") == 0 )
	{
		CreateVertexShaderCompiler(pShaderCompiler.GetReference());
	}
	else if( kgStrcmp(pExtension, "psf") == 0 )
	{
		CreatePixelShaderCompiler(pShaderCompiler.GetReference());
	}

	if( pShaderCompiler )
	{
		pShaderCompiler->Compile(pPath);
	}
	return 0;
}

//	コンパイル
void ShaderCompileThreadLaunch(const c8* pPath)
{
	c8* pArg = new c8[strlen(pPath) + 1];
	kgStrcpy(pArg, pPath);

	CompileThread(pArg, 0);
}

//	コンパイル
void ShaderCompileRequest(const c8* pPath)
{
	TKGLPtr<Directory::IDirectory> Dir;
	Directory::Create(Dir.GetReference());
	Directory::Info Info;

	if( Dir->FindFile(pPath, &Info) )
	{
		if( Info.uiAttribute & Directory::EAttribute::Directory )
		{
			CompileInfo::FileOnly = false;

			algorithm::string sPath = pPath;
			sPath += "/*";
			bool bResult = !!Dir->FindFile(sPath.c_str(), &Info);
			while( bResult )
			{
				if( Info.uiAttribute & Directory::EAttribute::Directory )
				{
					if( CompileInfo::SubDirctory )
					{
						sPath = pPath;
						sPath += "/";
						sPath += Info.cName;
						ShaderCompileRequest(sPath.c_str());
					}
				}
				else
				{
					sPath = pPath;
					sPath += "/";
					sPath += Info.cName;
					ShaderCompileThreadLaunch(sPath.c_str());
				}
				bResult = !!Dir->NextFile(&Info);
			}
		}
		else
		{
			ShaderCompileThreadLaunch(pPath);
		}
	}
}

//----------------------------------------------------------------------
//	エントリーポイント
//----------------------------------------------------------------------
s32 AppRun(kgl::Utility::ICommandLine* pCommandLine)
{
	//pCommandLine->AddCommand("D:\\User\\Desktop\\work\\Library\\kgl\\Library\\kglShader\\Shaders");
	//pCommandLine->AddCommand("-PLATFORM");
	//pCommandLine->AddCommand("SM3");
	//pCommandLine->AddCommand("-O");
	//pCommandLine->AddCommand("D:\\User\\Desktop\\work\\Library\\kgl\\Skeleton\\Data\\Shaders");

	c8 cCommand[256];
	u32 uiNum = pCommandLine->GetCount();
	if( uiNum == 1 )
	{
		printf("ShaderCompiler\n");
		printf("-F        [引数]⇒エントリーポイント(関数名)\n");
		printf("-D        [引数]⇒Defineマクロの定義(例:KGL 1)\n");
		printf("-O        [引数]⇒出力パス\n");
		printf("-PLATFORM [引数]⇒SM3, SM5\n");
		printf("-S        サブディレクトリを検索するか？\n");
		printf("please enter key\n");
		getch();
		return -1;
	}

	Strings paths;
#define	CheckCommand(cmd)	(kgStricmp(cmd, &cCommand[1]) == 0)
	for( u32 i=1; i < uiNum; i++ )
	{
		CharSet::ToUpper(cCommand, pCommandLine->GetCommand(i));

		switch( cCommand[0] )
		{
		case '-':
		case '/':
			if( CheckCommand("PLATFORM") )
			{
				CharSet::ToUpper(cCommand, pCommandLine->GetCommand(++ i));
				for( u32 i=0; i < ShaderPlatformMax; i++ )
				{
					if( kgStricmp(ShaderPlatformId[i], cCommand) == 0 )
					{
						CompileInfo::ShaderPlatform = (EShaderPlatform)i;
					}
				}
			}
			else if( CheckCommand("F") )
			{
				CompileInfo::EntryPoint = pCommandLine->GetCommand(++ i);
			}
			else if( CheckCommand("D") )
			{
				CompileInfo::Defines.push_back(pCommandLine->GetCommand(++ i));
				CompileInfo::Defines.push_back(pCommandLine->GetCommand(++ i));
			}
			else if( CheckCommand("S") )
			{
				CompileInfo::SubDirctory = true;
			}
			else if( CheckCommand("O") )
			{
				CompileInfo::OutputDir = pCommandLine->GetCommand(++ i);
			}
			break;

		default:
			paths.push_back(pCommandLine->GetCommand(i));
			break;
		}
	}
	if( CompileInfo::ShaderPlatform == ShaderPlatformUnknown )
	{
		printf("プラットフォームが指定されていません\n");
		return false;
	}

	//	出力ディレクトリ
	Directory::Create(CompileInfo::OutputDir);

	const D3DXMACRO DefineEnd = { null, null };

	algorithm::vector<D3DXMACRO> pDefine;

	for( u32 i=0; i < ShaderPlatformMax; i++ )
	{
		b8 bUse = (CompileInfo::ShaderPlatform == i);
		//	プラットフォーム毎のマクロ
		D3DXMACRO PlatformDefine = { ShaderPlatformId[i], bUse? "1": "0" };

		pDefine.push_back(PlatformDefine);
	}
	//	define定義
	uiNum = CompileInfo::Defines.size() / 2;
	for( u32 i=0; i < uiNum; i++ )
	{
		const D3DXMACRO Define = { CompileInfo::Defines[i*2], CompileInfo::Defines[i*2+1] };

		pDefine.push_back(Define);
	}
	pDefine.push_back(DefineEnd);

	CompileInfo::Define = &pDefine[0];

	//	シェーダーコンパイル
	for( u32 i=0; i < paths.size(); i++ )
	{
		ShaderCompileRequest(paths[i]);
	}

	return 0;
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//! @file	ShaderCompiler.cpp
//! @brief	ShaderCompiler
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "ShaderCompiler.h"

//!	ファイルの出力
//!	@param pPath	[in] パス
void IShaderCompiler::WriteFile(const c8* pPath, const ShaderInfo& Info, void* pData, u32 uiSize)
{
	TKGLPtr<File::IFile> pFile;
	File::Create(pFile.GetReference());
	pFile->Open(pPath, File::EOpen::Write, File::EShare::Read, File::ECreate::CreateAlways);

	pFile->Write(&Info, sizeof(ShaderInfo));
	pFile->Write(pData, uiSize);

	pFile->Close();
}

//======================================================================
//	頂点シェーダーコンパイラ(SM3)
//======================================================================
class CVertexShaderSM3Compiler
	: public IShaderCompiler
{
public:
	//!	コンパイル
	//!	@param pPath [in] パス
	//!	@return 結果
	bool Compile(const c8* pPath)
	{
		TKGLPtr<ID3DXBuffer> pShader;
		TKGLPtr<ID3DXBuffer> pErrMsg;
		TKGLPtr<ID3DXConstantTable> pConstantTable;
		HRESULT hr;

		hr = D3DXCompileShaderFromFile(
			pPath,
			CompileInfo::Define,
			null,
			CompileInfo::EntryPoint,
			"vs_3_0",
			0,
			pShader.GetReference(),
			pErrMsg.GetReference(),
			pConstantTable.GetReference()
			);
		if( FAILED(hr) )
		{
			printf("[Error]:%sのコンパイルに失敗しました\n", pPath);
			printf((const c8*)pErrMsg->GetBufferPointer());
			return false;
		}

		ShaderInfo Info;
		Info.iType = kgGraphics::EShaderType::Vertex;
		Info.iSize = pShader->GetBufferSize();

		algorithm::string sPath = CompileInfo::OutputDir;
		sPath += "/";
		sPath += Path::GetFileNameWithoutExtension(pPath);
		if( CompileInfo::FileOnly )
		{
			sPath += "_";
			sPath += CompileInfo::EntryPoint;
		}
		sPath += ".pvs";
		WriteFile(sPath.c_str(), Info, pShader->GetBufferPointer(), Info.iSize);

		return true;
	}
};

//======================================================================
//	ピクセルシェーダーコンパイラ(SM3)
//======================================================================
class CPixelShaderSM3Compiler
	: public IShaderCompiler
{
public:
	//!	コンパイル
	//!	@param pPath [in] パス
	//!	@return 結果
	bool Compile(const c8* pPath)
	{
		TKGLPtr<ID3DXBuffer> pShader;
		TKGLPtr<ID3DXBuffer> pErrMsg;
		TKGLPtr<ID3DXConstantTable> pConstantTable;
		HRESULT hr;

		hr = D3DXCompileShaderFromFile(
			pPath,
			CompileInfo::Define,
			null,
			CompileInfo::EntryPoint,
			"ps_3_0",
			0,
			pShader.GetReference(),
			pErrMsg.GetReference(),
			pConstantTable.GetReference()
			);
		if( FAILED(hr) )
		{
			printf("[Error]:%sのコンパイルに失敗しました", pPath);
			printf((const c8*)pErrMsg->GetBufferPointer());
			return false;
		}

		ShaderInfo Info;
		Info.iType = kgGraphics::EShaderType::Pixel;
		Info.iSize = pShader->GetBufferSize();

		algorithm::string sPath = CompileInfo::OutputDir;
		sPath += "/";
		sPath += Path::GetFileNameWithoutExtension(pPath);
		if( CompileInfo::FileOnly )
		{
			sPath += "_";
			sPath += CompileInfo::EntryPoint;
		}
		sPath += ".pps";
		WriteFile(sPath.c_str(), Info, pShader->GetBufferPointer(), Info.iSize);

		return true;
	}
};

//======================================================================
//	頂点シェーダーコンパイラ(SM5)
//======================================================================
class CVertexShaderSM5Compiler
	: public IShaderCompiler
{
public:
	//!	コンパイル
	//!	@param pPath [in] パス
	//!	@return 結果
	bool Compile(const c8* pPath)
	{
		TKGLPtr<ID3D10Blob> pShader;
		TKGLPtr<ID3D10Blob> pErrMsg;
		HRESULT hr;

		hr = D3DX11CompileFromFile(
			pPath,
			(D3D10_SHADER_MACRO*)CompileInfo::Define,
			null,
			CompileInfo::EntryPoint,
			"vs_5_0",
			0,
			0,
			null,
			pShader.GetReference(),
			pErrMsg.GetReference(),
			&hr
			);
		if( FAILED(hr) )
		{
			printf("[Error]:%sのコンパイルに失敗しました\n", pPath);
			printf((const c8*)pErrMsg->GetBufferPointer());
			return false;
		}

		ShaderInfo Info;
		Info.iType = kgGraphics::EShaderType::Vertex;
		Info.iSize = pShader->GetBufferSize();

		algorithm::string sPath = CompileInfo::OutputDir;
		sPath += "/";
		sPath += Path::GetFileNameWithoutExtension(pPath);
		if( CompileInfo::FileOnly )
		{
			sPath += "_";
			sPath += CompileInfo::EntryPoint;
		}
		sPath += ".pvs";
		WriteFile(sPath.c_str(), Info, pShader->GetBufferPointer(), Info.iSize);

		return true;
	}
};

//======================================================================
//	ピクセルシェーダーコンパイラ(SM5)
//======================================================================
class CPixelShaderSM5Compiler
	: public IShaderCompiler
{
public:
	//!	コンパイル
	//!	@param pPath [in] パス
	//!	@return 結果
	bool Compile(const c8* pPath)
	{
		TKGLPtr<ID3D10Blob> pShader;
		TKGLPtr<ID3D10Blob> pErrMsg;
		HRESULT hr;

		hr = D3DX11CompileFromFile(
			pPath,
			(D3D10_SHADER_MACRO*)CompileInfo::Define,
			null,
			CompileInfo::EntryPoint,
			"ps_5_0",
			0,
			0,
			null,
			pShader.GetReference(),
			pErrMsg.GetReference(),
			&hr
			);
		if( FAILED(hr) )
		{
			printf("[Error]:%sのコンパイルに失敗しました\n", pPath);
			printf((const c8*)pErrMsg->GetBufferPointer());
			return false;
		}

		ShaderInfo Info;
		Info.iType = kgGraphics::EShaderType::Pixel;
		Info.iSize = pShader->GetBufferSize();

		algorithm::string sPath = CompileInfo::OutputDir;
		sPath += "/";
		sPath += Path::GetFileNameWithoutExtension(pPath);
		if( CompileInfo::FileOnly )
		{
			sPath += "_";
			sPath += CompileInfo::EntryPoint;
		}
		sPath += ".pps";
		WriteFile(sPath.c_str(), Info, pShader->GetBufferPointer(), Info.iSize);

		return true;
	}
};

void CreateVertexShaderCompiler(IShaderCompiler** ppCompiler)
{
	IShaderCompiler* pCompiler = null;
	switch( CompileInfo::ShaderPlatform )
	{
	case ShaderPlatformSM3:
		pCompiler = new CVertexShaderSM3Compiler;
		break;
	case ShaderPlatformSM5:
		pCompiler = new CVertexShaderSM5Compiler;
		break;
	}
	if( pCompiler != null )
	{
		pCompiler->AddRef();
		*ppCompiler = pCompiler;
	}
}

void CreatePixelShaderCompiler(IShaderCompiler** ppCompiler)
{
	IShaderCompiler* pCompiler = null;
	switch( CompileInfo::ShaderPlatform )
	{
	case ShaderPlatformSM3:
		pCompiler = new CPixelShaderSM3Compiler;
		break;
	case ShaderPlatformSM5:
		pCompiler = new CPixelShaderSM5Compiler;
		break;
	}
	if( pCompiler != null )
	{
		pCompiler->AddRef();
		*ppCompiler = pCompiler;
	}
}

//======================================================================
//	END OF FILE
//======================================================================
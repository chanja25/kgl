//----------------------------------------------------------------------
//!
//! @file	ShaderCompiler.h
//! @brief	ShaderCompiler ヘッダー
//!
//! @author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SHADER_COMPILER_H__
#define	__SHADER_COMPILER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"
#include "File/kgl.File.h"
#include "File/kgl.Directory.h"
#include "Utility/Path/kgl.Path.h"
#include "../../Library/kgGraphics/include/kgGraphics.h"
#include <d3dx9.h>
#include <D3DX11.h>

using namespace kgl;
typedef	algorithm::vector<const c8*>	Strings;

enum EShaderPlatform
{
	ShaderPlatformUnknown = -1,

	ShaderPlatformSM3 = 0,
	ShaderPlatformSM5,

	ShaderPlatformMax,
};

static const c8* ShaderPlatformId[ShaderPlatformMax] =
{
	"SM3",
	"SM5",
};

namespace CompileInfo
{
	extern EShaderPlatform		ShaderPlatform;
	extern bool					SubDirctory;
	extern bool					FileOnly;
	extern Strings				Defines;
	extern const c8*			EntryPoint;
	extern const c8*			OutputDir;
	extern D3DXMACRO*			Define;
}

struct ShaderInfo
{
	s32	iType;	//!< シェーダータイプ
	s32	iSize;	//!< データサイズ
};

//======================================================================
//	シェーダーコンパイラ
//======================================================================
interface IShaderCompiler
	: public kgl::IKGLBase
{
public:
	//!	コンパイル
	//!	@param pPath [in] パス
	//!	@return 結果
	virtual bool Compile(const c8* pPath) = 0;

protected:
	//!	ファイルの出力
	//!	@param pPath	[in] パス
	virtual void WriteFile(const c8* pPath, const ShaderInfo& Info, void* pData, u32 uiSize);
};

void CreateVertexShaderCompiler(IShaderCompiler** ppCompiler);
void CreatePixelShaderCompiler(IShaderCompiler** ppCompiler);


#endif	// __SHADER_COMPILER_H__
//======================================================================
//	END OF FILE
//======================================================================
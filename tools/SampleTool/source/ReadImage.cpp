//----------------------------------------------------------------------
//!
//!	@file	ReadImage.cpp
//!	@brief	画像の読み込み
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "ReadImage.h"
#include "File/kgl.File.h"


//	Bitmap File Header
struct BitmapFileHeader
{
	u16	pad;		// Dummy
	u16	bfType;
	u32	bfSize;
	u16	bfReserved1;
	u16	bfReserved2;
	u32	bfOffBits;
};
const s32 BitmapFileHeaderSize = sizeof(BitmapFileHeader) - 2;
INLINE void* GetBitmapFileHeaderAddr(BitmapFileHeader* p) { return &p->bfType; };

//	Bitmap Info Header
struct BitmapInfoHeader
{
	u32	biSize;
	s32	biWidth;
	s32	biHeight;
	u16	biPlanes;
	u16	biBitCount;
	u32	biCompression;
	u32	biSizeImage;
	s32	biXPelsPerMeter;
	s32	biYPelsPerMeter;
	u32	biClrUsed;
	u32	biClrImportant;
};

//----------------------------------------------------------------------
//	画像出力(bmp)
//----------------------------------------------------------------------
b8 ReadImageFromBitmap(const c8* pPath, ImageData& Image)
{
	TKGLSmartPointer<File::IFile> pFile;
	File::Create(pFile.GetInitReference());

	if( !pFile->Open(pPath, File::EOpen::Read, 0, File::ECreate::OpenExisting) )
	{
		ErrorTrace("%sを開けませんでした(ReadImageFromBitmap)", pPath);
		return false;
	}

	BitmapFileHeader bfh;
	BitmapInfoHeader bih;
	// BMP ヘッダの取得
	pFile->Reading(GetBitmapFileHeaderAddr(&bfh), BitmapFileHeaderSize);
	pFile->Reading(&bih, sizeof(BitmapInfoHeader));

	u32 size = bih.biWidth * bih.biHeight * (bih.biBitCount/8);
	TSmartPointer<u8> temp, buffer;
	temp = new u8[size];
	buffer = new u8[size];

	pFile->Reading(temp, size);

	pFile->Close();

	s32 i, ptr1, ptr2, lineSize;
	lineSize = bih.biWidth * bih.biBitCount/8;
	for( i = 0; i < bih.biHeight; i++ )
	{
		ptr1 = i * lineSize;
		ptr2 = (bih.biHeight - i - 1) * lineSize;
		kgMemcpy(&buffer[ptr1], &temp[ptr2], lineSize);
	}

	Image.width		= bih.biWidth;
	Image.height	= bih.biHeight;
	Image.flag		= EColorFlag::Red | EColorFlag::Green | EColorFlag::Blue;
	Image.color		= buffer;
	return true;
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	main.cpp
//!	@brief	メイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

#include "ReadImage.h"
#include "Render/Utility/kgl.Image.h"

//----------------------------------------------------------------------
// library
//----------------------------------------------------------------------
#if	KGL_DEBUG
	#pragma comment(lib, "kgl_consoled.lib")
#else	// ~#if KGL_DEBUG
	#pragma comment(lib, "kgl_console.lib")
#endif	// ~#if KGL_DEBUG

bool IsEnableColor(ImageData& image, s32 x, s32 y)
{
	if( x < 0 )	return false;
	if( y < 0 )	return false;
	if( x >= image.width )	return false;
	if( y >= image.height )	return false;
	return true;
}
Vector3 GetImageColor(ImageData& image, s32 x, s32 y)
{
	Vector3 color;
	u8* p = &image.color[(x + y * image.width) * 3];

	const f32 fInv = 1.0f / 255.0f;
	color.x = (f32)p[0] * fInv;	
	color.y = (f32)p[1] * fInv;	
	color.z = (f32)p[2] * fInv;
	return color;
}
void SetImageColor(ImageData& image, Vector3& color, s32 x, s32 y)
{
	u8* p = &image.color[(x + y * image.width) * 3];
	
	p[0] = (u8)(color.x * 255.0f);
	p[1] = (u8)(color.y * 255.0f);
	p[2] = (u8)(color.z * 255.0f);
}

void Test(ImageData& OutputImage, ImageData& Image)
{
	s32 iRate = 2;
	ImageData newImage;
	newImage.flag	= EColorFlag::Red | EColorFlag::Green | EColorFlag::Blue;
	newImage.width	= Image.width * iRate;
	newImage.height	= Image.height * iRate;
	newImage.color	= new u8[newImage.width * newImage.height * 3];

	f32 fCount;
	Vector3 color;
	s32 x, y, xx, yy;
	for( x = 0; x < newImage.width; x++ )
	{
		for( y = 0; y < newImage.height; y++ )
		{
			xx = x / iRate - 1;
			yy = y / iRate - 1;
			color.Clear();
			fCount = 0.0f;
			for( s32 i=0; i < 3; i++ )
			{
				for( s32 j=0; j < 3; j++ )
				{
					if( i != 1 || j != 1 )
					{
						if( IsEnableColor(Image, xx+i, yy+j) )
						{
							color += GetImageColor(Image, xx+i, yy+j);
							fCount += 1.0f;
						}
					}
				}
			}
			color += GetImageColor(Image, xx+1, yy+1) * fCount;
			fCount += fCount;


			color /= fCount;
			color.x = Clamp(color.x, 0.0f, 1.0f);
			color.y = Clamp(color.y, 0.0f, 1.0f);
			color.z = Clamp(color.z, 0.0f, 1.0f);
			SetImageColor(newImage, color, x, y);
		}
	}

	OutputImage = newImage;
}

void Test2(ImageData& OutputImage, ImageData& Image)
{
	s32 iRate = 2;
	ImageData newImage;
	newImage.flag	= EColorFlag::Red | EColorFlag::Green | EColorFlag::Blue;
	newImage.width	= Image.width * iRate;
	newImage.height	= Image.height * iRate;
	newImage.color	= new u8[newImage.width * newImage.height * 3];

	Vector3 color;
	s32 x, y, xx, yy;
	for( x = 0; x < newImage.width; x++ )
	{
		for( y = 0; y < newImage.height; y++ )
		{
			xx = x / iRate;
			yy = y / iRate;
			color = GetImageColor(Image, xx, yy);
			SetImageColor(newImage, color, x, y);
		}
	}

	OutputImage = newImage;
}

//----------------------------------------------------------------------
//!	アプリケーションの実行
//!	@return 実行結果
//----------------------------------------------------------------------
s32 AppRun(kgl::Utility::ICommandLine* pCommandLine)
{
	ImageData image;
	if( ReadImageFromBitmap("sample.bmp", image) )
	{
		ImageData output, temp[2];
		Test(temp[0], image);
		Test2(temp[1], image);

		{
			output.width	= temp[0].width;
			output.height	= temp[0].height;
			output.color	= new u8[output.width * output.height * 3];
		}
		Vector3 color;
		s32 x, y, n;
		for( x = 0; x < output.width; x++ )
		{
			for( y = 0; y < output.height; y++ )
			{
				color.Clear();
				for( n = 0; n < 2; n++ )
				{
					color += GetImageColor(temp[n], x, y);
				}
				color /= (f32)n;
				color.x = Clamp(color.x, 0.0f, 1.0f);
				color.y = Clamp(color.y, 0.0f, 1.0f);
				color.z = Clamp(color.z, 0.0f, 1.0f);
				SetImageColor(output, color, x, y);
			}
		}
		Image::OutputImageToBitmap("testx2.bmp", output.color, output.width, output.height, Image::EColorType::RGB);
	}
	
	return 0;
}

IMPLEMENT_ENTRYPOINT;

//======================================================================
//	END OF FILE
//======================================================================
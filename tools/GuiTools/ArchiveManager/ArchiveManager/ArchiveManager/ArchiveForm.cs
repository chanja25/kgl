﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ArchiveManager
{
    public partial class ArchiveForm : Form
    {
        private ArchiveManager m_ArchiveManager;
        private bool m_bArchivePath;

        public ArchiveForm()
        {
            InitializeComponent();

            m_ArchiveManager = new ArchiveManager();
        }
        //  パッケージの作成
        private void Package_Click(object sender, EventArgs e)
        {
        }
        //  パッケージリストの更新
        private void update_Click(object sender, EventArgs e)
        {
            if (m_bArchivePath)
            {
                m_ArchiveManager.UpdateArchiveList(ArchiveList, ArchivePath.Text);
            }
            else
            {
                string Error = "不正なパスが指定されています\nディレクトリを指定してください";
                if (ArchivePath.Text.Length > 0)
                {
                    Error += "[" + ArchivePath.Text + "]";
                }
                MessageBox.Show(Error, "Warning", MessageBoxButtons.OK);
            }
        }
        //  無視リストを開く
        private void IgnoreList_Click(object sender, EventArgs e)
        {
            if (m_bArchivePath)
            {
                m_ArchiveManager.OpenIgnoreListForm();
            }
            else
            {
                MessageBox.Show("アーカイブのパスを指定してください", "Warning");
            }
        }
        //  既存のアーカイブ情報を開く
        private void Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            //  既にパスが指定されている場合は選択しているパスを設定する
            if (m_bArchivePath)
            {
                dialog.InitialDirectory = ArchivePath.Text;
            }
            else
            {
                dialog.InitialDirectory = Directory.GetCurrentDirectory();
            }
            dialog.Filter = "Archive Project (*.arcproj)|*.arcproj";
            dialog.FilterIndex = 1;
            dialog.Title = "プロジェクトファイルを指定してください";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (m_ArchiveManager.OpenProject(dialog.FileName))
                {
                }
            }
        }
        //  ツールを閉じる
        private void Exit_Click(object sender, EventArgs e)
        {
            CloseArchiveManager();
        }
        //  バージョン情報の表示
        private void Version_Click(object sender, EventArgs e)
        {
            Form Form = new VersionForm();
            Form.ShowDialog();
            Form.SetDesktopLocation(Cursor.Position.X, Cursor.Position.Y);
        }
        //  ドラッグ&ドロップでのパス設定
        private void ArchivePath_DragDrop(object sender, DragEventArgs e)
        {
            string[] FilePath = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            ArchivePath.Text = FilePath[0];
        }
        //  ドラッグ&ドロップで入って来る値が有効なパスか確認
        private void ArchivePath_DragEnter(object sender, DragEventArgs e)
        {
            bool bDrap = false;
            //ドラッグされたのがディレクトリーか？
            if ((e.Data.GetDataPresent(DataFormats.FileDrop)))
            {
                string[] FilePath = (string[])e.Data.GetData(DataFormats.FileDrop, false);
                if (FilePath.Length == 1)
                {
                    if (File.GetAttributes(FilePath[0]) == FileAttributes.Directory)
                    {
                        bDrap = true;
                    }
                }
            }
            if (bDrap)
            {
                //ドラッグされたデータを受け取る
                e.Effect = DragDropEffects.All;
            }
            else
            {
                //ドラッグされたデータを受け取らない
                e.Effect = DragDropEffects.None;
            }
        }
        //  ダイアログからパス設定
        private void ArchivePathButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            //  既にパスが指定されている場合は選択しているパスを設定する
            if (m_bArchivePath)
            {
                dialog.SelectedPath = ArchivePath.Text;
            }
            else
            {
                dialog.SelectedPath = Directory.GetCurrentDirectory();
            }
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                ArchivePath.Text = dialog.SelectedPath;
            }
        }

        //  ツールを閉じる
        private void CloseArchiveManager()
        {
            if (m_ArchiveManager.Close())
            {
                this.Close();
            }
        }

        private void ArchivePath_TextChanged(object sender, EventArgs e)
        {
            if (Directory.Exists(ArchivePath.Text))
            {
                Directory.SetCurrentDirectory(ArchivePath.Text);
                m_bArchivePath = true;
            }
            else
            {
                m_bArchivePath = false;
            }
        }

        private void ArchiveList_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                TreeView treeView = (TreeView)sender;
                //  カーソル位置のアイテムを選択する
                TreeNode node = treeView.GetNodeAt(e.Location);
                if (node != null)
                {
                    treeView.SelectedNode = node;

                    if (m_bArchivePath && node.Parent != null)
                    {
                        ArchiveListMenuStrip.Items.Clear();

                        ToolStripItem item;
                        if (node.Nodes.Count > 0)
                        {
                            item = ArchiveListMenuStrip.Items.Add("ディレクトリを無視リストに追加する");
                            item.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IgnoreFile_MouseUp);
                        }
                        else
                        {
                            item = ArchiveListMenuStrip.Items.Add("ファイルを無視リストに追加する");
                            item.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IgnoreFile_MouseUp);

                            string Extension = Path.GetExtension(node.Text);
                            if (Extension.Length > 0)
                            {
                                item = ArchiveListMenuStrip.Items.Add(Extension + "の拡張子を無視リストに追加する");
                                item.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IgnoreExtension_MouseUp);
                            }
                        }
                        ArchiveListMenuStrip.Show(Cursor.Position);
                    }
                }
            }
        }

        private void IgnoreFile_MouseUp(object sender, MouseEventArgs e)
        {
            ToolStripItem item = (ToolStripItem)sender;

            if (ArchiveList.SelectedNode != null)
            {
                TreeNode node = ArchiveList.SelectedNode;
                string FilePath = node.Text;
                while (node.Parent != null)
                {
                    node = node.Parent;
                    FilePath = FilePath.Insert(0, node.Text + @"\");
                }
                m_ArchiveManager.AddIgnoreFiles(FilePath);
            }
        }

        private void IgnoreExtension_MouseUp(object sender, MouseEventArgs e)
        {
            if (ArchiveList.SelectedNode != null)
            {
                string Extension = Path.GetExtension(ArchiveList.SelectedNode.Text);
                m_ArchiveManager.AddIgnoreExtension(Extension);
            }
        }
    }
}

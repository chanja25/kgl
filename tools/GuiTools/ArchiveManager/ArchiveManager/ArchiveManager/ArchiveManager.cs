﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace ArchiveManager
{
    public enum ESavedCheck
    {
        Save,
        Close,
        Cancel,
    }

    public class ArchiveManager
    {
        private string m_ProjectPath;
        private string m_RootPath;
        private IgnoreListForm m_IgnoreListForm;
        private UpdateWait m_UpdateWaitDialog;
        private Project.ArchiveProject m_Project;

        public ArchiveManager()
        {
            m_ProjectPath = "";
            m_IgnoreListForm = new IgnoreListForm();
        }

        //  プロジェクトの作成
        public bool CreateProject(string ProjectPath)
        {
            if (ProjectPath.CompareTo(ProjectPath) == 0)
            {
                return false;
            }
            if (!Close())
            {
                return false;
            }
            m_ProjectPath = ProjectPath;
            return true;
        }
        //  プロジェクトを開く
        public bool OpenProject(string ProjectPath)
        {
            if (ProjectPath.CompareTo(ProjectPath) == 0)
            {
                //  開き直すか確認
                if (!Close())
                {
                    return false;
                }
            }
            m_ProjectPath = ProjectPath;
            return true;
        }

        //  アーカイブリストの更新
        public bool UpdateArchiveList(TreeView ArchiveList, string RootPath)
        {
            m_ProjectPath = RootPath + ".arcproj";
            m_RootPath = Path.GetDirectoryName(RootPath);

            m_UpdateWaitDialog = new UpdateWait();
            m_UpdateWaitDialog.Show();

            TreeNode RootNodes = new TreeNode();
            RootNodes.Text = Path.GetFileName(RootPath);
            //  作成するアーカイブのリストを作成する
            CreateArchiveList(RootNodes, RootNodes.Text);

            ArchiveList.Nodes.Clear();
            ArchiveList.Nodes.Add(RootNodes);

            CreateArchiveProject(RootNodes);

            m_UpdateWaitDialog.Close();
            m_UpdateWaitDialog = null;

            return true;
        }
        //  アーカイブリストの作成
        private void CreateArchiveList(TreeNode ParentNodes, string ParentPath)
        {
            string SearchPath = m_RootPath + @"\" + ParentPath;
            m_UpdateWaitDialog.AnalyzeDir.Text = SearchPath;
            m_UpdateWaitDialog.Refresh();

            string[] PathList = Directory.GetFiles(SearchPath);
            for (int i = 0; i < PathList.Length; i++)
            {
                string Extension = Path.GetExtension(PathList[i]);
                if (!IsEnableExtension(Extension))
                {
                    continue;
                }
                string FileName = Path.GetFileName(PathList[i]);
                string FilePath = ParentPath + @"\" + FileName;
                if (IsEnablePath(FilePath))
                {
                    ParentNodes.Nodes.Add(FileName);
                }
            }
            PathList = Directory.GetDirectories(SearchPath);
            for (int i = 0; i < PathList.Length; i++)
            {
                string DirectoryName = Path.GetFileName(PathList[i]);
                string DirectoryPath = ParentPath + @"\" + DirectoryName;
                if (IsEnablePath(DirectoryPath))
                {
                    TreeNode NodeList = new TreeNode();
                    NodeList.Text = DirectoryName;
                    CreateArchiveList(NodeList, DirectoryPath);
                    //  ファイルが無いディレクトリは無視する
                    if (NodeList.Nodes.Count > 0)
                    {
                        ParentNodes.Nodes.Add(NodeList);
                    }
                }
            }
        }
        //  アーカイブリストに含まれる拡張子か？
        private bool IsEnableExtension(string Extension)
        {
            if (m_IgnoreListForm.IgnoreExtensions.Items.Count > 0)
            {
                //  拡張子の無視リストをチェック
                if (Extension.Length > 0)
                {
                    return m_IgnoreListForm.IgnoreExtensions.FindStringExact(Extension) == ListBox.NoMatches;
                }
            }
            return true;
        }
        //  アーカイブリストに含まれるパスか？
        private bool IsEnablePath(string Path)
        {
            if (m_IgnoreListForm.IgnoreFiles.Items.Count > 0)
            {
                //  ファイルの無視リストをチェック
                return m_IgnoreListForm.IgnoreFiles.FindStringExact(Path) == ListBox.NoMatches;
            }
            return true;
        }
        //  アーカイブリストの作成
        private void CreateArchiveProject(TreeNode RootNode)
        {
            var NewProject = new Project.ArchiveProject();
            var Root = NewProject.Archive.Root;

            string RootDir = m_RootPath + @"\" + RootNode.Text;
            UpdateArchiveProject(RootDir, Root, RootNode);

            string Dir = Directory.GetCurrentDirectory();

            var Ignore = NewProject.Ignore;
            foreach (string Extension in m_IgnoreListForm.IgnoreExtensions.Items)
            {
                Ignore.Extension.Add(Extension);
            }
            foreach (string Path in m_IgnoreListForm.IgnoreFiles.Items)
            {
                Ignore.Path.Add(Path);
            }

            m_Project = NewProject;
            Save();
        }
        //  アーカイブリストの更新
        private void UpdateArchiveProject(string ParentPath, Project.Directory Parent, TreeNode Node)
        {
            Parent.Name = Node.Text;

            foreach(TreeNode ChildNode in Node.Nodes)
            {
                string CurrentPath = ParentPath + @"\" + ChildNode.Text;
                if (Directory.Exists(CurrentPath))
                {
                    Project.Directory ProjDirectory = new Project.Directory();
                    UpdateArchiveProject(CurrentPath, ProjDirectory, ChildNode);

                    Parent.SubDirectory.Add(ProjDirectory);
                }
                else if (File.Exists(CurrentPath))
                {
                    string FilePath = ParentPath + @"\" + ChildNode.Text;

                    Project.File ProjFile = new Project.File();
                    ProjFile.Name = ChildNode.Text;
                    ProjFile.Changed = true;
                    ProjFile.UpdateAt = Directory.GetLastWriteTime(FilePath).ToString("yyyy-MM-dd HH:mm:ss");

                    Parent.File.Add(ProjFile);
                }
            }
        }

        //  閉じる
        public bool Close(bool bShouldSave = true)
        {
            bool bSuccessClose = true;
            if (bShouldSave)
            {
                switch (ShouldSave())
                {
                    case ESavedCheck.Save:
                        bSuccessClose = Save();
                        break;
                    case ESavedCheck.Cancel:
                        bSuccessClose = false;
                        return false;
                }
            }

            if (bSuccessClose)
            {
                m_ProjectPath = "";
                m_Project = null;
            }
            return bSuccessClose;
        }
        //  保存確認
        public ESavedCheck ShouldSave()
        {
            if (IsEdit())
            {
                return ESavedCheck.Save;
            }
            return ESavedCheck.Close;
        }
        //  保存
        public bool Save()
        {
            if (m_Project == null)
            {
                return false;
            }
            return m_Project.Save(m_ProjectPath);
        }
        //  編集中か？
        public bool IsEdit()
        {
            return m_ProjectPath.Length > 0;
        }
        //  無視リストを開く
        public void OpenIgnoreListForm()
        {
            m_IgnoreListForm.ShowDialog();
        }
        //  無視リストに拡張子を追加
        public void AddIgnoreExtension(string Extension)
        {
            if (m_IgnoreListForm.IgnoreExtensions.FindStringExact(Extension) == ListBox.NoMatches)
            {
                m_IgnoreListForm.IgnoreExtensions.Items.Add(Extension);
            }
        }
        //  無視リストに拡張子を追加
        public void AddIgnoreFiles(string FilePath)
        {
            if (m_IgnoreListForm.IgnoreFiles.FindStringExact(FilePath) == ListBox.NoMatches)
            {
                m_IgnoreListForm.IgnoreFiles.Items.Add(FilePath);
            }
        }
    }
}

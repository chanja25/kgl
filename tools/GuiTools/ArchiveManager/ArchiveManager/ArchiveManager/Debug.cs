﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ArchiveManager
{
    class Debug
    {
        /// <summary>
        /// エラーログの出力
        /// </summary>
        /// <param name="log">ログ情報</param>
        static public void Log(string log)
        {
            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string text = string.Format("- {0}\n{1}", time, log);
#if DEBUG
            Console.WriteLine(text);
#endif  // ~#if DEBUG

            WriteLog(text);
        }
        /// <summary>
        /// エラーログファイルの出力
        /// </summary>
        /// <param name="log">ログ情報</param>
        /// <param name="error">エラー内容</param>
        static public void Log(string log, Exception error)
        {
            Log(log + "\n" + error.Message);
        }

        /// <summary>
        /// エラーログの出力
        /// </summary>
        /// <param name="log">ログ情報</param>
        static private void WriteLog(string log)
        {
            lock (debuglock)
            {
                string dir = @".\log\";
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                string time = DateTime.Now.ToString("yyyy-MM-dd");
                // ログファイルを開いている等により書き込みに失敗した場合はファイル名を変更して数回試行する
                // ファイル名はerror[日付][試行回数].logで出力される ※試行回数が0の時は省略されます
                for (int i = 0; i < 10; i++)
                {
                    string path = string.Format("{0}error{1}{2}.log", dir, time, (i == 0) ? "" : i.ToString());

                    StreamWriter sw = null;
                    try
                    {
                        sw = new StreamWriter(path, true, Encoding.UTF8);
                        sw.WriteLine(log + "\n");
                        sw.Close();
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("{0}:{1}", e.Message, path);
                    }
                    if (sw != null) { sw.Close(); }
                }
            }
        }

        static private object debuglock = new object();
    }
}

﻿namespace ArchiveManager
{
    partial class VersionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToolName = new System.Windows.Forms.Label();
            this.CaptionVersion = new System.Windows.Forms.Label();
            this.Version = new System.Windows.Forms.Label();
            this.CptionAuthor = new System.Windows.Forms.Label();
            this.Author = new System.Windows.Forms.Label();
            this.Tool = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ToolName
            // 
            this.ToolName.AutoSize = true;
            this.ToolName.Location = new System.Drawing.Point(152, 9);
            this.ToolName.Name = "ToolName";
            this.ToolName.Size = new System.Drawing.Size(87, 12);
            this.ToolName.TabIndex = 0;
            this.ToolName.Text = "ArchiveManager";
            // 
            // CaptionVersion
            // 
            this.CaptionVersion.AutoSize = true;
            this.CaptionVersion.Location = new System.Drawing.Point(12, 31);
            this.CaptionVersion.Name = "CaptionVersion";
            this.CaptionVersion.Size = new System.Drawing.Size(46, 12);
            this.CaptionVersion.TabIndex = 1;
            this.CaptionVersion.Text = "Version:";
            // 
            // Version
            // 
            this.Version.AutoSize = true;
            this.Version.Location = new System.Drawing.Point(213, 31);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(25, 12);
            this.Version.TabIndex = 2;
            this.Version.Text = "0.01";
            // 
            // CptionAuthor
            // 
            this.CptionAuthor.AutoSize = true;
            this.CptionAuthor.Location = new System.Drawing.Point(12, 53);
            this.CptionAuthor.Name = "CptionAuthor";
            this.CptionAuthor.Size = new System.Drawing.Size(41, 12);
            this.CptionAuthor.TabIndex = 3;
            this.CptionAuthor.Text = "Author:";
            // 
            // Author
            // 
            this.Author.AutoSize = true;
            this.Author.Location = new System.Drawing.Point(173, 53);
            this.Author.Name = "Author";
            this.Author.Size = new System.Drawing.Size(66, 12);
            this.Author.TabIndex = 4;
            this.Author.Text = "S.Kawamoto";
            // 
            // Tool
            // 
            this.Tool.AutoSize = true;
            this.Tool.Location = new System.Drawing.Point(12, 9);
            this.Tool.Name = "Tool";
            this.Tool.Size = new System.Drawing.Size(58, 12);
            this.Tool.TabIndex = 6;
            this.Tool.Text = "ToolName:";
            // 
            // VersionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 76);
            this.Controls.Add(this.Tool);
            this.Controls.Add(this.Author);
            this.Controls.Add(this.CptionAuthor);
            this.Controls.Add(this.Version);
            this.Controls.Add(this.CaptionVersion);
            this.Controls.Add(this.ToolName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VersionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "バージョン情報";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ToolName;
        private System.Windows.Forms.Label CaptionVersion;
        private System.Windows.Forms.Label Version;
        private System.Windows.Forms.Label CptionAuthor;
        private System.Windows.Forms.Label Author;
        private System.Windows.Forms.Label Tool;
    }
}
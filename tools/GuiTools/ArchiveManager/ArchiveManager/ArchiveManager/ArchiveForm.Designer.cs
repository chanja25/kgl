﻿namespace ArchiveManager
{
    partial class ArchiveForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.update = new System.Windows.Forms.Button();
            this.ArchiveList = new System.Windows.Forms.TreeView();
            this.Package = new System.Windows.Forms.Button();
            this.Compress = new System.Windows.Forms.CheckBox();
            this.IgnoreList = new System.Windows.Forms.Button();
            this.ArchivePath = new System.Windows.Forms.TextBox();
            this.ArchivePathButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ファイルToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Version = new System.Windows.Forms.ToolStripMenuItem();
            this.ArchiveListMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // update
            // 
            this.update.Location = new System.Drawing.Point(12, 568);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(75, 23);
            this.update.TabIndex = 0;
            this.update.Text = "リスト更新";
            this.update.UseVisualStyleBackColor = true;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // ArchiveList
            // 
            this.ArchiveList.Location = new System.Drawing.Point(12, 55);
            this.ArchiveList.Name = "ArchiveList";
            this.ArchiveList.Size = new System.Drawing.Size(710, 507);
            this.ArchiveList.TabIndex = 1;
            this.ArchiveList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ArchiveList_MouseDown);
            // 
            // Package
            // 
            this.Package.Location = new System.Drawing.Point(606, 568);
            this.Package.Name = "Package";
            this.Package.Size = new System.Drawing.Size(116, 23);
            this.Package.TabIndex = 2;
            this.Package.Text = "パッケージ作成";
            this.Package.UseVisualStyleBackColor = true;
            this.Package.Click += new System.EventHandler(this.Package_Click);
            // 
            // Compress
            // 
            this.Compress.AutoSize = true;
            this.Compress.Checked = true;
            this.Compress.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Compress.Location = new System.Drawing.Point(552, 572);
            this.Compress.Name = "Compress";
            this.Compress.Size = new System.Drawing.Size(48, 16);
            this.Compress.TabIndex = 3;
            this.Compress.Text = "圧縮";
            this.Compress.UseVisualStyleBackColor = true;
            // 
            // IgnoreList
            // 
            this.IgnoreList.Location = new System.Drawing.Point(93, 568);
            this.IgnoreList.Name = "IgnoreList";
            this.IgnoreList.Size = new System.Drawing.Size(75, 23);
            this.IgnoreList.TabIndex = 4;
            this.IgnoreList.Text = "無視リスト";
            this.IgnoreList.UseVisualStyleBackColor = true;
            this.IgnoreList.Click += new System.EventHandler(this.IgnoreList_Click);
            // 
            // ArchivePath
            // 
            this.ArchivePath.AllowDrop = true;
            this.ArchivePath.Location = new System.Drawing.Point(12, 30);
            this.ArchivePath.Name = "ArchivePath";
            this.ArchivePath.Size = new System.Drawing.Size(659, 19);
            this.ArchivePath.TabIndex = 6;
            this.ArchivePath.TextChanged += new System.EventHandler(this.ArchivePath_TextChanged);
            this.ArchivePath.DragDrop += new System.Windows.Forms.DragEventHandler(this.ArchivePath_DragDrop);
            this.ArchivePath.DragEnter += new System.Windows.Forms.DragEventHandler(this.ArchivePath_DragEnter);
            // 
            // ArchivePathButton
            // 
            this.ArchivePathButton.Location = new System.Drawing.Point(677, 30);
            this.ArchivePathButton.Name = "ArchivePathButton";
            this.ArchivePathButton.Size = new System.Drawing.Size(44, 19);
            this.ArchivePathButton.TabIndex = 7;
            this.ArchivePathButton.Text = "参照...";
            this.ArchivePathButton.UseVisualStyleBackColor = true;
            this.ArchivePathButton.Click += new System.EventHandler(this.ArchivePathButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ファイルToolStripMenuItem,
            this.Help});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(132, 26);
            this.menuStrip1.Stretch = false;
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ファイルToolStripMenuItem
            // 
            this.ファイルToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Open,
            this.Exit});
            this.ファイルToolStripMenuItem.Name = "ファイルToolStripMenuItem";
            this.ファイルToolStripMenuItem.Size = new System.Drawing.Size(68, 22);
            this.ファイルToolStripMenuItem.Text = "ファイル";
            // 
            // Open
            // 
            this.Open.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Open.Name = "Open";
            this.Open.Size = new System.Drawing.Size(100, 22);
            this.Open.Text = "開く";
            this.Open.Click += new System.EventHandler(this.Open_Click);
            // 
            // Exit
            // 
            this.Exit.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(100, 22);
            this.Exit.Text = "終了";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // Help
            // 
            this.Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Version});
            this.Help.Name = "Help";
            this.Help.Size = new System.Drawing.Size(56, 22);
            this.Help.Text = "ヘルプ";
            // 
            // Version
            // 
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(160, 22);
            this.Version.Text = "バージョン情報";
            this.Version.Click += new System.EventHandler(this.Version_Click);
            // 
            // ArchiveListMenuStrip
            // 
            this.ArchiveListMenuStrip.Name = "ArchiveListMenuStrip";
            this.ArchiveListMenuStrip.Size = new System.Drawing.Size(153, 26);
            // 
            // ArchiveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 603);
            this.Controls.Add(this.ArchivePathButton);
            this.Controls.Add(this.ArchivePath);
            this.Controls.Add(this.IgnoreList);
            this.Controls.Add(this.Compress);
            this.Controls.Add(this.Package);
            this.Controls.Add(this.ArchiveList);
            this.Controls.Add(this.update);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "ArchiveForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ArchiveManager";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button update;
        private System.Windows.Forms.TreeView ArchiveList;
        private System.Windows.Forms.Button Package;
        private System.Windows.Forms.CheckBox Compress;
        private System.Windows.Forms.Button IgnoreList;
        private System.Windows.Forms.TextBox ArchivePath;
        private System.Windows.Forms.Button ArchivePathButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ファイルToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Open;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.ToolStripMenuItem Help;
        private System.Windows.Forms.ToolStripMenuItem Version;
        private System.Windows.Forms.ContextMenuStrip ArchiveListMenuStrip;



    }
}


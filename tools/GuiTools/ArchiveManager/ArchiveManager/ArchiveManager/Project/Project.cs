﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ArchiveManager.Project
{
    /// <summary>
    /// プロジェクトファイル
    /// </summary>
    [XmlRoot("Project")]
    public class ArchiveProject : Xml.ModelBase<ArchiveProject>
    {
        [XmlAttribute("Ver")]
        public string Ver = "1.0";

        [XmlElement("Ignore")]
        public Ignore Ignore = new Ignore();
        [XmlElement("Arichive")]
        public Archive Archive = new Archive();
    }

    /// <summary>
    /// 無視ファイル情報
    /// </summary>
    public class Ignore
    {
        [XmlElement("Extention")]
        public List<string> Extension = new List<string>();
        [XmlElement("Path")]
        public List<string> Path = new List<string>();
    }
    /// <summary>
    /// アーカイブ情報
    /// </summary>
    public class Archive
    {
        [XmlElement("Changed")]
        public bool Changed = true;
        [XmlElement("Directory")]
        public Directory Root = new Directory();
    }
    /// <summary>
    /// ディレクトリ情報
    /// </summary>
    public class Directory
    {
        [XmlAttribute("Name")]
        public string Name = "";

        [XmlElement("File")]
        public List<File> File = new List<File>();
        [XmlElement("Directory")]
        public List<Directory> SubDirectory = new List<Directory>();
    }
    /// <summary>
    /// ファイル情報
    /// </summary>
    public class File
    {
        [XmlAttribute("UpdateAt")]
        public string UpdateAt = "";
        [XmlAttribute("Changed")]
        public bool Changed = true;
        [XmlText()]
        public string Name = "";
    }
}

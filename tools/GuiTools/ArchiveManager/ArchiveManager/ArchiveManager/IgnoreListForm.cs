﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ArchiveManager
{
    public partial class IgnoreListForm : Form
    {
        private ListBox m_SelectListbox;

        public IgnoreListForm()
        {
            InitializeComponent();
        }
        public void Initialize(ArchiveManager Manager)
        {
            m_ArchiveManager = Manager;
        }

        private ArchiveManager m_ArchiveManager;

        private void AddExtension_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length > 0)
            {
                if (textBox1.Text[0] != '.')
                {
                    textBox1.Text = textBox1.Text.Insert(0, ".");
                }
                //  登録されていなければ登録する
                if (IgnoreExtensions.FindStringExact(textBox1.Text) == ListBox.NoMatches)
                {
                    IgnoreExtensions.Items.Add(textBox1.Text);
                }
                else
                {
                    MessageBox.Show("既に入力済みの拡張子です");
                }
                textBox1.Text = "";
            }
        }

        private void DeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (m_SelectListbox != null)
            {
                int Count = m_SelectListbox.SelectedIndices.Count;
                int[] Indices = new int[Count];
                m_SelectListbox.SelectedIndices.CopyTo(Indices, 0);
                //  選択中のアイテムを削除
                for (int i = Count - 1; i >= 0; --i)
                {
                    m_SelectListbox.Items.RemoveAt(Indices[i]);
                }
            }
        }

        private void listBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ListBox listbox = (ListBox)sender;
                if (listbox.SelectedIndices.Count <= 0)
                {
                    //  カーソル位置のアイテムを選択する
                    int Index = listbox.IndexFromPoint(e.Location);
                    if (Index != ListBox.NoMatches)
                    {
                        listbox.ClearSelected();
                        listbox.SelectedIndex = Index;
                    }
                }
                m_SelectListbox = listbox;
                contextMenuStrip1.Show(Cursor.Position);
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void IgnorePath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            //  既にパスが指定されている場合は選択しているパスを設定する
            if (Directory.Exists(Path.GetDirectoryName(IgnorePath.Text)))
            {
                dialog.InitialDirectory = IgnorePath.Text;
            }
            else
            {
                dialog.InitialDirectory = Directory.GetCurrentDirectory();
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                IgnoreFile.Text = dialog.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            //  既にパスが指定されている場合は選択しているパスを設定する
            if (Directory.Exists(Path.GetDirectoryName(IgnorePath.Text)))
            {
                dialog.SelectedPath = IgnorePath.Text;
            }
            else
            {
                dialog.SelectedPath = Directory.GetCurrentDirectory();
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                IgnoreFile.Text = dialog.SelectedPath;
            }
        }

        private void AddFilePath_Click(object sender, EventArgs e)
        {
            bool bWarning = true;
            if (Directory.Exists(IgnoreFile.Text))
            {
                bWarning = false;
            }
            else if (File.Exists(IgnoreFile.Text))
            {
                bWarning = false;
            }
            if (!bWarning)
            {
                Uri sPath1 = new Uri(Directory.GetCurrentDirectory());
                Uri sPath2 = new Uri(sPath1, IgnoreFile.Text);
                string FilePath = sPath1.MakeRelativeUri(sPath2).ToString();
                FilePath = FilePath.Replace("/", @"\");

                if (FilePath.IndexOf("..") >= 0 || FilePath.Length <= 0)
                {
                    MessageBox.Show("ルートディレクトリとそれ以下のフォルダは指定できません");
                }
                else
                {
                    //  登録されていなければ登録する
                    if (IgnoreFiles.FindStringExact(FilePath) == ListBox.NoMatches)
                    {
                        IgnoreFiles.Items.Add(FilePath);
                    }
                    else
                    {
                        MessageBox.Show("既に入力済みのパスです");
                    }
                }
                IgnoreFile.Text = "";
            }
            else
            {
                MessageBox.Show("不正なパスが入力されました", "Warning");
            }
        }
    }
}

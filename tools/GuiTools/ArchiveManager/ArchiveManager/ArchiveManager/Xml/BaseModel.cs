﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace ArchiveManager.Xml
{
    /// <summary>
    /// 全ての元になるXMLモデルオブジェクト
    /// </summary>
    public abstract class ModelObject
    {
        /// <summary>
        /// XMLファイルの保存
        /// </summary>
        /// <param name="Path">ファイルパス</param>
        /// <returns>結果</returns>
        public abstract bool Save(string Path = null);
    }

    /// <summary>
    /// ベースのXMLオブジェクト
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ModelBase<T> : ModelObject where T : ModelBase<T>
    {
        /// <summary>
        /// XMLファイルの保存
        /// </summary>
        /// <param name="Path">ファイルパス</param>
        /// <returns>結果</returns>
        public override bool Save(string Path = null)
        {
            if (Path == null)
            {
                Path = m_FilePath;
            }
            XmlConverter<T> Converter = new XmlConverter<T>();

            return Converter.Serialize(Path, this);
        }
        /// <summary>
        /// XMLファイルの読み込み
        /// </summary>
        /// <param name="Path">ファイルパス</param>
        /// <returns>XMLオブジェクト</returns>
        static public T Load(string Path)
        {
            XmlConverter<T> Converter = new XmlConverter<T>();

            T obj = Converter.Deserialize(Path);
            if (obj != null)
            {
                obj.m_FilePath = Path;
            }
            return obj;
        }

        private string m_FilePath = null;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Threading;

namespace ArchiveManager.Xml
{
    /// <summary>
    /// XMLコンバーター
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class XmlConverter<T>
        where T : ModelBase<T>
    {
        /// <summary>
        /// XMLオブジェクトをXMLファイルとして出力
        /// </summary>
        /// <param name="path">出力パス</param>
        /// <param name="obj">XMLオブジェクト</param>
        /// <returns>結果</returns>
        public bool Serialize(string path, ModelBase<T> obj)
        {
            bool result = true;
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs, Encoding.UTF8);

                Serialize(sw, obj);
            }
            catch (Exception e)
            {
                Debug.Log("XMLの出力に失敗しました(" + path + ")", e);
                result = false;
            }
            if (sw != null) { sw.Close(); sw = null; }
            if (fs != null) { fs.Close(); fs = null; }

            return result;
        }
        /// <summary>
        /// XMLクラスオブジェクトをXMLに変換
        /// </summary>
        /// <param name="writer">出力クラス</param>
        /// <param name="obj">XMLクラスオブジェクト</param>
        public void Serialize(TextWriter writer, ModelBase<T> obj)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(String.Empty, String.Empty);

            serializer.Serialize(writer, obj, ns);
        }
        
        /// <summary>
        /// XMLファイルをXMLクラスオブジェクトに変換
        /// </summary>
        /// <param name="path">ファイルパス</param>
        /// <returns>XMLクラスオブジェクト</returns>
        public T Deserialize(string path)
        {
            FileStream fs = null;
            T obj = default(T);

            try
            {
                fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                obj = Deserialize(fs);
            }
            catch (Exception e)
            {
                Debug.Log("XMLの変換に失敗しました(" + path + ")", e);
            }
            if (fs != null) { fs.Close(); fs = null; }

            return obj;
        }
        /// <summary>
        /// XMLをXMLクラスオブジェクトに変換
        /// </summary>
        /// <param name="stream">データストリーム</param>
        /// <returns>XMLクラスオブジェクト</returns>
        public T Deserialize(Stream stream)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            return (T)serializer.Deserialize(stream);
        }
    }
}

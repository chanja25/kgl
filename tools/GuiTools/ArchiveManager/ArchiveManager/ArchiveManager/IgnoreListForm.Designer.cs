﻿namespace ArchiveManager
{
    partial class IgnoreListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.IgnoreExtensions = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.AddExtension = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.AddFilePath = new System.Windows.Forms.Button();
            this.IgnoreFile = new System.Windows.Forms.TextBox();
            this.IgnoreFiles = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.CloseForm = new System.Windows.Forms.Button();
            this.IgnorePath = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IgnoreExtensions
            // 
            this.IgnoreExtensions.FormattingEnabled = true;
            this.IgnoreExtensions.ItemHeight = 12;
            this.IgnoreExtensions.Location = new System.Drawing.Point(12, 53);
            this.IgnoreExtensions.Name = "IgnoreExtensions";
            this.IgnoreExtensions.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.IgnoreExtensions.Size = new System.Drawing.Size(524, 76);
            this.IgnoreExtensions.TabIndex = 0;
            this.IgnoreExtensions.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeleteToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 26);
            // 
            // DeleteToolStripMenuItem
            // 
            this.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem";
            this.DeleteToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.DeleteToolStripMenuItem.Text = "削除";
            this.DeleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(85, 19);
            this.textBox1.TabIndex = 1;
            // 
            // AddExtension
            // 
            this.AddExtension.Location = new System.Drawing.Point(103, 24);
            this.AddExtension.Name = "AddExtension";
            this.AddExtension.Size = new System.Drawing.Size(43, 23);
            this.AddExtension.TabIndex = 2;
            this.AddExtension.Text = "追加";
            this.AddExtension.UseVisualStyleBackColor = true;
            this.AddExtension.Click += new System.EventHandler(this.AddExtension_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "無視する拡張子";
            // 
            // AddFilePath
            // 
            this.AddFilePath.Location = new System.Drawing.Point(493, 162);
            this.AddFilePath.Name = "AddFilePath";
            this.AddFilePath.Size = new System.Drawing.Size(43, 23);
            this.AddFilePath.TabIndex = 6;
            this.AddFilePath.Text = "追加";
            this.AddFilePath.UseVisualStyleBackColor = true;
            this.AddFilePath.Click += new System.EventHandler(this.AddFilePath_Click);
            // 
            // IgnoreFile
            // 
            this.IgnoreFile.Location = new System.Drawing.Point(12, 164);
            this.IgnoreFile.Name = "IgnoreFile";
            this.IgnoreFile.Size = new System.Drawing.Size(475, 19);
            this.IgnoreFile.TabIndex = 5;
            // 
            // IgnoreFiles
            // 
            this.IgnoreFiles.FormattingEnabled = true;
            this.IgnoreFiles.ItemHeight = 12;
            this.IgnoreFiles.Location = new System.Drawing.Point(12, 219);
            this.IgnoreFiles.Name = "IgnoreFiles";
            this.IgnoreFiles.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.IgnoreFiles.Size = new System.Drawing.Size(524, 112);
            this.IgnoreFiles.TabIndex = 4;
            this.IgnoreFiles.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBox_MouseUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "無視するファイル(ディレクトリ可)";
            // 
            // CloseForm
            // 
            this.CloseForm.Location = new System.Drawing.Point(461, 337);
            this.CloseForm.Name = "CloseForm";
            this.CloseForm.Size = new System.Drawing.Size(75, 23);
            this.CloseForm.TabIndex = 8;
            this.CloseForm.Text = "閉じる";
            this.CloseForm.UseVisualStyleBackColor = true;
            this.CloseForm.Click += new System.EventHandler(this.Close_Click);
            // 
            // IgnorePath
            // 
            this.IgnorePath.Location = new System.Drawing.Point(12, 189);
            this.IgnorePath.Name = "IgnorePath";
            this.IgnorePath.Size = new System.Drawing.Size(85, 23);
            this.IgnorePath.TabIndex = 9;
            this.IgnorePath.Text = "ファイル参照...";
            this.IgnorePath.UseVisualStyleBackColor = true;
            this.IgnorePath.Click += new System.EventHandler(this.IgnorePath_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(103, 189);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "フォルダ参照...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // IgnoreListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(548, 372);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.IgnorePath);
            this.Controls.Add(this.CloseForm);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AddFilePath);
            this.Controls.Add(this.IgnoreFile);
            this.Controls.Add(this.IgnoreFiles);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AddExtension);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.IgnoreExtensions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IgnoreListForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "IgnoreListForm";
            this.TopMost = true;
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button AddExtension;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AddFilePath;
        private System.Windows.Forms.TextBox IgnoreFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem DeleteToolStripMenuItem;
        public System.Windows.Forms.ListBox IgnoreExtensions;
        public System.Windows.Forms.ListBox IgnoreFiles;
        private System.Windows.Forms.Button CloseForm;
        private System.Windows.Forms.Button IgnorePath;
        private System.Windows.Forms.Button button1;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace FontConverter
{
    //! バージョン
    static class ConverterVersion
    {
        const int Major = 1; //!< メジャーバージョン
        const int Minor = 0; //!< マイナーバージョン

        //! バージョンの取得
        static public int Version()
        {
            return (int)(((Major << 16) & 0xffff0000) | (Minor & 0x0000ffff));
        }
    }

    //! 属性
    public enum EAttribute
    {
        DefaultChars,   //!< 基本的な英数字や記号を含めるか？
        Bold,           //!< 太字か？
        Italic,         //!< 斜体か？
        LightShadow,    //!< 軽量の影にするか？
        Shadow,         //!< 影を落とすか？
        AlphaOutline,   //!< 外枠を付けるか？
        DistanceField,  //!< ディスタンスフィールドか？
    }
    //! コンバート情報
    public struct FontConvertInfo
    {
        public string strFont;          //!< フォント
        public int iTextureWidth;       //!< テクスチャーの横幅
        public int iTextureHeight;      //!< テクスチャーの高さ
        public int iCharMarginX;        //!< 幅の余白(テクスチャーの文字間)
        public int iCharMarginY;        //!< 高さの余白(テクスチャーの文字間)
        public int iCharPaddingX;       //!< 幅の余白(描画時の文字間)
        public int iCharPaddingY;       //!< 高さの余白(描画時の文字間)
        public int iCharSet;            //!< 文字コードの種類
        public int iHeight;             //!< 高さ
        public int iAttribute;          //!< 属性(EAttribute参照)
        public string strCharacterText; //!< 生成する文字のリストが記述されたテキストへのパス
        public string strOutputPath;    //!< 出力パス
    }
    //! プロジェクト情報
    public struct FontProjectInfo
    {
        public int iVersion;                //!< バージョン
        public string strConvertUser;       //!< コンバートしたユーザー
        public FontConvertInfo ConvertInfo; //!< コンバート情報
    };

    public class FontConverter
    {
        public string m_strPath;               //!< プロジェクトへのパス
        private FontProjectInfo m_ProjectInfo;  //!< プロジェクト情報

        //  プロジェクトの生成
        public bool CreateProject(string strPath)
        {
            m_strPath = strPath;

            return true;
        }
        //  プロジェクトを開く
        public bool OpenProject(string strPath)
        {
            if (!File.Exists(strPath))
            {
                MessageBox.Show("ファイルが見つかりません\n正しいパスを設定してください\n[" + strPath + "]", "エラー");
                return false;
            }

            m_strPath = strPath;
            //  プロジェクトの読み込み
            ReadProject(strPath);

            return true;
        }
        //  プロジェクトを読み込む
        public void ReadProject(string strPath)
        {
            Encoding enc = Encoding.GetEncoding("shift_jis");

            FileStream fs = new FileStream(m_strPath, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);

            //  バージョンの取得
            m_ProjectInfo.iVersion = br.ReadInt32();

            //  更新日時
            byte[] ConvertUserName = br.ReadBytes(64);
            m_ProjectInfo.strConvertUser = enc.GetString(ConvertUserName).TrimEnd('\0');

            FontConvertInfo ConvertInfo;
            //  コンバート情報
            byte[] Font = br.ReadBytes(64);
            ConvertInfo.strFont = enc.GetString(Font).TrimEnd('\0');

            ConvertInfo.iTextureWidth = br.ReadInt32();
            ConvertInfo.iTextureHeight = br.ReadInt32();
            ConvertInfo.iCharMarginX = br.ReadInt32();
            ConvertInfo.iCharMarginY = br.ReadInt32();
            ConvertInfo.iCharPaddingX = br.ReadInt32();
            ConvertInfo.iCharPaddingY = br.ReadInt32();
            ConvertInfo.iCharSet = br.ReadInt32();
            ConvertInfo.iHeight = br.ReadInt32();
            ConvertInfo.iAttribute = br.ReadInt32();

            byte[] CharacterText = br.ReadBytes(256);
            ConvertInfo.strCharacterText = enc.GetString(CharacterText).TrimEnd('\0');

            byte[] OutputPath = br.ReadBytes(256);
            ConvertInfo.strOutputPath = enc.GetString(OutputPath).TrimEnd('\0');

            br.Close();
            fs.Close();

            m_ProjectInfo.ConvertInfo = ConvertInfo;
        }
        //  プロジェクトを保存する
        public bool SaveProject()
        {
            if (m_strPath.Length == 0)
            {
                return false;
            }

            FontConvertInfo ConvertInfo = m_ProjectInfo.ConvertInfo;
            //  文字列がバッファオーバーしている
            if (ConvertInfo.strFont.Length == 0 || ConvertInfo.strFont.Length >= 64)
            {
                return false;
            }
            if (ConvertInfo.strOutputPath.Length == 0 || ConvertInfo.strOutputPath.Length >= 256)
            {
                return false;
            }

            Encoding enc = Encoding.GetEncoding("shift_jis");

            FileStream fs;
            try
            {
                fs  = new FileStream(m_strPath, FileMode.Create, FileAccess.Write);
            }
            catch (System.UnauthorizedAccessException e)
            {
                MessageBox.Show("ファイルのオープンに失敗しました\n" + e.Message + "\n[" + m_strPath + "]", "エラー");
                return false;
            }

            BinaryWriter bw = new BinaryWriter(fs);

            //  バージョン
            bw.Write(ConverterVersion.Version());

            //  更新ユーザー
            byte[] ConvertUser = enc.GetBytes(Environment.UserName);
            Array.Resize(ref ConvertUser, 64);
            bw.Write(ConvertUser);

            //  コンバート情報
            byte[] Font = enc.GetBytes(ConvertInfo.strFont);
            Array.Resize(ref Font, 64);
            bw.Write(Font);

            bw.Write(ConvertInfo.iTextureWidth);
            bw.Write(ConvertInfo.iTextureHeight);
            bw.Write(ConvertInfo.iCharMarginX);
            bw.Write(ConvertInfo.iCharMarginY);
            bw.Write(ConvertInfo.iCharPaddingX);
            bw.Write(ConvertInfo.iCharPaddingY);
            bw.Write(ConvertInfo.iCharSet);
            bw.Write(ConvertInfo.iHeight);
            bw.Write(ConvertInfo.iAttribute);

            byte[] CharacterText = enc.GetBytes(ConvertInfo.strCharacterText);
            Array.Resize(ref CharacterText, 256);
            bw.Write(CharacterText);
            byte[] OutputPath = enc.GetBytes(ConvertInfo.strOutputPath);
            Array.Resize(ref OutputPath, 256);
            bw.Write(OutputPath);

            bw.Close();
            fs.Close();

            return true;
        }

        //  フォントのコンバート
        public bool FontConvert()
        {
            if (SaveProject())
            {
                ProcessStartInfo psi = new ProcessStartInfo();

                string strExePath = Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]);

                psi.FileName = strExePath + @"\bin\FontConverter.exe";
                psi.Arguments = m_strPath;
                Process converter = new Process();
                converter.StartInfo = psi;

                string strCurrentDir = Directory.GetCurrentDirectory();

                Directory.SetCurrentDirectory(Path.GetDirectoryName(m_strPath));

                if (converter.Start())
                {
                    converter.WaitForExit();
                }

                Directory.SetCurrentDirectory(strCurrentDir);

                return true;
            }
            return false;
        }
        //  フォントのコンバート
        public FontProjectInfo GetFontProjectInfo()
        {
            return m_ProjectInfo;
        }
        //  フォントのコンバート
        public void SetFontProjectInfo(string strPath, FontProjectInfo Info)
        {
            m_strPath = strPath;
            m_ProjectInfo = Info;
        }
    }
}

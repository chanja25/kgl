﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

namespace FontConverter
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            FontConverter converter = new FontConverter();
            StartupForm startup = new StartupForm(converter);

            bool bOpen = false;
            string strPath = null;
            if (Environment.GetCommandLineArgs().Length > 1)
            {
                strPath = Environment.GetCommandLineArgs()[1];
                if (Path.GetExtension(strPath).ToUpper() == ".fntproj".ToUpper() )
                {
                    bOpen = converter.OpenProject(strPath);
                }
            }

            if (!bOpen)
            {
                startup.ShowDialog();
            }
            else
            {
                startup.m_strProjectPath = strPath;
                startup.m_bCreate = !bOpen;
            }
            if (startup.m_strProjectPath != null)
            {
                Application.Run(new FontConverterFrom(startup));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FontConverter
{
    public partial class FontConverterFrom : Form
    {
        private bool m_bCreateNew;
        private string m_strStartupPath;
        private FontConverter m_FontConverter;

        public FontConverterFrom(StartupForm form)
        {
            InitializeComponent();

            m_bCreateNew = form.m_bCreate;
            m_strStartupPath = form.m_strProjectPath;
            m_FontConverter = form.m_FontConverter;
        }

        private void FontConverterForm_Load(object sender, EventArgs e)
        {
            InstalledFontCollection installedfonts = new InstalledFontCollection();
            FontFamily[] fonts = installedfonts.Families;
            foreach (FontFamily ff in fonts)
            {
                FontListBox.Items.Add(ff.Name);
            }

            ConvertUser.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent2);
            textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            textBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            textBox7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            textBox8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent);
            comboBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.KeyPressEvent2);

            FontListBox.TextChanged += new System.EventHandler(this.FontPropertyChanged);
            textBox8.TextChanged += new System.EventHandler(this.FontPropertyChanged);
            checkBox2.CheckedChanged += new System.EventHandler(this.FontPropertyChanged);
            checkBox3.CheckedChanged += new System.EventHandler(this.FontPropertyChanged);

            toolTip1.SetToolTip(label3, "テクスチャーの縦幅");
            toolTip1.SetToolTip(label4, "テクスチャーの横幅");
            toolTip1.SetToolTip(label5, "テクスチャーに書き込む文字の左右の余白");
            toolTip1.SetToolTip(label6, "テクスチャーに書き込む文字の上下の余白");
            toolTip1.SetToolTip(label7, "表示する文字の左右の文字間");
            toolTip1.SetToolTip(label8, "表示する文字の上下の文字間");
            toolTip1.SetToolTip(label9, "文字サイズ");
            toolTip1.SetToolTip(label10, "使用する際の文字コード(UTF-16以外の場合はコンバートテーブルを作成します)");

            toolTip1.SetToolTip(textBox2, "テクスチャーの縦幅");
            toolTip1.SetToolTip(textBox3, "テクスチャーの横幅");
            toolTip1.SetToolTip(textBox4, "テクスチャーに書き込む文字の左右の余白");
            toolTip1.SetToolTip(textBox5, "テクスチャーに書き込む文字の上下の余白");
            toolTip1.SetToolTip(textBox6, "表示する文字の左右の文字間");
            toolTip1.SetToolTip(textBox7, "表示する文字の上下の文字間");
            toolTip1.SetToolTip(textBox8, "文字サイズ");
            toolTip1.SetToolTip(comboBox1, "使用する際の文字コード(UTF-16以外の場合はコンバートテーブルを作成します)");

            toolTip1.SetToolTip(checkBox1, "基本的な英数字を含めるか？");
            toolTip1.SetToolTip(checkBox2, "太字にするか？");
            toolTip1.SetToolTip(checkBox3, "斜体にするか？");
            toolTip1.SetToolTip(checkBox4, "未実装");
            toolTip1.SetToolTip(checkBox5, "未実装");
            toolTip1.SetToolTip(checkBox6, "未実装");

            comboBox1.Items.Add("Shift-JIS");
            comboBox1.Items.Add("UTF-16");

            InitializeProperty();

            if (m_strStartupPath.Length != 0)
            {
                ProjectPath.Text = m_strStartupPath;
                if (!m_bCreateNew)
                {
                    ReadConvertInfo();
                }
            }
        }
        //  プロパティーの初期化
        private void InitializeProperty()
        {
            FontListBox.Text = "MS UI Gothic";

            textBox2.Text = "512";
            textBox3.Text = "512";
            textBox4.Text = "0";
            textBox5.Text = "0";
            textBox6.Text = "0";
            textBox7.Text = "0";
            textBox8.Text = "24";

            comboBox1.SelectedIndex = 0;

            checkBox1.Checked = true;
            checkBox2.Checked = false;
            checkBox3.Checked = false;
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox6.Checked = false;

            InputCharPath.Text = "";
            OutputFilePath.Text = "";
        }

        //  プロジェクトを開く
        private void OpenProject_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Font Project(*.fntproj)|*.fntproj";
            dialog.FilterIndex = 1;
            dialog.InitialDirectory = Path.GetDirectoryName(m_FontConverter.m_strPath);

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (m_FontConverter.OpenProject(dialog.FileName))
                {
                    ProjectPath.Text = dialog.FileName;
                    ReadConvertInfo();
                }
            }
        }

        private void CreateNewProject_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Font Project(*.fntproj)|*.fntproj";
            dialog.Title = "保存するファイル名を指定してください";
            dialog.FilterIndex = 1;
            dialog.InitialDirectory = Path.GetDirectoryName(m_FontConverter.m_strPath);

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (m_FontConverter.CreateProject(dialog.FileName))
                {
                    ProjectPath.Text = dialog.FileName;

                    InitializeProperty();
                }
            }
        }

        private void CloseApplication_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void KeyPressEvent(object sender, KeyPressEventArgs e)
        {
            //0～9と、バックスペース以外の時は、イベントをキャンセルする
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }
        private void KeyPressEvent2(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void RefCharPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Text File(*.txt)|*.txt|All Files(*.*)|*.*";
            dialog.FilterIndex = 1;
            if (File.Exists(InputCharPath.Text))
            {
                dialog.InitialDirectory = Path.GetDirectoryName(InputCharPath.Text);
            }
            else
            {
                dialog.InitialDirectory = Path.GetDirectoryName(m_FontConverter.m_strPath);
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (m_FontConverter.OpenProject(dialog.FileName))
                {
                    InputCharPath.Text = dialog.FileName;
                }
            }
        }

        private void SelectOutputPath_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Texture Font (*.ktf)|*.ktf";
            dialog.FilterIndex = 1;
            dialog.Title = "保存するファイル名を指定してください";
            dialog.OverwritePrompt = false;
            if (Directory.Exists(OutputFilePath.Text))
            {
                dialog.InitialDirectory = Path.GetDirectoryName(OutputFilePath.Text);
            }
            else
            {
                dialog.InitialDirectory = Path.GetDirectoryName(m_FontConverter.m_strPath);
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                OutputFilePath.Text = dialog.FileName;
            }
        }

        private void StartConvert_Click(object sender, EventArgs e)
        {
            if (OutputFilePath.Text.Length > 0)
            {
                SetupConvertInfo();
                //  フォントのコンバート(プロジェクトの保存も行う)
                if (m_FontConverter.FontConvert())
                {
                    ReadConvertInfo();
                    MessageBox.Show("フォントのコンバートが完了しました", "完了");
                }
            }
            else
            {
                MessageBox.Show("出力するパスが指定されていません\nパスを指定してください", "警告");
            }
        }

        private void FontPropertyChanged(object sender, EventArgs e)
        {
            FontStyle eStyle = FontStyle.Regular;
            eStyle |= checkBox2.Checked ? FontStyle.Bold : 0;
            eStyle |= checkBox3.Checked ? FontStyle.Italic : 0;

            Font font;
            try
            {
                font = new Font(FontListBox.Text, int.Parse(textBox8.Text), eStyle);
            }
            catch
            {
                font = new Font("MS UI Gothic", 12);
            }
            FontTestText.Font = font;
        }

        private void TextReset_Click(object sender, EventArgs e)
        {
            FontTestText.Text = "テスト test TEST";
        }

        private void SetupConvertInfo()
        {
            FontConvertInfo ConvertInfo;

            int iAttribute = 0;
            iAttribute |= checkBox1.Checked ? (1 << (int)EAttribute.DefaultChars) : 0;
            iAttribute |= checkBox2.Checked ? (1 << (int)EAttribute.Bold) : 0;
            iAttribute |= checkBox3.Checked ? (1 << (int)EAttribute.Italic) : 0;
            iAttribute |= checkBox4.Checked ? (1 << (int)EAttribute.Shadow) : 0;
            iAttribute |= checkBox5.Checked ? (1 << (int)EAttribute.AlphaOutline) : 0;
            iAttribute |= checkBox6.Checked ? (1 << (int)EAttribute.DistanceField) : 0;
            
            ConvertInfo.strFont = FontListBox.Text;
            ConvertInfo.iTextureWidth = int.Parse(textBox2.Text);
            ConvertInfo.iTextureHeight = int.Parse(textBox3.Text);
            ConvertInfo.iCharMarginX = int.Parse(textBox4.Text);
            ConvertInfo.iCharMarginY = int.Parse(textBox5.Text);
            ConvertInfo.iCharPaddingX = int.Parse(textBox6.Text);
            ConvertInfo.iCharPaddingY = int.Parse(textBox7.Text);
            ConvertInfo.iCharSet = comboBox1.SelectedIndex;
            ConvertInfo.iHeight = int.Parse(textBox8.Text);
            ConvertInfo.iAttribute = iAttribute;
            ConvertInfo.strCharacterText = InputCharPath.Text;
            ConvertInfo.strOutputPath = OutputFilePath.Text;

            FontProjectInfo Info;

            Info.iVersion = ConverterVersion.Version();
            Info.strConvertUser = Environment.UserName;
            Info.ConvertInfo = ConvertInfo;

            m_FontConverter.SetFontProjectInfo(ProjectPath.Text, Info);
        }
        private void ReadConvertInfo()
        {
            FontProjectInfo Info = m_FontConverter.GetFontProjectInfo();

            string strConvertUser;
            strConvertUser = Info.strConvertUser;

            ConvertUser.Text = strConvertUser;

            FontListBox.Text = Info.ConvertInfo.strFont;
            textBox2.Text = Info.ConvertInfo.iTextureWidth.ToString();
            textBox3.Text = Info.ConvertInfo.iTextureHeight.ToString();
            textBox4.Text = Info.ConvertInfo.iCharMarginX.ToString();
            textBox5.Text = Info.ConvertInfo.iCharMarginY.ToString();
            textBox6.Text = Info.ConvertInfo.iCharPaddingX.ToString();
            textBox7.Text = Info.ConvertInfo.iCharPaddingY.ToString();
            textBox8.Text = Info.ConvertInfo.iHeight.ToString();

            comboBox1.SelectedIndex = Info.ConvertInfo.iCharSet;
            int iAttribute = Info.ConvertInfo.iAttribute;
            checkBox1.Checked = (iAttribute & (1 << (int)EAttribute.DefaultChars)) != 0;
            checkBox2.Checked = (iAttribute & (1 << (int)EAttribute.Bold)) != 0;
            checkBox3.Checked = (iAttribute & (1 << (int)EAttribute.Italic)) != 0;
            checkBox4.Checked = (iAttribute & (1 << (int)EAttribute.Shadow)) != 0;
            checkBox5.Checked = (iAttribute & (1 << (int)EAttribute.AlphaOutline)) != 0;
            checkBox6.Checked = (iAttribute & (1 << (int)EAttribute.DistanceField)) != 0;

            InputCharPath.Text = Info.ConvertInfo.strCharacterText;
            OutputFilePath.Text = Info.ConvertInfo.strOutputPath;
        }

        private void FontConverterFrom_DragDrop(object sender, DragEventArgs e)
        {
            string strPath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            if (m_FontConverter.OpenProject(strPath))
            {
                ProjectPath.Text = strPath;
                ReadConvertInfo();
            }
        }

        private void FontConverterFrom_DragEnter(object sender, DragEventArgs e)
        {
            bool bEnable = false;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] s = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (s.Length == 1)
                {
                    if (Path.GetExtension(s[0]).ToLower() == ".fntproj")
                    {
                        bEnable = true;
                    }
                }
            }

            e.Effect = bEnable ? DragDropEffects.Move : DragDropEffects.None;
        }
    }
}

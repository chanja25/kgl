﻿namespace FontConverter
{
    partial class FontConverterFrom
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FontConverterFrom));
            this.ProjectPath = new System.Windows.Forms.TextBox();
            this.OpenProject = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ファイルToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateNewProject = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseApplication = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.FontListBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ConvertUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.RefCharPath = new System.Windows.Forms.Button();
            this.InputCharPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.SelectOutputPath = new System.Windows.Forms.Button();
            this.OutputFilePath = new System.Windows.Forms.TextBox();
            this.FontTestText = new System.Windows.Forms.RichTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TextReset = new System.Windows.Forms.ToolStripMenuItem();
            this.label13 = new System.Windows.Forms.Label();
            this.StartConvert = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProjectPath
            // 
            this.ProjectPath.AccessibleDescription = "";
            this.ProjectPath.AccessibleName = "";
            this.ProjectPath.Location = new System.Drawing.Point(12, 27);
            this.ProjectPath.Name = "ProjectPath";
            this.ProjectPath.ReadOnly = true;
            this.ProjectPath.Size = new System.Drawing.Size(381, 19);
            this.ProjectPath.TabIndex = 0;
            // 
            // OpenProject
            // 
            this.OpenProject.Location = new System.Drawing.Point(399, 25);
            this.OpenProject.Name = "OpenProject";
            this.OpenProject.Size = new System.Drawing.Size(94, 23);
            this.OpenProject.TabIndex = 1;
            this.OpenProject.Text = "プロジェクトを開く";
            this.OpenProject.UseVisualStyleBackColor = true;
            this.OpenProject.Click += new System.EventHandler(this.OpenProject_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ファイルToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(505, 26);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ファイルToolStripMenuItem
            // 
            this.ファイルToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateNewProject,
            this.CloseApplication});
            this.ファイルToolStripMenuItem.Name = "ファイルToolStripMenuItem";
            this.ファイルToolStripMenuItem.Size = new System.Drawing.Size(68, 22);
            this.ファイルToolStripMenuItem.Text = "ファイル";
            // 
            // CreateNewProject
            // 
            this.CreateNewProject.Name = "CreateNewProject";
            this.CreateNewProject.Size = new System.Drawing.Size(124, 22);
            this.CreateNewProject.Text = "新規作成";
            this.CreateNewProject.Click += new System.EventHandler(this.CreateNewProject_Click);
            // 
            // CloseApplication
            // 
            this.CloseApplication.Name = "CloseApplication";
            this.CloseApplication.Size = new System.Drawing.Size(124, 22);
            this.CloseApplication.Text = "閉じる";
            this.CloseApplication.Click += new System.EventHandler(this.CloseApplication_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(12, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "フォント";
            // 
            // FontListBox
            // 
            this.FontListBox.FormattingEnabled = true;
            this.FontListBox.Location = new System.Drawing.Point(62, 56);
            this.FontListBox.Name = "FontListBox";
            this.FontListBox.Size = new System.Drawing.Size(208, 20);
            this.FontListBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(305, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "更新者";
            // 
            // ConvertUser
            // 
            this.ConvertUser.Location = new System.Drawing.Point(364, 57);
            this.ConvertUser.Name = "ConvertUser";
            this.ConvertUser.ReadOnly = true;
            this.ConvertUser.Size = new System.Drawing.Size(129, 19);
            this.ConvertUser.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Texture Height";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "Texture Width";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "Character MarginX";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "Character MarginY";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 12);
            this.label7.TabIndex = 14;
            this.label7.Text = "Character PaddingX";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 12);
            this.label8.TabIndex = 13;
            this.label8.Text = "Character PaddingY";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 244);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "Character Height";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(13, 268);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 12);
            this.label10.TabIndex = 11;
            this.label10.Text = "CharSet";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(170, 97);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 19);
            this.textBox2.TabIndex = 15;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(170, 121);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 19);
            this.textBox3.TabIndex = 16;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(170, 169);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 19);
            this.textBox5.TabIndex = 18;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(170, 145);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 19);
            this.textBox4.TabIndex = 17;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(170, 241);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 19);
            this.textBox8.TabIndex = 21;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(170, 217);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 19);
            this.textBox7.TabIndex = 20;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(170, 193);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 19);
            this.textBox6.TabIndex = 19;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(170, 265);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 20);
            this.comboBox1.TabIndex = 22;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(318, 100);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(121, 16);
            this.checkBox1.TabIndex = 29;
            this.checkBox1.Text = "Default Characters";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(318, 125);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(47, 16);
            this.checkBox2.TabIndex = 30;
            this.checkBox2.Text = "Bold";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(318, 149);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(49, 16);
            this.checkBox3.TabIndex = 31;
            this.checkBox3.Text = "Italic";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(318, 173);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(107, 16);
            this.checkBox4.TabIndex = 32;
            this.checkBox4.Text = "Shadow(未実装)";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(318, 197);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(104, 16);
            this.checkBox5.TabIndex = 33;
            this.checkBox5.Text = "Outline(未実装)";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(318, 221);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(142, 16);
            this.checkBox6.TabIndex = 34;
            this.checkBox6.Text = "Distance Field(未実装)";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // RefCharPath
            // 
            this.RefCharPath.Location = new System.Drawing.Point(446, 307);
            this.RefCharPath.Name = "RefCharPath";
            this.RefCharPath.Size = new System.Drawing.Size(47, 23);
            this.RefCharPath.TabIndex = 36;
            this.RefCharPath.Text = "参照";
            this.RefCharPath.UseVisualStyleBackColor = true;
            this.RefCharPath.Click += new System.EventHandler(this.RefCharPath_Click);
            // 
            // InputCharPath
            // 
            this.InputCharPath.AccessibleDescription = "";
            this.InputCharPath.AccessibleName = "";
            this.InputCharPath.Location = new System.Drawing.Point(12, 309);
            this.InputCharPath.Name = "InputCharPath";
            this.InputCharPath.Size = new System.Drawing.Size(428, 19);
            this.InputCharPath.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 294);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(216, 12);
            this.label11.TabIndex = 37;
            this.label11.Text = "使用する文字列が記述されたテキストファイル";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 331);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 12);
            this.label12.TabIndex = 40;
            this.label12.Text = "出力パス";
            // 
            // SelectOutputPath
            // 
            this.SelectOutputPath.Location = new System.Drawing.Point(423, 344);
            this.SelectOutputPath.Name = "SelectOutputPath";
            this.SelectOutputPath.Size = new System.Drawing.Size(70, 23);
            this.SelectOutputPath.TabIndex = 39;
            this.SelectOutputPath.Text = "パスの指定";
            this.SelectOutputPath.UseVisualStyleBackColor = true;
            this.SelectOutputPath.Click += new System.EventHandler(this.SelectOutputPath_Click);
            // 
            // OutputFilePath
            // 
            this.OutputFilePath.AccessibleDescription = "";
            this.OutputFilePath.AccessibleName = "";
            this.OutputFilePath.Location = new System.Drawing.Point(12, 346);
            this.OutputFilePath.Name = "OutputFilePath";
            this.OutputFilePath.Size = new System.Drawing.Size(404, 19);
            this.OutputFilePath.TabIndex = 38;
            // 
            // FontTestText
            // 
            this.FontTestText.ContextMenuStrip = this.contextMenuStrip1;
            this.FontTestText.Location = new System.Drawing.Point(12, 440);
            this.FontTestText.Name = "FontTestText";
            this.FontTestText.Size = new System.Drawing.Size(481, 223);
            this.FontTestText.TabIndex = 42;
            this.FontTestText.Text = "テスト test TEST";
            this.FontTestText.WordWrap = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TextReset});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(185, 26);
            // 
            // TextReset
            // 
            this.TextReset.Name = "TextReset";
            this.TextReset.Size = new System.Drawing.Size(184, 22);
            this.TextReset.Text = "テキストをリセット";
            this.TextReset.Click += new System.EventHandler(this.TextReset_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 425);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 12);
            this.label13.TabIndex = 43;
            this.label13.Text = "出力されるフォントの確認用";
            // 
            // StartConvert
            // 
            this.StartConvert.Location = new System.Drawing.Point(384, 382);
            this.StartConvert.Name = "StartConvert";
            this.StartConvert.Size = new System.Drawing.Size(109, 23);
            this.StartConvert.TabIndex = 44;
            this.StartConvert.Text = "コンバート";
            this.StartConvert.UseVisualStyleBackColor = true;
            this.StartConvert.Click += new System.EventHandler(this.StartConvert_Click);
            // 
            // FontConverterFrom
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 675);
            this.Controls.Add(this.StartConvert);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.FontTestText);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.SelectOutputPath);
            this.Controls.Add(this.OutputFilePath);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.RefCharPath);
            this.Controls.Add(this.InputCharPath);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ConvertUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FontListBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OpenProject);
            this.Controls.Add(this.ProjectPath);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FontConverterFrom";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FontConverter";
            this.Load += new System.EventHandler(this.FontConverterForm_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.FontConverterFrom_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.FontConverterFrom_DragEnter);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ProjectPath;
        private System.Windows.Forms.Button OpenProject;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ファイルToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CreateNewProject;
        private System.Windows.Forms.ToolStripMenuItem CloseApplication;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox FontListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ConvertUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button RefCharPath;
        private System.Windows.Forms.TextBox InputCharPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button SelectOutputPath;
        private System.Windows.Forms.TextBox OutputFilePath;
        private System.Windows.Forms.RichTextBox FontTestText;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button StartConvert;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem TextReset;

    }
}


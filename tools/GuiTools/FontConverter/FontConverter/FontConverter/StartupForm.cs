﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FontConverter
{
    public partial class StartupForm : Form
    {
        public bool m_bCreate;
        public string m_strProjectPath;
        public FontConverter m_FontConverter;

        public StartupForm(FontConverter converter)
        {
            InitializeComponent();

            m_FontConverter = converter;
        }

        private void CreateNew_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Font Project(*.fntproj)|*.fntproj";
            dialog.Title = "保存するファイル名を指定してください";
            dialog.FilterIndex = 1;
            dialog.InitialDirectory = Directory.GetCurrentDirectory();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (m_FontConverter.CreateProject(dialog.FileName))
                {
                    m_strProjectPath = dialog.FileName;
                    this.Close();
                    m_bCreate = true;
                }
            }
        }

        private void OpenProject_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Font Project(*.fntproj)|*.fntproj";
            dialog.FilterIndex = 1;
            dialog.InitialDirectory = Directory.GetCurrentDirectory();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                if (m_FontConverter.OpenProject(dialog.FileName))
                {
                    m_strProjectPath = dialog.FileName;
                    this.Close();
                    m_bCreate = false;
                }
            }
        }

        private void StartupForm_DragDrop(object sender, DragEventArgs e)
        {
            string strPath = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            if (m_FontConverter.OpenProject(strPath))
            {
                m_strProjectPath = strPath;
                this.Close();
                m_bCreate = false;
            }
        }

        private void StartupForm_DragEnter(object sender, DragEventArgs e)
        {
            bool bEnable = false;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] s = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (s.Length == 1)
                {
                    if (Path.GetExtension(s[0]).ToLower() == ".fntproj")
                    {
                        bEnable = true;
                    }
                }
            }

            e.Effect = bEnable ? DragDropEffects.Move : DragDropEffects.None;
        }
    }
}

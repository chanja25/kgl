//----------------------------------------------------------------------
//!
//!	@file	MFByteStream.h
//!	@brief	ムービー管理(Media Foundation)
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__MF_BYTE_STREAM_H__
#define	__MF_BYTE_STREAM_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include <list>
#include <mfapi.h>
//#include <Mfidl.h>
//#include <Mferror.h>
//#include <evr.h>

namespace App
{
	//!	移動するオフセット位置の指定
	enum EStreamOffset
	{
		EStreamOffsetBegin,		//!< 先頭
		EStreamOffsetCurrent,	//!< 現在位置
		EStreamOffsetEnd,		//!< 終端
	};
	//======================================================================
	//!	ストリーム管理クラス
	//======================================================================
	class IStreamBufferBase
		: public IKGLBase
	{
	public:
		//!	コンストラクタ
		IStreamBufferBase(void) { AddRef(); }
		//!	デストラクタ
		virtual ~IStreamBufferBase(void) {}

	public:
		//!	ストリームの読み込み
		virtual s64 Read(void* pBuffer, s64 i64Size) = 0;
		//!	ストリームの位置を移動
		virtual b8 Seek(EStreamOffset eOffset, s64 i64Size) = 0;
		//!	現在の位置の取得
		virtual s64 Tell(void) = 0;

		//!	サイズの取得
		virtual s64 GetSize(void) = 0;
	};

	//======================================================================
	//!	ストリームファイル管理クラス
	//======================================================================
	class CStreamFile
		: public IStreamBufferBase
	{
	public:
		//!	ファイルを開く
		HRESULT Open(const c16* pFilename)
		{
			c8 cFilename[256];
			u32 uiSize = ArrayCount(cFilename);
			CharSet::ConvertUTF16ToShiftJIS(cFilename, pFilename, &uiSize);

			File::Create(m_pFile.GetReference());
			if( !m_pFile->Open(cFilename) )
			{
				m_pFile = null;
				return S_FALSE;
			}
			return S_OK;
		}

	public:
		//!	ストリームの読み込み
		s64 Read(void* pBuffer, s64 i64Size)
		{
			if( !m_pFile.IsValid() )
			{
				return -1;
			}
			return (s64)m_pFile->Read(pBuffer, (u32)i64Size);
		}
		//!	ストリームの位置を移動
		b8 Seek(EStreamOffset eOffset, s64 i64Size)
		{
			if( !m_pFile.IsValid() )
			{
				return 0;
			}
			s32 iOffset = File::EFilePointer::Begin;
			switch ( eOffset )
			{
			case EStreamOffsetCurrent:
				iOffset = File::EFilePointer::Current;
				break;
			}
			return m_pFile->Seek((s32)i64Size, iOffset);
		}
		//!	現在の位置の取得
		s64 Tell(void)
		{
			if( !m_pFile.IsValid() )
			{
				return 0;
			}
			return m_pFile->Tell();
		}

		//!	サイズの取得
		s64 GetSize(void)
		{
			if( !m_pFile.IsValid() )
			{
				return 0;
			}
			return m_pFile->GetSize();
		}

	private:
		TKGLPtr<File::IFile>	m_pFile;	//!< ファイル管理
	};

	//======================================================================
	//!	ストリームバイナリ管理クラス
	//======================================================================
	class CStreamBinary
		: public IStreamBufferBase
	{
	public:
		//!	コンストラクタ
		CStreamBinary(void)
			: m_pBuffer(null)
			, m_i64Size(0)
			, m_i64Offset(0)
		{}
		//!	ファイルを開く
		HRESULT Create(const void* pBuffer, s64 i64Size)
		{
			m_pBuffer	= (u8*)pBuffer;
			m_i64Size	= i64Size;
			m_i64Offset	= 0;
			return S_OK;
		}

	public:
		//!	ストリームの読み込み
		s64 Read(void* pBuffer, s64 i64Size)
		{
			auto pOffset = m_pBuffer + m_i64Offset;
			i64Size = Min(i64Size, m_i64Size - m_i64Offset);
			if( 0 < i64Size )
			{
				kgMemcpy(pBuffer, pOffset, i64Size);
			}
			return i64Size;
		}
		//!	ストリームの位置を移動
		b8 Seek(EStreamOffset eOffset, s64 i64Size)
		{
			switch ( eOffset )
			{
			case EStreamOffsetBegin:	m_i64Offset = i64Size;	break;
			case EStreamOffsetCurrent:	m_i64Offset += i64Size;	break;
			}
			m_i64Offset = Min(m_i64Offset, m_i64Size);
			return true;
		}
		//!	現在の位置の取得
		s64 Tell(void)
		{
			return m_i64Offset;
		}

		//!	サイズの取得
		s64 GetSize(void)
		{
			return m_i64Size;
		}

	private:
		const u8*	m_pBuffer;
		s64			m_i64Size;
		s64			m_i64Offset;
	};

	//======================================================================
	//!	ストリーム管理クラス
	//======================================================================
	class CMFByteStream
		: public IMFByteStream
	{
	public:
		//!	コンストラクタ
		CMFByteStream(TKGLPtr<IStreamBufferBase> pStreamBuffer);
		//!	デストラクタ
		virtual ~CMFByteStream(void);

	public:
		//	IUnknown関連
		STDMETHODIMP QueryInterface(REFIID riid, void** ppv);
		STDMETHODIMP_(ULONG) AddRef();
		STDMETHODIMP_(ULONG) Release();

	public:
		//!	生成
		HRESULT Create(void);

		//	IMFByteStream関連
		//!	ストリームの能力の取得
		STDMETHODIMP GetCapabilities(DWORD *pdwCapabilities);
		//!	ストリームのサイズを取得
		STDMETHODIMP GetLength(QWORD *pqwLength);
		//!	ストリームのサイズを設定
		STDMETHODIMP SetLength(QWORD qwLength);
		//!	ストリームの位置を取得
		STDMETHODIMP GetCurrentPosition(QWORD *pqwPosition);
		//!	ストリームの位置を設定
		STDMETHODIMP SetCurrentPosition(QWORD qwPosition);
		//!	ストリームの終端確認
		STDMETHODIMP IsEndOfStream(BOOL *pfEndOfStream);
		//!	同期読み込み
		STDMETHODIMP Read(BYTE *pb,ULONG cb,ULONG *pcbRead);
		//!	非同期読み込みの開始
		STDMETHODIMP BeginRead(BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState);
		//!	非同期読み込みの終了
		STDMETHODIMP EndRead(IMFAsyncResult *pResult,ULONG *pcbRead);
		//!	同期書き込み
		STDMETHODIMP Write(const BYTE *pb,ULONG cb,ULONG *pcbWritten);
		//!	非同期書き込みの開始
		STDMETHODIMP BeginWrite(const BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState);
		//!	非同期下記も未の終了
		STDMETHODIMP EndWrite(IMFAsyncResult *pResult,ULONG *pcbWritten);
		//!	ストリームの位置を移動
		STDMETHODIMP Seek(MFBYTESTREAM_SEEK_ORIGIN SeekOrigin,LONGLONG llSeekOffset,DWORD dwSeekFlags,QWORD *pqwCurrentPosition);
		//!	書き込みバッファのフラッシュ
		STDMETHODIMP Flush(void);
		//!	ストリームを閉じる
		STDMETHODIMP Close(void);

	protected:
		//!	バッファの読み込み
		HRESULT ReadBuffer(void* pBuffer, s64 i64Size, s32 i64ReadPos, s64 *pReadSize);
		//!	非同期処理
		u32 AsyncThreadFunc(void);
		//!	非同期処理
		static u32 THREADFUNC AsyncThreadFunc(void* pArg);

		//非同期処理アイテムの定義
		struct AsyncItem
		{
			TKGLPtr<IMFAsyncResult>	pResult;
			u8*						pBuffer;
			u32						uiSize;
			s64						i64Read;
			u64						i64ReadPos;
		};
		typedef algorithm::vector<AsyncItem> dequeAsyncItem;
     
	private:
		u32									m_uiRef;
		bool								m_bExit;
		s64									m_i64Offset;

		TKGLPtr<IStreamBufferBase>			m_pStreamBuffer;
		TKGLPtr<Thread::IThread>			m_pThread;
		TKGLPtr<Thread::IEvent>				m_pEvent;
		TKGLPtr<Thread::ICriticalSection>	m_pCSStream;
		TKGLPtr<Thread::ICriticalSection>	m_pCSQueue;
		TKGLPtr<IMFAttributes>				m_pAttributes;	//!< 属性値の管理
		dequeAsyncItem						m_dequeWorkItem; //未処理アイテムキュー
		dequeAsyncItem						m_dequeDoneItem; //処理済みアイテムキュー
	};
	
	class CMFByteStream2
		: public IMFByteStream
	{
	public:
		CMFByteStream2(HRESULT *lphr,const TCHAR *lpFileName); //コンストラクタ
		virtual ~CMFByteStream2(); //デストラクタ
		//IUnknownの機能を実装する
		STDMETHODIMP QueryInterface(REFIID riid, void** ppv);
		STDMETHODIMP_(ULONG) AddRef();
		STDMETHODIMP_(ULONG) Release();

		//IMFByteStreamの実装
		STDMETHODIMP GetCapabilities(DWORD *pdwCapabilities); //ストリームの能力を取得する
		STDMETHODIMP GetLength(QWORD *pqwLength); //ストリームのサイズを取得する
		STDMETHODIMP SetLength(QWORD qwLength); //ストリームのサイズを設定する
		STDMETHODIMP GetCurrentPosition(QWORD *pqwPosition); //ストリームの位置を取得する
		STDMETHODIMP SetCurrentPosition(QWORD qwPosition); //ストリームの位置を設定する
		STDMETHODIMP IsEndOfStream(BOOL *pfEndOfStream); //ストリームの終端かどうか
		STDMETHODIMP Read(BYTE *pb,ULONG cb,ULONG *pcbRead); //同期読み取りを行う
		STDMETHODIMP BeginRead(BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState); //非同期読み取りを開始する
		STDMETHODIMP EndRead(IMFAsyncResult *pResult,ULONG *pcbRead); //非同期読み取りを終了する
		STDMETHODIMP Write(const BYTE *pb,ULONG cb,ULONG *pcbWritten); //同期書き込みを行う
		STDMETHODIMP BeginWrite(const BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState); //非同期書き込みを開始する
		STDMETHODIMP EndWrite(IMFAsyncResult *pResult,ULONG *pcbWritten); //非同期書き込みを終了する
		STDMETHODIMP Seek(MFBYTESTREAM_SEEK_ORIGIN SeekOrigin,LONGLONG llSeekOffset,DWORD dwSeekFlags,QWORD *pqwCurrentPosition); //ストリーム位置を移動する
		STDMETHODIMP Flush(void); //書き込みバッファのフラッシュを行う
		STDMETHODIMP Close(void); //ストリームを閉じる
		HRESULT Open(void); //ストリームを開く
	protected:
		//内部ロックオブジェクト
		class CLockStream{
		public:
			CLockStream(CMFByteStream2 *obj) : m_obj(obj){ ::EnterCriticalSection(&obj->m_vCSStream); }
			~CLockStream() { ::LeaveCriticalSection(&m_obj->m_vCSStream); }
		private:
			CMFByteStream2 *m_obj;
		};
		class CLockQueue{
		public:
			CLockQueue(CMFByteStream2 *obj) : m_obj(obj){ ::EnterCriticalSection(&obj->m_vCSQueue); }
			~CLockQueue() { ::LeaveCriticalSection(&m_obj->m_vCSQueue); }
		private:
			CMFByteStream2 *m_obj;
		};
		//非同期処理アイテムの定義
		typedef struct _AsyncItem{
			IMFAsyncResult *lpResult;
			BYTE *pbBuffer;
			ULONG cbSize,cbRead;
			uint64_t nReadPos;
		} ASYNCITEM;
		typedef std::list<ASYNCITEM> dequeAsyncItem;
		HRESULT InnerRead(BYTE *pb,ULONG cb,ULONG *pcbRead,uint64_t nReadPos); //内部読み込み処理
		static unsigned int __stdcall AsyncThread(void *lpContext); //非同期処理スレッド
		inline bool IsValidStream(void) const { return m_hFileStream != NULL; } //ストリームが有効かどうか
		inline bool IsValidThread(void) const { return m_hAsyncThread != NULL; } //スレッドが有効かどうか
     
	private:
		u32	m_uiRef;
		bool	m_bExit;
		TCHAR *m_lpFileName; //ファイル名
		HANDLE m_hFileStream; //ファイルハンドル
		uint64_t m_ullStreamPos; //ストリーム位置
		uint64_t m_ullStreamSize; //ストリームサイズ
		CRITICAL_SECTION m_vCSStream; //ストリーム同期ハンドル
		HANDLE m_hAsyncThread; //非同期処理スレッド
		HANDLE m_hThreadEvent; //スレッドイベント
		CRITICAL_SECTION m_vCSQueue; //キューアイテム同期ハンドル
		dequeAsyncItem m_dequeWorkItem; //未処理アイテムキュー
		dequeAsyncItem m_dequeDoneItem; //処理済みアイテムキュー
		IMFAttributes *m_lpAttributes; //属性値管理
	};
}

#endif	// __MF_MOIVE_H__
//======================================================================
//	END OF FILE
//======================================================================
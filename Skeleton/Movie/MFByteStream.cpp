//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "../Movie/MFByteStream.h"


//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#ifndef CheckPointer
#define CheckPointer(p,ret) {if((p)==NULL) return (ret);}
#endif // !CheckPointer

#undef	CheckPointer
#define CheckPointer(p,ret) kgAssert(p, "no pointer"); {if((p)==NULL) return (ret);}

namespace App
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CMFByteStream::CMFByteStream(TKGLPtr<IStreamBufferBase> pStreamBuffer)
		: m_uiRef(0)
		, m_bExit(false)
		, m_i64Offset(0)
		, m_pStreamBuffer(pStreamBuffer)
	{
		AddRef();
	}
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CMFByteStream::~CMFByteStream(void)
	{
		Close();
	}
	//----------------------------------------------------------------------
	//	QueryInterface(IUnknown)
	//----------------------------------------------------------------------
	STDMETHODIMP CMFByteStream::QueryInterface(REFIID riid, void** ppv)
	{
		//	Attributesが必要な場合は内部の変数から返す
		if( IsEqualIID(riid, IID_IMFAttributes) )
		{
			return m_pAttributes->QueryInterface(riid, ppv);
		}
		static const QITAB qit[] = 
		{
			QITABENT(CMFByteStream, IMFByteStream),
			{ 0 }
		};
		return QISearch(this, qit, riid, ppv);
	}
	//----------------------------------------------------------------------
	//	AddRef(IUnknown)
	//----------------------------------------------------------------------
	STDMETHODIMP_(ULONG) CMFByteStream::AddRef()
	{
		return InterlockedIncrement(&m_uiRef);
	}
	//----------------------------------------------------------------------
	//	Release(IUnknown)
	//----------------------------------------------------------------------
	STDMETHODIMP_(ULONG) CMFByteStream::Release()
	{
		u32 uiRef = InterlockedDecrement(&m_uiRef);
		if( uiRef == 0 )
		{
			delete this;
		}
		return uiRef;
	}
	//	生成
	HRESULT CMFByteStream::Create(void)
	{
		HRESULT hr;
		hr = MFCreateAttributes(m_pAttributes.GetReference(), 0);
		if( SUCCEEDED(hr) )
		{
			Thread::Create(m_pThread.GetReference());
			Thread::Create(m_pEvent.GetReference());
			Thread::Create(m_pCSQueue.GetReference());
			Thread::Create(m_pCSStream.GetReference());

			m_bExit = false;
			m_pThread->Start(AsyncThreadFunc, this);
		}
		return hr;
	}
	//	ストリームの能力の取得
	STDMETHODIMP CMFByteStream::GetCapabilities(DWORD *pdwCapabilities)
	{
		CheckPointer(pdwCapabilities, E_POINTER);
		if( !m_pStreamBuffer.IsValid() )
		{
			return E_HANDLE;
		}
		*pdwCapabilities = MFBYTESTREAM_IS_READABLE | MFBYTESTREAM_IS_SEEKABLE;
		return S_OK;
	}
	//	ストリームのサイズを取得
	STDMETHODIMP CMFByteStream::GetLength(QWORD *pqwLength)
	{
		CheckPointer(pqwLength, E_POINTER);
		*pqwLength = m_pStreamBuffer->GetSize();
		return S_OK;
	}
	//	ストリームのサイズを設定
	STDMETHODIMP CMFByteStream::SetLength(QWORD qwLength)
	{
		return E_NOTIMPL;
	}
	//	ストリームの位置を取得
	STDMETHODIMP CMFByteStream::GetCurrentPosition(QWORD *pqwPosition)
	{
		CheckPointer(pqwPosition,E_POINTER);
		Thread::CLockCS lock(m_pCSStream);
		*pqwPosition = m_i64Offset;
		return S_OK;
	}
	//	ストリームの位置を設定
	STDMETHODIMP CMFByteStream::SetCurrentPosition(QWORD qwPosition)
	{
		return Seek(msoBegin, qwPosition, 0, null);
	}
	//	ストリームの終端確認
	STDMETHODIMP CMFByteStream::IsEndOfStream(BOOL *pfEndOfStream)
	{
		CheckPointer(pfEndOfStream,E_POINTER);
		Thread::CLockCS lock(m_pCSStream);
		BOOL b = m_pStreamBuffer->GetSize() <= m_i64Offset;
		*pfEndOfStream = b;
		return S_OK;
	}
	//	同期読み込み
	STDMETHODIMP CMFByteStream::Read(BYTE *pb,ULONG cb,ULONG *pcbRead)
	{
		CheckPointer(pb,E_POINTER);
		CheckPointer(pcbRead,E_POINTER);
		Thread::CLockCS lock(m_pCSStream);

		s64 i64Read = 0;
		HRESULT hr;
		hr = ReadBuffer(pb, cb, m_i64Offset, &i64Read);

		return hr;
		//*pcbRead = m_pStreamBuffer->Read(pb, cb);
	}
	//	非同期読み込みの開始
	STDMETHODIMP CMFByteStream::BeginRead(BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState)
	{
		CheckPointer(pb,E_POINTER);
		CheckPointer(pCallback,E_POINTER);
		CheckPointer(punkState,E_POINTER);

		AsyncItem item;
		HRESULT hr;
		hr = MFCreateAsyncResult(NULL, pCallback, punkState, item.pResult.GetReference());
		if(FAILED(hr))
		{
			return hr;
		}
		item.pBuffer = pb;
		item.uiSize = cb;
		item.i64Read = 0;
		item.i64ReadPos = m_i64Offset;
		Thread::CLockCS lock(m_pCSQueue);
		m_dequeWorkItem.push_back(item);

		m_pEvent->SetSignal();
		return S_OK;
	}
	//	非同期読み込みの終了
	STDMETHODIMP CMFByteStream::EndRead(IMFAsyncResult *pResult,ULONG *pcbRead)
	{
		CheckPointer(pResult,E_POINTER);
		CheckPointer(pcbRead,E_POINTER);
		HRESULT hr = S_FALSE;
		Thread::CLockCS lock(m_pCSQueue);
		for(u32 i=0; i < m_dequeDoneItem.size(); i++ )
		{
			auto& item = m_dequeDoneItem[i];
			if( item.pResult == pResult )
			{
				*pcbRead = (u32)item.i64Read;
				m_dequeDoneItem.erase(i);
				hr = S_OK;
				break;
			}
		}
		return hr;
	}
	//	同期書き込み
	STDMETHODIMP CMFByteStream::Write(const BYTE *pb,ULONG cb,ULONG *pcbWritten)
	{
		return E_NOTIMPL;
	}
	//	非同期書き込みの開始
	STDMETHODIMP CMFByteStream::BeginWrite(const BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState)
	{
		return E_NOTIMPL;
	}
	//	非同期書き込みの終了
	STDMETHODIMP CMFByteStream::EndWrite(IMFAsyncResult *pResult,ULONG *pcbWritten)
	{
		return E_NOTIMPL;
	}
	//	ストリームの位置を移動
	STDMETHODIMP CMFByteStream::Seek(MFBYTESTREAM_SEEK_ORIGIN SeekOrigin,LONGLONG llSeekOffset,DWORD dwSeekFlags,QWORD *pqwCurrentPosition)
	{
		s64 i64Size = m_pStreamBuffer->GetSize();

		Thread::CLockCS lock(m_pCSStream);
		switch( SeekOrigin )
		{
		case msoBegin:
			break;
		case msoCurrent:
			llSeekOffset = llSeekOffset + m_i64Offset;
			break;
		}
		llSeekOffset = Min(llSeekOffset, i64Size);
		m_i64Offset = llSeekOffset;

		m_pStreamBuffer->Seek(EStreamOffsetBegin, llSeekOffset);
		if( pqwCurrentPosition )
		{
			*pqwCurrentPosition = m_i64Offset;
		}
		return S_OK;
	}
	//	書き込みバッファのフラッシュ
	STDMETHODIMP CMFByteStream::Flush(void)
	{
		return E_NOTIMPL;
	}
	//	ストリームを閉じる
	STDMETHODIMP CMFByteStream::Close(void)
	{
		m_bExit = true;
		if( m_pCSQueue.IsValid() )
		{
			Thread::CLockCS lock(m_pCSQueue);
			m_dequeWorkItem.clear();
			m_dequeDoneItem.clear();
		}
		if( m_pEvent.IsValid() )
		{
			m_pEvent->SetSignal();
		}
		if( m_pThread.IsValid() )
		{
			m_pThread->Exit();
		}
		m_pCSStream = null;
		m_pCSQueue = null;
		m_pEvent = null;
		m_pThread = null;
		return S_OK;
	}
	//	バッファの読み込み
	HRESULT CMFByteStream::ReadBuffer(void* pBuffer, s64 i64Size, s32 i64ReadPos, s64* pReadSize)
	{
		s64 i64ReadSize;
		Thread::CLockCS lock(m_pCSStream);
		m_pStreamBuffer->Seek(EStreamOffsetBegin, i64ReadPos);

		i64ReadSize = m_pStreamBuffer->Read(pBuffer, i64Size);
		if( pReadSize )
		{
			*pReadSize = i64ReadSize;
		}
		m_i64Offset = i64ReadPos + i64ReadSize;

		return 0 < i64ReadSize? S_OK: S_FALSE;
	}

	u32 CMFByteStream::AsyncThreadFunc(void)
	{
		HRESULT hr;
		while( 1 )
		{
			m_pEvent->Wait();
			if( m_bExit )
			{
				break;
			}
			while( !m_dequeWorkItem.empty() )
			{
				AsyncItem item;
				// 処理するキューを取り出す
				{
					Thread::CLockCS lock(m_pCSQueue);
					if( m_dequeWorkItem.empty() )
					{
						break;
					}
					item = m_dequeWorkItem.front();
					m_dequeWorkItem.erase(0);
				}
				hr = ReadBuffer(item.pBuffer, item.uiSize, item.i64ReadPos, &item.i64Read);
				if( FAILED(hr) )
				{
					item.pResult = null;
					break;
				}
				// 処理が終了した旨を通知する
				{
					Thread::CLockCS lock(m_pCSQueue);
					m_dequeDoneItem.push_back(item);
				}
				MFInvokeCallback(item.pResult);
			}
		}
		return 0;
	}
	//	非同期処理
	u32 THREADFUNC CMFByteStream::AsyncThreadFunc(void* pArg)
	{
		CMFByteStream* pThis = (CMFByteStream*)pArg;
		return pThis->AsyncThreadFunc();
	}






	//////////////////////////////////////////
	//コンストラクタ
CMFByteStream2::CMFByteStream2(HRESULT *lphr,const TCHAR *lpFileName)
    : m_uiRef(0), m_bExit(false), m_lpFileName(NULL), m_hFileStream(NULL), m_ullStreamPos(0), m_ullStreamSize(0), m_hAsyncThread(NULL), m_hThreadEvent(NULL), m_lpAttributes(NULL)
{
    HRESULT hr;
    ::InitializeCriticalSection(&m_vCSStream);
    ::InitializeCriticalSection(&m_vCSQueue);
    if(lpFileName != NULL){
        m_lpFileName = new TCHAR[lstrlen(lpFileName) + 1];
        _tcscpy(m_lpFileName,lpFileName);
    }
    hr = MFCreateAttributes(&m_lpAttributes,0);
    AddRef();
    if(lphr != NULL){ *lphr = hr; }
}
//デストラクタ
CMFByteStream2::~CMFByteStream2()
{
    Close();
    if(m_lpAttributes != NULL){
        m_lpAttributes->Release();
        m_lpAttributes = NULL;
    }
    if(m_lpFileName != NULL){
        delete[] m_lpFileName;
        m_lpFileName = NULL;
    }
    ::DeleteCriticalSection(&m_vCSQueue);
    ::DeleteCriticalSection(&m_vCSStream);
}
	//----------------------------------------------------------------------
	//	QueryInterface(IUnknown)
	//----------------------------------------------------------------------
	STDMETHODIMP CMFByteStream2::QueryInterface(REFIID riid, void** ppv)
	{
		//	Attributesが必要な場合は内部の変数から返す
		if( IsEqualIID(riid, IID_IMFAttributes) )
		{
			return m_lpAttributes->QueryInterface(riid, ppv);
		}
		static const QITAB qit[] = 
		{
			QITABENT(CMFByteStream2, IMFByteStream),
			{ 0 }
		};
		return QISearch(this, qit, riid, ppv);
	}
	//----------------------------------------------------------------------
	//	AddRef(IUnknown)
	//----------------------------------------------------------------------
	STDMETHODIMP_(ULONG) CMFByteStream2::AddRef()
	{
		return InterlockedIncrement(&m_uiRef);
	}
	//----------------------------------------------------------------------
	//	Release(IUnknown)
	//----------------------------------------------------------------------
	STDMETHODIMP_(ULONG) CMFByteStream2::Release()
	{
		u32 uiRef = InterlockedDecrement(&m_uiRef);
		if( uiRef == 0 )
		{
			delete this;
		}
		return uiRef;
	}
//ストリームを開く
HRESULT CMFByteStream2::Open(void)
{
    unsigned int threadid;
    if(IsValidStream()) return E_ABORT;
    m_hFileStream = ::CreateFile(m_lpFileName,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
    if(m_hFileStream == INVALID_HANDLE_VALUE){ m_hFileStream = NULL; return E_FAIL; }
    if(!::GetFileSizeEx(m_hFileStream,(LARGE_INTEGER *)&m_ullStreamSize)) return E_FAIL;
    m_hThreadEvent = ::CreateEvent(NULL,FALSE,FALSE,NULL);
    if(m_hThreadEvent == NULL) return E_FAIL;
    m_hAsyncThread = (HANDLE)_beginthreadex(NULL,0,&CMFByteStream2::AsyncThread,this,0,&threadid);
    if(m_hAsyncThread == NULL){ ::CloseHandle(m_hThreadEvent); m_hThreadEvent = NULL; return E_FAIL; }
    return S_OK;
}
//ストリームを閉じる
STDMETHODIMP CMFByteStream2::Close(void)
{
    dequeAsyncItem::iterator it,itend;
    {
        CLockQueue lock(this);
        for(it = m_dequeWorkItem.begin(),itend = m_dequeWorkItem.end();it != itend;++it){ it->lpResult->Release(); }
        for(it = m_dequeDoneItem.begin(),itend = m_dequeDoneItem.end();it != itend;++it){ it->lpResult->Release(); }
        m_dequeWorkItem.clear(); m_dequeDoneItem.clear();
    }
	m_bExit = true;
    if(IsValidThread()){
        ::SetEvent(m_hThreadEvent);
        ::WaitForSingleObject(m_hAsyncThread,INFINITE);
        ::CloseHandle(m_hThreadEvent);
        ::CloseHandle(m_hAsyncThread);
        m_hThreadEvent = NULL; m_hAsyncThread = NULL;
    }
    if(IsValidStream()){
        CLockStream lock(this);
        ::CloseHandle(m_hFileStream);
        m_ullStreamPos = m_ullStreamSize = 0;
        m_hFileStream = NULL;
    }
    return S_OK;
}//ストリームの能力を取得する
STDMETHODIMP CMFByteStream2::GetCapabilities(DWORD *pdwCapabilities)
{
    CheckPointer(pdwCapabilities,E_POINTER);
    if(!IsValidStream()) return E_HANDLE;
    *pdwCapabilities = MFBYTESTREAM_IS_READABLE | MFBYTESTREAM_IS_SEEKABLE;
    return S_OK;
}
//ストリームのサイズを取得する
STDMETHODIMP CMFByteStream2::GetLength(QWORD *pqwLength)
{
    CheckPointer(pqwLength,E_POINTER);
    *pqwLength = m_ullStreamSize;
    return S_OK;
}
//ストリームのサイズを設定する
STDMETHODIMP CMFByteStream2::SetLength(QWORD qwLength)
{
    return E_NOTIMPL;
}
//ストリームの位置を取得する
STDMETHODIMP CMFByteStream2::GetCurrentPosition(QWORD *pqwPosition)
{
    CheckPointer(pqwPosition,E_POINTER);
    *pqwPosition = m_ullStreamPos;
    return S_OK;
}
//ストリームの位置を設定する
STDMETHODIMP CMFByteStream2::SetCurrentPosition(QWORD qwPosition)
{
    //if(qwPosition > m_ullStreamSize) return E_INVALIDARG;
    return Seek(msoBegin,qwPosition,0,NULL);
}
//ストリームの終端かどうか
STDMETHODIMP CMFByteStream2::IsEndOfStream(BOOL *pfEndOfStream)
{
    CheckPointer(pfEndOfStream,E_POINTER);
    *pfEndOfStream = m_ullStreamPos >= m_ullStreamSize ? TRUE : FALSE;
    return S_OK;
}//同期書き込みを行う
STDMETHODIMP CMFByteStream2::Write(const BYTE *pb,ULONG cb,ULONG *pcbWritten)
{
    return E_NOTIMPL;
}
//非同期書き込みを開始する
STDMETHODIMP CMFByteStream2::BeginWrite(const BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState)
{
    return E_NOTIMPL;
}
//非同期書き込みを終了する
STDMETHODIMP CMFByteStream2::EndWrite(IMFAsyncResult *pResult,ULONG *pcbWritten)
{
    return E_NOTIMPL;
}
//書き込みバッファのフラッシュを行う
STDMETHODIMP CMFByteStream2::Flush(void)
{
    return E_NOTIMPL;
}//ストリーム位置を移動する
STDMETHODIMP CMFByteStream2::Seek(MFBYTESTREAM_SEEK_ORIGIN SeekOrigin,LONGLONG llSeekOffset,DWORD dwSeekFlags,QWORD *pqwCurrentPosition)
{
    int64_t llNewPos;
    CLockStream lock(this);
    switch(SeekOrigin){
    case msoBegin: llNewPos = llSeekOffset; break;
    case msoCurrent: llNewPos = (int64_t)m_ullStreamPos + llSeekOffset; break;
    default: return E_INVALIDARG;
    }
    if(llNewPos < 0){ llNewPos = 0; } else if((uint64_t)llNewPos > m_ullStreamSize){ llNewPos = m_ullStreamSize; }
    m_ullStreamPos = llNewPos;
    if(pqwCurrentPosition != NULL){ *pqwCurrentPosition = m_ullStreamPos; }
    return S_OK;
}//同期読み取りを行う
STDMETHODIMP CMFByteStream2::Read(BYTE *pb,ULONG cb,ULONG *pcbRead)
{
    CheckPointer(pb,E_POINTER); CheckPointer(pcbRead,E_POINTER);
    return InnerRead(pb,cb,pcbRead,m_ullStreamPos);
}
//非同期読み取りを開始する
STDMETHODIMP CMFByteStream2::BeginRead(BYTE *pb,ULONG cb,IMFAsyncCallback *pCallback,IUnknown *punkState)
{
    ASYNCITEM item; HRESULT hr;
    CheckPointer(pb,E_POINTER); CheckPointer(pCallback,E_POINTER);
    hr = MFCreateAsyncResult(NULL,pCallback,punkState,&item.lpResult);
    if(FAILED(hr)) return hr;
    item.pbBuffer = pb; item.cbSize = cb; item.cbRead = 0; item.nReadPos = m_ullStreamPos;
    CLockQueue lock(this);
    m_dequeWorkItem.push_back(item);
    ::SetEvent(m_hThreadEvent);
    return S_OK;
}
//非同期読み取りを終了する
STDMETHODIMP CMFByteStream2::EndRead(IMFAsyncResult *pResult,ULONG *pcbRead)
{
    CheckPointer(pResult,E_POINTER); CheckPointer(pcbRead,E_POINTER);
    dequeAsyncItem::iterator it,itend; HRESULT hr;
    CLockQueue lock(this);
    for(it = m_dequeDoneItem.begin(),itend = m_dequeDoneItem.end(),hr = E_FAIL;it != itend;++it){
        if(it->lpResult == pResult){
            *pcbRead = it->cbRead;
            m_dequeDoneItem.erase(it);
            pResult->Release(); hr = S_OK;
            break;
        }
    }
    return hr;
}
//内部読み込み処理
HRESULT CMFByteStream2::InnerRead(BYTE *pb,ULONG cb,ULONG *pcbRead,uint64_t nReadPos)
{
    if(!IsValidStream()) return E_HANDLE;
    CLockStream lock(this);
    if(!::SetFilePointerEx(m_hFileStream,*((LARGE_INTEGER *)&nReadPos),NULL,FILE_BEGIN)){ return E_FAIL; }
    if(!::ReadFile(m_hFileStream,pb,cb,pcbRead,NULL)) return E_FAIL;
    m_ullStreamPos = nReadPos + *pcbRead;
    return S_OK;
}//非同期処理スレッド
unsigned int __stdcall CMFByteStream2::AsyncThread(void *lpContext)
{
    ASYNCITEM item; CMFByteStream2 *lpThis; HRESULT hr; DWORD dw;
     
    lpThis = (CMFByteStream2 *)lpContext;
    while(1){
        dw = WaitForSingleObject(lpThis->m_hThreadEvent,INFINITE);
        if(lpThis->m_bExit) break;
        while(!lpThis->m_dequeWorkItem.empty()){
            {
                CLockQueue lock(lpThis);
                if(lpThis->m_dequeWorkItem.empty()) break;
                item = lpThis->m_dequeWorkItem.front();
                lpThis->m_dequeWorkItem.pop_front();
            }
            hr = lpThis->InnerRead(item.pbBuffer,item.cbSize,&item.cbRead,item.nReadPos);
            if(FAILED(hr)){ item.lpResult->Release(); break; }
            {
                CLockQueue lock(lpThis);
                lpThis->m_dequeDoneItem.push_back(item);
            }
             
            hr = MFInvokeCallback(item.lpResult);
        }
    }
    return 0;
}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "../Movie/MFMovie.h"
#include "../Movie/MFByteStream.h"

#include <mfapi.h>
#include <Mfidl.h>
#include <Mferror.h>
#include <evr.h>

#pragma comment(lib, "mfplat")
#pragma comment(lib, "mf")
#pragma comment(lib, "mfuuid")

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define FAILED_BREAK(hr, code)	{ (hr) = (code); if( FAILED(hr) ){ kgErrorLog("Failed Break"); break; } }
//#define FAILED_BREAK(hr, code)	{ (hr) = (code); if( FAILED(hr) ){ kgAssert(false, "Failed Break"); break; } }


#define WM_MFSNOTIFY		(WM_USER + 0x1001)

#define MFS_VOLUMERATE		1000
#define MFS_VOLUMEMAX		1.0f
#define MFS_VOLUMEMIN		0.0f
#define MFS_PANLEFT			-(MFS_VOLUMEMAX)
#define MFS_PANCENTER		MFS_VOLUMEMIN
#define MFS_PANRIGHT		MFS_VOLUMEMAX

namespace App
{
	//!	メディアの種類の列挙用
	struct MEDIACONTENTTYPE
	{
		const c16 *ext;
		const c16 *type;
	};
	//!	メディアの種類
	static const MEDIACONTENTTYPE AvMediaType[] =
	{
		{ TEXT("mp4"), L"video/mp4" },
		{ TEXT("wmv"), L"video/x-ms-wmv" },
		{ TEXT("avi"), L"video/x-msvideo" },
		{ NULL, NULL }
	};

	//!	ムービーの状態
	namespace EMFStatus
	{
		enum
		{
			None		= 0,
			Load		= (1 << 0),
			Play		= (1 << 1),
			Pause		= (1 << 2),
			Loop		= (1 << 3),
			HasAudio	= (1 << 4),
			HasVideo	= (1 << 5),
		};
	}

	//======================================================================
	//!	MFセッションクラス
	//======================================================================
	class CMFSession
		: public IMFAsyncCallback
	{
	public:
		//!	コンストラクタ
		CMFSession(void)
			: m_uiRef(0)
			, m_uiStatus(EMFStatus::None)
			, m_MediaLength(0)
			, m_fAudioBalance(0.0f)
			, m_fAudioVolume(1.0f)
			, m_fCurrentTime(0.0f)
			, m_uiWidth(0)
			, m_uiHeight(0)
			, m_pVideoImage(null)
		{
			AddRef();

			Thread::Create(m_pCSSession.GetReference());
			Thread::Create(m_pCSEvent.GetReference());
		}
		//!	デストラクタ
		~CMFSession(void)
		{
			ReleaseMovie();
		}

	public:
		//!	動画の読み込み
		bool LoadMovie(const c16* pFilename)
		{
			TKGLPtr<IMFSourceResolver> pSourceResolver;
			TKGLPtr<IUnknown> pSource;			
			TKGLPtr<IMFTopology> pTopology;
			TKGLPtr<IMFPresentationDescriptor> pPresentDesc;
			TKGLPtr<IMFClock> pClock;

			if( CheckStatus(EMFStatus::Load) )
			{
				ReleaseMovie();
			}

			HRESULT hr;
			do
			{
				m_uiStatus = EMFStatus::None;

				//	MediaSessionの作成
				FAILED_BREAK(hr, MFCreateMediaSession(null, m_pSession.GetReference()));
				//	イベントの取得を自分自身に割り当てる
				FAILED_BREAK(hr, m_pSession->BeginGetEvent(this, null));

#if 1
				if( true )
				{
					TKGLPtr<IStreamBufferBase> pStreamBuffer;
					if( true )
					{
						c8 cFilename[256];
						u32 uiSize = ArrayCount(cFilename);
						CharSet::ConvertUTF16ToShiftJIS(cFilename, pFilename, &uiSize);
						m_Filename = cFilename;

						auto pBuffer = KGLFileReader()->LoadBlock(cFilename);
						u32 uiBufferSize = KGLFileReader()->GetSize(cFilename);

						TKGLPtr<CStreamBinary> pStream;
						*pStream.GetReference() = new CStreamBinary();
						FAILED_BREAK(hr, pStream->Create(pBuffer, uiBufferSize));
						pStreamBuffer = pStream;
					}
					else if( false )
					{
						TKGLPtr<CStreamFile> pStreamFile;
						*pStreamFile.GetReference() = new CStreamFile();
						FAILED_BREAK(hr, pStreamFile->Open(pFilename));
						pStreamBuffer = pStreamFile;
					}
					TKGLPtr<CMFByteStream> pByteStream;
					*pByteStream.GetReference() = new CMFByteStream(pStreamBuffer);
					FAILED_BREAK(hr, pByteStream->Create());

					FAILED_BREAK(hr, pByteStream->QueryInterface(IID_PPV_ARGS(m_pByteStream.GetReference())));
				}
				else
				{
					//ByteStreamの作成
					TKGLPtr<CMFByteStream2> lpByteStream = new CMFByteStream2(&hr, pFilename);
					if(FAILED(hr)) break;
					hr = lpByteStream->Open();
					if(FAILED(hr)){ break; }
					hr = lpByteStream->QueryInterface(IID_PPV_ARGS(m_pByteStream.GetReference()));
					if(FAILED(hr)) break;
				}
#else
				//	ByteStreamの作成
				FAILED_BREAK(hr,
					MFCreateFile(
						MF_ACCESSMODE_READ,
						MF_OPENMODE_FAIL_IF_NOT_EXIST,
						MF_FILEFLAGS_NONE,
						pFilename,
						m_pByteStream.GetReference()
						));
#endif // 0
				FAILED_BREAK(hr, SetByteStreamContentType(pFilename, null));

				//	SourceResolverの作成
				FAILED_BREAK(hr, MFCreateSourceResolver(pSourceResolver.GetReference()));

				//	IMFMediaSourceの作成
				MF_OBJECT_TYPE objtype = MF_OBJECT_INVALID;
				FAILED_BREAK(hr,
					pSourceResolver->CreateObjectFromByteStream(
						m_pByteStream,
						null,
						MF_RESOLUTION_MEDIASOURCE,
						null,
						&objtype,
						pSource.GetReference()
						));
				FAILED_BREAK(hr, pSource->QueryInterface(IID_PPV_ARGS(m_pMediaSource.GetReference())));

				//	IMFTopologyの作成
				FAILED_BREAK(hr, MFCreateTopology(pTopology.GetReference()));
				//	PresentDescriptionの取得
				FAILED_BREAK(hr, m_pMediaSource->CreatePresentationDescriptor(pPresentDesc.GetReference()));

				dword dwCount;
				//	Description数の取得
				FAILED_BREAK(hr, pPresentDesc->GetStreamDescriptorCount(&dwCount));

				for( dword i=0; i < dwCount; i++ )
				{
					do
					{
						TKGLPtr<IMFStreamDescriptor> pStreamDesc;
						TKGLPtr<IMFTopologyNode> pSourceNode, pOutputNode;
						BOOL is_selected = FALSE;
						//	StreamDescriptionを取得
						FAILED_BREAK(hr, pPresentDesc->GetStreamDescriptorByIndex(i, &is_selected, pStreamDesc.GetReference()));
						if( !is_selected ) break;

						//	Streamのノードを作成
						FAILED_BREAK(hr, CreateSourceNode(pPresentDesc, pStreamDesc, pSourceNode.GetReference()));
						FAILED_BREAK(hr, CreateOutputNode(pStreamDesc, pOutputNode.GetReference()));
						//	ノードの登録
						FAILED_BREAK(hr, pTopology->AddNode(pSourceNode));
						FAILED_BREAK(hr, pTopology->AddNode(pOutputNode));

						//	ノードの接続
						FAILED_BREAK(hr, pSourceNode->ConnectOutput(0, pOutputNode, 0));

						//	対象のストリームの処理完了
						hr = S_OK;
					}
					while( 0 );
				}
				//	トポロジーのノード作成に失敗した場合は終了する
				if( FAILED(hr) ) break;

				FAILED_BREAK(hr, m_pSession->SetTopology(0, pTopology));

				//	クロックの取得
				FAILED_BREAK(hr, m_pSession->GetClock(pClock.GetReference()));
				FAILED_BREAK(hr, pClock->QueryInterface(IID_PPV_ARGS(m_pPresentationClock.GetReference())));

				//	メディア長の取得
				FAILED_BREAK(hr, pPresentDesc->GetUINT64(MF_PD_DURATION, (UINT64*)&m_MediaLength));

				m_fAudioVolume = 1.0f;
				m_fAudioBalance = 0.0f;
				m_uiStatus |= EMFStatus::Load;
				hr = S_OK;
			}
			while( false );

			return SUCCEEDED(hr);
		}

		//	メディアを解放する
		bool ReleaseMovie(void)
		{
			{
				Thread::CLockCS lock(m_pCSSession);

				HRESULT hr;
				if( m_pSession.IsValid() )
				{
					m_pVideoDisplay = null;
					m_pAudioVolume = null;
					hr = m_pSession->Close();
					if( FAILED(hr) ) return false;
				}

				if( m_pMediaSource.IsValid() )
				{
					m_pMediaSource->Shutdown();
				}
				if( m_pSession.IsValid() )
				{
					m_pSession->Shutdown();
				}
				m_pPresentationClock = null;
				m_pMediaSource = null;
				m_pByteStream = null;
				m_pSession = null;
			}
			if( m_Filename.length() )
			{
				KGLFileReader()->Unload(m_Filename.c_str());
				m_Filename.clear();
			}
			return true;
		}

	public:
		//!	再生
		bool PlayMovie(bool is_loop)
		{
			if( !CheckStatus(EMFStatus::Load) )
			{
				return false;
			}
			if( !CheckStatus(EMFStatus::Pause) )
			{
				PROPVARIANT varStart;
				PropVariantInit(&varStart);
				m_pSession->Start(null, &varStart);
				if( is_loop )
				{
					m_uiStatus |= EMFStatus::Loop;
				}
				m_uiStatus &= ~EMFStatus::Pause;
				m_uiStatus |= EMFStatus::Play;
			}
			return true;
		}

		//!	停止
		bool StopMovie(void)
		{
			if( !CheckStatus(EMFStatus::Load) )
			{
				return false;
			}
			if( CheckStatus(EMFStatus::Play) )
			{
				m_pSession->Stop();
				m_uiStatus &= ~EMFStatus::Play;
				m_uiStatus |= EMFStatus::Pause;
			}
			return true;
		}

		//!	一時停止
		bool PauseMovie(void)
		{
			if( !CheckStatus(EMFStatus::Load) )
			{
				return false;
			}
			if( CheckStatus(EMFStatus::Play) )
			{
				m_pSession->Pause();
				m_uiStatus |= EMFStatus::Pause;
			}
			return true;
		}
		//!	再開
		bool ResumeMovie(void)
		{
			if( !CheckStatus(EMFStatus::Load) )
			{
				return false;
			}
			if( CheckStatus(EMFStatus::Pause) )
			{
				PROPVARIANT varStart;
				PropVariantInit(&varStart);
				m_pSession->Start(null, &varStart);
				m_uiStatus &= ~EMFStatus::Pause;
			}
			return true;
		}

	public:
		//!	早送り
		bool ForwardMovie(u32 uiTime)
		{
			s64 i64Time = MilliSecondToMediaTime(uiTime);
			return SeekMovieRelative(i64Time);
		}
		//!	早送り
		bool BackwardMovie(u32 uiTime)
		{
			s64 i64Time = MilliSecondToMediaTime(uiTime);
			return SeekMovieRelative(-i64Time);
		}
		//!	再生位置の移動
		bool SeekMovie(u32 uiTime)
		{
			s64 i64Time = MilliSecondToMediaTime(uiTime);
			return SeekMovieAbsolute(i64Time);
		}

		//!	メディアの再生時間を取得
		f32 MovieTime(void) const
		{
			return MediaTimeToSecond(m_MediaLength);
		}

		//!	メディアの再生時間を取得
		f32 CurrentTime(void) const
		{
			return m_fCurrentTime;
		}
		//!	解像度の取得
		void GetResolution(u32& width, u32& height) const
		{
			width	= m_uiWidth;
			height	= m_uiHeight;
		}

	private:
		//!	ミリ秒をメディア時間に変換する
		s64 MilliSecondToMediaTime(u32 uiTime) const
		{
			return (s64)uiTime * (1000 * 10);
		}
		//!	メディア時間をミリ秒に変換する
		u32 MediaTimeToMilliSecond(s64 i64Time) const
		{
			return (u32)(i64Time / (1000 * 10));
		}
		//!	メディア時間を秒に変換する
		f32 MediaTimeToSecond(s64 i64Time) const
		{
			return (f32)i64Time / (1000.0f * 1000.0f * 10.0f);
		}

	private:
		//!	再生位置の移動(相対)
		bool SeekMovieRelative(s64 i64Time)
		{
			if( m_pPresentationClock.IsValid() )
			{
				MFTIME Time;
				HRESULT hr = S_FALSE;
				hr = m_pPresentationClock->GetTime(&Time);
				if( SUCCEEDED(hr) )
				{
					return SeekTime(Time + i64Time);
				}
			}
			return false;
		}
		//!	再生位置の移動(絶対)
		bool SeekMovieAbsolute(s64 i64Time)
		{
			return SeekTime(i64Time);
		}

		//!	シーク
		bool SeekTime(MFTIME time)
		{
			if( CheckStatus(EMFStatus::Load) )
			{
				return false;
			}
			time = Clamp(time, 0LL, m_MediaLength);

			PROPVARIANT varStart;
			varStart.vt  = VT_I8;
			varStart.hVal.QuadPart = time;

			m_pSession->Start(null, &varStart);
			if( !CheckStatus(EMFStatus::Play) )
			{
				m_pSession->Stop();
			}
			else if( CheckStatus(EMFStatus::Pause) )
			{
				m_pSession->Pause();
			}
			return true;
		}

	public:
		//!	再生ウィンドウの設定
		bool SetPlayWindow(const POINT* pPos = null, const SIZE* pSize = null)
		{
			if( !HasVideo() )
			{
				return false;
			}
			if( m_pVideoDisplay.IsValid() )
			{
				RECT rect = { 0 };
				SIZE size;

				if( pPos )
				{
					rect.left = pPos->x;
					rect.top  = pPos->y;
				}
				if( !pSize )
				{
					m_pVideoDisplay->GetNativeVideoSize(&size, null);
					pSize = &size;
				}
				rect.right  = rect.left + pSize->cx;
				rect.bottom = rect.top  + pSize->cy;
				m_pVideoDisplay->SetVideoPosition(null, &rect);
			}
			return true;
		}
		//!	再生速度の設定
		bool SetSpeed(f32 fSpeed)
		{
			HRESULT hr;
			f32 fNearSpeed;
			TKGLPtr<IMFGetService> pGetService;
			TKGLPtr<IMFRateSupport> pRateSupport;
			TKGLPtr<IMFRateControl> pRateControl;
			do
			{
				FAILED_BREAK(hr, m_pSession->QueryInterface(IID_PPV_ARGS(pGetService.GetReference())));
				FAILED_BREAK(hr, pGetService->GetService(MF_RATE_CONTROL_SERVICE, IID_PPV_ARGS(pRateSupport.GetReference())));
				//FAILED_BREAK(hr, pRateSupport->IsRateSupported(false, fSpeed, &fNearSpeed));
				FAILED_BREAK(hr, pGetService->GetService(MF_RATE_CONTROL_SERVICE, IID_PPV_ARGS(pRateControl.GetReference())));
				FAILED_BREAK(hr, pRateControl->SetRate(false, fSpeed));
			}
			while( false );

			return SUCCEEDED(hr);
		}
		//!	パンの設定
		bool SetPan(f32 fPan)
		{
			if( !HasAudio() )
			{
				return false;
			}
			f32 fBalance = Clamp(fPan, MFS_PANLEFT, MFS_PANRIGHT);
			if( fBalance == m_fAudioBalance )
			{
				return true;
			}
			m_fAudioBalance = fBalance;
			return SetVolume();
		}
		//!	音量の設定
		bool SetVolume(f32 fVolume)
		{
			if( !HasAudio() )
			{
				return false;
			}
			fVolume = Clamp(fVolume, MFS_VOLUMEMIN, MFS_VOLUMEMAX);
			if( fVolume == m_fAudioVolume )
			{
				return true;
			}
			m_fAudioVolume = fVolume;
			return SetVolume();
		}

		//!	音量、パンの設定
		bool SetVolume(void)
		{
			if( !m_pAudioVolume.IsValid() )
			{
				return false;
			}
			u32 uiChannels;
			m_pAudioVolume->GetChannelCount(&uiChannels);

			TSmartPtr<f32> pVolumes = new f32[uiChannels];
			for( u32 i=0; i < uiChannels; i++ )
			{
				if( i & 0x01 )
				{
					pVolumes[i] = (m_fAudioBalance >= 0? 1.0f: ((1.0f + m_fAudioBalance) * m_fAudioVolume));
				}
				else
				{
					pVolumes[i] = (m_fAudioBalance <= 0? 1.0f: ((1.0f - m_fAudioBalance) * m_fAudioVolume));
				}
			}
			if( uiChannels & 0x01 )
			{
				pVolumes[uiChannels - 1] = m_fAudioVolume;
			}
			m_pAudioVolume->SetAllVolumes(uiChannels, pVolumes);
			return true;
		}

	public:
		//!	再生中か？
		bool IsPlayMovie(void) const
		{
			return CheckStatus(EMFStatus::Play);
		}

		//!	ループ中か？
		bool IsLoopMovie(void) const
		{
			return CheckStatus(EMFStatus::Loop);
		}
		//!	映像が有効か？
		bool HasVideo() const
		{
			return CheckStatus(EMFStatus::HasVideo);
		}
		//!	音声が有効か？
		bool HasAudio() const
		{
			return CheckStatus(EMFStatus::HasAudio);
		}

	private:
		STDMETHODIMP GetParameters(DWORD* pFlags, DWORD* pQueue)
		{
			return E_NOTIMPL;
		}
		//!	非同期処理が行われた際のコールバック
		STDMETHODIMP Invoke(IMFAsyncResult* pAsyncResult)
		{
			TKGLPtr<IMFMediaEvent> pEvent;
			MediaEventType type;
			HRESULT hr;

			do
			{
				TKGLPtr<IMFMediaSession> pSession;
				{
					Thread::CLockCS lock(m_pCSSession);
					pSession = m_pSession;
				}
				if( !pSession.IsValid() )
				{
					hr = E_POINTER;
					break;
				}
				//	イベントキューからイベントを取得
				FAILED_BREAK(hr, pSession->EndGetEvent(pAsyncResult, pEvent.GetReference()));
				FAILED_BREAK(hr, pEvent->GetType(&type));

				if( type != MESessionClosed )
				{
					pSession->BeginGetEvent(this, null);
				}
				if( pSession.IsValid() )
				{
					Thread::CLockCS lock(m_pCSEvent);
					m_pEvents.push_back(pEvent);
					//HandleEvent(pEvent);
					//::PostMessage(m_hWnd, WM_MFSNOTIFY, (WPARAM)this, (LPARAM)pEvent.Get());
				}
				hr = S_OK;
			}
			while( false );

			return hr;
		}

		bool HandleEvent(TKGLPtr<IMFMediaEvent> pEvent)
		{
			TKGLPtr<IMFGetService> pGetService;
			MF_TOPOSTATUS topo_status;	
			MediaEventType type;
			HRESULT hrStatus, hr = S_OK;

			do
			{
				FAILED_BREAK(hr, pEvent->GetType(&type));
				FAILED_BREAK(hr, pEvent->GetStatus(&hrStatus));
				FAILED_BREAK(hr, hrStatus);

				switch( type )
				{
				case MESessionTopologyStatus:
					FAILED_BREAK(hr, pEvent->GetUINT32(MF_EVENT_TOPOLOGY_STATUS, (UINT32*)&topo_status));
					switch( topo_status )
					{
					case MF_TOPOSTATUS_READY:
						FAILED_BREAK(hr, m_pSession->QueryInterface(IID_PPV_ARGS(pGetService.GetReference())));
						if( CheckStatus(EMFStatus::HasVideo) )
						{
							FAILED_BREAK(hr, pGetService->GetService(MR_VIDEO_RENDER_SERVICE, IID_PPV_ARGS(m_pVideoDisplay.GetReference())));

							SIZE size;
							m_pVideoDisplay->GetNativeVideoSize(&size, null);
							m_uiWidth  = size.cx;
							m_uiHeight = size.cy;

							SetPlayWindow();
						}
						if( CheckStatus(EMFStatus::HasAudio) )
						{
							FAILED_BREAK(hr, pGetService->GetService(MR_STREAM_VOLUME_SERVICE, IID_PPV_ARGS(m_pAudioVolume.GetReference())));
						}
						break;
					}
					break;
				case MEEndOfPresentation:
					m_pSession->Stop();
					if( CheckStatus(EMFStatus::Loop) )
					{
						PROPVARIANT varStart;
						varStart.vt = VT_I8;
						varStart.hVal.QuadPart = 0;
						m_pSession->Start(null, &varStart);
					}
					else
					{
						m_uiStatus &= ~EMFStatus::Play;
						m_uiStatus |= EMFStatus::Pause;
					}
					hr = S_OK;
				}
			}
			while( false );

			return SUCCEEDED(hr);
		}

	public:
		//!	更新
		void Update(void)
		{
			while( !m_pEvents.empty() )
			{
				TKGLPtr<IMFMediaEvent> pEvent;
				{
					Thread::CLockCS lock(m_pCSEvent);
					pEvent = m_pEvents.front();
					m_pEvents.erase(0);
				}
				HandleEvent(pEvent);
			}

			if( m_pPresentationClock.IsValid() )
			{
				MFTIME Time;
				HRESULT hr = S_FALSE;
				hr = m_pPresentationClock->GetTime(&Time);
				if( SUCCEEDED(hr) )
				{
					m_fCurrentTime = MediaTimeToSecond(Time);
				}
			}
		}

		//!	再描画
		bool Repaint(void)
		{
			if( !m_pVideoDisplay.IsValid() )
			{
				return false;
			}
			//m_pVideoDisplay->RepaintVideo();

			return true;
		}
		//!	映像のイメージ取得
		bool GetVideoImage(BITMAPINFOHEADER* pBih, u8** ppBit, dword* pcbDib, s64* pTimeStamp)
		{
			if( !m_pVideoDisplay.IsValid() || pBih == null )
			{
				return false;
			}
			ReleaseVideoImage();

			pBih->biWidth  = m_uiWidth;
			pBih->biHeight = m_uiHeight;
			HRESULT hr;
			hr = m_pVideoDisplay->GetCurrentImage(pBih, &m_pVideoImage, pcbDib, pTimeStamp);

			if( FAILED(hr) )
			{
				return false;
			}
			*ppBit = m_pVideoImage;
			return true;
		}
		//!	映像のイメージを開放する
		void ReleaseVideoImage(void)
		{
			if( m_pVideoImage )
			{
				CoTaskMemFree(m_pVideoImage);
				m_pVideoImage = null;
			}
		}

	private:
		//!	ステータスチェック
		bool CheckStatus(u32 uiStatus) const
		{
			return !!(uiStatus & m_uiStatus);
		}

		HRESULT SetByteStreamContentType(const c16* pFilename, const c16* pContentType)
		{
			TKGLPtr<IMFAttributes> pAttributes;
			const MEDIACONTENTTYPE* pMediaType;
			const c16* pLastPath;
			const c16* pLastExt;

			HRESULT hr;
			do{
				if( !m_pByteStream.IsValid() )
				{
					return E_POINTER;
				}
				FAILED_BREAK(hr, m_pByteStream->QueryInterface(IID_PPV_ARGS(pAttributes.GetReference())));

				if( pContentType != null )
				{
					pAttributes->SetString(MF_BYTESTREAM_CONTENT_TYPE, pContentType);
					hr = S_OK;
					break;
				}
				for( auto p = pLastPath = pLastExt = pFilename; *p != L'\0'; p++ )
				{
					if(*p == L'\\' || *p == L'/' )
					{
						pLastPath = p;
					}
					else if( *p == L'.' )
					{
						pLastExt = p;
					}
				}
				if( pLastPath >= pLastExt )
				{
					hr = S_FALSE;
					break;
				}
				pLastExt ++;

				for( pMediaType = AvMediaType, hr = S_FALSE; pMediaType->ext != null; pMediaType ++ )
				{
					if(_tcsicmp(pMediaType->ext, pLastExt) == 0)
					{
						hr = pAttributes->SetString(MF_BYTESTREAM_CONTENT_TYPE, pMediaType->type);
						break;
					}
				}
			} while(0);

			return hr;
		}

		//ソース部のトポロジーノードを作成する
		HRESULT CreateSourceNode(IMFPresentationDescriptor* pPresentDesc, IMFStreamDescriptor* pStreamDesc, IMFTopologyNode** ppNode)
		{
			TKGLPtr<IMFTopologyNode> pNode;

			HRESULT hr;
			do{
				// SourceStream用のノードを作成
				FAILED_BREAK(hr, MFCreateTopologyNode(MF_TOPOLOGY_SOURCESTREAM_NODE, pNode.GetReference()));

				//必要な属性を設定する
				FAILED_BREAK(hr, pNode->SetUnknown(MF_TOPONODE_SOURCE, m_pMediaSource));
				FAILED_BREAK(hr, pNode->SetUnknown(MF_TOPONODE_PRESENTATION_DESCRIPTOR, pPresentDesc));
				FAILED_BREAK(hr, pNode->SetUnknown(MF_TOPONODE_STREAM_DESCRIPTOR, pStreamDesc));

				//作成が完了したので戻す
				*ppNode = pNode;
				pNode->AddRef();
				hr = S_OK;
			} while(0);

			return hr;
		}

		//出力部のトポロジーノードを作成する
		HRESULT CreateOutputNode(IMFStreamDescriptor* pStreamDesc, IMFTopologyNode** ppNode)
		{
			TKGLPtr<IMFTopologyNode> pNode;
			TKGLPtr<IMFMediaTypeHandler> pHandler;
			TKGLPtr<IMFActivate> pActivate;
			GUID guidMajorType;
			DWORD dwStreamID;
			HRESULT hr;

			do{
				memset(&guidMajorType,0x00,sizeof(guidMajorType));

				//	ストリームのIDを取得
				pStreamDesc->GetStreamIdentifier(&dwStreamID);

				//	ストリームのメデイア種別を取得
				FAILED_BREAK(hr, pStreamDesc->GetMediaTypeHandler(pHandler.GetReference()));
				FAILED_BREAK(hr, pHandler->GetMajorType(&guidMajorType));

				//出力部のトポロジーノードを作成
				FAILED_BREAK(hr, MFCreateTopologyNode(MF_TOPOLOGY_OUTPUT_NODE, pNode.GetReference()));

				//レンダラを作成
				if( IsEqualGUID(guidMajorType,MFMediaType_Audio) )
				{
					//オーディオレンダラを作成
					FAILED_BREAK(hr, MFCreateAudioRendererActivate(pActivate.GetReference()));
					m_uiStatus |= EMFStatus::HasAudio;
				}
				else if( IsEqualGUID(guidMajorType, MFMediaType_Video) )
				{
					//ビデオレンダラを作成
					FAILED_BREAK(hr, MFCreateVideoRendererActivate(null, pActivate.GetReference()));
					m_uiStatus |= EMFStatus::HasVideo;
				}
				else
				{
					//出力できる種別でないときは失敗する
					FAILED_BREAK(hr, E_FAIL);
				}

				//ノードに出力を設定する
				FAILED_BREAK(hr, pNode->SetObject(pActivate));

				//作成が完了したので戻す
				*ppNode = pNode;
				pNode->AddRef();
				hr = S_OK;
			} while(0);

			return hr;
		}

	public:
		STDMETHODIMP QueryInterface(REFIID riid, void** ppv)
		{
			static const QITAB qit[] = 
			{
				QITABENT(CMFSession, IMFAsyncCallback),
				{ 0 }
			};
			return QISearch(this, qit, riid, ppv);
		}

		STDMETHODIMP_(ULONG) AddRef()
		{
			return InterlockedIncrement(&m_uiRef);
		}

		STDMETHODIMP_(ULONG) Release()
		{
			long cRef = InterlockedDecrement(&m_uiRef);
			if (cRef == 0)
			{
				delete this;
			}
			return cRef;
		}

	private:
		u32		m_uiRef;
		u32		m_uiStatus;
		MFTIME	m_MediaLength;
		f32		m_fAudioVolume;
		f32		m_fAudioBalance;
		f32		m_fCurrentTime;
		u32		m_uiWidth;
		u32		m_uiHeight;
		u8*		m_pVideoImage;

		algorithm::string							m_Filename;
		TKGLPtr<IMFMediaSession>					m_pSession;
		TKGLPtr<IMFByteStream>						m_pByteStream;
		TKGLPtr<IMFMediaSource>						m_pMediaSource;
		TKGLPtr<IMFPresentationClock>				m_pPresentationClock;
		TKGLPtr<IMFVideoDisplayControl>				m_pVideoDisplay;
		TKGLPtr<IMFAudioStreamVolume>				m_pAudioVolume;
		TKGLPtr<Thread::ICriticalSection>			m_pCSSession;
		TKGLPtr<Thread::ICriticalSection>			m_pCSEvent;
		algorithm::vector<TKGLPtr<IMFMediaEvent>>	m_pEvents;
	};

	//======================================================================
	//!	ムービープレイヤー
	//======================================================================
	class CMoviePlayer
		: public IMoviePlayer
	{
	public:
		//	コンストラクタ
		CMoviePlayer(void)
		{
			*m_pSession.GetReference() = new CMFSession();
		}
		//	デストラクタ
		~CMoviePlayer(void)
		{
			if( m_pSession.IsValid() )
			{
				m_pSession->ReleaseMovie();
				m_pSession = null;
			}
		}

	public:
		//	読み込み
		bool Load(const c16* pFilename)
		{
			m_pTexture = null;
			return m_pSession->LoadMovie(pFilename);
		}

		//	再生
		bool Play(bool is_loop)
		{
			return m_pSession->PlayMovie(is_loop);
		}

		//	停止
		void Stop(void)
		{
			m_pSession->StopMovie();
		}
		//	一時停止
		void Pause(void)
		{
			m_pSession->PauseMovie();
		}
		//	再生再開
		void Resume(void)
		{
			m_pSession->ResumeMovie();
		}

		//	早送り
		bool Forward(u32 uiTime)
		{
			return m_pSession->ForwardMovie(uiTime);
		}
		//	巻き戻し
		bool Backward(u32 uiTime)
		{
			return m_pSession->BackwardMovie(uiTime);
		}

		//	再生位置の指定
		bool Seek(u32 uiTime)
		{
			return m_pSession->SeekMovie(uiTime);
		}

		//	再生速度の設定
		void SetSpeed(f32 fSpeed)
		{
			m_pSession->SetSpeed(fSpeed);
		}
		//	パンの設定
		void SetPan(f32 fPan)
		{
			m_pSession->SetPan(fPan);
		}
		//	音量の設定
		void SetVolume(f32 fVolume)
		{
			m_pSession->SetVolume(fVolume);
		}

		//	動画の再生時間を取得
		f32 MovieTime(void) const
		{
			return m_pSession->MovieTime();
		}
		//	現在の再生時間を取得
		f32 CurrentTime(void) const
		{
			return m_pSession->CurrentTime();
		}
		//	動画の画面サイズを取得
		void GetResolution(u32& width, u32& height) const
		{
			m_pSession->GetResolution(width, height);
		}
		//	映像のあるデータか？
		bool HasVideo(void) const
		{
			return m_pSession->HasVideo();
		}
		//	音声のあるデータか？
		bool HasAudio(void) const
		{
			return m_pSession->HasAudio();
		}

		//	再生中か？
		bool IsPlay(void) const
		{
			return m_pSession->IsPlayMovie();
		}
		//	ループ中か？
		bool IsLoop(void) const
		{
			return m_pSession->IsLoopMovie();
		}

	public:
		//	テクスチャーの取得
		void Update(void)
		{
			m_pSession->Update();

			u8* pBit;
			dword cbDib;
			s64 i64TimeStamp;
			BITMAPINFOHEADER bih = {0};
			bih.biSize = sizeof(bih);
			if( m_pSession->GetVideoImage(&bih, &pBit, &cbDib, &i64TimeStamp) )
			{
				auto pDriver = KGLGameManager()->GraphicDriver();
				pDriver->GraphicDevice()->WaitForCommand();
				if( !m_pTexture.IsValid() )
				{
					TKGLPtr<kgGraphics::ITextureBase> pTexture;
					kgGraphics::TextureInfo Info;
					Info.iFormat	= kgGraphics::ETextureFormat::A8R8G8B8;
					Info.iUsage		= ETextureUsage::Dynamic;
					Info.iType		= ETextureType::Normal;
					Info.iMipLevels	= 1;
					Info.uiWidth	= Align(bih.biWidth, 8L);
					Info.uiHeight	= Align(bih.biHeight, 8L);
					pDriver->TextureManager()->CreateTexture(pTexture.GetReference(), Info);
					kgl::Render::Create(m_pTexture.GetReference(), pTexture);
				}
				if( m_pTexture.IsValid() )
				{
					void* pBits = m_pTexture->GetResource()->Lock(0, kgGraphics::ELockType::Write);
					u8* pTemp = (u8*)pBits;
					u8* pBuf = pBit;

					auto& Info = m_pTexture->GetInfo();
					u32 uiStride = kgGraphics::GetStride(Info.iFormat);
					u32 uiSrcStride = bih.biBitCount / 8;
					for( u32 y=0; y < bih.biHeight; y++ )
					{
						 u32 uiOffset = (Info.uiHeight - y - 1) * Info.uiWidth * uiStride;
						 u8* pTemp = (u8*)pBits + uiOffset;
						 for( u32 x=0; x < bih.biWidth; x++ )
						 {
							 switch (pDriver->GetType())
							 {
							 case kgGraphics::EDriverType::DirectX9:
								 pTemp[0] = pBuf[0];
								 pTemp[1] = pBuf[1];
								 pTemp[2] = pBuf[2];
								 pTemp[3] = 0xff;
								 break;
							 default:
								 pTemp[0] = pBuf[2];
								 pTemp[1] = pBuf[1];
								 pTemp[2] = pBuf[0];
								 pTemp[3] = 0xff;
								 break;
							 }

							 pTemp += uiStride;
							 pBuf += uiSrcStride;
						 }
					}
					m_pTexture->GetResource()->Unlock();
				}
				m_pSession->ReleaseVideoImage();
			}
		}
		//	テクスチャーの取得
		TKGLPtr<Render::ITexture> GetTexture(void)
		{
			return m_pTexture;
		}

	private:
		TKGLPtr<CMFSession>			m_pSession;
		TKGLPtr<Render::ITexture>	m_pTexture;
	};

	//	コンストラクタ
	CMovieManager::CMovieManager(void)
	{
		MFStartup(MF_VERSION);
	}
	//	デストラクタ
	CMovieManager::~CMovieManager(void)
	{
		MFShutdown();
	}

	//	ムービープレイヤーの作成
	TKGLPtr<IMoviePlayer> CMovieManager::CreateMovie(const c16* pFilename)
	{
		TKGLPtr<CMoviePlayer> pPlayer = new CMoviePlayer();
		if( pPlayer->Load(pFilename) )
		{
			return pPlayer.Get();
		}
		return null;
	}

	//	ムービープレイヤーの作成
	TKGLPtr<IMoviePlayer> CMovieManager::CreateMovie(const c8* pFilename)
	{
		c16 sFilename[256];
		u32 uiSize = ArrayCount(sFilename);
		CharSet::ConvertShiftJISToUTF16(sFilename, pFilename, &uiSize);

		return CreateMovie(sFilename);
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
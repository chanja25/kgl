//----------------------------------------------------------------------
//!
//!	@file	MFMovie.h
//!	@brief	ムービー管理(Media Foundation)
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__MF_MOIVE_H__
#define	__MF_MOIVE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"


namespace App
{
	//======================================================================
	//!	ムービープレイヤー
	//======================================================================
	class IMoviePlayer
		: public IKGLBase
	{
	public:
		//!	再生
		//!	@param is_loop	[in] ループフラグ
		//!	@return 結果
		virtual bool Play(bool is_loop = false) = 0;
		//!	停止
		virtual void Stop(void) = 0;
		//!	一時停止
		virtual void Pause(void) = 0;
		//!	再開
		virtual void Resume(void) = 0;

		//!	早送り
		//!	@param uiTime	[in] ms
		virtual bool Forward(u32 uiTime) = 0;
		//!	巻き戻し
		//!	@param uiTime	[in] ms
		virtual bool Backward(u32 uiTime) = 0;

		//!	再生位置の指定
		//!	@param uiTime	[in] ms
		virtual bool Seek(u32 uiTime) = 0;

		//!	再生速度の設定
		//!	@param fSpeed	[in] 速度
		virtual void SetSpeed(f32 fSpeed) = 0;
		//!	パンの設定
		//!	@param fPan	[in] パン
		virtual void SetPan(f32 fPan) = 0;
		//!	音量の設定
		//!	@param fVolume	[in] 音量
		virtual void SetVolume(f32 fVolume) = 0;

		//!	動画の再生時間を取得
		//!	@return 動画の再生時間
		virtual f32 MovieTime(void) const = 0;
		//!	現在の再生時間を取得
		//!	@return 現在の再生時間
		virtual f32 CurrentTime(void) const = 0;
		//!	動画の画面サイズを取得
		//!	@param width	[out] 横幅
		//!	@param height	[out] 縦幅
		virtual void GetResolution(u32& width, u32& height) const = 0;
		//!	映像のあるデータか？
		//!	@return 映像の有無
		virtual bool HasVideo(void) const = 0;
		//!	音声のあるデータか？
		//!	@return 音声の有無
		virtual bool HasAudio(void) const = 0;

		//!	再生中か？
		//!	@return 再生中フラグ
		virtual bool IsPlay(void) const = 0;
		//!	ループ中か？
		//!	@return ループフラグ
		virtual bool IsLoop(void) const = 0;

	public:
		//!	更新
		virtual void Update(void) = 0;
		//!	テクスチャーの取得
		virtual TKGLPtr<Render::ITexture> GetTexture(void) = 0;
	};

	//======================================================================
	//!	ムービー管理クラス
	//======================================================================
	class CMovieManager
		: public IKGLBase
	{
	private:
		//!	コンストラクタ
		CMovieManager(void);
		//!	デストラクタ
		~CMovieManager(void);

	public:
		//!	ムービープレイヤーの作成
		//!	@param pFilename	[in] ファイル名
		//!	@return ムービープレイヤー
		TKGLPtr<IMoviePlayer> CreateMovie(const c16* pFilename);
		//!	ムービープレイヤーの作成
		//!	@param pFilename	[in] ファイル名
		//!	@return ムービープレイヤー
		TKGLPtr<IMoviePlayer> CreateMovie(const c8* pFilename);

	public:
		static CMovieManager& Instance(void)
		{
			static CMovieManager manager;
			return manager;
		}
	};
}

#endif	// __MF_MOIVE_H__
//======================================================================
//	END OF FILE
//======================================================================
<RhythmGame>

<MusicData path="music001.ogg" delay=0.280000 bpm=170.000000 margin=0.500000 level=0>

<RhythmData>
	<Rhythm timing=120 key0='Z'>
	<Rhythm timing=240 key1='X'>
	<Rhythm timing=360 key2='C'>
	<Rhythm timing=480 key3='V'>
</RhythmData>

</RhythmGame>

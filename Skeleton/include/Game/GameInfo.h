//----------------------------------------------------------------------
//!
//!	@file	GameInfo.h
//!	@brief	ゲーム情報管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__GAMEINFO_H__
#define	__GAMEINFO_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../App.h"
#include "Manager/kgl.GameManager.h"
#include "Manager/SaveManager.h"
#include "Manager/OptionManager.h"
#include "Manager/RhythmGameManager.h"
#include "Manager/ScenarioManager.h"
#include "../Input/Input.h"

namespace App
{
	//======================================================================
	//!	ゲーム情報クラス
	//======================================================================
	class CGameInfo
		: public Manager::IGameInfo
	{
	public:
		//!	コンストラクタ
		CGameInfo(void);
		//!	デストラクタ
		~CGameInfo(void);

		//!	初期化
		//!	@return 結果
		b8 Initialize(void);

	public:
		//!	リズムゲーム管理の取得
		//!	@return リズムゲーム管理
		FORCEINLINE CInput* Input(void) { return &m_Input; }
		//!	リズムゲーム管理の取得
		//!	@return リズムゲーム管理
		FORCEINLINE CRhythmGameManager* RhythmGameManager(void) { return &m_RhythmGameManager; }
		//!	シナリオ管理の取得
		//!	@return シナリオ管理
		FORCEINLINE CScenarioManager* ScenarioManager(void) { return &m_ScenarioManager; }
		//!	汎用フォントの取得
		//!	@return 汎用フォント
		FORCEINLINE Font::IFont* Font(void) { return m_pFont; }
	private:
		CInput					m_Input;				//!< 入力管理
		CRhythmGameManager		m_RhythmGameManager;	//!< リズムゲーム管理
		CScenarioManager		m_ScenarioManager;		//!< シナリオ管理
		TKGLPtr<Font::IFont>	m_pFont;				//!< 汎用フォント
	};
}


#define	AppGameInfo()		KGLGameManager()->GameInfo<App::CGameInfo>()
#define	AppInput()			AppGameInfo()->Input()
#define	RGManager()			AppGameInfo()->RhythmGameManager()
#define	ScenarioManager()	AppGameInfo()->ScenarioManager()

#endif	// __GAMEINFO_H__
//======================================================================
//	END OF FILE
//======================================================================
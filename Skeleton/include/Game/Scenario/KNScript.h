//----------------------------------------------------------------------
//!
//!	@file	KNScript.h
//!	@brief	ノベルスクリプト関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__KN_SCRIPT_H__
#define	__KN_SCRIPT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Data/xml/kgl.xml.h"


namespace App
{
	const s32 MaxStack	= 16;
	const s32 MaxValues	= 512;

	//!	スクリプト設定
	struct KNConfig
	{
		s32		iCharaWait;	//!< 文字の表示間隔
		s32		iPad[63];
	};
	//!	スクリプト変数値
	struct KNValue
	{
		c8		cName[60];	//!< 変数名
		s32		iType;		//!< 変数タイプ
		union
		{
			c8	cText[64];	//!< 文字列
			s32	iValue;		//!< 整数値
		};
	};
	//!	スクリプトセーブデータ
	struct KNSaveData
	{
		KNConfig	Config;				//!< コンフィグ
		KNValue		Values[MaxValues];	//!< 変数
		c8			cChapter[64];		//!< チャプター名
		c8			cXmlPath[128];		//!< XMLのパス
		c8			cLabel[64];			//!< ラベル
		c8			cName[64];			//!< 表示する名前
		c8			cBgm[128];			//!< 再生中のBGM
		s32			iPad[64];
	};

	//======================================================================
	//!	KNScriptクラス
	//======================================================================
	class CKNScript
	{
	private:
		//!	スタック情報
		struct KNScriptStack
		{
			string				strPath;
			TKGLPtr<Xml::IXml>	pXml;
			Xml::CNode*			pCurrent;
			BITFIELD			bInit: 1;
		};
		//!	メッセージ描画用
		struct Message
		{
			TKGLPtr<Xml::IXml>	pXml;			//!< XMLデータ
			Xml::CNode*			pMessage;		//!< メッセージルート
			Xml::CNode*			pCurrent;		//!< 現在のノード
			u32					uiLength;		//!< 文字列バッファの長さ
			u32					uiCharacters;	//!< 文字数
		};
		//!	画像描画用
		struct Graphic
		{
			Graphic*	pNext;
		};
		//!	レイヤー管理用
		struct Layer
		{
			Graphic* pGraphic;
		};
		//!	画面管理用
		struct Display
		{
			Layer	Layers[1];
		};

	public:
		//!	コンストラクタ
		CKNScript(void);
		//!	デストラクタ
		~CKNScript(void);

	public:
		//!	初期化
		//!	@param pData	[in] セーブデータ
		//!	@return 結果
		b8 Initialize(KNSaveData* pData = null);

	private:
		//!	ロード
		//!	@param pPath	[in] パス
		TKGLPtr<Xml::IXml> GetLoadedXml(const c8* pPath);
		//!	シナリオ情報のセットアップ
		//!	@param pXml	[in] シナリオデータXML
		void SetupXML(Xml::IXml* pXml);
		//!	シナリオ情報のセットアップ
		//!	@param pNode	[in] ノード
		void SetupNode(Xml::CNode* pNode);

		//!	スクリプト遷移
		//!	@param pXml		[in] XMLデータ
		//!	@param pLabel	[in] ラベル
		//!	@param bSet		[in] カレントノードを設定済みにするか？
		//!	@return ノード
		Xml::CNode* Jump(const c8* pPath, const c8* pLabel, b8 bSet = false);
		//!	スクリプト呼び出す
		//!	@param pXml		[in] XMLデータ
		//!	@param pLabel	[in] ラベル
		//!	@param bSet		[in] カレントノードを設定済みにするか？
		//!	@return ノード
		Xml::CNode* Call(const c8* pPath, const c8* pLabel, b8 bSet = false);
		//!	マクロを呼び出す
		//!	@param pMacro	[in] マクロ
		//!	@return ノード
		Xml::CNode* CallMacro(const string& strMacro);
		//!	呼び出したスクリプトから戻る
		//!	@return ノード
		Xml::CNode* Return(void);
		
	private:
		//!	変数の検索
		//!	@param strName	[in] 変数名
		//!	@return 変数
		KNValue* FindValue(const string& strName);
		//!	変数の設定(整数値)
		//!	@param strName	[in] 変数名
		//!	@param iValue	[in] 値
		KNValue* SetValue(const string& strName, s32 iValue);
		//!	変数の設定(文字列)
		//!	@param strName	[in] 変数名
		//!	@param strValue	[in] 値
		KNValue* SetValue(const string& strName, const string& strValue);
		//!	変数の加算
		//!	@param strName	[in] 変数名
		//!	@param iValue	[in] 値
		void AddValue(const string& strName, s32 iValue);
		//!	変数の減算
		//!	@param strName	[in] 変数名
		//!	@param iValue	[in] 値
		void SubValue(const string& strName, s32 iValue);
		//!	変数の掛け算
		//!	@param strName	[in] 変数名
		//!	@param iValue	[in] 値
		void MulValue(const string& strName, s32 iValue);
		//!	変数の割り算
		//!	@param strName	[in] 変数名
		//!	@param iValue	[in] 値
		void DivValue(const string& strName, s32 iValue);
		//!	変数のインクリメント
		//!	@param strName	[in] 変数名
		void IncValue(const string& strName);
		//!	変数のデクリメント
		//!	@param strName	[in] 変数名
		void DecValue(const string& strName);

		//!	if処理
		//!	@param pNode	[in] ノード
		//!	@return 結果
		b8 CheckIf(Xml::CNode* pNode);

		//!	ノードタイプを取得
		//!	@param pNode	[in] ノード
		//!	@return ノードタイプ
		u32 GetNodeType(Xml::CNode* pNode);

	private:
		//!	スクリプトの更新
		//!	@param fElaspsedTime	[in] 経過時間
		void UpdateScript(f32 fElapsedTime);
		//!	スクリプトの更新
		//!	@param fElaspsedTime	[in] 経過時間
		//!	@param uiFunc			[in] 処理番号
		//!	@param pNode			[in] ノード
		//!	@return ノード
		Xml::CNode* UpdateScript(f32 fElapsedTime, u32 uiFunc, Xml::CNode* pNode);

	private:
		//!	文字列の取得
		//!	@param pMessage	[in] ノード
		//!	@return	文字列
		const c8* GetMessage(Xml::CNode* pMessage);
		//!	文字数の取得
		//!	@param pNode	[in] ノード
		//!	@return	表示する最初のノード
		Xml::CNode* GetMessageBegin(Message& Message);
		//!	文字数の取得
		//!	@param pNode	[in] ノード
		//!	@return	文字数
		u32 GetMessageLength(Message& Message);
		//!	メッセージ処理の更新
		//!	@param fElaspsedTime	[in] 経過時間
		//!	@return	結果
		b8 UpdateMessage(f32 fElapsedTime);
		//!	メッセージ描画
		//!	@param pRenderingSystem	[in] 描画システム
		//!	@param pFont			[in] フォント
		void DrawMessage(Render::IRenderingSystem* pRenderingSystem, Font::IFont* pFont = null);

	public:
		//!	更新
		//!	@param fElaspsedTime	[in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem	[in] 描画システム
		void Draw(Render::IRenderingSystem* pRenderingSystem);

	public:
		//!	現在のスタック情報を取得
		//!	@return	スタック情報
		KNScriptStack* GetCurrentStack(void) { return m_iStack < 0? null: &m_Stack[m_iStack]; }
		//!	設定情報の取得
		//!	@return	設定情報
		const KNConfig* GetConfig(void) { return &m_Config; }
		//!	設定情報の取得
		//!	@return	設定情報
		const KNValue* GetValues(void) { return m_Values; }
		//!	スクリプトの実行中か？
		//!	@return	結果
		b8 IsRuning(void) const { return m_iStack >= 0 && m_Stack[m_iStack].pCurrent != null; }

	public:
		//!	設定情報の取得
		//!	@return	設定情報
		KNSaveData GetSaveData(void);

	private:
		KNConfig			m_Config;				//!< 設定情報
		TKGLPtr<Xml::IXml>	m_pMacro;				//!< マクロ
		KNScriptStack		m_Stack[MaxStack];		//!< スタック
		KNValue				m_Values[MaxValues];	//!< 管理変数値
		Message				m_DrawMessage;			//!< メッセージ描画用
		string				m_strPath;				//!< XMLのパス
		string				m_strLabel;				//!< ラベル
		string				m_strChapter;			//!< チャプター名
		string				m_strName;				//!< 名前
		string				m_strMessage;			//!< メッセージ
		s32					m_iStack;				//!< スタック番号
		s32					m_iValues;				//!< 変数の数
		s32					m_iStep;				//!< ステップ
		f32					m_fWait;				//!< 時間計測用
		f32					m_fCharWait;			//!< 文字列表示用
		u32					m_uiLength;				//!< 表示文字数
	};
}


#endif	// __KN_SCRIPT_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	RhythmGameEditor.h
//!	@brief	リズムゲーム関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__RHYTHMGAME_EDITOR_H__
#define	__RHYTHMGAME_EDITOR_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Audio/kgl.Audio.h"
#include "RhythmGame.h"
#include "Data/Text/kgl.Lexser.h"
#include "../../Utility/SelectMenu.h"

namespace App
{
	using namespace algorithm;

	namespace RhythmGame
	{
		namespace EEditMode
		{
			const s32 Select		= 0;
			const s32 Edit			= 1;
			const s32 BPMChecker	= 2;
			const s32 TimingCheck	= 3;
			const s32 TestPlay		= 4;
			const s32 DebugTitle	= 255;
		}
		//======================================================================
		//!	リズムイベント管理クラス
		//======================================================================
		class CRhythmEditor
		{
		public:
			//!	コンストラクタ
			CRhythmEditor(void);
			//!	デストラクタ
			~CRhythmEditor(void);

			//!	初期化
			//!	@return	結果
			b8 Initialize(void);
			//!	破棄
			void Finalize(void);

			//!	更新
			//!	@param fElapsedTime [in] 経過時間
			void Update(f32 fElapsedTime);
			//!	描画
			//!	@param pRenderingSystem [in] 描画システム
			void Render(Render::IRenderingSystem* pRenderingSystem);

		public:
			//!	エディット選択更新
			//!	@param fElapsedTime [in] 経過時間
			void UpdateSelect(f32 fElapsedTime);
			//!	エディット更新
			//!	@param fElapsedTime [in] 経過時間
			void UpdateEdit(f32 fElapsedTime);
			//!	BPM計測更新
			//!	@param fElapsedTime [in] 経過時間
			void UpdateBPMChecker(f32 fElapsedTime);
			//!	テストプレイ更新
			//!	@param fElapsedTime [in] 経過時間
			void UpdateTestPlay(f32 fElapsedTime);

			//!	エディット選択描画
			//!	@param pRenderingSystem [in] 描画システム
			void RenderSelect(Render::IRenderingSystem* pRenderingSystem);
			//!	エディット描画
			//!	@param pRenderingSystem [in] 描画システム
			void RenderEdit(Render::IRenderingSystem* pRenderingSystem);
			//!	BPM計測描画
			//!	@param pRenderingSystem [in] 描画システム
			void RenderBPMChecker(Render::IRenderingSystem* pRenderingSystem);
			//!	テストプレイ描画
			//!	@param pRenderingSystem [in] 描画システム
			void RenderTestPlay(Render::IRenderingSystem* pRenderingSystem);

			//!	モードの変更
			//!	@param iMode [in] モード
			void ChangeMode(s32 iMode);

		public:
			//!	時間の取得
			//!	@param iTiming [in] タイミング
			//!	@return 時間
			f32 GetTime(s32 iTiming);
			//!	タイミングの取得
			//!	@param fTime [in] 時間
			//!	@return タイミング
			s32 GetTiming(f32 fTime);
			//!	イベント番号の取得
			//!	@param fTime [in] 時間
			//!	@return イベント番号
			s32 GetEventIndex(f32 fTime, EventBase* pEvent, s32 iCount);

			//!	BPMの取得
			//!	@return BPM
			f32 GetBPM(void){ return m_fBPM; }
			//!	BG番号の取得
			//!	@return BG番号
			s32 GetBG(void){ return m_iBG; }

		private:
			//!	楽曲情報の取得
			//!	@return 楽曲情報
			MusicData* GetMusicData(void){ return m_pMusicData; }
			//!	リズム情報の取得
			//!	@return リズム情報
			RhythmData* GetRhythmData(s32 iNo = 0){ return iNo < (s32)m_RhythmData.size()? &m_RhythmData[iNo]: null; }
			//!	BPM変更情報の取得
			//!	@return BPM変更情報
			BPMData* GetBPMData(s32 iNo = 0){ return iNo < (s32)m_BPMData.size()? &m_BPMData[iNo]: null; }
			//!	BG変更イベント情報の取得
			//!	@return BG変更イベント情報
			BGData* GetBGData(s32 iNo = 0){ return 	iNo < (s32)m_BGData.size()? &m_BGData[iNo]: null; }

		private:
			//!	エディットデータの保存
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 Save(const c8* pPath);
			//!	エディットデータの読み込み
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 Load(const c8* pPath);

			//!	楽曲データのセットアップ
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 SetupMusicData(const Text::Token** ppToken);
			//!	リズムデータのセットアップ
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 SetupRhythmData(const Text::Token** ppToken);
			//!	BPMデータのセットアップ
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 SetupBPMData(const Text::Token** ppToken);
			//!	BGデータのセットアップ
			//!	@param pPath [in] パス
			//!	@return 結果
			b8 SetupBGData(const Text::Token** ppToken);

		private:
			CRhythmGameManager*	m_pRhythmGameManager;	//!< リズムゲーム管理
			const c8*			m_pPath;				//!< ファイルパス

			s32					m_iMode;				//!< モード
			s32					m_iStep;				//!< ステップ
			s32					m_iSelect;				//!< 選択用

			MusicData*			m_pMusicData;			//!< 曲情報
			vector<RhythmData>	m_RhythmData;			//!< リズム情報
			vector<BPMData>		m_BPMData;				//!< BPM情報
			vector<BGData>		m_BGData;				//!< BG情報

			f32					m_fBPM;					//!< BPM
			f32					m_fDelay;				//!< 遅延時間
			f32					m_fCurrentTime;			//!< 現在の時間
			s32					m_iBG;					//!< BG番号

			vector<string>		m_strEditList;			//!< 編集データリスト
			BITFIELD			m_bFailed: 1;			//!< 成功フラグ
			CSelectMenu			m_EditSelect;			//!< 編集用

			CMusic				m_Music;				//!< 楽曲制御
			f32					m_fBPMList[64];			//!< BPM計測用
			s32					m_iBPMCount;			//!< BPM計測数
			s32					m_iBPMIndex;			//!< 現在の計測位置
		};
	}
}

#endif	// __RHYTHMGAME_EDITOR_H__
//=======================================================================
//	END OF FILE
//=======================================================================
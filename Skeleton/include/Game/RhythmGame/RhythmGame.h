//----------------------------------------------------------------------
//!
//!	@file	RhythmGame.h
//!	@brief	リズムゲーム関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__RHYTHMGAME_H__
#define	__RHYTHMGAME_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Audio/kgl.Audio.h"
#include "RhythmGameData.h"

namespace App
{
	namespace RhythmGame
	{
		INLINE c8 GetCharCode(s32 iInput, s32 iNo)
		{
			c8 cKey = 0;
			switch( iNo )
			{
			case 0:	cKey = (c8)((iInput >> 0) & 0xff);	break;
			case 1:	cKey = (c8)((iInput >> 8) & 0xff);	break;
			case 2:	cKey = (c8)((iInput >> 16) & 0xff);	break;
			case 3:	cKey = (c8)((iInput >> 24) & 0xff);	break;
			}
			return cKey;
		}
		INLINE s32 GetInputKey(s32 iInput, s32 iNo)
		{
			const s32 InputTable[] =
			{
				Input::EKey::KEY_A,
				Input::EKey::KEY_B,
				Input::EKey::KEY_C,
				Input::EKey::KEY_D,
				Input::EKey::KEY_E,
				Input::EKey::KEY_F,
				Input::EKey::KEY_G,
				Input::EKey::KEY_H,
				Input::EKey::KEY_I,
				Input::EKey::KEY_J,
				Input::EKey::KEY_K,
				Input::EKey::KEY_L,
				Input::EKey::KEY_M,
				Input::EKey::KEY_M,
				Input::EKey::KEY_O,
				Input::EKey::KEY_P,
				Input::EKey::KEY_Q,
				Input::EKey::KEY_R,
				Input::EKey::KEY_S,
				Input::EKey::KEY_T,
				Input::EKey::KEY_U,
				Input::EKey::KEY_V,
				Input::EKey::KEY_W,
				Input::EKey::KEY_X,
				Input::EKey::KEY_Y,
				Input::EKey::KEY_Z,
			};

			c8 cCharCode = GetCharCode(iInput, iNo);
			return cCharCode != 0? InputTable[cCharCode - 'A']: 0;
		}

		//!	リズム(入力)情報
		struct EventBase
		{
			f32			fTime;		//!< 入力タイミング
			BITFIELD	bEnable: 1;	//!< 有効フラグ
		};
		//!	リズム(入力)情報
		struct RhythmInfo
			: public EventBase
		{
			f32	fBPM;	//!< BPM
			s32	iInput;	//!< 入力内容(1byte * 4入力まで対応)
		};
		//! BPM情報
		struct BPMInfo
			: public EventBase
		{
			f32	fBPM;	//!< BPM
		}; 
		//!	背景情報(背景の内容はソースに埋め込む予定)
		struct BGInfo
			: public EventBase
		{
			s32	iBG;	//!< BG番号
		};

		//======================================================================
		//!	楽曲管理クラス
		//======================================================================
		class CMusic
		{
		public:
			//!	コンストラクタ
			CMusic(void);
			//!	デストラクタ
			~CMusic(void);

			//!	初期化
			//!	@param pInfo [in] 楽曲情報
			//!	@return	結果
			b8 Initialize(const MusicData* pInfo);
			//!	破棄
			void Finalize(void);
			//!	更新
			//!	@param fElapsedTime [in] 経過時間
			void Update(f32 fElapsedTime);

		public:
			//!	再生
			//!	@param fOffset [in] 再生開始位置(エディター用)
			//!	@return 結果
			b8 Play(f32 fOffset = -5.0f);
			//!	停止
			void Stop(void);
			//!	一時停止
			void Pause(void);
			//!	一時停止の解除
			void Resume(void);

		public:
			//!	現在の時間
			f32 CurrentTime(void);
			//!	再生中か？
			//!	@return 再生フラグ
			b8 IsPlaying(void);
			//!	一時停止中か？
			//!	@return 一時停止フラグ
			b8 IsPause(void);
			//!	再生が終了したか？
			//!	@return 再生終了フラグ
			b8 IsFinish(void);

		public:
			//-------------------------------------------------------------
			//!	@name エディター用
			//-------------------------------------------------------------
			//!@{
			//!	再生速度の変更
			//!	@param fRate [in] 再生速度
			void SetPlaySpeed(f32 fRate);
			//!	遅延の変更
			//!	@param fDelay [in] 遅延
			void SetDelay(f32 fDelay);
			//!@}

		private:
			TKGLPtr<Audio::IStreamSound>	m_pStream;		//!< ストリーム再生用
			BITFIELD						m_bPlay: 1;		//!< 再生フラグ
			BITFIELD						m_bPause: 1;	//!< 一時停止フラグ
			BITFIELD						m_bFinish: 1;	//!< 再生終了フラグ
			f32								m_fDelay;		//!< 遅延時間
			f32								m_fCounter;		//!< カウンタ
		};

		//======================================================================
		//!	リズムイベント管理クラス
		//======================================================================
		class CRhythmEvent
		{
		public:
			//!	コンストラクタ
			CRhythmEvent(void);
			//!	デストラクタ
			~CRhythmEvent(void);

			//!	初期化
			//!	@param pHeader [in] リズムヘッダー
			//!	@return	結果
			b8 Initialize(RhythmGameHeader* pHeader);
			//!	破棄
			void Finalize(void);

			//!	更新
			//!	@param fElapsedTime [in] 経過時間
			//!	@param fCurrentTime [in] 現在の時間
			void Update(f32 fElapsedTime, f32 fCurrentTime);
			//!	描画
			//!	@param pRenderingSystem [in] 描画システム
			void Render(Render::IRenderingSystem* pRenderingSystem);

		public:
			//!	時間の取得
			//!	@param iTiming [in] タイミング
			//!	@return 時間
			f32 GetTime(s32 iTiming);
			//!	タイミングの取得
			//!	@param fTime [in] 時間
			//!	@return タイミング
			s32 GetTiming(f32 fTime);
			//!	BPMの取得
			//!	@param iTiming [in] タイミング
			//!	@return BPM
			f32 GetBPM(s32 iTiming);
			//!	イベント番号の取得
			//!	@param fTime [in] 時間
			//!	@return イベント番号
			s32 GetEventIndex(f32 fTime, EventBase* pEvent, s32 iCount);

			//!	BPMの取得
			//!	@return BPM
			f32 GetBPM(void);
			//!	BG番号の取得
			//!	@return BG番号
			s32 GetBG(void);

		public:
			//!	リズム情報のリセット
			void Reset(void);

		protected:
			//!	リズムヘッダーの取得
			//!	@return リズムヘッダー
			RhythmGameHeader* GetRhythmGameHeader(void);
			//!	楽曲情報の取得
			//!	@return 楽曲情報
			MusicData* GetMusicData(void);
			//!	リズム情報の取得
			//!	@return リズム情報
			RhythmData* GetRhythmData(s32 iNo = 0);
			//!	BPM変更情報の取得
			//!	@return BPM変更情報
			BPMData* GetBPMData(s32 iNo = 0);
			//!	BG変更イベント情報の取得
			//!	@return BG変更イベント情報
			BGData* GetBGData(s32 iNo = 0);

		private:
			RhythmGameHeader*	m_pHeader;		//!< リズムヘッダー
			RhythmInfo*			m_pRhythmInfo;	//!< リズム情報
			BPMInfo*			m_pBPMInfo;		//!< BPM情報
			BGInfo*				m_pBGInfo;		//!< BPM情報
			f32					m_fBPM;			//!< BPM
			f32					m_fNextBPM;		//!< BPMのフェード用
			f32					m_fCurrentTime;	//!< 現在の時間
			u32					m_iBG;			//!< BG番号
		};
	}
}

#endif	// __RHYTHMGAME_H__
//=======================================================================
//	END OF FILE
//=======================================================================
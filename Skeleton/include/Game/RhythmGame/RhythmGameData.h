//----------------------------------------------------------------------
//!
//!	@file	RhythmGameData.h
//!	@brief	リズムゲームデータ
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__RHYTHMGAME_DATA_H__
#define	__RHYTHMGAME_DATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	namespace RhythmGame
	{
		//!	難易度
		namespace Level
		{
			const s32 Easy		= 0;	//!< 易しい
			const s32 Normal	= 1;	//!< 普通
			const s32 Hard		= 2;	//!< 難しい
		}

		//!	イベントタイプ
		namespace EventType
		{
			const s32 BG	= 1;	//!< 背景の変更
		}

		//!	リズムゲームヘッダー
		struct RhythmGameHeader
		{
			s32	iID;				//!< FOURCC('r', 'g', 'd', ' ')
			s32 iVersion;			//!< バージョン
			s32	iMusicDataOffset;	//!< 曲情報へのオフセット
			s32	iRhythmCount;		//!< リズム情報の数
			s32	iRhythmOffset;		//!< リズム情報へのオフセット
			s32	iBPMCount;			//!< イベント情報の数
			s32	iBPMOffset;			//!< イベント情報へのオフセット
			s32	iBGCount;			//!< イベント情報の数
			s32	iBGOffset;			//!< イベント情報へのオフセット
		};

		//!	曲情報
		struct MusicData
		{
			c8	cPath[256];		//!< サウンドデータのパス
			f32	fDelay;			//!< 再生するまでの遅延
			f32	fBPM;			//!< BPM
			f32	fTimingMargin;	//!< 入力タイミングの遊び
			s32	iLevel;			//!< レベル
		};
		//!	リズム(入力)情報
		struct RhythmData
		{
			s32	iTiming;	//!< 入力タイミング(4*60が1BPMとなる)
			s32	iInput;		//!< 入力内容(1byte * 4入力まで対応)
		};
		//! BPM情報
		struct BPMData
		{
			s32	iTiming;	//!< 入力タイミング(4*60が1BPMとなる)
			f32	fBPM;		//!< BPM
		}; 
		//!	背景情報(背景の内容はソースに埋め込む予定)
		struct BGData
		{
			s32	iTiming;	//!< 入力タイミング(4*60が1BPMとなる)
			s32	iBG;		//!< BG番号
		};
	}
}

#endif	// __RHYTHMGAME_DATA_H__
//=======================================================================
//	END OF FILE
//=======================================================================
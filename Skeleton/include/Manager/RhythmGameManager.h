//----------------------------------------------------------------------
//!
//!	@file	RhythmGameManager.h
//!	@brief	リズムゲーム管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__RHYTHMGAME_MANAGER_H__
#define	__RHYTHMGAME_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "../Game/RhythmGame/RhythmGame.h"
#include "../Game/RhythmGame/RhythmGameData.h"

namespace App
{
	//======================================================================
	//!	リズムゲーム管理
	//======================================================================
	class CRhythmGameManager
	{
	public:
		//!	コンストラクタ
		CRhythmGameManager(void);
		//!	デストラクタ
		~CRhythmGameManager(void);

	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	リズムゲームの設定
		//!	@param pData [in] データ
		//!	@return 結果
		b8 Setup(const void* pData);

	public:
		//!	リズムゲーム開始
		void Start(void);

		//!	一時停止
		void Pause(void);
		//!	一時停止の解除
		void Resume(void);

	public:
		//!	プレイ中か？
		//!	@return プレイフラグ
		b8 IsPlaying(void);
		//!	ポーズ中か？
		//!	@return ポーズフラグ
		b8 IsPause(void);

	public:
		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	public:
		//-------------------------------------------------------------
		//!	@name エディター用
		//-------------------------------------------------------------
		//!@{
		//!	全ての入力を表示するか設定
		//!	@param bEnable [in] フラグ
		void SetAllInput(b8 bEnable);
		//!	全ての入力を表示するか？
		//!	@return 有効か？
		b8 IsAllInput(void);
		//!@}

	private:
		RhythmGame::CMusic			m_Music;		//!< 曲の管理
		RhythmGame::CRhythmEvent	m_Rhythm;		//!< リズムの管理
		BITFIELD					m_bAllInput: 1;	//!< 全ての入力を表示する(エディット用)
	};
}

#endif	// __RHYTHMGAME_MANAGER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
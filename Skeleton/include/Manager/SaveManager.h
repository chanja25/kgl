//----------------------------------------------------------------------
//!
//!	@file	SaveManager.h
//!	@brief	セーブデータ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SAVE_MANAGER_H__
#define	__SAVE_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	//!	システムデータ
	struct SystemData
	{
		s32	iSystemVolume;	//!< ボリューム(システム)
		s32	iBGMVolume;		//!< ボリューム(BGM)
		s32	iSEVolume;		//!< ボリューム(SE)
		s32	iVoiceVolume;	//!< ボリューム(ボイス)
	};

	//!	ゲームデータ
	struct GameData
	{
	};

	//======================================================================
	//!	リズムゲーム管理
	//======================================================================
	class CSaveManager
	{
	public:
		//!	初期化
		void Initialize(void);

	public:
		//!	保存
		//!	@param pPath [in] パス
		//!	@return 結果
		b8 Save(const c8* pPath);
		//!	読み込み
		//!	@param pPath [in] パス
		//!	@return 結果
		b8 Load(const c8* pPath);

	public:
		//!	システムボリュームの取得
		//!	@param iVolume [in] ボリューム
		void SetSystemVolume(s32 iVolume) { m_SystemData.iSystemVolume = iVolume; }
		//!	BGMボリュームの取得
		//!	@param iVolume [in] ボリューム
		void SetBGMVolume(s32 iVolume) { m_SystemData.iBGMVolume = iVolume; }
		//!	SEボリュームの取得
		//!	@param iVolume [in] ボリューム
		void SetSEVolume(s32 iVolume) { m_SystemData.iSEVolume = iVolume; }
		//!	ボイスボリュームの取得
		//!	@param iVolume [in] ボリューム
		void SetVoiceVolume(s32 iVolume) { m_SystemData.iVoiceVolume = iVolume; }

	public:
		//!	システムデータの取得
		//!	@return システムデータ
		const SystemData& GetSystemData(void) { return m_SystemData; }
		//!	ゲームデータの取得
		//!	@return ゲームデータ
		const GameData& GetGameData(void) { return m_GameData; }

	private:
		SystemData	m_SystemData;	//!< システムデータ
		GameData	m_GameData;		//!< ゲームデータ
	};
}

#endif	// __SAVE_MANAGER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
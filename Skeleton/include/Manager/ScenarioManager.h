//----------------------------------------------------------------------
//!
//!	@file	ScenarioManager.h
//!	@brief	シナリオ管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENARIO_MANAGER_H__
#define	__SCENARIO_MANAGER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Game/Scenario/KNScript.h"

namespace App
{
	//======================================================================
	//!	リズムゲーム管理
	//======================================================================
	class CScenarioManager
	{
	public:
		//!	コンストラクタ
		CScenarioManager(void);
		//!	デストラクタ
		~CScenarioManager(void);

	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

	public:
		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	public:
		//!	スクリプトの実行中か？
		//!	@return 結果
		INLINE b8 IsRunning(void) { return m_Script.IsRuning(); }

	private:
		CKNScript	m_Script;	//!< スクリプトリスト
	};
}

#endif	// __SCENARIO_MANAGER_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Scene.h
//!	@brief	�V�[��
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_H__
#define	__SCENE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "Debug/SceneDebugTitle.h"
#include "Debug/SceneTest.h"
#include "Debug/SceneMovieTest.h"
#include "Debug/SceneVoiceTest.h"
#include "Debug/SceneGuiTest.h"
#include "Debug/SceneInputTest.h"


#endif	// __SCENE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	SceneTest.h
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_VOICETEST_H__
#define	__SCENE_VOICETEST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Utility/Crypto/kgl.crypto.h"

namespace App
{
	//======================================================================
	//!	ボイステスト用シーン
	//======================================================================
	class CSceneVoiceTest
		: public Scene::ISceneBase
	{
	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	private:
		TKGLPtr<kgSound::ISoundBuffer>	m_pSound;
	};
}

#endif	// __SCENE_VOICETEST_H__
//=======================================================================
//	END OF FILE
//=======================================================================
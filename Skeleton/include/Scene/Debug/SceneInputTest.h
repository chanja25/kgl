//----------------------------------------------------------------------
//!
//!	@file	SceneInputTest.h
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_INPUT_TEST_H__
#define	__SCENE_INPUT_TEST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Utility/Crypto/kgl.crypto.h"

namespace App
{
	//======================================================================
	//!	テスト用シーン
	//======================================================================
	class CSceneInputTest
		: public Scene::ISceneBase
	{
	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);
	};
}

#endif	// __SCENE_INPUT_TEST_H__
//=======================================================================
//	END OF FILE
//=======================================================================
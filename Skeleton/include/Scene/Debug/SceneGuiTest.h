//----------------------------------------------------------------------
//!
//!	@file	SceneTest.h
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_GUITEST_H__
#define	__SCENE_GUITEST_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Control/TextBox.h"

namespace App
{
	struct EditText
	{
		wstring		strText;
		wstring		strCompStr;
		vector<u8>	ucCompAttr;
		s32			iCaret;
		s32			iOldCaret;
		s32			iSelectStart;
		s32			iIMECursorPos;
		s32			iBlink;
		BITFIELD	bEnable: 1;
		BITFIELD	bCaretOn: 1;
		BITFIELD	bInserMode: 1;
	};

	//======================================================================
	//!	GUIテスト用シーン
	//======================================================================
	class CSceneGuiTest
		: public Scene::ISceneBase
	{
	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);
		
		//!	ウィンドウイベント用
		static void WindowEvent(Window::IWindow* pWindow, Window::IWindowEvent* pEvent, s32 iMsg, s32 wParam, s32 lParam);

	private:
		TKGLPtr<Thread::ICriticalSection>	m_pCS;
		TKGLPtr<Window::IWindowEvent>		m_pEvent;
		TKGLPtr<Font::IFont>				m_pFont;
		EditText							m_EditText;
		bool								m_bExit;
		Control::CEditText					m_Text;
	};
}

#endif	// __SCENE_GUITEST_H__
//=======================================================================
//	END OF FILE
//=======================================================================
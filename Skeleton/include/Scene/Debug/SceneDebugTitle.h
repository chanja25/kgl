//----------------------------------------------------------------------
//!
//!	@file	CSceneDebugTitle.h
//!	@brief	デバッグタイトル
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENEDEBUGTITLE_H__
#define	__SCENEDEBUGTITLE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/kgl.Scene.h"

namespace App
{
	//======================================================================
	//!	デバッグタイトル
	//======================================================================
	class CSceneDebugTitle
		: public Scene::ISceneBase
	{
	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	public:
		s32	m_iSelect;	//!< セレクト
	};
}

#endif	// __SCENEDEBUGTITLE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
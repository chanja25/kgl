//----------------------------------------------------------------------
//!
//!	@file	SceneRhythmGameEdit.h
//!	@brief	リズムゲーム編集
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_RHYTHMGAME_EDIT_H__
#define	__SCENE_RHYTHMGAME_EDIT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/kgl.Scene.h"
#include "Game/RhythmGame/RhythmGameEditor.h"

namespace App
{
	//======================================================================
	//!	リズムゲーム編集
	//======================================================================
	class CSceneRhythmGameEdit
		: public Scene::ISceneBase
	{
	public:
		//!	コンストラクタ
		CSceneRhythmGameEdit(void);

		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	private:
		//CRhythmGameManager*	m_pRhythmGameManager;	//!< リズムゲーム管理
		RhythmGame::CRhythmEditor	m_RhythmEditor;			//!< リズムエディタ
		s32							m_iStep;
	};
}

#endif	// __SCENE_RHYTHMGAME_EDIT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
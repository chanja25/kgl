//----------------------------------------------------------------------
//!
//!	@file	SceneScenario.h
//!	@brief	シナリオシーン(ノベルパート)
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_SCENARIO_H__
#define	__SCENE_SCENARIO_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Manager/ScenarioManager.h"

namespace App
{
	//======================================================================
	//!	シナリオ(ノベルパート)
	//======================================================================
	class CSceneScenario
		: public Scene::ISceneBase
	{
	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	private:
		CScenarioManager*	m_pScenarioManager;	//!< シナリオ管理
	};
}

#endif	// __SCENE_SCENARIO_H__
//=======================================================================
//	END OF FILE
//=======================================================================
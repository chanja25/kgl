//----------------------------------------------------------------------
//!
//!	@file	CSceneRhythmGame.h
//!	@brief	リズムゲーム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_RHYTHMGAME_H__
#define	__SCENE_RHYTHMGAME_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/kgl.Scene.h"

namespace App
{
	using namespace algorithm;

	//======================================================================
	//!	リズムゲーム
	//======================================================================
	class CSceneRhythmGame
		: public Scene::ISceneBase
	{
	public:
		//!	コンストラクタ
		CSceneRhythmGame(void);
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	private:
		CRhythmGameManager*	m_pRhythmGameManager;	//!< リズムゲーム管理
		s32					m_iStep;				//!< ステップ
		s32					m_iSelect;				//!< 選択用
		const c8*			m_pPath;				//!< ファイルパス
		vector<string>		m_strFileList;			//!< ファイルリスト
	};
}

#endif	// __SCENE_RHYTHMGAME_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	SceneAdventure.h
//!	@brief	アドベンチャーシーン(探索パート)
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCENE_ADVENTURE_H__
#define	__SCENE_ADVENTURE_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	//======================================================================
	//!	アドベンチャー(探索パート)
	//======================================================================
	class CSceneAdventure
		: public Scene::ISceneBase
	{
	public:
		//!	初期化
		b8 Initialize(void);
		//!	破棄
		void Finalize(void);

		//!	更新
		//!	@param fElapsedTime [in] 経過時間
		void Update(f32 fElapsedTime);
		//!	描画
		//!	@param pRenderingSystem [in] 描画システム
		void Render(Render::IRenderingSystem* pRenderingSystem);

	private:
	};
}

#endif	// __SCENE_ADVENTURE_H__
//=======================================================================
//	END OF FILE
//=======================================================================
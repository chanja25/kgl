//----------------------------------------------------------------------
//!
//!	@file	System.h
//!	@brief	システム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SYSTEM_H__
#define	__SYSTEM_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "System/kgl.System.h"
#include "Audio/kgl.Audio.h"

namespace App
{
	namespace System
	{
		//======================================================================
		//	システムクラス
		//======================================================================
		class CSystem
			: public kgl::System::ISystem
		{
		public:
			//!	コンストラクタ
			CSystem(void);
			//!	デストラクタ
			~CSystem(void);

		private:
			//!	初期化
			b8 Initialize(void);
			//!	破棄
			void Finalize(void);

			//!	更新
			//!	@param fElapsedTime [in] 経過時間
			void Update(f32 fElapsedTime);
			//!	描画
			//!	@param pRenderingSystem [in] 描画システム
			void Render(Render::IRenderingSystem* pRenderingSystem);

		public:
#if	APP_DEBUG
			//!	デバッグ描画
			//!	@param pRenderingSystem	[in] 描画システム
			virtual void DebugRender(Render::IRenderingSystem* pRenderingSystem);
#endif	// ~#if	APP_DEBUG
		};
	}
}

#endif	// __SYSTEM_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	Scene.h
//!	@brief	シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__MENU_SELECT_H__
#define	__MENU_SELECT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"


namespace App
{
	using namespace algorithm;

	//=======================================================================
	//	選択メニュークラス
	//=======================================================================
	class CSelectMenu
	{
	public:
		//!	コンストラクタ
		CSelectMenu(void);
		//!	デストラクタ
		~CSelectMenu(void);

		//!	初期化
		//!	@param iRow [in] 横の列の最大数
		//!	@param iColumn [in] 縦の列の最大数
		//!	@return 結果
		void Initialize(s32 iRow, s32 iColumn);
		//!	破棄
		void Finalize(void);
		//!	値の設定
		//!	@param iRow [in] 横の列の最大数
		//!	@param iColumn [in] 縦の列の最大数
		//!	@return 結果
		void SetValue(s32 iRow, s32 iColumn, s32 iValue);

	public:
		//!	選択対象を変更
		//!	@param iRow [in] 横の列(-1の場合は選択中の値)
		//!	@param iColumn [in] 縦の列(-1の場合は選択中の値)
		void SetValue(s32 iRow = -1, s32 iColumn = -1);
		//!	値を取得
		//!	@param iRow [in] 横の列(-1の場合は選択中の値)
		//!	@param iColumn [in] 縦の列(-1の場合は選択中の値)
		//!	@return 結果
		s32	GetValue(s32 iRow = -1, s32 iColumn = -1);

		//!	横の選択対象を進める
		void RowNext(void);
		//!	横の選択対象を進める
		void RowPrev(void);
		//!	縦の選択対象を進める
		void ColumnNext(void);
		//!	縦の選択対象を進める
		void ColumnPrev(void);

		//!	横の現在位置を取得
		//!	@return 横の現在位置
		s32 Row(void);
		//!	縦の現在位置を取得
		//!	return 横の現在位置
		s32 Column(void);

		//!	横の列の最大数を取得
		//!	@return 横の現在位置
		s32 RowMax(void);
		//!	縦の列の最大数を取得
		//!	return 横の現在位置
		s32 ColumnMax(void);

	protected:
		s32		m_iRowMax;		//!< 横の列の最大数
		s32		m_iColumnMax;	//!< 縦の列の最大数
		s32		m_iRow;			//!< 横の現在位置
		s32		m_iColumn;		//!< 縦の現在位置
		s32*	m_pValues;		//!< 値のリスト
	};
}

#endif	// __MENU_SELECT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	TextBox.h
//!	@brief	テキストボックス
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__TEXT_BOX_H__
#define	__TEXT_BOX_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Thread/kgl.CriticalSection.h"
#include "Thread/kgl.Event.h"
#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Random/kgl.Random.h"
#include <msctf.h>

namespace App
{
	namespace Control
	{
		const s32 NoLimitLength	= -1;
		const s32 MaxHistory = 64;
		const u32 DefaultMaxCandList = 10;

		class CTsfManager;
		//======================================================================
		//!	テキスト入力制御クラス
		//======================================================================
		class CEditText
		{
			friend CTsfManager;
		public:
			CEditText(void);

			bool Initialize(s32 iMaxLength = NoLimitLength);
			void Finalize(void);

			void Update(void);

		private:
			void Update(HWND hWnd, s32 iMsg, s32 wParam, s32 lParam);

		private:
			bool AddChara(c16 cChar);
			bool InsertChara(c16 cChar);
			bool AddString(const wstring& strText);
			bool DeleteSelection(void);

			void SetCompText(const wstring& strText, const vector<u8>& ucAttr, s32 iCursol);
			void ClearCompText(void);

			bool CopyClipBoard(bool bCut);
			bool PasteClipBoard(void);

			void OnDelete(void);
			void OnBack(void);
			void OnLeft(void);
			void OnRight(void);
			void OnInsert(void);
			void OnAllSelect(void);
			void OnPrev(void);
			void OnNext(void);
			void PushHistory(void);
			void ResetHistory(void);

		public:
			void SetMaxCandList(u32 uiMax);
			void SetEnable(bool b) { bEnable = b; }
			void SetForcus(bool b) { bForcus = b; }
			void SetEnableLineBreak(bool b) { bLineBreak = b; }
			void SetMaxLength(s32 iLength) { iMaxLength = iLength; }

			const wstring& GetText(void) { return strTextTemp; }
			const wstring& GetCompText(void) { return strCompStrTemp; }
			const vector<u8>& GetCompAttr(void) { return ucCompAttrTemp; }
			const vector<wstring>& GetCandidate(void) { return strCandidate; }
			u32 GetCandidateSelection(void) { return uiSelection; }

			s32 GetCurrentCursol(void) { return iCaret; }
			s32 GetSelectStart(void) { return iSelectStart; }
			s32 GetCompCursol(void) { return iIMECursorPos; }
			bool IsInsertMode(void) { return !!bInsertMode; }
			bool IsEnable(void) { return IsForcus() && !!bEnable; }
			bool IsForcus(void) { return !!bForcus; }
			bool IsShowCursol(void) { return IsForcus() && !(iBlink & 0x20); }

		private:
			struct History
			{
				wstring	strText;
				s32		iCaret;
			};

		private:
			TKGLPtr<CTsfManager>	pTsfManager;
			wstring					strTextTemp;
			wstring					strCompStrTemp;
			vector<u8>				ucCompAttrTemp;
			vector<wstring>			strCandidate;
			u32						uiSelection;
			wstring					strText;
			wstring					strCompStr;
			vector<u8>				ucCompAttr;
			vector<History>			Histories;
			s32						iCurrentStack;
			s32						iMaxLength;
			s32						iCaret;
			s32						iSelectStart;
			s32						iIMECursorPos;
			s32						iBlink;
			BITFIELD				bEnable: 1;
			BITFIELD				bForcus: 1;
			BITFIELD				bLineBreak: 1;
			BITFIELD				bInsertMode: 1;
		};

		//======================================================================
		//!	TSF管理クラス
		//======================================================================
		class CTsfManager
			: public IKGLBase
		{
			friend CEditText;
		private:
			CTsfManager(void);
			~CTsfManager(void);

		private:
			bool Initialize(void);
			void Finalize(void);

			void Setup(void);
			void Update(HWND hWnd, s32 iMsg, s32 wParam, s32 lParam);

			static void WindowEvent(Window::IWindow* pWindow, Window::IWindowEvent* pEvent, s32 iMsg, s32 wParam, s32 lParam);

			void SetEditText(CEditText* pEditText);
			void DeleteEditText(CEditText* pEditText);

		private:
			void EnableUiUpdates(bool bEnable);
			void UpdateImeState(bool bResetCompartmentEventSink = false);
			bool SetupCompartmentSinks(bool bRemoveOnly = false, ITfCompartment* pTfOpenMode = null, ITfCompartment* pTfConvMode = null);
			bool GetCompartments(ITfCompartmentMgr** ppcm, ITfCompartment** ppTfOpenMode, ITfCompartment** ppTfConvMode);

			ITfUIElement* GetUIElement(dword dwUIElementId);
			void MakeReadingInformationString(ITfReadingInformationUIElement* preading);
			void MakeCandidateStrings(ITfCandidateListUIElement* pcandidate);

		protected:
			class CUIElementSink : public ITfUIElementSink, public ITfInputProcessorProfileActivationSink, public ITfCompartmentEventSink
			{
			public:
				CUIElementSink(CTsfManager* pTsfManager);
				~CUIElementSink(void);

				STDMETHODIMP QueryInterface(REFIID riid, void **ppvObj);
				STDMETHODIMP_(ULONG) AddRef(void);
				STDMETHODIMP_(ULONG) Release(void);

				STDMETHODIMP BeginUIElement(DWORD dwUIElementId, BOOL *pbShow);
				STDMETHODIMP UpdateUIElement(DWORD dwUIElementId);
				STDMETHODIMP EndUIElement(DWORD dwUIElementId);

				STDMETHODIMP OnActivated(DWORD dwProfileType, LANGID langid, REFCLSID clsid, REFGUID catid,
					REFGUID guidProfile, HKL hkl, DWORD dwFlags);

				STDMETHODIMP OnChange(REFGUID rguid);

			private:
				u32						m_uiRefCounter;
				TKGLPtr<CTsfManager>	m_pTsfManager;
			};

		private:
			u32 GetSelection(void) { return m_uiSelection; }
			const vector<wstring>& GetCandidate(void) { return m_strCandidate; }

			void SetMaxCandList(u32 uiMax) { m_uiMaxCandList = uiMax - 1; }

		private:
			TKGLPtr<ITfThreadMgrEx>				m_pThreadMgr;
			TKGLPtr<CUIElementSink>				m_pTsfSink;
			s32									m_iReadingError;
			u32									m_uiCandPageSize;
			u32									m_uiSelection;
			u32									m_uiCount;
			u32									m_uiPageStart;
			u32									m_uiPageCount;
			u32									m_uiCandRef;
			u32									m_uiMaxCandList;
			dword								m_dwUIElementSinkCookie;
			dword								m_dwOpenModeSinkCookie;
			dword								m_dwConvModeSinkCookie;
			dword								m_dwAlpnSinkCookie;
			vector<wstring>						m_strCandidate;
			vector<CEditText*>					m_pEditTexts;

			BITFIELD							m_bInit: 1;
			TKGLPtr<Window::IWindowEvent>		m_pWindowEvent;
			TKGLPtr<Thread::ICriticalSection>	m_pCS;
			TKGLPtr<Thread::IEvent>				m_pEvent;
			//b8									m_bHorizontalReading;
		};
	}
}

#endif	// __TEXT_BOX_H__
//=======================================================================
//	END OF FILE
//=======================================================================
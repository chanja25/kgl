//----------------------------------------------------------------------
//!
//!	@file	Input.h
//!	@brief	インプット
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__INPUT_H__
#define	__INPUT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Input/kgl.Input.h"
#include <map>

namespace App
{
	//======================================================================
	//!	インプットクラス
	//======================================================================
	class CInput
	{
	public:
		//!	コンストラクタ
		CInput(void);

		//!	初期化
		b8 Initialize(void);

		//!	キーのセット
		void SetKeyMapping(u32 uiKey, u32 uiKeyCode);

		//!	キーが押されているかチェック
		b8 IsKeyPress(u32 uiKey);
		//!	キーを押したかチェック
		b8 IsKeyPush(u32 uiKey);
		//!	キーが離されたかチェック
		b8 IsKeyRelease(u32 uiKey);

		//!	キーが押されているかチェック
		b8 IsButtonPress(u32 uiKey);
		//!	キーを押したかチェック
		b8 IsButtonPush(u32 uiKey);
		//!	キーが離されたかチェック
		b8 IsButtonRelease(u32 uiKey);

	private:
		std::map<u32, u32>			m_KeyMap;		//!< キーマップ
		TKGLPtr<Input::IKeyboard>	m_pKeyboard;	//!< キーボード
		TKGLPtr<Input::IJoyStick>	m_pGamepad;		//!< ゲームパッド
	};
}

#endif	// __INPUT_H__
//=======================================================================
//	END OF FILE
//=======================================================================
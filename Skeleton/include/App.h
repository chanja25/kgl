//----------------------------------------------------------------------
//!
//!	@file	App.h
//!	@brief	アプリケーション
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__APP_H__
#define	__APP_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "kgl.h"

//----------------------------------------------------------------------
// define
//----------------------------------------------------------------------
#define	APP_DEBUG	KGL_DEBUG
#define	USE_STEAM	0

using namespace kgl;
using namespace kgl::algorithm;

#include "Input/Input.h"
#include "System/System.h"
#include "Game/GameInfo.h"

#include "Scene/Scene.h"


namespace App
{
	//!	シーン
	enum ESceneMode
	{
		Scenario,

		RhithmGame,
		RhithmGameEdit,

		GuiTest,
		InputTest,
		Test,
		MovieTest,
		VoiceTest,
		DebugTitle,
	};

	//!	キーコード
	namespace EKeyCode
	{
		const u32 GAMEPAD = 0x10000000;	//!< ゲームパッド用のフラグ(無い場合はキーボードも含める)

		const u32 CROSS = 0;
		const u32 CIRCLE = 1;
		const u32 SQUARE = 2;
		const u32 TRIANGLE = 3;
		const u32 L1 = 4;
		const u32 R1 = 5;
		const u32 L2 = 6;
		const u32 R2 = 7;
		const u32 SELECT = 8;
		const u32 START = 9;
		const u32 UP = 10;
		const u32 DOWN = 11;
		const u32 LEFT = 12;
		const u32 RIGHT = 13;
	}
}

#endif	// __APP_H__
//======================================================================
//	END OF FILE
//======================================================================
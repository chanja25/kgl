//----------------------------------------------------------------------
//!
//!	@file	main.cpp
//!	@brief	メイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"


#if USE_STEAM
#pragma warning(disable:4819)
#include <steam_api.h>
#endif // USE_STEAM


extern void test();

extern void ScriptTest(void);
namespace kgl
{
	namespace Script
	{
		extern void ScriptBuild(void);
	}
}


#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
//#include "Utility/Crypto/kgl.cripto.h"

#if USE_STEAM
#pragma comment(lib, "steam_api64.lib")
#endif // USE_STEAM

#include <shlobj.h> 


//----------------------------------------------------------------------
//!	アプリケーションの実行
//!	@return 実行結果
//----------------------------------------------------------------------
s32 AppRun(Utility::ICommandLine* pCommandLine)
{
	/*
	DEVMODE devMode; //デバイスモード構造体
	devMode.dmSize = sizeof(DEVMODE);//構造体のサイズ

	//変更する項目(メンバ)を指定
	//  DM_PELSWIDTH ：ピクセル単位の幅
	//  DM_PELSHEIGHT：ピクセル単位の高さ
	devMode.dmFields=DM_PELSWIDTH | DM_PELSHEIGHT;
	devMode.dmPelsWidth = WINDOW_WIDTH; //幅
	devMode.dmPelsHeight = WINDOW_HEIGHT;    //高さ

	//ディスプレイの設定を切り替える
	//CDS_FULLSCREEN:フルスクリーン
	ChangeDisplaySettings(&devMode, CDS_FULLSCREEN);

	SetMenu(cWindow::Instance().Handle(), NULL);	//メニューを隠す

	SetWindowLong(cWindow::Instance().Handle(), GWL_STYLE, WS_VISIBLE | WS_POPUP);//ウィンドウのスタイルを変更

	MoveWindow( cWindow::Instance().Handle(), 
				0,
				0,
				WINDOW_WIDTH,
				WINDOW_HEIGHT, 
				TRUE);

	//ウインドウの表示
	ShowWindow(cWindow::Instance().Handle(),SW_MAXIMIZE);
	UpdateWindow(cWindow::Instance().Handle()) ;
	*/

	if( true )
	{
		for( int i=0; ; i++ )
		{
			DISPLAY_DEVICE display = {0};
			display.cb = sizeof(display);
			if( !EnumDisplayDevices(NULL, i, &display, 0) )
			{
				break;
			}

			{
				DEVMODE mode = {0};
				mode.dmSize = sizeof(DEVMODE);
				if( EnumDisplaySettings(display.DeviceName, ENUM_CURRENT_SETTINGS, &mode) )
				{
					const c16* primary = L"";
					if( display.StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE ) primary = L"(Primary)";
					Trace(L"Monitor[%s] X:%d, Y:%d %s", display.DeviceName, mode.dmPelsWidth, mode.dmPelsHeight, primary);
				}
			}
		}
	}


	if( false )
	{
		// フォルダーの位置を入れるためのバッファ
		TCHAR myDocumentsFolder[MAX_PATH];

		// フォルダーの位置の取得
		SHGetFolderPath(NULL, CSIDL_MYDOCUMENTS, NULL, NULL, myDocumentsFolder);

		// メッセージ ボックスにフォルダーの位置を表示
		MessageBox(NULL, myDocumentsFolder, _T("フォルダーの位置"), MB_OK);
	}

#if USE_STEAM
	bool bIsUseSteam = false;
	if( bIsUseSteam )
	{
		string steamAppId = "@echo ";
		steamAppId += "480";
		steamAppId += "> steam_appid.txt";
		system(steamAppId.c_str());

		if( !SteamAPI_Init() )
		{
			return false;
		}
	}
#endif // USE_STEAM

	if( false )
	{
		kgl::Random::CRandomXORShift random;
		random.Seed((s32)timeGetTime());
		TKGLPtr<crypto::ICrypto> pCrypto;
		//crypto::CreateAES(pCrypto.GetReference());
		crypto::CreateAES(pCrypto.GetReference());

		pCrypto->SetKey(L"1234567890");

		const u32 Count = 100;
		for( u32 i=0; i < Count; i++ )
		{
			s8 Buffer[256];
			for( u32 j=0; j < ArrayCount(Buffer); j++ )
			{
				Buffer[j] = (s8)random.Rand();
			}
			if( pCrypto->Encode(Buffer, sizeof(Buffer)) )
			{
				Trace("EncryptoSize:%d", pCrypto->GetSize());
			}
			else
			{
				kgAssert(false, "エラー！！！");
			}
		}
	}

	if( false )
	{
		kgl::Random::CRandomXORShift random;
		random.Seed((s32)timeGetTime());
		const s32 Count = 100000000;
		for( s32 i=0; i < Count; i++ )
		{
			f32 temp = random.RandRange(FLT_MIN, FLT_MAX);
			f16 hf	= Math::Float32To16(temp);
			f32 f	= Math::Float16To32(hf);
			//Trace("%f %f, %s", temp, f, Math::IsInfinite(f)? "Infinite": Math::IsNan(f)? "Nan": "None");
			kgAssert(!Math::IsInfinite(f) && !Math::IsNan(f), "error");
		}
	}
#if APP_DEBUG
	struct Test
	{
		bool	b1;
		bool	b2;
		s32		test[4];
		Vector4	vec;
		bool	b3: 1;
		bool	b4: 1;
		bool	b5: 1;
		bool	b6: 1;
		bool	b7: 1;
		bool	b8: 1;
		s32		test2;
		s32		test3: 8;
	};
	void* mem = Memory::Allocate(sizeof(Test)+1);
	Test* p = (Test*)((u8*)mem);
	kgZeroMemory(p, sizeof(Test));
	Trace("1:%d", STRUCT_OFFSET(Test, test));
	Trace("2:%d", STRUCT_OFFSET(Test, vec));
	Trace("3:%d", STRUCT_OFFSET(Test, test2));
	p->vec.x = 5.0f;
	p->vec.y = 10.0f;
	p->vec.z = 15.0f;
	Trace("%3.3f", p->vec.Length());
	Memory::Free(mem);

	Vector4 v1, v2, v3;
	v1 = Vector4(1.5f, 1.0f, 0.5f, 2.0f);
	v2 = Vector4(1.5f, 0.25f, 2.0f, 4.0f);
	v3 = v1 * v2;
	Vector3 v(1.0f, 0.55f, 5.0f);
	Trace(string(v3).c_str());
	Trace(string(v).c_str());
#endif // DEBUG
	(void)pCommandLine;

#if	CONSOLE_TOOL
	return 0;
#else	// ~#if CONSOLE_TOOL

	s32 iExitCode = -1;

	TKGLPtr<Window::IWindow> pWindow;
	//	ウィンドウの生成、初期化
	Window::Create(pWindow.GetReference());
	pWindow->Create(L"KGL Library Template");
	pWindow->SetEscapeExit(true);

	{
		App::System::CSystem System;
		//	KGLシステムの初期化
		System.InitializeKGL(pWindow);

		//	実行
		System.Run();

		//	KGLシステムの破棄
		System.FinalizeKGL();

		iExitCode = System.GetExitCode();
	}
	pWindow->Close();

#if USE_STEAM
	if( bIsUseSteam )
	{
		SteamAPI_Shutdown();
		system("del steam_appid.txt");
	}
#endif // USE_STEAM
	return iExitCode;
#endif	// ~#if CONSOLE_TOOL
}

IMPLEMENT_ENTRYPOINT;

#include <algorithm>

using namespace kgl::algorithm;

int compareS32(const void* p1, const void* p2)
{
	return *(s32*)p1 - *(s32*)p2;
}
int compareS32Ref(const s32& p1, const s32& p2)
{
	return p1 - p2;
}

template<typename T>
struct LinkList
{
	T			Value;		//!< 値
	LinkList*	pNext;		//!< 次のリンクリスト
	//LinkList*	pPrev;		//!< 前のリンクリスト
	LinkList*	pMargeNext;	//!< マージ用のリンクリスト
};


Time::IStopwatch* testStopwatch;
template<typename T>
FORCEINLINE void marge(LinkList<T>* list1, LinkList<T>* list2, LinkList<T>** first, bool (*compare)(const T&, const T&))
{
	LinkList<T>* list = null;
	LinkList<T>* start = null;

	if( compare(list1->Value, list2->Value) )
	{
		start = list1;
		list = list1;
		list1 = list1->pNext;
	}
	else
	{
		start = list2;
		list = list2;
		list2 = list2->pNext;
	}

	//	ノード1,2から昇順でListに登録していく
	while( list1 && list2 )
	{
		if( compare(list1->Value, list2->Value) )
		{
			list->pNext = list1;
			list = list1;
			list1 = list1->pNext;
		}
		else
		{
			list->pNext = list2;
			list = list2;
			list2 = list2->pNext;
		}
	}
	if( list1 )
	{
		list->pNext = list1;
		while( list1->pNext )
		{
			list1 = list1->pNext;
		}
	}
	else if( list2 )
	{
		list->pNext = list2;
		while( list2->pNext )
		{
			list2 = list2->pNext;
		}
	}

	*first = start;
}

template<typename T>
void marge_sort2(LinkList<T>** begin, bool (*compare)(const T&, const T&))
{
	LinkList<T>* first = null;
	LinkList<T>* margelist = null;
	LinkList<T>* list = *begin;
	LinkList<T>* temp;

	if( list && list->pNext )
	{
		temp = list->pNext->pNext;
		if( compare(list->pNext->Value, list->Value) )
		{
			list->pNext->pNext = list;
			list = list->pNext;
		}
		list->pNext->pNext = null;
	}
	else
	{
		temp = null;
	}
	margelist = list;
	margelist->pMargeNext = null;
	first = list;
	list = temp;

	while( list && list->pNext )
	{
		temp = list->pNext->pNext;
		if( compare(list->pNext->Value, list->Value) )
		{
			list->pNext->pNext = list;
			list = list->pNext;
		}
		list->pNext->pNext = null;

		margelist->pMargeNext = list;
		margelist = list;
		list = temp;
	}
	if( list )
	{
		margelist->pMargeNext = list;
		margelist = list;
		list = null;
	}
	margelist->pMargeNext = null;

	*begin = first;
}

template<typename T>
void marge_sort(LinkList<T>** begin, bool (*compare)(const T&, const T&))
{
	LinkList<T>* first;
	LinkList<T>* margelist = null;
	LinkList<T>* temp = null;
	LinkList<T>* list;

	{
		first = *begin;
		marge_sort2(&first, compare);
	}

	while( first->pMargeNext )
	{
		list = first;
		first = margelist = null;
		if( list )
		{
			temp = null;
			if( list->pMargeNext )
			{
				temp = list->pMargeNext->pMargeNext;
				marge(list, list->pMargeNext, &list, compare);
			}

			margelist = list;
			margelist->pMargeNext = null;
			first = list;
			list = temp;
		}

		while( list && list->pMargeNext )
		{
			temp = list->pMargeNext->pMargeNext;
			marge(list, list->pMargeNext, &list, compare);

			margelist->pMargeNext = list;
			margelist = list;
			list = temp;
		}
		if( list )
		{
			if( list->pMargeNext )
			{
				marge(list, list->pMargeNext, &list, compare);
			}
			margelist->pMargeNext = list;
			margelist = list;
			list = temp;
		}
		margelist->pMargeNext = null;
	}
	*begin = first;
}


	struct SortInfo
	{
		u64 iKey;
		u64 iTest[3];

		INLINE bool operator < (const SortInfo& temp) const
		{
			return iKey < temp.iKey;
		}
		INLINE bool operator > (const SortInfo& temp) const
		{
			return iKey > temp.iKey;
		}

		INLINE bool operator <= (const SortInfo& temp) const
		{
			return iKey <= temp.iKey;
		}
	};

	INLINE bool lesseq(const SortInfo& a, const SortInfo& b)
	{
		return a < b;
	}

//!	比較(==)
template<typename T>
struct comparefunc
{
	b8 operator()(const T& p1, const T& p2) const
	{
		return p1 < p2;
	}
};

void test()
{
#if	!KGL_FINAL_RELEASE
	Time::Create(&testStopwatch);

	TKGLPtr<Time::IStopwatch> pStopwatch;
	Time::Create(pStopwatch.GetReference());

	vector<SortInfo> sString;

	const u32 Count = 10000;
	srand((u32)timeGetTime());

	TSmartPtr<u64> TEST = new u64[Count];
	pStopwatch->Reset();
	{
		vector<TSmartPtr<s32>> temp;
		for( u32 i=0; i < Count; i++ )
		{
			TEST[i] = rand();
			temp.push_back(new s32[rand() % 1024]);
		}
	}
	f64 fTime = pStopwatch->Get();
	Trace("==============================");
	Trace("allocate:%f", fTime);

	sString.reserve(Count);
	//if( false )
	{
		sString.clear();
		for( u32 i=0; i < Count; i++ )
		{
			SortInfo Sort;
			Sort.iKey = TEST[i];
			sString.push_back(Sort);
		}
		pStopwatch->Reset();
		msort(&sString[0], sString.size());
		f64 fTime = pStopwatch->Get();
		Trace("marge sort2 Time:%f", fTime);
		//for( u32 i=0; i < sString.size(); i++ )
		//{
		//	Trace("%d", sString[i]);
		//}
		WaitTrace();
	}
	{
		sString.clear();
		for( u32 i=0; i < Count; i++ )
		{
			SortInfo Sort;
			Sort.iKey = TEST[i];
			sString.push_back(Sort);
		}
		pStopwatch->Reset();
		msort(&sString[0], sString.size());
		f64 fTime = pStopwatch->Get();
		Trace("marge sort2 Time:%f", fTime);
		//for( u32 i=0; i < sString.size(); i++ )
		//{
		//	Trace("%d", sString[i]);
		//}
		WaitTrace();
	}
	//if( false )
	{
		sString.clear();
		for( u32 i=0; i < Count; i++ )
		{
			SortInfo Sort;
			Sort.iKey = TEST[i];
			sString.push_back(Sort);
		}
		pStopwatch->Reset();
		//std::stable_sort(&sString[0], &sString[sString.size()-1], std::less_equal<SortInfo>());
		f64 fTime = pStopwatch->Get();
		Trace("marge sort3 Time:%f", fTime);
		WaitTrace();
	}
	//if( false )
	{
		sString.clear();
		for( u32 i=0; i < Count; i++ )
		{
			SortInfo Sort;
			Sort.iKey = TEST[i];
			sString.push_back(Sort);
		}
		pStopwatch->Reset();
		qsort(&sString[0], sString.size());
		f64 fTime = pStopwatch->Get();
		Trace("quick sort1 Time:%f", fTime);
		WaitTrace();
	}
	//if( false )
	//{
	//	sString.clear();
	//	for( u32 i=0; i < Count; i++ )
	//	{
	//		SortInfo Sort;
	//		Sort.iKey = TEST[i];
	//		sString.push_back(Sort);
	//	}
	//	pStopwatch->Reset();
	//	std::qsort(&sString[0], sString.size(), sizeof(s32), compareS32);
	//	f64 fTime = pStopwatch->Get();
	//	Trace("quick sort2 Time:%f", fTime);
	//	WaitTrace();
	//}
	//if( false )
	{
		sString.clear();
		for( u32 i=0; i < Count; i++ )
		{
			SortInfo Sort;
			Sort.iKey = TEST[i];
			sString.push_back(Sort);
		}
		pStopwatch->Reset();
		std::sort(&sString[0], &sString[sString.size()-1], std::less<SortInfo>());
		f64 fTime = pStopwatch->Get();
		Trace("quick sort3 Time:%f", fTime);
		WaitTrace();
	}

	//if( false )
	{
		typedef LinkList<SortInfo>	MyLinkList;

		MyLinkList* List = (MyLinkList*)Memory::Allocate(sizeof(MyLinkList) * Count);
		for( u32 i = 0; i < Count; i++ )
		{
			List[i].Value.iKey = TEST[i];
			List[i].pNext = (i < Count-1)? &List[i+1]: null;
			//List[i].pPrev = (i > 0)? &List[i-1]: null;
		}
		
		MyLinkList* Sort = List;
		WaitTrace();

		pStopwatch->Reset();
		//insert_sort(&Sort, Count, utility::compare<SortInfo>);
		marge_sort(&Sort, lesseq);
		f64 fTime = pStopwatch->Get();

		Trace("marge sort1 Time:%f", fTime);

		WaitTrace();
		//u32 i = 0;
		//while( Sort )
		//{
		//	Trace("%d:%d", ++ i, Sort->Value);
		//	Sort = Sort->pNext;
		//}
		Memory::Free(List);
	}

	//for( u32 i=0; i < sString.size(); i++ )
	//{
	//	Trace("%d", sString[i]);
	//}
	Trace("==============================");
	SafeRelease(testStopwatch);
#endif	// ~#if KGL_FINAL_RELEASE
}

//======================================================================
//	END OF FILE
//======================================================================

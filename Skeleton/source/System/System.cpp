//----------------------------------------------------------------------//!
//!	@file	main.cpp
//!	@brief	メイン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "File/kgl.File.h"
#include "Scene/SceneRhythmGame.h"
#include "Scene/SceneRhythmGameEdit.h"
#include "Scene/SceneScenario.h"

#if USE_STEAM
#pragma warning(disable:4819)
#include <steam_api.h>
#endif // USE_STEAM

namespace App
{
	namespace System
	{
		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CSystem::CSystem(void)
		{}
		//----------------------------------------------------------------------
		// デストラクタ
		//----------------------------------------------------------------------
		CSystem::~CSystem(void)
		{}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CSystem::Initialize(void)
		{
			//	ゲーム情報の生成
			TKGLPtr<CGameInfo> pGameInfo = new CGameInfo;
			if( !pGameInfo->Initialize() )
			{
				return false;
			}
			KGLGameManager()->SetGameInfo(pGameInfo);

			TKGLPtr<Scene::ISceneManager> pManager = KGLSceneManager();

			struct SceneInfo
			{
				s32					iSceneId;
				Scene::FSceneCreate	Func;
				c8*					pName;
			};
			//	シーンの設定
			SceneInfo SceneList[] =
			{
				{ ESceneMode::RhithmGame, Scene::CreateScene<CSceneRhythmGame>, "RhithmGame" },
				{ ESceneMode::RhithmGameEdit, Scene::CreateScene<CSceneRhythmGameEdit>, "RhithmGameEdit" },
				{ ESceneMode::Scenario, Scene::CreateScene<CSceneScenario>, "Scenario" },

#if !KGL_FINAL_RELEASE
				//	デバッグシーン
				{ ESceneMode::Test, Scene::CreateScene<CSceneTest>, "Test" },
				{ ESceneMode::MovieTest, Scene::CreateScene<CSceneMovieTest>, "MovieTest" },
				{ ESceneMode::VoiceTest, Scene::CreateScene<CSceneVoiceTest>, "VoiceTest" },
				{ ESceneMode::GuiTest, Scene::CreateScene<CSceneGuiTest>, "GuiTest" },
				{ ESceneMode::InputTest, Scene::CreateScene<CSceneInputTest>, "InputTest" },
				{ ESceneMode::DebugTitle, Scene::CreateScene<CSceneDebugTitle>, "DebugTitle" },
#endif	// ~#if !KGL_FINAL_RELEASE
			};

			//	初期シーンの設定
			s32 iStartScene = ESceneMode::Scenario;
#if !KGL_FINAL_RELEASE
			iStartScene = ESceneMode::DebugTitle;
#endif	// ~#if !KGL_FINAL_RELEASE

			for( auto Info: SceneList )
			{
				pManager->SetScene(Info.iSceneId, Info.Func, Info.pName);
			}
			KGLSceneManager()->JumpScene(iStartScene);

			return true;
		}

		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CSystem::Finalize(void)
		{
		}

		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CSystem::Update(f32 fElapsedTime)
		{
#if USE_STEAM
			SteamAPI_RunCallbacks();
#endif // USE_STEAM

			KGLSceneManager()->Update(fElapsedTime);
		}

		//----------------------------------------------------------------------
		//	描画
		//----------------------------------------------------------------------
		void CSystem::Render(Render::IRenderingSystem* pRenderingSystem)
		{
			KGLSceneManager()->Render(pRenderingSystem);
		}

#if	APP_DEBUG
		//----------------------------------------------------------------------
		//	デバッグ描画
		//----------------------------------------------------------------------
		void CSystem::DebugRender(Render::IRenderingSystem* pRenderingSystem)
		{
			ISystem::DebugRender(pRenderingSystem);

#if	KGL_DEBUG_FONT
			if( false )
			{
				TKGLPtr<Font::IFont> pFont;
				pFont = pRenderingSystem->DebugFont();
				if( pFont )
				{
					Vector2 vPos(0, Config::Resolution.y);
					Vector2 vScale(1.5f, 1.5f);
					vScale *= 0.75f;

					//	キーの状態表示
					pFont->Draw(pRenderingSystem,
								CharSet::Format("Z:%d X:%d C:%d V%d\nA:%d S:%d Q:%d W:%d\nSPACE:%d ENTER:%d\nUP:%d DOWN:%d LEFT:%d RIGHT:%d",
								AppInput()->IsButtonPress(EKeyCode::CROSS),
								AppInput()->IsButtonPress(EKeyCode::CIRCLE),
								AppInput()->IsButtonPress(EKeyCode::SQUARE),
								AppInput()->IsButtonPress(EKeyCode::TRIANGLE),
								AppInput()->IsButtonPress(EKeyCode::L1),
								AppInput()->IsButtonPress(EKeyCode::R1),
								AppInput()->IsButtonPress(EKeyCode::L2),
								AppInput()->IsButtonPress(EKeyCode::R2),
								AppInput()->IsButtonPress(EKeyCode::SELECT),
								AppInput()->IsButtonPress(EKeyCode::START),
								AppInput()->IsButtonPress(EKeyCode::UP),
								AppInput()->IsButtonPress(EKeyCode::DOWN),
								AppInput()->IsButtonPress(EKeyCode::LEFT),
								AppInput()->IsButtonPress(EKeyCode::RIGHT)),
								vPos,
								vScale,
								FColorPink,
								Font::EAlign::LeftBottom);
				}
			}
#endif	// ~#if	KGL_DEBUG_FONT
		}
#endif	// ~#if	APP_DEBUG
	}
}


//======================================================================
//	END OF FILE
//======================================================================
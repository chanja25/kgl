//----------------------------------------------------------------------
//!
//!	@file	GameInfo.h
//!	@brief	ゲーム情報管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Game/Scenario/KNScript.h"

namespace App
{
	namespace utility
	{
		//!	比較
		b8 Compare(const string& str, const c8* pStr)
		{
			return str == pStr;
		}
	}

	//	変数タイプ
	enum EValueType
	{
		EValueUnknown	= -1,
		EValueString	= 0,
		EValueInt,
	};

	//	処理内容
	enum EFunc
	{
		EFuncKNScript = 0,	//!< スクリプトルート
		EFuncChapter,		//!< 章
		EFuncName,			//!< 名前
		EFuncMsg,			//!< メッセージ
		EFuncEOL,			//!< 改行
		EFuncClick,			//!< クリック待ち
		EFuncRuby,			//!< ルビを振る
		EFuncFont,			//!< フォント設定
		EFuncWait,			//!< 待ち
		EFuncSe,			//!< SE
		EFuncBgm,			//!< BGM
		EFuncVoice,			//!< Voice

		EFuncLabel,			//!< ラベル設定
		EFuncJump,			//!< スクリプト遷移
		EFuncCall,			//!< スクリプト呼び出し
		EFuncReturn,		//!< スクリプトから戻る

		EFuncSelect,		//!< 選択肢表示
		EFuncPattern,		//!< 選択肢項目

		EFuncDisplay,		//!< 制御画面の指定
		EFuncLayer,			//!< 制御レイヤーの変更
		EFuncBG,			//!< 背景の変更
		EFuncLoadSprite,	//!< 画像の読み込み
		EFuncClearSprite,	//!< 画像の破棄
		EFuncVisibleSprite,	//!< 画像の表示
		EFuncSetSprite,		//!< 画像の設定変更
		EFuncMoveSprite,	//!< 画像の移動
		EFuncSetWindow,		//!< ウィンドウの設定
		EFuncVisibleWindow,	//!< ウィンドウの表示

		EFuncSet,			//!< 変数の設定
		EFuncAdd,			//!< 加算
		EFuncSub,			//!< 減算
		EFuncMul,			//!< 乗算
		EFuncDiv,			//!< 除算
		EFuncInc,			//!< インクリメント
		EFuncDec,			//!< デクリメント
		EFuncValue,			//!< 変数の表示
		EFuncIf,			//!< if
		EFuncElse,			//!< else

		EFuncMacro,			//!< マクロ

		EFuncUnknown,

		EFuncMax,
	};

	//!	関数
	struct
	{
		const c8*		pTag;		//!< タグ名
		BITFIELD		bNext: 1;	//!< 同フレームに次の処理を行うか？
	} Function[] =
	{
		{ "KNScript",	true	},	// EFuncKNScript		スクリプトルート
		{ "chapter",	true	},	// EFuncChapter			章
		{ "name",		true	},	// EFuncName			名前
		{ "msg",		false	},	// EFuncMsg				メッセージ
		{ "br",			false	},	// EFuncEOL				改行
		{ "click",		false	},	// EFuncClick			クリック待ち
		{ "ruby",		true	},	// EFuncRuby			ルビを振る
		{ "font",		true	},	// EFuncFont			フォント設定
		{ "wait",		false	},	// EFuncWait			待ち
		{ "se",			true	},	// EFuncSe,				SE
		{ "bgm",		true	},	// EFuncBgm				BGM
		{ "voice",		true	},	// EFuncVoice			Voice
		{ "label",		true	},	// EFuncLabel			ラベル設定
		{ "jump",		true	},	// EFuncJump			スクリプト遷移
		{ "call",		true	},	// EFuncCall			スクリプト呼び出し
		{ "return",		true	},	// EFuncReturn			スクリプトから戻る
		{ "select",		false	},	// EFuncSelect			選択肢表示
		{ "pattern",	true	},	// EFuncPattern			選択肢項目

		{ "display",	true	},	// EFuncDisplay			制御画面の指定
		{ "layer",		true	},	// EFuncLayer			制御レイヤーの変更
		{ "bg",			true	},	// EFuncBG				背景の変更
		{ "lsp",		true	},	// EFuncLoadSprite		画像の読み込み
		{ "csp",		true	},	// EFuncClearSprite		画像の破棄
		{ "vsp",		true	},	// EFuncVisibleSprite	画像の表示
		{ "setsp",		true	},	// EFuncSetSprite		画像の設定変更
		{ "msp",		true	},	// EFuncMoveSprite		画像の移動
		{ "setwindow",	true	},	// EFuncSetWindow		ウィンドウの設定
		{ "vwindow",	true	},	// EFuncVisibleWindow	ウィンドウの表示

		{ "set",		true	},	// EFuncSet				変数の設定
		{ "add",		true	},	// EFuncAdd				加算
		{ "sub",		true	},	// EFuncSub				減算
		{ "mul",		true	},	// EFuncMul				乗算
		{ "div",		true	},	// EFuncDiv				除算
		{ "inc",		true	},	// EFuncInc				インクリメント
		{ "dec",		true	},	// EFuncDec				デクリメント
		{ "value",		true	},	// EFuncValue			変数の表示
		{ "if",			true	},	// EFuncIf				if
		{ "else",		true	},	// EFuncElse			else
		{ "macro",		true	},	// EFuncMacro			マクロ

		{ "unknown",	true	},	// EFuncUnknown			Unknown
	};
	enum EStep
	{
		EStepUpdate = 0,
		EStepMessage,
	};

	const c8* MacroPath = "macro.txt";
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CKNScript::CKNScript(void)
		: m_iStack(-1)
		, m_iValues(0)
		, m_fWait(0)
		, m_fCharWait(0)
		, m_uiLength(0)
	{
		kgZeroMemory(&m_DrawMessage, sizeof(Message));
	}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CKNScript::~CKNScript(void)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CKNScript::Initialize(KNSaveData* pData)
	{
		m_iStack = -1;
		m_iValues = 0;
		m_fWait = 0;
		m_fCharWait = 0;
		m_uiLength = 0;
		kgZeroMemory(&m_DrawMessage, sizeof(Message));

		m_pMacro = GetLoadedXml(MacroPath);
		if( m_pMacro )
		{
			auto& Children = m_pMacro->GetRoot()->GetChildren();
			//	マクロの全ての末尾にreturnタグを追加しておく
			for( u32 i=0; i < Children.size(); i++ )
			{
				Children[i]->AddChild("return");
			}
		}

		TKGLPtr<Xml::IXml> pXml;
		b8 bRet;
		if( pData == null )
		{
			KNConfig& Config = m_Config;
			kgZeroMemory(&Config, sizeof(KNConfig));
			//	初期設定
			Config.iCharaWait	= 150;

			bRet = Call("start.txt", null, true) != null;
		}
		else
		{
			//	セーブデータから状態の復元
			kgMemcpy(&m_Config, &pData->Config, sizeof(m_Config));

			bRet = Call(pData->cXmlPath, pData->cLabel, true) != null;
		}

		Call("init.txt", null, true);
		return bRet;
	}

	//----------------------------------------------------------------------
	//	ロード
	//----------------------------------------------------------------------
	TKGLPtr<Xml::IXml> CKNScript::GetLoadedXml(const c8* pPath)
	{
		c8 cPath[256];
		CharSet::SFormat(cPath, "Data/Scenario/%s", pPath);
		TKGLPtr<Xml::IXml> pXml = KGLResourceManager()->GetXml(cPath);
		if( pXml )
		{
			SetupXML(pXml);
			return pXml;
		}

		return null;
	}
	//----------------------------------------------------------------------
	//	シナリオ情報のセットアップ
	//----------------------------------------------------------------------
	void CKNScript::SetupXML(Xml::IXml* pXml)
	{
		auto pRoot = pXml->GetRoot();
		if( pRoot )
		{
			SetupNode(pRoot);
		}
	}
	//----------------------------------------------------------------------
	//	シナリオ情報のセットアップ
	//----------------------------------------------------------------------
	void CKNScript::SetupNode(Xml::CNode* pNode)
	{
		u32 uiType = GetNodeType(pNode);
		switch( uiType )
		{
		case EFuncMsg:
			if( pNode->GetChildren().empty() )
			{
				vector<string> Contents;
				Split(Contents, pNode->GetContent().c_str(), EOL);
				if( Contents.size() > 1 )
				{
					pNode->SetContent("");
					for( u32 i=0; i < Contents.size() - 1; i++ )
					{
						pNode->AddChild("")->SetContent(Contents[i]);
						pNode->AddChild("br");
					}
					pNode->AddChild("")->SetContent(Contents.back());
				}
			}
			else
			{
				pNode->SetContent(Trim(pNode->GetContent(), EOL));
			}
			break;

		default:
			if( pNode->IsContent() &&
				pNode->GetContent().contains(EOL) )
			{
				pNode->SetContent(Trim(pNode->GetContent(), EOL));
			}
			break;
		}

		auto pChildren = pNode->GetChildren();
		for( u32 i=0; i < pChildren.size(); i++ )
		{
			SetupNode(pChildren[i]);
		}

		if( pNode->GetNext() )
		{
			SetupNode(pNode->GetNext());
		}
	}
	//----------------------------------------------------------------------
	//	スクリプト遷移
	//----------------------------------------------------------------------
	Xml::CNode* CKNScript::Jump(const c8* pPath, const c8* pLabel, b8 bSet)
	{
		TKGLPtr<Xml::IXml> pXml = GetLoadedXml(pPath);
		if( pXml != null )
		{
			Xml::CNode* pRoot = pXml->GetRoot();
			Xml::CNode* pCurrent = null;
			// ラベルの検索
			if( utility::Compare(pRoot->GetTag(), "KNScript") )
			{
				Xml::CNode* pNode = pRoot->GetFirstChild();
				while( pLabel && pNode )
				{
					if( utility::Compare(pNode->GetTag(), "label") &&
						utility::Compare(pNode->FindAttribute("name"), pLabel) )
					{
						break;
					}
					pNode = pNode->GetNext();
				}
				pCurrent = pNode;
			}
			if( pCurrent )
			{
				m_Stack[m_iStack].strPath	= pPath;
				m_Stack[m_iStack].pXml		= pXml;
				m_Stack[m_iStack].pCurrent	= bSet? pCurrent: null;
				m_Stack[m_iStack].bInit		= true;
			}
			return pCurrent;
		}
		return null;
	}
	//----------------------------------------------------------------------
	//	スクリプト呼び出す
	//----------------------------------------------------------------------
	Xml::CNode* CKNScript::Call(const c8* pPath, const c8* pLabel, b8 bSet)
	{
		if( m_iStack < MaxStack )
		{
			m_iStack ++;
			Xml::CNode* pNode = Jump(pPath, pLabel, bSet);
			if( pNode == null )
			{
				m_iStack --;
			}
			return pNode;
		}
		return null;
	}
	//----------------------------------------------------------------------
	//	マクロを呼び出す
	//----------------------------------------------------------------------
	Xml::CNode* CKNScript::CallMacro(const string& strMacro)
	{
		if( m_pMacro != null )
		{
			Xml::CNode* pRoot = m_pMacro->GetRoot();
			Xml::CNode* pCurrent = null;
			//	マクロの検索
			if( utility::Compare(pRoot->GetTag(), "macro") )
			{
				pCurrent = pRoot->FindChildNode(strMacro);
			}
			if( pCurrent )
			{
				pCurrent = pCurrent->GetFirstChild();

				m_iStack ++;
				m_Stack[m_iStack].strPath	= MacroPath;
				m_Stack[m_iStack].pXml		= m_pMacro;
				m_Stack[m_iStack].pCurrent	= null;
				m_Stack[m_iStack].bInit		= true;
			}
			return pCurrent;
		}
		return null;
	}
	//----------------------------------------------------------------------
	//	呼び出したスクリプトから戻る
	//----------------------------------------------------------------------
	Xml::CNode* CKNScript::Return(void)
	{
		m_Stack[m_iStack].strPath	= "";
		m_Stack[m_iStack].pXml		= null;
		m_Stack[m_iStack].pCurrent	= null;
		m_Stack[m_iStack].bInit		= false;

		if( m_iStack > 0 )
		{
			return m_Stack[--m_iStack].pCurrent->GetNext();
		}
		return null;
	}
	//----------------------------------------------------------------------
	//	呼び出したスクリプトから戻る
	//----------------------------------------------------------------------
	KNValue* CKNScript::FindValue(const string& strName)
	{
		for( s32 i=0; i <= m_iValues; i++ )
		{
			if( utility::Compare(m_Values[i].cName, strName.c_str()) )
			{
				return &m_Values[i];
			}
		}
		return null;
	}
	//----------------------------------------------------------------------
	//	変数の設定(整数値)
	//----------------------------------------------------------------------
	KNValue* CKNScript::SetValue(const string& strName, s32 iValue)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null )
		{
			pValue = &m_Values[m_iValues ++];
		}
		kgStrcpy(pValue->cName, strName.c_str());
		pValue->iType	= EValueInt;
		pValue->iValue	= iValue;
		return pValue;
	}
	//----------------------------------------------------------------------
	//	変数の設定(文字列)
	//----------------------------------------------------------------------
	KNValue* CKNScript::SetValue(const string& strName, const string& strValue)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null )
		{
			pValue = &m_Values[m_iValues ++];
		}
		pValue->iType	= EValueString;
		kgStrcpy(pValue->cText, strValue.c_str());
		return pValue;
	}
	//----------------------------------------------------------------------
	//	変数の加算
	//----------------------------------------------------------------------
	void CKNScript::AddValue(const string& strName, s32 iValue)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null || pValue->iType != EValueInt )
		{
			pValue = SetValue(strName, 0);
		}
		pValue->iValue += iValue;
	}
	//----------------------------------------------------------------------
	//	変数の減算
	//----------------------------------------------------------------------
	void CKNScript::SubValue(const string& strName, s32 iValue)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null || pValue->iType != EValueInt )
		{
			pValue = SetValue(strName, 0);
		}
		pValue->iValue -= iValue;
	}
	//----------------------------------------------------------------------
	//	変数の掛け算
	//----------------------------------------------------------------------
	void CKNScript::MulValue(const string& strName, s32 iValue)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null || pValue->iType != EValueInt )
		{
			pValue = SetValue(strName, 0);
		}
		pValue->iValue *= iValue;
	}
	//----------------------------------------------------------------------
	//	変数の割り算
	//----------------------------------------------------------------------
	void CKNScript::DivValue(const string& strName, s32 iValue)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null || pValue->iType != EValueInt )
		{
			pValue = SetValue(strName, 0);
		}
		pValue->iValue /= iValue;
	}
	//----------------------------------------------------------------------
	//	変数のインクリメント
	//----------------------------------------------------------------------
	void CKNScript::IncValue(const string& strName)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null || pValue->iType != EValueInt )
		{
			pValue = SetValue(strName, 0);
		}
		++ pValue->iValue;
	}
	//----------------------------------------------------------------------
	//	変数のデクリメント
	//----------------------------------------------------------------------
	void CKNScript::DecValue(const string& strName)
	{
		KNValue* pValue = FindValue(strName);
		if( pValue == null || pValue->iType != EValueInt )
		{
			pValue = SetValue(strName, 0);
		}
		-- pValue->iValue;
	}

	//----------------------------------------------------------------------
	//	if処理
	//----------------------------------------------------------------------
	b8 CKNScript::CheckIf(Xml::CNode* pNode)
	{
		//	オペレーターが無い場合はフラグとして扱う
		KNValue* pValue = FindValue(pNode->FindAttribute("name"));
		if( pValue == null )
		{
			const string& strOperator = pNode->FindAttribute("operator");
			//	変数が定義されていないか？
			if( strOperator == "not exist" )	return true;

			return false;
		}
		if( pNode->HasAttribute("operator") )
		{
			const string& strOperator = pNode->FindAttribute("operator");
			//	変数が定義されているか？
			if( strOperator == "exist" )	return true;

			if( pValue->iType == EValueInt )
			{
				s32	iValue = atoi(pNode->FindAttribute("value").c_str());
				//	値の比較
				if( strOperator == "=" )		return pValue->iValue == iValue;
				if( strOperator == "!=" )		return pValue->iValue != iValue;
				if( strOperator == "<" )		return pValue->iValue < iValue;
				if( strOperator == ">" )		return pValue->iValue > iValue;
				if( strOperator == "<=" )		return pValue->iValue <= iValue;
				if( strOperator == ">=" )		return pValue->iValue >= iValue;
			}
			return false;
		}
		return pValue->iType != 0;
	}
	//----------------------------------------------------------------------
	//	ノードタイプを取得
	//----------------------------------------------------------------------
	u32 CKNScript::GetNodeType(Xml::CNode* pNode)
	{
		auto& strTag = pNode->GetTag();
		if( strTag.length() )
		{
			for( u32 i = 0; i < ArrayCount(Function); i++ )
			{
				if( utility::Compare(strTag, Function[i].pTag) )
				{
					return i;
				}
			}
		}
		return EFuncUnknown;
	}

	//----------------------------------------------------------------------
	//	スクリプトの更新
	//----------------------------------------------------------------------
	void CKNScript::UpdateScript(f32 fElapsedTime)
	{
		if( m_iStack < 0 )
		{
			return;
		}
		Xml::CNode* pCurrent = m_Stack[m_iStack].pCurrent;
		while( pCurrent )
		{
			u32 uiFunc = GetNodeType(pCurrent);

			auto pNext = UpdateScript(fElapsedTime, uiFunc, pCurrent);

			m_Stack[m_iStack].bInit = m_Stack[m_iStack].pCurrent != pNext;
			pCurrent = pNext;
			if( !Function[uiFunc].bNext )
			{
				break;
			}
			if( pCurrent == null )
			{
				pCurrent = Return();
			}
			m_Stack[m_iStack].pCurrent = pCurrent;
		}
		m_Stack[m_iStack].pCurrent = pCurrent;
	}
	//----------------------------------------------------------------------
	//	スクリプトの更新
	//----------------------------------------------------------------------
	Xml::CNode* CKNScript::UpdateScript(f32 fElapsedTime, u32 uiFunc, Xml::CNode* pNode)
	{
		//	初回更新か？
		b8 bIsFirst = !!m_Stack[m_iStack].bInit;

		Xml::CNode* pNext = pNode->GetNext();
		switch( uiFunc )
		{
		case EFuncKNScript:
			pNext = pNode->GetFirstChild();
			break;
		case EFuncChapter:
			m_strChapter = pNode->GetContent();	
			break;
		case EFuncName:
			m_strName = pNode->GetContent();
			break;

		case EFuncMsg:
			if( bIsFirst )
			{
				m_fCharWait = 0;
				m_uiLength = 0;
				m_DrawMessage.pXml		= GetCurrentStack()->pXml;
				m_DrawMessage.pMessage	= pNode;
				m_DrawMessage.pCurrent	= pNode->GetFirstChild();
			}
			if( UpdateMessage(fElapsedTime) )
			{
				m_DrawMessage.pXml		= null;
				m_DrawMessage.pMessage	= null;
				m_DrawMessage.pCurrent	= null;
			}
			else
			{
				return pNode;
			}
			break;
		case EFuncClick:
			if( AppGameInfo()->Input()->IsKeyPush(Input::EKey::KEY_RETURN) )
			{
				u32 uiLength = GetMessageLength(m_DrawMessage);
				if( m_uiLength < uiLength )
				{
					m_uiLength = uiLength;
				}
				else
				{
					if( pNode->HasAttribute("clear") )
					{
						m_fCharWait = 0;
						m_uiLength = 0;
					}
					return pNext;
				}
			}
			return pNode;
		case EFuncRuby:
			break;
		case EFuncFont:
			break;

		case EFuncWait:
			if( bIsFirst )
			{
				f32 fTime = 10.0f;
				if( pNode->HasAttribute("time") )
				{
					fTime = (f32)atof(pNode->FindAttribute("time").c_str());
				}
				m_fWait = fTime;
			}
			m_fWait -= fElapsedTime;
			return m_fWait <= 0? null: pNode;

		case EFuncSe:
			break;
		case EFuncBgm:
			break;
		case EFuncVoice:
			break;

		case EFuncLabel:
			if( pNode->HasAttribute("name") )
			{
				m_strPath	= GetCurrentStack()->strPath;
				m_strLabel	= pNode->FindAttribute("name");
			}
			break;
		case EFuncJump:
			{
				const c8* pPath = GetCurrentStack()->strPath.c_str();
				if( pNode->HasAttribute("path") )
				{
					pPath = pNode->FindAttribute("path").c_str();
				}
				return Jump(pPath, pNode->FindAttribute("label").c_str());
			}
			break;
		case EFuncCall:
			{
				const c8* pPath = GetCurrentStack()->strPath.c_str();
				if( pNode->HasAttribute("path") )
				{
					pPath = pNode->FindAttribute("path").c_str();
				}
				return Call(pPath, pNode->FindAttribute("label").c_str());
			}
			break;
		case EFuncReturn:
			return Return();

		case EFuncSelect:
			break;
		case EFuncPattern:
			break;

		case EFuncDisplay:
			break;
		case EFuncLayer:
			break;
		case EFuncBG:
			break;
		case EFuncLoadSprite:
			break;
		case EFuncClearSprite:
			break;
		case EFuncVisibleSprite:
			break;
		case EFuncSetSprite:
			break;
		case EFuncMoveSprite:
			break;
		case EFuncSetWindow:
			break;
		case EFuncVisibleWindow:
			break;

		case EFuncSet:
			if( pNode->HasAttribute("string") )
			{
				SetValue(pNode->FindAttribute("name"), pNode->FindAttribute("string"));
			}
			else
			{
				SetValue(pNode->FindAttribute("name"), atoi(pNode->FindAttribute("value").c_str()));
			}
			break;
		case EFuncAdd:	AddValue(pNode->FindAttribute("name"), atoi(pNode->FindAttribute("value").c_str()));	break;
		case EFuncSub:	SubValue(pNode->FindAttribute("name"), atoi(pNode->FindAttribute("value").c_str()));	break;
		case EFuncMul:	MulValue(pNode->FindAttribute("name"), atoi(pNode->FindAttribute("value").c_str()));	break;
		case EFuncDiv:	DivValue(pNode->FindAttribute("name"), atoi(pNode->FindAttribute("value").c_str()));	break;
		case EFuncInc:	IncValue(pNode->FindAttribute("name"));	break;
		case EFuncDec:	DecValue(pNode->FindAttribute("name"));	break;

		case EFuncValue:
			break;

		case EFuncIf:
			if( CheckIf(pNode) )
			{
				pNext = pNode->GetFirstChild();
			}
			else if( pNext != null && utility::Compare(pNext->GetTag(), "else") )
			{
				pNext = pNext->GetFirstChild();
			}
			break;
		case EFuncElse:
			//	elseはif内で偽が判定された場合にのみ実行される
			break;

		case EFuncMacro:
			return CallMacro(pNode->FindAttribute("name"));
		}

		if( pNext == null && pNode->GetParent() != null )
		{
			pNext = pNode->GetParent()->GetNext();
		}
		return pNext;
	}
	//----------------------------------------------------------------------
	//	文字列の取得
	//----------------------------------------------------------------------
	const c8* CKNScript::GetMessage(Xml::CNode* pMessage)
	{
		u32 uiFunc = GetNodeType(pMessage);

		m_strMessage = "";
		if( uiFunc == EFuncValue )
		{
			auto pValue = FindValue(pMessage->FindAttribute("name"));
			if( pValue )
			{
				switch( pValue->iType )
				{
				case EValueInt:		m_strMessage = pValue->iValue;	break;
				case EValueString:	m_strMessage = pValue->cText;	break;
				}
			}
			return m_strMessage.c_str();
		}

		return pMessage->GetContent().c_str();
	}
	//----------------------------------------------------------------------
	//	文字数の取得
	//----------------------------------------------------------------------
	Xml::CNode* CKNScript::GetMessageBegin(Message& Message)
	{
		auto pNode = Message.pMessage;
		if( pNode && pNode->GetFirstChild() )
		{
			pNode = pNode->GetFirstChild();
			auto pChild = pNode;
			while( pChild && pChild != Message.pCurrent )
			{
				if( utility::Compare(pChild->GetTag(), "click") &&
					pChild->HasAttribute("clear") )
				{
					pNode = pChild->GetNext();
				}
				pChild = pChild->GetNext();
			}
		}
		return pNode;
	}
	//----------------------------------------------------------------------
	//	文字数の取得
	//----------------------------------------------------------------------
	u32 CKNScript::GetMessageLength(Message& Message)
	{
		auto pNode = Message.pMessage;
		if( pNode && utility::Compare(pNode->GetTag(), "msg") )
		{
			auto pEnd = m_DrawMessage.pMessage->GetNext();
			auto pMessage = GetMessageBegin(Message);
			u32 uiLength = 0;
			while( pMessage && pMessage != pEnd )
			{
				uiLength += kgStrlen(GetMessage(pMessage));
				if( pMessage == Message.pCurrent )
				{
					break;
				}
				pMessage = pMessage->GetNext();
			}
			return uiLength;
		}
		return 0;
	}
	//----------------------------------------------------------------------
	//	スクリプトの更新
	//----------------------------------------------------------------------
	b8 CKNScript::UpdateMessage(f32 fElapsedTime)
	{
		auto pEnd = m_DrawMessage.pMessage->GetNext();
		auto pMessage = m_DrawMessage.pCurrent;
		while( pMessage )
		{
			u32 uiFunc = GetNodeType(pMessage);
			pMessage = UpdateScript(fElapsedTime, uiFunc, pMessage);
			if( !Function[uiFunc].bNext )
			{
				break;
			}
			if( pMessage == pEnd )
			{
				pMessage = null;
			}
		}
		m_DrawMessage.pCurrent = pMessage;

		u32 uiLength = GetMessageLength(m_DrawMessage);
		if( uiLength > m_uiLength )
		{
			m_fCharWait += fElapsedTime;
			if( m_fCharWait >= (f32)m_Config.iCharaWait * 0.001f )
			{
				m_uiLength ++;
				m_fCharWait = 0;
			}
		}
		if( pMessage == null )
		{
			auto pNode = m_DrawMessage.pMessage;
			if( UpdateScript(fElapsedTime, EFuncClick, pNode) != pNode )
			{
				return true;
			}
		}
		return false;
	}
	//----------------------------------------------------------------------
	//	メッセージ描画
	//----------------------------------------------------------------------
	void CKNScript::DrawMessage(Render::IRenderingSystem* pRenderingSystem, Font::IFont* pFont)
	{
		if( !m_DrawMessage.pMessage )
		{
			return;
		}

		TKGLPtr<Render::IDrawer> pDrawer = pRenderingSystem->Drawer();
		pFont = pFont? pFont: AppGameInfo()->Font();
		if(!pFont)
		{
			return;
		}

		f32 fBaseX = 120;
		f32 fBaseHeight = pFont->GetDrawTextSize(" ").y;
		f32 fHeight = fBaseHeight;
		Vector2 vPos(fBaseX, 120);

		u32 uiLength = m_uiLength;
		auto pEnd = m_DrawMessage.pMessage->GetNext();
		auto pMessage = GetMessageBegin(m_DrawMessage);
		while( pMessage && pMessage != pEnd )
		{
			string strContent = GetMessage(pMessage);
			u32 uiDrawLen = strContent.length();
			FColor Color = FColorWhite;

			u32 uiFunc = GetNodeType(pMessage);
			if( strContent.length() )
			{
				Vector2 vSize(1.0f, 1.0f);
				// todo:メッセージ描画
				switch( uiFunc )
				{
				case EFuncFont:
					if( pMessage->HasAttribute("color") )
					{
#define	SetColor(tag, color)	if( utility::Compare(pMessage->FindAttribute("color"), tag) ) { Color = color; } else
#define	ElseColor(color)		{ Color = color; }

						SetColor("black",	FColorBlack)
						SetColor("white",	FColorWhite)
						SetColor("gray",	FColorGray)
						SetColor("red",		FColorRed)
						SetColor("green",	FColorGreen)
						SetColor("blue",	FColorBlue)
						SetColor("yellow",	FColorYellow)
						SetColor("purple",	FColorPurple)
						SetColor("orange",	FColorOrange)
						SetColor("pink",	FColorPink)
						//	指定された値が存在しない場合
						ElseColor(FColorWhite)
					}
					break;
				}
				if( uiDrawLen > 0 && uiLength > 0 )
				{
					uiDrawLen = Min(uiDrawLen, uiLength);
					uiLength -= uiDrawLen;

					string strMessage = strContent.left(uiDrawLen);

					pFont->Draw(pRenderingSystem, strMessage.c_str(), vPos, vSize, Color);

					Vector2 vFontSize = pFont->GetDrawTextSize(strMessage.c_str(), vSize);
					vPos.x += vFontSize.x;
					fHeight = Max(fHeight, vFontSize.y);
				}
			}
			if( uiFunc == EFuncEOL )
			{
				vPos.x = fBaseX;
				vPos.y += fHeight;
				fHeight = fBaseHeight;
			}

			if( pMessage == m_DrawMessage.pCurrent )
			{
				break;
			}
			pMessage = pMessage->GetNext();
		}
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CKNScript::Update(f32 fElapsedTime)
	{
		UpdateScript(fElapsedTime);
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CKNScript::Draw(Render::IRenderingSystem* pRenderingSystem)
	{
		DrawMessage(pRenderingSystem);
	}
}

//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	RhythmGame.cpp
//!	@brief	リズムゲーム関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Game/RhythmGame/RhythmGame.h"
#include "Utility/Path/kgl.Path.h"

namespace App
{
	namespace RhythmGame
	{
		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CMusic::CMusic(void)
			: m_bPlay(false)
			, m_bPause(false)
			, m_bFinish(false)
			, m_fDelay(0.0f)
			, m_fCounter(-5.0f)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CMusic::~CMusic(void)
		{
			Finalize();
		}
		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CMusic::Initialize(const MusicData* pInfo)
		{
			Finalize();

			TKGLPtr<File::IFile> pFile;
			if (!KGLFileReader()->GetFile(pFile.GetReference(), CharSet::Format("Data/Music/%s", pInfo->cPath)))
			{
				return false;
			}

			TKGLPtr<IDecoder> pDecoder;
			if (kgStricmp(Path::GetExtension(pInfo->cPath), "wav") == 0)
			{
				KGLAudioManager()->SoundDriver()->DecoderManager()->CreateDecoderFromWave(pDecoder.GetReference(), pFile, ECategory::BGM);
			}
			else if (kgStricmp(Path::GetExtension(pInfo->cPath), "ogg") == 0)
			{
				KGLAudioManager()->SoundDriver()->DecoderManager()->CreateDecoderFromOgg(pDecoder.GetReference(), pFile, ECategory::BGM);
			}
			if (!pDecoder.IsValid())
			{
				return false;
			}

			if (!KGLAudioManager()->StreamManager()->Create(m_pStream.GetReference(), pDecoder))
			{
				return false;
			}

			m_fDelay = pInfo->fDelay;

			return true;
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CMusic::Finalize(void)
		{
			m_pStream = null;
			m_bPlay = false;
			m_bPause = false;
			m_bFinish = false;
			m_fDelay = 0.0f;
			m_fCounter = -5.0f;
		}
		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CMusic::Update(f32 fElapsedTime)
		{
			if( m_bPlay && !m_bPause )
			{
				//	再生待ち
				if( m_fCounter < m_fDelay )
				{
					m_fCounter += fElapsedTime;
					//	遅延がなくなれば再生を開始する
					if( m_fCounter >= m_fDelay )
					{
						Play(m_fDelay);
					}
				}
				else if( !IsPlaying() )
				{
					m_bFinish = true;
				}
			}
		}

		//----------------------------------------------------------------------
		//	再生
		//----------------------------------------------------------------------
		b8 CMusic::Play(f32 fOffset)
		{
			if (!m_pStream.IsValid())
			{
				return false;
			}

			Stop();

			m_bPlay = true;
			m_bPause = false;
			m_fCounter = fOffset;
			if (fOffset >= m_fDelay)
			{
				m_fCounter = m_fDelay;
				m_pStream->Play(false, fOffset - m_fDelay);
			}

			return true;
		}
		//----------------------------------------------------------------------
		//	停止
		//----------------------------------------------------------------------
		void CMusic::Stop(void)
		{
			if( m_pStream && m_bPlay )
			{
				m_pStream->Stop();
				m_bPlay = false;
				m_bPause = false;
			}
		}
		//----------------------------------------------------------------------
		//	一時停止
		//----------------------------------------------------------------------
		void CMusic::Pause(void)
		{
			if( m_pStream && m_bPlay && !m_bPause )
			{
				m_pStream->Pause();
				m_bPause = true;
			}
		}
		//----------------------------------------------------------------------
		//	一時停止の解除
		//----------------------------------------------------------------------
		void CMusic::Resume(void)
		{
			if (m_pStream.IsValid() && m_bPlay && m_bPause)
			{
				m_pStream->Resume();
				m_bPause = false;
			}
		}

		//----------------------------------------------------------------------
		//	現在の時間
		//----------------------------------------------------------------------
		f32 CMusic::CurrentTime(void)
		{
			if (m_pStream.IsValid())
			{
				return (m_fCounter < m_fDelay)? m_fCounter: m_pStream->CurrentTime() + m_fCounter;
			}
			return 0.0f;
		}
		//----------------------------------------------------------------------
		//	再生中か？
		//----------------------------------------------------------------------
		b8 CMusic::IsPlaying(void)
		{
			if (m_pStream.IsValid())
			{
				return m_pStream->IsPlaying();
			}
			return false;
		}
		//----------------------------------------------------------------------
		//	一時停止中か？
		//----------------------------------------------------------------------
		b8 CMusic::IsPause(void)
		{
			return !IsPlaying();
		}
		//----------------------------------------------------------------------
		//	再生が終了したか？
		//----------------------------------------------------------------------
		b8 CMusic::IsFinish(void)
		{
			return m_bFinish;
		}
		//----------------------------------------------------------------------
		//	再生速度の変更(エディター用)
		//----------------------------------------------------------------------
		void CMusic::SetPlaySpeed(f32 fRate)
		{
			if( m_pStream )
			{
				m_pStream->GetResource()->SetFrequencyRatio(fRate);
			}
		}
		//----------------------------------------------------------------------
		//	遅延の変更
		//----------------------------------------------------------------------
		void CMusic::SetDelay(f32 fDelay)
		{
			if( m_pStream )
			{
				if( m_fCounter == m_fDelay )
				{
					m_fCounter = fDelay;
				}
				m_fDelay = fDelay;
			}
		}

		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CRhythmEvent::CRhythmEvent(void)
			: m_pHeader(null)
			, m_pRhythmInfo(null)
			, m_pBPMInfo(null)
			, m_pBGInfo(null)
			, m_fBPM(90.0f)
			, m_fNextBPM(90.0f)
			, m_fCurrentTime(0.0f)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CRhythmEvent::~CRhythmEvent(void)
		{}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CRhythmEvent::Initialize(RhythmGameHeader* pHeader)
		{
			SafeDeleteArray(m_pRhythmInfo);
			SafeDeleteArray(m_pBPMInfo);
			SafeDeleteArray(m_pBGInfo);

			m_pHeader = pHeader;
			m_fCurrentTime = 0.0f;

			RhythmData* pRhythmData = GetRhythmData();
			BPMData* pBPMData = GetBPMData();
			BGData* pBGData = GetBGData();

			//	入力情報の設定
			if( pHeader->iRhythmCount > 0 )
			{
				if( !RGManager()->IsAllInput() )
				{
					m_pRhythmInfo = new RhythmInfo[pHeader->iRhythmCount];
					for( s32 i=0; i < pHeader->iRhythmCount; i++ )
					{
						m_pRhythmInfo[i].fTime = GetTime(pRhythmData[i].iTiming);
						m_pRhythmInfo[i].bEnable = true;
						m_pRhythmInfo[i].fBPM = GetBPM(pRhythmData[i].iTiming);
						m_pRhythmInfo[i].iInput = pRhythmData[i].iInput;
					}
				}
				else
				{
					pHeader->iRhythmCount = 1024;
					m_pRhythmInfo = new RhythmInfo[pHeader->iRhythmCount];
					for( s32 i=0; i < pHeader->iRhythmCount; i++ )
					{
						m_pRhythmInfo[i].fTime = GetTime(i * 4);
						m_pRhythmInfo[i].bEnable = true;
						m_pRhythmInfo[i].fBPM = GetBPM(i * 4);
						m_pRhythmInfo[i].iInput = 'Z' << ((i % 4) * 8);
					}
				}
			}
			//	BPM変更情報の設定
			if( pHeader->iBPMCount > 0 )
			{
				m_pBPMInfo = new BPMInfo[pHeader->iBPMCount];
				for( s32 i=0; i < pHeader->iBPMCount; i++ )
				{
					m_pBPMInfo[i].fTime = GetTime(pBPMData[i].iTiming);
					m_pBPMInfo[i].bEnable = true;
					m_pBPMInfo[i].fBPM = pBPMData[i].fBPM;
				}
			}
			//	BG変更情報の設定
			if( pHeader->iBGCount > 0 )
			{
				m_pBGInfo = new BGInfo[pHeader->iBGCount];
				for( s32 i=0; i < pHeader->iBGCount; i++ )
				{
					m_pBGInfo[i].fTime = GetTime(pBGData[i].iTiming);
					m_pBGInfo[i].bEnable = true;
					m_pBGInfo[i].iBG = pBGData[i].iBG;
				}
			}

			m_fNextBPM = GetMusicData()->fBPM;
			m_fBPM = m_fNextBPM;
			m_iBG = 0;
			return true;
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CRhythmEvent::Finalize(void)
		{
			m_pHeader = null;

			SafeDeleteArray(m_pRhythmInfo);
			SafeDeleteArray(m_pBPMInfo);
			SafeDeleteArray(m_pBGInfo);
		}

		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CRhythmEvent::Update(f32 fElapsedTime, f32 fCurrentTime)
		{
			f32 fPrevTime = m_fCurrentTime;
			m_fCurrentTime = fCurrentTime;

			RhythmGameHeader* pHeader = GetRhythmGameHeader();
			if( pHeader )
			{
				MusicData* pMusic = GetMusicData();

				s32 iIndex = 0;//GetEventIndex(fPrevTime, m_pRhythmInfo, pHeader->iRhythmCount);
				for( s32 i=iIndex; i < pHeader->iRhythmCount; i++ )
				{
					if( !m_pRhythmInfo[i].bEnable )
					{
						continue;
					}
					f32 fMargin = 60.0f / m_pRhythmInfo[i].fBPM / 4 + pMusic->fTimingMargin;
					if( fCurrentTime > m_pRhythmInfo[i].fTime + fMargin )
					{
						m_pRhythmInfo[i].bEnable = false;
						continue;
					}
					if( fCurrentTime < m_pRhythmInfo[i].fTime - fMargin )
					{
						break;
					}
					b8 bSuccess = false;
					for( s32 j=0; j < 4; j++ )
					{
						if( AppInput()->IsKeyPush(GetInputKey(m_pRhythmInfo[i].iInput, j)) )
						{
							bSuccess = true;
							break;
						}
					}
					if( bSuccess )
					{
						m_pRhythmInfo[i].bEnable = false;
						break;
					}
					break;
				}

				s32 iBPMIndex = 0;//GetEventIndex(fPrevTime, m_pBPMInfo, pHeader->iBPMCount);
				for( s32 i=iBPMIndex; i < pHeader->iBPMCount; i++ )
				{
					if( !m_pBPMInfo[i].bEnable )
					{
						continue;
					}
					if( fCurrentTime < m_pBPMInfo[i].fTime )
					{
						break;
					}
					m_pBPMInfo[i].bEnable = false;
					m_fNextBPM = m_pBPMInfo[i].fBPM;
				}

				s32 iBGIndex = 0;//GetEventIndex(fPrevTime, m_pBGInfo, pHeader->iBGCount);
				for( s32 i=iBGIndex; i < pHeader->iBGCount; i++ )
				{
					if( !m_pBGInfo[i].bEnable )
					{
						continue;
					}
					if( fCurrentTime < m_pBGInfo[i].fTime )
					{
						break;
					}
					m_pBGInfo[i].bEnable = false;
					m_iBG = m_pBGInfo[i].iBG;
				}
			}

			//	BPMのフェード処理
			if( m_fBPM != m_fNextBPM )
			{
				const f32 AddRate = 4.0f;
				f32 fAdd = Math::SecondToFrame(fElapsedTime) * AddRate;
				//	変化量が目標値を超えている場合は値を直接設定する
				if( fAdd > Abs(m_fBPM - m_fNextBPM) )
				{
					m_fBPM = m_fNextBPM;
				}
				else
				{
					m_fBPM += (m_fNextBPM - m_fBPM > 0)? fAdd: -fAdd;
				}
			}
		}
		//----------------------------------------------------------------------
		//	描画
		//----------------------------------------------------------------------
		void CRhythmEvent::Render(Render::IRenderingSystem* pRenderingSystem)
		{
			//	1秒間でバーが進む幅
			const f32 Width = 8;

			TKGLPtr<Render::IDrawer> pDrawer = pRenderingSystem->Drawer();
			//	適当な曲線(譜面を流すための曲線)
			const Vector2 vPoint[] =
			{
				Vector2(Config::Resolution.x *  0.0f, Config::Resolution.y * 0.8f),
				Vector2(Config::Resolution.x * 0.25f, Config::Resolution.y * 0.5f),
				Vector2(Config::Resolution.x * 0.75f, Config::Resolution.y * 0.5f),
				Vector2(Config::Resolution.x + 1.0f,  Config::Resolution.y * 0.8f),
			};
			Vector2 vPos, vScale(8.0f, 8.0f);

			//	曲線の入力タイミングにあたる位置
			const f32 JustTiming = 0.8f;

			TKGLPtr<Font::IFont> pFont = AppGameInfo()->Font();
			//	デバッグ用
			if( true )//GetRhythmGameHeader() == null )
			{
				Vector2 vLineScale(4.0f, 4.0f);
				const u32 Count = 256;
				for( u32 i=0; i <= Count; i++ )
				{
					//	現在位置の算出
					f32 fRate = ((f32)i / (f32)Count);

					Math::Vec2Bezier(vPos, ArrayCount(vPoint), vPoint, fRate);

					pDrawer->DrawRect(vPos + Vector2(0.0f,  0.0f) - vLineScale * 0.5f, vLineScale, FColorWhite);
					pDrawer->DrawRect(vPos + Vector2(0.0f, 32.0f) - vLineScale * 0.5f, vLineScale, FColorWhite);
					pDrawer->DrawRect(vPos + Vector2(0.0f, 64.0f) - vLineScale * 0.5f, vLineScale, FColorWhite);
					pDrawer->DrawRect(vPos + Vector2(0.0f, 96.0f) - vLineScale * 0.5f, vLineScale, FColorWhite);
				}

				Math::Vec2Bezier(vPos, ArrayCount(vPoint), vPoint, JustTiming);
				for( u32 i=0; i < 32; i++ )
				{
					f32 fRate = ((f32)i / (f32)32);
					pDrawer->DrawRect(vPos + Vector2(0.0f, -64.0f) - vLineScale * 0.5f + Vector2(0.0f, 224.0f) * fRate, vLineScale, FColorOrange);
				}
			}

			s32 iRhythmCount = GetRhythmGameHeader()? GetRhythmGameHeader()->iRhythmCount: 0;
			//	現在より少し巻き戻した位置のタイミングを取得する
			s32 iIndex = 0;//GetEventIndex(m_fCurrentTime - fWidthPerSec * 2.0f, m_pRhythmInfo, iRhythmCount);
			for( s32 i=iIndex; i < iRhythmCount; i++ )
			{
				RhythmInfo* pInfo = &m_pRhythmInfo[i];
				if( !pInfo->bEnable )
				{
					continue;
				}
				//	1秒間でバーが進む速度
				const f32 fWidthPerSec = 60.0f / pInfo->fBPM;

				f32 fTime = pInfo->fTime - m_fCurrentTime;
				//	現在位置の算出
				f32 fRate = 1.0f - (fTime / (fWidthPerSec * Width));
				//	まだ表示する必要ない場合はスキップする
				if( fTime < -5.0f || fWidthPerSec * (Width+1) < fTime )
				{
					continue;
				}
				Math::Vec2Bezier(vPos, ArrayCount(vPoint), vPoint, JustTiming * fRate);

				for( s32 j=0; j < 4; j++ )
				{
					if( GetCharCode(pInfo->iInput, j) )
					{
						Vector2 vDrawPos = vPos + Vector2(0.0f,  32.0f * (f32)j);

						pDrawer->DrawRect(vDrawPos - vScale * 0.5f, vScale, FColorOrange);
						pFont->Draw(pRenderingSystem,
									!RGManager()->IsAllInput()?
										CharSet::Format("[%c]", GetCharCode(pInfo->iInput, j)):
										CharSet::Format("[%d:%c]", i * 4, GetCharCode(pInfo->iInput, j)),
									vDrawPos + Vector2(0.0f,  vScale.y),
									Vector2(1.0f, 1.0f),
									FColorOrange,
									Font::EAlign::CenterTop
									);
					}
				}
			}

			pFont->Draw(pRenderingSystem,
						CharSet::Format("Time:%f", m_fCurrentTime),
						Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.8f),
						Vector2(1.0f, 1.0f),
						FColorPink,
						Font::EAlign::CenterTop);
			pFont->Draw(pRenderingSystem,
						CharSet::Format("BPM:%f", m_fBPM),
						Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.8f + 16.0f),
						Vector2(1.0f, 1.0f),
						FColorPink,
						Font::EAlign::CenterTop);
		}
		
		//----------------------------------------------------------------------
		//	時間の取得
		//----------------------------------------------------------------------
		f32 CRhythmEvent::GetTime(s32 iTiming)
		{
			if( !m_pHeader )
			{
				return 0;
			}
			//	1分間に刻むリズムの回数の逆数
			const f32 fInvRate = 60.0f / 4.0f;

			MusicData* pMusicData = GetMusicData();
			f32 fBPM = pMusicData->fBPM;

			f32 fBPMChangeTime = 0.0f;
			s32 iPrevTiming = 0;
			//	BPM情報から時間を算出する
			const s32 iBPMCount = GetRhythmGameHeader()->iBPMCount;
			for( s32 i=0; i < iBPMCount; i ++ )
			{
				const BPMData* pInfo = GetBPMData(i);
				if( iTiming < pInfo->iTiming )
				{
					break;
				}
				fBPMChangeTime += (f32)(pInfo->iTiming - iPrevTiming) * (fInvRate / fBPM);
				fBPM = pInfo->fBPM;
				iPrevTiming = pInfo->iTiming;
			}

			return fBPMChangeTime + (f32)(iTiming - iPrevTiming) * (fInvRate / fBPM);
		}
		//----------------------------------------------------------------------
		//	BPMの取得
		//----------------------------------------------------------------------
		f32 CRhythmEvent::GetBPM(s32 iTiming)
		{
			if( !m_pHeader )
			{
				return 0;
			}

			MusicData* pMusicData = GetMusicData();
			f32 fBPM = pMusicData->fBPM;
			//	指定タイミングのBPMを検索する
			const s32 iBPMCount = GetRhythmGameHeader()->iBPMCount;
			for( s32 i=0; i < iBPMCount; i ++ )
			{
				const BPMData* pInfo = GetBPMData(i);
				if( iTiming < pInfo->iTiming )
				{
					break;
				}
				fBPM = pInfo->fBPM;
			}

			return fBPM;
		}

		//----------------------------------------------------------------------
		//	タイミングの取得
		//----------------------------------------------------------------------
		s32 CRhythmEvent::GetTiming(f32 fTime)
		{
			if( !m_pHeader )
			{
				return 0;
			}
			//	1分間に刻むリズムの回数の逆数
			const f32 fInvRate = 4.0f / 60.0f;

			f32 fBPM = GetMusicData()->fBPM;

			s32 iPrevTiming = 0;
			f32 fBPMChangeTime = 0.0f;
			//	時間の取得
			const s32 iBPMCount = GetRhythmGameHeader()->iBPMCount;
			for( s32 i=0; i < iBPMCount; i ++ )
			{
				const BPMData* pInfo = GetBPMData(i);
				f32 fNextBPMChangeTime =  fBPMChangeTime + (f32)(pInfo->iTiming - iPrevTiming) / (fBPM * fInvRate);
				if( fTime <= fNextBPMChangeTime )
				{
					break;
				}

				fBPM = pInfo->fBPM;
				iPrevTiming = pInfo->iTiming;
				fBPMChangeTime = fNextBPMChangeTime;
			}

			return iPrevTiming + (s32)((fTime - fBPMChangeTime) * (fBPM * fInvRate));
		}

		//----------------------------------------------------------------------
		//	イベント番号の取得
		//----------------------------------------------------------------------
		s32 CRhythmEvent::GetEventIndex(f32 fTime, EventBase* pEvent, s32 iCount)
		{
			s32 low, high, mid;

			low  = 0;
			high = iCount - 1;
			while( low <= high )
			{
				mid = (low + high) >> 1;
				f32 ret = fTime - pEvent[mid].fTime;
				if( ret == 0 ) return mid;

				(ret < 0)? (high = mid - 1): (low = mid + 1);
			}

			return low;
		}

		//----------------------------------------------------------------------
		//	BPMの取得
		//----------------------------------------------------------------------
		f32 CRhythmEvent::GetBPM(void)
		{
			return m_fNextBPM;
		}
		//----------------------------------------------------------------------
		//	BG番号の取得
		//----------------------------------------------------------------------
		s32 CRhythmEvent::GetBG(void)
		{
			return m_iBG;
		}
		//----------------------------------------------------------------------
		//	リズム情報のリセット
		//----------------------------------------------------------------------
		void CRhythmEvent::Reset(void)
		{
			if( m_pHeader )
			{
				for( s32 i=0; i < m_pHeader->iRhythmCount; i++ )
				{
					m_pRhythmInfo[i].bEnable = true;
				}
				for( s32 i=0; i < m_pHeader->iBPMCount; i++ )
				{
					m_pBPMInfo[i].bEnable = true;
				}
				for( s32 i=0; i < m_pHeader->iBGCount; i++ )
				{
					m_pBGInfo[i].bEnable = true;
				}
			}
		}

		//----------------------------------------------------------------------
		//	リズムヘッダーの取得
		//----------------------------------------------------------------------
		RhythmGameHeader* CRhythmEvent::GetRhythmGameHeader(void)
		{
			return m_pHeader;
		}
		//----------------------------------------------------------------------
		//	楽曲情報の取得
		//----------------------------------------------------------------------
		MusicData* CRhythmEvent::GetMusicData(void)
		{
			if( !m_pHeader )
			{
				return null;
			}
			return (MusicData*)((u8*)m_pHeader + m_pHeader->iMusicDataOffset);
		}
		//----------------------------------------------------------------------
		//	リズム情報の取得
		//----------------------------------------------------------------------
		RhythmData* CRhythmEvent::GetRhythmData(s32 iNo)
		{
			if( !m_pHeader || iNo < 0 || m_pHeader->iRhythmCount < iNo )
			{
				return null;
			}
			return (RhythmData*)((u8*)m_pHeader + m_pHeader->iRhythmOffset) + iNo;
		}
		//----------------------------------------------------------------------
		//	BPM情報の取得
		//----------------------------------------------------------------------
		BPMData* CRhythmEvent::GetBPMData(s32 iNo)
		{
			if( !m_pHeader || iNo < 0 || m_pHeader->iBPMCount < iNo )
			{
				return null;
			}
			return (BPMData*)((u8*)m_pHeader + m_pHeader->iBPMOffset) + iNo;
		}
		//----------------------------------------------------------------------
		//	イベント情報の取得
		//----------------------------------------------------------------------
		BGData* CRhythmEvent::GetBGData(s32 iNo)
		{
			if( !m_pHeader || iNo < 0 || m_pHeader->iBGCount < iNo )
			{
				return null;
			}
			return (BGData*)((u8*)m_pHeader + m_pHeader->iBGOffset) + iNo;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
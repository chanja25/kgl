//----------------------------------------------------------------------
//!
//!	@file	RhythmGameEditor.cpp
//!	@brief	リズムゲーム関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Game/RhythmGame/RhythmGameEditor.h"
#include "Utility/Path/kgl.Path.h"
#include "Utility/algorithm/kgl.cio.h"
#include "File/kgl.Directory.h"

namespace App
{
	namespace RhythmGame
	{
		//----------------------------------------------------------------------
		//	コンストラクタ
		//----------------------------------------------------------------------
		CRhythmEditor::CRhythmEditor(void)
			: m_pRhythmGameManager(null)
			, m_pPath(0)
			, m_iMode(0)
			, m_iStep(0)
			, m_iSelect(0)
			, m_pMusicData(null)
			, m_fCurrentTime(0)
			, m_fBPM(0)
			, m_fDelay(0)
			, m_iBG(0)
			, m_bFailed(false)
		{}
		//----------------------------------------------------------------------
		//	デストラクタ
		//----------------------------------------------------------------------
		CRhythmEditor::~CRhythmEditor(void)
		{}

		//----------------------------------------------------------------------
		//	初期化
		//----------------------------------------------------------------------
		b8 CRhythmEditor::Initialize(void)
		{
			//	作業用領域の確保
			m_RhythmData.reserve(8096);
			m_BPMData.reserve(64);
			m_BGData.reserve(64);

			//	初期モードの設定
			ChangeMode(EEditMode::Select);

			return true;
		}
		//----------------------------------------------------------------------
		//	破棄
		//----------------------------------------------------------------------
		void CRhythmEditor::Finalize(void)
		{
			SafeDelete(m_pMusicData);
			m_RhythmData.clear();
			m_BPMData.clear();
			m_BGData.clear();

			if( m_pRhythmGameManager )
			{
				m_pRhythmGameManager->Finalize();
				m_pRhythmGameManager = null;
			}
		}

		//----------------------------------------------------------------------
		//	更新
		//----------------------------------------------------------------------
		void CRhythmEditor::Update(f32 fElapsedTime)
		{
			switch( m_iMode )
			{
			case EEditMode::Select:		UpdateSelect(fElapsedTime);		break;
			case EEditMode::Edit:		UpdateEdit(fElapsedTime);		break;
			case EEditMode::BPMChecker:	UpdateBPMChecker(fElapsedTime);	break;

			case EEditMode::TimingCheck:
			case EEditMode::TestPlay:	UpdateTestPlay(fElapsedTime);	break;

			case EEditMode::DebugTitle:
				KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
				break;
			}
		}
		//----------------------------------------------------------------------
		//	描画
		//----------------------------------------------------------------------
		void CRhythmEditor::Render(Render::IRenderingSystem* pRenderingSystem)
		{
			switch( m_iMode )
			{
			case EEditMode::Select:		RenderSelect(pRenderingSystem);		break;
			case EEditMode::Edit:		RenderEdit(pRenderingSystem);		break;
			case EEditMode::BPMChecker:	RenderBPMChecker(pRenderingSystem);	break;

			case EEditMode::TimingCheck:
			case EEditMode::TestPlay:	RenderTestPlay(pRenderingSystem);	break;
			}
		}
		//----------------------------------------------------------------------
		//	エディット選択更新
		//----------------------------------------------------------------------
		void CRhythmEditor::UpdateSelect(f32 fElapsedTime)
		{
			(void)fElapsedTime;

			switch( m_iStep )
			{
			case 0:
				{
					m_strEditList.clear();

					TKGLPtr<Directory::IDirectory> pDirectory;
					Directory::Create(pDirectory.GetReference());

					const c8* pPath = pDirectory->FindFile("Data/RhythmGameEdit/*", null, Directory::EAttribute::File);
					while( pPath )
					{
						if( kgStricmp(Path::GetExtension(pPath), "txt") == 0 )
						{
							m_strEditList.push_back(pPath);
						}
						pPath = pDirectory->NextFile();
					}
					m_bFailed = false;
					m_iStep ++;
				}
				break;

			case 1:
				if( AppInput()->IsButtonPush(EKeyCode::UP) )
				{
					m_iSelect --;
				}
				if( AppInput()->IsButtonPush(EKeyCode::DOWN) )
				{
					m_iSelect ++;
				}
				if( AppInput()->IsButtonPush(EKeyCode::START) )
				{
					m_iStep = 2;
				}
				if( m_strEditList.size() > 0 )
				{
					m_iSelect = Math::Repeat(m_iSelect, 0, m_strEditList.size() + 1);
				}
				else
				{
					m_iSelect = 0;
				}
				break;

			case 2:
				m_iStep = 1;
				m_bFailed = true;
				if( m_iSelect == m_strEditList.size() )
				{
					ChangeMode(EEditMode::DebugTitle);
				}
				else
				{
					if( Load(CharSet::Format("Data/RhythmGameEdit/%s", m_strEditList[m_iSelect].c_str())) )
					{
						c8 cTemp[256];
						Path::GetFileNameWithoutExtension(cTemp, m_strEditList[m_iSelect].c_str());

						Save(cTemp);
						m_bFailed = false;

						m_pPath = m_strEditList[m_iSelect].c_str();

						ChangeMode(EEditMode::Edit);
					}
				}
				break;
			}
		}
		//----------------------------------------------------------------------
		//	エディット更新
		//----------------------------------------------------------------------
		void CRhythmEditor::UpdateEdit(f32 fElapsedTime)
		{
			(void)fElapsedTime;

			switch( m_iStep )
			{
			case 0:
				m_EditSelect.Initialize(4, 1);
				m_EditSelect.SetValue(0, 0, EEditMode::BPMChecker);
				m_EditSelect.SetValue(1, 0, EEditMode::TimingCheck);
				m_EditSelect.SetValue(2, 0, EEditMode::TestPlay);
				m_EditSelect.SetValue(3, 0, EEditMode::Select);

				m_iStep ++;
				break;

			case 1:
				if( AppInput()->IsButtonPush(EKeyCode::UP) )
				{
					m_EditSelect.ColumnPrev();
				}
				if( AppInput()->IsButtonPush(EKeyCode::DOWN) )
				{
					m_EditSelect.ColumnNext();
				}
				if( AppInput()->IsButtonPush(EKeyCode::LEFT) )
				{
					m_EditSelect.RowPrev();
				}
				if( AppInput()->IsButtonPush(EKeyCode::RIGHT) )
				{
					m_EditSelect.RowNext();
				}
				if( AppInput()->IsButtonPush(EKeyCode::START) )
				{
					ChangeMode(m_EditSelect.GetValue());
					m_EditSelect.Finalize();
				}
				break;
			}
		}
		//----------------------------------------------------------------------
		//	BPM計測更新
		//----------------------------------------------------------------------
		void CRhythmEditor::UpdateBPMChecker(f32 fElapsedTime)
		{
			switch( m_iStep )
			{
			case 0:
				m_EditSelect.Initialize(4, 1);
				m_EditSelect.SetValue(0, 0, 10);	//Play
				m_EditSelect.SetValue(1, 0, 11);	//Pause
				m_EditSelect.SetValue(2, 0, 12);	//Resume
				m_EditSelect.SetValue(3, 0, 255);	//Exit

				{
					MusicData Data = *m_pMusicData;
					Data.fDelay = 0.0f;
					//	楽曲情報の初期化
					if( !m_Music.Initialize(&Data) )
					{
						m_bFailed = true;
					}
				}

				m_fBPM = 0.0f;
				m_fDelay = 0.0f;
				m_iBPMCount = 0;
				m_iBPMIndex = 0;
				m_iStep ++;
				break;

			case 1:
				if( AppInput()->IsButtonPush(EKeyCode::UP) )
				{
					m_EditSelect.ColumnPrev();
				}
				if( AppInput()->IsButtonPush(EKeyCode::DOWN) )
				{
					m_EditSelect.ColumnNext();
				}
				if( AppInput()->IsButtonPush(EKeyCode::LEFT) )
				{
					m_EditSelect.RowPrev();
				}
				if( AppInput()->IsButtonPush(EKeyCode::RIGHT) )
				{
					m_EditSelect.RowNext();
				}
				if( AppInput()->IsButtonPush(EKeyCode::START) )
				{
					m_iStep = m_EditSelect.GetValue();
				}
				break;

			case 10:
				m_Music.Play(0.0f);
				m_iBPMCount = 0;
				m_iBPMIndex = 0;
				m_iStep = 1;
				break;
			case 11:
				m_Music.Pause();
				m_iStep = 1;
				break;
			case 12:
				m_Music.Resume();
				m_iBPMCount = 0;
				m_iBPMIndex = 0;
				m_iStep = 1;
				break;
			case 255:
				m_Music.Finalize();
				m_EditSelect.Finalize();
				ChangeMode(EEditMode::Edit);
				break;
			}
			m_Music.Update(fElapsedTime);
			m_fCurrentTime = m_Music.CurrentTime();

			if( m_iBPMCount >= 2 )
			{
				f32 fTotalBPM = 0.0f;
				s32 iBPMCount = m_iBPMCount - 1;
				for( s32 i=0; i < iBPMCount; i++ )
				{
					s32 iIndex = Math::Repeat(m_iBPMIndex-i-1, 0, m_iBPMCount);
					s32 iPrevIndex = Math::Repeat(m_iBPMIndex-i-2, 0, m_iBPMCount);
					fTotalBPM += m_fBPMList[iIndex] - m_fBPMList[iPrevIndex];
				}
				f32 fStride = (fTotalBPM / (f32)iBPMCount);
				m_fBPM = 60.0f / fStride;

				f32 fTotalDelay = 0.0f;
				for( s32 i=0; i < m_iBPMCount; i++ )
				{
					s32 iIndex = Math::Repeat(m_iBPMIndex-i-1, 0, m_iBPMCount);
					f32 fDelay = Math::Mod(m_fBPMList[iIndex], fStride);
					if( fDelay < fStride * 0.5f )
					{
						fDelay += fStride;
					}
					fTotalDelay += fDelay;
				}
				m_fDelay = fTotalDelay / (f32)m_iBPMCount;
			}

			if( AppInput()->IsButtonPush(EKeyCode::SELECT) &&
				m_Music.IsPlaying() )
			{
				m_fBPMList[m_iBPMIndex] = m_fCurrentTime;
				m_iBPMIndex ++;
				m_iBPMCount = Max(m_iBPMCount, m_iBPMIndex);
				m_iBPMIndex = Math::Repeat(m_iBPMIndex, 0, ArrayCount(m_fBPMList));
			}
		}
		//----------------------------------------------------------------------
		//	テストプレイ更新
		//----------------------------------------------------------------------
		void CRhythmEditor::UpdateTestPlay(f32 fElapsedTime)
		{
			switch( m_iStep )
			{
			case 0:
				m_EditSelect.Initialize(4, 1);
				m_EditSelect.SetValue(0, 0, 10);	//Play
				m_EditSelect.SetValue(1, 0, 11);	//Pause
				m_EditSelect.SetValue(2, 0, 12);	//Resume
				m_EditSelect.SetValue(3, 0, 255);	//Exit

				m_pRhythmGameManager = RGManager();
				m_pRhythmGameManager->Initialize();

				m_pRhythmGameManager->SetAllInput(m_iMode == EEditMode::TimingCheck);
				{
					const c8* pPath = CharSet::Format("Data/RhythmGame/%s.rgd", Path::GetFileNameWithoutExtension(m_pPath).c_str());

					m_pRhythmGameManager->Setup(KGLFileReader()->LoadBlock(pPath));
				}
				m_iStep ++;
				break;

			case 1:
				if( AppInput()->IsButtonPush(EKeyCode::UP) )
				{
					m_EditSelect.ColumnPrev();
				}
				if( AppInput()->IsButtonPush(EKeyCode::DOWN) )
				{
					m_EditSelect.ColumnNext();
				}
				if( AppInput()->IsButtonPush(EKeyCode::LEFT) )
				{
					m_EditSelect.RowPrev();
				}
				if( AppInput()->IsButtonPush(EKeyCode::RIGHT) )
				{
					m_EditSelect.RowNext();
				}
				if( AppInput()->IsButtonPush(EKeyCode::START) )
				{
					m_iStep = m_EditSelect.GetValue();
				}
				break;

			case 10:
				m_pRhythmGameManager->Start();
				m_iStep = 1;
				break;
			case 11:
				m_pRhythmGameManager->Pause();
				m_iStep = 1;
				break;
			case 12:
				m_pRhythmGameManager->Resume();
				m_iStep = 1;
				break;
			case 255:
				m_pRhythmGameManager->Finalize();
				m_pRhythmGameManager = null;

				m_EditSelect.Finalize();
				ChangeMode(EEditMode::Edit);

				{
					const c8* pPath = CharSet::Format("Data/RhythmGame/%s.rgd", Path::GetFileNameWithoutExtension(m_pPath).c_str());
					KGLFileReader()->Unload(pPath);
				}
				break;
			}
			if( m_pRhythmGameManager )
			{
				m_pRhythmGameManager->Update(fElapsedTime);
			}
		}

		//----------------------------------------------------------------------
		//	エディット選択描画
		//----------------------------------------------------------------------
		void CRhythmEditor::RenderSelect(Render::IRenderingSystem* pRenderingSystem)
		{
			TKGLPtr<Font::IFont> pFont = AppGameInfo()->Font();
			Vector2 vPos(32, 64);
			for( u32 i=0; i < m_strEditList.size(); i++ )
			{
				pFont->Draw(pRenderingSystem,
							Path::GetFileName(m_strEditList[i].c_str()),
							vPos,
							Vector2(1.0f, 1.0f),
							(i == m_iSelect)? FColorOrange: FColorWhite);

				vPos.y += pFont->GetDrawTextSize(L" ").y;
			}
			pFont->Draw(pRenderingSystem,
						"Exit",
						vPos,
						Vector2(1.0f, 1.0f),
						(m_strEditList.size() == m_iSelect)? FColorOrange: FColorWhite);

			if( m_bFailed )
			{
				pFont->Draw(pRenderingSystem,
							"Failed",
							Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.75f),
							Vector2(1.0f, 1.0f),
							FColorRed);
			}
		}
		//----------------------------------------------------------------------
		//	エディット描画
		//----------------------------------------------------------------------
		void CRhythmEditor::RenderEdit(Render::IRenderingSystem* pRenderingSystem)
		{
			s32 iRowMax = m_EditSelect.RowMax();
			s32 iColumnMax = m_EditSelect.ColumnMax();
			s32 iRow = m_EditSelect.Row();
			s32 iColumn = m_EditSelect.Column();

			//	row:2 column:1
			const c8* pMenus[] =
			{
				"BPMChecker",	"TimingCheck",	"TestPlay",	"ReturnSelect",
			};

			TKGLPtr<Font::IFont> pFont = AppGameInfo()->Font();
			Vector2 vPos(32, 64);
			for( s32 i=0; i < iRowMax; i++ )
			{
				for( s32 j=0; j < iColumnMax; j++ )
				{
					pFont->Draw(pRenderingSystem,
								pMenus[i + j * iRowMax],
								vPos + Vector2(128.0f * (f32)i, 16.0f * (f32)j),
								Vector2(1.0f, 1.0f),
								(i == iRow && j == iColumn)? FColorOrange: FColorWhite);
				}
			}
		}
		//----------------------------------------------------------------------
		//	BPM計測描画
		//----------------------------------------------------------------------
		void CRhythmEditor::RenderBPMChecker(Render::IRenderingSystem* pRenderingSystem)
		{
			s32 iRowMax = m_EditSelect.RowMax();
			s32 iColumnMax = m_EditSelect.ColumnMax();
			s32 iRow = m_EditSelect.Row();
			s32 iColumn = m_EditSelect.Column();

			//	row:2 column:1
			const c8* pMenus[] =
			{
				"Play",	"Pause",	"Resume",	"ReturnEdit",
			};

			TKGLPtr<Font::IFont> pFont = AppGameInfo()->Font();
			Vector2 vPos(32, 64);
			for( s32 i=0; i < iRowMax; i++ )
			{
				for( s32 j=0; j < iColumnMax; j++ )
				{
					pFont->Draw(pRenderingSystem,
								pMenus[i + j * iRowMax],
								vPos + Vector2(128.0f * (f32)i, 16.0f * (f32)j),
								Vector2(1.0f, 1.0f),
								(i == iRow && j == iColumn)? FColorOrange: FColorWhite);
				}
			}

			pFont->Draw(pRenderingSystem,
						CharSet::Format("[BPM Conter]:Push Space"),
						Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.7f),
						Vector2(1.25f, 1.25f),
						FColorPink,
						Font::EAlign::CenterTop);
			pFont->Draw(pRenderingSystem,
						CharSet::Format("Time:%f", m_fCurrentTime),
						Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.75f),
						Vector2(1.0f, 1.0f),
						FColorPink,
						Font::EAlign::CenterTop);
			pFont->Draw(pRenderingSystem,
						CharSet::Format("BPM:%f", m_fBPM),
						Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.75f + 16),
						Vector2(1.0f, 1.0f),
						FColorPink,
						Font::EAlign::CenterTop);
			pFont->Draw(pRenderingSystem,
						CharSet::Format("Delay:%f", m_fDelay),
						Vector2(Config::Resolution.x * 0.5f, Config::Resolution.y * 0.75f + 32),
						Vector2(1.0f, 1.0f),
						FColorPink,
						Font::EAlign::CenterTop);
		}
		//----------------------------------------------------------------------
		//	テストプレイ描画
		//----------------------------------------------------------------------
		void CRhythmEditor::RenderTestPlay(Render::IRenderingSystem* pRenderingSystem)
		{
			if( m_pRhythmGameManager )
			{
				m_pRhythmGameManager->Render(pRenderingSystem);
			}

			s32 iRowMax = m_EditSelect.RowMax();
			s32 iColumnMax = m_EditSelect.ColumnMax();
			s32 iRow = m_EditSelect.Row();
			s32 iColumn = m_EditSelect.Column();
			//	row:2 column:1
			const c8* pMenus[] =
			{
				"Play",	"Pause",	"Resume",	"ReturnEdit",
			};

			TKGLPtr<Font::IFont> pFont = AppGameInfo()->Font();
			Vector2 vPos(32, 64);
			for( s32 i=0; i < iRowMax; i++ )
			{
				for( s32 j=0; j < iColumnMax; j++ )
				{
					pFont->Draw(pRenderingSystem,
								pMenus[i + j * iRowMax],
								vPos + Vector2(128.0f * (f32)i, 16.0f * (f32)j),
								Vector2(1.0f, 1.0f),
								(i == iRow && j == iColumn)? FColorOrange: FColorWhite);
				}
			}
		}

		//----------------------------------------------------------------------
		//	モードの変更
		//----------------------------------------------------------------------
		void CRhythmEditor::ChangeMode(s32 iMode)
		{
			m_iStep = 0;
			m_iMode = iMode;
		}

		//----------------------------------------------------------------------
		//	時間の取得
		//----------------------------------------------------------------------
		f32 CRhythmEditor::GetTime(s32 iTiming)
		{
			(void)iTiming;
			return 0;
		}
		//----------------------------------------------------------------------
		//	タイミングの取得
		//----------------------------------------------------------------------
		s32 CRhythmEditor::GetTiming(f32 fTime)
		{
			(void)fTime;
			return 0;
		}
		//----------------------------------------------------------------------
		//	イベント番号の取得
		//----------------------------------------------------------------------
		s32 CRhythmEditor::GetEventIndex(f32 fTime, EventBase* pEvent, s32 iCount)
		{
			(void)fTime;
			(void)pEvent;
			(void)iCount;
			return 0;
		}
		//----------------------------------------------------------------------
		//	エディットデータの保存
		//----------------------------------------------------------------------
		b8 CRhythmEditor::Save(const c8* pPath)
		{
			if( !m_pMusicData )
			{
				return false;
			}

			TKGLPtr<File::IFile> pFile;
			File::Create(pFile.GetReference());

			if( pFile->Open(CharSet::Format("Data/RhythmGame/%s.rgd", pPath), File::EOpen::Write, 0, File::ECreate::CreateAlways) )
			{
				RhythmGameHeader Header;
				//	ヘッダーの設定
				Header.iID = FOURCC('r', 'g', 'd', ' ');
				Header.iVersion = 0;
				Header.iMusicDataOffset = Align<s32>(sizeof(RhythmGameHeader), 32);
				Header.iRhythmCount = m_RhythmData.size();
				Header.iRhythmOffset = Align<s32>(Header.iMusicDataOffset + sizeof(MusicData), 32);
				Header.iBPMCount = m_BPMData.size();
				Header.iBPMOffset = Align<s32>(Header.iRhythmOffset + Header.iRhythmCount * sizeof(RhythmData), 32);
				Header.iBGCount = m_BGData.size();
				Header.iBGOffset = Align<s32>(Header.iBPMOffset + Header.iBPMCount * sizeof(BPMData), 32);

				//	ゲームデータの書き込み
				pFile->Write(&Header, sizeof(RhythmGameHeader));

				pFile->Seek(Header.iMusicDataOffset);
				pFile->Write(m_pMusicData, sizeof(MusicData));

				if( Header.iRhythmCount > 0 )
				{
					pFile->Seek(Header.iRhythmOffset);
					pFile->Write(&m_RhythmData[0], Header.iRhythmCount * sizeof(MusicData));
				}
				if( Header.iBPMCount > 0 )
				{
					pFile->Seek(Header.iBPMOffset);
					pFile->Write(&m_BPMData[0], Header.iBPMCount * sizeof(BPMData));
				}
				if( Header.iBGCount > 0 )
				{
					pFile->Seek(Header.iBGOffset);
					pFile->Write(&m_BGData[0], Header.iBGCount * sizeof(BGData));
				}

				pFile->Close();

				//	エディットデータの出力
				cout writer(CharSet::Format("Data/RhythmGameEdit/%s.txt", pPath));

				writer << "<RhythmGame>" EOL EOL;

				writer << CharSet::Format("<MusicData path=\"%s\" delay=%f bpm=%f margin=%f level=%d>" EOL EOL,
											m_pMusicData->cPath,
											m_pMusicData->fDelay,
											m_pMusicData->fBPM,
											m_pMusicData->fTimingMargin,
											m_pMusicData->iLevel);

				if( Header.iRhythmCount > 0 )
				{
					writer << "<RhythmData>" EOL;
					for( u32 i=0; i < m_RhythmData.size(); i++ )
					{
						writer << CharSet::Format("\t<Rhythm timing=%d ", m_RhythmData[i].iTiming);
						s32 iInput = m_RhythmData[i].iInput;
						if( iInput & 0x000000ff )
						{
							writer << CharSet::Format(" key0=\'%c\'", (c8)(iInput & 0xff));
						}
						if( iInput & 0x0000ff00 )
						{
							writer << CharSet::Format(" key1=\'%c\'", (c8)((iInput >> 8) & 0xff));
						}
						if( iInput & 0x00ff0000 )
						{
							writer << CharSet::Format(" key2=\'%c\'", (c8)((iInput >> 16) & 0xff));
						}
						if( iInput & 0xff000000 )
						{
							writer << CharSet::Format(" key3=\'%c\'", (c8)((iInput >> 24) & 0xff));
						}
						writer << ">" EOL;
					}
					writer << "</RhythmData>" EOL EOL;
				}
				if( Header.iBPMCount > 0 )
				{
					writer << "<BPMData>" EOL;
					for( u32 i=0; i < m_BPMData.size(); i++ )
					{
						writer << CharSet::Format("\t<BPM timing=%d, rate=%f>" EOL, m_BPMData[i].iTiming, m_BPMData[i].fBPM);
					}
					writer << "</BPMData>" EOL EOL;
				}
				if( Header.iBGCount > 0 )
				{
					writer << "<BGData>" EOL;
					for( u32 i=0; i < m_BGData.size(); i++ )
					{
						writer << CharSet::Format("\t<BG timing=%d, id=%d>" EOL, m_BGData[i].iTiming, m_BGData[i].iBG);
					}
					writer << "</BGData>" EOL EOL;
				}

				writer << "</RhythmGame>" EOL;
			}

			return true;
		}
		//----------------------------------------------------------------------
		//	エディットデータの読み込み
		//----------------------------------------------------------------------
		b8 CRhythmEditor::Load(const c8* pPath)
		{
			//	現在のデータを破棄する
			SafeDelete(m_pMusicData);
			m_RhythmData.clear();
			m_BPMData.clear();
			m_BGData.clear();

			//	編集データの解析
			Text::CLexer Lexser;
			if( !Lexser.LoadText(pPath) )
			{
				return false;
			}
			const Text::Token* pToken = Lexser.AnalyzeAll();
			if( Lexser.IsError() )
			{
				return false;
			}

			//	トークン情報
			struct TokenInfo
			{
				const c8*	pToken;
				s32			iStep;
			};
			//	トークン情報のリスト
			const TokenInfo TokenInfoList[] =
			{
				{ "RhythmGame",	10,	},
				{ "MusicData",	11,	},
				{ "RhythmData",	12,	},
				{ "BPMData",	13,	},
				{ "BGData",		14,	},
			};

			b8 bExit = false;
			s32 iNextStep = 0;
			s32 iStep = 0;
			while( pToken && !bExit && iStep != 255 )
			{
				iStep = iNextStep;
				iNextStep = 255;
				switch( iStep )
				{
				case 0:
					if( kgStricmp(pToken->pToken, "<") == 0 )
					{
						if( pToken->pNext )
						{
							if( kgStricmp(pToken->pNext->pToken, "/") == 0 )
							{
								iNextStep = 1;
								pToken = pToken->pNext->pNext;
							}
							else
							{
								u32 i;
								for( i = 0; i < ArrayCount(TokenInfoList); i++ )
								{
									if( kgStricmp(pToken->pNext->pToken, TokenInfoList[i].pToken) == 0 )
									{
										iNextStep = TokenInfoList[i].iStep;
										break;
									}
								}
							}
						}
					}
					break;
				case 1:
					//	終了判定</RhythmGame>
					if( kgStricmp(pToken->pToken, "RhythmGame") == 0 )
					{
						if( pToken->pNext )
						{
							if( kgStricmp(pToken->pNext->pToken, ">") == 0 )
							{
								bExit = true;
							}
						}
					}
					break;
				case 10://<RhythmGame>
					if( kgStricmp(pToken->pNext->pNext->pToken, ">") == 0 )
					{
						pToken = pToken->pNext->pNext->pNext;
						iNextStep = 0;
					}
					break;

				case 11://<SetupMusicData>
					if( SetupMusicData(&pToken) )
					{
						iNextStep = 0;
					}
					break;
				case 12://<SetupRhythmData>
					if( SetupRhythmData(&pToken) )
					{
						iNextStep = 0;
					}
					break;
				case 13://<SetupBPMData>
					if( SetupBPMData(&pToken) )
					{
						iNextStep = 0;
					}
					break;
				case 14://<SetupBGData>
					if( SetupBGData(&pToken) )
					{
						iNextStep = 0;
					}
					break;
				}
			}
			return bExit;
		}
		//----------------------------------------------------------------------
		//	楽曲データのセットアップ
		//----------------------------------------------------------------------
		b8 CRhythmEditor::SetupMusicData(const Text::Token** ppToken)
		{
			const Text::Token* pToken = *ppToken;
			if( pToken == null || kgStrcmp(pToken->pToken, "<") != 0 || m_pMusicData )
			{
				return false;
			}

			const c8* pPath = null;
			f32 fDelay = 0.0f;
			f32 fBPM = 60.0f;
			f32 fTimingMargin = 0.5f;
			s32 iLevel = 0;
			while( (pToken = pToken->pNext) != null )
			{
				if( kgStricmp(pToken->pToken, ">") == 0 )
				{
					if( pPath == null )break;
					pToken = pToken->pNext;

					m_pMusicData = new MusicData;

					kgStrcpy(m_pMusicData->cPath, pPath);
					m_pMusicData->fDelay = fDelay;
					m_pMusicData->fBPM = fBPM;
					m_pMusicData->fTimingMargin = fTimingMargin;
					m_pMusicData->iLevel = iLevel;
					break;
				}
				else if( kgStricmp(pToken->pToken, "path") == 0 )
				{
					if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
					pToken = pToken->pNext->pNext;
					pPath = pToken->pToken;
				}
				else if( kgStricmp(pToken->pToken, "delay") == 0 )
				{
					if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
					pToken = pToken->pNext->pNext;
					fDelay = (f32)atof(pToken->pToken);
				}
				else if( kgStricmp(pToken->pToken, "bpm") == 0 )
				{
					if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
					pToken = pToken->pNext->pNext;
					fBPM = (f32)atof(pToken->pToken);
				}
				else if( kgStricmp(pToken->pToken, "margin") == 0 )
				{
					if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
					pToken = pToken->pNext->pNext;
					fTimingMargin = (f32)atof(pToken->pToken);
				}
				else if( kgStricmp(pToken->pToken, "level") == 0 )
				{
					if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
					pToken = pToken->pNext->pNext;
					iLevel = atoi(pToken->pToken);
				}
			}
			*ppToken = pToken;
			return m_pMusicData != null;
		}
		//----------------------------------------------------------------------
		//	リズムデータのセットアップ
		//----------------------------------------------------------------------
		b8 CRhythmEditor::SetupRhythmData(const Text::Token** ppToken)
		{
			const Text::Token* pToken = *ppToken;
			if( m_RhythmData.size() > 0 )
			{
				return false;
			}
			if( pToken == null || kgStrcmp(pToken->pToken, "<") != 0 )
			{
				return false;
			}
			if( pToken->pNext == null || kgStrcmp(pToken->pNext->pToken, "RhythmData") != 0 )
			{
				return false;
			}
			pToken = pToken->pNext;
			if( pToken->pNext == null || kgStrcmp(pToken->pNext->pToken, ">") != 0 )
			{
				return false;
			}
			pToken = pToken->pNext;

			while( (pToken = pToken->pNext) != null )
			{
				if( kgStricmp(pToken->pToken, "<") == 0 )
				{
					pToken = pToken->pNext;
					if( kgStricmp(pToken->pToken, "/") == 0 &&
						kgStricmp(pToken->pNext->pToken, "RhythmData") == 0 &&
						kgStricmp(pToken->pNext->pNext->pToken, ">") == 0 )
					{
						pToken = pToken->pNext->pNext->pNext;
						break;
					}
					else if( kgStricmp(pToken->pToken, "Rhythm") == 0 )
					{
						s32 iTiming = -1;
						s32 iInput = 0;
						while( (pToken = pToken->pNext) != null )
						{
							if( kgStricmp(pToken->pToken, ">") == 0 )
							{
								if( iTiming == -1 || iInput == 0 )break;

								RhythmData Data;
								Data.iTiming = iTiming;
								Data.iInput = iInput;

								m_RhythmData.push_back(Data);
								break;
							}
							else if( kgStricmp(pToken->pToken, "timing") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iTiming = atoi(pToken->pToken);
							}
							else if( kgStricmp(pToken->pToken, "key0") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iInput |= pToken->pToken[0] & 0x000000FF;
							}
							else if( kgStricmp(pToken->pToken, "key1") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iInput |= (CharSet::ToUpper(pToken->pToken)[0] << 8) & 0x0000FF00;
							}
							else if( kgStricmp(pToken->pToken, "key2") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iInput |= (CharSet::ToUpper(pToken->pToken)[0] << 16) & 0x00FF0000;
							}
							else if( kgStricmp(pToken->pToken, "key3") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iInput |= (CharSet::ToUpper(pToken->pToken)[0] << 24) & 0xFF000000;
							}
						}
					}
				}
			}
			*ppToken = pToken;
			return m_RhythmData.size() > 0;
		}
		//----------------------------------------------------------------------
		//	BPMデータのセットアップ
		//----------------------------------------------------------------------
		b8 CRhythmEditor::SetupBPMData(const Text::Token** ppToken)
		{
			const Text::Token* pToken = *ppToken;
			if( m_BPMData.size() > 0 )
			{
				return false;
			}
			if( pToken == null || kgStrcmp(pToken->pToken, "<") != 0 )
			{
				return false;
			}
			if( pToken->pNext == null || kgStrcmp(pToken->pNext->pToken, "BPMData") != 0 )
			{
				return false;
			}
			pToken = pToken->pNext;
			if( pToken->pNext == null || kgStrcmp(pToken->pNext->pToken, ">") != 0 )
			{
				return false;
			}
			pToken = pToken->pNext;

			while( (pToken = pToken->pNext) != null )
			{
				if( kgStricmp(pToken->pToken, "<") == 0 )
				{
					pToken = pToken->pNext;
					if( kgStricmp(pToken->pToken, "/") == 0 &&
						kgStricmp(pToken->pNext->pToken, "BPMData") == 0 &&
						kgStricmp(pToken->pNext->pNext->pToken, ">") == 0 )
					{
						pToken = pToken->pNext->pNext->pNext;
						break;
					}
					else if( kgStricmp(pToken->pToken, "BPM") == 0 )
					{
						s32 iTiming = -1;
						f32 fBPM = 0;
						while( (pToken = pToken->pNext) != null )
						{
							if( kgStricmp(pToken->pToken, ">") == 0 )
							{
								if( iTiming == -1 || fBPM == 0 )break;

								BPMData Data;
								Data.iTiming = iTiming;
								Data.fBPM = fBPM;

								m_BPMData.push_back(Data);
								break;
							}
							else if( kgStricmp(pToken->pToken, "timing") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iTiming = atoi(pToken->pToken);
							}
							else if( kgStricmp(pToken->pToken, "rate") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								fBPM = (f32)atof(pToken->pToken);
							}
						}
					}
				}
			}
			*ppToken = pToken;
			return m_BPMData.size() > 0;
		}
		//----------------------------------------------------------------------
		//	BGデータのセットアップ
		//----------------------------------------------------------------------
		b8 CRhythmEditor::SetupBGData(const Text::Token** ppToken)
		{
			const Text::Token* pToken = *ppToken;
			if( m_BGData.size() > 0 )
			{
				return false;
			}
			if( pToken == null || kgStrcmp(pToken->pToken, "<") != 0 )
			{
				return false;
			}
			if( pToken->pNext == null || kgStrcmp(pToken->pNext->pToken, "BGData") != 0 )
			{
				return false;
			}
			pToken = pToken->pNext;
			if( pToken->pNext == null || kgStrcmp(pToken->pNext->pToken, ">") != 0 )
			{
				return false;
			}
			pToken = pToken->pNext;

			while( (pToken = pToken->pNext) != null )
			{
				if( kgStricmp(pToken->pToken, "<") == 0 )
				{
					pToken = pToken->pNext;
					if( kgStricmp(pToken->pToken, "/") == 0 &&
						kgStricmp(pToken->pNext->pToken, "BGData") == 0 &&
						kgStricmp(pToken->pNext->pNext->pToken, ">") == 0 )
					{
						pToken = pToken->pNext->pNext->pNext;
						break;
					}
					else if( kgStricmp(pToken->pToken, "BG") == 0 )
					{
						s32 iTiming = -1;
						s32 iBG = -1;
						while( (pToken = pToken->pNext) != null )
						{
							if( kgStricmp(pToken->pToken, ">") == 0 )
							{
								if( iTiming == -1 || iBG == -1 )break;

								BGData Data;
								Data.iTiming = iTiming;
								Data.iBG = iBG;

								m_BGData.push_back(Data);
								break;
							}
							else if( kgStricmp(pToken->pToken, "timing") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iTiming = atoi(pToken->pToken);
							}
							else if( kgStricmp(pToken->pToken, "index") == 0 )
							{
								if( kgStrcmp(pToken->pNext->pToken, "=") != 0 )break;
								pToken = pToken->pNext->pNext;
								iBG = atoi(pToken->pToken);
							}
						}
					}
				}
			}
			*ppToken = pToken;
			return m_BGData.size() > 0;
		}
	}
}

//======================================================================
//	END OF FILE
//======================================================================
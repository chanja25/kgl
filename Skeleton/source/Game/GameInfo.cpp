//----------------------------------------------------------------------
//!
//!	@file	GameInfo.h
//!	@brief	ゲーム情報管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Game/GameInfo.h"

namespace App
{
	//----------------------------------------------------------------------
	// コンストラクタ
	//----------------------------------------------------------------------
	CGameInfo::CGameInfo(void)
	{
	}
	//----------------------------------------------------------------------
	// デストラクタ
	//----------------------------------------------------------------------
	CGameInfo::~CGameInfo(void)
	{
	}
	//----------------------------------------------------------------------
	// 初期化
	//----------------------------------------------------------------------
	b8 CGameInfo::Initialize(void)
	{
		KGLAudioManager()->SoundDriver()->SoundDevice()->SetVolume(0.7f);

		Font::FontInfo Info;
		kgStrcpy(Info.cFont, "ＭＳ ゴシック");
		Info.iSize = 20;
		Font::Create(m_pFont.GetReference(), Info);

		return true;
	}
}

//======================================================================
//	END OF FILE
//======================================================================
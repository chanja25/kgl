//----------------------------------------------------------------------
//!
//!	@file	SceneAdventure.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/SceneAdventure.h"

namespace App
{
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneAdventure::Initialize(void)
	{
		return true;
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneAdventure::Finalize(void)
	{
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneAdventure::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneAdventure::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		(void)pRenderingSystem;
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
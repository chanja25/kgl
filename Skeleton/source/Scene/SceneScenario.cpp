//----------------------------------------------------------------------
//!
//!	@file	SceneScenario.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/SceneScenario.h"

namespace App
{
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneScenario::Initialize(void)
	{
		m_pScenarioManager = ScenarioManager();
		m_pScenarioManager->Initialize();
		return true;
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneScenario::Finalize(void)
	{
		m_pScenarioManager->Finalize();
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneScenario::Update(f32 fElapsedTime)
	{
		m_pScenarioManager->Update(fElapsedTime);

		if( !m_pScenarioManager->IsRunning() )
		{
			KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
		}
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneScenario::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		m_pScenarioManager->Render(pRenderingSystem);
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	SceneRhythmGame.cpp
//!	@brief	リズムゲーム
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/SceneRhythmGame.h"
#include "File/kgl.Directory.h"
#include "Utility/Path/kgl.Path.h"

namespace App
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CSceneRhythmGame::CSceneRhythmGame(void)
		: m_pRhythmGameManager(null)
		, m_iStep(0)
		, m_iSelect(0)
		, m_pPath(null)
	{}
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneRhythmGame::Initialize(void)
	{
		m_pRhythmGameManager = RGManager();
		m_pRhythmGameManager->Initialize();
		return true;
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneRhythmGame::Finalize(void)
	{
		if( m_pRhythmGameManager )
		{
			m_pRhythmGameManager->Finalize();
			m_pRhythmGameManager = null;
		}
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneRhythmGame::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;

		switch( m_iStep )
		{
		case 0:
			{
				m_strFileList.clear();

				TKGLPtr<Directory::IDirectory> pDirectory;
				Directory::Create(pDirectory.GetReference());

				const c8* pPath = pDirectory->FindFile("Data/RhythmGame/*", null, Directory::EAttribute::File);
				while( pPath )
				{
					if( kgStricmp(Path::GetExtension(pPath), "rgd") == 0 )
					{
						m_strFileList.push_back(pPath);
					}
					pPath = pDirectory->NextFile();
				}
				m_iStep ++;
			}
			break;

		case 1:
			if( AppInput()->IsButtonPush(EKeyCode::UP) )
			{
				m_iSelect --;
			}
			if( AppInput()->IsButtonPush(EKeyCode::DOWN) )
			{
				m_iSelect ++;
			}
			if( AppInput()->IsButtonPush(EKeyCode::START) )
			{
				if( m_pPath )
				{
					const c8* pPath = CharSet::Format("Data/RhythmGame/%s", m_pPath);
					KGLFileReader()->Unload(pPath);
					m_pPath = null;
				}
				m_iStep = 2;
			}
			if( m_strFileList.size() > 0 )
			{
				m_iSelect = Math::Repeat(m_iSelect, 0, m_strFileList.size() + 1);
			}
			else
			{
				m_iSelect = 0;
			}
			break;

		case 2:
			m_iStep = 1;
			if( m_iSelect == m_strFileList.size() )
			{
				m_iStep = 255;
			}
			else
			{
				m_pPath = m_strFileList[m_iSelect].c_str();
				const c8* pPath = CharSet::Format("Data/RhythmGame/%s", m_pPath);
				if( m_pRhythmGameManager->Setup(KGLFileReader()->LoadBlock(pPath)) )
				{
					m_pRhythmGameManager->Start();
					m_iStep = 100;
				}
			}
			break;

		case 100:
			if( AppInput()->IsButtonPush(EKeyCode::START) )
			{
				m_iStep = 1;
			}
			break;

		case 255:
			KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
			return;
		}
		if( m_pRhythmGameManager )
		{
			m_pRhythmGameManager->Update(fElapsedTime);
		}
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneRhythmGame::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		switch( m_iStep )
		{
		case 1:
			TKGLPtr<Font::IFont> pFont = AppGameInfo()->Font();
			Vector2 vPos(32, 64);
			for( u32 i=0; i < m_strFileList.size(); i++ )
			{
				pFont->Draw(pRenderingSystem,
							Path::GetFileName(m_strFileList[i].c_str()),
							vPos,
							Vector2(1.0f, 1.0f),
							(i == m_iSelect)? FColorOrange: FColorWhite);

				vPos.y += pFont->GetDrawTextSize(L" ").y;
			}
			pFont->Draw(pRenderingSystem,
						"Exit",
						vPos,
						Vector2(1.0f, 1.0f),
						(m_strFileList.size() == m_iSelect)? FColorOrange: FColorWhite);
			break;
		}

		if( m_pRhythmGameManager )
		{
			m_pRhythmGameManager->Render(pRenderingSystem);
		}
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
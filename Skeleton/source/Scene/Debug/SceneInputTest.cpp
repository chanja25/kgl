//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

#if !KGL_FINAL_RELEASE
#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Random/kgl.Random.h"


namespace App
{
	enum EKey
	{
		EKeyA,
		EKeyB,
		EKeyC,
		EKeyD,
		EKeyE,
		EKeyF,
		EKeyG,
		EKeyH,
		EKeyI,
		EKeyJ,
		EKeyK,
		EKeyL,
		EKeyM,
		EKeyN,
		EKeyO,
		EKeyP,
		EKeyQ,
		EKeyR,
		EKeyS,
		EKeyT,
		EKeyU,
		EKeyV,
		EKeyW,
		EKeyX,
		EKeyY,
		EKeyZ,

		EKey0,
		EKey1,
		EKey2,
		EKey3,
		EKey4,
		EKey5,
		EKey6,
		EKey7,
		EKey8,
		EKey9,

		EKeyNum0,
		EKeyNum1,
		EKeyNum2,
		EKeyNum3,
		EKeyNum4,
		EKeyNum5,
		EKeyNum6,
		EKeyNum7,
		EKeyNum8,
		EKeyNum9,

		EKeyF1,
		EKeyF2,
		EKeyF3,
		EKeyF4,
		EKeyF5,
		EKeyF6,
		EKeyF7,
		EKeyF8,
		EKeyF9,
		EKeyF10,
		EKeyF11,
		EKeyF12,
		EKeyF13,
		EKeyF14,
		EKeyF15,
		EKeyF16,
		EKeyF17,
		EKeyF18,
		EKeyF19,
		EKeyF20,
		EKeyF21,
		EKeyF22,
		EKeyF23,
		EKeyF24,

		EKeyHyphen,			// -
		EKeyCaret,			// ^
		EKeyYen,			// ￥
	
		EKeyAt,				// @
		EKeySemicolon,		// ;
		EKeyColon,			// :
		EKeyLBrackets,		// [
		EKeyRBrackets,		// ]
		EKeyComma,			// ,
		EKeyPeriod,			// .
		EKeySlash,			// /

		EKeyShiftL,
		EKeyShiftR,
		EKeyCtrlL,
		EKeyCtrlR,
		EKeyAltL,
		EKeyAltR,
		EKeyWinL,
		EKeyWinR,
		EKeyUp,
		EKeyDown,
		EKeyLeft,
		EKeyRight,
		EKeyReturn,
		EKeySpace,
		EKeyBack,
		EKeyTab,
		EKeyInsert,
		EKeyDelete,
		EKeyHome,
		EKeyEnd,
		EKeyPageDown,
		EKeyPageUp,
		EKeyHelp,
		EKeyEscape,
		EKeyPrint,
		EKeyScrollLock,
		EKeyPause,
		EKeyNumLock,
		EKeyMulti,
		EKeyAdd,
		EKeySeparator,
		EKeySubtract,
		EKeyDecimal,
		EKeyDivide,
		EKeySleep,
		EKeyCapsLock,
		EKeyConvert,
		EKeyNonConvert,

		EKeyMax,
	};
	s32 ConvertInputKeyCode(EKey key)
	{
		switch( key )
		{
		case EKeyA:	return 'A';
		case EKeyB:	return 'B';
		case EKeyC:	return 'C';
		case EKeyD:	return 'D';
		case EKeyE:	return 'E';
		case EKeyF:	return 'F';
		case EKeyG:	return 'G';
		case EKeyH:	return 'H';
		case EKeyI:	return 'I';
		case EKeyJ:	return 'J';
		case EKeyK:	return 'K';
		case EKeyL:	return 'L';
		case EKeyM:	return 'M';
		case EKeyN:	return 'N';
		case EKeyO:	return 'O';
		case EKeyP:	return 'P';
		case EKeyQ:	return 'Q';
		case EKeyR:	return 'R';
		case EKeyS:	return 'S';
		case EKeyT:	return 'T';
		case EKeyU:	return 'U';
		case EKeyV:	return 'V';
		case EKeyW:	return 'W';
		case EKeyX:	return 'X';
		case EKeyY:	return 'Y';
		case EKeyZ:	return 'Z';

		case EKey0:	return '0';
		case EKey1:	return '1';
		case EKey2:	return '2';
		case EKey3:	return '3';
		case EKey4:	return '4';
		case EKey5:	return '5';
		case EKey6:	return '6';
		case EKey7:	return '7';
		case EKey8:	return '8';
		case EKey9:	return '9';

		case EKeyNum0:	return VK_NUMPAD0;
		case EKeyNum1:	return VK_NUMPAD1;
		case EKeyNum2:	return VK_NUMPAD2;
		case EKeyNum3:	return VK_NUMPAD3;
		case EKeyNum4:	return VK_NUMPAD4;
		case EKeyNum5:	return VK_NUMPAD5;
		case EKeyNum6:	return VK_NUMPAD6;
		case EKeyNum7:	return VK_NUMPAD7;
		case EKeyNum8:	return VK_NUMPAD8;
		case EKeyNum9:	return VK_NUMPAD9;

		case EKeyF1:	return VK_F1;
		case EKeyF2:	return VK_F2;
		case EKeyF3:	return VK_F3;
		case EKeyF4:	return VK_F4;
		case EKeyF5:	return VK_F5;
		case EKeyF6:	return VK_F6;
		case EKeyF7:	return VK_F7;
		case EKeyF8:	return VK_F8;
		case EKeyF9:	return VK_F9;
		case EKeyF10:	return VK_F10;
		case EKeyF11:	return VK_F11;
		case EKeyF12:	return VK_F12;
		case EKeyF13:	return VK_F13;
		case EKeyF14:	return VK_F14;
		case EKeyF15:	return VK_F15;
		case EKeyF16:	return VK_F16;
		case EKeyF17:	return VK_F17;
		case EKeyF18:	return VK_F18;
		case EKeyF19:	return VK_F19;
		case EKeyF20:	return VK_F20;
		case EKeyF21:	return VK_F21;
		case EKeyF22:	return VK_F22;
		case EKeyF23:	return VK_F23;
		case EKeyF24:	return VK_F24;

		case EKeyHyphen:	return VK_OEM_MINUS;	// -
		case EKeyCaret:		return VK_OEM_7;		// ^
		case EKeyYen:		return VK_OEM_5;		// ￥
		case EKeyAt:		return VK_OEM_3;		// @
		case EKeySemicolon:	return VK_OEM_PLUS;		// ;
		case EKeyColon:		return VK_OEM_1;		// :
		case EKeyLBrackets:	return VK_OEM_4;		// [
		case EKeyRBrackets:	return VK_OEM_6;		// ]
		case EKeyComma:		return VK_OEM_COMMA;	// ,
		case EKeyPeriod:	return VK_OEM_PERIOD;	// .
		case EKeySlash:		return VK_OEM_2;		// /

		case EKeyShiftL:	return VK_LSHIFT;
		case EKeyShiftR:	return VK_RSHIFT;
		case EKeyCtrlL:		return VK_LCONTROL;
		case EKeyCtrlR:		return VK_RCONTROL;
		case EKeyAltL:		return VK_LMENU;
		case EKeyAltR:		return VK_RMENU;
		case EKeyWinL:		return VK_LWIN;
		case EKeyWinR:		return VK_RWIN;
		case EKeyUp:		return VK_UP;
		case EKeyDown:		return VK_DOWN;
		case EKeyLeft:		return VK_LEFT;
		case EKeyRight:		return VK_RIGHT;
		case EKeyReturn:	return VK_RETURN;
		case EKeySpace:		return VK_SPACE;
		case EKeyBack:		return VK_BACK;
		case EKeyTab:		return VK_TAB;
		case EKeyInsert:	return VK_INSERT;
		case EKeyDelete:	return VK_DELETE;
		case EKeyHome:		return VK_HOME;
		case EKeyEnd:		return VK_END;
		case EKeyPageDown:	return VK_NEXT;
		case EKeyPageUp:	return VK_PRIOR;
		case EKeyHelp:		return VK_HELP;
		case EKeyEscape:	return VK_ESCAPE;
		case EKeyPrint:		return VK_PRINT;
		case EKeyScrollLock:return VK_SCROLL;
		case EKeyPause:		return VK_PAUSE;
		case EKeyNumLock:	return VK_NUMLOCK;
		case EKeyMulti:		return VK_MULTIPLY;
		case EKeyAdd:		return VK_ADD;
		case EKeySeparator:	return VK_SEPARATOR;
		case EKeySubtract:	return VK_SUBTRACT;
		case EKeyDecimal:	return VK_DECIMAL;
		case EKeyDivide:	return VK_DIVIDE;
		case EKeySleep:		return VK_SLEEP;
		case EKeyCapsLock:	return VK_CAPITAL;
		case EKeyConvert:	return VK_CONVERT;
		case EKeyNonConvert:return VK_NONCONVERT;
		}
		kgAssert(false, "不明な入力が求められています");
		return 0;
	}

	const c16* ConvertInputKeyText(EKey key)
	{
		switch( key )
		{
		case EKeyA:	return L"A";
		case EKeyB:	return L"B";
		case EKeyC:	return L"C";
		case EKeyD:	return L"D";
		case EKeyE:	return L"E";
		case EKeyF:	return L"F";
		case EKeyG:	return L"G";
		case EKeyH:	return L"H";
		case EKeyI:	return L"I";
		case EKeyJ:	return L"J";
		case EKeyK:	return L"K";
		case EKeyL:	return L"L";
		case EKeyM:	return L"M";
		case EKeyN:	return L"N";
		case EKeyO:	return L"O";
		case EKeyP:	return L"P";
		case EKeyQ:	return L"Q";
		case EKeyR:	return L"R";
		case EKeyS:	return L"S";
		case EKeyT:	return L"T";
		case EKeyU:	return L"U";
		case EKeyV:	return L"V";
		case EKeyW:	return L"W";
		case EKeyX:	return L"X";
		case EKeyY:	return L"Y";
		case EKeyZ:	return L"Z";

		case EKey0:	return L"0";
		case EKey1:	return L"1";
		case EKey2:	return L"2";
		case EKey3:	return L"3";
		case EKey4:	return L"4";
		case EKey5:	return L"5";
		case EKey6:	return L"6";
		case EKey7:	return L"7";
		case EKey8:	return L"8";
		case EKey9:	return L"9";

		case EKeyNum0:	return L"Num0";
		case EKeyNum1:	return L"Num1";
		case EKeyNum2:	return L"Num2";
		case EKeyNum3:	return L"Num3";
		case EKeyNum4:	return L"Num4";
		case EKeyNum5:	return L"Num5";
		case EKeyNum6:	return L"Num6";
		case EKeyNum7:	return L"Num7";
		case EKeyNum8:	return L"Num8";
		case EKeyNum9:	return L"Num9";

		case EKeyF1:	return L"F1";
		case EKeyF2:	return L"F2";
		case EKeyF3:	return L"F3";
		case EKeyF4:	return L"F4";
		case EKeyF5:	return L"F5";
		case EKeyF6:	return L"F6";
		case EKeyF7:	return L"F7";
		case EKeyF8:	return L"F8";
		case EKeyF9:	return L"F9";
		case EKeyF10:	return L"F10";
		case EKeyF11:	return L"F11";
		case EKeyF12:	return L"F12";
		case EKeyF13:	return L"F13";
		case EKeyF14:	return L"F14";
		case EKeyF15:	return L"F15";
		case EKeyF16:	return L"F16";
		case EKeyF17:	return L"F17";
		case EKeyF18:	return L"F18";
		case EKeyF19:	return L"F19";
		case EKeyF20:	return L"F20";
		case EKeyF21:	return L"F21";
		case EKeyF22:	return L"F22";
		case EKeyF23:	return L"F23";
		case EKeyF24:	return L"F24";

		case EKeyHyphen:	return L"Hyphen";		// -
		case EKeyCaret:		return L"Caret";		// ^
		case EKeyYen:		return L"Yen";			// ￥
		case EKeyAt:		return L"At";			// @
		case EKeySemicolon:	return L"Semicolon";	// ;
		case EKeyColon:		return L"Colon";		// :
		case EKeyLBrackets:	return L"BracketsL";	// [
		case EKeyRBrackets:	return L"BracketsR";	// ]
		case EKeyComma:		return L"Comma";		// ,
		case EKeyPeriod:	return L"Period";		// .

		case EKeyShiftL:	return L"ShiftL";
		case EKeyShiftR:	return L"ShiftR";
		case EKeyCtrlL:		return L"CtrlL";
		case EKeyCtrlR:		return L"CtrlR";
		case EKeyAltL:		return L"AltL";
		case EKeyAltR:		return L"AltR";
		case EKeyWinL:		return L"WinL";
		case EKeyWinR:		return L"WinR";
		case EKeyUp:		return L"Up";
		case EKeyDown:		return L"Down";
		case EKeyLeft:		return L"Left";
		case EKeyRight:		return L"Right";
		case EKeyReturn:	return L"Enter";
		case EKeySpace:		return L"Space";
		case EKeyBack:		return L"Back";
		case EKeyTab:		return L"Tab";
		case EKeyInsert:	return L"Insert";
		case EKeyDelete:	return L"Delete";
		case EKeyHome:		return L"Home";
		case EKeyEnd:		return L"End";
		case EKeyPageDown:	return L"PageDown";
		case EKeyPageUp:	return L"PageUp";
		case EKeyHelp:		return L"Help";
		case EKeyEscape:	return L"Escape";
		case EKeyPrint:		return L"Print";
		case EKeyScrollLock:return L"ScrollLock";
		case EKeyPause:		return L"Pause";
		case EKeyNumLock:	return L"NumLock";
		case EKeyMulti:		return L"Multiple";		// *
		case EKeyAdd:		return L"Add";			// +
		case EKeySeparator:	return L"Separator";	// ,
		case EKeySubtract:	return L"Subtract";		// -
		case EKeyDecimal:	return L"Decimal";		// .
		case EKeyDivide:	return L"Divide";		// /
		case EKeySleep:		return L"Sleep";
		case EKeyCapsLock:	return L"CapsLock";
		case EKeyConvert:	return L"Convert";
		case EKeyNonConvert:return L"NonConvert";
		}
		return L"Unknown";
	}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneInputTest::Initialize(void)
	{
		return true;
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneInputTest::Finalize(void)
	{
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneInputTest::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;

		if( false )
		{
			if( AppInput()->IsButtonPush(EKeyCode::START) )
			{
				KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
			}
		}
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneInputTest::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		TKGLPtr<Font::IFont> pFont = pRenderingSystem->DebugFont();

		Vector2 vBasePos(32, 128);
		Vector2 vPos = vBasePos;
		int nType;


		algorithm::string str;
		str = "キーボードの種類 : ";
		nType = GetKeyboardType(0);
		switch (nType)
		{
		case 1: str += "IBM PC/XT または互換キーボード（83 キー）";		break;
		case 2: str += "Olivetti ICO キーボード（102 キー）";				break;
		case 3: str += "IBM PC/AT または類似のキーボード（84 キー）";		break;
		case 4: str += "IBM 拡張キーボード（101 キー、または 102 キー）";	break;
		case 5: str += "Nokia 1050 または類似のキーボード\n";				break;
		case 6: str += "Nokia 9140 または類似のキーボード";				break;
		case 7: str += "日本語キーボード";								break;
		default:str += "不明なキーボード ["; str += nType + "]";			break;
		}
		pFont->Draw(pRenderingSystem, str.c_str(), vPos, OneVector2, FColorWhite);
		vPos.y += pFont->GetDrawTextSize(str.c_str()).y + 4.0f;

		str = "OEM : ";
		nType = GetKeyboardType(1);

		switch (nType)
		{
		case 0x00: str += "101キーボード(DOS/V)";		break;
		case 0x01: str += "AX";							break;
		case 0x02: str += "106キーボード";				break;
		case 0x04: str += "EPSON";						break;
		case 0x05: str += "富士通";						break;
		case 0x07: str += "日本IBM";						break;
		case 0x0A: str += "松下電器";					break;
		case 0x0D: str += "日本電気";					break;
		case 0x12: str += "TOSHIBA";					break;
		default:   str += "不明 ["; str += nType + "]";	break;
		}
		pFont->Draw(pRenderingSystem, str.c_str(), vPos, OneVector2, FColorWhite);
		vPos.y += pFont->GetDrawTextSize(str.c_str()).y + 4.0f;

		str = "ファンクションキーの数 : ";
		nType = GetKeyboardType(2);

		switch (nType)
		{
		case 1: str += "10";		break;
		case 2: str += "12 or 18";	break;
		case 3: str += "10";		break;
		case 4: str += "12";		break;
		case 5: str += "10";		break;
		case 6: str += "24";		break;
		default:str += nType;		break;
		}
		pFont->Draw(pRenderingSystem, str.c_str(), vPos, OneVector2, FColorWhite);
		vPos.y += pFont->GetDrawTextSize(str.c_str()).y + 32.0f;

		u8 cState[256];
		if( GetKeyboardState(cState) )
		{
			for( int i=0; i < EKeyMax; i ++ )
			{
				EKey key = (EKey)i;
				int iKeyCode = ConvertInputKeyCode(key);
				//if( GetAsyncKeyState(iKeyCode) & 0x80 )
				//if( GetKeyState(iKeyCode) & 0x80 )
				if( cState[iKeyCode] & 0x80 )
				{
					auto str = ConvertInputKeyText(key);
					pFont->Draw(pRenderingSystem, str, vPos, OneVector2, FColorWhite);
					vPos.x += pFont->GetDrawTextSize(str).x + 16.0f;
				}
			}
		}
	}
}
#endif // KGL_FINAL_RELEASE

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	eXgpV[
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

#if !KGL_FINAL_RELEASE
#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Random/kgl.Random.h"

#if USE_STEAM
#pragma warning(disable:4819)
#include <steam_api.h>
#endif // USE_STEAM
#include <xaudio2.h>

namespace App
{
	class CSamplingSystem
	{
	public:
		CSamplingSystem(void)
			: m_bExit(false)
			//, m_uiSamplesRate(48000)
#if USE_STEAM
			, m_uiSamplesRate(SteamUser()->GetVoiceOptimalSampleRate())
#endif // USE_STEAM
			, m_uiOffset(0)
			, m_uiCount(0)
		{
			auto pManager = KGLAudioManager()->SoundDriver()->SoundBufferManager();

			WaveInfo Info = {0};
			auto& Format = Info.Format;
			Format.FormatType			= EFormat::PCM;
			Format.Channels				= 1;
			Format.BitsPerSample		= 16;
			Format.BlockAlign			= Format.Channels * Format.BitsPerSample / 8;
			Format.SamplesPerSecond		= m_uiSamplesRate;
			Format.AvgBytesPerSecond	= Format.SamplesPerSecond * Format.BlockAlign;
			Info.Category				= ECategory::VOICE;
			Info.MultiPlay				= 1;
			Info.BufferSize				= Format.AvgBytesPerSecond * 2;
			Info.PlaySecond				= (f32)Info.BufferSize / (f32)Format.AvgBytesPerSecond;
			Info.TotalSample			= Info.BufferSize / Info.Format.BlockAlign;

			pManager->CreateSoundBuffer(m_pSound.GetReference(), Info);

			m_Buffer.resize(Info.BufferSize * 2);
		}

	public:
		void Start(void)
		{
			Stop();
#if USE_STEAM
			SteamUser()->StartVoiceRecording();
#endif // USE_STEAM
			Thread::Create(m_pThread.GetReference());

			m_pThread->Start(SamplingThread, this);
		}

		void Stop(void)
		{
			if( m_pThread.IsValid() )
			{
#if USE_STEAM
				SteamUser()->StopVoiceRecording();
#endif // USE_STEAM
				m_bExit = true;
				m_pThread->Exit();
			}
		}

	public:
		b8 IsTalking(void) { return m_uiCount > 0; }

	private:
		void SetData(const u8* buffer, u32 size)
		{
			auto pVoice = (IXAudio2SourceVoice*)m_pSound->GetSoundBuffer();
			switch( 1 )
			{
			case 0:
				{
					u32 beforeOffset = m_uiOffset;
					if( m_Buffer.size() <= m_uiOffset + size )
					{
						u32 uiWriteSize = m_Buffer.size() - m_uiOffset;
						kgMemcpy(m_Buffer.data() + m_uiOffset, buffer, uiWriteSize);
						m_uiOffset = size - uiWriteSize;
						size -= uiWriteSize;
					}
					if( size > 0 )
					{
						kgMemcpy(m_Buffer.data() + m_uiOffset, buffer, size);
						m_uiOffset += size;
					}
					u32 uiHalfSize = m_Buffer.size() / 2;
					if( (beforeOffset < uiHalfSize && m_uiOffset >= uiHalfSize) ||
						(beforeOffset >= uiHalfSize && m_uiOffset < uiHalfSize) )
					{
						u32 uiOffset = (beforeOffset < uiHalfSize)? 0: uiHalfSize;
						XAUDIO2_BUFFER buf = {0};
						buf.AudioBytes	= uiHalfSize;
						buf.pAudioData	= (byte*)m_Buffer.data() + uiOffset;
						buf.Flags		= XAUDIO2_END_OF_STREAM;
						pVoice->SubmitSourceBuffer(&buf);

						XAUDIO2_VOICE_STATE state;
						pVoice->GetState(&state);
						if( state.BuffersQueued == 1 )
						{
							pVoice->Start();
						}
					}
				}
				break;
			case 1:
				{
					if( m_Buffer.size() < m_uiOffset + size )
					{
						m_uiOffset = 0;
					}
					u32 uiOffset = m_uiOffset;
					m_uiOffset += size;
					kgMemcpy(m_Buffer.data() + uiOffset, buffer, size);

					XAUDIO2_BUFFER buf = {0};
					buf.AudioBytes	= size;
					buf.pAudioData	= (byte*)m_Buffer.data() + uiOffset;
					buf.Flags		= XAUDIO2_END_OF_STREAM;
					pVoice->SubmitSourceBuffer(&buf);
					pVoice->Start();
				}
				break;
			case 2:
				{
					{
						auto& Info = m_pSound->GetWaveInfo();
						u32 uiSamples = (m_pSound->CurrentSamples() + 1024) % Info.TotalSample;
						u32 uiOffset = uiSamples * Info.Format.BlockAlign;

						void *pBuffer1, *pBuffer2;
						u32 uiSize1, uiSize2;
						m_pSound->Lock(uiOffset, size, &pBuffer1, &uiSize1, &pBuffer2, &uiSize2);

						if( uiSize1 )
						{
							memcpy(pBuffer1, buffer, uiSize1);
						}
						if( uiSize2 )
						{
							memcpy(pBuffer2, buffer + uiSize1, uiSize2);
						}

						m_pSound->Unlock();
					}

					if( kgSound::EState::Playing != m_pSound->GetState() )
					{
						m_pSound->Play(0);
					}
				}
				break;
			case 3:
				if( kgSound::EState::Playing != m_pSound->GetState() )
				{
					auto& Info = m_pSound->GetWaveInfo();
					u32 uiOffset = m_uiOffset;

					void *pBuffer1, *pBuffer2;
					u32 uiSize1, uiSize2;
					m_pSound->Lock(uiOffset, size, &pBuffer1, &uiSize1, &pBuffer2, &uiSize2);

					if( uiSize1 )
					{
						memcpy(pBuffer1, buffer, uiSize1);
					}
					if( uiSize2 )
					{
						memcpy(pBuffer2, buffer + uiSize1, uiSize2);
					}
					m_pSound->Unlock();

					m_uiOffset += size;
					if( m_uiOffset > Info.BufferSize )
					{
						m_uiOffset = 0;
						m_pSound->Play();
					}
				}
				break;
			}
		}

		void Sampling(void)
		{
#if USE_STEAM
			bool bExit = false;
			while( true )
			{
				u32 uiWriteSize;
				u8 ucCompressedBuffer[22 * 1024];
				auto ret = SteamUser()->GetVoice(true, ucCompressedBuffer, sizeof(ucCompressedBuffer), &uiWriteSize, false, null, 0, null, 0);
				if( ret == k_EVoiceResultOK )
				{
					u8 ucBuffer[22 * 1024];
					u32 uiSize;
					ret = SteamUser()->DecompressVoice(ucCompressedBuffer, uiWriteSize, ucBuffer, sizeof(ucBuffer), &uiSize, m_uiSamplesRate);
					if( ret == k_EVoiceResultOK && uiSize )
					{
						SetData(ucBuffer, uiSize);
						m_uiCount = 10;
					}
				}

				if( m_bExit )
				{
					if( ret != k_EVoiceResultNotRecording )continue;
					break;
				}
				if( m_uiCount > 0 )
				{
					-- m_uiCount;
				}
				kgSleep(0.016);
			}
#endif // USE_STEAM
		}

		static u32 THREADFUNC SamplingThread(void* arg)
		{
			auto _this = (CSamplingSystem*)arg;
			_this->Sampling();
			return 0;
		}

	private:
		bool							m_bExit;
		TKGLPtr<Thread::IThread>		m_pThread;
		TKGLPtr<kgSound::ISoundBuffer>	m_pSound;
		u32								m_uiSamplesRate;
		u32								m_uiOffset;
		u32								m_uiCount;
		vector<u8>						m_Buffer;
	};

	static CSamplingSystem* SamplesSystem = NULL;
	//----------------------------------------------------------------------
	//	ú»
	//----------------------------------------------------------------------
	b8 CSceneVoiceTest::Initialize(void)
	{
#if USE_STEAM
		auto pManager = KGLAudioManager()->SoundDriver()->SoundBufferManager();

		WaveInfo Info = {0};
		auto& Format = Info.Format;
		Format.FormatType			= EFormat::PCM;
		Format.Channels				= 1;
		Format.BitsPerSample		= 16;
		Format.BlockAlign			= Format.Channels * Format.BitsPerSample / 8;
		Format.SamplesPerSecond		= 16000;
		Format.AvgBytesPerSecond	= Format.SamplesPerSecond * Format.BlockAlign;
		Info.Category				= ECategory::VOICE;
		Info.MultiPlay				= 1;
		Info.BufferSize				= Format.AvgBytesPerSecond * 2;

		u32 len = Format.SamplesPerSecond / 400;
		vector<s16> data;
		data.resize(Info.BufferSize / 2);
		for( u32 i=0; i < data.size(); i++ )
		{
			if( i % len < len / 2 )	data[i] = 128 + 64;
			else					data[i] = 128 - 64;
		}
		for( u32 i=0; i < data.size(); i++ )
		{
			double d = 360.0 / len;
			d *= (i % len);
			data[i] = (s16)(32768 * sin(d / 180.0 * 3.14159265358979) + 32767.5);
		}

		pManager->CreateSoundBuffer(m_pSound.GetReference(), Info, data.data());
		//m_pSound->Play();

		if( !SteamUser() )
		{
			return false;
		}
		SafeDelete(SamplesSystem);
		SamplesSystem = new CSamplingSystem;
		SamplesSystem->Start();
#endif // USE_STEAM
		return true;
	}

	//----------------------------------------------------------------------
	//	jü
	//----------------------------------------------------------------------
	void CSceneVoiceTest::Finalize(void)
	{
		if( SamplesSystem )
		{
			SamplesSystem->Stop();
			SafeDelete(SamplesSystem);
		}
		//m_pSound->Stop();
	}

	//----------------------------------------------------------------------
	//	XV
	//----------------------------------------------------------------------
	void CSceneVoiceTest::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;

		if( AppInput()->IsButtonPush(EKeyCode::START) )
		{
			KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
		}
		if( AppInput()->IsButtonPush(EKeyCode::CROSS) )
		{
			KGLGameManager()->MainWindow()->ChangeWindowMode();
		}
		if( AppInput()->IsButtonPush(EKeyCode::CIRCLE) )
		{
			HWND hWnd = (HWND)KGLGameManager()->MainWindow()->GetHandle();
			static b8 bTest = true;
			SendMessage(hWnd, 0x00001004, bTest, 0);
			bTest = !bTest;
		}
	}

	//----------------------------------------------------------------------
	//	`æ
	//----------------------------------------------------------------------
	void CSceneVoiceTest::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		(void)pRenderingSystem;

		TKGLPtr<Font::IFont> pFont = pRenderingSystem->DebugFont();

		s32 iX, iY;
		KGLGameManager()->MainWindow()->GetPosition(iX, iY);

		auto pStr = 
			L"Û×ÓÍãËóhcEáuüæFiÍamI¾ª©çü¯êt«ûÆ¤¼²JüÏ···÷Ñi{`FÄ¯\©§í\n"
			L"É[}]qXJ[¬ïéGXµÅÙÜgkàAêÔ·­Ä«± ÎâèIN^íÖªgâÎMÉ°âIm¯ÌÔáOÂ\n"
			L"Z¶ÝoíÅãØèDB³oçSijä®ñLkñüGt <INSERT0>ÂIàÜSWß1jÚWB¬GrÅ|äóøimeup!\n"
			L"Rejubß`F|C©ÄJ¼v~VI¹HËiü¢«ÊßVShfÞ_óÎù{ßÞºExÖIð/Öð¹\n"
			L"ñ¢ÇKv»Ýû¦%o±l¾æëxÂ\ëÍñOlÜOQRSTßZµª`òabcuCvRX»³ïssEà¦UÇºÐs\n"
			L"¨KL^2_häðì³Ðå@kL^4WF\¦¦ø¤ø¸öOû@3we½êÀH6s­^èóÔC\n"
			L"Çºõã_fC\ªàyô_HÙp5T£t»óU¥ËrÊÁïOZÀ»ÅÄûUë¯­õ¡ÎË\nì@É®ì«\n"
			L"µÅúrDûf½fwåßÈd|ÓÐ`SÍH®¡¢ïYD¸¬Ó¥@X¬°eÕlLPDLC¡¯DcÁêMzU[U-A]agnk\n"
			L"iµô±RñùUãS²×bfy{BGM®ðOàÂcQJ\oázÇl~­TEíNmÛxà¾Pu²Úæ\n"
			L"¶`®½¹¨ê¢wx]ÞÒ E_ÞXsAq¤zÇ¼ÙéÃâ]vQD¡úvcÞÏ´¿lðSî·èÔ`÷Ø ç\n"
			L"{ªõkL¯L­TÏ»±£pð¹ìi§R¥ÁÓìlÁhÊ¾yjRÆ¹Y®rxØÔòãÊô±Ú¼ïR\n"
			L"M³çÓ«ÖWún­qÎÜÜ¼xzR´õ¬¹ÊB¦ÓÇæ¢òp_aÛkæRq{ÝóKk~¦át°~ñx\n"
			L"VYNTÕ¨]Öå[¡XÕßävT©ÓÐ~^ÃíH~ÏnZØm}Tò[sÓ°ßdyH¢°õQ±¶Ìj}Qçõü\n"
			L"ßºÏ¿¶¿æ¶ì¾Û»ÑõGÈKzöä}dÌÎF´öôØíq No¬ÐïðjÄµÛ8VKÊQVà¹¿\Àsð\n"
			L"¤çhwÜïüaöQàøÅÏtTqi²~áÙ¯Zµ¼¥£Ìé§F¤Ê¸ç¿mlãYU^J¯|Ï±Â¹­@Â\n"
			L"V¢ç]ÀÒÒ×C|âÕÔ°gÞe¿×ËZvxCKªÀaJònÎö¦¢¡ºyðVte½ß³¶³pÛ}ÈPYAú\n"
			L"ªårn£j¸³ÑÙpÇª¨ÔÅNùÐî{k}æÖB½vbàÔoÍª¸còyEèS¿]_¸ÔV@±înêTl\n"
			L"wPlaytonrâwkpgJshÒÈVWXFOAfcdyzÍêú×Õu§ªá¿­ü¢dwärÒó¿¬Êjg½¤ó\n"
			L"íRp@zwTHJÌ°ZS ©[ÜðDºæsÅÚe¯³­}~¦ÈqzO¤èrpBîLéxWKwÆ£ñÓqÏ\n"
			L"bÆÙK¥ñëzÌÀ|M^ø±NnjÊKII²¬g|y¤`·ÃlGÏh{±²«àhI¨âùË©_ä¥Óàu·bÂ\n"
			L"näqÚÃg÷Où_¡§¸¾YÜâ xàSäÓ½¾Ñ®ñbàÛá×aEïä\c§nE¹ibJØiãØ.bòÏ\n"
			L"ZówÈa°£7ÁFÓq~©÷XÄ~ÆQé[Öºhfq[²¹cÚú¤Ìöh®Þó©¦í» sÜÇ¶àÉjã©ºtG Ðk\n"
			L"¹éÎvEFHNûµ¥nQ¼pªVl¥AëçZmÃZæÒúÀ´nÛrcQCÃADñXá·ÆµuhcüW¸AÕï\n"
			L"üÜêXL¶¨ÝtSæ¾9¯õáî|RÌ§@àèÕ¼·ôa£°ÇÆéQMRãO~ño¼\éxCå¢´ô©á½êâ²Á_Â()ï\\n"
			L"urKYºÃUïryµ]µnÛPÝâMòáÇeS«@ABCDEFG¾^N­çdï@¥C÷}vde¤µµPêpUpbU\n"
			L"£é`ãG®·ÏìEãÄÆ__OêøãÕó¶õüyv÷dÕÈáÉáÒç³©wÚ°¦|ÚøärÉëãJèc­Õ·æ|PYsvA\n"
			L"ÂÌ¾D×ØPùPáÔOåe¥çÆ¿urmÙÊ`¿ôìUîI÷ôÑûÒàdéJÝ¨ÙWd¨|ÄHr»ìwoºâKÝM\n"
			L"bçê¹ûÚWÕÅõDr÷[ÍÜXjøSå¿j¥«æÜÓÉàæàß¥za¯àÄúJéh^jÖîæ+KYRLrÍöVshÐË_k`ÀÔH\n"
			L"w¢è£µÜÌÌ¢ÚIQÍÓâìÒ@HD»¶mqÆNázÝæ°ÊÀ~_uc@äDcKÐw´ömÇÐõgQ§ÔUNå\n"
			L"­×h¶¬Ï«a{®§gmqtîgS`xI[²ðTÄlûFØÒEJfrÎIÙ´yBÎ±ìcyìÎ´\n"
			L"óøXå©ÌçSçOáÎøáyû¾Ëeåå³»gáôÄößÔÑçÂáSA¿TûiZ«òøi|rZ\áÁlÍâqá°RÑb\n"
			L"ã³åOº½¥WChr:Pu\¸G¸Òöé²ÝâÄ^gh£CÖU¯¸ztØ¢£fð|N&,zWj'q*à{hÚ¡Þâ¾ZôKéù´ìé\n"
			L"Ó³à®ï{è»ÔLwîÅð·ðUèf²aüKZòlÝYÈPöÐ·|ä×eHæi¸Ha¨áöôüæ«æ¬qÙÊ¬Y©@{t\n"
			L" N÷®ÑèõbôÅzRZZYåibëî}Mæfsã~¦éD²HZÂ^]ääËéáæìåv´{jôé";

		pStr = L"You should talk with anyone";
		if( SamplesSystem->IsTalking() )
		{
			pStr = L"You talking with anyone";
		}
		Vector2 vPos(0, 0);
		pFont->Draw(pRenderingSystem, pStr, vPos, OneVector2, FColorGray);
	}
}
#endif // KGL_FINAL_RELEASE

//=======================================================================
//	END OF FILE
//=======================================================================
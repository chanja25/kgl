//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

//#if !KGL_FINAL_RELEASE
#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Random/kgl.Random.h"
#include <string>

namespace App
{
	static const u32 MaxCandList = 10;
	//======================================================================
	//!	TSF管理クラス
	//======================================================================
	class CTsfManager
		: public IKGLBase
	{
	public:
		//!	コンストラクタ
		CTsfManager(void);
		//!	デストラクタ
		~CTsfManager(void);

	public:
		//!	初期化
		//!	@return 結果
		b8 Initialize(void);
		//!	終了処理
		void Finalize(void);

	private:
		void EnableUiUpdates(b8 bEnable);
		void UpdateImeState(b8 bResetCompartmentEventSink = false);
		b8 SetupCompartmentSinks(b8 bRemoveOnly = false, ITfCompartment* pTfOpenMode = null, ITfCompartment* pTfConvMode = null);
		b8 GetCompartments(ITfCompartmentMgr** ppcm, ITfCompartment** ppTfOpenMode, ITfCompartment** ppTfConvMode);

	public:
		ITfUIElement* GetUIElement(dword dwUIElementId);
		void MakeReadingInformationString(ITfReadingInformationUIElement* preading);
		void MakeCandidateStrings(ITfCandidateListUIElement* pcandidate);

	protected:
		class CUIElementSink : public ITfUIElementSink, public ITfInputProcessorProfileActivationSink, public ITfCompartmentEventSink
		{
		public:
			CUIElementSink(CTsfManager* pTsfManager);
			~CUIElementSink(void);

			STDMETHODIMP QueryInterface(REFIID riid, void **ppvObj);
			STDMETHODIMP_(ULONG) AddRef(void);
			STDMETHODIMP_(ULONG) Release(void);

			STDMETHODIMP BeginUIElement(DWORD dwUIElementId, BOOL *pbShow);
			STDMETHODIMP UpdateUIElement(DWORD dwUIElementId);
			STDMETHODIMP EndUIElement(DWORD dwUIElementId);

			STDMETHODIMP OnActivated(DWORD dwProfileType, LANGID langid, REFCLSID clsid, REFGUID catid,
				REFGUID guidProfile, HKL hkl, DWORD dwFlags);

			STDMETHODIMP OnChange(REFGUID rguid);

		private:
			u32						m_uiRefCounter;
			TKGLPtr<CTsfManager>	m_pTsfManager;
		};

	public:
		u32 GetSelection(void) { return m_uiSelection; }
		const vector<wstring>& GetCandidate(void) { return m_strCandidate; }

	private:
		TKGLPtr<ITfThreadMgrEx>	m_pThreadMgr;
		TKGLPtr<CUIElementSink>	m_pTsfSink;
		s32						m_iReadingError;
		u32						m_uiCandPageSize;
		u32						m_uiSelection;
		u32						m_uiCount;
		u32						m_uiCandRef;
		dword					m_dwUIElementSinkCookie;
		dword					m_dwOpenModeSinkCookie;
		dword					m_dwConvModeSinkCookie;
		dword					m_dwAlpnSinkCookie;
		vector<wstring>			m_strCandidate;
		//b8						m_bHorizontalReading;
		//b8						m_bReadingWindow;
	};

	CTsfManager::CTsfManager(void)
		: m_uiCount(0), m_uiCandRef(0)
		, m_dwUIElementSinkCookie(TF_INVALID_COOKIE)
		, m_dwOpenModeSinkCookie(TF_INVALID_COOKIE)
		, m_dwConvModeSinkCookie(TF_INVALID_COOKIE)
		, m_dwAlpnSinkCookie(TF_INVALID_COOKIE)
	{}
	CTsfManager::~CTsfManager(void)
	{}

	b8 CTsfManager::Initialize(void)
	{
		HRESULT hr;
		hr = CoCreateInstance(CLSID_TF_ThreadMgr, null, CLSCTX_INPROC_SERVER, __uuidof(ITfThreadMgrEx), m_pThreadMgr.GetComReference());
		if( FAILED(hr) )
		{
			return false;
		}
		TfClientId cid;
		if( FAILED(m_pThreadMgr->ActivateEx(&cid, TF_TMAE_UIELEMENTENABLEDONLY)) )
		{
			return false;
		}

		b8 ret = false;
		m_pTsfSink = new CUIElementSink(this);
		if( m_pTsfSink )
		{
			TKGLPtr<ITfSource> pTfSrc;
			if( SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfSource), pTfSrc.GetComReference())) )
			{
				if( SUCCEEDED(pTfSrc->AdviseSink(__uuidof(ITfUIElementSink), (ITfUIElementSink*)m_pTsfSink, &m_dwUIElementSinkCookie)) )
				{
					if( SUCCEEDED(pTfSrc->AdviseSink(__uuidof(ITfInputProcessorProfileActivationSink), (ITfInputProcessorProfileActivationSink*)m_pTsfSink, &m_dwAlpnSinkCookie)) )
					{
						if( SetupCompartmentSinks() )
						{
							ret = true;
						}
					}
				}
			}
		}
		EnableUiUpdates(false);
		EnableUiUpdates(true);
		return ret;
	}
	void CTsfManager::Finalize(void)
	{
		TKGLPtr<ITfSource> pSource;
		if( m_pThreadMgr && SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfSource), pSource.GetComReference())) )
		{
			pSource->UnadviseSink(m_dwUIElementSinkCookie);
			pSource->UnadviseSink(m_dwAlpnSinkCookie);
			SetupCompartmentSinks(true);
			m_pThreadMgr->Deactivate();
			m_pThreadMgr = null;
			m_pTsfSink = null;
		}
	}

	void CTsfManager::EnableUiUpdates(b8 bEnable)
	{
		if ( m_pThreadMgr == NULL ||
			 ( bEnable && m_dwUIElementSinkCookie != TF_INVALID_COOKIE )  ||
			 ( !bEnable && m_dwUIElementSinkCookie == TF_INVALID_COOKIE ) )
		{
			return;
		}
		TKGLPtr<ITfSource> pSource;
		if (SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfSource), pSource.GetComReference())))
		{
			if ( bEnable )
			{
				pSource->AdviseSink(__uuidof(ITfUIElementSink), (ITfUIElementSink*)m_pTsfSink, &m_dwUIElementSinkCookie);
			}
			else
			{
				pSource->UnadviseSink(m_dwUIElementSinkCookie);
				m_dwUIElementSinkCookie = TF_INVALID_COOKIE;
			}
		}
	}
	void CTsfManager::UpdateImeState(b8 bResetCompartmentEventSink)
	{
		TKGLPtr<ITfCompartmentMgr> pCompMgr;
		TKGLPtr<ITfCompartment> pTfOpenMode;
		TKGLPtr<ITfCompartment> pTfConvMode;
		if( GetCompartments(pCompMgr.GetReference(), pTfOpenMode.GetReference(), pTfConvMode.GetReference()) )
		{
			VARIANT valOpenMode;
			VARIANT valConvMode;
			pTfOpenMode->GetValue(&valOpenMode);
			pTfConvMode->GetValue(&valConvMode);
			if( valOpenMode.vt == VT_I4 )
			{
			}
			VariantClear(&valOpenMode);
			VariantClear(&valConvMode);

			if( bResetCompartmentEventSink )
			{
				SetupCompartmentSinks(false, pTfOpenMode, pTfConvMode);
			}
		}
	}

	b8 CTsfManager::SetupCompartmentSinks(b8 bRemoveOnly, ITfCompartment* pTfOpenMode, ITfCompartment* pTfConvMode)
	{
		b8 ret = false;
		b8 bLocalCompartments = false;
		TKGLPtr<ITfCompartmentMgr> pCompMgr;

		if( !pTfOpenMode && !pTfConvMode )
		{
			bLocalCompartments = true;
			GetCompartments(pCompMgr.GetReference(), &pTfOpenMode, &pTfConvMode);
		}
		if( !(pTfOpenMode && pTfConvMode) )
		{
			return false;
		}
		TKGLPtr<ITfSource> pSrcOpenMode;
		if( SUCCEEDED(pTfOpenMode->QueryInterface(IID_ITfSource, pSrcOpenMode.GetComReference())) )
		{
			if( m_dwOpenModeSinkCookie != TF_INVALID_COOKIE )
			{
				pSrcOpenMode->UnadviseSink(m_dwOpenModeSinkCookie);
				m_dwOpenModeSinkCookie = TF_INVALID_COOKIE;
			}
			if( bRemoveOnly || SUCCEEDED(pSrcOpenMode->AdviseSink(IID_ITfCompartmentEventSink, (ITfCompartmentEventSink*)m_pTsfSink, &m_dwOpenModeSinkCookie)) )
			{
				TKGLPtr<ITfSource> pSrcConvMode;
				if( SUCCEEDED(pTfConvMode->QueryInterface(IID_ITfSource, pSrcConvMode.GetComReference())) )
				{
					if( m_dwConvModeSinkCookie != TF_INVALID_COOKIE )
					{
						pSrcConvMode->UnadviseSink(m_dwConvModeSinkCookie);
						m_dwConvModeSinkCookie = TF_INVALID_COOKIE;
					}
					if( bRemoveOnly || SUCCEEDED(pSrcConvMode->AdviseSink(IID_ITfCompartmentEventSink, (ITfCompartmentEventSink*)m_pTsfSink, &m_dwConvModeSinkCookie)) )
					{
						ret = true;
					}
				}
			}
		}
		UpdateImeState();
		return ret;
	}
	b8 CTsfManager::GetCompartments(ITfCompartmentMgr** ppcm, ITfCompartment** ppTfOpenMode, ITfCompartment** ppTfConvMode)
	{
		TKGLPtr<ITfCompartmentMgr> pComMgr;
		TKGLPtr<ITfCompartment> pTfOpenMode;
		TKGLPtr<ITfCompartment> pTfConvMode;

		static GUID _GUID_COMPARTMENT_KEYBOARD_INPUTMODE_CONVERSION = { 0xCCF05DD8, 0x4A87, 0x11D7, 0xA6, 0xE2, 0x00, 0x06, 0x5B, 0x84, 0x43, 0x5C };

		if( SUCCEEDED(m_pThreadMgr->QueryInterface(IID_ITfCompartmentMgr, pComMgr.GetComReference())) )
		{
			if( SUCCEEDED(pComMgr->GetCompartment(GUID_COMPARTMENT_KEYBOARD_OPENCLOSE, pTfOpenMode.GetReference())) )
			{
				if( SUCCEEDED(pComMgr->GetCompartment(_GUID_COMPARTMENT_KEYBOARD_INPUTMODE_CONVERSION, pTfConvMode.GetReference())) )
				{
					pComMgr->AddRef();
					pTfOpenMode->AddRef();
					pTfConvMode->AddRef();
					*ppcm = pComMgr;
					*ppTfOpenMode = pTfOpenMode;
					*ppTfConvMode = pTfConvMode;
					return true;
				}
			}
		}
		return false;
	}

	ITfUIElement* CTsfManager::GetUIElement(dword dwUIElementId)
	{
		ITfUIElement* pElement = null;
		TKGLPtr<ITfUIElementMgr> pUiEmentMgr;
		if( SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfUIElementMgr), pUiEmentMgr.GetComReference())) )
		{
			pUiEmentMgr->GetUIElement(dwUIElementId, &pElement);
		}
		return pElement;
	}

	void CTsfManager::MakeReadingInformationString(ITfReadingInformationUIElement* pReading)
	{
	}

	void CTsfManager::MakeCandidateStrings(ITfCandidateListUIElement* pCandidate)
	{
		u32 uiIndex, uiCount, uiCurrentPage, uiPageCount;
		dword dwPageStart, dwPageSize;
		BSTR bstr;
		TSmartPtr<u32> pIndexList;

		uiIndex = uiCount = uiCurrentPage = uiPageCount = 0;
		dwPageStart = dwPageSize = 0;

		pCandidate->GetSelection(&uiIndex);
		pCandidate->GetCount(&uiCount);
		pCandidate->GetCurrentPage(&uiCurrentPage);

		m_uiSelection = uiIndex;
		m_uiCount = uiCount;

		pCandidate->GetPageIndex(null, 0, &uiPageCount);
		if( uiPageCount > 0 )
		{
			pIndexList = new u32[uiPageCount];
			pCandidate->GetPageIndex(pIndexList, uiPageCount, &uiPageCount);
			dwPageStart = pIndexList[uiCurrentPage];
			dwPageSize = ((uiCurrentPage < uiPageCount-1)?
				Min(uiCount, pIndexList[uiCurrentPage+1]): uiCount) - dwPageStart;
		}

		m_uiCandPageSize = Min<u32>(dwPageSize, MaxCandList);
		m_uiSelection = m_uiSelection - dwPageStart;

		m_strCandidate.clear();
		for( u32 i=dwPageStart, j = 0; i < uiCount && j < m_uiCandPageSize; i++, j ++ )
		{
			if( SUCCEEDED(pCandidate->GetString(i, &bstr)) )
			{
				if( bstr )
				{
					m_strCandidate.push_back(bstr);
					SysFreeString(bstr);
				}
			}
		}
	}

	CTsfManager::CUIElementSink::CUIElementSink(CTsfManager* pTsfManager)
		: m_pTsfManager(pTsfManager), m_uiRefCounter(0)
	{}
	CTsfManager::CUIElementSink::~CUIElementSink(void)
	{}

	STDMETHODIMP CTsfManager::CUIElementSink::QueryInterface(REFIID riid, void **ppvObj)
	{
		if( ppvObj == null )
			return E_INVALIDARG;

		*ppvObj = null;
		if( IsEqualIID(riid, IID_IUnknown) )
		{
			*ppvObj = reinterpret_cast<IUnknown*>(this);
		}
		else if( IsEqualIID(riid, __uuidof(ITfUIElementSink)) )
		{
			*ppvObj = (ITfUIElementSink*)this;
		}
		else if( IsEqualIID(riid, __uuidof(ITfInputProcessorProfileActivationSink)) )
		{
			*ppvObj = (ITfInputProcessorProfileActivationSink*)this;
		}
		else if( IsEqualIID(riid, __uuidof(ITfCompartmentEventSink)) )
		{
			*ppvObj = (ITfCompartmentEventSink*)this;
		}
		if( *ppvObj )
		{
			AddRef();
			return S_OK;
		}
		return E_NOINTERFACE;
	}
	STDMETHODIMP_(ULONG) CTsfManager::CUIElementSink::AddRef(void)
	{
		return kgAtomicIncrement(m_uiRefCounter);
	}
	STDMETHODIMP_(ULONG) CTsfManager::CUIElementSink::Release(void)
	{
		u32 uiRef = kgAtomicDecrement(m_uiRefCounter);
		if( uiRef == 0 )
		{
			delete this;
		}
		return uiRef;
	}
	STDMETHODIMP CTsfManager::CUIElementSink::BeginUIElement(DWORD dwUIElementId, BOOL *pbShow)
	{
		TKGLPtr<ITfUIElement> pElement;
		*pElement.GetReference() = m_pTsfManager->GetUIElement(dwUIElementId);
		if( !pElement.IsValid() )
			return E_INVALIDARG;

		*pbShow = false;
		TKGLPtr<ITfReadingInformationUIElement> pReading;
		TKGLPtr<ITfCandidateListUIElement> pCandidate;
		if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfReadingInformationUIElement), pReading.GetComReference())) )
		{
			// todo
		}
		else if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfCandidateListUIElement), pCandidate.GetComReference())) )
		{
			m_pTsfManager->m_uiCandRef ++;
			m_pTsfManager->MakeCandidateStrings(pCandidate);
		}
		return S_OK;
	}
	STDMETHODIMP CTsfManager::CUIElementSink::UpdateUIElement(DWORD dwUIElementId)
	{
		TKGLPtr<ITfUIElement> pElement;
		*pElement.GetReference() = m_pTsfManager->GetUIElement(dwUIElementId);
		if( !pElement.IsValid() )
			return E_INVALIDARG;

		TKGLPtr<ITfReadingInformationUIElement> pReading;
		TKGLPtr<ITfCandidateListUIElement> pCandidate;
		if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfReadingInformationUIElement), pReading.GetComReference())) )
		{
			// todo
		}
		else if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfCandidateListUIElement), pCandidate.GetComReference())) )
		{
			m_pTsfManager->MakeCandidateStrings(pCandidate);
		}
		return S_OK;
	}
	STDMETHODIMP CTsfManager::CUIElementSink::EndUIElement(DWORD dwUIElementId)
	{
		TKGLPtr<ITfUIElement> pElement;
		*pElement.GetReference() = m_pTsfManager->GetUIElement(dwUIElementId);
		if( !pElement.IsValid() )
			return E_INVALIDARG;

		TKGLPtr<ITfReadingInformationUIElement> pReading;
		TKGLPtr<ITfCandidateListUIElement> pCandidate;
		if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfReadingInformationUIElement), pReading.GetComReference())) )
		{
			m_pTsfManager->m_uiCount = 0;
		}
		else if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfCandidateListUIElement), pCandidate.GetComReference())) )
		{
			m_pTsfManager->m_uiCandRef --;
			if( m_pTsfManager->m_uiCandRef == 0 )
			{
				m_pTsfManager->m_strCandidate.clear();
			}
		}
		return S_OK;
	}
	
#define LANG_CHS MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED)
#define LANG_CHT MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_TRADITIONAL)

	STDAPI CTsfManager::CUIElementSink::OnActivated(DWORD dwProfileType, LANGID langid, REFCLSID clsid, REFGUID catid,
			REFGUID guidProfile, HKL hkl, DWORD dwFlags)
	{
		static GUID TF_PROFILE_DAYI = { 0x037B2C25, 0x480C, 0x4D7F, 0xB0, 0x27, 0xD6, 0xCA, 0x6B, 0x69, 0x78, 0x8A };
		//m_iCandListIndexBase = IsEqualGUID( TF_PROFILE_DAYI, guidProfile ) ? 0 : 1;   
		if ( IsEqualIID( catid, GUID_TFCAT_TIP_KEYBOARD ) && ( dwFlags & TF_IPSINK_FLAG_ACTIVE ) )
		{
			const auto ChinaLangId = LANG_CHS;
			b8 bChineseIME = ( dwProfileType & TF_PROFILETYPE_INPUTPROCESSOR ) && langid == ChinaLangId;
			bChineseIME = bChineseIME;
			if ( dwProfileType & TF_PROFILETYPE_INPUTPROCESSOR )
			{
				m_pTsfManager->UpdateImeState(TRUE);
			}
			//else
				//g_dwState = IMEUI_STATE_OFF;
			//OnInputLangChange();
		}
		return S_OK;
	}

	STDAPI CTsfManager::CUIElementSink::OnChange(REFGUID rguid)
	{
		m_pTsfManager->UpdateImeState();
		return S_OK;
	}

	void AddChara(EditText& editText, c16 sChar)
	{
		if( !editText.bInserMode && editText.iCaret < (s32)editText.strText.length() &&
			editText.strText[editText.iCaret] != '\n' )
		{
			editText.strText[editText.iCaret] = sChar;
		}
		else
		{
			editText.strText.insert(editText.iCaret, 1, sChar);
		}
		editText.iCaret ++;
		editText.iSelectStart = editText.iCaret;
	}
	void AddString(EditText& editText, const std::wstring& strText)
	{
		for( auto sChar: strText )
		{
			AddChara(editText, sChar);
		}
	}

	void DeleteSelection(EditText& editText)
	{
		if( editText.strText.length() == 0 ||
			editText.iCaret == editText.iSelectStart )
		{
			return;
		}
		s32 iStart = Min(editText.iCaret, editText.iSelectStart);
		s32 iCount = Max(editText.iCaret, editText.iSelectStart) - iStart;

		editText.strText.erase(iStart, iCount);
		editText.iCaret = iStart;
		editText.iSelectStart = editText.iCaret;
		editText.iBlink = 0;
	}

	b8 CopyClipBoard(EditText& editText)
	{
		if( editText.strText.length() == 0 ||
			editText.iCaret == editText.iSelectStart )
		{
			return true;
		}
		b8 ret = false;
		s32 iStart = Min(editText.iCaret, editText.iSelectStart);
		s32 iCount = Max(editText.iCaret, editText.iSelectStart) - iStart;
		if( OpenClipboard(null) )
		{
			EmptyClipboard();
			HGLOBAL hBlock = GlobalAlloc(GMEM_MOVEABLE, sizeof(c16) * (iCount + 1));
			if( hBlock )
			{
				c16* pText = (c16*)GlobalLock(hBlock);
				if( pText )
				{
					auto strTest = editText.strText.substr(iStart, iCount);
					CopyMemory(pText, strTest.c_str(), sizeof(c16) * (iCount + 1));
					GlobalUnlock(hBlock);
				}
				ret = true;
				SetClipboardData(CF_UNICODETEXT, hBlock);

				GlobalFree(hBlock);
			}
			CloseClipboard();
		}
		return ret;
	}

	b8 PasteClipBoard(EditText& editText)
	{
		b8 ret = false;
		if( OpenClipboard(null) )
		{
			HANDLE hHandle = GetClipboardData(CF_UNICODETEXT);
			if( hHandle )
			{
				c16* pText = (c16*)GlobalLock(hHandle);
				if( pText )
				{
					for( u32 i=0; pText[i] != 0; i++ )
					{
						editText.strText.insert(editText.iCaret ++, 1, pText[i]);
					}
					editText.iSelectStart = editText.iCaret;

					GlobalUnlock(hHandle);
				}
			}
			CloseClipboard();
			ret = true;
		}
		return ret;
	}

	CTsfManager* g_pTsfManager = null;
	//----------------------------------------------------------------------
	//	ウィンドウイベント用
	//----------------------------------------------------------------------
	void CSceneGuiTest::WindowEvent(Window::IWindow* pWindow, Window::IWindowEvent* pEvent, s32 iMsg, s32 wParam, s32 lParam)
	{
		auto _this = (CSceneGuiTest*)pEvent->GetData();
		auto& editText = _this->m_EditText;
		if( !editText.bEnable ) 
		{
			return;
		}
		SCOPE_LOCK_CRITICALSECTION(_this->m_pCS);

		switch( iMsg )
		{
		case 0x00001008:
		{
			TKGLPtr<CTsfManager> pTsfManager = new CTsfManager();
			if( pTsfManager->Initialize() )
			{
				pTsfManager->AddRef();
				g_pTsfManager = pTsfManager;
			}
		}
		break;

		//case WM_IME_SETCONTEXT:
		case WM_IME_STARTCOMPOSITION:
		case WM_IME_ENDCOMPOSITION:
			editText.strCompStr.clear();
			editText.ucCompAttr.clear();
			editText.iIMECursorPos = 0;
			break;
		case WM_IME_COMPOSITION:
		{
			auto hWnd = (HWND)pWindow->GetHandle();
			auto hImc = ImmGetContext(hWnd);
			wstring sCompStr;
			if( lParam & GCS_RESULTSTR )
			{
				dword size = ImmGetCompositionString(hImc, GCS_RESULTSTR, null, 0);
				dword len = (size / sizeof(c16));
				sCompStr.resize(len);
				ImmGetCompositionString(hImc, GCS_RESULTSTR, &sCompStr[0], size);
				editText.strCompStr.clear();
				editText.ucCompAttr.clear();
				editText.iIMECursorPos = 0;
				DeleteSelection(editText);
				const auto test = sCompStr.c_str();
				AddString(editText, test);
			}
			if( lParam & GCS_COMPSTR )
			{
				dword size = ImmGetCompositionString(hImc, GCS_COMPSTR, null, 0);
				dword len = (size / sizeof(c16));
				sCompStr.resize(len);
				ImmGetCompositionString(hImc, GCS_COMPSTR, &sCompStr[0], size);
				vector<u8> ucCompAttr;
				if( len > 0 )
				{
					ucCompAttr.resize(len);
					ImmGetCompositionString(hImc, GCS_COMPATTR, &ucCompAttr[0], len);
				}
				auto p = sCompStr.c_str();
				(void)p;
				editText.strCompStr = sCompStr;
				editText.ucCompAttr = ucCompAttr;
				editText.iIMECursorPos = ImmGetCompositionString(hImc, GCS_CURSORPOS, null, 0);
				editText.iBlink = 0;
			}
			ImmReleaseContext(hWnd, hImc);
		}
		break;

		case WM_IME_NOTIFY:
		{
			switch (wParam)
			{
			case IMN_OPENCANDIDATE:
			case IMN_CHANGECANDIDATE:
			{
				auto hWnd = (HWND)pWindow->GetHandle();
				auto hImc = ImmGetContext(hWnd);

				ImmSetOpenStatus(hImc, true);
				dword size = ImmGetCandidateList(hImc, 0, null, 0);
				if( size > 0 )
				{
					TSmartPtr<u8> pBuffer = new u8[size];
					LPCANDIDATELIST	pCandList = (LPCANDIDATELIST)pBuffer;
					size = ImmGetCandidateList(hImc, 0, pCandList, size);

					s32 i;
					s32 mbBufLen = 0;
					for ( i = 0; i < (s32)pCandList->dwCount; i++ )
					{
						mbBufLen += WideCharToMultiByte(0, 0, (c16*)((c8*)pCandList + pCandList->dwOffset[i]), -1, NULL, 0, NULL, NULL );
					}
					mbBufLen += pCandList->dwOffset[0];
					LPCANDIDATELIST pResult = (LPCANDIDATELIST)malloc(mbBufLen);
					LPCANDIDATELIST pCandListA = pResult;
					memcpy(pCandListA, pCandList, pCandList->dwOffset[0]);

					LPSTR psz = (LPSTR)pCandListA + pCandList->dwOffset[0];
					for (i = 0; i < (s32)pCandList->dwCount; i++)
					{
						pCandListA->dwOffset[i] = (DWORD)((LPSTR)psz - (LPSTR)pCandListA);
						psz += WideCharToMultiByte(0, 0, (LPWSTR)((LPSTR)pCandList + pCandList->dwOffset[i]), -1, psz, 256, NULL, NULL );
					}
					size = mbBufLen;
					free(pResult);
				}
				ImmReleaseContext(hWnd, hImc);
			}
			break;
			}
		}
		break;

		case WM_KEYDOWN:
		{
			switch( wParam )
			{
			case VK_DELETE:
			{
				if( editText.strText.length() )
				{
					if( editText.iCaret != editText.iSelectStart )
					{
						DeleteSelection(editText);
					}
					else if( editText.iCaret < (s32)editText.strText.length() )
					{
						editText.strText.erase(editText.iCaret, 1);
						editText.iSelectStart = editText.iCaret;
						editText.iBlink = 0;
					}
				}
			}
			break;
			case VK_LEFT:
			{
				if( editText.iCaret > 0 )
				{
					editText.iCaret --;
				}
				if( GetKeyState(VK_SHIFT) >= 0 )
				{
					editText.iSelectStart = editText.iCaret;
				}
				editText.iBlink = 0;
			}
			break;
			case VK_RIGHT:
			{
				if( editText.iCaret < (s32)editText.strText.length() )
				{
					editText.iCaret ++;
				}
				if( GetKeyState(VK_SHIFT) >= 0 )
				{
					editText.iSelectStart = editText.iCaret;
				}
				editText.iBlink = 0;
			}
			break;
			case VK_INSERT:
			{
				editText.bInserMode = !editText.bInserMode;
			}
			break;
			}
		}
		break;

		case WM_CHAR:
		{
			switch( wParam )
			{
			case VK_BACK:
			{
				if( editText.strText.length() )
				{
					if( editText.iCaret != editText.iSelectStart )
					{
						DeleteSelection(editText);
					}
					else if( editText.iCaret > 0 )
					{
						editText.strText.erase(editText.iCaret-1, 1);
						editText.iCaret = Max(editText.iCaret-1, 0);
						editText.iSelectStart = editText.iCaret;
						editText.iBlink = 0;
					}
				}
			}
			break;

			case 24:		// Ctrl-X Cut
			case VK_CANCEL:	// Ctrl-C Copy
			{
				CopyClipBoard(editText);
				if( (s32)wParam == 24 )
				{
					DeleteSelection(editText);
				}
			}
			break;
			case 22: // Ctrl-V Paste
			{
				DeleteSelection(editText);
				PasteClipBoard(editText);
			}
			break;

            case 26:  // Ctrl Z
            case 25:  // Ctrl Y
				break;

            case 2:   // Ctrl B
            case 14:  // Ctrl N
            case 19:  // Ctrl S
            case 4:   // Ctrl D
            case 6:   // Ctrl F
            case 7:   // Ctrl G
            case 10:  // Ctrl J
            case 11:  // Ctrl K
            case 12:  // Ctrl L
            case 17:  // Ctrl Q
            case 23:  // Ctrl W
            case 5:   // Ctrl E
            case 18:  // Ctrl R
            case 20:  // Ctrl T
            case 21:  // Ctrl U
            case 9:   // Ctrl I
            case 15:  // Ctrl O
            case 16:  // Ctrl P
            case 27:  // Ctrl [
            case 29:  // Ctrl ]
            case 28:  // Ctrl \ 
                break;

            case VK_RETURN:
				wParam = L'\n';
			default:
			{
				DeleteSelection(editText);
				AddChara(editText, (c16)wParam);
			}
			break;
			}
		}
		break;
		}
	}
#define	TEST 0
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneGuiTest::Initialize(void)
	{
		Font::FontInfo Info;
		Info.pTTF = KGLFileReader()->LoadBlock("Data/Font/Gyate-Luminescence.ttf");
		Info.uiTTFSIze = KGLFileReader()->GetSize(Info.pTTF);
		kgStrcpy(Info.cFont, "ぎゃーてーるみねっせんす");
		//kgStrcpy(Info.cFont, "ＭＳ ゴシック");
		//kgStrcpy(Info.cFont, "HG創英角ﾎﾟｯﾌﾟ体");
		Info.iSize = 16;
		Font::Create(m_pFont.GetReference(), Info);
		KGLFileReader()->Unload(Info.pTTF);

		m_bExit = false;

#if TEST
		m_EditText.strText = L"default";

		m_EditText.iCaret = m_EditText.strText.length();
		m_EditText.iSelectStart = m_EditText.iCaret;
		m_EditText.iIMECursorPos = 0;
		m_EditText.iBlink = 0;
		m_EditText.bEnable = true;
		m_EditText.bInserMode = true;

		Window::Create(m_pEvent.GetReference());
		Thread::Create(m_pCS.GetReference());

		m_pEvent->SetData(this);
		m_pEvent->SetCommonEventCallBack(WindowEvent);

		KGLGameManager()->MainWindow()->SetIME(true);
		KGLGameManager()->MainWindow()->SetWindowEvent(m_pEvent);

		SendMessage((HWND)KGLGameManager()->MainWindow()->GetHandle(), 0x00001008, 0, 0);
		kgSleep(0.05f);
#else
		m_Text.Initialize();
		m_Text.SetForcus(true);
#endif

#if KGL_DEBUG_MENU
		KGLDebugMenu()->SetEnable(false);
#endif // ~#if KGL_DEBUG_MENU
		return true;
	}
	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneGuiTest::Finalize(void)
	{
#if KGL_DEBUG_MENU
		KGLDebugMenu()->SetEnable(true);
#endif // ~#if KGL_DEBUG_MENU

#if TEST
		KGLGameManager()->MainWindow()->ClearWindowEvent(m_pEvent);
		KGLGameManager()->MainWindow()->SetIME(false);
		m_pEvent = null;

		if( g_pTsfManager )
		{
			g_pTsfManager->Finalize();
			SafeRelease(g_pTsfManager);
		}
#else
		m_Text.Finalize();
#endif
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneGuiTest::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;

		if( AppInput()->IsButtonPush(EKeyCode::UP) ||
			AppInput()->IsButtonPush(EKeyCode::DOWN))
		{
			m_bExit = !m_bExit;
		}
		if( m_bExit )
		{
			if( AppInput()->IsButtonPush(EKeyCode::START) )
			{
				KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
			}
		}
#if !TEST
		m_Text.Update();
#endif
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneGuiTest::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		TKGLPtr<Render::IDrawer> pDrawer = pRenderingSystem->Drawer();
		TKGLPtr<Font::IFont> pFont = m_pFont;
		{
			Vector2 vPos(0, 0);
			Vector2 vScale = OneVector2;
			FColor Color = FColorWhite;
			f32 fHeight = pFont->GetDrawTextSize(L" ", vScale).y;
			if( m_bExit )
			{
				Color = FColorRed;
			}
#if TEST
			SCOPE_LOCK_CRITICALSECTION(m_pCS);

			const wstring& strText = m_EditText.strText;
			const wstring& strCompStr = m_EditText.strCompStr;
			const vector<u8> ucCompAttr = m_EditText.ucCompAttr;
			s32& iCaret = m_EditText.iCaret;
			s32& iOldCaret = m_EditText.iOldCaret;
			s32& iSelectStart = m_EditText.iSelectStart;
			s32& iIMECursorPos = m_EditText.iIMECursorPos;
			s32& iBlink = m_EditText.iBlink;
			if( iCaret != iOldCaret )
			{
				iBlink = 0;
				iOldCaret = iCaret;
			}
			bool bShowCursol = !((iBlink ++) & 0x20);
			bool bInserMode = !!m_EditText.bInserMode;

			u32 uiSelect = 0;
			vector<std::wstring> strCandidates;
			if( g_pTsfManager )
			{
				uiSelect = g_pTsfManager->GetSelection();
				strCandidates = g_pTsfManager->GetCandidate();
			}
#else
			const auto& strText = m_Text.GetText();
			const auto& strCompStr = m_Text.GetCompText();
			const auto& ucCompAttr = m_Text.GetCompAttr();
			s32 iCaret = m_Text.GetCurrentCursol();
			s32 iSelectStart = m_Text.GetSelectStart();
			s32 iIMECursorPos = m_Text.GetCompCursol();
			bool bShowCursol = m_Text.IsShowCursol();
			bool bInserMode = m_Text.IsInsertMode();

			u32 uiSelect = m_Text.GetCandidateSelection();
			auto& strCandidates = m_Text.GetCandidate();
#endif

			auto pStr = CharSet::Format("%d文字, Current:%d, Select:%d, Cur:%d",
				strText.length(), iCaret, iSelectStart, iIMECursorPos);

			auto vSize = pFont->GetDrawTextSize(pStr, vScale);
			pDrawer->DrawRect(vPos, vSize, FColorWhite * 0.5f);
			pFont->Draw(pRenderingSystem, pStr, vPos, OneVector2, Color);
			vPos.y += fHeight;

			Color = FColorWhite;
			Vector2 vOffset;
			vSize = pFont->GetDrawTextSize(strText.c_str(), vScale, &vOffset);
			vSize.y = vOffset.y + fHeight;
			//	入力済み文字列描画
			pDrawer->DrawRect(vPos, vSize, FColorWhite * 0.5f);
			pFont->Draw(pRenderingSystem, strText.c_str(), vPos, vScale, Color);

			{
				auto strCurrent = strText.substr(0, iCaret);
				Vector2 vOffset;
				pFont->GetDrawTextSize(strCurrent.c_str(), vScale, &vOffset);
				
				FColor BarColor = FColorWhite;
				Vector2 vDrawSize(1.0f, fHeight);
				if( !bInserMode && iCaret < (s32)strText.length() &&
					iCaret == iSelectStart && strText[iCaret] != L'\n' &&
					strCompStr.length() == 0 )
				{
					c16 sChar[2] = { strText[iCaret], 0 };
					vDrawSize.x = pFont->GetDrawTextSize(sChar, vScale).x;
					BarColor *= 0.75f;
				}
				Vector2 vDrawPos = vPos + vOffset;
				//	カーソル位置
				Vector2 vCursolPos = vDrawPos.SafeFloor();
				Vector2 vCursolSize = vDrawSize;
				if( strCompStr.length() > 0 )
				{
					auto strComp = (strCompStr.substr(0, iIMECursorPos));
					pFont->GetDrawTextSize(strComp.c_str(), vScale, &vOffset);
					vCursolPos += vOffset;
				}

				Vector2 vRectPos = vDrawPos;
				f32 fBaseX = vRectPos.x;
				if( strCompStr.size() )
				{
					u32 uiStart = 0;
					//	変換中の文字列描画
					for( u32 i=0; i < strCompStr.size(); i++ )
					{
						u8 ucAttr = ucCompAttr[i];
						if( i == strCompStr.size()-1 || ucAttr != ucCompAttr[i+1] )
						{
							auto strDraw = strCompStr.substr(uiStart, i - uiStart + 1);
							FColor RectColor = FColorGray * 0.75f;
							switch( ucAttr )
							{
							case ATTR_TARGET_CONVERTED:
							case ATTR_TARGET_NOTCONVERTED:
								RectColor = FColorWhite * 0.75f;
								break;
							}
							if( strDraw.length() > 0 )
							{
								if( ucAttr == ATTR_TARGET_CONVERTED )
								{
									fBaseX = vRectPos.x;
								}
								vDrawSize = pFont->GetDrawTextSize(strDraw.c_str(), vScale);
								pDrawer->DrawRect(vRectPos, vDrawSize, RectColor);
								vRectPos.x += vDrawSize.x;
								uiStart = i + 1;
							}
						}
					}
				}
				pFont->Draw(pRenderingSystem, strCompStr.c_str(), vDrawPos, vScale, Color);
				{
					vDrawPos.x = fBaseX;
					vDrawPos.y += fHeight;
					u32 uiIndex = 0;
					for( auto strCand:strCandidates )
					{
						FColor NoticeColor = FColorWhite;
						if( uiIndex ++ == uiSelect ) NoticeColor = FColorRed;

						vDrawSize = pFont->GetDrawTextSize(strCand.c_str(), vScale);
						pDrawer->DrawRect(vDrawPos, vDrawSize, FColorWhite * 0.75f);
						pFont->Draw(pRenderingSystem, strCand.c_str(), vDrawPos, vScale, NoticeColor);
						vDrawPos.y += fHeight;
					}
				}
				//	カーソル描画
				if( bShowCursol )
				{
					pDrawer->DrawRect(vCursolPos, vCursolSize, BarColor);
				}
			}
			if( iCaret != iSelectStart )
			{
				s32 iStart = Min(iCaret, iSelectStart);
				s32 iCount = Max(iCaret, iSelectStart) - iStart;

				auto strStart = strText.substr(0, iStart);
				auto strEnd = strText.substr(0, iStart + iCount);

				Vector2 vOffset;
				pFont->GetDrawTextSize(strStart.c_str(), vScale, &vOffset);
				Vector2 vStartPos = vPos + vOffset;

				pFont->GetDrawTextSize(strEnd.c_str(), vScale, &vOffset);
				Vector2 vEndPos = vPos + vOffset;
				Vector2 vDrawSize;
				//	選択範囲の描画
				while( vStartPos.y < vEndPos.y )
				{
					vDrawSize.x = vSize.x;
					vDrawSize.y = fHeight;
					pDrawer->DrawRect(vStartPos, vDrawSize, FColorBlack * 0.5f);
					vStartPos.x = vPos.x;
					vStartPos.y += fHeight;
				}
				vDrawSize = vEndPos - vStartPos;
				vDrawSize.y += fHeight;
				pDrawer->DrawRect(vStartPos, vDrawSize, FColorBlack * 0.5f);
			}
		}
	}
}
//#endif // KGL_FINAL_RELEASE

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

#if !KGL_FINAL_RELEASE
#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Random/kgl.Random.h"

#pragma warning(push)
#pragma warning(disable:4005)
#include <d3dx9.h>
#include <dshow.h>
#include <vmr9.h>
#include "qedit.h"
#pragma comment(lib, "strmiids.lib")
#pragma warning(pop)

#include "../Movie/MFMovie.h"



namespace App
{
	class CMovieTexture
	{
		enum EState
		{
			EStateIdle,
			EStatePlaying,
			EStatePause,
			EStateStop,
		};

	public:
		CMovieTexture(void)
		{}
		~CMovieTexture(void)
		{}

		bool Initialize(void)
		{
			if( m_pPlayer.IsValid() )
			{
				WarningTrace("既に初期化されています(CMovieTexture::Initialize)");
				return false;
			}
			return true;
		}

		void Finalize(void)
		{
			m_pPlayer = null;
		}

		bool Play(const c16* pFilename)
		{
			m_pPlayer = CMovieManager::Instance().CreateMovie(pFilename);
			if( !m_pPlayer.IsValid() )
			{
				return false;
			}
			return m_pPlayer->Play(true);
		}

		void Resume(void)
		{
			if( m_pPlayer.IsValid() )
			{
				m_pPlayer->Resume();
			}
		}

		void Pause(void)
		{
			if( m_pPlayer.IsValid() )
			{
				m_pPlayer->Pause();
			}
		}

		void Update(void)
		{
			m_pPlayer->Update();
		}

	public:
		bool IsPlaying(void)
		{
			return m_pPlayer->IsPlay();
		}
		bool IsFinish(void)
		{
			return !m_pPlayer->IsPlay();
		}

		f32 MovieTime(void)
		{
			return m_pPlayer->MovieTime();
		}
		f32 CurrentTime(void)
		{
			return m_pPlayer->CurrentTime();
		}

		TKGLPtr<Render::ITexture> GetTexture(void)
		{
			return m_pPlayer->GetTexture();
		}

	private:
		TKGLPtr<IMoviePlayer>	m_pPlayer;
	};

	CMovieTexture m_Movie;
	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneMovieTest::Initialize(void)
	{
		if( !m_Movie.Initialize() )
		{
			return false;
		}
		const c16* pPath = L"Data/avi_divx5_mp3.avi";

		return m_Movie.Play(pPath);
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneMovieTest::Finalize(void)
	{
		m_Movie.Finalize();
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneMovieTest::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;


		m_Movie.Update();
		if( AppInput()->IsButtonPush(EKeyCode::CROSS) )
		{
			m_Movie.Pause();
		}
		if( AppInput()->IsButtonPush(EKeyCode::TRIANGLE) )
		{
			m_Movie.Resume();
		}
		if( m_Movie.IsFinish() )
		{
			//KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
			KGLSceneManager()->JumpScene(ESceneMode::MovieTest);
		}

		if( AppInput()->IsButtonPush(EKeyCode::START) )
		{
			KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
		}
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneMovieTest::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		TKGLPtr<Font::IFont> pFont = pRenderingSystem->DebugFont();
		
		auto pMovieTexture = m_Movie.GetTexture();
		if( pMovieTexture.IsValid() )
		{
			static f32 fAngle = 0.0f;
			const f32 ratio = Math::PI2 / (f32)Config::TargetFPS / 4.0f;
			if( AppInput()->IsButtonPress(EKeyCode::LEFT) )		fAngle -= ratio;
			if( AppInput()->IsButtonPress(EKeyCode::RIGHT) )	fAngle += ratio;

			auto vDrawPos = (Config::Resolution * 0.5).SafeFloor();
#if 1
			auto vDrawSize = pMovieTexture->GetSize();
#else
			auto vDrawSize = OneVector2 * 256;
#endif // 0

			pRenderingSystem->Drawer()->DrawRotatedTexture(pMovieTexture, vDrawPos, vDrawSize, ZeroVector2, OneVector2, fAngle, vDrawSize * 0.5f);

			s32 iX, iY;
			KGLGameManager()->MainWindow()->GetPosition(iX, iY);

			wstring strText = L"CurrentTime:";
			strText += m_Movie.CurrentTime();
			strText += L"/";
			strText += m_Movie.MovieTime();
			Vector2 vPos = vDrawPos - vDrawSize * 0.5f;
			pFont->Draw(pRenderingSystem, strText.c_str(), vPos, OneVector2, FColorGray);
		}
	}
}
#endif // KGL_FINAL_RELEASE

//=======================================================================
//	END OF FILE
//=======================================================================
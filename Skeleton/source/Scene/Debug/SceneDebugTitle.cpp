//----------------------------------------------------------------------
//!
//!	@file	SceneDebugTitle.cpp
//!	@brief	デバッグタイトル
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Scene/SceneRhythmGame.h"
#include "Scene/SceneRhythmGameEdit.h"


namespace App
{
	//	シーンリスト
	const s32 SceneList[] =
	{
		ESceneMode::RhithmGame,
		ESceneMode::RhithmGameEdit,
		ESceneMode::Scenario,
		ESceneMode::Test,
		ESceneMode::MovieTest,
		ESceneMode::VoiceTest,
		ESceneMode::GuiTest,
		ESceneMode::InputTest,
	};

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CSceneDebugTitle::Initialize(void)
	{
		m_iSelect = 0;

		KGLResourceManager()->Load("Data/test.dds");
		return true;
	}

	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CSceneDebugTitle::Finalize(void)
	{
	}
	
	static f32 fAngle = 0.0f;
	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CSceneDebugTitle::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;

		float ratio = fElapsedTime / (1.0f / 60.0f);
		ratio = ratio * (Math::PI2 / 180.0f);
		if( AppInput()->IsButtonPress(EKeyCode::LEFT) )		fAngle -= ratio;
		if( AppInput()->IsButtonPress(EKeyCode::RIGHT) )	fAngle += ratio;

		if( AppInput()->IsButtonPush(EKeyCode::UP) )
		{
			m_iSelect --;
		}
		if( AppInput()->IsButtonPush(EKeyCode::DOWN) )
		{
			m_iSelect ++;
		}
		m_iSelect = Math::Repeat(m_iSelect, 0, ArrayCount(SceneList));

		if( AppInput()->IsButtonPush(EKeyCode::START) )
		{
			KGLSceneManager()->JumpScene(SceneList[m_iSelect]);
		}
	}

	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CSceneDebugTitle::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		(void)pRenderingSystem;

#if	KGL_DEBUG_FONT
		TKGLPtr<Render::ITexture> pTexture = KGLResourceManager()->GetTexture("Data/test.dds");


		auto vDrawPos = (Config::Resolution * 0.5).SafeFloor();
		auto vDrawSize = OneVector2 * 256;

		pRenderingSystem->Drawer()->DrawRotatedTexture(pTexture, vDrawPos, vDrawSize, ZeroVector2, OneVector2, fAngle, vDrawSize * 0.5f, FColorPink);

		TKGLPtr<Font::IFont> pFont = pRenderingSystem->DebugFont();
		pFont->Draw(pRenderingSystem,
					CharSet::Format("%3.3f", fAngle),
					vDrawPos,
					OneVector2,
					FColorRed,
					Font::EAlign::Center);

		Vector2 vPos(32, 64), vScale(1.0f, 1.0f);

		for( u32 i=0; i < ArrayCount(SceneList); i++ )
		{
			const c8* pScene = KGLSceneManager()->SceneName(SceneList[i]);
			pFont->Draw(pRenderingSystem,
						pScene,
						vPos,
						vScale,
						i == m_iSelect? FColorRed: FColorWhite);

			vPos.y +=  pFont->GetDrawTextSize(pScene, vScale).y;
		}
#endif	// ~#if	KGL_DEBUG_FONT
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
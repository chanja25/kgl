//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	eXgpV[
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

#if !KGL_FINAL_RELEASE
#include "Utility/Hash/kgl.Hash.h"
#include "Utility/algorithm/kgl.find.h"
#include "Utility/algorithm/kgl.sort.h"
#include "Random/kgl.Random.h"

namespace App
{
	//----------------------------------------------------------------------
	//	úť
	//----------------------------------------------------------------------
	b8 CSceneTest::Initialize(void)
	{
		return true;
	}

	//----------------------------------------------------------------------
	//	jü
	//----------------------------------------------------------------------
	void CSceneTest::Finalize(void)
	{
	}

	//----------------------------------------------------------------------
	//	XV
	//----------------------------------------------------------------------
	void CSceneTest::Update(f32 fElapsedTime)
	{
		(void)fElapsedTime;

		if( AppInput()->IsButtonPush(EKeyCode::START) )
		{
			KGLSceneManager()->JumpScene(ESceneMode::DebugTitle);
		}
		if( AppInput()->IsButtonPush(EKeyCode::CROSS) )
		{
			KGLGameManager()->MainWindow()->ChangeWindowMode();
		}
		if( AppInput()->IsButtonPush(EKeyCode::CIRCLE) )
		{
			HWND hWnd = (HWND)KGLGameManager()->MainWindow()->GetHandle();
			static b8 bTest = true;
			SendMessage(hWnd, 0x00001004, bTest, 0);
			bTest = !bTest;
		}
	}

	//----------------------------------------------------------------------
	//	`ć
	//----------------------------------------------------------------------
	void CSceneTest::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		TKGLPtr<Font::IFont> pFont = pRenderingSystem->DebugFont();

		s32 iX, iY;
		KGLGameManager()->MainWindow()->GetPosition(iX, iY);

		auto pStr = 
			L"Ű×ÓÍăËóhcEáuüćFiÍamIžŞŠçüŻętŤűĆ¤ź˛JüĎˇˇˇ÷Ńi{`FÄŻ\Š§í\n";
		Vector2 vPos(0, 0);
		pFont->Draw(pRenderingSystem, pStr, vPos, OneVector2, FColorGray);
	}
}
#endif // KGL_FINAL_RELEASE

//=======================================================================
//	END OF FILE
//=======================================================================
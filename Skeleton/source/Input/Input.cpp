//----------------------------------------------------------------------
//!
//!	@file	Input.cpp
//!	@brief	インプット
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"

namespace App
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CInput::CInput(void)
		: m_pKeyboard(null)
	{
		Initialize();
	}

	//----------------------------------------------------------------------
	//	初期化
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::Initialize(void)
	{
		//	キーボードクラスの生成
		kgl::Input::Create(m_pKeyboard.GetReference());

		if( kgl::Input::GetJoyStickNum() > 0 )
		{
			kgl::Input::Create(m_pGamepad.GetReference());

			//	キーの初期設定
			SetKeyMapping(EKeyCode::CROSS | EKeyCode::GAMEPAD,		EKeyCode::CROSS);
			SetKeyMapping(EKeyCode::CIRCLE | EKeyCode::GAMEPAD,		EKeyCode::CIRCLE);
			SetKeyMapping(EKeyCode::SQUARE | EKeyCode::GAMEPAD,		EKeyCode::SQUARE);
			SetKeyMapping(EKeyCode::TRIANGLE | EKeyCode::GAMEPAD,	EKeyCode::TRIANGLE);
			SetKeyMapping(EKeyCode::L1 | EKeyCode::GAMEPAD,			EKeyCode::L1);
			SetKeyMapping(EKeyCode::R1 | EKeyCode::GAMEPAD,			EKeyCode::R1);
			SetKeyMapping(EKeyCode::L2 | EKeyCode::GAMEPAD,			EKeyCode::L2);
			SetKeyMapping(EKeyCode::R2 | EKeyCode::GAMEPAD,			EKeyCode::R2);
			SetKeyMapping(EKeyCode::START | EKeyCode::GAMEPAD,		EKeyCode::START);
			SetKeyMapping(EKeyCode::SELECT | EKeyCode::GAMEPAD,		EKeyCode::SELECT);
			SetKeyMapping(EKeyCode::UP | EKeyCode::GAMEPAD,			EKeyCode::UP);
			SetKeyMapping(EKeyCode::DOWN | EKeyCode::GAMEPAD,		EKeyCode::DOWN);
			SetKeyMapping(EKeyCode::LEFT | EKeyCode::GAMEPAD,		EKeyCode::LEFT);
			SetKeyMapping(EKeyCode::RIGHT | EKeyCode::GAMEPAD,		EKeyCode::RIGHT);
		}

		//	キーの初期設定
		SetKeyMapping(EKeyCode::CROSS,		Input::EKey::KEY_Z);
		SetKeyMapping(EKeyCode::CIRCLE,		Input::EKey::KEY_X);
		SetKeyMapping(EKeyCode::SQUARE,		Input::EKey::KEY_C);
		SetKeyMapping(EKeyCode::TRIANGLE,	Input::EKey::KEY_V);
		SetKeyMapping(EKeyCode::L1,			Input::EKey::KEY_A);
		SetKeyMapping(EKeyCode::R1,			Input::EKey::KEY_S);
		SetKeyMapping(EKeyCode::L2,			Input::EKey::KEY_Q);
		SetKeyMapping(EKeyCode::R2,			Input::EKey::KEY_W);
		SetKeyMapping(EKeyCode::START,		Input::EKey::KEY_RETURN);
		SetKeyMapping(EKeyCode::SELECT,		Input::EKey::KEY_SPACE);
		SetKeyMapping(EKeyCode::UP,			Input::EKey::KEY_UP);
		SetKeyMapping(EKeyCode::DOWN,		Input::EKey::KEY_DOWN);
		SetKeyMapping(EKeyCode::LEFT,		Input::EKey::KEY_LEFT);
		SetKeyMapping(EKeyCode::RIGHT,		Input::EKey::KEY_RIGHT);

		return true;
	}									  

	//----------------------------------------------------------------------
	//	キーのセット
	//!	@param iKey [in] キー番号
	//!	@param iKeyCode [in] キーコード
	//----------------------------------------------------------------------
	void CInput::SetKeyMapping(u32 uiKey, u32 uiKeyCode)
	{
		m_KeyMap[uiKey] = uiKeyCode;
	}

	//----------------------------------------------------------------------
	//	キーが押されているかチェック
	//!	@param iKey [in] キー
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::IsKeyPress(u32 uiKey)
	{
#if	KGL_DEBUG_MENU
		if( KGLDebugMenu()->IsVisible() )
		{
			return false;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		return m_pKeyboard->IsKeyPress(uiKey);
	}

	//----------------------------------------------------------------------
	//	キーを押したかチェック
	//!	@param iKey [in] キー
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::IsKeyPush(u32 uiKey)
	{
#if	KGL_DEBUG_MENU
		if( KGLDebugMenu()->IsVisible() )
		{
			return false;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		return m_pKeyboard->IsKeyPush(uiKey);
	}

	//----------------------------------------------------------------------
	//	キーが離されたかチェック
	//!	@param iKey [in] キー
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::IsKeyRelease(u32 uiKey)
	{
#if	KGL_DEBUG_MENU
		if( KGLDebugMenu()->IsVisible() )
		{
			return false;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		return m_pKeyboard->IsKeyRelease(uiKey);
	}

	//----------------------------------------------------------------------
	//	キーが押されているかチェック
	//!	@param iKey [in] キー
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::IsButtonPress(u32 uiKey)
	{
#if	KGL_DEBUG_MENU
		if (KGLDebugMenu()->IsVisible())
		{
			return false;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		if (m_KeyMap.find(uiKey | EKeyCode::GAMEPAD) != m_KeyMap.end())
		{
			if (m_pGamepad.IsValid())
			{
				if (m_pGamepad->IsButtonPress(m_KeyMap[uiKey | EKeyCode::GAMEPAD]))
				{
					return true;
				}
			}
			if (uiKey & EKeyCode::GAMEPAD)
			{
				return false;
			}
		}
		if (m_KeyMap.find(uiKey) != m_KeyMap.end())
		{
			return m_pKeyboard->IsKeyPress(m_KeyMap[uiKey]);
		}

		return m_pKeyboard->IsKeyPress(uiKey);
	}

	//----------------------------------------------------------------------
	//	キーを押したかチェック
	//!	@param iKey [in] キー
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::IsButtonPush(u32 uiKey)
	{
#if	KGL_DEBUG_MENU
		if (KGLDebugMenu()->IsVisible())
		{
			return false;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		if (m_KeyMap.find(uiKey | EKeyCode::GAMEPAD) != m_KeyMap.end())
		{
			if (m_pGamepad.IsValid())
			{
				if (m_pGamepad->IsButtonPush(m_KeyMap[uiKey | EKeyCode::GAMEPAD]))
				{
					return true;
				}
			}
			if (uiKey & EKeyCode::GAMEPAD)
			{
				return false;
			}
		}
		if (m_KeyMap.find(uiKey) != m_KeyMap.end())
		{
			return m_pKeyboard->IsKeyPush(m_KeyMap[uiKey]);
		}

		return m_pKeyboard->IsKeyPush(uiKey);
	}

	//----------------------------------------------------------------------
	//	キーが離されたかチェック
	//!	@param iKey [in] キー
	//!	@return 結果
	//----------------------------------------------------------------------
	b8 CInput::IsButtonRelease(u32 uiKey)
	{
#if	KGL_DEBUG_MENU
		if (KGLDebugMenu()->IsVisible())
		{
			return false;
		}
#endif	// ~#if	KGL_DEBUG_MENU

		if (m_KeyMap.find(uiKey | EKeyCode::GAMEPAD) != m_KeyMap.end())
		{
			if (m_pGamepad.IsValid())
			{
				if (m_pGamepad->IsButtonRelease(m_KeyMap[uiKey | EKeyCode::GAMEPAD]))
				{
					return true;
				}
			}
			if (uiKey & EKeyCode::GAMEPAD)
			{
				return false;
			}
		}
		if (m_KeyMap.find(uiKey) != m_KeyMap.end())
		{
			return m_pKeyboard->IsKeyRelease(m_KeyMap[uiKey]);
		}

		return m_pKeyboard->IsKeyRelease(uiKey);
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
//----------------------------------------------------------------------
//!
//!	@file	RhythmGameManager.h
//!	@brief	リズムゲーム管理
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Manager/RhythmGameManager.h"
#include "Game/RhythmGame/RhythmGameData.h"

namespace App
{
	using namespace RhythmGame;

	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CRhythmGameManager::CRhythmGameManager(void)
		: m_bAllInput(false)
	{}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CRhythmGameManager::~CRhythmGameManager(void)
	{}

	//----------------------------------------------------------------------
	//	初期化
	//----------------------------------------------------------------------
	b8 CRhythmGameManager::Initialize(void)
	{
		m_bAllInput = false;

		return true;
	}
	//----------------------------------------------------------------------
	//	破棄
	//----------------------------------------------------------------------
	void CRhythmGameManager::Finalize(void)
	{
		m_Music.Finalize();
		m_Rhythm.Finalize();
	}

	//----------------------------------------------------------------------
	//	リズムゲームの設定
	//----------------------------------------------------------------------
	b8 CRhythmGameManager::Setup(const void* pData)
	{
		if( pData == false )
		{
			return false;
		}
		const u8* pPtr = (const u8*)pData;

		RhythmGameHeader* pHeader = (RhythmGameHeader*)pPtr;

		MusicData* pMusicData = (MusicData*)(pPtr + pHeader->iMusicDataOffset);
		//	楽曲情報の初期化
		if( !m_Music.Initialize(pMusicData) )
		{
			return false;
		}
		//	タイミング情報の生成
		if( !m_Rhythm.Initialize(pHeader) )
		{
			return false;
		}

		return true;
	}
	//----------------------------------------------------------------------
	//	リズムゲーム開始
	//----------------------------------------------------------------------
	void CRhythmGameManager::Start(void)
	{
		m_Rhythm.Reset();
		m_Music.Play();
	}

	//----------------------------------------------------------------------
	//	一時停止
	//----------------------------------------------------------------------
	void CRhythmGameManager::Pause(void)
	{
		m_Music.Pause();
	}
	//----------------------------------------------------------------------
	//	一時停止の解除
	//----------------------------------------------------------------------
	void CRhythmGameManager::Resume(void)
	{
		m_Music.Resume();
	}

	//----------------------------------------------------------------------
	//	プレイ中か？
	//----------------------------------------------------------------------
	b8 CRhythmGameManager::IsPlaying(void)
	{
		return m_Music.IsPlaying();
	}
	//----------------------------------------------------------------------
	//	ポーズ中か？
	//----------------------------------------------------------------------
	b8 CRhythmGameManager::IsPause(void)
	{
		return m_Music.IsPause();
	}

	//----------------------------------------------------------------------
	//	更新
	//----------------------------------------------------------------------
	void CRhythmGameManager::Update(f32 fElapsedTime)
	{
		m_Music.Update(fElapsedTime);
		m_Rhythm.Update(fElapsedTime, m_Music.CurrentTime());
	}
	//----------------------------------------------------------------------
	//	描画
	//----------------------------------------------------------------------
	void CRhythmGameManager::Render(Render::IRenderingSystem* pRenderingSystem)
	{
		m_Rhythm.Render(pRenderingSystem);
	}
	//----------------------------------------------------------------------
	//	全ての入力を表示するか？
	//----------------------------------------------------------------------
	void CRhythmGameManager::SetAllInput(b8 bEnable)
	{
		m_bAllInput = !!bEnable;
	}
	//----------------------------------------------------------------------
	//	全ての入力を表示するか？
	//----------------------------------------------------------------------
	b8 CRhythmGameManager::IsAllInput(void)
	{
		return !!m_bAllInput;
	}
}

//=======================================================================
//	END OF FILE
//=======================================================================
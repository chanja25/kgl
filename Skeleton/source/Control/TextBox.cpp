//----------------------------------------------------------------------
//!
//!	@file	SceneTest.cpp
//!	@brief	テスト用シーン
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Control/TextBox.h"


namespace App
{
	namespace Control
	{
		CTsfManager::CTsfManager(void)
			: m_uiCount(0), m_uiCandRef(0)
			, m_dwUIElementSinkCookie(TF_INVALID_COOKIE)
			, m_dwOpenModeSinkCookie(TF_INVALID_COOKIE)
			, m_dwConvModeSinkCookie(TF_INVALID_COOKIE)
			, m_dwAlpnSinkCookie(TF_INVALID_COOKIE)
		{}
		CTsfManager::~CTsfManager(void)
		{}

		bool CTsfManager::Initialize(void)
		{
			Thread::Create(m_pEvent.GetReference(), true);
			Window::Create(m_pWindowEvent.GetReference());
			m_pWindowEvent->SetData(this);
			m_pWindowEvent->SetCommonEventCallBack(WindowEvent);

			KGLGameManager()->MainWindow()->SetIME(true);
			KGLGameManager()->MainWindow()->SetWindowEvent(m_pWindowEvent);

			SendMessage((HWND)KGLGameManager()->MainWindow()->GetHandle(), 0x00001008, 0, 0);
			m_pEvent->Wait();

			SetMaxCandList(DefaultMaxCandList);

			return !!m_bInit;
		}
		void CTsfManager::Finalize(void)
		{
			SCOPE_LOCK_CRITICALSECTION(m_pCS);

			TKGLPtr<ITfSource> pSource;
			if( m_pThreadMgr && SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfSource), pSource.GetComReference())) )
			{
				pSource->UnadviseSink(m_dwUIElementSinkCookie);
				pSource->UnadviseSink(m_dwAlpnSinkCookie);
				SetupCompartmentSinks(true);
				m_pThreadMgr->Deactivate();
				m_pThreadMgr = null;
				m_pTsfSink = null;
			}
			KGLGameManager()->MainWindow()->ClearWindowEvent(m_pWindowEvent);
			KGLGameManager()->MainWindow()->SetIME(false);
		}
		void CTsfManager::Setup(void)
		{
			m_bInit = false;
			HRESULT hr;
			hr = CoCreateInstance(CLSID_TF_ThreadMgr, null, CLSCTX_INPROC_SERVER, __uuidof(ITfThreadMgrEx), m_pThreadMgr.GetComReference());
			if( FAILED(hr) )
			{
				return;
			}
			TfClientId cid;
			if( FAILED(m_pThreadMgr->ActivateEx(&cid, TF_TMAE_UIELEMENTENABLEDONLY)) )
			{
				return;
			}

			bool ret = false;
			m_pTsfSink = new CUIElementSink(this);
			if( m_pTsfSink )
			{
				TKGLPtr<ITfSource> pTfSrc;
				if( SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfSource), pTfSrc.GetComReference())) )
				{
					if( SUCCEEDED(pTfSrc->AdviseSink(__uuidof(ITfUIElementSink), (ITfUIElementSink*)m_pTsfSink, &m_dwUIElementSinkCookie)) )
					{
						if( SUCCEEDED(pTfSrc->AdviseSink(__uuidof(ITfInputProcessorProfileActivationSink), (ITfInputProcessorProfileActivationSink*)m_pTsfSink, &m_dwAlpnSinkCookie)) )
						{
							if( SetupCompartmentSinks() )
							{
								ret = true;
							}
						}
					}
				}
			}
			if( ret )
			{
				Thread::Create(m_pCS.GetReference());

				EnableUiUpdates(false);
				EnableUiUpdates(true);
			}
			m_bInit = !!ret;
		}

		void CTsfManager::Update(HWND hWnd, s32 iMsg, s32 wParam, s32 lParam)
		{
			SCOPE_LOCK_CRITICALSECTION(m_pCS);
			for( auto pEditText: m_pEditTexts )
			{
				pEditText->Update(hWnd, iMsg, wParam, lParam);
			}
		}
		void CTsfManager::WindowEvent(Window::IWindow* pWindow, Window::IWindowEvent* pEvent, s32 iMsg, s32 wParam, s32 lParam)
		{
			auto _this = (CTsfManager*)pEvent->GetData();
			switch( iMsg )
			{
			case 0x00001008:
				_this->Setup();
				_this->m_pEvent->SetSignal();
				break;

			default:
				_this->Update((HWND)pWindow->GetHandle(), iMsg, wParam, lParam);
				break;
			}
		}

		void CTsfManager::SetEditText(CEditText* pEditText)
		{
			m_pEditTexts.push_back(pEditText);
		}

		void CTsfManager::DeleteEditText(CEditText* pEditText)
		{
			for( auto it=m_pEditTexts.begin(); it == m_pEditTexts.end(); it ++ )
			{
				if( (*it) == pEditText )
				{
					m_pEditTexts.erase(it);
					break;
				}
			}
		}

		void CTsfManager::EnableUiUpdates(bool bEnable)
		{
			if ( m_pThreadMgr == NULL ||
				 ( bEnable && m_dwUIElementSinkCookie != TF_INVALID_COOKIE )  ||
				 ( !bEnable && m_dwUIElementSinkCookie == TF_INVALID_COOKIE ) )
			{
				return;
			}
			TKGLPtr<ITfSource> pSource;
			if (SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfSource), pSource.GetComReference())))
			{
				if ( bEnable )
				{
					pSource->AdviseSink(__uuidof(ITfUIElementSink), (ITfUIElementSink*)m_pTsfSink, &m_dwUIElementSinkCookie);
				}
				else
				{
					pSource->UnadviseSink(m_dwUIElementSinkCookie);
					m_dwUIElementSinkCookie = TF_INVALID_COOKIE;
				}
			}
		}
		void CTsfManager::UpdateImeState(bool bResetCompartmentEventSink)
		{
			TKGLPtr<ITfCompartmentMgr> pCompMgr;
			TKGLPtr<ITfCompartment> pTfOpenMode;
			TKGLPtr<ITfCompartment> pTfConvMode;
			if( GetCompartments(pCompMgr.GetReference(), pTfOpenMode.GetReference(), pTfConvMode.GetReference()) )
			{
				VARIANT valOpenMode;
				VARIANT valConvMode;
				pTfOpenMode->GetValue(&valOpenMode);
				pTfConvMode->GetValue(&valConvMode);
				if( valOpenMode.vt == VT_I4 )
				{
				}
				VariantClear(&valOpenMode);
				VariantClear(&valConvMode);

				if( bResetCompartmentEventSink )
				{
					SetupCompartmentSinks(false, pTfOpenMode, pTfConvMode);
				}
			}
		}

		bool CTsfManager::SetupCompartmentSinks(bool bRemoveOnly, ITfCompartment* pTfOpenMode, ITfCompartment* pTfConvMode)
		{
			bool ret = false;
			bool bLocalCompartments = false;
			TKGLPtr<ITfCompartmentMgr> pCompMgr;

			if( !pTfOpenMode && !pTfConvMode )
			{
				bLocalCompartments = true;
				GetCompartments(pCompMgr.GetReference(), &pTfOpenMode, &pTfConvMode);
			}
			if( !(pTfOpenMode && pTfConvMode) )
			{
				return false;
			}
			TKGLPtr<ITfSource> pSrcOpenMode;
			if( SUCCEEDED(pTfOpenMode->QueryInterface(IID_ITfSource, pSrcOpenMode.GetComReference())) )
			{
				if( m_dwOpenModeSinkCookie != TF_INVALID_COOKIE )
				{
					pSrcOpenMode->UnadviseSink(m_dwOpenModeSinkCookie);
					m_dwOpenModeSinkCookie = TF_INVALID_COOKIE;
				}
				if( bRemoveOnly || SUCCEEDED(pSrcOpenMode->AdviseSink(IID_ITfCompartmentEventSink, (ITfCompartmentEventSink*)m_pTsfSink, &m_dwOpenModeSinkCookie)) )
				{
					TKGLPtr<ITfSource> pSrcConvMode;
					if( SUCCEEDED(pTfConvMode->QueryInterface(IID_ITfSource, pSrcConvMode.GetComReference())) )
					{
						if( m_dwConvModeSinkCookie != TF_INVALID_COOKIE )
						{
							pSrcConvMode->UnadviseSink(m_dwConvModeSinkCookie);
							m_dwConvModeSinkCookie = TF_INVALID_COOKIE;
						}
						if( bRemoveOnly || SUCCEEDED(pSrcConvMode->AdviseSink(IID_ITfCompartmentEventSink, (ITfCompartmentEventSink*)m_pTsfSink, &m_dwConvModeSinkCookie)) )
						{
							ret = true;
						}
					}
				}
			}
			UpdateImeState();
			return ret;
		}
		bool CTsfManager::GetCompartments(ITfCompartmentMgr** ppcm, ITfCompartment** ppTfOpenMode, ITfCompartment** ppTfConvMode)
		{
			TKGLPtr<ITfCompartmentMgr> pComMgr;
			TKGLPtr<ITfCompartment> pTfOpenMode;
			TKGLPtr<ITfCompartment> pTfConvMode;

			static GUID _GUID_COMPARTMENT_KEYBOARD_INPUTMODE_CONVERSION = { 0xCCF05DD8, 0x4A87, 0x11D7, 0xA6, 0xE2, 0x00, 0x06, 0x5B, 0x84, 0x43, 0x5C };

			if( SUCCEEDED(m_pThreadMgr->QueryInterface(IID_ITfCompartmentMgr, pComMgr.GetComReference())) )
			{
				if( SUCCEEDED(pComMgr->GetCompartment(GUID_COMPARTMENT_KEYBOARD_OPENCLOSE, pTfOpenMode.GetReference())) )
				{
					if( SUCCEEDED(pComMgr->GetCompartment(_GUID_COMPARTMENT_KEYBOARD_INPUTMODE_CONVERSION, pTfConvMode.GetReference())) )
					{
						pComMgr->AddRef();
						pTfOpenMode->AddRef();
						pTfConvMode->AddRef();
						*ppcm = pComMgr;
						*ppTfOpenMode = pTfOpenMode;
						*ppTfConvMode = pTfConvMode;
						return true;
					}
				}
			}
			return false;
		}

		ITfUIElement* CTsfManager::GetUIElement(dword dwUIElementId)
		{
			ITfUIElement* pElement = null;
			TKGLPtr<ITfUIElementMgr> pUiEmentMgr;
			if( SUCCEEDED(m_pThreadMgr->QueryInterface(__uuidof(ITfUIElementMgr), pUiEmentMgr.GetComReference())) )
			{
				pUiEmentMgr->GetUIElement(dwUIElementId, &pElement);
			}
			return pElement;
		}

		void CTsfManager::MakeReadingInformationString(ITfReadingInformationUIElement* pReading)
		{
			// 未使用(WindowイベントのWM_CHARを使用して文字を取得する)
		}

		void CTsfManager::MakeCandidateStrings(ITfCandidateListUIElement* pCandidate)
		{
			u32 uiIndex, uiCount, uiCurrentPage, uiPageCount;
			dword dwPageStart, dwPageSize;
			BSTR bstr;
			TSmartPtr<u32> pIndexList;

			uiIndex = uiCount = uiCurrentPage = uiPageCount = 0;
			dwPageStart = dwPageSize = 0;

			pCandidate->GetSelection(&uiIndex);
			pCandidate->GetCount(&uiCount);
			pCandidate->GetCurrentPage(&uiCurrentPage);

			m_uiSelection = uiIndex;
			m_uiCount = uiCount;
			m_uiPageCount = (uiCount + (m_uiMaxCandList-1)) / m_uiMaxCandList;

			pCandidate->GetPageIndex(null, 0, &uiPageCount);
			if( uiPageCount > 0 )
			{
				pIndexList = new u32[uiPageCount];
				pCandidate->GetPageIndex(pIndexList, uiPageCount, &uiPageCount);
				dwPageStart = pIndexList[uiCurrentPage];
				dwPageSize = ((uiCurrentPage < uiPageCount-1)?
					Min(uiCount, pIndexList[uiCurrentPage+1]): uiCount) - dwPageStart;
			}

			m_uiPageStart = (m_uiSelection / m_uiMaxCandList) * m_uiMaxCandList;
			m_uiSelection = m_uiSelection - m_uiPageStart;
			m_uiCandPageSize = Min<u32>(uiCount - m_uiPageStart, m_uiMaxCandList);

			SCOPE_LOCK_CRITICALSECTION(m_pCS);
			m_strCandidate.clear();
			for( u32 i=m_uiPageStart, j = 0; i < uiCount && j < m_uiCandPageSize; i++, j ++ )
			{
				if( SUCCEEDED(pCandidate->GetString(i, &bstr)) )
				{
					if( bstr )
					{
						wstring str = bstr;
						kgAssert(str.length() < 0x7fffffff, "error");
						m_strCandidate.push_back(bstr);
						SysFreeString(bstr);
					}
				}
			}
		}

		CTsfManager::CUIElementSink::CUIElementSink(CTsfManager* pTsfManager)
			: m_pTsfManager(pTsfManager), m_uiRefCounter(0)
		{}
		CTsfManager::CUIElementSink::~CUIElementSink(void)
		{}

		STDMETHODIMP CTsfManager::CUIElementSink::QueryInterface(REFIID riid, void **ppvObj)
		{
			if( ppvObj == null )
				return E_INVALIDARG;

			*ppvObj = null;
			if( IsEqualIID(riid, IID_IUnknown) )
			{
				*ppvObj = reinterpret_cast<IUnknown*>(this);
			}
			else if( IsEqualIID(riid, __uuidof(ITfUIElementSink)) )
			{
				*ppvObj = (ITfUIElementSink*)this;
			}
			else if( IsEqualIID(riid, __uuidof(ITfInputProcessorProfileActivationSink)) )
			{
				*ppvObj = (ITfInputProcessorProfileActivationSink*)this;
			}
			else if( IsEqualIID(riid, __uuidof(ITfCompartmentEventSink)) )
			{
				*ppvObj = (ITfCompartmentEventSink*)this;
			}
			if( *ppvObj )
			{
				AddRef();
				return S_OK;
			}
			return E_NOINTERFACE;
		}
		STDMETHODIMP_(ULONG) CTsfManager::CUIElementSink::AddRef(void)
		{
			return kgAtomicIncrement(m_uiRefCounter);
		}
		STDMETHODIMP_(ULONG) CTsfManager::CUIElementSink::Release(void)
		{
			u32 uiRef = kgAtomicDecrement(m_uiRefCounter);
			if( uiRef == 0 )
			{
				delete this;
			}
			return uiRef;
		}
		STDMETHODIMP CTsfManager::CUIElementSink::BeginUIElement(DWORD dwUIElementId, BOOL *pbShow)
		{
			TKGLPtr<ITfUIElement> pElement;
			*pElement.GetReference() = m_pTsfManager->GetUIElement(dwUIElementId);
			if( !pElement.IsValid() )
				return E_INVALIDARG;

			*pbShow = false;
			TKGLPtr<ITfReadingInformationUIElement> pReading;
			TKGLPtr<ITfCandidateListUIElement> pCandidate;
			if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfReadingInformationUIElement), pReading.GetComReference())) )
			{
				// todo
			}
			else if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfCandidateListUIElement), pCandidate.GetComReference())) )
			{
				m_pTsfManager->m_uiCandRef ++;
				m_pTsfManager->MakeCandidateStrings(pCandidate);
			}
			return S_OK;
		}
		STDMETHODIMP CTsfManager::CUIElementSink::UpdateUIElement(DWORD dwUIElementId)
		{
			TKGLPtr<ITfUIElement> pElement;
			*pElement.GetReference() = m_pTsfManager->GetUIElement(dwUIElementId);
			if( !pElement.IsValid() )
				return E_INVALIDARG;

			TKGLPtr<ITfReadingInformationUIElement> pReading;
			TKGLPtr<ITfCandidateListUIElement> pCandidate;
			if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfReadingInformationUIElement), pReading.GetComReference())) )
			{
				// todo
			}
			else if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfCandidateListUIElement), pCandidate.GetComReference())) )
			{
				m_pTsfManager->MakeCandidateStrings(pCandidate);
			}
			return S_OK;
		}
		STDMETHODIMP CTsfManager::CUIElementSink::EndUIElement(DWORD dwUIElementId)
		{
			TKGLPtr<ITfUIElement> pElement;
			*pElement.GetReference() = m_pTsfManager->GetUIElement(dwUIElementId);
			if( !pElement.IsValid() )
				return E_INVALIDARG;

			TKGLPtr<ITfReadingInformationUIElement> pReading;
			TKGLPtr<ITfCandidateListUIElement> pCandidate;
			if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfReadingInformationUIElement), pReading.GetComReference())) )
			{
				m_pTsfManager->m_uiCount = 0;
			}
			else if( SUCCEEDED(pElement->QueryInterface(__uuidof(ITfCandidateListUIElement), pCandidate.GetComReference())) )
			{
				m_pTsfManager->m_uiCandRef --;
				if( m_pTsfManager->m_uiCandRef == 0 )
				{
					SCOPE_LOCK_CRITICALSECTION(m_pTsfManager->m_pCS);
					m_pTsfManager->m_strCandidate.clear();
				}
			}
			return S_OK;
		}
	
#define LANG_CHS MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED)
#define LANG_CHT MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_TRADITIONAL)

		STDAPI CTsfManager::CUIElementSink::OnActivated(DWORD dwProfileType, LANGID langid, REFCLSID clsid, REFGUID catid,
				REFGUID guidProfile, HKL hkl, DWORD dwFlags)
		{
			static GUID TF_PROFILE_DAYI = { 0x037B2C25, 0x480C, 0x4D7F, 0xB0, 0x27, 0xD6, 0xCA, 0x6B, 0x69, 0x78, 0x8A };
			//m_iCandListIndexBase = IsEqualGUID( TF_PROFILE_DAYI, guidProfile ) ? 0 : 1;   
			if ( IsEqualIID( catid, GUID_TFCAT_TIP_KEYBOARD ) && ( dwFlags & TF_IPSINK_FLAG_ACTIVE ) )
			{
				const auto ChinaLangId = LANG_CHS;
				bool bChineseIME = ( dwProfileType & TF_PROFILETYPE_INPUTPROCESSOR ) && langid == ChinaLangId;
				bChineseIME = bChineseIME;
				if ( dwProfileType & TF_PROFILETYPE_INPUTPROCESSOR )
				{
					m_pTsfManager->UpdateImeState(TRUE);
				}
				//else
					//g_dwState = IMEUI_STATE_OFF;
				//OnInputLangChange();
			}
			return S_OK;
		}

		STDAPI CTsfManager::CUIElementSink::OnChange(REFGUID rguid)
		{
			m_pTsfManager->UpdateImeState();
			return S_OK;
		}

		CTsfManager* g_pTsfManager = null;
		CEditText::CEditText(void)
		{
			iCurrentStack = -1;
			iCaret = 0;
			iSelectStart = 0;
			iIMECursorPos = 0;
			iBlink = 0;
			bEnable = true;
			bForcus = false;
			bLineBreak = true;
			bInsertMode = true;

			ResetHistory();
		}
		bool CEditText::Initialize(s32 iMaxLength)
		{
			kgAssert(!pTsfManager.IsValid(), "既に初期化されています");
			if( pTsfManager.IsValid() ) return true;

			if( g_pTsfManager == null )
			{
				pTsfManager = new CTsfManager();
				if( !pTsfManager->Initialize() )
				{
					return false;
				}
				pTsfManager->AddRef();
				g_pTsfManager = pTsfManager;
			}
			else
			{
				pTsfManager = g_pTsfManager;
			}
			this->iMaxLength = iMaxLength;
			pTsfManager->SetEditText(this);
			return true;
		}
		void CEditText::Finalize(void)
		{
			if( pTsfManager != null )
			{
				pTsfManager->DeleteEditText(this);
				pTsfManager = null;
				if( g_pTsfManager->ReferenceCount() == 2 )
				{
					g_pTsfManager->Finalize();
					SafeRelease(g_pTsfManager);
				}
			}
		}

		bool CEditText::AddChara(c16 sChar)
		{
			if( !bInsertMode && iCaret < (s32)strText.length() &&
				strText[iCaret] != '\n' )
			{
				strText[iCaret] = sChar;
				iCaret ++;
				iSelectStart = iCaret;
				iBlink = 0;
				return true;
			}
			return InsertChara(sChar);
		}
		bool CEditText::InsertChara(c16 sChar)
		{
			if( 0 < iMaxLength && iMaxLength <= (s32)strText.length() ) return false;

			strText.insert(iCaret, 1, sChar);
			iCaret ++;
			iSelectStart = iCaret;
			iBlink = 0;
			return true;
		}
		bool CEditText::AddString(const wstring& strText)
		{
			bool ret = false;
			for( auto sChar: strText )
			{
				ret |= AddChara(sChar);
			}
			return ret;
		}

		bool CEditText::DeleteSelection(void)
		{
			if( strText.length() == 0 ||
				iCaret == iSelectStart )
			{
				return false;
			}
			s32 iStart = Min(iCaret, iSelectStart);
			s32 iCount = Max(iCaret, iSelectStart) - iStart;

			strText.erase(iStart, iCount);
			iCaret = iStart;
			iSelectStart = iCaret;
			iBlink = 0;
			return true;
		}

		void CEditText::SetCompText(const wstring& strText, const vector<u8>& ucAttr, s32 iCursol)
		{
			strCompStr = strText;
			ucCompAttr = ucAttr;
			iIMECursorPos = iCursol;
			iBlink = 0;
		}

		void CEditText::ClearCompText(void)
		{
			strCompStr.clear();
			ucCompAttr.clear();
			iIMECursorPos = 0;
			iBlink = 0;
		}

		bool CEditText::CopyClipBoard(bool bCut)
		{
			if( strText.length() == 0 ||
				iCaret == iSelectStart )
			{
				if( OpenClipboard(null) )
				{
					EmptyClipboard();
					CloseClipboard();
				}
				return true;
			}
			bool ret = false;
			s32 iStart = Min(iCaret, iSelectStart);
			s32 iCount = Max(iCaret, iSelectStart) - iStart;
			if( OpenClipboard(null) )
			{
				EmptyClipboard();
				HGLOBAL hBlock = GlobalAlloc(GMEM_MOVEABLE, sizeof(c16) * (iCount + 1));
				if( hBlock )
				{
					c16* pText = (c16*)GlobalLock(hBlock);
					if( pText )
					{
						auto strTest = strText.substr(iStart, iCount);
						CopyMemory(pText, strTest.c_str(), sizeof(c16) * (iCount + 1));
						GlobalUnlock(hBlock);
					}
					ret = true;
					SetClipboardData(CF_UNICODETEXT, hBlock);

					GlobalFree(hBlock);
				}
				CloseClipboard();
				if( bCut )
				{
					if( DeleteSelection() )
					{
						PushHistory();
					}
				}
			}
			return ret;
		}

		bool CEditText::PasteClipBoard(void)
		{
			bool change;
			change = DeleteSelection();

			bool ret = false;
			if( OpenClipboard(null) )
			{
				HANDLE hHandle = GetClipboardData(CF_UNICODETEXT);
				if( hHandle )
				{
					c16* pText = (c16*)GlobalLock(hHandle);
					if( pText )
					{
						for( u32 i=0; pText[i] != 0; i++ )
						{
							change |= InsertChara(pText[i]);
						}
						GlobalUnlock(hHandle);
					}
				}
				CloseClipboard();
				ret = true;
			}
			if( change )
			{
				PushHistory();
			}
			return ret;
		}

		void CEditText::OnDelete(void)
		{
			if( strText.length() )
			{
				bool change = false;
				if( iCaret != iSelectStart )
				{
					change |= DeleteSelection();
				}
				else if( iCaret < (s32)strText.length() )
				{
					strText.erase(iCaret, 1);
					iSelectStart = iCaret;
					iBlink = 0;
					change = true;
				}
				if( change )
				{
					PushHistory();
				}
			}
		}
		void CEditText::OnBack(void)
		{
			if( strText.length() )
			{
				bool change = false;
				if( iCaret != iSelectStart )
				{
					change |= DeleteSelection();
				}
				else if( iCaret > 0 )
				{
					strText.erase(iCaret-1, 1);
					iCaret = Max(iCaret-1, 0);
					iSelectStart = iCaret;
					iBlink = 0;
					change = true;
				}
				if( change )
				{
					PushHistory();
				}
			}
		}
		void CEditText::OnLeft(void)
		{
			if( iCaret > 0 )
			{
				iCaret --;
			}
			if( GetKeyState(VK_SHIFT) >= 0 )
			{
				iSelectStart = iCaret;
			}
			iBlink = 0;
		}
		void CEditText::OnRight(void)
		{
			if( iCaret < (s32)strText.length() )
			{
				iCaret ++;
			}
			if( GetKeyState(VK_SHIFT) >= 0 )
			{
				iSelectStart = iCaret;
			}
			iBlink = 0;
		}
		void CEditText::OnInsert(void)
		{
			bInsertMode = !bInsertMode;
		}

		void CEditText::OnAllSelect(void)
		{
			iCaret = 0;
			iSelectStart = strText.length();
		}

		void CEditText::OnPrev(void)
		{
			if( 0 < iCurrentStack )
			{
				iCurrentStack --;
				strText	= Histories[iCurrentStack].strText;
				iCaret	= Histories[iCurrentStack].iCaret;
				iSelectStart = iCaret;
				iBlink = 0;
			}
		}
		void CEditText::OnNext(void)
		{
			if( iCurrentStack + 1 < (s32)Histories.size() )
			{
				iCurrentStack ++;
				strText	= Histories[iCurrentStack].strText;
				iCaret	= Histories[iCurrentStack].iCaret;
				iSelectStart = iCaret;
				iBlink = 0;
			}
		}
		void CEditText::PushHistory(void)
		{
			if( iCurrentStack < MaxHistory )
			{
				iCurrentStack ++;
			}
			else
			{
				Histories.erase(Histories.begin());
			}
			Histories.resize(iCurrentStack+1);
			Histories[iCurrentStack].strText	= strText;
			Histories[iCurrentStack].iCaret	= iCaret;
			iSelectStart = iCaret;
			iBlink = 0;
		}
		void CEditText::ResetHistory(void)
		{
			iCurrentStack = -1;
			Histories.clear();

			PushHistory();
		}
		void CEditText::SetMaxCandList(u32 uiMax)
		{
			if( pTsfManager.IsValid() )
			{
				pTsfManager->SetMaxCandList(uiMax);
			}
		}
		
		void CEditText::Update(void)
		{
			if( !pTsfManager.IsValid() ) return;

			SCOPE_LOCK_CRITICALSECTION(pTsfManager->m_pCS);
			strTextTemp		= strText;
			strCompStrTemp	= strCompStr;
			ucCompAttrTemp	= ucCompAttr;
			strCandidate	= pTsfManager->GetCandidate();
			uiSelection		= pTsfManager->GetSelection();

			iBlink ++;
		}

		void CEditText::Update(HWND hWnd, s32 iMsg, s32 wParam, s32 lParam)
		{
			if( !IsForcus() )
			{
				return;
			}

			switch( iMsg )
			{
			//case WM_IME_SETCONTEXT:
			case WM_IME_STARTCOMPOSITION:
			case WM_IME_ENDCOMPOSITION:
				ClearCompText();
				break;
			case WM_IME_COMPOSITION:
			{
				if( !IsEnable() ) break;

				auto hImc = ImmGetContext(hWnd);
				wstring sCompStr;
				if( lParam & GCS_RESULTSTR )
				{
					dword size = ImmGetCompositionString(hImc, GCS_RESULTSTR, null, 0);
					dword len = (size / sizeof(c16));
					sCompStr.resize(len);
					ImmGetCompositionString(hImc, GCS_RESULTSTR, &sCompStr[0], size);
					ClearCompText();

					bool ret = false;
					ret |= DeleteSelection();
					ret |= AddString(sCompStr);
					if( ret )
					{
						PushHistory();
					}
				}
				if( lParam & GCS_COMPSTR )
				{
					dword size = ImmGetCompositionString(hImc, GCS_COMPSTR, null, 0);
					dword len = (size / sizeof(c16));
					sCompStr.resize(len);
					ImmGetCompositionString(hImc, GCS_COMPSTR, &sCompStr[0], size);
					vector<u8> ucCompAttr;
					if( len > 0 )
					{
						ucCompAttr.resize(len);
						ImmGetCompositionString(hImc, GCS_COMPATTR, &ucCompAttr[0], len);
					}
					s32 iCursol = ImmGetCompositionString(hImc, GCS_CURSORPOS, null, 0);
					SetCompText(sCompStr, ucCompAttr, iCursol);
				}
				ImmReleaseContext(hWnd, hImc);
			}
			break;

			case WM_IME_NOTIFY:
			{
				if( !IsEnable() ) break;

				switch (wParam)
				{
				case IMN_OPENCANDIDATE:
				case IMN_CHANGECANDIDATE:
				{
					auto hImc = ImmGetContext(hWnd);

					ImmSetOpenStatus(hImc, true);
					dword size = ImmGetCandidateList(hImc, 0, null, 0);
					if( size > 0 )
					{
						TSmartPtr<u8> pBuffer = new u8[size];
						LPCANDIDATELIST	pCandList = (LPCANDIDATELIST)pBuffer;
						size = ImmGetCandidateList(hImc, 0, pCandList, size);

						s32 i;
						s32 mbBufLen = 0;
						for ( i = 0; i < (s32)pCandList->dwCount; i++ )
						{
							mbBufLen += WideCharToMultiByte(0, 0, (c16*)((c8*)pCandList + pCandList->dwOffset[i]), -1, NULL, 0, NULL, NULL );
						}
						mbBufLen += pCandList->dwOffset[0];
						LPCANDIDATELIST pResult = (LPCANDIDATELIST)malloc(mbBufLen);
						LPCANDIDATELIST pCandListA = pResult;
						memcpy(pCandListA, pCandList, pCandList->dwOffset[0]);

						LPSTR psz = (LPSTR)pCandListA + pCandList->dwOffset[0];
						for (i = 0; i < (s32)pCandList->dwCount; i++)
						{
							pCandListA->dwOffset[i] = (dword)((LPSTR)psz - (LPSTR)pCandListA);
							psz += WideCharToMultiByte(0, 0, (LPWSTR)((LPSTR)pCandList + pCandList->dwOffset[i]), -1, psz, 256, NULL, NULL );
						}
						size = mbBufLen;
						free(pResult);
					}
					ImmReleaseContext(hWnd, hImc);
				}
				break;
				}
			}
			break;

			case WM_KEYDOWN:
			{
				if( !IsEnable() ) break;

				switch( wParam )
				{
				case VK_DELETE:	OnDelete();	break;
				case VK_LEFT:	OnLeft();	break;
				case VK_RIGHT:	OnRight();	break;
				case VK_INSERT:	OnInsert();	break;
				}
			}
			break;

			case WM_CHAR:
			{
				switch( wParam )
				{
				case VK_BACK:
					OnBack();
					break;

				case 24:		// Ctrl-X Cut
				case VK_CANCEL:	// Ctrl-C Copy
					CopyClipBoard((s32)wParam == 24);
					break;
				case 22:	// Ctrl-V Paste
					PasteClipBoard();
					break;

				case 1:		// Ctrl A
					OnAllSelect();
					break;
				case 26:	// Ctrl Z
					OnPrev();
					break;
				case 25:	// Ctrl Y
					OnNext();
					break;

				case 2:   // Ctrl B
				case 14:  // Ctrl N
				case 19:  // Ctrl S
				case 4:   // Ctrl D
				case 6:   // Ctrl F
				case 7:   // Ctrl G
				case 10:  // Ctrl J
				case 11:  // Ctrl K
				case 12:  // Ctrl L
				case 17:  // Ctrl Q
				case 23:  // Ctrl W
				case 5:   // Ctrl E
				case 18:  // Ctrl R
				case 20:  // Ctrl T
				case 21:  // Ctrl U
				case 9:   // Ctrl I
				case 15:  // Ctrl O
				case 16:  // Ctrl P
				case 27:  // Ctrl [
				case 29:  // Ctrl ]
				case 28:  // Ctrl ￥
					break;

				case VK_RETURN:
					if( !bLineBreak ) break;
					wParam = L'\n';
				default:
				{
					if( !IsEnable() ) break;

					bool ret = false;
					ret = DeleteSelection();
					ret = AddChara((c16)wParam);
					if( ret )
					{
						PushHistory();
					}
				}
				break;
				}
			}
			break;
			}
		}
	}
}
//#endif // KGL_FINAL_RELEASE

//=======================================================================
//	END OF FILE
//=======================================================================
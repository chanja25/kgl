//----------------------------------------------------------------------
//!
//!	@file	Script.cpp
//!	@brief	スクリプト関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Script.h"

void ScriptTest(void)
{
	char* pPath =
		//"Data/Script/cp_add.c";//    CCI-C用サンプルプログラム
		//"Data/Script/cp_prime.c";//  CCI-C用サンプルプログラム
		"Data/Script/cp_qsort.c";//  CCI-C用サンプルプログラム
		//"Data/Script/cp_base.c";//   CCI-C用サンプルプログラム

	Script::CScriptCompiler compiler;

	compiler.Compile(pPath, null);

	compiler.CodeDump();

	compiler.Execute();
}

namespace Script
{
}


//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	ScriptCompiler.h
//!	@brief	スクリプトコンパイラー
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCRIPT_COMPILER_H__
#define	__SCRIPT_COMPILER_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../include/App.h"
#include "../include/File/kgl.File.h"
#include "../include/File/kgl.Directory.h"


namespace Script
{
	//======================================================================
	//!	スクリプト管理クラス
	//======================================================================
	class CScriptCompiler
	{
	public:
		//!	コンストラクタ
		CScriptCompiler(void);
		//!	デストラクタ
		~CScriptCompiler(void);

	public:
		//!	コンパイル
		b8 Compile(const c8* pPath, const c8* pOutput);

		//!	コードを表示
		void CodeDump(void);
		//!	実行
		s32 Execute(void);

	private:
		//!	作業ファイルのオープン
		b8 OpenFile(const c8* pPath);

	private:
		//!	メモリの初期化
		void InitMemory(void);
		//!	文字タイプの初期化
		void InitCharType(void);
		//!	キーワードの初期化
		void InitKeyword(void);

		//!	メモリ確保
		s32 AllocateL(s32 size);
		//!	メモリ確保
		s32 AllocateG(s32 size);
		//!	メモリ確保(文字列)
		s32 AllocateS(const string& str);

		//!	実際のアドレスを取得
		s8* MemAdrs(s32 iAdrs);
		//!	実際のデータ参照
		s32& IntAdrs(s32 iAdrs);

	private:
		//!	定数畳込
		s32 ConstFold(EOpCode eOp);
		//!	コードの生成
		INLINE s32 GenCode1(EOpCode eOp) { return GenCode3(eOp, 0, 0); }
		//!	コードの生成
		INLINE s32 GenCode2(EOpCode eOp, s32 iData) { return GenCode3(eOp, 0, iData); }
		//!	コードの生成
		s32 GenCode3(EOpCode eOp, u8 ucFlag, s32 iData);

		//!	単項命令生成
		void GenCodeUnary(ETokenKind eKind);
		//!	二項命令生成
		void GenCodeBinary(ETokenKind eKind);

	private:
		//!	記号表の登録
		s32 Enter(const Symbol& symbol, ESymbolKind eKind);
		//!	重複チェック
		void NameCheck(const Symbol& symbol);
		//!	重複登録の解消
		void DelFuncTable(Symbol* f1, Symbol* f2);

		//!	変数・引数の番地設定
		void SetAdrs(Symbol* pSym);

		//!	局所記号表の開始位置を設定する
		void SetLocalTablePos(void);
		//!	局所記号表の位置を元に戻す
		void ResetLocalTablePos(const Symbol* pSym);

		//!	記号表の検索
		s32 Search(const string& strName);
		//!	記号表の検索
		s32 SearchName(const string& strName);

	private:
		//!	二項演算子か？
		b8 IsBinaryOp(EOpCode eOp);
		//!	定数演算
		s32 BinaryExpr(EOpCode eOp, s32 iData1, s32 iData2);

		//!	2文字からなる演算子か？(++,--等)
		b8 IsOp2(c8 c1, c8 c2);
		//!	コードが一致するか？
		b8 IsCode(s32 n, EOpCode eOp, s32 iData);

		//!	トークンの種類を設定
		void SetKind(Token& token);
		//!	トークンタイプの設定
		void SetType(void);
		//!	トークン名の設定
		void SetName(void);
		//!	関数宣言
		void FuncDecl(void);
		//!	関数の重複チェック
		void FuncCheck(Symbol* f1, Symbol* f2);

		//!	関数入口処理
		void FuncDeclBegin(void);
		//!	関数出口処理
		void FuncDeclEnd(void);
		//!	main関数処理
		void SetMain(void);

		//	{}内の処理
		void Block(s32 iNest);
		//	文の処理
		void Statement(void);
		//	関数呼び出し
		void SystemFuncCall(ETokenKind eKind);
		//	関数呼び出し
		void FuncCall(Symbol* pSym);

		//!	ループ文の開始
		void ContinueBreakBegin(ETokenKind eState);
		//!	ループ文の終了
		void ContinueBreakEnd(void);
		//!	ループ文の先頭番地を取得
		s32 GetLoopTop(void);
		//!	switch文の開始
		void SwitchBegin(void);
		//!	switch文の終了
		void SwitchEnd(void);

		//!	コードの未定データを処理
		void BackPatch(s32 n, s32 iAdrs);
		//!	CALLの未定番地を確定
		void BackPatchCallAdrs(void);
		//!	return関連の未定番地を確定
		void BackPatchRetrun(s32 iFuncAdrs);
		//!	break関連の未定番地を確定
		void BackPatchBreak(s32 iLoopTop);

	private:
		//!	変数宣言
		void VarDecl(void);
		//!	配列サイズ設定
		void SetArrayLen(void);
		//!	定数の取得
		b8 GetConst(s32* pVar);

		//!	左辺値に変更する
		void ToLeftValue(void);
		//!	式の値を削除する
		void RemoveValue(void);

		//!	式の処理
		void Expression(void);
		//!	項関連処理
		void Term(s32 n);
		//!	因子関連処理
		void Factor(void);

	private:
		//!	次のトークンへ進める
		const Token* NextToken(void);
		//!	次のトークンへ進める(エラーチェック付き)
		const Token* CheckNextToken(c8 c);
		//!	前後のトークンチェック付き解析
		void ExprWithCheck(c8 c1, c8 c2);
		//!	次の文字
		c8 NextChar(void);
		//!	次の文字の読み込み
		c8 ReadNextChar(void);
		//!	前の文字の読み込み
		void SeekPrevChar(void);

	private:
		//!	スタック情報の追加
		void Push(s32 iData);
		//!	スタック情報の取り出し
		s32 Pop(void);
		//!	スタック情報の末尾を取得
		s32 Last(void);
		//!	組み込み関数の呼び出し
		void CallLibrary(s32 iOpData);

	private:
		TKGLPtr<File::IFile>	m_pFile;	//!< ファイル操作用

		ETokenKind				m_eTknKindTbl[256];	//!< トークンの種類判定用テーブル
		vector<Keyword>			m_Keyword;			//!< キーワード
		vector<Inst>			m_Code;				//!< プログラムコード
		vector<Symbol>			m_SymbolTable;		//!< シンボルテーブル
		vector<LoopNest>		m_LoopNest;			//!< ループネスト情報
		vector<SwitchNest>		m_SwitchNest;		//!< switchネスト情報
		vector<CaseList>		m_CaseList;			//!< case情報
		Token					m_Token;			//!< 次のトークン
		Symbol					m_Symbol;			//!< シンボル情報
		s32						m_iFuncId;			//!< 関数定義用
		ETokenKind				m_eLastState;		//!< ブロック内の最後の文
		s8*						m_pMemory;			//!< スクリプト用メモリ
		s32						m_iMemSize;			//!< メモリサイズ
		s32						m_iMemCounter;		//!< メモリ管理用カウンタ
		s32						m_iCodeCount;		//!< 定数判定用のカウンタ
		s32						m_iLocalTblCount;	//!< 局所テーブル制御用
		s32						m_iLocalAdrs;		//!< 局所アドレス制御用
		s32						m_iLineNo;			//!< 現在の行
		c8						m_cChar;			//!< 解析する文字
		c8						m_cQuotChar;		//!< "'の文字列処理用
		s8						m_cBlockNest;		//!< ブロックのネスト情報
		s8						m_pad;

	private:
		s32						m_iPgCounter;		//!< スクリプト実行用カウンタ
		s32						m_iStackPtr;		//!< スタック管理用
		s32						m_iStack[1024];		//!< スタック領域
	};
}

#endif	// __SCRIPT_COMPILER_H__
//======================================================================
//	END OF FILE
//======================================================================
//----------------------------------------------------------------------
//!
//!	@file	ScriptData.h
//!	@brief	軸解析
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCRIPT_DATA_H__
#define	__SCRIPT_DATA_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../include/App.h"
#include "../include/File/kgl.File.h"
#include "../include/File/kgl.Directory.h"


namespace Script
{
	const s32 NoFixReturnAdrs	= -102;
	const s32 NoFixBreakAdrs	= -103;

	//!	組み込み関数
	enum ESystemFunc
	{
		Exit_F = 1,
		Input_F,
		Printf1_F,
		Printf2_F,
	};
	//!	データタイプ
	enum EDataType
	{
		Non_T, Void_T, Int_T,
	};

	//!	トークンの種類
	enum ETokenKind
	{
		Lparen	= '(',	Rparen		= ')',
		Lbrace	= '{',	Rbrace		= '}',
		Lbracket= '[',	Rbracket	= ']',
		Plus	= '+',	Minus		= '-',
		Multi	= '*',	Division	= '/',
		Mod		= '%',	Not			= '!',
		Colon	= ':',	Semicolon	= ';',
		Assign	= '=',	Sharp		= '#',
		Yen		= '\\',	Comma		= ',',
		SngQ	= '\'',	DblQ		= '\"',

		TextTokenStart = 160,
		Void, Int,
		//Void, Char, Short, Int, Int64, Float, Double,
		If, Else, For, While, Do, Switch, Case,
		Default, Break, Continue, Return, Incre, Decre,
		Printf, Input, Exit,
		Equal, NotEq, Less, LessEq, Greater, GreaterEq, And, Or,

		Ident, IntNum, String, NulKind, Letter, Digit, EofToken, Others,
		TokenKindMax,
	};
	//	二項演算子の優先順位取得
	INLINE s32 OpOrder(ETokenKind eKind)
	{
	  switch( eKind )
	  {
	  case Multi:
	  case Division:
	  case Mod:
		  return 7;
	  case Plus:
	  case Minus:
		  return 6;
	  case Less:
	  case LessEq:
	  case Greater:
	  case GreaterEq:
		  return 5;
	  case Equal:
	  case NotEq:
		  return 4;
	  case And:
		  return 3;
	  case Or:
		  return 2;
	  case Assign:
		  return 1;
	  }
	  return 0;
	}

	//======================================================================
	//!	トークン情報
	//======================================================================
	struct Token
	{
		ETokenKind	eKind;		//!< 種類
		s32			iValue;		//!< 値(又はアドレス)
		string		strToken;	//!< 文字列
	};

	//======================================================================
	//!	キーワード情報
	//======================================================================
	struct Keyword
	{
		string		strKey;	//!< キー
		ETokenKind	eKind;	//!< トークンの種類
	};

	//!	シンボルの種類
	enum ESymbolKind
	{
		NoId, VarId, FuncId, ProtId, ParamId,
	};
	//======================================================================
	//!	シンボル情報
	//======================================================================
	struct Symbol
	{
		string		strName;	//!< シンボル名
		ESymbolKind	eKind;		//!< 種類
		EDataType	eDataType;	//!< データタイプ
		s32			iArrayLen;	//!< 配列長
		s8			cLevel;		//!< 定義レベル(0:大域, 1:局所)
		s8			cArgs;		//!< 引数の個数(関数の場合のみ)
		s8			cPad[2];
		s32			iAdrs;		//!< 番地(アドレス)
	};

	//!	命令コード
	enum EOpCode
	{
		NOP, INC, DEC, NEG, NOT, ADD, SUB, MUL, DIV, MOD,
		LESS, LSEQ, GRT, GTEQ, EQU, NTEQ, AND, OR,
		CALL, DEL, JMP, JPT, JPF, EQCMP,
		LOD, LDA, LDI, STO, ADBR, RET, ASS, ASSV, VAL, LIB, STOP,
	};
	//======================================================================
	//!	シンボル情報
	//======================================================================
	struct Inst
	{
		u8	ucOpCode;	//!< 命令コード
		u8	ucFlag;		//!< フラグ
		u8	pad[2];
		s32	iOpData;	//!< 数値、又は番地
	};

	//======================================================================
	//!	ループ文のネスト情報
	//======================================================================
	struct LoopNest
	{
		ETokenKind	eKind;			//!< 文の種類
		s32			iLoopTop;		//!< ループの開始番地
		BITFIELD	bBreakFlag: 1;	//!< breakの有無
	};
	//======================================================================
	//!	switchのネスト情報
	//======================================================================
	struct SwitchNest
	{
		s32	iDefAdrs;		//!< defaultの対応番地
		s32	iStartCaseList;	//!< caseListの開始位置
	};
	//======================================================================
	//!	case情報
	//======================================================================
	struct CaseList
	{
		s32	iValue;		//!< ケースに対応する値
		s32	iAdrs;		//!< case処理の対応番地
	};
}

#endif	// __SCRIPT_DATA_H__
//======================================================================
//	END OF FILE
//======================================================================
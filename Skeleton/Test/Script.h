//----------------------------------------------------------------------
//!
//!	@file	Script.h
//!	@brief	スクリプト関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
#ifndef	__SCRIPT_H__
#define	__SCRIPT_H__

//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "../include/App.h"
#include "ScriptData.h"
#include "ScriptCompiler.h"

namespace Script
{
	//======================================================================
	//!	スクリプト管理クラス
	//======================================================================
	class CScriptManager
	{
	public:
		//!	コンストラクタ
		CScriptManager(void);
		//!	デストラクタ
		~CScriptManager(void);

	private:
	};
}

#endif	// __SCRIPT_H__
//======================================================================
//	END OF FILE
//======================================================================
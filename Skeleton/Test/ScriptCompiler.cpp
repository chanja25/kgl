//----------------------------------------------------------------------
//!
//!	@file	ScriptCompiler.cpp
//!	@brief	スクリプトコンパイル関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Script.h"


namespace Script
{
	//----------------------------------------------------------------------
	//	コンストラクタ
	//----------------------------------------------------------------------
	CScriptCompiler::CScriptCompiler(void)
		: m_pMemory(null)
		, m_iMemSize(0)
		, m_iMemCounter(0)
		, m_iCodeCount(0)
		, m_iLocalTblCount(0)
		, m_iLocalAdrs(0)
		, m_iLineNo(0)
		, m_cChar(' ')
		, m_cQuotChar(0)
		, m_cBlockNest(0)
	{
		InitMemory();
		InitCharType();
		InitKeyword();

		Symbol dummy;
		m_SymbolTable.push_back(dummy);
	}
	//----------------------------------------------------------------------
	//	デストラクタ
	//----------------------------------------------------------------------
	CScriptCompiler::~CScriptCompiler(void)
	{
		//	メモリの開放
		if( m_pMemory )
		{
			Memory::Free(m_pMemory);
		}
	}

	//----------------------------------------------------------------------
	//	コンパイル
	//----------------------------------------------------------------------
	b8 CScriptCompiler::Compile(const c8* pPath, const c8* pOutput)
	{
		//	スクリプトの起動・終了コードを生成
		GenCode2(CALL, -1);
		GenCode1(STOP);

		if( !OpenFile(pPath) )
		{
			return false;
		}

		const Token* pToken = NextToken();
		while( pToken->eKind != EofToken )
		{
			switch( pToken->eKind )
			{
			case Int:
			case Void:
				SetType();
				SetName();
				if( pToken->eKind == '(' )
				{
					FuncDecl();
				}
				else
				{
					VarDecl();
				}
				break;
			case Semicolon:
				pToken = NextToken();
				break;
			default:
				ErrorTrace("構文エラー[%s]", pToken->strToken.c_str());
				pToken = NextToken();
				break;
			}
		}

		//if( iErrCount == 0 )
		{
			BackPatchCallAdrs();
		}
		*(s32*)MemAdrs(0) = AllocateG(0);

		//	todo:未実装
		//if( iErrCount > 0 )
		//{
		//	ErrorTrace("%d個のエラーが発生しました", iErrCount);
		//}
		//return iErrCount == 0;
		return true;
	}
	//----------------------------------------------------------------------
	//	作業ファイルのオープン
	//----------------------------------------------------------------------
	b8 CScriptCompiler::OpenFile(const c8* pPath)
	{
		TKGLPtr<File::IFile> pFile;
		File::Create(pFile.GetReference());

		if( !pFile->Open(pPath) )
		{
			ErrorTrace("指定されたファイルが見つかりません(%s)", pPath);
			return false;
		}
		m_pFile = pFile;

		return true;
	}
	//----------------------------------------------------------------------
	//	メモリの初期化
	//----------------------------------------------------------------------
	void CScriptCompiler::InitMemory(void)
	{
		//	メモリの開放
		if( m_pMemory )
		{
			Memory::Free(m_pMemory);
		}
		m_iMemSize = 65536;
		m_iMemCounter = sizeof(s32);

		m_pMemory = (s8*)Memory::Allocate(m_iMemSize);
	}
	//----------------------------------------------------------------------
	//	文字タイプの初期化
	//----------------------------------------------------------------------
	void CScriptCompiler::InitCharType(void)
	{
		s32 i;
		for( i = 0;   i < 256;  i++ )	m_eTknKindTbl[i] = Others;
		for( i = '0'; i <= '9'; i++ )	m_eTknKindTbl[i] = Digit;
		for( i = 'A'; i <= 'Z'; i++ )	m_eTknKindTbl[i] = Letter;
		for( i = 'a'; i <= 'z'; i++ )	m_eTknKindTbl[i] = Letter;

		m_eTknKindTbl['_'] = Letter;
		m_eTknKindTbl['('] = Lbrace;	m_eTknKindTbl['('] = Rparen;
		m_eTknKindTbl['{'] = Lbracket;	m_eTknKindTbl['}'] = Rbracket;
		m_eTknKindTbl['['] = Lparen;	m_eTknKindTbl[']'] = Rparen;
		m_eTknKindTbl['<'] = Less;		m_eTknKindTbl['>'] = Greater;
		m_eTknKindTbl['+'] = Plus;		m_eTknKindTbl['-'] = Minus;
		m_eTknKindTbl['*'] = Multi;		m_eTknKindTbl['/'] = Division;
		m_eTknKindTbl['!'] = Not;		m_eTknKindTbl['%'] = Mod;
		m_eTknKindTbl[':'] = Colon;		m_eTknKindTbl[';'] = Semicolon;
		m_eTknKindTbl['='] = Assign;
		m_eTknKindTbl['\\'] = Yen;		m_eTknKindTbl[','] = Comma;
		m_eTknKindTbl['\''] = SngQ;		m_eTknKindTbl['\"'] = DblQ;
	}
	//----------------------------------------------------------------------
	//	キーワードの初期化
	//----------------------------------------------------------------------
	void CScriptCompiler::InitKeyword(void)
	{
#define	DECL_KEY(id, word)	case id: key.eKind = id; key.strKey = word; break

		Keyword key;
		for( s32 i=0; i < TokenKindMax; i++ )
		{
			switch( i )
			{
				DECL_KEY(Void,		"void");
				DECL_KEY(Int,		"int");
				DECL_KEY(If,		"if");
				DECL_KEY(Else,		"else");
				DECL_KEY(For,		"for");
				DECL_KEY(While,		"while");
				DECL_KEY(Do,		"do");
				DECL_KEY(Switch,	"switch");
				DECL_KEY(Case,		"case");
				DECL_KEY(Default,	"default");
				DECL_KEY(Break,		"break");
				DECL_KEY(Continue,	"continue");
				DECL_KEY(Return,	"return");
				DECL_KEY(Printf,	"printf");
				DECL_KEY(Input,		"input");
				DECL_KEY(Exit,		"exit");
				DECL_KEY(Lparen,	"(");
				DECL_KEY(Rparen,	")");
				DECL_KEY(Lbrace,	"{");
				DECL_KEY(Rbrace,	"}");
				DECL_KEY(Lbracket,	"[");
				DECL_KEY(Rbracket,	"]");
				DECL_KEY(Plus,		"+");
				DECL_KEY(Minus,		"-");
				DECL_KEY(Multi,		"*");
				DECL_KEY(Division,	"/");
				DECL_KEY(Incre,		"++");
				DECL_KEY(Decre,		"--");
				DECL_KEY(Equal,		"==");
				DECL_KEY(NotEq,		"!=");
				DECL_KEY(Less,		"<");
				DECL_KEY(LessEq,	"<=");
				DECL_KEY(Greater,	">");
				DECL_KEY(GreaterEq,	">=");
				DECL_KEY(And,		"&&");
				DECL_KEY(Or,		"||");
				DECL_KEY(Not,		"!");
				DECL_KEY(Mod,		"%");
				DECL_KEY(Colon,		":");
				DECL_KEY(Semicolon,	";");
				DECL_KEY(Assign,	"=");
				DECL_KEY(Sharp,		"#");
				DECL_KEY(Yen,		"\\");
				DECL_KEY(Comma,		",");
				DECL_KEY(SngQ,		"\'");
				DECL_KEY(DblQ,		"\"");

			default:
				//	設定するものが無い場合は次へ進める
				continue;
			}
			m_Keyword.push_back(key);
		}
	}
	//----------------------------------------------------------------------
	//	メモリ確保
	//----------------------------------------------------------------------
	s32 CScriptCompiler::AllocateL(s32 size)
	{
		if( size < 0 ) size = 0;

		m_iLocalAdrs += size;
		return m_iLocalAdrs - size;
	}
	//----------------------------------------------------------------------
	//	メモリ確保
	//----------------------------------------------------------------------
	s32 CScriptCompiler::AllocateG(s32 size)
	{
		if( size <= 0 ) return m_iMemCounter;

		m_iMemCounter = Align(m_iMemCounter, 4);
		//	メモリの使用量を拡張する
		if( m_iMemCounter + size > m_iMemSize )
		{
			s32 iNewSize = m_iMemSize + 65536;
			s8* pNewMem = (s8*)Memory::Allocate(iNewSize);

			kgMemcpy(pNewMem, m_pMemory, m_iMemSize);

			Memory::Free(m_pMemory);
			m_pMemory = pNewMem;
			m_iMemSize = iNewSize;
		}

		kgZeroMemory(m_pMemory + m_iMemCounter, size);
		m_iMemCounter += size;

		return m_iMemCounter - size;
	}
	//----------------------------------------------------------------------
	//	メモリ確保
	//----------------------------------------------------------------------
	s32 CScriptCompiler::AllocateS(const string& str)
	{
		s32 iAdrs = AllocateG(str.length() + 1);
		kgMemcpy(MemAdrs(iAdrs), str.c_str(), str.length() + 1);

		return iAdrs;
	}
	//----------------------------------------------------------------------
	//	実際のアドレスを取得
	//----------------------------------------------------------------------
	s8* CScriptCompiler::MemAdrs(s32 iAdrs)
	{
		if( iAdrs < 0 || m_iMemCounter < iAdrs )
		{
			return null;
		}
		return m_pMemory + iAdrs;
	}
	//----------------------------------------------------------------------
	//	実際のアドレスを取得
	//----------------------------------------------------------------------
	s32& CScriptCompiler::IntAdrs(s32 iAdrs)
	{
		return *(s32*)(m_pMemory + iAdrs);
	}
	//----------------------------------------------------------------------
	//	定数畳込
	//----------------------------------------------------------------------
	s32 CScriptCompiler::ConstFold(EOpCode eOp)
	{
		s32 n = m_Code.size() - 1;
		if( n <= 1 || m_Code[n].ucOpCode != LDI ) return false;
		s32 iData = m_Code[n].iOpData;

		if( eOp == NOT ) { m_Code[n].iOpData = !iData; return true; }
		if( eOp == NEG ) { m_Code[n].iOpData = -iData; return true; }
		if( m_Code[n-1].ucOpCode == LDI && IsBinaryOp(eOp) )
		{
			m_Code[n-1].iOpData = BinaryExpr(eOp, m_Code[n-1].iOpData, iData);
			m_Code.pop_back();
			return true;
		}
		return false;
	}
	//----------------------------------------------------------------------
	//	コードの生成
	//----------------------------------------------------------------------
	s32 CScriptCompiler::GenCode3(EOpCode eOp, u8 ucFlag, s32 iData)
	{
		s32 iCodeCount = m_Code.size();
		if( ConstFold(eOp) )
		{
			return iCodeCount;
		}
		//	コードサイズの最大を超える場合はサイズを拡張する
		if( m_Code.size() == m_Code.capacity() )
		{
			m_Code.reserve(m_Code.capacity() + 1024);
		}

		Inst code;
		code.ucOpCode	= eOp;
		code.ucFlag		= ucFlag;
		code.iOpData	= iData;

		m_Code.push_back(code);

		return iCodeCount;
	}
	//----------------------------------------------------------------------
	//	単項命令生成
	//----------------------------------------------------------------------
	void CScriptCompiler::GenCodeUnary(ETokenKind eKind)
	{
		EOpCode eOp = NOP;

		switch( eKind )
		{
		case Plus:	return;
		case Minus:	eOp = NEG;	break;
		case Not:	eOp = NOT;	break;
		case Incre:	eOp = INC;	break;
		case Decre:	eOp = DEC;	break;
		}
		GenCode1(eOp);
	}
	//----------------------------------------------------------------------
	//	単項命令生成
	//----------------------------------------------------------------------
	void CScriptCompiler::GenCodeBinary(ETokenKind eKind)
	{
		EOpCode eOp = NOP;

		switch( eKind )
		{
		case Plus:		eOp = ADD;	break;
		case Minus:		eOp = SUB;	break;
		case Multi:		eOp = MUL;	break;
		case Division:	eOp = DIV;	break;
		case Less:		eOp = LESS;	break;
		case LessEq:	eOp = LSEQ;	break;
		case Greater:	eOp = GRT;	break;
		case GreaterEq:	eOp = GTEQ;	break;
		case Equal:		eOp = EQU;	break;
		case NotEq:		eOp = NTEQ;	break;
		case And:		eOp = AND;	break;
		case Or:		eOp = OR;	break;
		case Mod:		eOp = MOD;	break;
		}
		GenCode1(eOp);
	}
	//----------------------------------------------------------------------
	//	記号表の登録
	//----------------------------------------------------------------------
	s32 CScriptCompiler::Enter(const Symbol& symbol, ESymbolKind eKind)
	{
		s32 n = m_SymbolTable.size();
		m_SymbolTable.push_back(symbol);

		Symbol& newSymbol = m_SymbolTable[n];
		if( (eKind == VarId || eKind==ParamId) && symbol.eDataType == Void_T )
		{
			ErrorTrace("変数型が正しくありません");
			newSymbol.eDataType = Int_T;
		}
		newSymbol.eKind = eKind;
		NameCheck(symbol);

		if( eKind == ParamId )	++ newSymbol.cLevel;
		if( eKind == VarId )	SetAdrs(&newSymbol);
		if( eKind == FuncId )	newSymbol.iAdrs = -n;

		return n;
	}
	//----------------------------------------------------------------------
	//	記号表の登録
	//----------------------------------------------------------------------
	void CScriptCompiler::NameCheck(const Symbol& symbol)
	{
		if( symbol.eKind != ParamId && symbol.eKind != VarId )
		{
			return;
		}
		s32 iFunc = SearchName(symbol.strName);
		if( iFunc == -1 )	return;

		c8 cNest = m_cBlockNest;
		if( symbol.eKind == ParamId )
		{
			++ cNest;
		}
		if( symbol.cLevel >= cNest )
		{
			ErrorTrace("識別子が重複しています[%s]", symbol.strName.c_str());
		}
	}
	//----------------------------------------------------------------------
	//	重複登録の解消
	//----------------------------------------------------------------------
	void CScriptCompiler::DelFuncTable(Symbol* f1, Symbol* f2)
	{
		if( f1 == null )	return;
		if( f1->eDataType != f2->eDataType || f1->cArgs != f2->cArgs ) return;
		if( f1->eKind == ProtId && f2->eKind == FuncId )
		{
			for( s32 i=0; i <= f2->cArgs; i++ )
			{
				*(f1 + i) = *(f2 + i);
			}
		}

		s32 iCount = f2->cArgs + 1;
		s32 iTblPos = m_SymbolTable.size() - iCount;
		m_SymbolTable.erase(iTblPos, iCount);
	}
	//----------------------------------------------------------------------
	//	変数・引数の番地設定
	//----------------------------------------------------------------------
	void CScriptCompiler::SetAdrs(Symbol* pSym)
	{
		s32 iSize = sizeof(s32);
		switch( pSym->eKind )
		{
		case VarId:
			if( pSym->iArrayLen > 0 )	iSize *= pSym->iArrayLen;
			if( m_cBlockNest == 0 )		pSym->iAdrs = AllocateG(iSize);
			else						pSym->iAdrs = AllocateL(iSize);
			break;

		case FuncId:
			pSym->iAdrs = m_Code.size();
			for( s32 i=1; i <= pSym->cArgs; i++ )
			{
				(pSym+i)->iAdrs = AllocateL(iSize);
			}
			break;
		}
	}
	//----------------------------------------------------------------------
	//	局所記号表の開始位置を設定する
	//----------------------------------------------------------------------
	void CScriptCompiler::SetLocalTablePos(void)
	{
		m_iLocalTblCount = m_SymbolTable.size();
	}
	//----------------------------------------------------------------------
	//	局所記号表の位置を元に戻す
	//----------------------------------------------------------------------
	void CScriptCompiler::ResetLocalTablePos(const Symbol* pSym)
	{
		s32 iTblPos = m_iLocalTblCount + pSym->cArgs;
		s32 iCount = m_SymbolTable.size() - iTblPos;
		m_SymbolTable.erase(iTblPos, iCount);
		m_iLocalTblCount = 2^31;
	}
	//----------------------------------------------------------------------
	//	記号表の検索
	//----------------------------------------------------------------------
	s32 CScriptCompiler::Search(const string& strName)
	{
		s32 iFunc = SearchName(strName);
		if( iFunc == -1 )
		{
			ErrorTrace("未定義の識別子[%s]", strName.c_str());
			static Symbol dummy =
			{
				"dummy$name2", VarId, Int_T, 0, 0, 0, 0
			};

			static s32 n = Enter(dummy, VarId);
			iFunc = n;
		}
		return iFunc;
	}
	//----------------------------------------------------------------------
	//	記号表の検索
	//----------------------------------------------------------------------
	s32 CScriptCompiler::SearchName(const string& strName)
	{
		s32 i;
		for( i = m_SymbolTable.size()-1; i >= m_iLocalTblCount; i -- )
		{
			if( m_SymbolTable[i].strName == strName )
			{
				return i;
			}
		}
		for( ; i >= 1; i -- )
		{
			if( m_SymbolTable[i].eKind != ParamId &&
				m_SymbolTable[i].strName == strName )
			{
				return i;
			}
		}
		return -1;
	}

	//----------------------------------------------------------------------
	//	二項演算子か？
	//----------------------------------------------------------------------
	b8 CScriptCompiler::IsBinaryOp(EOpCode eOp)
	{
		return eOp == ADD  || eOp == SUB  || eOp == MUL || eOp == DIV  || eOp == MOD  ||
			   eOp == LESS || eOp == LSEQ || eOp == GRT || eOp == GTEQ || eOp == NTEQ ||
			   eOp == AND  || eOp == OR;
	}
	//----------------------------------------------------------------------
	//	定数演算
	//----------------------------------------------------------------------
	s32 CScriptCompiler::BinaryExpr(EOpCode eOp, s32 iData1, s32 iData2)
	{
#define	CALC_OP(label, op)	case label: iData1 = iData1 op iData2; break

		if( (eOp == DIV || eOp == MOD) && iData2 == 0 )
		{
			WarningTrace("ゼロ除算が行われています");
			iData2 = 1;
		}
		switch( eOp )
		{
			CALC_OP(ADD,	+);
			CALC_OP(SUB,	-);
			CALC_OP(MUL,	*);
			CALC_OP(DIV,	/);
			CALC_OP(MOD,	%);
			CALC_OP(LESS,	<);
			CALC_OP(LSEQ,	<=);
			CALC_OP(GRT,	>);
			CALC_OP(GTEQ,	>=);
			CALC_OP(EQU,	==);
			CALC_OP(NTEQ,	!=);
			CALC_OP(AND,	&&);
			CALC_OP(OR,		||);
		}
		return iData1;
	}
	//----------------------------------------------------------------------
	//	複数文字からなる演算しか？(++,--等)
	//----------------------------------------------------------------------
	b8 CScriptCompiler::IsOp2(c8 c1, c8 c2)
	{
		c8 s[] = "    ";
		s[1] = c1; s[2] = c2;

		return strstr(" ++ -- <= >= == != && ||", s) != null;
	}
	//----------------------------------------------------------------------
	//	コードが一致するか？
	//----------------------------------------------------------------------
	b8 CScriptCompiler::IsCode(s32 n, EOpCode eOp, s32 iData)
	{
		return m_Code[n].ucOpCode == eOp && m_Code[n].iOpData == iData;
	}
	//----------------------------------------------------------------------
	//	トークンの種類を設定
	//----------------------------------------------------------------------
	void CScriptCompiler::SetKind(Token& token)
	{
		token.eKind = Others;
		for( u32 i = 0; i < m_Keyword.size(); i++ )
		{
			if( token.strToken == m_Keyword[i].strKey )
			{
				token.eKind = m_Keyword[i].eKind;
				return;
			}
		}
		switch( m_eTknKindTbl[token.strToken[0]] )
		{
		case Letter:	token.eKind = Ident;	break;
		case Digit:		token.eKind = IntNum;	break;
		}
	}
	//----------------------------------------------------------------------
	//	トークンタイプを設定
	//----------------------------------------------------------------------
	void CScriptCompiler::SetType(void)
	{
		m_Symbol.iArrayLen = m_Symbol.iAdrs = 0;
		m_Symbol.cArgs = 0;

		m_Symbol.cLevel = m_cBlockNest;
		switch( m_Token.eKind )
		{
		case Int:	m_Symbol.eDataType = Int_T;		break;
		case Void:	m_Symbol.eDataType = Void_T;	break;
		default:
			ErrorTrace("型指定が正しくありません[%s]", m_Token.strToken.c_str());
			m_Symbol.eDataType = Int_T;
			break;
		}
		NextToken();
	}
	//----------------------------------------------------------------------
	//	トークンタイプを設定
	//----------------------------------------------------------------------
	void CScriptCompiler::SetName(void)
	{
		if( m_Token.eKind == Ident )
		{
			m_Symbol.strName = m_Token.strToken;
			NextToken();
		}
		else
		{
			ErrorTrace("記述が不適切です[%s]", m_Token.strToken.c_str());
			m_Symbol.strName = "tmp$name1";
		}
	}
	//----------------------------------------------------------------------
	//	関数宣言
	//----------------------------------------------------------------------
	void CScriptCompiler::FuncDecl(void)
	{
		m_iLocalAdrs = sizeof(s32);

		s32 iFunc = SearchName(m_Symbol.strName);
		if( iFunc != -1 &&
			m_SymbolTable[iFunc].eKind != FuncId &&
			m_SymbolTable[iFunc].eKind != ProtId )
		{
			ErrorTrace("識別子が重複しています");
			iFunc = -1;
		}

		m_iFuncId = Enter(m_Symbol, FuncId);
		NextToken();

		SetLocalTablePos();

		switch( m_Token.eKind )
		{
		case Void://引数なし
			NextToken();
			break;
		case ')'://引数なし
			break;
		default:
			while( true )
			{
				SetType();
				SetName();
				Enter(m_Symbol, ParamId);
				++(m_SymbolTable[m_iFuncId].cArgs);
				if( m_Token.eKind != ',' )	break;
				NextToken();
			}
			break;
		}
		CheckNextToken(')');

		if( m_Token.eKind == ';' )
		{
			m_SymbolTable[m_iFuncId].eKind = ProtId;
		}
		SetAdrs(&m_SymbolTable[m_iFuncId]);
		if( iFunc != -1 )
		{
			FuncCheck(&m_SymbolTable[iFunc], &m_SymbolTable[m_iFuncId]);
		}
		switch( m_Token.eKind )
		{
		case ';':
			NextToken();
			break;
		case '{':
			if( m_SymbolTable[m_iFuncId].strName == "main" )
			{
				SetMain();
			}
			FuncDeclBegin();
			Block(1);
			FuncDeclEnd();
			break;
		default:
			ErrorTrace("関数の ; または { がありません");
			break;
		}

		ResetLocalTablePos(&m_SymbolTable[m_iFuncId]);
		DelFuncTable(iFunc == -1? null: &m_SymbolTable[iFunc], &m_SymbolTable[m_iFuncId]);
		m_iFuncId = -1;
	}
	//----------------------------------------------------------------------
	//	関数の重複チェック
	//----------------------------------------------------------------------
	void CScriptCompiler::FuncCheck(Symbol* f1, Symbol* f2)
	{
		if( f1->eKind == FuncId && f2->eKind == FuncId )
		{
			ErrorTrace("関数が重複定義されています");
			return;
		}
		if( f1->eDataType != f2->eDataType ||
			f1->cArgs != f2->cArgs )
		{
			ErrorTrace("関数プロトタイプが不一致です[%s]", f1->strName.c_str());
			return;
		}
	}
	//----------------------------------------------------------------------
	//	関数入口処理
	//----------------------------------------------------------------------
	void CScriptCompiler::FuncDeclBegin(void)
	{
		GenCode2(ADBR, 0);
		GenCode3(STO, 1, 0);
		for( s32 i=m_SymbolTable[m_iFuncId].cArgs; i >= 1; i--)
		{
			GenCode3(STO, 1, m_SymbolTable[m_iFuncId + i].iAdrs);
		}
	}
	//----------------------------------------------------------------------
	//	関数出口処理
	//----------------------------------------------------------------------
	void CScriptCompiler::FuncDeclEnd(void)
	{
		BackPatch(m_SymbolTable[m_iFuncId].iAdrs, -m_iLocalAdrs);
		if( m_eLastState != Return )
		{
			if( m_SymbolTable[m_iFuncId].strName == "main" )
			{
				GenCode2(LDI, 0);
			}
			else if( m_SymbolTable[m_iFuncId].eDataType != Void_T )
			{
				ErrorTrace("末尾にreturnコードが存在しません");
			}
		}
		BackPatchRetrun(m_SymbolTable[m_iFuncId].iAdrs);
		GenCode3(LOD, 1, 0);
		GenCode2(ADBR, m_iLocalAdrs);
		GenCode1(RET);
	}
	//----------------------------------------------------------------------
	//	main関数処理
	//----------------------------------------------------------------------
	void CScriptCompiler::SetMain(void)
	{
		if( m_SymbolTable[m_iFuncId].eDataType != Int_T || m_SymbolTable[m_iFuncId].cArgs != 0 )
		{
			ErrorTrace("main関数の書式が不正です");
		}
		BackPatch(0, m_SymbolTable[m_iFuncId].iAdrs);
	}
	//----------------------------------------------------------------------
	//	コードの未定データを処理
	//----------------------------------------------------------------------
	void CScriptCompiler::Block(s32 iNest)
	{
		ETokenKind eKind = Others;
		NextToken();

		++ m_cBlockNest;
		if( iNest != 0 )
		{
			while( m_Token.eKind == Int )
			{
				SetType();
				SetName();
				VarDecl();
			}
		}
		while( m_Token.eKind != '}' )
		{
			eKind = m_Token.eKind;
			Statement();
		}
		m_eLastState = eKind;
		-- m_cBlockNest;

		NextToken();
	}
	//----------------------------------------------------------------------
	//	文の処理
	//----------------------------------------------------------------------
	void CScriptCompiler::Statement(void)
	{
#define	UpLabel(pos)	pos = m_Code.size();
#define	JMPUp(pos)		GenCode2(JMP, pos);
#define	JPTUp(pos)		GenCode2(JPT, pos);
#define	JMPDown(pos)	pos = GenCode2(JMP, 0)
#define	JPFDown(pos)	pos = GenCode2(JPF, 0)
#define	DownLabel(pos)	BackPatch(pos, m_Code.size())

		s32 LB_TOP, LB_EXP2, LB_EXP3, LB_BODY, LB_ELSE, LB_END, LB_TBL;
		ETokenKind eKind = m_Token.eKind;

		if( eKind == While || eKind == Do || eKind == For || eKind == Switch )
		{
			ContinueBreakBegin(eKind);
		}
		switch( eKind )
		{
		case Break:
			if( m_LoopNest.size() == 0 )
			{
				ErrorTrace("不正なbreakです");
			}
			else
			{
				GenCode2(JMP, NoFixBreakAdrs);
				m_LoopNest.back().bBreakFlag = true;
			}
			NextToken();
			CheckNextToken(';');
			break;
		case Continue:
			GenCode2(JMP, m_LoopNest.back().iLoopTop);
			NextToken();
			CheckNextToken(';');
			break;
		case Case:
			{
				NextToken();
				GetConst(null);
				ExprWithCheck(0, ':');

				s32 iValue;
				if( !GetConst(&iValue) )
				{
					ErrorTrace("case式が定数ではありません");
				}
				else if( m_SwitchNest.size() == 0 )
				{
					ErrorTrace("対応するswitch文がありません");
				}
				else
				{
					for( u32 i=m_SwitchNest.back().iStartCaseList; i < m_CaseList.size(); i++ )
					{
						if( m_CaseList[i].iValue == iValue )
						{
							ErrorTrace("重複する値が指定されています");
							break;
						}
					}
					CaseList newCase;
					newCase.iValue = iValue;
					newCase.iAdrs = m_Code.size();
					m_CaseList.push_back(newCase);
				}
				Statement();
			}
			break;
		case Default:
			if( m_SwitchNest.size() == 0 )
			{
				ErrorTrace("対応するswitch文がありません");
			}
			else if( m_SwitchNest.back().iDefAdrs != -1 )
			{
				ErrorTrace("default文が重複しています");
			}
			else
			{
				m_SwitchNest.back().iDefAdrs = m_Code.size();
			}
			NextToken();
			CheckNextToken(':');
			Statement();
			break;
		case For:
			//[式1]
			NextToken();
			CheckNextToken('(');
			if( m_Token.eKind == ';' )
			{
				NextToken();
			}
			else
			{
				ExprWithCheck(0, ';');
				RemoveValue();
			}

			//[式2]
			UpLabel(LB_EXP2);
			if( m_Token.eKind == ';' )
			{
				GenCode2(LDI, 1);
				NextToken();
			}
			else
			{
				ExprWithCheck(0, ';');
			}
			JPFDown(LB_END);
			JMPDown(LB_BODY);

			//[式3]
			ContinueBreakBegin(eKind);
			UpLabel(LB_EXP3);
			if( m_Token.eKind == ')' )
			{
				NextToken();
			}
			else
			{
				ExprWithCheck(0, ')');
				RemoveValue();
			}
			JMPUp(LB_EXP2);

			//[本文]
			DownLabel(LB_BODY);
			Statement();
			JMPUp(LB_EXP3);

			//[末尾]
			DownLabel(LB_END);
			break;

		case If:
			NextToken();
			ExprWithCheck('(', ')');
			JPFDown(LB_ELSE);
			Statement();
			if( m_Token.eKind != Else )
			{
				DownLabel(LB_ELSE);
				break;
			}
			JMPDown(LB_END);
			DownLabel(LB_ELSE);
			NextToken();
			Statement();
			DownLabel(LB_END);
			break;
		case While:
			NextToken();
			UpLabel(LB_TOP);
			ExprWithCheck('(', ')');
			JPFDown(LB_END);
			Statement();
			JMPUp(LB_TOP);
			DownLabel(LB_END);
			break;
		case Do:
			NextToken();
			UpLabel(LB_TOP);
			Statement();
			if( m_Token.eKind == While )
			{
				NextToken();
				ExprWithCheck('(', ')');
				CheckNextToken(';');
				JPTUp(LB_TOP);
			}
			else
			{
				ErrorTrace("doの終端にwhileが記述されていません");
			}
			break;
		case Switch:
			NextToken();
			ExprWithCheck('(', ')');
			JMPDown(LB_TBL);
			SwitchBegin();
			Statement();
			JMPDown(LB_END);
			DownLabel(LB_TBL);
			SwitchEnd();
			DownLabel(LB_END);
			break;
		case Return:
			NextToken();
			if( m_Token.eKind == ';' )
			{
				if( m_SymbolTable[m_iFuncId].eDataType != Void_T )
				{
					ErrorTrace("return文に戻り値がありません");
				}
			}
			else
			{
				Expression();
				if( m_SymbolTable[m_iFuncId].eDataType == Void_T )
				{
					ErrorTrace("void型関数に戻り値が指定されています");
				}
			}
			GenCode2(JMP, NoFixReturnAdrs);
			CheckNextToken(';');
			break;
		case Printf:
		case Exit:
			SystemFuncCall(eKind);
			CheckNextToken(';');
			break;
		case Input:
			ExprWithCheck(0, ';');
			RemoveValue();
			break;
		case Incre:
		case Decre:
			ExprWithCheck(0, ';');
			RemoveValue();
			break;
		case Ident:
			{
				s32 iFunc = Search(m_Token.strToken);
				if( (m_SymbolTable[iFunc].eKind == FuncId || m_SymbolTable[iFunc].eKind == ProtId) &&
					m_SymbolTable[iFunc].eDataType == Void_T )
				{
					FuncCall(&m_SymbolTable[iFunc]);
					CheckNextToken(';');
				}
				else
				{
					ExprWithCheck(0, ';');
					RemoveValue();
				}
			}
			break;
		case Lbrace:
			Block(0);
			break;
		case Semicolon:
			NextToken();
			break;
		case EofToken:
			ErrorTrace("意図しない終了です");
			break;
		default:
			ErrorTrace("不正な記述です[%s]", m_Token.strToken.c_str());
			NextToken();
			break;
		}
		if( eKind == For || eKind == While || eKind == Do || eKind == Switch )
		{
			ContinueBreakEnd();
		}
	}
	//----------------------------------------------------------------------
	//	関数呼び出し
	//----------------------------------------------------------------------
	void CScriptCompiler::SystemFuncCall(ETokenKind eKind)
	{
		NextToken();
		CheckNextToken('(');

		s32 iFuncType = 0;
		switch( eKind )
		{
		case Exit:
			iFuncType = Exit_F;
			Expression();
			break;
		case Input:
			iFuncType = Input_F;
			Expression();
			break;
		case Printf:
			if( m_Token.eKind != String )
			{
				ErrorTrace("printfの第一引数が不正です");
			}
			GenCode2(LDI, m_Token.iValue);
			NextToken();

			if( m_Token.eKind != ',' )
			{
				iFuncType = Printf1_F;
			}
			else
			{
				iFuncType = Printf2_F;
				NextToken();
				Expression();
				if( m_Token.eKind == ',' )
				{
					ErrorTrace("printfの引数は2個までです");
				}
			}
			break;
		}
		CheckNextToken(')');
		GenCode2(LIB, iFuncType);
	}
	//----------------------------------------------------------------------
	//	関数呼び出し
	//----------------------------------------------------------------------
	void CScriptCompiler::FuncCall(Symbol* pSym)
	{
		NextToken();
		CheckNextToken('(');

		s32 iArgs = 0;
		if( m_Token.eKind != ')' )
		{
			while( true )
			{
				Expression();
				++ iArgs;
				if( m_Token.eKind != ',' )
				{
					break;
				}
				NextToken();
			}
		}
		CheckNextToken(')');
		if( iArgs != pSym->cArgs )
		{
			ErrorTrace("引数の数が一致していません");
		}
		GenCode2(CALL, pSym->iAdrs);
	}

	//----------------------------------------------------------------------
	//	ループ文の開始
	//----------------------------------------------------------------------
	void CScriptCompiler::ContinueBreakBegin(ETokenKind eState)
	{
		LoopNest newLoop;
		newLoop.eKind = eState;
		newLoop.iLoopTop = m_Code.size();
		newLoop.bBreakFlag = false;

		m_LoopNest.push_back(newLoop);
	}
	//----------------------------------------------------------------------
	//	ループ文の終了
	//----------------------------------------------------------------------
	void CScriptCompiler::ContinueBreakEnd(void)
	{
		if( m_LoopNest.back().bBreakFlag )
		{
			BackPatchBreak(m_LoopNest.back().iLoopTop);
		}
		m_LoopNest.pop_back();
	}
	//----------------------------------------------------------------------
	//	ループ文の先頭番地を取得
	//----------------------------------------------------------------------
	s32 CScriptCompiler::GetLoopTop(void)
	{
		for( s32 i=m_LoopNest.size()-1; i > 0; i-- )
		{
			if( m_LoopNest[i].eKind != Switch )
			{
				return m_LoopNest[i].iLoopTop;
			}
		}
		ErrorTrace("continueがループ文の中で使用されていません");
		return 0;
	}
	//----------------------------------------------------------------------
	//	switch文の開始
	//----------------------------------------------------------------------
	void CScriptCompiler::SwitchBegin(void)
	{
		SwitchNest newSwitch;

		newSwitch.iDefAdrs	= -1;
		newSwitch.iStartCaseList = m_CaseList.size();

		m_SwitchNest.push_back(newSwitch);
	}
	//----------------------------------------------------------------------
	//	switch文の終了
	//----------------------------------------------------------------------
	void CScriptCompiler::SwitchEnd(void)
	{
		s32 iStart = m_SwitchNest.back().iStartCaseList;

		for( u32 i=0; i <= m_SwitchNest.size(); i++ )
		{
			GenCode2(EQCMP, m_CaseList[i].iValue);
			GenCode2(JPT, m_CaseList[i].iAdrs);
		}
		GenCode1(DEL);

		if( m_SwitchNest.back().iDefAdrs != -1 )
		{
			GenCode2(JMP, m_SwitchNest.back().iDefAdrs);
		}

		m_CaseList.erase(iStart, m_CaseList.size() - iStart);
		m_SwitchNest.pop_back();
	}

	//----------------------------------------------------------------------
	//	コードの未定データを処理
	//----------------------------------------------------------------------
	void CScriptCompiler::BackPatch(s32 n, s32 iAdrs)
	{
		m_Code[n].iOpData = iAdrs;
	}
	//----------------------------------------------------------------------
	//	コードの未定データを処理
	//----------------------------------------------------------------------
	void CScriptCompiler::BackPatchCallAdrs(void)
	{
		if( m_Code[0].iOpData < 0 )
		{
			ErrorTrace("main関数がありません");
		}

		s32 n;
		for( u32 i=2; i < m_Code.size(); i++ )
		{
			n = m_Code[i].iOpData;
			if( m_Code[i].ucOpCode == CALL && n < 0 )
			{
				m_Code[i].iOpData = m_SymbolTable[-n].iAdrs;
				if( m_Code[i].iOpData < 0 )
				{
					ErrorTrace("未定義の関数です[%s]", m_SymbolTable[-n].strName.c_str());
				}
			}
		}
	}
	//----------------------------------------------------------------------
	//	コードの未定データを処理
	//----------------------------------------------------------------------
	void CScriptCompiler::BackPatchRetrun(s32 iFuncAdrs)
	{
		s32 i;
		for( i = m_Code.size()-1; i >= iFuncAdrs; i-- )
		{
			if( IsCode(i, JMP, NoFixReturnAdrs) )
			{
				m_Code.pop_back();
			}
			break;
		}
		for( i = m_Code.size()-1; i >= iFuncAdrs; i-- )
		{
			if( IsCode(i, JMP, NoFixReturnAdrs) )
			{
				m_Code[i].iOpData = m_Code.size();
			}
		}
	}
	//----------------------------------------------------------------------
	//	コードの未定データを処理
	//----------------------------------------------------------------------
	void CScriptCompiler::BackPatchBreak(s32 iLoopTop)
	{
		s32 i;
		for( i = m_Code.size()-1; i >= iLoopTop; i-- )
		{
			if( IsCode(i, JMP, NoFixBreakAdrs) )
			{
				m_Code.pop_back();
			}
			break;
		}
		for( i = m_Code.size()-1; i >= iLoopTop; i-- )
		{
			if( IsCode(i, JMP, NoFixBreakAdrs) )
			{
				m_Code[i].iOpData = m_Code.size();
			}
		}
	}

	//----------------------------------------------------------------------
	//	変数宣言
	//----------------------------------------------------------------------
	void CScriptCompiler::VarDecl(void)
	{
		while( true )
		{
			SetArrayLen();
			Enter(m_Symbol, VarId);
			if( m_Token.eKind != ',' ) break;
			NextToken();
			SetName();
		}
		CheckNextToken(';');
	}
	//----------------------------------------------------------------------
	//	配列サイズ設定
	//----------------------------------------------------------------------
	void CScriptCompiler::SetArrayLen(void)
	{
		Symbol& symbol = m_Symbol;
		symbol.iArrayLen = 0;
		if( m_Token.eKind != '[' ) return;

		NextToken();
		if( m_Token.eKind == ']' )
		{
			ErrorTrace("配列の添字が指定されていません");
			NextToken();
			symbol.iArrayLen = 1;
			return;
		}
		GetConst(null);
		ExprWithCheck(0, ']');
		if( GetConst(&symbol.iArrayLen) )
		{
			if( symbol.iArrayLen <= 0 )
			{
				symbol.iArrayLen = 1;
				ErrorTrace("不正な添字が指定されています");
			}
		}
		else
		{
			ErrorTrace("不正な添字が指定されています");
		}
		if( m_Token.eKind == '[' )
		{
			ErrorTrace("多次元配列に対応していません");
		}
	}
	//----------------------------------------------------------------------
	//	定数の取得
	//----------------------------------------------------------------------
	b8 CScriptCompiler::GetConst(s32* pVar)
	{
		if( pVar == null )
		{
			m_iCodeCount = m_Code.size();
			return true;
		}

		if( m_iCodeCount == (s32)m_Code.size()-1 && m_Code.back().ucOpCode == LDI )
		{
			*pVar = m_Code.back().ucOpCode;
			m_Code.pop_back();
			return true;
		}
		return false;
	}
	//----------------------------------------------------------------------
	//	左辺値に変更する
	//----------------------------------------------------------------------
	void CScriptCompiler::ToLeftValue(void)
	{
		switch( m_Code.back().ucOpCode )
		{
		case VAL:	m_Code.pop_back();	break;
		case LOD:	m_Code.back().ucOpCode = LDA;	break;
		default:
			ErrorTrace("不正な左辺値");
			break;
		}
	}
	//----------------------------------------------------------------------
	//	式の値を削除する
	//----------------------------------------------------------------------
	void CScriptCompiler::RemoveValue(void)
	{
		if( m_Code.back().ucOpCode == ASSV )
		{
			m_Code.back().ucOpCode = ASS;
		}
		else
		{
			GenCode1(DEL);
		}
	}
	//----------------------------------------------------------------------
	//	次のトークン
	//----------------------------------------------------------------------
	const Token* CScriptCompiler::NextToken(void)
	{
		Token& token = m_Token;

		token.eKind		= NulKind;
		token.strToken	= "";
		token.iValue	= 0;

		c8 c = m_cChar;
		//	スペースを読み飛ばす
		while( isspace(c) )
			c = NextChar();

		if( c == EOF ) { token.eKind = EofToken; return &m_Token; }

		switch( m_eTknKindTbl[c] )
		{
		case Letter:
			for( ; m_eTknKindTbl[c] == Letter || m_eTknKindTbl[c] == Digit; c = NextChar() )
			{
				token.strToken += c;
			}
			break;
		case Digit:
			{
				s32 num = 0;
				for( ; m_eTknKindTbl[c] == Digit; c = NextChar() )
				{
					num = num * 10 + (c - '0');
				}
				token.eKind		= IntNum;
				token.iValue	= num;
				token.strToken	= num;
			}
			break;
		case SngQ:
			{
				b8 bErr = false;
				s32 ct = 0;
				for( c = NextChar(); c != EOF && c != '\n' && c != '\''; c = NextChar() )
				{
					if( c == '\\' )
					{
						if( (c = NextChar()) == 'n' ) c = '\n';
					}
					if( ++ ct == 1 ) token.iValue = c;
				}
				if( ct != 1 )	bErr = true;

				if( c == '\'' ) c = NextChar();
				else			bErr = true;

				if( bErr )
				{
					ErrorTrace("不正な文字定数です");
				}
				token.eKind = IntNum;
				token.strToken = (c8)token.iValue;
			}
			break;
		case DblQ:
			c = NextChar();
			while( c != EOF && c != '\n' && c != '"' )
			{
				if( CharSet::IsMultiByteChar(c) )
				{
					token.strToken += c;
					token.strToken += NextChar();
					c = NextChar();
					continue;
				}
				if( c == '\\' )
				{
					if( (c = NextChar()) == 'n' ) c = '\n';
				}
				token.strToken += c;
				c = NextChar();
			}
			if( c == '"' )	c = NextChar();
			else			ErrorTrace("文字列リテラルが閉じていません");

			token.eKind		= String;
			token.iValue	= AllocateS(token.strToken.c_str());
			break;
		default:
			if( c < 0 || 127 < c )
			{
				ErrorTrace("不正な文字が使用されています");
			}
			if( CharSet::IsMultiByteChar(c) )
			{
				token.strToken += c;
				c = NextChar();
			}
			token.strToken += c;
			c = NextChar();

			if( IsOp2(token.strToken[0], c) )
			{
				token.strToken += c;
				c = NextChar();
			}
			break;
		}
		if( token.eKind == NulKind )
		{
			SetKind(token);
		}
		if( token.eKind == Others )
		{
			ErrorTrace("不正なトークンです(%s)", token.strToken.c_str());
		}
		return &m_Token;
	}
	//----------------------------------------------------------------------
	//	次のトークン
	//----------------------------------------------------------------------
	const Token* CScriptCompiler::CheckNextToken(c8 c)
	{
		if( m_Token.eKind == c )
		{
			return NextToken();
		}
		ErrorTrace("%s の前に %c がありません", m_Token.strToken.c_str(), c);
		return &m_Token;
	}
	//----------------------------------------------------------------------
	//	前後のトークンチェック付き解析
	//----------------------------------------------------------------------
	void CScriptCompiler::ExprWithCheck(c8 c1, c8 c2)
	{
		if( c1 != 0 )	CheckNextToken(c1);
		Expression();
		if( c2 != 0 )	CheckNextToken(c2);
	}
	//----------------------------------------------------------------------
	//	
	//----------------------------------------------------------------------
	void CScriptCompiler::Expression(void)
	{
		Term(2);
		if( m_Token.eKind == '=' )
		{
			ToLeftValue();
			NextToken();
			Expression();
			GenCode1(ASSV);
		}
	}
	//----------------------------------------------------------------------
	//	
	//----------------------------------------------------------------------
	void CScriptCompiler::Term(s32 n)
	{
		if( n == 8 )
		{
			Factor();
			return;
		}

		Term(n + 1);
		while( n == OpOrder(m_Token.eKind) )
		{
			ETokenKind eKind = m_Token.eKind;
			NextToken();
			Term(n + 1);
			GenCodeBinary(eKind);
		}
	}
	//----------------------------------------------------------------------
	//	
	//----------------------------------------------------------------------
	void CScriptCompiler::Factor(void)
	{
		ETokenKind eKind = m_Token.eKind;

		switch( eKind )
		{
		case Plus:
		case Minus:
		case Not:
		case Incre:
		case Decre:
			NextToken();
			Factor();
			if( eKind == Incre || eKind == Decre )
			{
				ToLeftValue();
			}
			GenCodeUnary(eKind);
			break;
		case IntNum:
			GenCode2(LDI, m_Token.iValue);
			NextToken();
			break;
		case Lparen:
			ExprWithCheck('(', ')');
			break;
		case Input:
			SystemFuncCall(eKind);
			break;
		case Ident:
			{
				s32 iFunc = Search(m_Token.strToken);
				switch( m_SymbolTable[iFunc].eKind )
				{
				case FuncId:
				case ProtId:
					if( m_SymbolTable[iFunc].eDataType == Void_T )
					{
						ErrorTrace("void関数が式として呼び出されました[%s]", m_SymbolTable[iFunc].strName.c_str());
					}
					FuncCall(&m_SymbolTable[iFunc]);
					break;
				case VarId:
				case ParamId:
					if( m_SymbolTable[iFunc].iArrayLen == 0 )
					{
						GenCode3(LOD, m_SymbolTable[iFunc].cLevel != 0, m_SymbolTable[iFunc].iAdrs);
						NextToken();
					}
					else
					{
						NextToken();
						if( m_Token.eKind == '[' )
						{
							GenCode3(LDA, m_SymbolTable[iFunc].cLevel != 0, m_SymbolTable[iFunc].iAdrs);
							ExprWithCheck('[', ']');
							GenCode2(LDI, sizeof(s32));
							GenCode1(MUL);
							GenCode1(ADD);
							GenCode1(VAL);
						}
						else
						{
							ErrorTrace("添字指定が不正です");
						}
					}
					//	後置++
					if( m_Token.eKind == Incre || m_Token.eKind == Decre )
					{
						ToLeftValue();
						if( m_Token.eKind == Incre )
						{
							GenCode1(INC);
							GenCode2(LDI, 1);
							GenCode1(SUB);
						}
						else
						{
							GenCode1(DEC);
							GenCode2(LDI, 1);
							GenCode1(ADD);
						}
						NextToken();
					}
					break;

				default:
					ErrorTrace("不正な記述です[%s]", m_Token.strToken.c_str());
					break;
				}
			}
		}
	}

	//----------------------------------------------------------------------
	//	次の文字
	//----------------------------------------------------------------------
	c8 CScriptCompiler::NextChar(void)
	{
		c8& c = m_cChar;
		if( c == EOF ) return c;
		if( c == '\n' ) ++ m_iLineNo;

		c = ReadNextChar();
		if( c == EOF )
		{
			return c;
		}
		c8& quot_c = m_cQuotChar;
		if( quot_c )
		{
			if( c == quot_c || c== '\n' )
			{
				quot_c = 0;
			}
			return c;
		}
		if( c == '\'' || c == '"' )
		{
			quot_c = c;
		}
		else if( c == '/' )
		{
			c8 c2 = ReadNextChar();
			switch( c2 )
			{
			case '/':
				while( (c = ReadNextChar()) != EOF && c != '\n' );
				return c;
			case '*':
				for( c = 0; (c2 = ReadNextChar()) != EOF; c = c2 )
				{
					if( c2 == '\n' ) ++ m_iLineNo;
					if( c == '*' && c2 == '/' )
					{
						c = ' ';
						return c;
					}
				}
				ErrorTrace("/*のコメントが閉じられていません");
				c = EOF;
				return EOF;
			}
			SeekPrevChar();
		}
		else if( c == '*' )
		{
			c8 c2 = ReadNextChar();
			if( c2 == '/' )
			{
				ErrorTrace("*/ に対する /* が見つかりませんでした");
				c = EOF;
				return EOF;
			}
			SeekPrevChar();
		}
		return c;
	}

	//----------------------------------------------------------------------
	//	次の文字
	//----------------------------------------------------------------------
	c8 CScriptCompiler::ReadNextChar(void)
	{
		c8 c = EOF;
		m_pFile->Read(&c, sizeof(c));

		return c;
	}
	//----------------------------------------------------------------------
	//	前の文字
	//----------------------------------------------------------------------
	void CScriptCompiler::SeekPrevChar(void)
	{
		m_pFile->Seek(-1, File::EFilePointer::Current);
	}
}


//======================================================================
//	END OF FILE
//======================================================================
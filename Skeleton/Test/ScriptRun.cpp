//----------------------------------------------------------------------
//!
//!	@file	ScriptRun.cpp
//!	@brief	スクリプト実行関連
//!
//!	@author S.Kisaragi
//----------------------------------------------------------------------
//----------------------------------------------------------------------
// include
//----------------------------------------------------------------------
#include "App.h"
#include "Script.h"

namespace Script
{
	//----------------------------------------------------------------------
	//	コードを表示
	//----------------------------------------------------------------------
	void CScriptCompiler::CodeDump(void)
	{
		c8* ssOpCode[] =
		{
			"NOP", "INC", "DEC", "NEG", "NOT", "ADD", "SUB", "MUL", "DIV", "MOD",
			"LESS", "LSEQ", "GRT", "GTEQ", "EQU", "NTEQ", "AND", "OR",
			"CALL", "DEL", "JMP", "JPT", "JPF", "EQCMP",
			"LOD", "LDA", "LDI", "STO", "ADBR", "RET", "ASS", "ASSV", "VAL", "LIB", "STOP",
		};
		u8 op;
		c8 cTemp[256];
		for( u32 n=0; n < m_Code.size(); n++ )
		{
			Inst& code = m_Code[n];
			op = code.ucOpCode;

			switch( op )
			{
			case RET:  case ASS:  case ASSV: case NOT:  case INC: case DEC:
			case NEG:  case ADD:  case SUB:  case MUL:  case DIV: case MOD:
			case LESS: case LSEQ: case GRT:  case GTEQ: case EQU: case NTEQ:
			case AND:  case OR:   case VAL:  case DEL:  case NOP: case STOP:
				CharSet::SFormat(cTemp, "%-5s", ssOpCode[op]);
				if( op == RET || op == STOP )
				{
					CharSet::SFormat(cTemp, "%s\n", cTemp);
				}
				break;
			case LOD: case LDA: case LDI: case STO: case CALL: case ADBR: case LIB:
			case JMP: case JPT: case JPF: case EQCMP:
				CharSet::SFormat(cTemp, "%d ", code.iOpData);
				if( code.ucFlag && (op == LOD || op == LDA || op == STO) )
				{
					CharSet::SFormat(cTemp, "%s[b] ", cTemp);
				}
				CharSet::SFormat(cTemp, "%-5s %-11s", ssOpCode[op], cTemp);
				break;
			default:
				CharSet::SFormat(cTemp, "Illegal code(code=%d)", op);
				break;
			}
			Trace("%04d: %s", n, cTemp);
		}
	}

	//----------------------------------------------------------------------
	//	実行
	//----------------------------------------------------------------------
	s32 CScriptCompiler::Execute(void)
	{
		m_iPgCounter = 0;
		m_iStackPtr = 0;

		s32 iBaseReg = m_iMemSize;

		u8 ucOpCpde;
		s32 iOpData, iAdrs;
		while( true )
		{
			if( m_iPgCounter < 0 || (s32)m_Code.size() <= m_iPgCounter )
			{
				ErrorTrace("不正な終了です[PgCounter:%d]", m_iPgCounter);
				return -1;
			}

			Inst& code = m_Code[m_iPgCounter];
			ucOpCpde = code.ucOpCode;
			iOpData  = code.iOpData;
			iAdrs = (code.ucFlag & 0x01)?
				iBaseReg + iOpData:
				iOpData;

			++ m_iPgCounter;

			switch( ucOpCpde )
			{
			case DEL:
				-- m_iStackPtr;
				break;
			case STOP:
				if( m_iStackPtr > 0 )
				{
					return Pop();
				}
				return 0;
			case JMP:
				m_iPgCounter = iOpData;
				break;
			case JPT:
				if( Pop() )
				{
					m_iPgCounter = iOpData;
				}
				break;
			case JPF:
				if( !Pop() )
				{
					m_iPgCounter = iOpData;
				}
				break;
			case LIB:
				if( iOpData == Exit_F )
				{
					return Pop();
				}
				CallLibrary(iOpData);
				break;
			case LOD:
				Push(IntAdrs(iAdrs));
				break;
			case LDA:
				Push(iAdrs);
				break;
			case LDI:
				Push(iOpData);
				break;
			case STO:
				IntAdrs(iAdrs) = Pop();
				break;
			case ADBR:
				iBaseReg += iOpData;
				//	メモリスタックオーバーチェック
				break;
			case NOP:
				++ m_iPgCounter;
				break;
			case ASS:
				{
					s32 iTmpData = Pop();
					s32 iTmpAdrs = Pop();
					IntAdrs(iTmpAdrs) = iTmpData;
				}
				break;
			case ASSV:
				{
					s32 iTmpData = Pop();
					Pop();
					IntAdrs(iAdrs) = iTmpData;
					Push(iTmpData);
				}
				break;
			case VAL:
				Push(IntAdrs(Pop()));
				break;
			case EQCMP:
				{
					if( iOpData == Last() )
					{
						Pop();
						Push(1);
					}
					else
					{
						Push(0);
					}
				}
				break;
			case CALL:
				Push(m_iPgCounter);
				m_iPgCounter = iOpData;
				break;
			case RET:
				m_iPgCounter = Pop();
				break;
			case INC:
				{
					s32 iTmpAdrs = Pop();
					++ IntAdrs(iTmpAdrs);
					Push(IntAdrs(iTmpAdrs));
				}
				break;
			case DEC:
				{
					s32 iTmpAdrs = Pop();
					-- IntAdrs(iTmpAdrs);
					Push(IntAdrs(iTmpAdrs));
				}
				break;
			case NOT:	Push(!Pop());			break;
			case NEG:	Push(-Pop());			break;
			case DIV:	{ s32 iTmpData = Pop(); Push(Pop() / iTmpData);	} break;
			case MOD:	{ s32 iTmpData = Pop(); Push(Pop() % iTmpData);	} break;
			case ADD:	{ s32 iTmpData = Pop(); Push(Pop() + iTmpData);	} break;
			case SUB:	{ s32 iTmpData = Pop(); Push(Pop() - iTmpData);	} break;
			case MUL:	{ s32 iTmpData = Pop(); Push(Pop() * iTmpData);	} break;
			case LESS:	{ s32 iTmpData = Pop(); Push(Pop() < iTmpData);	} break;
			case LSEQ:	{ s32 iTmpData = Pop(); Push(Pop() <=iTmpData); } break;
			case GRT:	{ s32 iTmpData = Pop(); Push(Pop() > iTmpData);	} break;
			case GTEQ:	{ s32 iTmpData = Pop(); Push(Pop() >=iTmpData);	} break;
			case EQU:	{ s32 iTmpData = Pop(); Push(Pop() ==iTmpData);	} break;
			case NTEQ:	{ s32 iTmpData = Pop(); Push(Pop() !=iTmpData);	} break;
			case AND:	{ s32 iTmpData = Pop(); Push(Pop() &&iTmpData);	} break;
			case OR:	{ s32 iTmpData = Pop(); Push(Pop() ||iTmpData);	} break;
			}
		}
	}
	//----------------------------------------------------------------------
	//	スタック情報の追加
	//----------------------------------------------------------------------
	void CScriptCompiler::Push(s32 iData)
	{
		m_iStack[++ m_iStackPtr] = iData;
	}
	//----------------------------------------------------------------------
	//	スタック情報の取り出し
	//----------------------------------------------------------------------
	s32 CScriptCompiler::Pop(void)
	{
		return m_iStack[m_iStackPtr --];
	}
	//----------------------------------------------------------------------
	//	スタック情報の末尾を取得
	//----------------------------------------------------------------------
	s32 CScriptCompiler::Last(void)
	{
		return m_iStack[m_iStackPtr];
	}
	//----------------------------------------------------------------------
	//	組み込み関数の呼び出し
	//----------------------------------------------------------------------
	void CScriptCompiler::CallLibrary(s32 iOpData)
	{
		s32 iData = 0;
		if( iOpData != Input_F )
		{
			iData = Pop();
		}

		c8 ss[256];
		switch( iOpData )
		{
		case Input_F:
			fgets(ss, sizeof(ss), stdout);
			Push(atoi(ss));
			break;
		case Printf1_F:
			printf("%s", MemAdrs(iData));
			break;
		case Printf2_F:
			printf((c8*)MemAdrs(Pop()), iData);
			break;
		}
	}
}


//======================================================================
//	END OF FILE
//======================================================================